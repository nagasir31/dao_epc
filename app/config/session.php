<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Session namespace
$config['sess_namespace'] = md5($_SERVER['HTTP_HOST'].'_'.APPLICATION);

/* End of file session.php */
/* Location: ./application/config/session.php */
