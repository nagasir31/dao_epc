<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('display_errors', 0);
error_reporting(E_ALL);
error_reporting(E_ALL ^E_WARNING);
class MY_Controller extends CI_Controller
{
	public $site_config;
	public $words = array();
	public $header = array();
	public $footer = array();
	public $module;
	public $module_functions;
	public $ref_verify_status;
	public $ref_berkas_status;
	public $ref_payment_method;
	public $array_proyeks;
	public $ref_berkas_type;
	public $http_usr_sdmonline = 'ret';
	public $http_pwd_sdmonline = '0712f913ed30e7508380f596cf53a4ef';
	public $api_server_sdmonline = 'http://sdmonline.ptpp.co.id/2/api/';
	public $api_key_sdmonline = '255d95b789ccbc23ff3e7aebeb5914f7';
	public $month_triwulan = array(3,6,9,12);
	
	function __construct()
	{
		parent::__construct();
		$this->_module = $this->router->fetch_module();
		
		$this->instance = &get_instance();
		$this->class = strtolower(get_class($this->instance));
		
		$this->load->database();
		
		$this->load->model('def_model');
		
		$this->load->library(array('session','login_manager','date_indonesia','form_validation','user_agent'));
		
		$this->load->helper(array('url','inflector','jsonwrapper/jsonwrapper','convert_date','text','utf8','file','form','application','cookie','relative_time'));
		
		$res_config = $this->db->get('sys_config')->result_array();
		foreach($res_config as $data)
		{
			$this->site_config[$data['keyword']] = $data['value'];
		}
		
		if(isset($this->site_config['app_logo']))
        {
			if(! file_exists(ABSOLUTE_PATH.'asset/global/images/'.$this->site_config['app_logo'])) 
			{
				$this->site_config['app_logo'] = "blank.gif";
			}
        }
		
		// set language
		$this->lang->load('global', $this->get_lang());	
		
		$param_lang['table'] = 'sys_language';
		if(! $this->site_config['is_multilingual'])
		{
			$param_lang['where'] = array('name' => $this->get_lang());
		}
		$this->header['languages'] = $this->def_model->get_list($param_lang)->result_array();
		
		// set themes
		$this->header['themes'] = $this->def_model->get_list(array('table' => 'sys_themes'))->result_array();
		
		$this->form_validation->set_message('required', $this->lang->line('global_error_required_field'));
		
		$ref_verify_status = $this->db->get('ref_verify_status')->result_array();
		foreach($ref_verify_status as $data)
		{
			$this->ref_verify_status[$data['id']] = $data['name'];
		}
		
		$ref_berkas_status = $this->db->get('ref_berkas_status')->result_array();
		foreach($ref_berkas_status as $data)
		{
			$this->ref_berkas_status[$data['id']] = $data['name'];
		}
		
		$ref_payment_method = $this->db->get('ref_payment_method')->result_array();
		foreach($ref_payment_method as $data)
		{
			$this->ref_payment_method[$data['id']] = $data['name'];
		}
		
		$ref_berkas_type = $this->db->get('ref_berkas_type')->result_array();
		foreach($ref_berkas_type as $data)
		{
			$this->ref_berkas_type[$data['id']] = $data['name'];
		}
		
		$this->array_proyeks = $this->get_array_proyeks();
	}
	
	function session_key($key)
	{
		return APPLICATION.'_'.$key;
	}
	
	protected function js_min($string)
	{
		return $this->jsmin->minify(trim(str_replace("''", "'",$string)));
	}
	
	protected function js_array($php_array, $obj_name, &$obj = '') {
		$obj .= $obj_name . " = new Array(); \r\n ";
		foreach ($php_array as $key => $value) {
			$outKey = (is_int($key)) ? '[' . $key . ']' : "['" . $key . "']";

			if(is_array($value)) 
			{
				$this->js_array($value, $obj_name . $outKey, $obj);
				continue;
			}
			
			$obj .= $obj_name . $outKey . " = ";
			if(is_string($value)) 
			{
				$obj .= "'" . $value . "'; \r\n ";
			} 
			else if($value === FALSE) 
			{
				$obj .= "false; \r\n";
			} 
			else if($value === NULL) 
			{
				$obj .= "null; \r\n";
			}
			else if ($value === TRUE) 
			{
				$obj .= "true; \r\n";
			} 
			else 
			{
				$obj .= $value . "; \r\n";
			}
		}
	   
		return $obj;
	}
	
	function set_lang($lang)
	{
		$this->session->set_userdata($this->session_key('language'),$lang);
		return TRUE;
	}
	
	function get_lang()
	{
		if($this->session->userdata($this->session_key('language')))
		{
			return $this->session->userdata($this->session_key('language'));
		}
		else
		{
			$lang = $this->session->userdata($this->session_key('language')) ? $this->session->userdata($this->session_key('language')) : $this->site_config['app_def_lang'];
			$this->set_lang($lang);
			return $lang;
		}
	}
	
	function set_user_picture($src)
	{
		$this->session->set_userdata($this->session_key('user_picture'),$src);
		return TRUE;
	}
	
	function get_user_picture()
	{
		return $this->session->userdata($this->session_key('user_picture'));
	}
	
	protected function debug_query()
	{
		echo $this->db->last_query();die();
	}
	
	protected function print_die($input)
	{
		print_r($input);die(' - Print die !!!');
	}
	
	protected function debug()
	{
		$this->output->enable_profiler(TRUE);
	}
	
	public function get_date()
	{
		return date("Y/m/d");
	}
	
	public function get_date_time()
	{
		return date("Y/m/d H:i:s");
	}
	
	public function get_user_name()
	{
		return $this->session->userdata(APPLICATION.'_user_name');
	}
	
	public function get_user_id()
	{
		return $this->session->userdata(APPLICATION.'_user_id');
	}
	
	public function get_employee_name()
	{
		return $this->session->userdata(APPLICATION.'_name');
	}
	
	public function get_employee_id()
	{
		return $this->session->userdata(APPLICATION.'_id');
	}
	
	public function get_jabatan_id()
	{
		return $this->session->userdata(APPLICATION.'_ref_jabatan_id');
	}
	
	public function get_jabatan_glosarry()
	{
		if($this->session->userdata(APPLICATION.'_jabatan_glosarry'))
		{
			if( preg_match("/VER M/i", $this->session->userdata(APPLICATION.'_jabatan_glosarry')))
			{
				return 'VER M';
			}
			else
			{
				return $this->session->userdata(APPLICATION.'_jabatan_glosarry');
			}
		}
		else
		{
			return '';
		}
		return $this->session->userdata(APPLICATION.'_jabatan_glosarry') ? $this->session->userdata(APPLICATION.'_jabatan_glosarry') : '';
	}
	
	public function get_employee_email()
	{
		return $this->session->userdata(APPLICATION.'_email');
	}
	
	public function get_group_name()
	{
		return $this->session->userdata(APPLICATION.'_group_name');
	}
	
	public function get_group_name2()
	{
		$group_name = $this->session->userdata(APPLICATION.'_group_name') ? $this->session->userdata(APPLICATION.'_group_name') : 'unknown';
		return $this->replace_underscore($group_name);
	}
	
	public function get_group_child_id()
	{
		return $this->session->userdata(APPLICATION.'_sys_group_child_id');
	}
	
	public function get_group_child($id)
	{
		$prm_list = array(
			'table' => 'sys_group_child', 
			'where' => array('id' => $id), 
			'select' => 'name, description'
		);
		return $this->def_model->get_one($prm_list)->row_array();
	}
	
	public function get_jabatan($id)
	{
		$prm_list = array(
			'table' => 'ref_jabatan', 
			'where' => array('id' => $id), 
			'select' => 'name, glosarry'
		);
		return $this->def_model->get_one($prm_list)->row_array();
	}
	
	public function get_employee_by_id($id)
	{
		$prm_list = array(
			'table' => 'employee', 
			'where' => array('id' => $id)
		);
		return $this->def_model->get_one($prm_list)->row_array();
	}
	
	public function get_employee($where = array())
	{
		if(! empty($where))
		{
			$res_employee = $this->def_model->get_one(
				array(
					'table' => 'employee a', 
					'select' => 'a.*,b.glosarry jabatan_glosarry, c.name cabang, d.name status_name',
					'where' => $where,
					'join' => array(
						array(
							'table' => "ref_jabatan b",
							'on'    => "a.ref_jabatan_id = b.id",
							'type'  => 'left'
						),
						array(
							'table' => "ref_cabang c",
							'on'    => "a.ref_cabang_id = c.id",
							'type'  => 'left'
						),
						array(
							'table' => 'ref_status d',
							'on' => 'a.ref_status_id = d.id',
							'type' => 'left'
						)
					)
				)
			)->row_array();
			
			return $res_employee;
		}
		else
		{
			return $where;
		}
	}
	
	public function get_divisi()
	{
		return $this->session->userdata(APPLICATION.'_divisi');
	}
	
	public function get_cabang_id()
	{
		return $this->session->userdata(APPLICATION.'_cabang_id');
	}
	
	public function get_proyeks()
	{
		return $this->session->userdata(APPLICATION.'_proyeks');
	}
	
	public function get_ref_proyek_id()
	{
		return $this->session->userdata(APPLICATION.'_ref_proyek_id');
	}
	
	public function get_array_proyeks()
	{
		$proyek_id = $this->get_ref_proyek_id();
		$proyeks = $this->get_proyeks();

		if(! empty($proyek_id) && $proyek_id != 1 && $proyek_id != 2)
		{
			$proyeks[] = $proyek_id;
		}
		
		return $proyeks;
	}
	
	public function get_cabang($id)
	{
		$prm_list = array(
			'table' => 'ref_cabang', 
			'where' => array('id' => $id), 
			'select' => 'name'
		);
		$res = $this->def_model->get_one($prm_list)->row_array();
		$cabang = isset($res['name']) && ! empty($res['name']) ? $res['name'] : '';
		return $cabang;
	}
	
	function get_pic_url()
	{
		if($_SERVER['HTTP_HOST'] == 'localhost')
		{
			$pic_url = 'http://localhost/sdmonline/app/assets/images/employee_photos/';
		}
		else
		{
			$pic_url = 'http://sdmonline.ptpp.co.id/2/app/assets/images/employee_photos/';
		}
		
		$pic_url = 'http://sdmonline.ptpp.co.id/2/app/assets/images/employee_photos/';
		
		return $pic_url;
	}
	
	public function get_ip_address()
	{
		return gethostbyname($_SERVER['REMOTE_ADDR']);
	}
	
	function get_ip_client()
	{
		if (! empty($_SERVER['HTTP_CLIENT_IP']))
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
	    {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		return $ip;
	} 
	
	protected function sess_check_redirect()
	{	
		$href = 'javascript: parent.window != "undefined" ? parent.window.location = "backend/login" : window.location = "backend/login"';
		$lang = sprintf($this->lang->line('global_session_redirect'), $href);
		echo "
			<div>{$lang}</div>
			<script>
				var Timer;
				var TotalSeconds;
				Timer = document.getElementById('timer');
				TotalSeconds = 10;
				UpdateTimer();
				window.setTimeout('Tick()', 1000);
				function Tick() {
					TotalSeconds -= 1;
					if (TotalSeconds <= 0) {
						if(parent.window != 'undefined'){
							parent.window.location = 'backend/login';
						}else{
							window.location = 'backend/login';
						}
					}
					UpdateTimer();
					window.setTimeout('Tick()', 1000);
				}
				function UpdateTimer() {
					Timer.innerHTML = TotalSeconds;
				}
			</script>
			
		";
		die();
		
	}
	
	public function log_activities($data)
	{
		$employee_id = $this->get_employee_id();
		$employee_name = $this->get_employee_name();
		
		$prm['table'] = 'activities_log'; 
		$prm['data'] = array(
			'module' => $data['module'],
			'lang_subject' => $data['lang_subject'],
			'item' => $data['lang_item'],
			'lang_action' => $data['lang_action'],
			'detail_link' => isset($data['detail_link']) ? $data['detail_link'] : NULL,
			'user_id' => $this->get_user_id(),
			'user_name' => $this->get_user_name(),
			'employee_id' => ! empty($employee_id) ? $employee_id : NULL,
			'employee_name' => ! empty($employee_name) ? $employee_name : NULL,
			'log_datetime' => $this->get_date_time(),
			'note' => isset($data['note']) ? $data['note'] : NULL
		);
		
		$this->def_model->insert($prm);
	}
	
	protected function get_log_info()
	{        	
		$info = array(
			'ip_address'   => gethostbyname($_SERVER['REMOTE_ADDR']),
			'user_agent'   => $_SERVER['HTTP_USER_AGENT'],
			'log_datetime' => date("Y-m-d H:i:s"),
			'sys_user_id'  => $this->get_user_id(),
			'sys_user_name'  => $this->get_user_name()
		);
		
		return $info;
	}
	
	public function log_activity($action, $id = '',$mark = '')
	{
		$data = $this->get_log_info();
		unset($data['sys_user_name']);
		$data['mark'] = strtolower($mark);
		$data['action'] = strtolower($action);
		$data['sys_user_id'] = (!empty($id) ? $id : $this->get_user_id());
		return $this->def_model->log_activity($data);
	}
	
	public function login_logout($action)
	{
		$data = $this->get_log_info();
		unset($data['log_datetime']);
		$data[$action.'_datetime'] = date("Y-m-d H:i:s");
		$data['action'] = $action;
		
		if($action == 'login')
		{
			$login_logout_id = $this->def_model->login_logout($data);
			$this->session->set_userdata($this->session_key('login_logout_id'),$login_logout_id);
		}
		else
		{
			$id = $this->session->userdata(APPLICATION.'_login_logout_id');
			if(! empty($id))
			{
				$this->def_model->login_logout($data,$id);
			}
		}
		
	}
	
	protected function get_module_function($type = '', $class = '')
	{
		if($this->uri->segment(2))
		{
			if($this->uri->segment(2) == 'frontend')
			{
				$mode = 'app';
			}
			else
			{
				$mode = 'sys';
			}
		}
		else
		{
			$mode = 'sys';
		}
		
		$list_function = array();
		if($this->session->userdata($this->session_key('sys_group_child_id')) != 0)
		{
			if(! empty($class))
			{
				$prm_list = array('table' => $mode.'_menu', 'where' => array('name' => $class.(!empty($type) ? '_'.$type : '')), 'select' => 'id');
			}
			else
			{
				$prm_list = array('table' => $mode.'_menu', 'where' => array('name' => $this->class.(!empty($type) ? '_'.$type : '')), 'select' => 'id');
			}
			
			$res = $this->def_model->get_one($prm_list)->row_array();
			
			$param['where'] = array('b.'.$mode.'_menu_id'  => (isset($res['id']) ? $res['id'] : 0));
			
			$res_function = $this->def_model->get_menu_user($mode, $param)->row_array();
			
			if(! empty($res_function['active_function']))
			{
				$function_array = json_decode($res_function['active_function']);
				
				foreach($function_array as $val)
				{
					$list_function[strtolower($val)] = TRUE;
				}
			}
		}
		
		return $list_function;
	}
	
	public function money_format($number,$splitter = '.')
	{
		return number_format($number, 0, '', $splitter);
	}
	
	public function date_format_dmy($date,$splitter = '/')
	{
		if(! isset($date) || $date == '') $date = '0000-00-00';
		$d = explode("-",$date);
		
		return $d[2].$splitter.$d[1].$splitter.$d[0];
	}
	
	public function date_format_Ymd($date,$splitter)
	{
		if(! isset($date) || $date == '') $date = '0000-00-00';
		$d = explode($splitter,$date);
		
		return $d[2].$splitter.$d[1].$splitter.$d[0];
	}
	
	public function date_format($date,$splitter = '/')
	{
		if(! isset($date) || $date == '') $date = '0000-00-00';
		$d = explode("-",$date);
		
		return $d[2].$splitter.$d[1].$splitter.$d[0];
	}
	
	function convert_date($val,$format)
	{
		$date = date_create($val);
		return date_format($date, $format);
	}
	
	function get_weeks_by_year_month($year, $month) 
	{
		$w_arr = array();
		
		//$last_day = ! in_array(intval($month), $this->month_triwulan) ? 25 : cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$last_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		
		$d1 = $year."-".$month."-1";
		$d2 = $year."-".$month."-".$last_day;
		
		$start_date = date('Y-m-d', strtotime($d1));
		$end_date = date('Y-m-d', strtotime($d2));
		
		for($date = $start_date; $date <= $end_date; $date = date('Y-m-d', strtotime($date. ' + 1 days')))
		{
			$w = date("W", strtotime($date));
			$w_arr[$w] = $w;
		}
		
		return $w_arr;
	} 
	
	function get_diff_date($date, $date2 = '')
	{
		if(! empty($date2))
		{
			$now = new DateTime($date2);
		}
		else
		{
			$now = new DateTime();
		}
		
		$_date = new DateTime($date);
		
		if($_date <= $now)
		{
			$year = $now->diff($_date)->format("%Y Tahun, %m Bulan, %d Hari" );
			$val = str_replace('00 Tahun,', '', $year);
			$val = str_replace(', 0 Bulan,', '', $val);
			$val = str_replace(', 0 Hari', '', $val);
			return $val;
			
			return $val;
		}
		else
		{
			return '';
		}
	}
	
	public function retrive_data_ref($data = array(),$field1,$field2 = array(),$curr_data,$empty = 'N/A')
	{
		$value = '';
		foreach($data as $key => $val)
		{
			if($val[$field1] === $curr_data)
			{
				$str = '';
				$i = 0;
				foreach($field2 as $v)
				{
					if($i != 0)
					{
						$str .= " - ".$val[$v];
					}
					else
					{
						$str .= $val[$v];
					}
					$i++;
				}
				$value = $str;
				break;
			}
		}
		unset($data);
		return ! empty($value) ? $value : $empty;
	}
	
	public function create_dir($path)
	{
		if(! is_dir($path)) 
		{
		   mkdir($path,DIR_WRITE_MODE,TRUE);
		} 
	}
	
	function get_current_url()
	{
		return site_url().$this->uri->uri_string();
	}
	
	function get_file_ext($file)
	{
		return strtolower(pathinfo($file, PATHINFO_EXTENSION));
	}
	
	function get_file_name($file, $file_ext)
	{
		return basename($file,".".$file_ext);
	}
	
	public function send_email($prm)
	{
		$this->load->library('email');
		
		if($_SERVER['HTTP_HOST'] == 'apps.pp-epc.com')
		{
			$config['protocol']  = 'smtp';
			$config['smtp_host']  = $this->config->item('smtp_host');
			$config['smtp_user']  = $this->config->item('smtp_user');
			$config['smtp_pass']  = $this->config->item('smtp_pass');
			$config['smtp_port']  = $this->config->item('smtp_port');
			$config['smtp_timeout']  = $this->config->item('smtp_timeout');
		}
		else
		{
			$config['protocol']  = isset($this->site_config['email_protocol']) ? $this->site_config['email_protocol'] : 'mail';
		}
		
		$config['charset']   = 'iso-8859-1';
		$config['wordwrap']  = TRUE;
		$config['mailtype']	 = isset($prm['mailtype']) ? $prm['mailtype'] : 'html';
		$config['validate']	 = TRUE;
		$config['priority']	 = isset($prm['priority']) ? $prm['priority'] : 3;
		$config['crlf']	 	 = "\r\n";
		
		$this->email->initialize($config);
		
		$default = array(
			'subject'   => trim($prm['subject']),
			'content'   => trim($prm['content']), 
			'from_name' => $prm['from_name'],
			'from_email' => $prm['from_email']
		);
		
		$i = 0;		   
		foreach($prm['to'] as $name => $addrs)
		{
			$this->email->clear();
			$this->email->from($default['from_email'], $default['from_name']);
			$this->email->subject($default['subject']);
			$this->email->to($addrs);
			
			if(isset($prm['attachment']))
			{
				foreach($prm['attachment'] as $attach)
				{
					$this->email->attach($attach);
				}
			}
			
			$default['to_name'] = $name;
			
			if(isset($prm['is_test']) && $prm['is_test'])
			{
				$msg = $this->_build_email($default);
			}
			else
			{
				$msg = $prm['content'];
			}
			
			$this->email->message($msg);
		
			if($this->email->send())
			{
				$i++;
			}
			
			sleep(3);
		}
		
		if($i > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function _test_email_ci()
	{
		$this->load->library('email');
		$config['protocol']  = 'mail';
		$config['charset']   = 'iso-8859-1';
		$config['wordwrap']  = TRUE;
		$config['mailtype']	 = 'html';
		$config['validate']	 = TRUE;
		$config['priority']	 = isset($prm['priority']) ? $prm['priority'] : 3;
		$config['crlf']	 	 = "\r\n";
		//$config['newline'] = "\r\n";
		
		$this->email->initialize($config);
		
		$default = array(
			'subject'   => 'TEST EMAIL',
			'content'   => 'email Tes', 
			'from_name' => 'WEBMASTER CI',
			'from_email' => 'webmaster@ret.co.id',
			'to_name' => 'RYAN'
		);
		
		$this->email->clear();
		$this->email->from($default['from_email'], $default['from_name']);
		$this->email->subject($default['subject']);
		$this->email->to('cekicuki@gmail.com');
			
		$msg = $this->_build_email($default);
		
		$this->email->message($msg);
		
		if($this->email->send())
		{
			echo "SENT";
		}
		else
		{
			echo "NOT SENT";
		}
	}
	
	private function _build_email($prm)
	{
		$content = isset($prm['content']) ? $prm['content'] : '';
		$msg = "
		<html>
			<head>
			  <title>{$prm['subject']}</title>
			</head>
			<body style='font-family: Arial; font-size: 12px;color:#15428B;style=margin:10px 0px 10px 0px'>
				<p>Dear {$prm['to_name']},</p> 
				<p>
					{$content}
				</p>
				<br/>
				<p>Regards,</p> 
				<p>{$prm['from_name']}</p> 
			</body>
		</html>
		";

		return $msg;
	}
	
	function is_email_valid($email)
	{
		$pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
		if(preg_match($pattern, $email) === 1) 
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function get_years($decrement = 4, $start_year = "")
	{
		$years = array();
		$current_year = ! empty($start_year) ? $start_year : date("Y");
		$last_year = $current_year - $decrement;
		for($i = $current_year; $i >= $last_year; $i--)
		{
			$years[] = $i; 
		}
		
		return $years;
	}
	
	function replace_space($str)
	{
		return str_replace(" ","_",$str);
	}
	
	function replace_underscore($str)
	{
		return str_replace("_"," ",$str);
	}
	
	function replace_str_numeric($str, $pattern, $replace = '')
	{
		return preg_replace("/[{$pattern}]+/", $replace, $str);
	}
	
	function convert_str_ucwords($str)
	{
		return ucwords(strtolower(trim($str)));
	}
	
	function convert_str_upper($str)
	{
		return strtoupper(strtolower(trim($str)));
	}

	function columnLetter($c, $decrement = 1)
	{
		$letter="";
		$c = intval($this->columnNumber($c,$decrement));
		if ($c<=0) return '';

		while($c != 0){
		   $p = ($c - 1) % 26;
		   $c = intval(($c - $p) / 26);
		   $letter = chr(65 + $p) . $letter;
		}

		return $letter;

	}

	function columnNumber($col,$decrement)
	{
		$col = str_pad($col,2,'0',STR_PAD_LEFT);
		$i = ($col{0} == '0') ? 0 : (ord($col{0}) - 64) * 26;
		$i += ord($col{1}) - 64;

		return $i-$decrement;
	}
	
	function columnLetterIncrement($col, $increment)
	{
		for($i = 0; $i < $increment; $i++)
		{
			++$col;
		}
		return $col;
	}
	
	function recursiveRemoveDirectory($path)
	{
		array_map('unlink', glob($path."/*"));
		rmdir($path);
	}
	
	function unlink_file($path_file)
	{
		unlink($path_file);
	}
	
	function sentence_case_ucfirst($string, $adj = '') 
	{
		$sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
		$new_string = '';
		foreach ($sentences as $key => $sentence) {
			$new_string .= ($key & 1) == 0?
				ucfirst(strtolower(trim($sentence))) :
				$sentence.' '.$adj;
		}
		return trim($new_string);
	} 
	
	function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 0) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
   }
   
}

class Backend_Controller extends MY_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->init();
	}
	
	public function sess_check()
	{
		return ($this->login_manager->logged_in ? TRUE : FALSE);
	}
	
	function init()
	{	
		if($this->_module != 'api')
		{
			if(! $this->login_manager->logged_in)
			{
				if(! $this->site_config['is_backend_online'])
				{
					redirect('offline');
				}
				
				if($this->input->is_ajax_request())
				{
					echo $this->output->status_callback('json_unsuccess',$this->lang->line('global_session_expired'), TRUE);
					exit();
				}
				else
				{
					if($this->class == 'filemanager')
					{
						$this->sess_check_redirect();
						exit();
					}
					
					redirect('backend/login');
				}
			}
			
			if($this->get_lang() == 'indonesia') 
			{
				$this->site_config['display_date'] = $this->date_indonesia->format_date();
			}
			else
			{
				$this->site_config['display_date'] = date("l, j F Y");
			}
			
			$this->words['PLEASE_WAIT'] = $this->lang->line('global_please_wait');  
			$this->words['LANG_ARRAY'] = $this->header['languages']; 
			$this->words['SESSION_EXPIRED'] = $this->lang->line('global_session_expired'); 
			$this->words['ERROR'] = $this->lang->line('global_error'); 
			$this->words['SUCCESS'] = $this->lang->line('global_success'); 
			$this->words['KEYWORD_CANNOT_EMPTY'] = $this->lang->line('global_keyword_must_not_empty'); 
			
			$this->header['js'][] = '<script type="text/javascript">
				var BASE_URL = "'.base_url().'",
				BASE_ASSET_URL = "'.base_asset_url().'",
				SITE_URL = "'.site_url().'",
				APP_BACKEND_URL = "'.app_backend_url().'",
				APP_NAME = "'.$this->site_config['app_name'].'",
				APP_ASSET_URL = "'.app_asset_url().'",
				APP_ASSET_BACKEND_URL = "'.app_asset_backend_url().'",
				DEFAULT_THEME = "'.$this->site_config['app_def_theme'].'",
				SESS_ID = "'.$this->session->userdata($this->session_key('sess_id')).'",
				IS_SOCK = "'.$this->site_config['real_time_notif'].'",
				GROUP_PARENT_ID = "'.$this->session->userdata($this->session_key('group_parent_id')).'";
				'.$this->js_array($this->words,'LANGUAGE').'
			</script>';

			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap/css/bootstrap.min.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap/css/bootstrap-responsive.min.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/font-awesome/css/font-awesome.min.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/style-metro.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/style.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/style-responsive.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/themes/default.css').'" />';
			if($this->site_config['app_def_theme'] != 'default')
			{
				$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/themes/'.$this->site_config['app_def_theme'].'.css').'" />';
			}
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-modal/css/bootstrap-modal.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/uniform/css/uniform.default.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/gritter/css/jquery.gritter.css').'" />';
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/jquery-alerts/jquery.alerts.css').'" />';
			$this->header['css'][] = '<link rel="shortcut icon" href="'.app_asset_backend_url('img/favicon.ico').'">';
			
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-1.10.1.min.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-migrate-1.2.1.min.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js').'"></script>';

			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap/js/bootstrap.min.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js').'"></script>';
			$this->header['js'][] = '<!--[if lt IE 9]><script src="'.app_asset_backend_url('plugins/excanvas.min.js').'"></script><script src="'.app_asset_backend_url('plugins/respond.min.js').'"></script><![endif]-->';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-slimscroll/jquery.slimscroll.min.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.blockui.min.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-alerts/jquery.alerts.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/uniform/jquery.uniform.min.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/gritter/js/jquery.gritter.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-validation/jquery.validate.min.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-validation/additional-methods.min.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_modules_backend_url('views/global/app.js?=n'.time()).'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_modules_backend_url('views/global/header.js?n='.time()).'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_modules_backend_url('views/global/base64.js').'"></script>';
			
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>';
			
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/nprogress/nprogress.css').'" />';
			$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/nprogress/nprogress.js?=n'.time()).'"></script>';
			
			if($this->site_config['real_time_notif'])
			{
				$this->header['js'][] = '<script type="text/javascript" src="'.site_url('node-server/sockjs-0.3.min.js').'"></script>';
			}
			
			$this->load->library('navigation_manager');
			$this->load->model('berkas_model','berkas_m');
			$this->load->model('user_model','user_m');
			
			$hidden_menu = array();
			$res_menu = $this->def_model->get_menu_user('sys')->result_array();
			
			if($this->class == 'backend' || $this->class == 'filemanager')
			{
				if(! $this->site_config['is_backend_online'])
				{
					redirect('offline');
				}
				
				$this->header['menu'] = $this->navigation_manager->get_menu_backend(NULL,$hidden_menu);
				$this->header['menu2'] = $this->navigation_manager->get_menu_backend2(NULL,$hidden_menu);
				
				$function = $this->get_module_function();
				$this->module_functions = $function;
				$this->site_config['module_function'] = $function;
				
				$this->header['js'][] = '<script type="text/javascript">var MODULE_FUNCTION = '.json_encode($function).';</script>';
				
				delete_cookie('last_url_'.APPLICATION);
			}
			else
			{
				$this->module = $this->retrive_data_ref($res_menu,'name',array('description','location'),$this->class,'');
				
				$this->header['menu'] = $this->navigation_manager->get_menu_backend($this->class,$hidden_menu);
				$this->header['menu2'] = $this->navigation_manager->get_menu_backend2($this->class,$hidden_menu);
				
				$function = $this->get_module_function();
				$this->module_functions = $function;
				if(! empty($hidden_menu))
				{
					unset($function['add']);
					unset($function['del']);
					unset($function['edit']);
				}
				
				$this->site_config['module_function'] = $function;
				$this->header['js'][] = '<script type="text/javascript">var MODULE_FUNCTION = '.json_encode($function).';</script>';
				$this->header['js'][] = '<script type="text/javascript">var THIS_URL = "'.current_url().'";</script>';
				
				$cur_lang = $this->get_lang();
				if(file_exists(ABSOLUTE_PATH.'language/'.$cur_lang.'/'.$this->class.'_lang.php')) 
				{
					$this->lang->load($this->class, $cur_lang);
				}	
				
				if($this->input->server('REQUEST_METHOD') === 'GET' && ! $this->input->is_ajax_request())
				{
					if(substr($this->router->fetch_method(),0,6) != 'export')
					{
						$this->input->set_cookie('last_url_'.APPLICATION, base64_encode(current_url()), time()+86500);
					}
				}
			}
			
			// count new task
			if($this->get_group_name2() == 'PROYEK')
			{
				$this->header['res_new_task'] = $this->berkas_m->get_berkas(
					array(
						'where' => array('last_verify_status' => $this->ref_verify_status[3]),
						'order_sort' => 'last_verify_datetime',
						'order_dir' => 'DESC'
					)
				)->result_array();
			}
			else
			{
				$this->header['res_new_task'] = $this->berkas_m->get_berkas(
					array(
						'where' => array('last_verify_status' => $this->ref_verify_status[1], 'last_verify_level' => $this->get_group_name2()),
						'order_sort' => 'last_verify_datetime',
						'order_dir' => 'DESC'
					)
				)->result_array();
			}
			
			$this->header['MCTR'] = $this;
			$this->lang->load('berkas', $this->get_lang());
		}
	}
	
	function check_module()
	{
		if(empty($this->module))
		{
			redirect('backend/my404');
		}
	}
	
	function check_module_functions($func)
	{
		if(!isset($this->module_functions[$func]))
		{
			redirect('backend/my404');
		}
		else
		{
			if($this->module_functions[$func] == 0) 
			{
				redirect('backend/my404');
			}
		}
	}
	
	public function _get_color($value)
	{
		$color = "";
		if ($value > 90) {
			$color = 'blue';
		} else if ($value > 75 && $value <= 90) {
			$color = 'green';
		} else if ($value > 55 && $value <= 75) {
			$color = 'yellow';
		} else if ($value >= 0 && $value <= 55) {
			$color = 'red';
		}
		
		return $color;
	}
	
	public function _get_color_code($value)
	{
		$color = "";
		if ($value > 90) {
			$color = '4D90FE';
		} else if ($value > 75 && $value <= 90) {
			$color = '35AA47';
		} else if ($value > 55 && $value <= 75) {
			$color = 'FFB848';
		} else if ($value >= 0 && $value <= 55) {
			$color = 'D84A38';
		}
		
		return $color;
	}
	
	public function get_days_left($date)
	{
		$date_start = strtotime(date('Y-m-d'));
		$date_end = strtotime($date);
		$time_left = ($date_end-$date_start);
		return round((($time_left/24)/60)/60) + 1; 
	}
	
	function get_max_level_berkas2($payment_method)
	{
		if($payment_method == $this->ref_payment_method[1])
		{
			$arr = $this->ref_verify_level_ncl;
		}
		else
		{
			$arr = $this->ref_verify_level_reg;
		}
		
		return max(array_keys($arr));
	}
	
	function get_verify_level2($payment_method)
	{
		if($payment_method == $this->ref_payment_method[1])
		{
			$arr = $this->ref_verify_level_ncl;
		}
		else
		{
			$arr = $this->ref_verify_level_reg;
		}

		return $arr;
	}
	
	function get_verify_level($payment_method = "REGULAR")
	{
		$res = $this->def_model->get_list(
			array(
				'table' => 'ref_verify_levels a', 
				'select' => 'b.name payment_method, c.name verify_level, a.*',
				'where' => array('b.name' => $payment_method),
				'join' => array(
					array(
						'table' => "ref_payment_method b",
						'on'    => "a.payment_method_id = b.id",
						'type'  => 'inner'
					),
					array(
						'table' => "sys_group_child c",
						'on'    => "a.group_child_id = c.id",
						'type'  => 'inner'
					)
				),
				'order_sort' => 'level_index',
				'order_dir' => 'ASC'
			)
		)->result_array();
		
		$arr = array();
		foreach($res as $key => $val)
		{
			$arr[$val['level_index']] = array(
				'verify_level' => $this->replace_underscore($val['verify_level']),
				'group_child_id' => $val['group_child_id'],
				'payment_method' => $val['payment_method'],
				'payment_method_id' => $val['payment_method_id']
			);
		}
		
		if(empty($arr))
		{
			die("Data Level Verifikasi tdk boleh kosong, utk tindak lanjut silahkan hubungi administrator.");
		}
		
		return $arr;
	}
	

}

class Frontend_Controller extends MY_Controller 
{    	
    protected $side_bar = array();

	
	function __construct()
	{
		parent::__construct();
		$this->init();
	}
	
	public function sess_check()
	{
		return ($this->login_manager->logged_in2 ? TRUE : FALSE);
	}
	
	function init()
	{
		if(!$this->site_config['is_frontend_online'])
        {
				die('front');
			redirect('offline');
        }
	
	}
	
	public function get_applicant_name()
	{
		return $this->session->userdata(APPLICATION.'_applicant_name');
	}
	
	public function get_applicant_id()
	{
		return $this->session->userdata(APPLICATION.'_applicant_id');
	}
	
	public function get_applicant_email()
	{
		return $this->session->userdata(APPLICATION.'_applicant_email');
	}
	
	function my404()
	{
		redirect('frontend/my404');
	}
}

class API_Controller extends MY_Controller 
{    	
	function __construct()
	{
		parent::__construct();
		$this->init();
	}
	
	function init()
	{
		if(! $this->site_config['is_backend_online'])
		{
			$http_code = 400;
			$output = json_encode(array('status' => FALSE, 'error' => 'System offline.'));
			
			header('HTTP/1.1: ' . $http_code);
			header('Status: ' . $http_code);
			header('Content-Length: ' . strlen($output));
			header('Content-Type: application/json');

			exit($output);
		}
		
		if(isset($_REQUEST['usr']) && isset($_REQUEST['pwd']))
		{
			$process = $this->login_manager->login_process($_REQUEST,FALSE);
			
			if($process)
			{
				$this->log_activity('login');
			}
			else
			{
				$http_code = 404;
				$output = json_encode(array('status' => FALSE, 'error' => 'User not Found !!!'));
				
				header('HTTP/1.1: ' . $http_code);
				header('Status: ' . $http_code);
				header('Content-Length: ' . strlen($output));
				header('Content-Type: application/json');

				exit($output);
			}
		}
	}
}