<?php
// application/core/MY_Exceptions.php
class MY_Exceptions extends CI_Exceptions {

    public function show_404($page = '')
    {
		$this->CI = &get_instance();
		if(! empty($page))
		{
			list($parent) = explode("/", $page);
			$this->_module = $this->CI->router->fetch_module();
			
			if($parent == 'backend')
			{
				redirect('backend/my404');
			}
			else
			{
				if($this->_module == 'api')
				{
					redirect('api/my404');
				}
				else
				{
					redirect('backend/my404');
				}
			}
		}
    }
	
	/*  public function show_error($heading, $message, $template = 'error_general', $status_code = 500) {
        try {
            $str = parent::show_error($heading, $message, $template = 'error_general', $status_code = 500);
            throw new Exception($str);
        } catch (Exception $e) {
            $msg = $e->getMessage();
            $trace = "<h1>Call Trace</h1><pre>". $e->getTraceAsString(). "<pre>";
            //append our stack trace to the error message
            $err = str_replace('</div>', $trace.'</div>', $msg);
            echo $err;
        }
    } */
}