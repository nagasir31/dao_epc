<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MY_Model
 *
 * Base CRUD pattern for CodeIgniter
 *
 * @package     CI-Extjs CRUD
 * @copyright  	Copyright (c) 2012, Ryanceki
 * @version    	1.0.0 (March 2012)
 * @author      Ryanceki <ry4nceki@yahoo.com>
 * @link		http://www.cekicuki.com
 */
class MY_Model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('inflector');
	}

	function MY_debug_query()
	{
		echo $this->db->last_query();
	}
	
	function MY_check_table_exists($table_name)
	{
		if($this->db->table_exists($table_name))
		{
			return TRUE;
		}
		else
		{
			show_error("Table \"{$table_name}\" doesn't exist !!!");
		}
	}
	
	function MY_get_field_types($table_name, $field_name = '')
    {
    	$results = $this->db->field_data($table_name, $field_name);
		
    	return $results;
    }
	
	function MY_get_list_field($table_name)
	{
		$results = $this->db->list_fields($table_name);
    	
    	return $results;
	}
	
	function MY_get_field_types_by($table_name, $field_name)
    {
    	$results = $this->db->query('DESCRIBE '.$table_name.' '.$field_name);
    	
    	return $results->result_array();
    }
	
	function MY_get_primary_key($table_name)
	{
		if(isset($table_name))
		{
			$fields = $this->MY_get_field_types($table_name);
				
			foreach($fields as $field)
			{
				if($field->primary_key == 1)
				{
					return $field->name;
				}	
			}
		}
		
		return FALSE;
	}
	
	function MY_is_primary_key($table_name, $field)
	{
		if(isset($table_name))
		{
			$fields = $this->MY_get_field_types($table_name);
				
			foreach($fields as $val)
			{
				if($val->primary_key == 1)
				{
					if($val->name == strtolower($field))
					{
						return TRUE;
					}
					else
					{
						return FALSE;
					}
				}	
			}
		}
		
		return FALSE;
	}
	
	function MY_build_foreign_key($table_name)
	{
		return $table_name.'_id';
	}
	
	function MY_is_field_exists($field, $table_name)
    {
		return $this->db->field_exists($field, $table_name);
    }
	
	function MY_build_alias($table_name)
	{
		return substr($table_name,2,strlen($table_name));
	}
	
	function MY_select($field,$escape = FALSE)
    {
    	$this->db->select($field, $escape);
    }
	
	function MY_from($table_name)
    {
    	$this->db->from($table_name);
    }
	
	function MY_join($table_name, $condition, $type = 'left')
    {
    	$this->db->join($table_name, $condition, $type);
    }
	
	function MY_order_by($order_by, $direction)
    {
    	$this->db->order_by($order_by , $direction);
    }
    
    function MY_where($key, $value = NULL, $escape = TRUE, $operator = '=')
    {
    	$this->db->where("upper(".$key.") {$operator}", strtoupper($value), $escape);
    }
	
	function MY_where_array($where)
    {
    	$this->db->where(array_map('strtoupper', $where));
    }
	
	function MY_where_in($key, $value)
    {
    	$this->db->where_in($key, $value);
    }
    
    function MY_or_where($key, $value = NULL, $escape = TRUE)
    {
    	$this->db->or_where("upper(".$key.")", strtoupper($value), $escape);
    }    
    
    function MY_like($field, $match = '', $side = 'both')
    {
    	$this->db->like("upper(".$field.")", strtoupper($match), $side);
    }
    
    function MY_or_like($field, $match = '', $side = 'both')
    {
    	$this->db->or_like("upper(".$field.")", strtoupper($match), $side);
    }    
    
    function MY_limit($value, $offset = '')
    {
    	$this->db->limit($value, $offset);
    }
	
	function MY_get()
	{
		return $this->db->get();
	}
	
	function MY_count_all_results()
	{
		return $this->db->count_all_results();
	}
    
    function MY_get_total_results($table_name)
    {
    	$this->db->from($table_name);
		return $this->db->count_all_results();
    }
	
	function MY_update($table_name, $pk, $id, $data)
	{
		if(isset($pk) && isset($id))
		{
			$this->db->where_in($pk, $id, FALSE);
		}
		
		if(isset($table_name))
		{
			if(isset($data))
			{
				return $this->db->update($table_name, $data);
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	function MY_insert($table_name, $data)
	{
		if(isset($table_name))
		{
			if(isset($data))
			{
				if($this->db->insert($table_name, $data))
				{
					return $this->db->insert_id();
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	function MY_delete_update($table_name, $pk, $id)
	{
		$data  = array('is_deleted' => 1);
		return $this->MY_update($table_name, $pk, $id, $data);
	}
	
	function MY_delete($table_name, $pk, $id, $del_update)
	{
		if($del_update)
		{
			$data  = array('is_deleted' => 1);
			return $this->MY_update($table_name, $pk, $id, $data);
		}
		else
		{
			if(isset($pk) && isset($id))
			{
				$this->db->where_in($pk, $id);
			}
			
			if(isset($table_name))
			{
				return $this->db->delete($table_name);
			}
			else
			{
				return FALSE;
			}
		}
	}
	
	function MY_delete_has_many($table_name, $pk, $id, $attribute, $del_update)
	{
		foreach($id as $val)
		{
			$ids[] = $this->MY_escape($val);
		}
		
		
		if($del_update) // if mode delete update
		{
			$query = $this->db->query("
				UPDATE ".$table_name.", ".$attribute['related_table']." 
				SET ".$table_name.".IS_DELETED = 1, ".$attribute['related_table'].".IS_DELETED = 1   
				WHERE ".$table_name.".".$pk." = ".$attribute['related_table'].".".$attribute['related_table_fk']."
				AND ".$table_name.".".$pk." IN (".implode(",", $ids).")
			");
		}
		else
		{
			/*$query = $this->db->query("
				DELETE ".$table_name.", ".$attribute['related_table']." 
				FROM ".$table_name." 
				JOIN ".$attribute['related_table']." 
				ON ".$table_name.".".$pk." = ".$attribute['related_table'].".".$attribute['related_table_fk']." 
				WHERE ".$attribute['related_table'].".".$attribute['related_table_fk']." IN (".implode(",", $id).")
				AND ".$table_name.".".$pk." IN (".implode(",", $id).")
			");*/
			
			$query = $this->db->query("
				DELETE ".$attribute['related_table']." ,".$table_name."
				FROM ".$attribute['related_table']." 
				LEFT JOIN ".$table_name." 
				ON ".$table_name.".".$pk." = ".$attribute['related_table'].".".$attribute['related_table_fk']." 
				WHERE ".$attribute['related_table'].".".$attribute['related_table_fk']." IN (".implode(",", $ids).")
			");
		}
		
		return $query;
	}
	
	function MY_update_has_one($table_name, $pk, $id, $attribute, $data)
	{
		//print_r($data);
		$query = $this->db->query("
			UPDATE ".$table_name.", ".$attribute['related_to']." 
			SET ".implode(",", $data)."
			WHERE ".$table_name.".".$attribute['foreign_key']." = ".$attribute['related_to'].".".$attribute['related_key']."
			AND ".$table_name.".".$pk." IN (".$id.")
		");
		
		return $query;
	}
	
	function MY_escape($str)
	{
		if (is_string($str))
		{
			$str = "'".$str."'";
		}
		elseif (is_bool($str))
		{
			$str = ($str === FALSE) ? 0 : 1;
		}
		elseif (is_null($str))
		{
			$str = 'NULL';
		}

		return $str;
	}
}