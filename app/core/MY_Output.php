<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Output extends CI_Output
{
	function status_callback($type, $msg = NULL, $expired = FALSE)
	{
		if(substr($type,0,4) == 'json')
		{
			$return = array();
				
			if($type == 'json_success')
			{
				$return['success'] = TRUE;
			}
			
			if($type == 'json_unsuccess')
			{
				$return['success'] = FALSE;
			}
			
			
			if($msg != NULL)
			{
				$return['message'] = $msg;
			}
			
			if($expired)
			{
				$return['expired'] = TRUE;
			}
			
			return json_encode($return);
		}
		else
		{
			if($type == 'success')
			{
				return 1;
			}
			
			if($type == 'unsuccess')
			{
				return 0;
			}
		}
	}

}