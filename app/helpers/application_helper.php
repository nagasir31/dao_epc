<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('char_limiter')) 
{
    function char_limiter($str, $limit)
	{
		if(strlen($str) > $limit)
		{
			$str = substr($str, 0, $limit).'...';
		}
		
		return $str;
	}
}

if (! function_exists('calculate_percentage')) 
{
    function calculate_percentage($val1, $val2, $round = 0)
    {
		$res = "-";
		if(is_numeric($val1) && is_numeric($val2) && ! empty($val2))
		{
			$res = round(($val1/$val2) * 100);
		}
		
		return $res;
	}
}

if (! function_exists('xml_to_array'))
{
	function xml_to_array($xmlObject, $out = array ())
	{
		foreach((array)$xmlObject as $index => $node)
		{
			$out[$index] = (is_object($node)) ? xml_to_array($node) : $node;
		}

		return $out;
	}
}