<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/** 
 * Converts a date string from one format to another (e.g. d/m/Y => Y-m-d, d.m.Y => Y/d/m, ...) 
 * 
 * @param string $date_format1 
 * @param string $date_format2 
 * @param string $date_str 
 * @return string 
 */ 
function convert_date( $date_format1, $date_format2, $date_str) 
{ 
	$base_struc     = split('[-]', $date_format1); 
	$date_str_parts = split('[/]', $date_str ); 
	
	print_r( $base_struc ); echo "<br>"; 
	print_r( $date_str_parts ); echo "<br>"; 
	
	/* $date_elements = array(); 
	
	$p_keys = array_keys( $base_struc ); 
	foreach ( $p_keys as $p_key ) 
	{ 
		if ( !empty( $date_str_parts[$p_key] )) 
		{ 
			$date_elements[$base_struc[$p_key]] = $date_str_parts[$p_key]; 
		} 
		else 
			return false; 
	} 
	
	$dummy_ts = mktime( 0,0,0, $date_elements['m'],$date_elements['d'],$date_elements['Y']); 
	
	return date( $date_format2, $dummy_ts );  */
} 

if ( ! function_exists('date_create'))
{
    function date_create($date)
    {
        return strtotime($date);
    }
}

if ( ! function_exists('date_format'))
{
    function date_format($date,$format)
    {
        return date($format, $date);
    }
}