<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define("SECOND", 1);
define("MINUTE", 60 * SECOND);
define("HOUR", 60 * MINUTE);
define("DAY", 24 * HOUR);
define("MONTH", 30 * DAY);
function relative_time($time)
{   
    $delta = time() - $time;

    if ($delta < 1 * MINUTE)
    {
        return $delta == 1 ? "1 detik lalu" : $delta . " detik lalu";
    }
    if ($delta < 2 * MINUTE)
    {
      return "1 menit lalu";
    }
    if ($delta < 45 * MINUTE)
    {
        return floor($delta / MINUTE) . " menit lalu";
    }
    if ($delta < 90 * MINUTE)
    {
      return "1 jam lalu";
    }
    if ($delta < 24 * HOUR)
    {
      return floor($delta / HOUR) . " jam lalu";
    }
    if ($delta < 48 * HOUR)
    {
      return "kemarin";
    }
    if ($delta < 30 * DAY)
    {
        return floor($delta / DAY) . " hari lalu";
    }
    if ($delta < 12 * MONTH)
    {
      $months = floor($delta / DAY / 30);
      return $months <= 1 ? "1 bulan lalu" : $months . " bulan lalu";
    }
    else
    {
        $years = floor($delta / DAY / 365);
        return $years <= 1 ? "1 tahun lalu" : $years . " tahun lalu";
    }
} 