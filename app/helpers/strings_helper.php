<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function char_limiter($str, $limit)
{
	if(strlen($str) > $limit)
	{
		$str = substr($str, 0, $limit).'...';
	}
	
	return $str;
}