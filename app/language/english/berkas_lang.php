<?php
// Global
$lang['berkas_jobs_type'] = "Jenis Pekerjaan";
$lang['berkas_job_type'] = "Jenis Pekerjaan";
$lang['berkas_vendor_type'] = "Jenis Vendor";
$lang['berkas_created_division'] = "Divisi Pembuat";
$lang['berkas_created_branch'] = "Cabang Pembuat";
$lang['berkas_created_division_branch'] = "Divisi & Cabang Pembuat";
$lang['berkas_prequal'] = "Prakual";
$lang['berkas_prequal_last'] = "Prakual Terakhir";
$lang['berkas_approved'] = "Approved";
$lang['berkas_non_approved'] = "Non Approved";
$lang['berkas_administration'] = "Administrasi";
$lang['berkas_technic'] = "Teknik";
$lang['berkas_project_status'] = "Status Proyek";
$lang['berkas_branch'] = "Cabang";
$lang['berkas_division'] = "Divisi";
$lang['berkas_value'] = "Nilai";
$lang['berkas_payment_method'] = "Cara Pembayaran";
$lang['berkas_payment'] = "Pembayaran";
$lang['berkas_personnel_number'] = "Jumlah Personil";
$lang['berkas_num_of_jobs_type'] = "Jumlah Jenis Pekerjaan";
$lang['berkas_change_to_not_approved'] = "Ubah ke non approved";
$lang['berkas_blacklist'] = "Blacklist";
$lang['berkas_whitelist'] = "Whitelist";
$lang['berkas_project_date'] = "Waktu Pelaksanaan";
$lang['berkas_days_ahead'] = "Hari kedepan";
$lang['berkas_already_in_whitelist'] = "Item sudah ada dalam whitelist";
$lang['berkas_already_in_blacklist'] = "Item sudah ada dalam blacklist";
$lang['berkas_total_tenaga_kerja'] = "Total Tenaga Kerja";
$lang['berkas_report'] = "Laporan";
$lang['berkas_used'] = "Digunakan";
$lang['berkas_left'] = "Sisa";
$lang['berkas_bobot_0_100_error'] = "Bobot 0-100";
$lang['berkas_npwp'] = "NPWP";

// Proyek
$lang['berkas_project_plan'] = "Rencana Proyek";
$lang['berkas_project_realisasi'] = "Realisasi Proyek";
$lang['berkas_project_plan_detail'] = "Rencana Proyek Detail";
$lang['berkas_project'] = "Proyek";
$lang['berkas_project_name'] = "Nama Proyek";
$lang['berkas_project_code'] = "Kode Proyek";
$lang['berkas_project_type'] = "Jenis Proyek";
$lang['berkas_project_owner'] = "Pengguna Jasa";
$lang['berkas_project_penyedia'] = "Penyedia Jasa";
$lang['berkas_project_perencana'] = "Perencana";
$lang['berkas_project_pengawas'] = "Pengawas";
$lang['berkas_project_location'] = "Lokasi";
$lang['berkas_project_code_already_exist'] = "Kode Proyek Sudah Ada!";
$lang['berkas_project_siteplan'] = "Site Plan";

// SPK
$lang['berkas_spk'] = "Kontrak";
$lang['berkas_spk_no'] = "No. Kontrak";
$lang['berkas_spk_date'] = "Tgl. Kontrak";
$lang['berkas_spk_investor'] = "Sumber Dana";
$lang['berkas_spk_value'] = "Nilai Kontrak (Rp)";
$lang['berkas_spk_implementation_date'] = "Tgl Pelaksanaan";
$lang['berkas_spk_implementation_duration'] = "Durasi Pelaksanaan";
$lang['berkas_spk_maintenance_date'] = "Tgl Pemeliharaan";
$lang['berkas_spk_maintenance_duration'] = "Durasi Pemeliharaan";
$lang['berkas_spk_jenis'] = "Jenis Kontrak";
$lang['berkas_spk_status'] = "Status Kontrak";
$lang['berkas_spk_label'] = "Label Kontrak";
$lang['berkas_spk_payment_type'] = "Sistem Pembayaran";
$lang['berkas_spk_lingkup'] = "Lingkup Pekerjaan";
$lang['berkas_spk_portion_pp'] = "Porsi PP";
$lang['berkas_spk_number_already_exist'] = "No. Kontrak Sudah Ada!";
$lang['berkas_spk_included_ppn'] = "Included PPn";
$lang['berkas_spk_minus_ppn'] = "Nilai Kontrak - PPn (Rp)";
$lang['berkas_spk_adendum'] = "Adendum";
$lang['berkas_spk_confirm_adendum'] = "Apakah Anda yakin untuk meng-adendum kontrak ini? <br> Data existing pada kontrak ini akan dihapus dan schedule harus dibuat kembali.";


?>