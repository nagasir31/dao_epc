<?php
$lang['employee_title'] = "Employee";
$lang['employee_name'] = "Name";
$lang['employee_nrp'] = "Nrp";
$lang['employee_position'] = "Position";
$lang['employee_division'] = "Division";
$lang['employee_branch'] = "Branch";
$lang['employee_project'] = "Project";
$lang['employee_project_code'] = "Project Code";
$lang['employee_status'] = "Status";
$lang['employee_date_in'] = "Date in";
?>