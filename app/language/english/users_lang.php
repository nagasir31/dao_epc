<?php
$lang['users_title_list'] = "Users List";
$lang['users_group_name'] = "Group Name";
$lang['users_name'] = "User Name";
$lang['users_password'] = "Password";
$lang['users_password_conf'] = "Password Confirmation";
$lang['users_password_conf_wrong'] = "Wrong Password Confirmation !";
$lang['users_password_old'] = "Old Password";
$lang['users_password_equalto'] = "Please enter the same password as above";
$lang['users_password_old_wrong'] = "Old Password wrong";
$lang['users_email'] = "Email";
$lang['users_email_invalid'] = "Invalid Email !";
$lang['users_is_active'] = "Active ?";
$lang['users_active'] = "Active";
$lang['users_picture'] = "Picture";
$lang['users_password_old_required'] = "Password Old Required !";

?>
