<?php
$lang['employee_title'] = "Karyawan";
$lang['employee_name'] = "Nama";
$lang['employee_nrp'] = "Nrp";
$lang['employee_position'] = "Jabatan";
$lang['employee_division'] = "Divisi";
$lang['employee_branch'] = "Cabang";
$lang['employee_project'] = "Proyek";
$lang['employee_project_code'] = "Kode Proyek";
$lang['employee_status'] = "Status";
$lang['employee_date_in'] = "Tgl Masuk";
?>