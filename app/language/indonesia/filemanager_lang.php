<?php
$lang['filemanager_rename'] = "Rename";
$lang['filemanager_upload_files'] = "Upload Files";
$lang['filemanager_folder'] = "Folder";
$lang['filemanager_create_folder'] = "Create A New Folder";
$lang['filemanager_new_folder'] = "New Folder";
$lang['filemanager_show_url'] = "Show Url";
$lang['filemanager_extract'] = "Extract";
$lang['filemanager_file_info'] = "File Info";
$lang['filemanager_dropzone_upload_help'] = "Drag & Drop files or click in the area above (modern browsers) and select the file(s). When the upload is complete, click the 'Return to files list' button.";
$lang['filemanager_dropzone_return'] = "Return to the files list";
$lang['filemanager_text_filter'] = "filter text...";
$lang['filemanager_archives'] = "Archives";
$lang['filemanager_audios'] = "Audios";
$lang['filemanager_videos'] = "Videos";
$lang['filemanager_box_view'] = "Box View";
$lang['filemanager_list_view'] = "List View";
$lang['filemanager_column_view'] = "Column View";
$lang['filemanager_help_swipe'] = "Swipe the name of file/folder to show options";
$lang['filemanager_filename'] = "Filename";
$lang['filemanager_size'] = "Size";
$lang['filemanager_date'] = "Date";
$lang['filemanager_type'] = "Type";
$lang['filemanager_sorting'] = "Sorting";
$lang['filemanager_operations'] = "Operations";
$lang['filemanager_dimensions'] = "Dimensions";
$lang['filemanager_confirm_delete_folder'] = "Are you sure to delete the folder and all the elements in it ?";
$lang['filemanager_confirm_delete_file'] = "Are you sure you want to delete this file ?";

$lang['filemanager_error_perm_rename_folder'] = "You do not have permission to rename folder '%s'";
$lang['filemanager_error_rename_folder'] = "Could not rename folder '%s'";

$lang['filemanager_error_perm_delete_folder'] = "You do not have permission to delete folder '%s'";
$lang['filemanager_error_delete_folder'] = "Could not delete folder '%s'";

$lang['filemanager_error_perm_rename_file'] = "You do not have permission to rename file '%s'";
$lang['filemanager_error_is_not_file'] = "'%s' is not a file";
$lang['filemanager_error_not_exist_file'] = "The file '%s' does not exist";
$lang['filemanager_error_rename_file'] = "Could not rename file '%s'";

$lang['filemanager_error_perm_delete_file'] = "You do not have permission to delete folder '%s'";
$lang['filemanager_error_delete_file'] = "Could not delete file '%s'";

$lang['filemanager_error_open_file'] = "Failed to open the file";
$lang['filemanager_error_unknown_action'] = "Unknown Action";
$lang['filemanager_error_something_wrong'] = "Something Wrong !!!";

$lang['filemanager_error_invalid_ext'] = "Invalid Extension";
$lang['filemanager_error_upload'] = "Upload Error";
$lang['filemanager_error_server'] = "Server Error";
?>