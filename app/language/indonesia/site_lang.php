<?php
$lang['site_app_logo'] = "Logo Aplikasi";
$lang['site_app_name'] = "Nama Aplikasi";
$lang['site_app_author'] = "Pemilik Aplikasi";
$lang['site_app_email'] = "Akun Email Sistem";
$lang['site_app_def_date_format'] = "Default Date Format";
$lang['site_app_def_datetime_format'] = "Default Datetime Format";
$lang['site_app_def_lang'] = "Default Language";
$lang['site_is_frontend_online'] = "Activate Frontend Site ?";
$lang['site_is_mobile_online'] = "Activate Mobile Site ?";
$lang['site_is_backend_online'] = "Aktifkan Aplikasi ?";
$lang['site_is_active_frontned'] = "Activate Frontend Module ?";
$lang['site_is_active_backend'] = "Activate Backend Module ?";
$lang['site_is_active_frontend_acl'] = "Activate Frontend ACL Module ?";
$lang['site_is_multilingual'] = "Activate Multilingual ?";
$lang['site_is_multitheme'] = "Activate Multitheme ?";
$lang['site_def_theme'] = "Default Theme";
$lang['site_online'] = "Site Online ?";
$lang['site_is_email_system_active'] = "Aktifkan Sistem Email ?";
$lang['site_email_test'] = "Akun Email Testing";
$lang['site_is_email_test_active'] = "Aktifkan Email Testing ?";
?>