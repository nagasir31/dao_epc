<?php
$lang['users_title_list'] = "List Pengguna";
$lang['users_group_name'] = "Nama Grup";
$lang['users_name'] = "Nama Pengguna";
$lang['users_password'] = "Katasandi";
$lang['users_password_conf'] = "Konfirmasi Katasandi";
$lang['users_password_conf_wrong'] = "Konfirmasi Katasandi Salah !";
$lang['users_password_old'] = "Katasandi Lama";
$lang['users_password_equalto'] = "Masukan katasandi yg sama seperti diatas";
$lang['users_password_old_wrong'] = "Katasandi Lama salah";
$lang['users_email'] = "Email";
$lang['users_email_invalid'] = "Email tidak valid !";
$lang['users_is_active'] = "Aktif ?";
$lang['users_active'] = "Aktif";
$lang['users_picture'] = "Gambar";
$lang['users_password_old_required'] = "Katasandi lama diperlukan !";
?>
