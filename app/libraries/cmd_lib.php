<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: rette
 * Date: 07/08/2018
 * Time: 10:46
 */

class Cmd_lib
{
    protected $cmd;

    function __construct()
    {
        if(strstr(PHP_OS, "WIN"))
        {
            $this->cmd =  FCPATH ."others/";
        }
        else
        {
            $this->cmd = "";
        }
    }

    function generate_image_from_video($video_source, $image_output, $width, $height){

        if(file_exists($video_source))
        {
            $duration = shell_exec($this->cmd."ffprobe {$video_source} -show_entries format=duration -of compact=p=0:nk=1 -v 0");
            shell_exec($this->cmd."ffmpeg -y  -i {$video_source} -f mjpeg -vframes 1 -ss " . ($duration / 2) . " -s {$width}x{$height} {$image_output}");

            return file_exists($image_output);
        }

        return false;
    }

    function set_watermark_pdf($file_in,$file_out,$file_watermark){
        if(file_exists($file_in))
        {

            $this->cmd .= "cpdf -stamp-under {$file_watermark} -diagonal {$file_in} -o {$file_out}";

            shell_exec("{$this->cmd}");

            return file_exists($file_out);
        }

        return FALSE;
    }

    function set_protection_pdf($file_in,$file_out,$password = '')
    {
        if(file_exists($file_in))
        {

            $year = date("Y");
            $author = '*Kn0wl3dge-Center_ptppBDEDoc@%?!^'.$year;

            if(! empty($password))
            {
                $this->cmd .= 'cpdf -encrypt AES "'.$author.'" "'.$password.'" -no-annot -no-edit -no-copy '.$file_in.'. -o '.$file_out;
            }
            else
            {
                $this->cmd .= 'cpdf -encrypt AES "'.$author.'" "" -no-annot -no-edit -no-copy '.$file_in.' -o '.$file_out;
            }

            shell_exec("{$this->cmd}");

            return file_exists($file_out);
        }

        return FALSE;

    }

    function create_zip($files = array(),$destination = '',$password = '')
    {
        $valid_files = array();
        if(is_array($files))
        {
            foreach($files as $file)
            {
                if(file_exists($file))
                {
                    //preg_replace('/[\(\)]/','',$file);
                    $valid_files[] = '"'.$file.'"';
                }
            }
        }

        if(count($valid_files))
        {
            $cmd_files = implode(" ",$valid_files);

            $cmd_password = "";
            if(! empty($password))
            {
                $cmd_password = "-P {$password}";
            }

            $this->cmd .= 'zip -j';

            shell_exec("{$this->cmd} {$cmd_password} {$destination} {$cmd_files}");

            return file_exists($destination);
        }

        return FALSE;

    }

    function set_watermark_pdf4($file_in,$file_out)
    {
        if(file_exists($file_in))
        {
            $text_watermark = 'PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			PT. PP (Persero) Tbk-PT. PP (Persero) Tbk-PT. PP (Persero) Tbk\n
			';
            $string = str_replace(array("\n", "\r"), '', $text_watermark);
            $cmd_command = $this->cmd.'cpdf -add-text "'.$string.'" -diagonal -font-size 40 -opacity 0.15 -line-spacing 2.5 "'.$file_in.'" -o "'.$file_out.'"';

            shell_exec("{$cmd_command}");

            return file_exists($file_out);
        }

        return FALSE;

    }

    function set_copyright_pdf($file_in,$file_out)
    {
        if(file_exists($file_in))
        {

            $text = 'Hak Cipta dilindungi undang-undang. Dilarang memperbanyak atau memindahkan sebagian atau seluruh isi buku ini dalam\n 
			bentuk apapun, baik secara elektronis maupun mekanis, termasuk memfotocopy, merekam atau dengan sistem penyimpanan lainnya, tanpa izin tertulis dari penulis.
			';
            $string = str_replace(array("\n", "\r"), '', $text);
            $font = "Times-Italic";

            $this->cmd .= 'cpdf -add-text "'.$string.'" -bottom 10 -font-size 8 -opacity 0.9 -font "'.$font.'" "'.$file_in.'" -o "'.$file_out.'"';

            shell_exec("{$this->cmd}");

            return file_exists($file_out);
        }
        else
        {
            return FALSE;
        }
    }

    function get_url_from_qr($file_in, $file_out, $text_out){
        $this->cmd = 'identify -format %n '. $file_in;
        exec($this->cmd, $output, $exit_status);
        if( $exit_status !== 0 ) {
            return false;
        }
        if(count($output) > 0){
            //count total page
            $total_page = substr($output[0], 0, 1);
            $page_index = (int)$total_page - 1;

            //convert to jpg
            $this->cmd  = 'convert -density 150 -trim '.$file_in.'['.$page_index.'] -quality 100 -flatten -sharpen 0x1.0 '.$file_out;
            shell_exec("{$this->cmd}");

            //scan QR Code
            if(file_exists($file_out))
            {
                $this->cmd = 'zbarimg --raw '. $file_out . ' > '. $text_out;
                exec($this->cmd, $outputs);
                $result =  file_get_contents($text_out);
                if(strlen($result) > 0 ){
                    $result = strtok($result, "\n\t ");
                    if($this->_is_real_url($result)){
                        return $result;
                    }
                }else{
                    $this->cmd = 'convert -density  200x200 ' . $file_in . '[' . $page_index . '] -contrast -adaptive-sharpen 0x.6 -colorspace gray +dither -colors 2 -type bilevel ' . $file_out;
                    shell_exec("{$this->cmd}");
                    if(file_exists($file_out)) {
                        $this->cmd = 'zbarimg --raw ' . $file_out . ' > ' . $text_out;
                        exec($this->cmd, $outputs);
                        $result = file_get_contents($text_out);
                        if (strlen($result) > 0 ) {
                            $result = strtok($result, "\n\t ");
                            if($this->_is_real_url($result)){
                                return $result;
                            }
                        } else {
                            $this->cmd = 'convert -density 300 -trim ' . $file_in . '[' . $page_index . '] -quality 100 ' . $file_out;
                            shell_exec("{$this->cmd}");
                            if (file_exists($file_out)) {
                                $this->cmd = 'zbarimg --raw ' . $file_out . ' > ' . $text_out;
                                exec($this->cmd, $outputs);
                                $result = file_get_contents($text_out);
                                if (strlen($result) > 0 ) {
                                    $result = strtok($result, "\n\t ");
                                    if($this->_is_real_url($result)){
                                        return $result;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return false;
    }

    function _is_real_url($url){
        $regex = "((https?|ftp)\:\/\/)?"; // SCHEME
        $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
        $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
        $regex .= "(\:[0-9]{2,5})?"; // Port
        $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
        $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
        $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor

        if(preg_match("/^$regex$/i", $url)) // `i` flag for case-insensitive
        {
            return true;
        }

        return false;
    }

}