<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CI-Extjs CRUD
 *
 * A Codeigniter library that creates a CRUD automatically with just few lines of code.
 *
 * Copyright (C) 2012  Ryanceki. 
 *
 * LICENSE
 *
 * CI-Extjs CRUD is released using the GPL v3 (license-gpl3.txt)
 * You are free to use, modify and distribute this software, but all copyright information must remain.
 *
 * @package     CI-Extjs CRUD
 * @copyright  	Copyright (c) 2012, Ryanceki
 * @version    	1.0.0 (March 2012)
 * @author      Ryanceki <ry4nceki@yahoo.com>
 * @link		http://www.cekicuki.com
 */
class Crud extends MY_Model {
	/**
	 * Declaration variable here
	 */
	// All private variable
	private $_action = array(
		0	=> 'unknown',
		1	=> 'create',
		2	=> 'read',
		3	=> 'update',
		4	=> 'delete',
		5	=> 'list',
		6	=> 'update_bool',
		7   => 'export_xls'
	);
	
	private $_rel_attributes = array('primary_key','foreign_key','related_key','related_alias','related_column');
	private $_rel_type = array('belongs_to','has_many','has_one','many_many');
	private $_start;
	private $_limit;
	private $_search = NULL;
	private $_fields;
	private $_sort;
	private $_order_by = array();
	private $_dir;
	private $_perpage;
	private $_fields_meta = array();
	private $_id;
	private $_del_update;
	private $_table_alias = array();
	
	// All protected variable
	protected $base_table          = NULL;
	protected $base_table_key      = NULL;
	protected $action_code 		   = NULL;
	protected $relations		   = NULL;
	protected $relation_type 	   = NULL;
	protected $relation_attributes = NULL;
	protected $order_by 		   = NULL;
	protected $limit 			   = NULL;
	protected $where 			   = NULL;
	protected $or_where            = NULL;
	protected $like                = NULL;
	protected $or_like             = NULL;
	protected $search         	   = NULL;
	protected $select              = NULL;
	protected $unselect            = NULL;
	protected $with_total          = FALSE;
	protected $with_paging         = FALSE;
	protected $extjs_grid          = FALSE;
	protected $extjs_grid_meta     = FALSE;
	protected $extjs_grid_title    = NULL;
	protected $extjs_grid_header   = array();
	protected $extjs_grid_hide     = array();
	protected $extjs_form          = FALSE;
	protected $extjs_form_meta     = FALSE;
	protected $input_post          = array();
	protected $type_of_grid        = "regular";
	protected $created_id		   = array();
	protected $data_updated		   = array();
	protected $boolean_column      = array();
	protected $boolean_inactive_action_column = array();
	protected $field_password      = array();
	protected $field_email         = array();
	protected $field_year	   	   = array();
	protected $unique_uname        = '';
	protected $unique_field        = array();
	protected $field_alphanum      = array();
	protected $do_spot      	   = FALSE;
	protected $input_mode          = '';
	protected $show_pk             = FALSE;
	protected $extjs_grid_renderer = array();
	protected $extjs_grid_header_width = array();
	protected $spinner_field = array();
	protected $field_duplicate_entry = array();
	
	
	protected $show_ext_form_fields   = array();
	protected $unshow_ext_form_fields = array();
	
	protected $show_ext_grid_cols   = array();
	protected $unshow_ext_grid_cols = array();
	
	// callbacks
	protected $before_create = FALSE;
	protected $after_create	 = FALSE;
	protected $before_read	 = FALSE;
	protected $after_read	 = FALSE;
	protected $before_update = FALSE;
	protected $after_update  = FALSE;
	protected $before_delete = FALSE;
	protected $after_delete	 = FALSE;
	
	protected $before_create_params = NULL;
	protected $after_create_params	= NULL;
	
	// The class constractor
	function __construct()
	{
		$this->load->helper('url','convert_date');
	}
	
	/**
	 * 
	 * Get the default assets for use in CI-Extjs CRUD
	 * @return array
	 *
	 */
	function header_js($config = array())
	{		
		$search = isset($config['search']) ? $config['search'] : TRUE;
		$type   = isset($config['grid_type']) ? $config['grid_type'] : "both";
		
		$regular = $editor = FALSE;
		
		if($type == "regular")
		{
			$regular = TRUE;
		}
		elseif($type == "editor")
		{
			$editor = TRUE;
		}
		else
		{
			$regular = TRUE;
			$editor = TRUE;
		}
		
		if($search === TRUE)
		{
			$header[] = '<script type="text/javascript" src="'.base_assets_url('js/ext/plugins/ext.ux.grid.search.js').'"></script>';
		}
		
		$header[] = '<script type="text/javascript" src="'.base_assets_url('js/ext/plugins/bufferView.js').'"></script>';
		
		if($regular === TRUE)
		{
			$header[] = '<script type="text/javascript" src="'.base_assets_url('js/ext/plugins/dynamic/ext.ux.DynamicGridPanel.js?n='.time()).'"></script>';
		}
		
		if($editor === TRUE)
		{
			$header[] = '<script type="text/javascript" src="'.base_assets_url('js/ext/plugins/checkColumn.js').'"></script>';
			$header[] = '<script type="text/javascript" src="'.base_assets_url('js/ext/plugins/dynamic/ext.ux.DynamicEditorGridPanel.js?n='.time()).'"></script>';
		}
		
		$header[] = '<script type="text/javascript" src="'.base_assets_url('js/ext/plugins/multiSelect/multiSelect.js').'"></script>';
		$header[] = '<script type="text/javascript" src="'.base_assets_url('js/ext/plugins/multiSelect/itemSelector.js').'"></script>';
		
		return $header;
	}
	
	/**
	 * 
	 * Here is the core of the CI-Extjs CRUD
	 * @return void
	 *
	 */
	function render()
	{
		if($this->login_manager->logged_in)
		{
			switch ($this->action_code) 
			{
				case 1: //create
					if($this->_create())
					{
						echo '{"success":true, "data":'.json_encode($this->created_id).'}';
					}
					else
					{
						echo $this->output->status_callback('json_unsuccess');
					}
					break;
				case 2: //read
					// call the _read() function				
					$output = $this->_read();
					
					if($this->extjs_grid)
					{
						/* print_r($this->_fields_meta);
						die(); */
						$this->load->library('extjs_grid');
						$extjs_grid = new Extjs_Grid();
						
						$extjs_grid->success   = TRUE;
						$extjs_grid->total     = $output['total'];
						$extjs_grid->data      = $output['data'];
						$extjs_grid->meta      = $this->_fields_meta;
						$extjs_grid->form      = $this->extjs_form;
						$extjs_grid->type_grid = $this->type_of_grid;
						$extjs_grid->bool_col  = $this->boolean_column;
						$extjs_grid->bool_inactive_action_col = $this->boolean_inactive_action_column;
						$extjs_grid->paging    = $this->with_paging;
						$extjs_grid->pk        = $this->base_table_key;
						$extjs_grid->field_pwd = $this->field_password;
						$extjs_grid->field_duplicate_entry = $this->field_duplicate_entry;
						$extjs_grid->field_eml = $this->field_email;
						$extjs_grid->field_yr  = $this->field_year;
						$extjs_grid->uniq_name = $this->unique_uname;
						$extjs_grid->uniq_field = $this->unique_field;
						$extjs_grid->unshow_fields = $this->unshow_ext_form_fields;
						$extjs_grid->unshow_cols = $this->unshow_ext_grid_cols;
						$extjs_grid->title = $this->extjs_grid_title;
						$extjs_grid->field_alphanum = $this->field_alphanum;
						$extjs_grid->do_spot = $this->do_spot;
						$extjs_grid->header = $this->extjs_grid_header;
						$extjs_grid->header_width = $this->extjs_grid_header_width;
						$extjs_grid->hide = $this->extjs_grid_hide;
						$extjs_grid->show_pk = $this->show_pk;
						$extjs_grid->base_table = $this->base_table;
						$extjs_grid->renderer_cols = $this->extjs_grid_renderer;
						$extjs_grid->spinner_field = $this->spinner_field;
						
						if($this->extjs_grid_meta)
						{
							$extjs_grid->build_json_meta($this->base_table);
						}
						else
						{
							$extjs_grid->build_json();
						}
					}
					else
					{
						$this->_build_json_output($output['total'],$output['data']);
					}
					
					$this->_unset_data($output);
					break;
				case 3: //update
					if($this->_update())
					{
						echo $this->output->status_callback('json_success');
					}
					else
					{
						echo $this->output->status_callback('json_unsuccess');
					}
					break;
				case 4: // delete
					if($this->_delete())
					{
						echo $this->output->status_callback('json_success');
					}
					else
					{
						echo $this->output->status_callback('json_unsuccess');
					}
					break;
				case 5: // list
					$this->_list();
					break;
				case 6: // update bool
					$this->_update_bool();
					break;
				case 7:
					$output = $this->_read();
					$this->_export_xls($output['data']);
					//print_r($output['data']);
					break;
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess',NULL,TRUE);
		}
	}
	
	/**
	 * 
	 * Set an action to understand what type of CRUD you use.
	 * @param ($action_name string)
	 * @return Crud
	 *
	 */
	function set_action($action_name)
	{	
		if( ! empty($action_name) && $this->action_code === NULL)
		{
			$code = $this->_get_action_code(strtolower($action_name));
			
			if( ! empty($code))
			{
				$this->action_code = $code;
			}
			else
			{
				show_error('Unknown action');
			}
		}
		elseif( ! empty($action_name))
		{
			show_error('You have already set an \"action_name\" once...');
		}
		else
		{
			show_error('The \"action_name\" cannot be empty.');
		}
		
		return $this;
	}
	
	
	/**
	 * 
	 * Set the post data
	 * @param ($input_post object)
	 * @return string of the input post
	 *
	 */
	function post_data($input_post)
	{
		
		if($this->type_of_grid === 'editor')
		{
			if(isset($input_post['data']))
			{
				
				$post_data = array();
				
				if($this->action_code == 4) 
				{
					$input_post['id'] = $input_post['data'];
					unset($input_post['data']);
				}	
				else
				{
					$input_post_data = json_decode($input_post['data']);
					
					unset($input_post);
					
					if(count($input_post_data) <= 1) 
					{
						$input_post[] = $input_post_data;
					}
					else
					{
						$input_post = $input_post_data;
					}
					
					$input_post = array_map('get_object_vars', $input_post);
					
				}
			}
			else
			{
				$input_post_data = $input_post;
				unset($input_post);
				$input_post[] = $input_post_data;
				//$this->debug_variable($input_post_data);
			}
		}
		else
		{
			$input_post_data = $input_post;
			unset($input_post);
			$input_post[] = $input_post_data;
		}
		
		
		$this->input_post  = $input_post;
		$this->_start      = isset($input_post[0]['start'])   ? $input_post[0]['start'] : 0;
		$this->_limit      = isset($input_post[0]['limit'])   ? $input_post[0]['limit'] : 15;
		$this->_search     = isset($input_post[0]['query'])   ? $input_post[0]['query'] : NULL;
		$this->_fields     = isset($input_post[0]['fields'])  ? json_decode($input_post[0]['fields']) : "";
		$this->_sort       = isset($input_post[0]['sort'])    ? $input_post[0]['sort'] : "";
		$this->_dir        = isset($input_post[0]['dir'])     ? $input_post[0]['dir']  : "";
		$this->_perpage    = isset($input_post[0]['perpage']) ? $input_post[0]['perpage'] : "";
		$this->_id         = isset($input_post[0]['id'])   ? json_decode($input_post[0]['id']) : (isset($input_post['id']) ?  json_decode($input_post['id']) : "");
		//$this->_del_update = isset($input_post['delupdate'])? json_decode($input_post['delupdate']) : TRUE;
		
		//print_r($this->_id);die();
		
		if(isset($this->_limit))
		{
			$this->limit($this->_limit,$this->_start);
		}
		
		if( ! empty($this->_sort))
		{
			$this->order_by($this->_sort, $this->_dir);
		}
	}
	
	private function _validate_input($input)
	{
		//$this->debug_variable($input);
		$data_input = array();
		foreach($input as $val)
		{
			$temp = array();
			foreach($val as $key => $child)
			{
				if($child == "")
				{
					$child = "";
					$col = $this->MY_get_field_types($this->base_table, $key);
					if(isset($col[0]))
					{
						if($col[0]->type == "tinyint" && $col[0]->max_length == 1)
						{
							$child = 0;
						}
					}
				}
				$temp[$key] = $child; 
			}
			$data_input[] = $temp;
		}

		return $data_input;
	}
	
	/**
	 * 
	 * Sets the base table that we will get the data. 
	 * @param ($table_name string)
	 * @return Crud
	 *
	 */
	function set_table($table_name)
	{
		if( ! empty($table_name) && $this->base_table === NULL)
		{
			if($this->MY_check_table_exists($table_name))
			{
				$this->base_table = $table_name;
			}
		}
		elseif( ! empty($table_name))
		{
			show_error('You have already insert a table_name once...');
		}
		else 
		{
			show_error('The \"table_name\" cannot be empty.');
		}
			
		return $this;
	}
	
	function set_relation($rel_type, $attributes = array())
	{
		//print_r($attributes);
		if( ! empty($rel_type) && in_array(strtolower($rel_type), $this->_rel_type))
		{
			if( ! empty($attributes))
			{
				if(isset($attributes['related_to']) && isset($attributes['related_key']) && isset($attributes['related_alias']))
				{
					$exist = $this->MY_check_table_exists($attributes['related_to']);
					
					$rel = FALSE;
					if($exist)
					{
						if($rel_type == 'belongs_to')
						{
							if(isset($attributes['foreign_key']))
							{
								$rel = TRUE;
							}
							else
							{	
								$this->_show_error('Relation "belongs_to" must have "foregin_key" !"');
							}
						}
						else
						{
							if($rel_type == 'many_many')
							{
								if(isset($attributes['link_to']) && isset($attributes['link_key_1']) && isset($attributes['link_key_2']) && isset($attributes['link_alias']))
								{
									$rel = TRUE;
								}
								else
								{	
									$this->_show_error('Relation "many_many" must have "link_to", "link_key_1", "link_key_2", "link_alias" !"');
								}
							}
							else
							{
								$rel = TRUE;
							}
						}
						
					}
					
					if($rel === TRUE)
					{
						$this->relations[] = array('rel_type' => $rel_type, 'rel_attributes' => $attributes);
					}
				}
				else
				{
					$this->_show_error('The attributes of "related_to" "related_key" "related_alias" must be define !');
				}
			}
			else
			{
				$this->_show_error('The "attributes" cannot be empty !');
			}
		}
		else
		{
			$this->_show_error('The relation type not valid !');
		}
		
		return $this;
	}
	
	function set_type_grid($val = 'regular')
	{
		$this->type_of_grid = $val;
	}
	
	function set_boolean_column($val = array())
	{
		$this->bool_column = $val;
	}
	
	function set_boolean_inactive_action_column($val = array())
	{
		$this->boolean_inactive_action_column = $val;
	}
	
	function set_field_password($field = array())
	{
		$this->field_password = $field;
	}
	
	function set_field_year($field = array())
	{
		$this->field_year = $field;
	}
	
	function set_field_email($field = array())
	{
		$this->field_email = $field;
	}
	
	function set_field_duplicate_entry($field = array())
	{
		$this->field_duplicate_entry = $field;
	}
	
	function set_field_alphanum($field = array())
	{
		$this->field_alphanum = $field;
	}
	
	function set_unique_uname($field = '')
	{
		$this->unique_uname = $field;
	}
	
	function set_unique_field($field = array())
	{
		$this->unique_field = $field;
	}
	
	function set_input_mode($str = '')
	{
		$this->input_mode = $str;
	}
	
	function set_show_pk($bool = TRUE)
	{
		$this->show_pk = $bool;
	}
	
	function order_by($order, $direction = 'ASC')
	{
		if( ! empty($order))
		{
			$this->_order_by[] = array($order, $direction);
		}
	}
	
	function select($select, $escape = TRUE)
	{
		/* foreach($select as $val)
		{
			$this->select[] = array($val,$escape);
		} */
		
		$this->select = array($select, $escape);	
		//print_r($select);die();
	}
	
	function unselect($unselect, $escape = TRUE)
	{
		$this->unselect = array($unselect, $escape);	
	}
	
	function unshow_extjs_form_fields($fields = array())
	{
		$this->unshow_ext_form_fields = $fields;	
	}
	
	function unshow_extjs_grid_cols($cols = array())
	{
		$this->unshow_ext_grid_cols = $cols;	
	}
	
	function set_do_spot($bool = TRUE)
	{
		$this->do_spot = $bool;	
	}
	
	function where($key, $value = NULL, $escape = TRUE, $operator = '=')
	{
		$this->where[] = array($key, $value, $escape, $operator);
	}
	
	function or_where($key, $value = NULL, $escape = TRUE)
	{
		$this->or_where[] = array($key, $value, $escape);
	}	
	
	function like($field, $match = '', $side = 'both')
	{
		$this->like[] = array($field, $match, $side);
	}
	
	function or_like($field, $match = '', $side = 'both')
	{
		$this->or_like[] = array($field, $match, $side);
	}	

	function limit($limit, $offset = '')
	{
		$this->limit = array($limit,$offset);
	}
	
	function with_total($bool = TRUE)
	{
		$this->with_total = $bool;
	}
	
	function with_paging($bool = TRUE)
	{
		$this->with_paging = $bool;
	}
	
	function set_extjs_grid($bool = TRUE, $meta = TRUE)
	{
		$this->extjs_grid = $bool;
		$this->extjs_grid_meta = $meta;
	}
	
	function set_extjs_grid_title($title)
	{
		$this->extjs_grid_title = $title;
	}
	
	function set_extjs_grid_header($header = array())
	{
		$this->extjs_grid_header = $header;
	}
	
	function set_extjs_grid_header_width($header = array())
	{
		$this->extjs_grid_header_width = $header;
	}
	
	function set_extjs_grid_sel_model($type)
	{
		$this->extjs_grid_sel_model = $type;
	}
	
	function set_extjs_grid_renderer($renderer = array())
	{
		$this->extjs_grid_renderer = $renderer;
	}
	
	function set_extjs_grid_hide($header = array())
	{
		$this->extjs_grid_hide = $header;
	}
	
	function set_extjs_form($bool = TRUE, $meta = TRUE)
	{
		$this->extjs_form = $bool;
		$this->extjs_form_meta = $meta;
	}
	
	function set_spinner_field($field = array())
	{
		$this->spinner_field = $field;
	}
	
	function del_update($bool = FALSE)
	{
		$this->_del_update = $bool;
	}
	
	function set_table_key($base_table_key)
	{
		if( ! empty($base_table_key) && $this->base_table_key === NULL)
		{
			if($this->MY_is_primary_key($this->base_table, $base_table_key))
			{
				$this->base_table_key = $base_table_key;
			}
			else
			{
				$this->base_table_key = $this->MY_get_primary_key($this->base_table);
			}
		}
		elseif( ! empty($base_table_key))
		{
			show_error('You have already set a base table primary key...');
		}
		else 
		{
			show_error('The \"key\" cannot be empty.');
		}
			
		return $this;
	}
	
	// set callbacks create
	function set_before_create($config = FALSE)
	{
		$this->before_create[] = $config;
	}
	
	function set_after_create($func = FALSE, $extra_params = NULL)
	{
		$this->after_create[] = $func;
	}
	
	// set callbacks read
	function set_before_read($func = FALSE)
	{
		$this->before_read[] = $func;
	}
	
	function set_after_read($config = FALSE)
	{
		$this->after_read[] = $config;
	}
	
	// set callbacks update
	function set_before_update($func = FALSE)
	{
		$this->before_update[] = $func;
	}
	
	function set_after_update($func = FALSE)
	{
		$this->after_update[] = $func;
	}
	
	// set callbacks create
	function set_before_delete($func = FALSE)
	{
		$this->before_delete[] = $func;
	}
	
	function set_after_delete($func = NULL)
	{
		$this->before_delete[] = $func;
	}
	
	/**
	 * CRUD Function start here
	 * Function : 
	 * _create()
	 * _read()
	 * _update()
	 * _delete()
	 * _list()
	 */
	 
	private function _create()
	{
		$rel = array();
		$tmp_rel = array();
		$data_insert_1 = array();
		// reset the created id
		$this->created_id = array();
		
		$this->input_post = $this->_validate_input($this->input_post);
		
		
		foreach($this->input_post as $key0 => $val0)
		{
			foreach($val0 as $key => $val)
			{
				if(is_array($val))
				{
					//$val = serialize($val);
					$val = json_encode($val);
				}
				
				if(strlen($val) > 0)
				{
					$keys = explode('__',$key);
					if(isset($keys[1]))
					{
						if(in_array($keys[0],$tmp_rel))
						{
							$i = array_search($keys[0],$tmp_rel);
							$rel[$i][$key] = $val;
						}
						else
						{
							$tmp_rel[] = $keys[0];
							$rel[] = array($key => $val);
						}	
					}
					else
					{
						if($this->MY_is_field_exists($key, $this->base_table))
						{
							if($this->_is_date($val))
							{
								$data_insert_1[$key] = $this->_check_date($val);
							}
							elseif(in_array($key, $this->field_password))
							{
								$this->load->library('encrypt');
								$data_insert_1[$key] = $this->encrypt->sha1($val.$this->config->item('encryption_key'));
							}
							else
							{
								$val = $this->_check_input_mode($this->input_mode,trim($val));
								/* if($this->input_mode == 'upper')
								{
									$data_insert_1[$key] = $this->_check_field_type($this->base_table, $key, strtoupper($val));
								}
								elseif($this->input_mode == 'lower')
								{
									$data_insert_1[$key] = $this->_check_field_type($this->base_table, $key, strtolower($val));
								}
								elseif($this->input_mode == 'ucwords')
								{
									$data_insert_1[$key] = $this->_check_field_type($this->base_table, $key, ucwords($val));
								}
								else
								{
									$data_insert_1[$key] = $this->_check_field_type($this->base_table, $key, $val);
								} */
								
								$data_insert_1[$key] = $this->_check_field_type($this->base_table, $key, $val);
							}
						}
					}
				}
			}
			
			//print_r($data_insert_1);die();
			
			if($this->MY_is_field_exists('created_by', $this->base_table))
			{
				$instance = &get_instance();
				$data_insert_1['created_by'] = $this->_check_input_mode($this->input_mode,$instance->get_user_name());
			}
			
			if($this->MY_is_field_exists('created_datetime', $this->base_table))
			{
				$instance = &get_instance();
				$data_insert_1['created_datetime'] = $instance->get_date_time();
			}

			if( ! empty($data_insert_1))
			{
				if($this->before_create)
				{	
					$data_insert_1 = $this->_run_before_callbacks('create', $this->before_create, $data_insert_1);
				}
				
				$id = $this->MY_insert($this->base_table, $data_insert_1);
				
				if( ! empty($id))
				{
					$this->created_id[] = array('id' => $id);
					if( ! empty($this->relations))
					{
						$relations = $this->relations;
						$total_rel = count($relations);
						$total_success = 0;
						$total_insert  = $total_rel;
						
						for($i = 0; $i < $total_rel; $i++)
						{
							if(! isset($relations[$i]['rel_attributes']['uneditable']) || $relations[$i]['rel_attributes']['uneditable'] == FALSE)
							{
								if($relations[$i]['rel_type'] === 'has_one') // if the relationship is has_one
								{	
									$data_insert_2 = array();
									foreach($rel[$i] as $key => $val)
									{
										$keys = explode('__',$key);
										if(isset($keys[1]))
										{
											$data_insert_2[$keys[1]] = $val;
										}
									}
									
									if( ! empty($data_insert_2))
									{
										$id_2 = $this->MY_insert($relations[$i]['rel_attributes']['related_to'], $data_insert_2);
												
										if( ! empty($id_2))	
										{
											$data_update_1 = array($relations[$i]['rel_attributes']['foreign_key'] => $id_2);
											$result_2 = $this->MY_update($this->base_table, $this->base_table_key, $id, $data_update_1);
												
											if($result_2)	
											{
												$total_success++;
											}
										}
									}
									else
									{
										$total_insert--;
									}
								}
								elseif($relations[$i]['rel_type'] === 'has_many') // if the relationship is has_many
								{
									if( ! empty($rel[$i]))
									{
										foreach($rel[$i] as $key => $val)
										{
											$keys = explode('__',$key);
											if(isset($keys[1]))
											{
												$id2 = explode(",",$val);	
											}
										}
									}
									
									if( ! empty($id2))
									{
										$data_insert = array($relations[$i]['rel_attributes']['related_fk'] => $id);
										$result_2 = $this->MY_update($relations[$i]['rel_attributes']['related_to'], $relations[$i]['rel_attributes']['related_key'], $id2, $data_insert);
										
										if($result_2)	
										{
											$total_success++;
										}
									}
									else
									{
										$total_insert--;
									}
								}
								elseif($relations[$i]['rel_type'] === 'many_many') // if the relationship is many_many
								{
									if( ! empty($rel[$i]))
									{
										foreach($rel[$i] as $key => $val)
										{
											$keys = explode('__',$key);
											if(isset($keys[1]))
											{
												$id2 = explode(",",$val);	
											}
										}
									}
									
									if( ! empty($id2))
									{
										$s = 0;
										foreach($id2 as $link_key)
										{
											$data_insert = array(
												$relations[$i]['rel_attributes']['link_key_1'] => $id,
												$relations[$i]['rel_attributes']['link_key_2'] => $link_key
											);
											
											$res_key = $this->MY_insert($relations[$i]['rel_attributes']['link_to'], $data_insert);
											
											if( ! empty($res_key))
											{
												$s++;
											}
										}
										
										if(count($id2) == $s)
										{
											$total_success++;
										}
									}
									else
									{
										$total_insert--;
									}
								}
								else // if the relationship is belongs_to
								{	
									if(isset($rel[$i]))
									{
										$data_insert_2 = array();
										foreach($rel[$i] as $key => $val)
										{
											$keys = explode('__',$key);
											if(isset($keys[1]))
											{
												$col = $this->MY_get_field_types($this->base_table, $relations[$i]['rel_attributes']['foreign_key']);
												if($col[0]->type == "tinyint" || $col[0]->type == "int")
												{
													if(is_numeric($val))
													{
														// we have to change the key here
														$data_insert_2[$relations[$i]['rel_attributes']['foreign_key']] = $val;
													}
												}
												elseif($col[0]->type == "varchar" || $col[0]->type == "char" || $col[0]->type == "text")
												{
													if(isset($relations[$i]['rel_attributes']['fk_char_numeric']) && $relations[$i]['rel_attributes']['fk_char_numeric'] == TRUE)
													{
														if(is_numeric($val))
														{
															$data_insert_2[$relations[$i]['rel_attributes']['foreign_key']] = $val;
														}
													}
													else
													{
														$exp = explode("-",$val);
														if(!isset($exp[1]))
														{
															$data_insert_2[$relations[$i]['rel_attributes']['foreign_key']] = $val;
														}
													}
												}
												else
												{
													$data_insert_2[$relations[$i]['rel_attributes']['foreign_key']] = $val;
												}
											}
										}
										
										if( ! empty($data_insert_2))
										{
											$result_2 = $this->MY_update($this->base_table, $this->base_table_key, $id, $data_insert_2);
													
											if($result_2)	
											{
												$total_success++;
											}
										}
										else
										{
											$total_insert--;
										}
									}
									else
									{
										return TRUE;
									}
								}
							}
							else
							{
								$total_success++;
							}
						}
						
						if($total_insert == $total_success)
						{
							//return TRUE;
							continue;
						}
						else
						{
							return FALSE;
						}
					}
					else
					{
						//return TRUE;
						continue;
					}
				}
				else
				{
					if($this->show_pk)
					{
						return TRUE;
					}
					else
					{
						return FALSE;
					}
				}
			}
			else
			{
				return FALSE;
			}
		}
		
		if(count($this->created_id) == count($this->input_post))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
		unset($rel);
		unset($tmp_rel);
		unset($data_insert_1);
	}
	 
	private function _create2()
	{
		if( ! empty($this->relations))
		{
			foreach($this->relations as $relation)
			{
				if($relation['rel_type'] === 'has_one') // if there is has_one relationship
				{
					$data_insert_1 = array();
					$data_insert_2 = array();
					foreach($this->input_post as $key => $val)
					{
						if(strlen($val) > 0)
						{
							$keys = explode('__',$key);
							if(isset($keys[1]))
							{
								$data_insert_1[$keys[1]] = $val;
							}
							else
							{
								if($this->_is_date($val))
								{
									// call the _check_date function
									$data_insert_2[$key] = $this->_check_date($val);
								}
								else
								{
									// force to keep the input lowercase
									$data_insert_2[$key] = trim(strtolower($val));
								}
							}	
						}
					}
					
					if( ! empty($data_insert_1) && ! empty($data_insert_2))
					{
						$key_1 = $this->MY_insert($relation['rel_attributes']['related_to'], $data_insert_1);
						if( ! empty($key_1))
						{
							// merge the foreign key for insert in base table
							$data_insert_2[$relation['rel_attributes']['foreign_key']] = $key_1;
							// do insert
							$key_2 = $this->MY_insert($this->base_table, $data_insert_2);
							if( ! empty($key_2))
							{
								return TRUE;
							}
							else
							{
								return FALSE;
							}
						}
						else
						{
							return FALSE;
						}
						//$this->MY_debug_query();
					}
					else
					{
						return FALSE;
					}
				}
				elseif($relation['rel_type'] === 'has_many') // if there is has_many relationship
				{
					$data_insert = array();
					foreach($this->input_post as $key => $val)
					{
						if(strlen($val) > 0)
						{
							$keys = explode('__',$key);
							if(isset($keys[1]))
							{
								if(strlen($val) > 0)
								{
									$id2 = explode(",",$val);
								}
							}
							else
							{
								if($this->_is_date($val))
								{
									// call the _check_date function
									$data_insert[$key] = $this->_check_date($val);
								}
								else
								{
									// force to keep the input lowercase
									$data_insert[$key] = trim(strtolower($val));
								}
							}	
						}
					}
					
					if( ! empty($data_insert))
					{
						$key = $this->MY_insert($this->base_table, $data_insert);
						if( ! empty($key))
						{
							if( ! empty($id2))
							{
								$data_insert = array($relation['rel_attributes']['related_fk'] => $key);
								return $this->MY_update($relation['rel_attributes']['related_to'], $relation['rel_attributes']['related_key'], $id2, $data_insert);
							}
							else
							{
								return TRUE;
							}
						}
						else
						{
							return FALSE;
						}
					}
				}
				elseif($relation['rel_type'] === 'many_many') // if there is many_many relationship
				{
					$data_insert = array();
					foreach($this->input_post as $key => $val)
					{
						if(strlen($val) > 0)
						{
							$keys = explode('__',$key);
							if(isset($keys[1]))
							{
								if(strlen($val) > 0)
								{
									$id2 = explode(",",$val);
								}
							}
							else
							{
								if($this->_is_date($val))
								{
									// call the _check_date function
									$data_insert[$key] = $this->_check_date($val);
								}
								else
								{
									// force to keep the input lowercase
									$data_insert[$key] = trim(strtolower($val));
								}
							}	
						}
					}
					
					if( ! empty($data_insert))
					{
						$key = $this->MY_insert($this->base_table, $data_insert);
						if( ! empty($key))
						{
							if( ! empty($id2))
							{
								//$data_insert = array($relation['rel_attributes']['related_fk'] => $key);
								//return $this->MY_update($relation['rel_attributes']['related_to'], $relation['rel_attributes']['related_key'], $id2, $data_insert);
								$s = 0;
								foreach($id2 as $link_key)
								{
									$data_insert = array(
										$relation['rel_attributes']['link_key_1'] => $key,
										$relation['rel_attributes']['link_key_2'] => $link_key
									);
									
									$res_key = $this->MY_insert($relation['rel_attributes']['link_to'], $data_insert);
									
									if( ! empty($res_key))
									{
										$s++;
									}
								}
								
								if(count($id2) == $s)
								{
									return TRUE;
								}
								else
								{
									return FALSE;
								}
							}
							else
							{
								return TRUE;
							}	
						}
						else
						{
							return FALSE;
						}
					}
				}
				else // if the relation belongs_to 
				{
					$data_insert = array();
					foreach($this->input_post as $key => $val)
					{
						if(strlen($val) > 0)
						{
							$keys = explode('__',$key);
							if(isset($keys[1]))
							{
								if(is_numeric($val))
								{
									// change the key attribute
									$data_insert[$relation['rel_attributes']['foreign_key']] = $val;
								}
							}
							else
							{
								if($this->_is_date($val))
								{
									// call the _check_date function
									$data_insert[$key] = $this->_check_date($val);
								}
								else
								{
									// force to keep the input lowercase
									$data_insert[$key] = trim(strtolower($val));
								}
							}	
						}
					}
					
					if( ! empty($data_insert))
					{
						$key = $this->MY_insert($this->base_table, $data_insert);
						if( ! empty($key))
						{
							return TRUE;
						}
						else
						{
							return FALSE;
						}
					}
				}
			}
		}
		else
		{
			$data_insert = array();
			foreach($this->input_post as $key => $val)
			{
				if(strlen($val) > 0)
				{
					if($this->_is_date($val))
					{
						// call the _check_date function
						$data_insert[$key] = $this->_check_date($val);
					}
					else
					{
						// force to keep the input lowercase
						$data_insert[$key] = trim(strtolower($val));
					}
				}
			}
			
			if( ! empty($data_insert))
			{
				$key = $this->MY_insert($this->base_table, $data_insert);
				if( ! empty($key))
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			
		}
	}
	
	private function _read()
	{
		$base_table_alias = "";
		$res = array();
		$i = 1;
		$n = ($this->with_total === TRUE ? 2 : 1);
		$pk = FALSE;
		
		//$base_table_alias = $this->MY_build_alias($this->base_table);
		$base_table_alias = $this->base_table;
		$from = $this->base_table." as ".$base_table_alias;
		
		if( ! empty($this->select[0]))
		{
			$this->_iterate_selected($this->select[0], $this->base_table, $base_table_alias, "", $this->select[1]);
			
			// checking if the selected fields have primary_key
			$pk = $this->_is_there_pk($this->select[0]);
		}
		else
		{	// if there is no selected field the you hae to generate it automatically
			$select_all = $this->MY_get_list_field($this->base_table);
			
			if(isset($this->unselect[0]))
			{
				// take out the unselected from the searching
				$select_all = array_diff($select_all,$this->unselect[0]);
			}
			
			// checking if the selected fields have primary_key
			$pk = $this->_is_there_pk($select_all);
			
			$this->_iterate_selected($select_all, $this->base_table, $base_table_alias, "");
		}
		
		// force to have primary_key in selected field
		if($pk === FALSE)
		{
			//$selected_pk = $this->MY_get_primary_key($this->base_table);
			$selected_pk = $this->base_table_key;
			$arr_selected_pk[] = $selected_pk; 
			$this->_iterate_selected($arr_selected_pk, $this->base_table, $base_table_alias, "", FALSE);
				
			$field_meta = $this->MY_get_field_types($this->base_table, $selected_pk);
			foreach($field_meta as $meta)
			{
				// merge the primary_key meta into the first row of fields_meta 
				$this->_fields_meta = array_merge(array($selected_pk => get_object_vars($meta)),$this->_fields_meta);
			}
		}
		
		while ($i <= $n) 
		{
			if( ! empty($this->relations))
			{
				$this->_table_alias = array(); //set base alias to empty array
				$this->_table_alias[] = $base_table_alias;
				foreach($this->relations as $relation)
				{
					// if relation type is belongs_to or has_one
					if($relation['rel_type'] === 'belongs_to' || $relation['rel_type'] === 'has_one')
					{
						// get the attribute and return array
						$attributes = $this->_set_relation_attribute($relation['rel_attributes'], TRUE);
						//print_r($attributes); die();
						foreach($attributes as $attribute)
						{
							if($i == 1) // iterate only when first looping 
							{
								if( ! empty($attribute['related_table_select'][0]))
								{
									$split  = $attribute['related_table_select'][1];
									$escape = $attribute['related_table_select'][2];
									
									if($split === TRUE) // display selected join table in split column
									{
										$this->_iterate_selected($attribute['related_table_select'][0], $attribute['related_table'], $attribute['related_table_alias'], "__", $escape);
									}
									else // display selected join table in merge column
									{
										// special condition if we want to display the fields into one merge field
										
										$str_sel = "concat(";
										$str_as  = "";
										$x = 0;
										foreach($attribute['related_table_select'][0] as $select)
										{
											$str_sel .= $attribute['related_table_alias'].".".$select;
											$str_as  .= $select;
											if($x < count($attribute['related_table_select'][0])-1)
											{
												$str_sel .= ",' - ',";
												$str_as  .= "00";
											}
											else
											{
												$str_sel .= ")";
											}
											
											$x++;
										}
										
										$this->MY_select($str_sel." as ".$attribute['related_table_alias']."__".$str_as, $escape);
										//$this->MY_select($str_sel." as ".$attribute['base_table_fk'], $escape);
										
										if($this->extjs_grid)
										{
											$meta = array(
												'name' => $attribute['related_table_alias']."__".$str_as,
												//'name' => $attribute['base_table_fk'],
												'type' => 'combo',
												'default' => '',
												'max_length' => 100,
												'primary_key' => 0,
												'related_to'  => $attribute['related_table'],
												'added_data'  => isset($attribute['added_data']) ? $attribute['added_data'] : '',
												'related_pk'  => $attribute['related_table_key'],
												'related_where' => $attribute['related_table_where'],
												'base_table_fk' => $attribute['base_table_fk'],
												'uneditable' => $attribute['uneditable']
											);
											
											$this->_fields_meta[$attribute['related_table_alias']."__".$str_as] = $meta;
											//$this->_fields_meta[$attribute['base_table_fk']] = $meta;
										}
									}
									
								}
								else // if related select not set then no need to auto iterate
								{
									//$select_all = $this->MY_get_list_field($attribute['related_table']);
									//$this->_iterate_selected($select_all, $attribute['related_table'], $attribute['related_table'], "__");
								}
							}
							
							// set where condition
							if( ! empty($attribute['related_table_where']))
							{
								$escape = ( ! empty($attribute['related_table_where'][1]) ? $attribute['related_table_where'][1] : TRUE);
								foreach($attribute['related_table_where'][0] as $key => $val)
								{
									$this->MY_where($attribute['related_table_alias'].".".$key, $val, $escape);
								}
							}
							
							// set table join
							$this->MY_join($attribute['related_table']." as ".$attribute['related_table_alias'], $base_table_alias.".".$attribute['base_table_fk']." = ".$attribute['related_table_alias'].".".$attribute['related_table_key']);	
						}
						
						//set the base_table_alias for the rest of the query
						//$base_table_alias .= ".";
					}
					else
					{
						/* if( ! empty($this->select[0]))
						{	
							$this->_iterate_selected($this->select[0], $this->base_table, "", "", $this->select[1]);
							
							foreach($this->select[0] as $field)
							{
								//checking if the selected field is primary_key
								if($this->MY_is_primary_key($this->base_table, $field))
								{
									$pk = TRUE;
								}
							}
							
							// force to have primary_key in selected field
							if($pk === FALSE)
							{
								$selected_pk = $this->base_table_key;
								$arr_selected_pk[] = $selected_pk; 
								$this->_iterate_selected($arr_selected_pk, $this->base_table, "", "", NULL, FALSE);
									
								$field_meta = $this->MY_get_field_types($this->base_table, $selected_pk);
								foreach($field_meta as $meta)
								{
									// merge the primary_key meta into the first row of fields_meta 
									$this->_fields_meta = array_merge(array($selected_pk => get_object_vars($meta)),$this->_fields_meta);
								}
							}
						}
						else
						{
							$select_all = $this->MY_get_list_field($this->base_table);
							
							$this->_iterate_selected($select_all, $this->base_table, "", "");
						} */
						
					}
				} // end of relations looping
			}
			
			$this->MY_from($from);
			
			if( ! empty($this->where))
			{
				foreach($this->where as $where)
				{
					$this->MY_where($base_table_alias.".".$where[0],$where[1],$where[2],$where[3]);
				}
			}
			
			if( ! empty($this->or_where))
			{
				foreach($this->or_where as $where)
				{
					$this->MY_or_where($base_table_alias.".".$where[0],$where[1],$where[2]);
				}
			}
			
			if( ! empty($this->like))
			{
				foreach($this->like as $like)
				{
					$this->MY_like($base_table_alias.".".$like[0],$like[1],$like[2]);
				}
			}
			
			if( ! empty($this->_search))
			{
				$field2 = array();
				$x = 0;
				$c = count($this->_fields);
				foreach($this->_fields as $field)
				{
					$related_field = explode('__',$field);
					
					if(isset($related_field[1]))
					{
						$related_field2 = explode('00',$related_field[1]);
						
						//condition if the field name are merge
						if(count($related_field2) > 1)
						{
							foreach($related_field2 as $val)
							{
								$field2[] = $related_field[0].".".$val; 
							}	
						}
						else
						{
							$field = $related_field[0].".".$related_field[1];
						}
					}
					else
					{	
						$field = $base_table_alias.".".$field;
					}
					
					if(empty($field2))
					{
						if($x == 0)
						{
							$where = '(upper('.$field.') LIKE \'%' .strtoupper($this->_search). '%\' ';
						}
						else
						{
							$where .=  'OR upper('.$field. ') LIKE \'%' .strtoupper($this->_search). '%\' ';
						}
						$x++;
					}
					else
					{
						//$this->debug_variable($field2);
						foreach($field2 as $field)
						{
							if($x == 0)
							{
								$where = '(upper('.$field.') LIKE \'%' .strtoupper($this->_search). '%\' ';
							}
							else
							{
								$where .=  'OR upper('.$field. ') LIKE \'%' .strtoupper($this->_search). '%\' ';
							}
							$x++;
						}
					}
					
					
				}
				$where .= ')';
				$this->db->where($where, NULL, FALSE);
			}
			
			if( ! empty($this->_order_by))
			{
				foreach($this->_order_by as $order_by)
				{
					$related_order = explode('__',$order_by[0]);
						
					if(isset($related_order[1]))
					{
						$related_order2 = explode('00',$related_order[1]);
						
						//condition if the field name are merge
						if(count($related_order2) > 1)
						{
							foreach($related_order2 as $val)
							{
								$this->MY_order_by($related_order2[0].".".$val,$order_by[1]);
							}	
						}
						else
						{
							$this->MY_order_by($related_order[0].".".$related_order[1],$order_by[1]);
						}
					}
					else
					{	
						$this->MY_order_by($base_table_alias.".".$order_by[0],$order_by[1]);
					}
				}
			}		
		
			if($i == 1)
			{
				if( ! empty($this->limit))
				{
					$this->MY_limit($this->limit[0],$this->limit[1]);
				}
			
				$data  = $this->db->get()->result_array();
				//$this->MY_debug_query();
			}
			else
			{
				$res['total'] = $this->db->count_all_results();
			}
			
			$i++; 
		}

		// iterate the output
		$res['data'] = $this->_iterate_data($data);
		
		// checking for has_many relation type
		if( ! empty($this->relations))
		{
			foreach($this->relations as $relation)
			{
				// condition if the relation_type is has many 
				// if it's has many relation type then we will process the output before pass to the view
				if($relation['rel_type'] == 'has_many')
				{
					$res['data'] = $this->_read_has_many($res['data'], $relation['rel_attributes']);
				}
				elseif($relation['rel_type'] == 'many_many')
				{
					$res['data'] = $this->_read_many_many($res['data'], $relation['rel_attributes']);
				}
			}
		}
		
		if($this->after_read)
		{	
			$res = $this->_run_after_callbacks('read', $this->after_read, $res);
		}
		
		return $res;
	}
	
	private function _read_many_many($init_data, $rel_attributes)
	{	
		// get the attribute and return an array
		$attributes = $this->_set_relation_attribute($rel_attributes);
		
		foreach($attributes as $attribute)
		{	
			if( ! empty($attribute['related_table_select'][0]))
			{
				$x = 0;
				$new_field = "";
				$selected  = "'(',	";
				foreach($attribute['related_table_select'][0] as $select)
				{
					if($x < count($attribute['related_table_select'][0])-1)
					{
						$new_field .= $select."00";
						$selected  .= $attribute['related_table_alias'].".".$select." ,' , ',"; 
					}
					else
					{
						$new_field .= $select;
						$selected  .= $attribute['related_table_alias'].".".$select.",')'"; 
					}
					$x++;
				}
				
				$selected = 'concat('.$selected.')';
				
				$new_field_1 = $attribute['related_table_alias']."__".$new_field."";
				$new_field = $attribute['related_table_alias']."__".$new_field;
				
				if($this->extjs_grid)
				{
					// don't forget to insert the meta
					$meta = array(
						'name' => $new_field_1,
						'type' => 'multiselect',
						'default' => '',
						'max_length' => 50,
						'primary_key' => 0,
						'disable_search' => TRUE,
						'related_to'  => $attribute['related_table'],
						'related_pk'  => $attribute['related_table_key'],
						'related_fk'  => $attribute['related_table_fk'],
						'related_where' => $attribute['related_table_where'],
						'base_table_fk' => $attribute['base_table_fk'],
						'base_table_pk' => $this->base_table_key,
						'rel_type'		=> 'many_many',
						'link_to'       => $attribute['link_to'],
						'link_key_1'    => $attribute['link_key_1'],
						'link_key_2'    => $attribute['link_key_2']
					);
					
					$this->_fields_meta[$new_field_1] = $meta;
				}
				
				$i = 0;
				foreach($init_data as $data)
				{
					$this->MY_select($selected." as ".$new_field, FALSE); // hardcode $escape = FALSE
					$this->MY_from($rel_attributes['link_to']." as ".$rel_attributes['link_alias']);
					$this->MY_join($attribute['related_table']." as ".$attribute['related_table_alias'], $rel_attributes['link_alias'].".".$rel_attributes['link_key_2']." = ".$attribute['related_table_alias'].".".$attribute['related_table_key']);	
					$this->MY_where($rel_attributes['link_alias'].".".$rel_attributes['link_key_1'], $data['id']);
					foreach($attribute['related_table_where'][0] as $w_key => $w_val)
					{
						$this->MY_where($rel_attributes['link_alias'].".".$w_key, $w_val);
					}
					
					$res = $this->MY_get()->result_array();
					
					$z = 0;
					$selected_val  = "";
					foreach($res as $key => $val)
					{
						if($z < count($res)-1)
						{
							$selected_val .= $val[$new_field]." ,"; 
						}
						else
						{
							$selected_val .= $val[$new_field]; 
						}
						$z++;
					}
					
					// merge the new field of data
					$init_data[$i] = array_merge($init_data[$i], array($new_field_1 => $selected_val));
					
					$i++;
				}
			}
		}
		
		return $init_data;
	}
	
	private function _read_has_many($init_data, $rel_attributes)
	{	
		// get the attribute and return array
		$attributes = $this->_set_relation_attribute($rel_attributes);
		
		foreach($attributes as $attribute)
		{
			if( ! empty($attribute['related_table_select'][0]))
			{
				$x = 0;
				$new_field = "";
				$selected  = "'(',	";
				foreach($attribute['related_table_select'][0] as $select)
				{
					if($x < count($attribute['related_table_select'][0])-1)
					{
						$new_field .= $select."00";
						$selected  .= $select." ,' , ',"; 
					}
					else
					{
						$new_field .= $select;
						$selected  .= $select.",')'"; 
					}
					$x++;
				}
				
				$selected = 'concat('.$selected.')';
				
				$new_field_1 = $attribute['related_table_alias']."__".$new_field."";
				$new_field = $attribute['related_table_alias']."__".$new_field;
				
				if($this->extjs_grid)
				{
					// don't forget to insert the meta
					$meta = array(
						'name' => $new_field_1,
						'type' => 'multiselect',
						'default' => '',
						'max_length' => 50,
						'primary_key' => 0,
						'disable_search' => TRUE,
						'related_to'  => $attribute['related_table'],
						'related_pk'  => $attribute['related_table_key'],
						'related_fk'  => $attribute['related_table_fk'],
						'related_where' => $attribute['related_table_where'],
						'base_table_fk' => $attribute['base_table_fk'],
						'base_table_pk' => $this->base_table_key,
						'rel_type'		=> 'has_many',
						'link_to'       => $attribute['link_to'],
						'link_key_1'    => $attribute['link_key_1'],
						'link_key_2'    => $attribute['link_key_2']
					);
					
					$this->_fields_meta[$new_field_1] = $meta;
				}
				
				$i = 0;
				foreach($init_data as $data)
				{
					$this->MY_select($selected." as ".$new_field, FALSE); // hardcode $escape = FALSE
					$this->MY_from($attribute['related_table']);
					$this->MY_where($attribute['related_table_fk'], $data['id']);
					$this->MY_where_array($attribute['related_table_where'][0]);
					$res = $this->MY_get()->result_array();
					
					$z = 0;
					$selected_val  = "";
					foreach($res as $key => $val)
					{
						if($z < count($res)-1)
						{
							$selected_val .= $val[$new_field]." ,"; 
						}
						else
						{
							$selected_val .= $val[$new_field]; 
						}
						$z++;
					}
					
					// merge the new field of data
					$init_data[$i] = array_merge($init_data[$i], array($new_field_1 => $selected_val));
					$i++;
				}
			}
			/* else
			{
				// auto selected field index 0 beside primary_key
				$fields = $this->MY_get_field_types($attribute['related_table']);
				
				$x = 0;
				foreach($fields as $field)
				{
					if($field->primary_key == 0)
					{
						if($x == 0)
						{
							$new_field = $field->name;
							$selected  = $field->name;
						}
						 
						$x++;
					}	
					
				}
			} */
		}
		
		return $init_data;
	}
	
	private function _update()
	{	
		$rel = array();
		$tmp_rel = array();
		$data_update_1 = array();
		
		$this->input_post = $this->_validate_input($this->input_post);
		
		foreach($this->input_post as $key0 => $val0)
		{
			foreach($val0 as $key => $val)
			{
				if(is_array($val))
				{
					//$val = serialize($val);
					$val = json_encode($val);
				}
				
				//if(strlen($val) > 0)
				//{
					$keys = explode('__',$key);
					if(isset($keys[1]))
					{
						if(in_array($keys[0],$tmp_rel))
						{
							$i = array_search($keys[0],$tmp_rel);
							$rel[$i][$key] = $val;
						}
						else
						{
							$tmp_rel[] = $keys[0];
							$rel[] = array($key => $val);
						}	
					}
					else
					{
						if($key == $this->base_table_key)
						{
							$id = $val;
						}
						else
						{
							if($this->MY_is_field_exists($key, $this->base_table))
							{
								if($this->_is_date($val))
								{
									$data_update_1[$key] = $this->_check_date($val);
								}
								elseif(in_array($key, $this->field_password))
								{
									$this->load->library('encrypt');
									$data_update_1[$key] = $this->encrypt->sha1($val.$this->config->item('encryption_key'));
								}
								else
								{
									$val = $this->_check_input_mode($this->input_mode,trim($val));
									$data_update_1[$key] = $this->_check_field_type($this->base_table, $key, $val);
								}
							}
						}
					}
					
				//}
			}	
			//print_r($data_update_1);die($id);
			if($this->MY_is_field_exists('modified_by', $this->base_table))
			{
				$instance = &get_instance();
				$data_update_1['modified_by'] = $this->_check_input_mode($this->input_mode,$instance->get_user_name());
			}
			
			if($this->MY_is_field_exists('modified_datetime', $this->base_table))
			{
				$instance = &get_instance();
				$data_update_1['modified_datetime'] = $instance->get_date_time();
			}
			
			if( ! empty($id))
			{
				if($this->before_update)
				{	
					$data_update_1 = $this->_run_before_callbacks('update', $this->before_update, $data_update_1);
				}
				
				$result_1 = $this->MY_update($this->base_table, $this->base_table_key, $id, $data_update_1);
				
				if($result_1 === TRUE)
				{
					if( ! empty($this->relations))
					{
						$relations = $this->relations;
						$total_rel = count($relations);
						$total_success = 0;
						$total_update = $total_rel;
						
						for($i = 0; $i < $total_rel; $i++)
						{
							if($relations[$i]['rel_type'] === 'has_one') // if the relationship is has_one
							{
								$set_update = array();
								foreach($rel[$i] as $key => $val)
								{
									$keys = explode('__',$key);
									if(isset($keys[1]))
									{
										$set_update[] = $relations[$i]['rel_attributes']['related_to'].".".$keys[1]." = '".$val."'";
									}
								}
								
								if( ! empty($set_update))
								{
									$result_2 = $this->MY_update_has_one($this->base_table, $this->base_table_key, $id, $relations[$i]['rel_attributes'], $set_update);
									if($result_2)	
									{
										$total_success++;
									}
								}
								else
								{
									$total_update--;
								}
								
							}
							elseif($relations[$i]['rel_type'] === 'has_many') // if the relationship is has_many
							{
								if( ! empty($rel[$i]))
								{
									foreach($rel[$i] as $key => $val)
									{
										$keys = explode('__',$key);
										if(isset($keys[1]))
										{
											if(strlen($val) > 0)
											{
												$id2 = explode(",",$val);
											}
										}
									}
								}
								
								// if the relation is not null
								if(isset($id2))
								{
									// reset the data first
									$data_update_2 = array($relations[$i]['rel_attributes']['related_fk'] => 0);
									if($this->MY_update($relations[$i]['rel_attributes']['related_to'], $relations[$i]['rel_attributes']['related_fk'], $id, $data_update_2))
									{
										if(! empty($id2))
										{
											// then update the data again
											$data_update_3 = array($relations[$i]['rel_attributes']['related_fk'] => $id);
											$result_3 = $this->MY_update($relations[$i]['rel_attributes']['related_to'], $relations[$i]['rel_attributes']['related_key'], $id2, $data_update_3);
											
											if($result_3)	
											{
												$total_success++;
											}
										}
										else
										{
											$total_update--;
										}
									}
								}
								else
								{
									$total_update--;
								}
							}
							elseif($relations[$i]['rel_type'] === 'many_many') // if the relationship is many_many
							{
								if( ! empty($rel[$i]))
								{
									foreach($rel[$i] as $key => $val)
									{
										$keys = explode('__',$key);
										if(isset($keys[1]))
										{
											if(strlen($val) > 0)
											{
												$id2 = explode(",",$val);
											}
										}
									}
								}
								
								// delete the data first
								if($this->MY_delete($relations[$i]['rel_attributes']['link_to'], $relations[$i]['rel_attributes']['link_key_1'], $id, $this->_del_update))
								{
									if( ! empty($id2))
									{
										// then insert the data again
										$s = 0;
										foreach($id2 as $link_key)
										{
											$data_insert = array(
												$relations[$i]['rel_attributes']['link_key_1'] => $id,
												$relations[$i]['rel_attributes']['link_key_2'] => $link_key
											);
											
											$pk = $this->MY_insert($relations[$i]['rel_attributes']['link_to'], $data_insert);
											
											if( ! empty($pk))
											{
												$s++;
											}
										}
										
										if(count($id2) == $s)
										{
											$total_success++;
										}
									}
									else
									{
										$total_update--;
									}
								}
							}
							else // if the relationship is belongs_to
							{	
								if( ! empty($rel[$i]))
								{
									$data_update_2 = array();
									foreach($rel[$i] as $key => $val)
									{
										$keys = explode('__',$key);
										if(isset($keys[1]))
										{
											$col = $this->MY_get_field_types($this->base_table, $relations[$i]['rel_attributes']['foreign_key']);
											if(isset($col[0]))
											{
												if($col[0]->type == "tinyint" || $col[0]->type == "int")
												{
													if(is_numeric($val))
													{
														// we have to change the key here
														$data_update_2[$relations[$i]['rel_attributes']['foreign_key']] = $val;
													}
												}
												elseif($col[0]->type == "varchar" || $col[0]->type == "char" || $col[0]->type == "text")
												{
													if(isset($relations[$i]['rel_attributes']['fk_char_numeric']) && $relations[$i]['rel_attributes']['fk_char_numeric'] == TRUE)
													{
														if(is_numeric($val))
														{
															$data_update_2[$relations[$i]['rel_attributes']['foreign_key']] = $val;
														}
													}
													else
													{
														$exp = explode("-",$val);
														if(!isset($exp[1]))
														{
															$data_update_2[$relations[$i]['rel_attributes']['foreign_key']] = $val;
														}
													}
												}
												else
												{
													$data_update_2[$relations[$i]['rel_attributes']['foreign_key']] = $val;
												}
											}
										}
									}
								}
								//$this->db->last_query();print_r($data_update_2);die();	
								if( ! empty($data_update_2))
								{
									$result_2 = $this->MY_update($this->base_table, $this->base_table_key, $id, $data_update_2);
										
									if($result_2)	
									{
										$total_success++;
									}
								}
								else
								{
									$total_update--;
								}
							}
						}
					
						if($total_update == $total_success)
						{
							continue;
						}
						else
						{
							return FALSE;
						}
					}
					else
					{
						continue;
					}
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
		
		return TRUE;
	}
	
	private function _delete()
	{
		// delete data on the base table
		$del = $this->MY_delete($this->base_table, $this->base_table_key, $this->_id, $this->_del_update);
		if($del)
		{
			if( ! empty($this->relations))
			{
				$x = 0;
				$a = 0;
				foreach($this->relations as $relation)
				{
					if($relation['rel_type'] === 'has_one' || $relation['rel_type'] === 'has_many') // if there is has_one or has_many relationship
					{
						// do the next process
						// get the attribute and return array
						$attributes = $this->_set_relation_attribute($relation['rel_attributes']);
						
						foreach($attributes as $attribute)
						{
							$del_2 = $this->MY_delete_has_many($this->base_table, $this->base_table_key, $this->_id, $attribute, $this->_del_update);
							
							if($del_2)
							{
								$a++;
							}
							//echo $this->db->last_query();die();
						}
						
					}
					elseif($relation['rel_type'] === 'many_many') // if there is many_many relationship
					{
						// delete the link table that related to the table
						$del_2 = $this->MY_delete($relation['rel_attributes']['link_to'], $relation['rel_attributes']['link_key_1'], $this->_id, $this->_del_update);
						
						if($del_2)
						{
							$a++;
						}
					}
					else // if the relation belongs_to 
					{
						$a++;
					}
					$x++;
				}
				
				if($x == $a)
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else // if there is no relation 
			{
				return TRUE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	private function _list()
	{
		$rel_type  = isset($this->input_post[0]['rel_type']) ? $this->input_post[0]['rel_type'] : "";
		
		if(isset($this->input_post[0]['where']))
		{	
			if($this->input->post('where_field'))
			{
				$prm = array($this->input->post('where_field') => $this->input->post('where'));
			}
			else
			{
				$prm = get_object_vars(json_decode($this->input_post[0]['where']));
			}
			foreach($prm as $p)
			{
				$where = $p;
			}
		}
		
		$condition = json_decode($this->input_post[0]['condition']);
		
		$table = base64_decode($this->input_post[0]['table_code']);
		$alias_1 = "a";
		$alias_2 = "b";
		$str_sel = "";
		
		$x = 0;
		$w = array();
		foreach($this->input_post[0]['list_fields'] as $field)
		{
			$sel = explode('__',$field);
			if(isset($sel[1]))
			{
				$str_sel .= ", concat(".str_replace("00"," , ' - ' , ",$alias_1.".".$sel[1]).") as ".$field;
				$w[] = $sel[1];
			}
			else
			{
				if($x == 0)
				{
					$str_sel .= $alias_1.".".$field." as ".$field;
				}
				else
				{
					$str_sel .= ", ".$alias_1.".".$field." as ".$field;
				}
			}
			$x++;
		}
		
		$this->MY_select($str_sel);
		$this->MY_from($table." as ".$alias_1);
		// iterate condition
		if(isset($condition[0]))
		{
			foreach($condition[0] as $key => $val){
				$this->MY_where($alias_1.".".$key, $val);
			}
		}
		
		if(isset($where) && ! empty($w))
		{
			foreach($w as $f)
			{
				$this->MY_like($f,$where);
			}
		}
		
		if($rel_type == "many_many" && isset($this->input_post[0]['link']))
		{
			$link   = json_decode($this->input_post[0]['link']);
			$rel_pk = $this->input_post[0]['related_pk'];
			$id 	= $this->input_post[0]['id'];
			
			if( ! empty($link) && ! empty($rel_pk) && ! empty($id))
			{
				$this->MY_join($link->link_to." as ".$alias_2, $alias_1.".".$rel_pk." = ".$alias_2.".".$link->link_key_2);
				$this->MY_where($alias_2.".".$link->link_key_1, $id);
				
				// condition if delete methode is update
				if($this->_del_update)
				{
					$this->MY_where($alias_2.".is_deleted", 0);
					$this->MY_where($alias_2.".is_active", 1);
				}
			}
		}
		
		$res = $this->MY_get()->result_array();
		//$this->MY_debug_query();
		
		echo json_encode($this->_iterate_data($res));
	}
	
	private function _update_bool()
	{
		
		//echo $this->input_post[0]['value'];
		if( ! empty($this->input_post[0]['id']))
		{
			if($this->MY_is_field_exists($this->input_post[0]['field'], $this->base_table))
			{
				if($this->input_post[0]['value'])
				{
					$value = 1;
				}
				else
				{
					$value = 0;
				}
				
				$result = $this->MY_update($this->base_table, $this->base_table_key, $this->input_post[0]['id'], array($this->input_post[0]['field'] => $value));
				//echo $this->db->last_query();	
				if($result)
				{
					echo $this->output->status_callback('json_success');
				}
				else
				{
					echo $this->output->status_callback('json_unsuccess');
				}
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
	
	private function _export_xls($res_data)
	{
		$this->load->library('pxl');
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()->setTitle('title')->setDescription("description");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
		$objPHPExcel->getActiveSheet()->setTitle($this->extjs_grid_title); 
		
		$x = 0;
		$col_start = "A";
		$col = $col_start;
		$col_array = array();
		$fields_array = array();
		
		$beginning = array('no' => array('primary_key' => 0));
		$meta = array_merge((array)$beginning, (array)$this->_fields_meta);
		
		foreach($meta as $key => $val)
		{
			if($val['primary_key'] == 0)
			{
				$header = str_replace("__"," - ",($key));
				
				$merge = explode('00',$header);
				if(isset($merge))
				{
					$header = str_replace("00"," ,",ucwords($header));
					if(isset($val['uneditable']))
					{
						$uneditable = $val['uneditable'];
					}
				}
				
				$header = str_replace("_"," ",ucwords($header));
				
				if(isset($this->extjs_grid_header[strtolower($header)]))
				{
					$header = $this->extjs_grid_header[strtolower($header)];
				}
				
				if(isset($this->extjs_grid_header_width[strtolower($header)]))
				{
					$width = $this->extjs_grid_header_width[strtolower($header)]/5;
				}
				else
				{
					$width = $this->_set_xls_col_width($header);
				}

				if($col == 'A')
				{
					$width = 5;
				}
				
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
				$objPHPExcel->getActiveSheet()->setCellValue($col."1", strtoupper($header));
				
				$col_array[] = $col;
				$fields_array[$x] = $key;
				
				$col++;
				$x++;
			}
		}
	
		$rnum = 1;
		$row = 2;
		foreach($res_data as $data)
		{
			for($i = 0; $i <= count($col_array)-1; $i++)
			{
				if($i == 0)
				{
					$objPHPExcel->getActiveSheet()->setCellValue($col_array[$i].$row, $rnum);
				}
				else
				{
					$objPHPExcel->getActiveSheet()->setCellValue($col_array[$i].$row,(isset($data[$fields_array[$i]]) ? $data[$fields_array[$i]] : "N/A"));
				}
			}
			
			$rnum++;
			$row++;
		}
		
		$col = $col_array[count($col_array)-1];
		$row = $row - 1;
		
		$sharedStyle1 = new PHPExcel_Style();
		$sharedStyle1->applyFromArray(
			array('borders' => array(
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		));

		$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $col_start.'1:'.$col.'1');
		
		$objPHPExcel->getActiveSheet()->getStyle($col_start.'1:'.$col.'1')->applyFromArray(
			array(
				'fill'	=>array(
					'type'	=> 'solid',
					'border' => 1,
					'color'	=>	array(
						'rgb' => 'D99795'
						)
					),
				'alignment'	=> array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'wrapText' => true
					),
				'borders' => array(
					 'outline' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('argb' => 'balck'),
					 )
				)
				
			)	
		);
		
		$objPHPExcel->getActiveSheet()->getStyle($col_start.'1:'.$col.'1')->getAlignment()->setWrapText(true); 
		$objPHPExcel->getActiveSheet()->getStyle($col_start.'1:'.$col.'1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setAutoFilter($col_start.'1:'.$col.'1');
		
		$objPHPExcel->getActiveSheet()->getStyle($col_start.'1:'.$col.$row)->getFont()->setName('Arial');
		$objPHPExcel->getActiveSheet()->getStyle($col_start.'1:'.$col.$row)->getFont()->setSize(8);
		
		$objPHPExcel->getActiveSheet()->getStyle($col_start.'1:'.$col.$row)->applyFromArray(
			array(
				'alignment'	=> array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'wrapText' => true
				)
			)	
		);
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='.(! is_null($this->extjs_grid_title) ? str_replace(" ","_",$this->extjs_grid_title) : 'export').'.xls');
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		unset($res_data);
		unset($data);
		
	}
	
	function _check_field_type($table, $key, $val)
	{
		$col = $this->MY_get_field_types($table, $key);
		if($col[0]->type == "tinyint" || $col[0]->type == "int" || $col[0]->type == "bigint")
		{
			if(is_numeric($val))
			{
				return $val;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return $val;
		}
	}
	
	/**
	 * 
	 * Get action code function
	 * @param ($action_name string)
	 * @return string
	 *
	 */
	private function _get_action_code($action_name)
	{	
		if( ! empty($action_name) && in_array($action_name, $this->_action) )
		{
			$action_code = array_search($action_name, $this->_action);
		}
		else
		{
			$action_code = 0;
		}
		
		return $action_code;
	}
	
	/**
	 * 
	 * Check the relation attributes
	 * @param ($attributes array)
	 * @return boolean
	 *
	 */
	private function _check_relation_attributes($attributes)
	{
		return in_array($attributes,$this->_rel_attributes);
	}
	
	/**
	 * 
	 * Build the json output
	 * @param ($total int, $data array)
	 * @return json format
	 *
	 */
	private function _build_json_output($total, $data)
	{
		echo '{success: true, total: '.$total.', data: '.utf8_encode(json_encode($data)).'}';
	}
	
	/**
	 * 
	 * Function for iterate data
	 * @param ($data array)
	 * @return array
	 *
	 */
	private function _iterate_data($data)
	{
		$i = $this->_start + 1;
		$output = array();
		foreach($data as $key => $row) 
		{
			// give the first uppercase in array data
			// $row = array_map('ucfirst', $row);
			
			$rows = array();
			foreach($row as $key2 => $val)
			{
				if(is_array(json_decode($val)))
				{
					$rows[$key2] = (array)$val;
				}
				else
				{
					$rows[$key2] = $val;
				}
			}
			
			$output[] = array_merge(array('rnum' => $i), $rows);
			
			$i++;
		}
		
		return $output;
	}
	
	/**
	 * 
	 * Function for before update
	 * @param ($data array)
	 * @return array
	 *
	 */
	private function _before_update($data)
	{
		if( ! is_null($this->bu_function))
		{
			$data = array_map($this->bu_function, $data);
		}
		
		return $data;
	}
	
	/**
	 * 
	 * Check if there is primary_key in the fields
	 * @param ($fields string)
	 * @return string (primary_key of the table)
	 *
	 */
	private function _is_there_pk($fields)
	{
		$pk = FALSE;
		foreach($fields as $field)
		{
			if($this->MY_is_primary_key($this->base_table, $field))
			{
				$pk = TRUE;
			}
		}
		return $pk;
	}
	
	/**
	 * 
	 * Function for unset data
	 * @param ($data string)
	 * @return void unset
	 *
	 */
	private function _unset_data($data)
	{
		unset($data);
	}
	
	/**
	 * 
	 * Function for checking whether the value given is date or not
	 * @param ($val string)
	 * @return boolean
	 *
	 */
	private function _is_date($val)
	{
		$preg_exp = '"[0-9][0-9]-[A-Z][a-z][a-z]-[0-9][0-9][0-9][0-9]"'; 
		$preg_exp3 = '"[0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]"'; 
		$preg_exp2 = '"/(\d{2})\s(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)\s(\d{4})\s(\d{2}):(\d{2})/i"';
		if(preg_match($preg_exp, $val)) 
		{
			return TRUE;
		}
		else
		{
			if(preg_match($preg_exp3, $val))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	
	/**
	 * 
	 * Function for date format
	 * @param ($val string)
	 * @return void format date
	 *
	 */
	private function _check_date($val)
	{
		$date = explode(" ", $val);
		if(isset($date[1]))
		{
			return $this->_convert_date($val, 'Y-m-d H:i:s');
		}
		else
		{
			return $this->_convert_date($val, 'Y-m-d');
		}
	}
	
	/**
	 * 
	 * Function for converting date
	 * @param ($val string, $format string)
	 * @return void date format
	 *
	 */
	private function _convert_date($val, $format)
	{
		// only php 5.3 and up
		$date = new DateTime($val);
		return $date->format($format);
	}
	
	/**
	 * 
	 * Get meta data of the table
	 * @param ($table_name string, $alias string, $field string)
	 * @return array
	 *
	 */
	private function _get_meta($table_name, $alias, $field)
	{
		// get the meta of the fields
		$field_meta = $this->MY_get_field_types($table_name, $field);
		foreach($field_meta as $meta)
		{
			// change the object into array variable
			$this->_fields_meta[$alias.$field] = get_object_vars($meta);
		}
	}
	
	/**
	 * 
	 * Function for iterate selected data
	 * @param ($data array, $table_name string, $alias string, $postfix string, $escape bool, $meta bool)
	 * @return array of the meta data
	 *
	 */
	private function _iterate_selected($data, $table_name, $alias, $postfix, $escape = NULL, $meta = TRUE)
	{
		foreach($data as $field)
		{
			$alias1 = "";
			$alias2 = "";
			
			if( ! empty($alias))
			{
				$alias1 = $alias.".";
			}
			
			if( ! empty($postfix))
			{
				$alias2 = $alias.$postfix;
			}
			
			// call this->db->select()
			$this->MY_select($alias1.$field." as ".$alias2.$field, $escape);
			if($this->extjs_grid && $meta == TRUE)
			{
				// set the meta data of the table
				$this->_get_meta($table_name, $alias2, $field);
			
			}
		}
	}
	
	/**
	 * 
	 * Set relation attribute use for setting the attribute of the relation table
	 * @param ($attributes string)
	 * @return array
	 *
	 */
	private function _set_relation_attribute($attributes, $set_query = TRUE)
	{
		$attributes_output = array();
		
		if(array_key_exists('primary_key',$attributes))
		{
			if($this->MY_is_primary_key($this->base_table, $attributes['primary_key']))
			{
				$base_table_pk = $attributes['primary_key'];
			}
			else
			{
				$base_table_pk = $this->MY_get_primary_key($this->base_table);
			}
		} 
		else
		{	
			$base_table_pk = $this->MY_get_primary_key($this->base_table);
		}
		
		if(array_key_exists('foreign_key',$attributes))
		{
			if($this->MY_is_field_exists($attributes['foreign_key'],$this->base_table))
			{
				//echo $attributes['foreign_key'];
				$base_table_fk = $attributes['foreign_key'];
			}
			else
			{
				//echo $attributes['foreign_key'];
				$fk = $this->MY_build_foreign_key($related_table_origin);
				
				if($this->MY_is_field_exists($fk,$this->base_table))
				{
					$base_table_fk = $fk;
				}
				else
				{
					echo 'You must define the \"Foreign Key\" for the base table';
					die();
				}
			}
		}
		else
		{
			$base_table_fk = "";
		}
		
		if(array_key_exists('related_to',$attributes))
		{
			$related_table = $attributes['related_to'];
		}
		else
		{
			echo 'You must define the \"related_to attribute\" ';
			die();
		}
		
		if(array_key_exists('related_key',$attributes))
		{
			$related_table_key = $attributes['related_key'];
		} 
		else
		{	
			$related_table_key = $this->MY_get_primary_key($related_table);
		}
		
		if(array_key_exists('related_fk',$attributes))
		{
			$related_table_fk = $attributes['related_fk'];
		}
		
		if(array_key_exists('related_alias',$attributes))
		{
			if( ! in_array($attributes['related_alias'],$this->_table_alias))
			{
				$related_table_alias = $attributes['related_alias'];
			}
			else
			{
				$postfix = count(array_keys($this->_table_alias, $attributes['related_alias']));
				$related_table_alias = $attributes['related_alias']."_".$postfix;
			}
			
			$related_table_alias_origin = $attributes['related_alias'];
		}
		else
		{
			$build_alias = $this->MY_build_alias($related_table);
			
			if( ! in_array($build_alias,$this->_table_alias))
			{
				$related_table_alias = $build_alias;
			}
			else
			{
				$postfix = count(array_keys($this->_table_alias, $build_alias));
				$related_table_alias = $build_alias."_".$postfix;
			}
			
			$related_table_alias_origin = $build_alias;
		}
		
		$this->_table_alias[] = $related_table_alias_origin;
		
		
		if(array_key_exists('related_select',$attributes))
		{
			$split    = ( isset($attributes['related_select'][1]) ? $attributes['related_select'][1] : TRUE); //default true
			$escape   = ( isset($attributes['related_select'][2]) ? $attributes['related_select'][2] : TRUE); //default true
			$related_table_select = array($attributes['related_select'][0], $split, $escape);
		}
		else
		{
			$related_table_select = "";
		}
		
		if(array_key_exists('uneditable',$attributes))
		{
			$uneditable = $attributes['uneditable'];
		}
		
		if(array_key_exists('added_data',$attributes))
		{
			$added_data = $attributes['added_data'];
		}
		
		
		if(array_key_exists('related_where',$attributes))
		{
			$related_table_where = $attributes['related_where'];
		}
		else
		{
			$related_table_where = "";
		}
		
		// special for many_many relationship
		if(array_key_exists('link_to',$attributes))
		{
			$link_to = $attributes['link_to'];
		}
		
		if(array_key_exists('link_key_1',$attributes))
		{
			$link_key_1 = $attributes['link_key_1'];
		}
		
		if(array_key_exists('link_key_2',$attributes))
		{
			$link_key_2 = $attributes['link_key_2'];
		}
		
		$attribute['link_to']     = isset($link_to)   ? $link_to : "";
		$attribute['link_key_1']  = isset($link_key_1) ? $link_key_1 : "";
		$attribute['link_key_2']  = isset($link_key_2) ? $link_key_2 : "";
			
		//$attribute['base_table_pk']        = isset($base_table_pk) ? $base_table_pk : "";
		$attribute['base_table_fk']        = isset($base_table_fk) ? $base_table_fk : "";
		$attribute['related_table']        = isset($related_table) ? $related_table : "";
		$attribute['related_table_alias']  = isset($related_table_alias) ? $related_table_alias : "";
		$attribute['related_table_key']    = isset($related_table_key)   ? $related_table_key : "";
		$attribute['related_table_fk']     = isset($related_table_fk)    ? $related_table_fk : "";
		$attribute['related_table_select'] = isset($related_table_select)? $related_table_select : "";
		$attribute['related_table_where']  = isset($related_table_where) ? $related_table_where : "";
		$attribute['uneditable']  		   = isset($uneditable) ? $uneditable : "";
		$attribute['added_data']  		   = isset($added_data) ? $added_data : "";
		
		
		$attributes_output[] = $attribute;
		
		// return array
		return $attributes_output;
	}
	
	/**
	 * 
	 * Show error message
	 * @param ($msg string)
	 * @return string
	 *
	 */
	private function _show_error($msg)
	{
		print($msg);
		die();
	}
	
	/**
	 * 
	 * Function just for debugging
	 * @param ($var string)
	 * @return void
	 *
	 */
	function debug_variable($var)
	{
		print_r($var)."\n";
		die('debug!!!!');
	}	
	
	 /**
     * Run the before_ callbacks, each callback taking a $data
     * variable and returning it
     */
    private function _run_before_callbacks($type, $configs = array(), $params = array())
    {
        $name = 'before_' . $type;
		$data = FALSE;
		foreach($configs as $config)
		{
			if($config['type'] == 'internal')
			{
				if(method_exists($this, $config['method']))
				{
					if($data === FALSE)
					{
						$data = call_user_func_array(array($this, $config['method']), array($params));
					}
					else
					{
						$data = call_user_func_array(array($this, $config['method']), array($data));
					}
				}
				else
				{
					$data = $params;
				}
			}
			else
			{
				// only php >= 5.3
				if(function_exists('get_class'))
				{
					$instance = &get_instance();
					if($data === FALSE)
					{
						$data = call_user_func_array(array(get_class($instance), $config['method']), array($params, $config['extra_params']));
					}
					else
					{
						$data = call_user_func_array(array(get_class($instance), $config['method']), array($data, $config['extra_params']));
					}
				}
			}
		}
		//print_r($data);die('d');
        return $data;
    }
    
    /**
     * Run the after_ callbacks, each callback taking a $data
     * variable and returning it
     */
    /*private function _run_after_callbacks($type, $params = array())
    {
        $name = 'after_' . $type;
        
        if (!empty($name))
        {
            $data = (isset($params[0])) ? $params[0] : FALSE;
        
            foreach ($this->$name as $method)
            {
                $data = call_user_func_array(array($this, $method), array($params));
            }
        }
        
        return $data;
    }*/
	
	private function _run_after_callbacks($type, $configs = array(), $params = array())
    {
        $name = 'after_' . $type;
		$data = FALSE;
		foreach($configs as $config)
		{
			if($config['type'] == 'internal')
			{
				if(method_exists($this, $config['method']))
				{
					if($data === FALSE)
					{
						$data = call_user_func_array(array($this, $config['method']), array($params));
					}
					else
					{
						$data = call_user_func_array(array($this, $config['method']), array($data));
					}
				}
				else
				{
					$data = $params;
				}
			}
			else
			{
				// only php >= 5.3
				if(function_exists('get_class'))
				{
					$instance = &get_instance();
					if($data === FALSE)
					{
						$data = call_user_func_array(array(get_class($instance), $config['method']), array($params, $config['extra_params']));
					}
					else
					{
						$data = call_user_func_array(array(get_class($instance), $config['method']), array($data, $config['extra_params']));
					}
				}
			}
		}
		
        return $data;
    }
	
	private function _check_input_mode($mode,$val)
	{
		if($mode == 'upper')
		{
			$val = strtolower($val);
			$val = strtoupper($val);
		}
		elseif($mode == 'lower')
		{
			$val = strtolower($val);
		}
		elseif($mode == 'ucwords')
		{
			$val = strtolower($val);
			$val = ucwords($val);
		}
		else
		{
			$val = $val;
		}
		
		return $val;
	}
	
	protected function upper_format($params = array())
	{
		return array_map('strtoupper', $params);
	}
	
	private function _set_xls_col_width($str)
	{
		$length = strlen($str);
		
		if($length > 0 && $length <= 10)
		{
			$width = 15;
		}
		elseif($length > 10 && $length <= 20)
		{
			$width = 20;
		}
		else
		{
			$width = 30;
		}
		
		return $width;
	}
}