<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Date_indonesia {

	function format_date($date = null, $display_hari = TRUE)
	{
		//buat array nama hari dalam bahasa Indonesia dengan urutan 1-7
		$array_hari = array(1=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
		//buat array nama bulan dalam bahasa Indonesia dengan urutan 1-12
		$array_bulan = array(1=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus',
		'September','Oktober', 'November','Desember');
		if($date == null) {
		//jika $date kosong, makan tanggal yang diformat adalah tanggal hari ini
		$hari = $array_hari[date('N')];
		$tanggal = date ('j');
		$bulan = $array_bulan[date('n')];
		$tahun = date('Y');
		} else {
		//jika $date diisi, makan tanggal yang diformat adalah tanggal tersebut
		$date = strtotime($date);
		$hari = $array_hari[date('N',$date)];
		$tanggal = date ('j', $date);
		$bulan = $array_bulan[date('n',$date)];
		$tahun = date('Y',$date);
		}
		if($display_hari) {
			$formatTanggal = $hari . ", " . $tanggal ." ". $bulan ." ". $tahun;
		}else {
			$formatTanggal = $tanggal ." ". $bulan ." ". $tahun;
		}
		
		
		return $formatTanggal;
	}
	
	function get_month()
	{
		$month = array(
			'1' => 'JANUARI',
			'2' => 'FEBRUARI',
			'3' => 'MARET',
			'4' => 'APRIL',
			'5' => 'MEI',
			'6' => 'JUNI',
			'7' => 'JULI',
			'8' => 'AGUSTUS',
			'9' => 'SEPTEMBER',
			'10' => 'OKTOBER',
			'11' => 'NOVEMBER',
			'12' => 'DESEMBER'
		);
		
		return $month;
	}
	
	function get_diff($date,$date2 = '')
	{
		if(! empty($date2))
		{
			$now = new DateTime($date2);
		}
		else
		{
			$now = new DateTime();
		}
		
		$_date = new DateTime($date);
		
		if($_date <= $now)
		{
			$year = $now->diff($_date)->format("%Y Tahun, %m Bulan, %d Hari" );
			$val = str_replace('00 Tahun,', '', $year);
			$val = str_replace(', 0 Bulan,', '', $val);
			$val = str_replace(', 0 Hari', '', $val);
			return $val;
		}
		else
		{
			return '';
		}
	}
	
	function get_diff_datetime($date,$date2 = '')
	{
		if(! empty($date2))
		{
			$now = new DateTime($date2);
		}
		else
		{
			$now = new DateTime();
		}
		
		$_date = new DateTime($date);
		
		if($_date <= $now)
		{
			$year = $now->diff($_date)->format("%Y Tahun, %m Bulan, %d Hari, %H Jam, %i Menit, %s Detik" );
			
			$val = str_replace('00 Tahun,', '', $year);
			$val = str_replace(' 0 Bulan,', '', $val);
			$val = str_replace(' 0 Hari,', '', $val);
			$val = str_replace(' 00 Jam,', '', $val);
			$val = str_replace(' 00 Menit,', '', $val);
			$val = str_replace(' 00 Detik', '', $val);
			return $val;
		}
		else
		{
			return '';
		}
	}
}