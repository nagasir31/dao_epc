<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Email_builder {
	
	protected $email_protocol = 'mail';
	
	public function send_email($prm)
	{
		$this->load->library('email');
		
		$config['protocol']  = $this->email_protocol;
		$config['charset']   = 'iso-8859-1';
		$config['wordwrap']  = TRUE;
		$config['mailtype']	 = isset($prm['mailtype']) ? $prm['mailtype'] : 'html';
		$config['validate']	 = TRUE;
		$config['priority']	 = isset($prm['priority']) ? $prm['priority'] : 3;
		$config['crlf']	 	 = "\r\n";
		$config['newline']	 = "\r\n";

		$this->email->initialize($config);
		
		$default = array(
			'subject'   => trim($prm['subject']),
			'content'   => trim($prm['content']), 
			'from_name' => $prm['from_name'],
			'from_email' => $prm['from_email']
		);
		
		$i = 0;		   
		foreach($prm['to'] as $name => $addrs)
		{
			$this->email->clear();
			$this->email->from($default['from_email'], $default['from_name']);
			$this->email->subject($default['subject']);
			$this->email->to($addrs);
			//echo $addrs;die();
			
			if(isset($prm['attachment']))
			{
				foreach($prm['attachment'] as $attach)
				{
					$this->email->attach($attach);
				}
			}
			
			$default['to_name'] = $name;
				   
			$msg = $this->_build_email($default);
			
			$this->email->message($msg);
		
			if($this->email->send())
			{
				$i++;
			}
			
			sleep(3);
		}
		
		if($i > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		//return $i;
	}
	
	private function _build_email($prm)
	{
		$msg = "
		<html>
			<head>
			  <title>{$prm['subject']}</title>
			</head>
			<body style='font-family: Arial; font-size: 12px;color:#15428B;style=margin:10px 0px 10px 0px'>
				<p>Dear {$prm['to_name']},</p> 
				<p>
					{$prm['content']}
				</p>
				<br/>
				<p>Regards,</p> 
				<p>{$prm['from_name']}</p> 
			</body>
		</html>
		";

		return $msg;
	}
}