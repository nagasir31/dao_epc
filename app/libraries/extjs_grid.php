<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Extjs_Grid {
	
	public $success = TRUE;
	public $total   = TRUE;
	public $data    = TRUE;
	
	public $title   = NULL;
	public $meta	   = NULL;
	public $total_prop = 0;
	public $root  	   = array();
	public $fields     = array();
	public $col_model  = array(); 
	public $form 	   = FALSE;
	public $relations  = FALSE;
	public $type_grid  = NULL;
	public $paging	   = NULL;
	public $pk		   = NULL;
	public $bool_col   = array();
	public $bool_inactive_action_col = array();
	public $field_pwd  = array();
	public $field_eml  = array();
	public $field_yr   = array();
	public $uniq_name  = '';
	public $uniq_field = array();
	public $show_fields   = array();
	public $unshow_fields = array();
	public $show_cols   = array();
	public $unshow_cols = array();
	public $field_alphanum = array();
	public $do_spot = FALSE;
	public $header = array();
	public $header_width = array();
	public $hide = array();
	public $show_pk = FALSE;
	public $base_table = NULL;
	public $renderer_cols = array();
	public $spinner_field = array();
	public $field_duplicate_entry = array();
	
	private $_string_data_types   = array('char','varchar','text','tinytext','longtext');
	private $_numeric_data_types  = array('int','tinyint','smallint','mediumint','bigint','float','decimal','double');
	private $_datetime_data_types = array('date','time','datetime','timestamp');
	
	//The class constractor
	function __construct()
	{
		$this->CI =& get_instance(); 
		$this->db =& $this->CI->db;
		$this->CI->load->helper(array('url','inflector'));
	}
	
	function build_json() 
	{
		echo '{success: '.$this->success.', total: '.$this->total.', data: '.utf8_encode(json_encode($this->data)).'}';
	}
	
	function build_json_meta($table = '') 
	{
		if(isset($this->meta) && ! empty($this->meta))
		{
			$form_fields  = $this->_build_form();
			$store_fields = $this->_build_store();
			$col_model    = $this->_build_col_model();
			$title		  = is_null($this->title) ? str_replace("_"," ",$table) : $this->title;
			$baseConfig[] = array(
				'title' => ucwords($title),
				'do_spot' => $this->do_spot
			);
			
			
			/* echo '{ success: true, total: '.$this->total.', data: '.utf8_encode(json_encode($this->data)).', 
			"metaData":{"totalProperty": "total", "root": "data", "idProperty": "'.$this->pk.'", successProperty: "success",  messageProperty: "message", 
				"baseConfig": '.(json_encode($baseConfig)).',
				"fields": '.utf8_encode(json_encode($store_fields)).',
				"colModel": '.utf8_encode(json_encode($col_model)).',
				"formFields": '.utf8_encode(json_encode($form_fields)).'} }'; */
				
			echo '{ success: true, total: '.$this->total.', data: '.utf8_encode(json_encode($this->data)).', 
			"metaData":{"totalProperty": "total", "root": "data", "idProperty": "'.$this->pk.'", successProperty: "success",  messageProperty: "message", 
				"baseConfig": '.(json_encode($baseConfig)).',
				"fields": '.utf8_encode(json_encode($store_fields)).',
				"colModel": '.utf8_encode(json_encode($col_model)).',
				"formFields": '.utf8_encode(json_encode($form_fields)).'} }';
		}
		else
		{
			show_error('The \"meta of the grid \" must be define !');
		}
	}
	
	private function _generate_store_date_format($date_type)
	{
		switch($date_type)
		{
			case 'date':
				return 'Y-m-d';
				break;
			case 'time':
				return 'H:i:s';
				break;
			case 'datetime':
				return 'Y-m-d H:i:s';
				break;
			default:
				return 'Y-m-d';
				break;
		}
	}
	
	private function _generate_column_date_format($date_type)
	{
		switch($date_type)
		{
			case 'date':
				return 'd-M-Y';
				break;
			case 'time':
				return 'H:i:s';
				break;
			case 'datetime':
				return 'd-M-Y H:i:s';
				break;
			default:
				return 'd-M-Y';
				break;
		}
	}
	
	private function _build_store()
	{
		$store_fields   = array();
		$store_fields[] = array('name' => 'rnum'); 
		foreach($this->meta as $key => $val)
		{
			$type = $this->_generate_store_type($val['type']);
			
			if($type == 'int' && $val['max_length'] == 1)
			{
				$type = 'bool';
			}
			
			$fields = array();
			$fields['name'] = $key;
			$fields['type'] = $type;
			
			if($type == 'date')
			{
				$fields['dateFormat'] = $this->_generate_store_date_format($val['type']);
			}
			
			$store_fields[] = $fields;
		}
		
		return $store_fields;
	}

	private function _generate_store_type($val_type)
	{
		if(in_array($val_type,$this->_string_data_types))
		{
			return 'string';
		}
		elseif(in_array($val_type,$this->_numeric_data_types))
		{
			if($val_type == 'float')
			{
				return 'float';
			}
			elseif($val_type == 'decimal')
			{
				return 'decimal';
			}
			elseif($val_type == 'double')
			{
				return 'double';
			}
			else
			{
				return 'int';
			}
		}
		elseif(in_array($val_type,$this->_datetime_data_types))
		{
			return 'date';
		}
		else
		{
			return 'string';
		}
	}
	
	private function _build_form()
	{
		$form_fields   = array();
		$pwd_count = 0;
		if($this->form)
		{
			//print_r($this->meta);die();
			foreach($this->meta as $key => $val)
			{
				if(!in_array($key, $this->hide))
				{
					// reset array
					$fields = array(); 
					$label = str_replace("__"," - ",($key));
					
					$merge = explode('00',$label);
					if(isset($merge))
					{
						$label = str_replace("00"," ,",ucwords($label));
					}
					
					$label = str_replace("_"," ",ucwords($label));
					
					//change the label
					if(isset($this->header[strtolower($label)]))
					{
						$label = $this->header[strtolower($label)];
					}
					
					if($val['primary_key'])
					{
						if($this->show_pk)
						{
							$xtype = strtolower($this->_generate_xtype($val['type']));
							$fields['vtype'] = 'uniquefield';
							$fields['tbid']  = $this->base_table;
							$fields['showPk'] = true;
						}
						else
						{
							$xtype = 'hidden';
						}
						//$xtype = ($this->show_pk ? ! $val['primary_key'] :  $val); 
					}
					elseif($val['type'] == 'combo') // if type data is dropdown combobox
					{
						$xtype = $val['type'];
						$fields['triggerAction'] = 'all';
						$fields['mode']  = 'remote';
						$fields['store'] = json_encode(array($val['related_pk'],$key));
						$fields['addedData']     = $val['added_data'];
						$fields['related_to']    = json_encode(array(base64_encode($val['related_to'])));
						$fields['related_where'] = json_encode($val['related_where']);
						$fields['hiddenName']    = $key;
						$fields['hiddenValue']   = $val['related_pk'];
						$fields['valueField']    = $val['related_pk'];
						$fields['displayField']  = $key;
						$fields['selectOnFocus'] = TRUE;
						$fields['typeAhead']  = TRUE;
						$fields['editable']   = FALSE;
						$fields['emptyText']  = 'Choose One';
						$fields['lazyRender'] = TRUE;
					}
					elseif($val['type'] == 'multiselect') // if type data is multiselect
					{
						$keys = explode("__",$key);
						$xtype = 'itemselector';
						$fields['imagePath']  = base_assets_url().'js/ext/plugins/multiSelect/images';
						$fields['autoScroll'] = TRUE;
						$fields['related_to'] = json_encode(array(base64_encode($val['related_to'])));
						$fields['store1'] 	  = json_encode(array($val['related_pk'],$key));
						$fields['related_where'] = json_encode($val['related_where']);
						$fields['store'] 		 = json_encode(array($val['related_pk'],$keys[1]));
						$fields['store2'] 		 = json_encode(array(1,2));
						$fields['base_table_pk'] = $val['base_table_pk'];
						$fields['related_fk'] = $val['related_fk'];
						$fields['related_pk'] = $val['related_pk'];
						$fields['rel_type']   = $val['rel_type'];
						$fields['link']       = json_encode(array('link_to' => $val['link_to'], 'link_key_1' => $val['link_key_1'], 'link_key_2' => $val['link_key_2']));
					}
					else
					{
						$xtype = strtolower($this->_generate_xtype($val['type']));
					}
					
					
					if($xtype == 'numberfield' && $val['max_length'] == 1)
					{
						$fields['id'] = $key;
						$xtype = 'radiogroup';
					}
					
					// check if there is a password field
					$pwd_mode = FALSE;
					if(in_array($key, $this->field_pwd))
					{			
						$pwd_count++;
						$fields['id'] = "pwd".$pwd_count."";
						$fields['name'] = $key;
						$fields['fieldLabel'] = $label;
						$fields['inputType']  = 'password';
						$fields['minLength']  = 5;
						$fields['allowBlank'] = TRUE;
						$fields['enableKeyEvents'] = TRUE;
						$pwd_mode = TRUE;		
					}
					elseif(array_search($key, $this->field_yr))
					{			
						$fields['xtype'] = 'combo';
						$fields['fieldLabel'] = $label;
						$fields['triggerAction'] = 'all';
						$fields['mode']  = 'local';
						$fields['store'] = $key;
						$fields['year']  = TRUE;
						$fields['yearType'] = array_search($key, $this->field_yr);
						$fields['name'] = $key;
						$fields['valueField']    = 'id';
						$fields['displayField']  = $key;
						$fields['selectOnFocus'] = TRUE;
						$fields['typeAhead']  = TRUE;
						$fields['editable']   = FALSE;
						$fields['emptyText']  = 'Choose Year';
						$fields['lazyRender'] = TRUE;
					}
					else
					{
						$fields['name']  = $key;
						$fields['xtype'] = $xtype;
						$fields['fieldLabel'] = $label;
						$fields['allowBlank'] = isset($val['null']) ? $val['null'] : FALSE;
					}
					
					if(in_array($key, $this->field_alphanum))
					{
						$fields['vtype']  = 'alphanum';
					}
					
					if(in_array($key, $this->field_duplicate_entry))
					{
						$fields['duplicateEntry'] = TRUE;
					}
					
					if($key === $this->uniq_name)
					{
						$fields['vtype']  = 'uniqueusername';
						$fields['minLength']  = 5;
					}
					// array_search('green', $array); 
					// check if there is a unique field
					$key_field = array_search($key, $this->uniq_field);
					
					if(! empty($key_field))
					{
						$fields['vtype'] = 'uniquefield';
						$fields['tbid']  = $key_field;
					}
					
					// check if there is a email field
					if(in_array($key, $this->field_eml))
					{
						$fields['vtype']  = 'uniqueemail';
					}
					
					if($xtype === 'datefield')
					{
						$fields['format'] = $this->_generate_column_date_format($val['type']);
					}
					else // if the field_type is not datefield
					{
						if($val['max_length'] != NULL)
						{
							$fields['maxLength'] = $val['max_length'];
						}
					}

					// check if there is some hidden fields
					if(in_array($key, $this->unshow_fields))
					{
						$fields['hidden'] = TRUE;
						$fields['disabled'] = TRUE;
						$fields['allowBlank'] = TRUE;
					}
					
					$form_fields[] = $fields;
					
					if($pwd_mode)
					{
						$pwd_count2 = $pwd_count+1;
						$fields['id'] = "pwd".$pwd_count2."";
						$fields['name']  = $key.'_confirm';
						$fields['fieldLabel'] = 'Confirm '.$label;
						$fields['vtype'] = 'matchpassword';
						$fields['initialPassField'] = 'pwd'.$pwd_count;
						$fields['enableKeyEvents'] = TRUE;
						
						$form_fields[] = $fields;
						
						$fieldsold['id'] = 'old'.$key;
						$fieldsold['name'] = 'old'.$key;
						$fieldsold['fieldLabel'] = 'Old '.$label;
						$fieldsold['inputType']  = 'password';
						$fieldsold['minLength']  = 5;
						$fieldsold['allowBlank'] = TRUE;
						$fieldsold['vtype'] = 'checkoldpassword';
						$fieldsold['hidden'] = TRUE;
						
						$form_fields[] = $fieldsold;
						
						$fields2 = array();
						$fields2['id'] = 'pwdLabel';
						$fields2['hidden']= FALSE;
						$fields2['width'] = 200;
						$fields2['xtype'] = 'label';
						$fields2['text']  = 'Leave both password blank to keep current';
						$fields2['style'] = 'font-size: 10px;text-align:right;display:block;margin:5px;color:red';
						$form_fields[] = $fields2;
					}
				}
			}
		}
		
		return $form_fields;
	}
	
	private function _build_col_model()
	{
		
		$col_model   = array();
		$col_model[] = array('dataIndex' => 'rnum', 'header' => ucwords('No'), 'width' => 50, 'sortable' => true); 
		foreach($this->meta as $key => $val)
		{
			if(!in_array($key, $this->hide))
			{
				if( ($this->show_pk == FALSE ? ! $val['primary_key'] :  $val) )
				{
					$header = str_replace("__"," - ",($key));
					
					$merge = explode('00',$header);
					if(isset($merge))
					{
						$header = str_replace("00"," ,",ucwords($header));
						if(isset($val['uneditable']))
						{
							$uneditable = $val['uneditable'];
						}
					}
					
					$header = str_replace("_"," ",ucwords($header));
					//change the header
					if(isset($this->header[strtolower($header)]))
					{
						$header = $this->header[strtolower($header)];
					}
					
					if(isset($this->header_width[strtolower($header)]))
					{
						$width = $this->header_width[strtolower($header)];
					}
					else
					{
						$width  = $this->_set_width($header);
					}
					
					if(isset($val['disable_search']))	
					{
						$disable_search = $val['disable_search'];
						$sortable = FALSE;
					}
					else
					{
						$disable_search = FALSE;
						$sortable = TRUE;
					}
					
					
					
					$col_attribute = array();
					$col_attribute['dataIndex'] = $key;
					$col_attribute['header']    = ucwords($header);
					$col_attribute['width']     = $width;
					$col_attribute['sortable']  = $sortable;
					$col_attribute['disSearch'] = $disable_search;
					
					$col_type = $this->_generate_store_type($val['type']);
				
					if($col_type == 'int' && $val['max_length'] == 1)
					{
						$col_type = 'bool';
						$col_attribute['xtype'] = 'actioncolumn';
						if(in_array($key, $this->bool_inactive_action_col))
						{
							$col_attribute['inactiveActionCol'] = TRUE;
						}
						else
						{
							$col_attribute['inactiveActionCol'] = FALSE;
						}
					}
					
					$col_attribute['colType'] = $col_type;
					
					if($col_type == 'date')
					{
						$col_attribute['dateFormat'] = $this->_generate_column_date_format($val['type']);
					}
					
					// check if there is some hidden cols
					if(in_array($key, $this->unshow_cols))
					{
						$col_attribute['hidden'] = TRUE;
					}
					
					if($this->type_grid == 'editor' && ! isset($uneditable))
					{
						//$att = array('xtype' => 'textfield','allowBlank' => false);
						//$editor = ($this->_generate_xtype($val['type']));
						//$col_attribute['editor'] = "new Ext.form.{$editor}()";
						if($val['type'] == 'combo')
						{
							$fields['xtype'] = 'combo';
							$fields['triggerAction'] = 'all';
							$fields['mode']  = 'remote';
							$fields['store'] = json_encode(array($val['related_pk'],$key));
							$fields['related_to']    = json_encode(array(base64_encode($val['related_to'])));
							$fields['related_where'] = json_encode($val['related_where']);
							$fields['hiddenName']    = $key;
							$fields['hiddenValue']   = $val['related_pk'];
							$fields['valueField']    = $val['related_pk'];
							$fields['displayField']  = $key;
							$fields['selectOnFocus'] = TRUE;
							$fields['typeAhead']  = TRUE;
							$fields['editable']   = FALSE;
							$fields['emptyText']  = 'Choose One';
							$fields['lazyRender'] = TRUE;
							
							$col_attribute['editor'] = json_encode($fields);
						}
						else
						{
							$editor = strtolower($this->_generate_xtype($val['type']));
							// force to have checkcolumn
							if($editor == 'numberfield' && $val['max_length'] == 1)
							{
								$col_attribute['xtype'] = 'checkcolumn';
								$col_attribute['editor'] = null;
							}
							else
							{
								//$att = array('xtype' => strtolower($editor),'allowBlank' => false);
								$fields['xtype'] = $editor;
								$fields['allowBlank'] = FALSE;
								$col_attribute['editor'] = json_encode($fields);
							}
						}
					}
					
					$store_type = $this->_generate_store_type($val['type']);
				
					if($store_type == 'date')
					{
						$col_attribute['renderer'] = "fnDateRenderer('".$this->_generate_column_date_format($val['type'])."')";
					}
					
					if(isset($this->renderer_cols[$key]))
					{
						$col_attribute['renderer'] = $this->renderer_cols[$key];
					}
					
					
					$col_model[] = $col_attribute;
				}
			}
		}
		
		return $col_model;
	}
	
	private function _generate_xtype($val_type)
	{
		if(in_array($val_type,$this->_string_data_types))
		{
			if($val_type == 'text')
			{
				return 'TextArea';
			}
			else
			{
				return 'TextField';
			}
		}
		elseif(in_array($val_type,$this->_numeric_data_types))
		{
			return 'NumberField';
		}
		elseif(in_array($val_type,$this->_datetime_data_types))
		{
			return 'DateField';
		}
		else
		{
			return 'TextField';
		}
	}
	
	private function _set_width($str)
	{
		$length = strlen($str);
		
		if($length > 0 && $length <= 10)
		{
			$width = 100;
		}
		elseif($length > 10 && $length <= 20)
		{
			$width = 150;
		}
		else
		{
			$width = 170;
		}
		
		return $width;
	}
}