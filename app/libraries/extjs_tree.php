<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TreeNode {

	public $id = "";
	public $text = "";
	public $iconCls = "";
	public $leaf = TRUE;
	public $draggable = FALSE;
	public $href = "#";
	public $hrefTarget = "";
	public $isTarget = FALSE;
	public $checked = FALSE;
	public $qtip = FALSE;
	public $addAttr = array();
	public $expanded = FALSE;

	function  __construct($id,$text,$iconCls,$leaf,$draggable,$href,$hrefTarget,$isTarget,$checked,$qtip,$addAttr,$expanded) 
	{
		$this->id = $id;
		$this->text = $text;
		$this->iconCls = $iconCls;
		$this->leaf = $leaf;
		$this->draggable = $draggable;
		$this->href = $href;
		$this->hrefTarget = $hrefTarget;
		$this->isTarget = $isTarget;
		$this->checked = $checked;
		$this->qtip = $qtip;
		$this->addAttr = $addAttr;
		$this->expanded = $expanded;
	}
}

class Extjs_Tree {

	protected $nodes = array();

	function add($id,$text,$iconCls,$leaf,$draggable,$href,$hrefTarget,$isTarget,$checked,$qtip,$addAttr = array(),$expanded) 
	{
		$n = new TreeNode($id,$text,$iconCls,$leaf,$draggable,$href,$hrefTarget,$isTarget,$checked,$qtip,$addAttr,$expanded);
		$this->nodes[] = $n;
	}

	function json() 
	{
		return json_encode($this->nodes);
	}
}