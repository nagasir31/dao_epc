<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File_Manager {
	
	public $absolute_path = ABSOLUTE_PATH;
	public $root_dir = "global";
	public $thumbs_dir = "global_thumbs";
	public $max_upload_size = 1000;
	public $add_dir = TRUE;
	public $edit_dir = TRUE;
	public $del_dir = TRUE;
	public $add_file = TRUE;
	public $edit_file = TRUE;
	public $del_file = TRUE; 
	
	public $files_ext  = array('doc', 'docx','rtf', 'pdf', 'xls', 'xlsx', 'txt', 'csv','html','xhtml','psd','sql','log','fla','xml','ade','adp','mdb','accdb','ppt','pptx','odt','ots','ott','odb','odg','otp','otg','odf','ods','odp','css','ai'); //Files 1
	public $images_ext = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'tiff', 'svg'); //Images 2
	public $miscs_ext  = array('zip', 'rar','gz','tar','iso','dmg'); //Archives 3
	public $videos_ext = array('mov', 'mpeg', 'mp4', 'avi', 'mpg','wma','flv','webm'); //Video 4
	public $musics_ext = array('mp3', 'm4a', 'ac3', 'aiff', 'mid','ogg','wav'); //Audio 5
	
	public $ext_type = array(
		'files_ext'  => array(2,4),
		'images_ext' => array(1,4),
		'miscs_ext'  => array(2,4),
		'videos_ext' => array(2,3,4),
		'musics_ext' => array(2,3,4)
	);
	
	public $files_mime  = 'application/*';
	public $images_mime = 'image/*';
	public $miscs_mime  = 'text/*';
	public $videos_mime = 'video/*';
	public $musics_mime = 'audio/*';
	
	public $mime_type = array(
		'files_mime'  => array(2,4),
		'images_mime' => array(1,4),
		'miscs_mime'  => array(2,4),
		'videos_mime' => array(2,3,4),
		'musics_mime' => array(2,3,4)
	);
	
	public $thumbs_size = array(
		array('width' => 1000,'height' => 750),
		array('width' => 122,'height' => 91),
		array('width' => 45,'height' => 38)
	);
	
	public $max_image_size = array('width' => 1000, 'height' => 750);
	
	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->lang->load('filemanager', $this->CI->get_lang());	
	}
	
	function base_path()
	{
		return $this->absolute_path.'asset/';
	}
	
	function base_file_url()
	{
		return app_asset_url();
	}
	
	function base_thumb_path()
	{
		return $this->base_path().$this->thumbs_dir.'/';
	}
	
	function thumb_url()
	{
		return app_asset_url($this->thumbs_dir.'/');
	}
	
	function module_path()
	{
		return $this->base_path().'backend/responsiveFilemanager/';
	}
	
	function module_url()
	{
		return app_asset_backend_url('responsiveFilemanager/');
	}
	
	function allowed_ext($implode = FALSE)
	{
		$allowed_types = array();
		$type = $this->CI->session->userdata('filemanager_type');
		foreach($this->ext_type as $key => $types)
		{
			if(in_array($type, $types))
			{
				$allowed_types = array_merge($allowed_types, $this->$key);
			}
		}
		
		if($implode)
		{
			$allowed_types = implode("|", $allowed_types);
		}
		
		return $allowed_types;
	}
	
	function accepted_mimes()
	{
		$accepted_mimes = array();
		$type = $this->CI->session->userdata('filemanager_type');
		foreach($this->mime_type as $key => $types)
		{
			if(in_array($type, $types))
			{
				$accepted_mimes[] = $this->$key;
			}
		}
		
		$accepted_mimes = implode(",", $accepted_mimes);
		
		return $accepted_mimes;
	}
	
	function set_root_dir($dir)
	{
		$this->root_dir = $dir;
	}
	
	function get_breadcrumb($current_dir, $type, $field_id)
	{
		$arr_current_dir = explode("/", $current_dir);
		
		$breadcrumb = '';
		
		$path = '';
		
		$x = 0;
		
		foreach($arr_current_dir as $dir)
		{
			$path .= $dir;
			if($x != 0)
			{	
				$breadcrumb .= '<li><span class="divider">/</span></li>';
				
				if(end($arr_current_dir) == $dir) 
				{
					$breadcrumb .= '<li class="active">'.$dir.'</li>';
				}
				else
				{
					$breadcrumb .= '<li><a href="?req_dir='.$path.'&type='.$type.'&field_id='.$field_id.'">'.$dir.'</a></li>';
				}
			}
			else
			{
				$path .= '/';
			}
			
			$x++;
		}
		
		return ! empty($breadcrumb) ? $breadcrumb : '<li><span class="divider">/</span></li>';
	}
	
	function get_prev_dir($current_dir)
	{
		$arr_current_dir = explode("/", $current_dir);
		
		$path = '';
		
		$x = 0;
		
		foreach($arr_current_dir as $dir)
		{
			if($x != count($arr_current_dir)-1)
			{	
				$path .= $dir;
			}
			
			if($x < count($arr_current_dir)-2)
			{
				$path .= '/';
			}
			
			$x++;
		}
		
		return $path;
	}
	
	function get_content($current_dir, $type, $field_id, $thumbs, $root_dir)
	{
		switch($type)
		{	
			case 1: $apply = 'apply_img'; break;
			case 2: $apply = 'apply_link'; break;
			case 3: $apply = 'apply_video'; break;
			case 4: $apply = 'apply'; break;
			default: $apply = 'apply_none'; break;
		}
		
		$html = '';
		
		$path = $this->base_path().$current_dir;
		
		if (is_dir($path)) 
		{
			$prev_dir = $this->get_prev_dir($current_dir);
			if(! empty($prev_dir))
			{
				if($this->root_dir != $current_dir) 
				{
					$link_back = "?req_dir={$prev_dir}&type={$type}&field_id={$field_id}&thumbs={$thumbs}&root_dir={$root_dir}";
					$html .= '
					<li class="back" data-name="back_'.uniqid().'">
						<figure class="back-directory" data-name="back_'.uniqid().'">
							<a href="'.$link_back.'" class="folder-link" title="'.$this->CI->lang->line('global_open').'">
								<div class="img-precontainer">
									<div class="img-container directory"><span></span>
										<img alt="folder" src="'.$this->module_url().'img/ico/folder_back.jpg'.'" class="directory-img">
									</div>
								</div>
								<div class="img-precontainer-mini directory">
									<div class="img-container-mini">
										<span></span>
										<img alt="folder" src="'.$this->module_url().'img/ico/folder_back.png'.'" class="directory-img">
									</div>
								</div>
								<div class="box no-effect">
									<h4>'.$this->CI->lang->line('global_back').'</h4>
								</div>
							</a>
						</figure>
					</li>';
				}
			}
			
			$iterator = new DirectoryIterator($path);

			$x = 0;
			foreach($iterator as $item) 
			{
				if(!$item->isDot()) 
				{
					$full_path = $path.'/'.$item;
					if($item->isDir() && strpos($item, '.') !== (int)0) 
					{
						$dir_size = $this->get_dir_size($full_path);
						$dir_last_modified = date ("d/m/Y", $item->getMTime());
						$link_dir = "?req_dir={$current_dir}/{$item}&type={$type}&field_id={$field_id}&thumbs={$thumbs}&root_dir={$root_dir}";
						
						$html .= '
						<li class="dir" data-name="'.(string)$item.'">
							<figure data-type="dir" class="directory" data-name="'.(string)$item.'">
								<a href="'.$link_dir.'" class="folder-link" title="'.$this->CI->lang->line('global_open').'">
									<div class="img-precontainer">
										<div class="img-container directory"><span></span>
											<img alt="'.$item.'" src="'.$this->module_url().'img/ico/folder.jpg'.'" class="directory-img">
										</div>
									</div>
									<div class="img-precontainer-mini directory">
										<div class="img-container-mini">
											<span></span>
											<img alt="'.$item.'" src="'.$this->module_url().'img/ico/folder.png'.'" class="directory-img">
										</div>
									</div>
								</a>
								<div class="box">
									<h4 class="ellipsis"><a href="'.$link_dir.'" data-file="'.(string)$item.'" class="folder-link" title="'.$this->CI->lang->line('global_open').'">'.(string)$item.'</a></h4>
								</div>
								<input type="hidden" value="'.$item.'" class="name">
								<input type="hidden" value="'.$dir_last_modified.'" class="date">
								<input type="hidden" value="'.$dir_size.'" class="size">
								<input type="hidden" value="dir" class="extension">
								<div class="file-date">'.$dir_last_modified.'</div>
								<div class="file-size">'.$dir_size.'</div>
								<div class="file-extension">dir</div>
								<figcaption>
									<a data-thumb="../thumbs/'.$item.'" data-name="'.$item.'" title="'.$this->CI->lang->line('filemanager_rename').'" class="tip-left edit-button rename-folder" href="javascript:void(&quot;&quot;);"><i class="icon-pencil "></i></a>
									<a data-thumb="../thumbs/'.$item.'" data-name="'.$item.'" data-confirm="'.$this->CI->lang->line('filemanager_confirm_delete_folder').'" title="'.$this->CI->lang->line('global_delete').'" class="tip-left erase-button delete-folder" href="javascript:void(&quot;&quot;);"><i class="icon-trash"></i></a>
								</figcaption>
							</figure>
						</li>';
					}
					else
					{
						//print_r($this->allowed_ext());die();
						//echo get_mime_by_extension($item->getFilename());
						
						$file_ext  = $this->get_file_ext($item->getFilename());
						$file_name = $this->get_file_name($item->getFilename(), $file_ext);
						$file_size = $this->format_file_size($item->getSize());
						$file_last_modified = date ("d/m/Y", $item->getMTime());
						$src_thumb_mini = '';
						
						$src = app_asset_url($current_dir.'/'.$item);
						
						
						if(file_exists($this->module_path().'img/ico/'.$file_ext.'.jpg'))
						{
							$src_thumb = $this->module_url().'img/ico/'.$file_ext.'.jpg';
						}
						else
						{
							$src_thumb = $this->module_url().'img/ico/default.jpg';
						}
						
						$is_file =  FALSE; $is_img = FALSE; $is_misc = FALSE; $is_video = FALSE; $is_audio = FALSE;
						
						$show_ori_image = TRUE;
						
						if(in_array($file_ext, $this->musics_ext)) 
						{
							$class_ext = 5;
							if(in_array($type, $this->ext_type['musics_ext']))
							{
								$is_audio = TRUE;
							}
						}
						elseif(in_array($file_ext, $this->videos_ext)) 
						{
							$class_ext = 4;
							if(in_array($type, $this->ext_type['videos_ext']))
							{
								$is_video = TRUE;
							}
						}
						elseif (in_array($file_ext, $this->miscs_ext)) 
						{
							$class_ext = 3;
							if(in_array($type, $this->ext_type['miscs_ext']))
							{
								$is_misc = TRUE;
							}
						}
						elseif(in_array($file_ext, $this->images_ext)) 
						{
							$class_ext = 2;
							if(in_array($type, $this->ext_type['images_ext']))
							{
								$is_img = TRUE;
							}
								
							$this->create_thumbs($current_dir, $item);
							
							$t = str_replace('global', '', $current_dir);
							$src_thumb = preg_replace('/global\/?/',$this->thumb_url() , $current_dir.'/'.$item);
							$src_thumb_mini = preg_replace('/global\/?/',$this->thumb_url() , $current_dir.'/'.$file_name.'_2.'.$file_ext);
							
							list($img_width, $img_height, $img_type, $attr) = getimagesize($this->base_path().$current_dir.'/'.$item); 
							$img_dimensions = $img_width." x ".$img_height;
						}
						else
						{
							$class_ext = 1;
							if(in_array($type, $this->ext_type['files_ext']))
							{
								$is_file = TRUE;
							}
						}
											
						if($is_file || $is_img || $is_misc || $is_video || $is_audio)
						{
							if($is_img && $thumbs == 2)
							{
								$src = $src_thumb_mini;
							}
							
							$html .= '
							<li data-name="'.$item.'" class="ff-item-type-'.$class_ext.' file">
								<figure data-type="'.($is_img ? 'img' : 'file').'" data-name="'.$item.'">
									<a data-function="'.$apply.'" data-field_id="'.$field_id.'" data-file="'.$item.'" data-src="'.$src.'" class="link" title="'.$this->CI->lang->line('global_select').'" href="javascript:void(&quot;&quot;);">
										<div class="img-precontainer">
											<div class="filetype">'.$file_ext.'</div>				    
											<div class="img-container">
												<span></span>
												<img src="'.$src_thumb.'" class="'.($show_ori_image ? "original" : "").' icon" alt="'.$file_name.' thumbnails">
											</div>
										</div>
										<div class="img-precontainer-mini ">
											<div class="filetype '.$file_ext.' '.($is_img ? "hide" : "").'">'.$file_ext.'</div>
											<div class="img-container-mini">
												<span></span>';
												if($src_thumb_mini)
												{
													$html .= '<img src="'.$src_thumb_mini.'" class="icon" alt="">';
												}
							$html .= '		</div>
										</div>
										<div class="cover"></div>
									</a>	
									<div class="box">				
										<h4 class="ellipsis">
											<a data-function="'.$apply.'" data-field_id="'.$field_id.'" data-file="'.$item.'" data-src="'.$src.'" class="link" title="'.$this->CI->lang->line('global_select').'" href="javascript:void(&quot;&quot;);">'.$file_name.'</a> 
										</h4>
									</div>
									
									<input type="hidden" value="'.$item.'" class="name">
									<input type="hidden" value="'.$file_last_modified.'" class="date">
									<input type="hidden" value="'.$file_size.'" class="size">
									<input type="hidden" value="'.$file_ext.'" class="extension">
									<div class="file-date">'.$file_last_modified.'</div>
									<div class="file-size">'.$file_size.'</div>
									<div class="img-dimension">'.($is_img ? $img_dimensions : "").'</div>
									<div class="file-extension">'.$file_ext.'</div>';
									
									$html .= '<figcaption>';
									$html .= '<form id="form'.$x.'" class="download-form" method="post" action="filemanager/force_download">';
									$html .= '<input type="hidden" value="'.$full_path.'" name="path">';
									$html .= '<input type="hidden" value="'.$item.'" name="name" class="name_download">';
									$html .= '<a href="javascript:void(&quot;&quot;);" onclick="javascript:jQuery(&quot;#form'.$x.'&quot;).submit();" class="tip-right" title="'.$this->CI->lang->line('global_download').'"><i class="icon-download"></i></a>';
									if($is_img)
									{
										$html .=  '<a class="tip-right preview" title="'.$this->CI->lang->line('global_preview').'" data-url="'.$src.'" data-toggle="lightbox" href="#previewLightbox"><i class=" icon-eye-open"></i></a>';
									}
									else
									{
										$html .= '<a class="preview disabled"><i class="icon-eye-open icon-white"></i></a>';
									}
									$html .= '<a data-thumb="../thumbs/'.$item.'" data-name="'.$item.'" title="'.$this->CI->lang->line('filemanager_rename').'" class="tip-left edit-button rename-file" href="javascript:void(&quot;&quot;)"><i class="icon-pencil "></i></a>';
									$html .= '<a data-thumb="../thumbs/'.$item.'" data-name="'.$item.'" data-confirm="'.$this->CI->lang->line('filemanager_confirm_delete_file').'" title="'.$this->CI->lang->line('global_delete').'" class="tip-left erase-button delete-file" href="javascript:void(&quot;&quot;)"><i class="icon-trash "></i></a>';
									$html .= '</form></figcaption>';
							$html .= '</figure></li>';
						}
					}
				}
				
				$x++;
			}
		}
			
		return trim($html);
	}
	
	function create_thumbs($dir, $file)
	{
		$path = $this->base_path().$dir;
		$path_thumbs = preg_replace('/global\/?/', $this->base_thumb_path() , $dir.'/');
		
		$this->CI->create_dir($path_thumbs);
		
		if(! file_exists($path_thumbs.'/'.$file))
		{	
			ini_set('gd.jpeg_ignore_warning', 1);
			$this->CI->load->library('image_lib');
			
			list($img_width, $img_height, $img_type, $attr) = getimagesize($path.'/'.$file); 
			$x = 1;
			foreach($this->thumbs_size as $size)
			{	
				$new_image = $path_thumbs.'/'.$file;
			
				if(file_exists($path_thumbs.'/'.$file))
				{
					$file_ext = $this->get_file_ext($file);
					$file_name = $this->get_file_name($file, $file_ext);
					$new_image = $path_thumbs.'/'.$file_name.'_'.$x.'.'.$file_ext;
				}
				
				if($img_width < $size['width'])
				{
					$size['width'] = $img_width;
				}
				
				if($img_height < $size['height'])
				{
					$size['height'] = $img_height;
				}
				
				$config = array();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $path.'/'.$file;
				$config['maintain_ratio'] = FALSE;
				$config['width'] = $size['width'];
				$config['height'] = $size['height'];
				$config['new_image'] = $new_image;
				
				$this->CI->image_lib->initialize($config); 
				$this->CI->image_lib->resize();
				
				$this->CI->image_lib->clear(); 
				$x++;
			}
		}
		
	}
	
	function delete_thumbs($path, $file)
	{
		$path_thumbs = $this->change_path_thumbs($path);
		//$path_thumbs = preg_replace('/global\/?/', $this->base_thumb_path() , $dir.'/');
		
		$file_delete = $file;
		$x = 1;
		foreach($this->thumbs_size as $size)
		{
			if($x != 1)
			{
				$file_ext = $this->get_file_ext($file);
				$file_name = $this->get_file_name($file, $file_ext);
				$file_delete = $file_name.'_'.$x.'.'.$file_ext;
			}
			
			$full_path = $path_thumbs.'/'.$file_delete;
			if(file_exists($full_path) && is_writable($full_path))
			{
				unlink($full_path);
			}
			
			$x++;
		}
	}
	
	function change_path_thumbs($path)
	{
		return preg_replace('/global\/?/', $this->thumbs_dir.'/' , $path);
	}
	
	function get_dir_size($dir_path) 
	{
		$size = 0;
		foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir_path)) as $file){
			$size += $file->getSize();
		}
	
		return $this->format_file_size($size);
	} 
	
	function format_file_size($size) {
		$mod = 1024;
		$units = explode(' ','B KB MB GB TB PB');
		for ($i = 0; $size > $mod; $i++) {
			$size /= $mod;
		}

		return round($size, 2) . ' ' . $units[$i];
	}
	
	function get_file_ext($file)
	{
		return strtolower(pathinfo($file, PATHINFO_EXTENSION));
	}
	
	function get_file_name($file, $file_ext)
	{
		return basename($file,".".$file_ext);
	}
}