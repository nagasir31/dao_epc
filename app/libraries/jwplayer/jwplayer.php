<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jwplayer
{
	private $id = 1;

	public function jw_embedder( $params = array() )
	{
		$defaults = array(
			//'config'      => base_assets_url('js/jwplayer/jwplayer_config.xml'),
			'width'       => '100%',
			'height'      => '324',
			'flashplayer' => base_assets_url('js/jwplayer/jwplayer.flash.swf')
		);
		$config = array_merge( $defaults, $params );

		$output = '
		<div id="video-container-' . $this->id . '" >
		Loading video ...
		</div>
		<script>
		jwplayer("video-container-' . $this->id . '").setup({' . "\n";

		$num = count( $config );

		$i = 1;

		foreach( $config as $k => $v )
		{
			$output .= '"' . $k . '": "' . $v . '"';

			$output .= ( $i != $num ) ? ",\n" : "";

			$i++;
		}

		$output .= '     
			});
			</script>
		';

		$this->id++;

		return $output;
	}
}