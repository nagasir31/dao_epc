<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('display_errors', 1);
error_reporting(E_ALL);
class Login_Manager {
	
	var $logged_in = FALSE;
	var $logged_in2 = FALSE;
	
	function __construct()
	{
		$this->CI =& get_instance(); 
		$this->session =& $this->CI->session;
		$this->CI->load->library('encrypt');
		$this->_init();
		
		$this->pic_url = $this->CI->get_pic_url();
	}
	
	private function _init()
	{
		if( ! $this->CI->db->table_exists('sys_user'))
		{
			show_error('Table sys_user doesn\'t\ exist !!!');
		}
		
        $this->logged_in = $this->session->userdata($this->CI->session_key('user_id')) ? TRUE : FALSE;
		
		 $this->logged_in2 = $this->session->userdata($this->CI->session_key('applicant_id')) ? TRUE : FALSE;
	}
	
	function check_old_password($params)
	{
		$where['password'] = md5($params['password']);
		
        if(isset($params['user_id']))
		{
		  $where['id'] = $params['user_id'];
		}
		
		$prm['where'] = $where;
		$prm['table'] = 'sys_user';
		
		$row = $this->CI->def_model->count_rows($prm);
		if($row['CNT'] > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function login_process($param, $md5 = TRUE)
	{
		$where = array(
			'sys_user.name' => strtolower($param['usr']),
			'password' => $md5 === TRUE ? md5($param['pwd']) : $param['pwd'],
			'sys_user.is_active' => 1
		);
		
		if($md5 === TRUE)
		{
			$where['sys_user.sys_group_child_id !='] = 0;
		}
		
		$params = array(
			'select' => 'sys_user.name user_name, sys_user.password user_password, sys_user.last_login last_login, sys_user.proyeks,
				sys_user.sys_group_child_id sys_group_child_id, sys_user.counter counter, sys_user.id user_id, 
				sys_group_child.name group_name, sys_group_child.description group_description, sys_group_child.sys_group_parent_id group_parent_id,
				employee.id, employee.name, employee.nrp, employee.picture, employee.divisi, employee.ref_cabang_id, employee.ref_status_id, employee.email, employee.ref_jabatan_id, employee.ref_proyek_id
			',
			'table'  => 'sys_user',
			'where'  => $where,
			'join'  => array(
					array(
						'table' => 'sys_group_child',
						'on' => 'sys_user.sys_group_child_id = sys_group_child.id',
						'type' => 'left'
					),array(
						'table' => 'sys_group_parent',
						'on' => 'sys_group_child.sys_group_parent_id = sys_group_parent.id',
						'type' => 'left'
					),array(
						'table' => 'employee',
						'on' => 'sys_user.employee_id = employee.id',
						'type' => 'left'
					)
				)
		);
		
        $query = $this->CI->def_model->get_list($params);
		//echo $this->CI->db->last_query();die('d');
		if($query->num_rows())
		{	
			$row = $query->first_row();
			$data_update = array(
            	'last_login' => date("Y-m-d H:i:s"),
            	'counter' => $row->counter + 1
        	);
        	
    		$this->CI->db->where('id', $row->user_id);
        	$update = $this->CI->db->update('sys_user', $data_update);
			
			if($update)
			{	
				$arr_sess = array();
				if(isset($row->id) && ! empty($row->id))
				{
					$arr_sess[$this->CI->session_key('id')] = $row->id;
				}
				
				if(isset($row->name) && ! empty($row->name))
				{
					$arr_sess[$this->CI->session_key('name')] = $row->name;
				}
				
				if(isset($row->nrp) && ! empty($row->nrp))
				{
					$arr_sess[$this->CI->session_key('nrp')] = $row->nrp;
				}
				
				
				if(isset($row->ref_jabatan_id) && ! empty($row->ref_jabatan_id))
				{
					$arr_sess[$this->CI->session_key('ref_jabatan_id')] = $row->ref_jabatan_id;
					
					$res_jabatan = $this->CI->get_jabatan($row->ref_jabatan_id);
					
					if(isset($res_jabatan['glosarry']) && ! empty($res_jabatan['glosarry']))
					{
						$arr_sess[$this->CI->session_key('jabatan_glosarry')] = $res_jabatan['glosarry'];
					}
				}
				
				if(isset($row->email) && ! empty($row->email))
				{
					$arr_sess[$this->CI->session_key('email')] = $row->email;
				}
				
				if(isset($row->picture) && ! empty($row->picture))
				{
					$arr_sess[$this->CI->session_key('picture')] = $this->pic_url.$row->picture;
				}
				else
				{
					$arr_sess[$this->CI->session_key('picture')] = app_asset_url('backend/img/no-photo.jpg');
				}
				
				if(isset($row->divisi) && ! empty($row->divisi))
				{
					$arr_sess[$this->CI->session_key('divisi')] = $row->divisi;
				}
				else
				{
					$arr_sess[$this->CI->session_key('divisi')] = '-';
				}
				
				if(isset($row->ref_cabang_id) && ! empty($row->ref_cabang_id))
				{
					$arr_sess[$this->CI->session_key('cabang_id')] = $row->ref_cabang_id;
				}
				else
				{
					$arr_sess[$this->CI->session_key('cabang_id')] = '-';
				}
				
				if(isset($row->ref_proyek_id) && ! empty($row->ref_proyek_id))
				{
					$arr_sess[$this->CI->session_key('ref_proyek_id')] = $row->ref_proyek_id;
				}
				else
				{
					$arr_sess[$this->CI->session_key('ref_proyek_id')] = '';
				}
				
				///////////////////////////////// USER /////////////////////////////////
				if(isset($row->user_id) && ! empty($row->user_id))
				{
					$arr_sess[$this->CI->session_key('user_id')] = $row->user_id;
					$arr_sess[$this->CI->session_key('sess_id')] = md5($row->user_id);
				}
				
				if(isset($row->user_name) && ! empty($row->user_name))
				{
					$arr_sess[$this->CI->session_key('user_name')] = $row->user_name;
				}
				
				if(isset($row->sys_group_child_id) && ! empty($row->sys_group_child_id))
				{
					$arr_sess[$this->CI->session_key('sys_group_child_id')] = $row->sys_group_child_id;
				}
				
				if(isset($row->group_name) && ! empty($row->group_name))
				{
					$arr_sess[$this->CI->session_key('group_name')] = $row->group_name;
				}
				
				if(isset($row->proyeks) && ! empty($row->proyeks))
				{
					$proyeks = json_decode($row->proyeks);
					if(!is_array($proyeks))
					{
						$proyeks = array($proyeks);
					}
					$arr_sess[$this->CI->session_key('proyeks')] = $proyeks;
				}
				
				$arr_sess[$this->CI->session_key('language')] = $this->CI->get_lang();
				$this->session->set_userdata($arr_sess);
				$this->logged_in = TRUE;
				
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	function login_process2($param)
	{
		$params = array(
			'table'  => 'applicant',
			'where'  => array(
					'email' => strtolower($param['eml']),
					'password' => $param['pwd'],
					'is_active' => 1,
					//'is_email_valid' => 1
				)
		);
		
        $query = $this->CI->def_model->get_list($params);
		if($query->num_rows())
		{	
			$row = $query->first_row();
			$data_update = array(
            	'last_login' => date("Y-m-d H:i:s"),
            	'counter' => $row->counter + 1
        	);
        	
    		$this->CI->db->where('id', $row->id);
        	$update = $this->CI->db->update('applicant', $data_update);
			
			if($update)
			{	
				$arr_sess = array();
				if(isset($row->id) && ! empty($row->id))
				{
					$arr_sess[$this->CI->session_key('applicant_id')] = $row->id;
				}
				
				if(isset($row->name) && ! empty($row->name))
				{
					$arr_sess[$this->CI->session_key('applicant_name')] = $row->name;
				}
				
				if(isset($row->email) && ! empty($row->email))
				{
					$arr_sess[$this->CI->session_key('applicant_email')] = $row->email;
				}
				
				if(isset($row->photo) && ! empty($row->photo))
				{
					$arr_sess[$this->CI->session_key('applicant_photo')] = $row->photo;
				}
				
				$this->session->set_userdata($arr_sess);
				$this->logged_in2 = TRUE;
				
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		$this->logged_in = FALSE;
	}
	
	function lock()
	{
		$this->session->unset_userdata($this->CI->session_key('user_id'));
		$this->logged_in = FALSE;
	}
	
	function logout2()
	{
		//$this->session->sess_destroy();
		$this->session->unset_userdata($this->CI->session_key('applicant_id'));
		$this->session->unset_userdata($this->CI->session_key('applicant_name'));
		$this->session->unset_userdata($this->CI->session_key('applicant_email'));
		$this->session->unset_userdata($this->CI->session_key('applicant_photo'));
		$this->logged_in2 = FALSE;
	}
}