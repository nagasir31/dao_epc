<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Navigation_Manager {
	
	function __construct()
	{
		$this->CI =& get_instance();
	}
	
	function rebuild_breadcrumb($active_menu,$active_menu2 = array())
	{
		$mode = "sys";
		$act_menu = ! empty($active_menu) ? $active_menu : 'dashboard';
		$act_menu_lang = '';
		$act_breadcrumb_lang = '';
		
		$res_menu = $this->CI->def_model->get_menu($mode, array('where' => array('a.name' => $act_menu)))->row_array();
		
		if(! empty($res_menu))
		{
			$act_menu = $res_menu['name'];
			
			$path = json_decode($res_menu['path']);
			
			for($i = 0; $i < count($path); $i++)
			{
				$parent_act_menu[] = $path[$i];
				
				$res_parent = $this->CI->def_model->get_menu($mode, array('where' => array('a.id' => $path[$i])))->row_array();
				
				if(! empty($res_parent))
				{
					$href = $res_parent == 1 ? app_backend_url($res_parent["location"]) : ($res_parent["location"] == "#" ? "#" : app_backend_url($res_parent["location"]));	
					$target = $res_parent["link_target_id"] == 2 ? "_blank" : "";
					
					$act_breadcrumb_lang .= '<li><a href="'.$href.'" target="'.$target.'">'.ucwords($res_parent['description']).'</a> <i class="icon-angle-right"></i></li>';
				}
			}
			
			$x = 0;
			foreach($active_menu2 as $key => $val)
			{
				if($x == count($active_menu2) - 1) 
				{
					$act_breadcrumb_lang .= '<li>'.ucwords($val).'</li>';
				}
				else
				{
					$act_breadcrumb_lang .= '<li><a href="'.$key.'">'.ucwords($val).'</a> <i class="icon-angle-right"></i></li>';
				}
				$x++;
			}
			
			$this->CI->session->set_userdata($this->CI->session_key('active_breadcrumb_lang'),$act_breadcrumb_lang);
		}
	}
	
	function get_menu_backend($active_menu = '', $hidden_menu = array())
	{
		global $index, $data, $html, $array_level, $array_id, $act_menu, $parent_act_menu;
		$data = array();
		$index = array();
		$array_level = array();
		$array_id = array();
		$html = "";
		$mode = "sys";
		$parent_act_menu = array();
		$act_menu = ! empty($active_menu) ? $active_menu : '';
		$act_menu_lang = '';
		$act_breadcrumb_lang = '';
		
		$res_menu = $this->CI->def_model->get_menu($mode, array('where' => array('a.name' => $act_menu)))->row_array();
		
		if(! empty($res_menu))
		{
			$act_menu = $res_menu['name'];
			
			$path = json_decode($res_menu['path']);
			
			for($i = 1; $i < count($path)-1; $i++)
			{
				$parent_act_menu[] = $path[$i];
				
				$res_parent = $this->CI->def_model->get_menu($mode, array('where' => array('a.id' => $path[$i])))->row_array();
				
				if(! empty($res_parent))
				{
					$href = $res_parent == 1 ? app_backend_url($res_parent["location"]) : $res_parent["location"];	
					$target = $res_parent["link_target_id"] == 2 ? "_blank" : "";
					
					$act_breadcrumb_lang .= '<li><a href="'.$href.'" target="'.$target.'">'.ucwords($res_parent['description']).'</a> </li>';
				}
			}
					
			$act_breadcrumb_lang .= '<li>'.ucwords($res_menu['description']).'</li>';
			
			$act_menu_lang = $res_menu['description'];
		}
		
		$this->CI->session->set_userdata($this->CI->session_key('active_menu'),$act_menu);
		
		$this->CI->session->set_userdata($this->CI->session_key('active_menu_lang'),$act_menu_lang);
		
		$this->CI->session->set_userdata($this->CI->session_key('active_breadcrumb_lang'),$act_breadcrumb_lang);
		
		$res_menu_user = $this->CI->def_model->get_menu_user($mode)->result_array();
	
		foreach($res_menu_user as $menu)
		{
			if(! in_array($menu['name'], $hidden_menu))
			{
				$id = $menu['id'];
				$parent_id = $menu['parent_id'] === NULL ? "NULL" : $menu['parent_id'];
				
				$data[$id] = $menu;
				$index[$parent_id][] = $id;
			}
		}
		
		function get_child_nodes($parent_id, $level)
		{
			global $index, $data, $html, $array_level, $array_id, $array_menu, $act_menu, $parent_act_menu;
			$parent_id = $parent_id === NULL ? "NULL" : $parent_id;
			
			if(isset($index[$parent_id]))
			{	
				$span_a = '';
				$ul_class = "dropdown-menu";
				if(in_array($parent_id, $parent_act_menu))
				{
					//$ul_class = "dropdown-menu";
				}
				
				if($level != 1)
				{
					$html .= "<ul class='".$ul_class."'>";
				}
				
				
				
				foreach($index[$parent_id] as $id)
				{
					$li_class = '';
					if(isset($index[$id]))
					{
						//print_r( $index[$id]);die();
						//$li_class = 'dropdown-submenu';
						
						if(isset($parent_act_menu[0]) && $parent_act_menu[0] == $data[$id]["id"])
						{
							$li_class .= 'active';
						}
					}
					
					
					if($act_menu == $data[$id]["name"])
					{
						if($level == 1)
						{
							//$span_a = '<span class="selected"></span>';
						}
						$li_class = 'active';
					}
					
					$span_b = '';
					$a_attribute = '';
					$a_class = '';
			
					if($data[$id]["location"] == "#")
					{
						$href = "javascript:;";			
						$span_b = '<span class="arrow"></span>';
						
						if($level == 1)
						{
							$a_attribute = "data-toggle='dropdown' data-hover='dropdown' data-close-others='true'"; 
							$a_class = "dropdown-toggle"; 
						}
						else
						{
							$li_class = 'dropdown-submenu';
						}
					}
					else
					{
						$href = $data[$id]["link_target_id"] == 1 ? app_backend_url($data[$id]["location"]) : $data[$id]["location"];	
						if($href == "''") $href = site_url();
						elseif($data[$id]["location"] == 'backend') $href = site_url($data[$id]["location"]);
					}
					
					$target = $data[$id]["link_target_id"] == 2 ? "_blank" : "";
					
					$html .= '<li class="'.$li_class.'"><a '.$a_attribute.' class="'.$a_class.'" href="'.$href.'" target="'.$target.'" title="'.ucwords($data[$id]["description"]).'">'.$span_a.ucwords(char_limiter($data[$id]["description"],'15')).$span_b.'</a>';
					get_child_nodes($id,$level + 1);
				}
				
				$html .= "</li>";
				
				if($level != 1)
				{
					$html .= "</ul>";
				}
			}
			return $html;
		}
		
		return get_child_nodes(NULL,1);
	}
	
	function get_menu_backend2($active_menu = '')
	{
		global $index, $data, $html, $array_level, $array_id, $act_menu, $parent_act_menu;
		$data = array();
		$index = array();
		$array_level = array();
		$array_id = array();
		$html = "";
		$mode = "sys";
		$parent_act_menu = array();
		$act_menu = ! empty($active_menu) ? $active_menu : 'dashboard';
		$act_menu_lang = '';
		$act_breadcrumb_lang = '';
		
		$res_menu = $this->CI->def_model->get_menu($mode, array('where' => array('a.name' => $act_menu)))->row_array();
		
		if(! empty($res_menu))
		{
			$act_menu = $res_menu['name'];
			
			$path = json_decode($res_menu['path']);
			
			for($i = 1; $i < count($path)-1; $i++)
			{
				$parent_act_menu[] = $path[$i];
				
				$res_parent = $this->CI->def_model->get_menu($mode, array('where' => array('a.id' => $path[$i])))->row_array();
				
				if(! empty($res_parent))
				{
					$href = $res_parent == 1 ? app_backend_url($res_parent["location"]) : $res_parent["location"];	
					$target = $res_parent["link_target_id"] == 2 ? "_blank" : "";
					
					$act_breadcrumb_lang .= '<li><a href="'.$href.'" target="'.$target.'">'.ucwords($res_parent['description']).'</a> <i class="icon-angle-right"></i></li>';
				}
			}
					
			$act_breadcrumb_lang .= '<li>'.ucwords($res_menu['description']).'</li>';
			
			$act_menu_lang = $res_menu['description'];
		}
		
		$this->CI->session->set_userdata($this->CI->session_key('active_menu'),$act_menu);
		
		$this->CI->session->set_userdata($this->CI->session_key('active_menu_lang'),$act_menu_lang);
		
		$this->CI->session->set_userdata($this->CI->session_key('active_breadcrumb_lang'),$act_breadcrumb_lang);
		
		$res_menu_user = $this->CI->def_model->get_menu_user($mode)->result_array();
		
		foreach($res_menu_user as $menu)
		{
			$id = $menu['id'];
			$parent_id = $menu['parent_id'] === NULL ? "NULL" : $menu['parent_id'];
			
			$data[$id] = $menu;
			$index[$parent_id][] = $id;
		}
		
		function get_child_nodes2($parent_id, $level)
		{
			global $index, $data, $html, $array_level, $array_id, $array_menu, $act_menu, $parent_act_menu;
			$parent_id = $parent_id === NULL ? "NULL" : $parent_id;
			
			if(isset($index[$parent_id]))
			{	
				$ul_class = "sub-menu";
				
				if($level != 1)
				{
					$html .= "<ul class='".$ul_class."'>";
				}
				
				foreach($index[$parent_id] as $id)
				{
					$li_class = '';
					$span_a = '';
					$span_b = '';
					$span_b_open = '';
					
					$a_attribute = '';
					$a_class = '';
					
					if(isset($index[$id]))
					{
						//print_r( $index[$id]);die();
						//$li_class = 'dropdown-submenu';
						
						if(isset($parent_act_menu[0]) && $parent_act_menu[0] == $data[$id]["id"])
						{
							$span_a = '<span class="selected"></span>';
							$li_class .= 'active';
							$span_b_open = 'open';
						}
					}
					
					if($act_menu == $data[$id]["name"])
					{
						
						$li_class = 'active';
					}
			
					if($data[$id]["location"] == "#")
					{
						$href = "javascript:;";			
						$span_b = '<span class="arrow '.$span_b_open.'"></span>';
						
						if($level == 1)
						{
							//$a_attribute = "data-hover='dropdown' data-close-others='true'"; 
							//$a_class = "dropdown-toggle"; 
						}
						else
						{
							//$li_class = 'dropdown-submenu';
						}
					}
					else
					{
						$href = $data[$id]["link_target_id"] == 1 ? app_backend_url($data[$id]["location"]) : $data[$id]["location"];	
						if($href == "''") $href = site_url();
						elseif($data[$id]["location"] == 'backend') $href = site_url($data[$id]["location"]);
					}
					
					$target = $data[$id]["link_target_id"] == 2 ? "_blank" : "";
					
					$html .= '<li class="'.$li_class.'"><a '.$a_attribute.' class="'.$a_class.'" href="'.$href.'" target="'.$target.'">'.ucwords($data[$id]["description"]).$span_b.$span_a.'</a>';
					get_child_nodes2($id,$level + 1);
				}
				
				$html .= "</li>";
				
				if($level != 1)
				{
					$html .= "</ul>";
				}
			}
			return $html;
		}
		
		return get_child_nodes2(NULL,1);
	}
	
	function get_menu_config($mode)
	{
		global $index, $data, $html, $array_level, $array_id, $modes;
		$data = array();
		$index = array();
		$array_level = array();
		$array_id = array();
		$html = "";
		$modes = $mode;
		
		$res_menu = $this->CI->def_model->get_menu($mode)->result_array();
	
		foreach($res_menu as $menu)
		{
			$id = $menu['id'];
			$parent_id = $menu['parent_id'] === NULL || empty($menu['parent_id']) ? "NULL" : $menu['parent_id'];
			
			$data[$id] = $menu;
			$index[$parent_id][] = $id;
		}
		
		if (!function_exists('get_child_nodes_config'))
		{
			function get_child_nodes_config($parent_id, $level)
			{
				global $index, $data, $html, $array_level, $array_id, $array_menu, $modes;
				$parent_id = $parent_id === NULL ? "NULL" : $parent_id;
				
				if(isset($index[$parent_id]))
				{	
					if($level != 1)
					{
						$html .= "<ol class='dd-list'>";
					}
					
					foreach($index[$parent_id] as $id)
					{			
						//<label class="radio"><input value="'.$data[$id]["id"].'" id="radio_'.$modes.'_'.$data[$id]["id"].'" mode="'.$modes.'" type="radio" name="radio_'.$modes.'" /></label>
						$html .= '<li class="dd-item dd3-item" data-id="'.$data[$id]["id"].'" id="list_'.$modes.'_'.$data[$id]["id"].'"><div class="dd-handle dd3-handle"></div><div data-id="'.$data[$id]["id"].'" data-mode="'.$modes.'" class="dd3-content">'.ucwords($data[$id]["description"]).'</div>';
						get_child_nodes_config($id,$level + 1);
					}
					
					$html .= "</li>";
					
					if($level != 1)
					{
						$html .= "</ol>";
					}
				}
				return $html;
			}
		}
		return get_child_nodes_config(NULL,1);
	}
	
	function get_menu_group($mode, $group_id)
	{
		global $index, $data, $html, $array_level, $array_id, $modes;
		$data = array();
		$index = array();
		$array_level = array();
		$array_id = array();
		$html = "";
		$modes = $mode;
		
		$res_menu = $this->CI->def_model->get_menu_group($mode,NULL,$group_id)->result_array();
	
		foreach($res_menu as $menu)
		{
			$id = $menu['id'];
			$parent_id = $menu['parent_id'] === NULL || empty($menu['parent_id']) ? "NULL" : $menu['parent_id'];
			
			$data[$id] = $menu;
			$index[$parent_id][] = $id;
		}
		
		if (!function_exists('get_child_nodes_config'))
		{
			function get_child_nodes_config($parent_id, $level)
			{
				global $index, $data, $html, $array_level, $array_id, $array_menu, $modes;
				$parent_id = $parent_id === NULL ? "NULL" : $parent_id;
				
				if(isset($index[$parent_id]))
				{	
					if($level != 1)
					{
						$html .= "<ol class='dd-list'>";
					}
					
					foreach($index[$parent_id] as $id)
					{				
						$checked = '';
						if($data[$id]["is_active_sett"])
						{
							$checked = 'checked="checked"';
						}
						
						$html .= '<li class="dd-item" data-id="'.$data[$id]["id"].'" id="list_'.$modes.'_'.$data[$id]["id"].'"><div class="dd-handle"><input value="'.$data[$id]["id"].'" id="cbox_'.$modes.'_'.$data[$id]["id"].'" mode="'.$modes.'" type="checkbox" name="cbox_'.$modes.'" '.$checked.' menuID="'.$data[$id]["id"].'"><a style="margin-left:5px;" href="#tab-content" id="'.$data[$id]["id"].'" class="menulink">'.ucwords($data[$id]["description"]).'</a></div>';
						get_child_nodes_config($id,$level + 1);
					}
					
					$html .= "</li>";
					
					if($level != 1)
					{
						$html .= "</ol>";
					}
				}
				return $html;
			}
		}
		return get_child_nodes_config(NULL,1);
	}
	
	function set_menu_order($mode, $array)
	{
		global $index, $data, $html, $array_level, $array_id, $modes, $ci;
		$data = array();
		$index = array();
		$array_level = array();
		$array_id = array();
		$html = "";
		$modes = $mode;
		$ci = $this->CI;
		
		foreach($array as $menu)
		{
			if(! empty($menu['item_id']))
			{
				$id = $menu['item_id'];
				$parent_id = $menu['parent_id'] === NULL || empty($menu['parent_id']) ? "NULL" : $menu['parent_id'];
				
				$data[$id] = $menu;
				$index[$parent_id][] = $id;
			}
		}

		function set_child_nodes($parent_id, $level)
		{
			global $index, $data, $array_level, $array_id, $array_menu, $modes, $ci;
			$parent_id = $parent_id === NULL || empty($parent_id) ? "NULL" : $parent_id;
			
			if(isset($index[$parent_id]))
			{	
				$x = 1;
				foreach($index[$parent_id] as $id)
				{			
					if($parent_id == "NULL")
					{
						$path_arr = array((string)0);
					}
					else
					{
						$parent_path = $ci->def_model->get_menu($modes,array('where' => array('a.id' => $parent_id)))->row_array();
						$path_arr = json_decode($parent_path['path']);
					}
					
					array_push($path_arr,(string)$id);
					
					$params = array(
						'table' => $modes.'_menu', 
						'where' => array('id' => $id),
						'data' => array(
							'order_number' => $x,
							'level' => $level,
							'path' => json_encode($path_arr),
							'parent_id' => ($parent_id == "NULL" ? NULL : $parent_id)
						)
					);
					
					$ci->def_model->update($params);
				
					set_child_nodes($id,$level + 1);
					
					$x++;
				}
			}
		}
		
		set_child_nodes(NULL,1);
	}
	
	function get_menu_frontend($active_menu = '')
	{
		global $index, $data, $html, $array_level, $array_id, $x, $act_menu;
		$data = array();
		$index = array();
		$array_level = array();
		$array_id = array();
		$html = "";
		$x = 0;
		$act_menu = $active_menu;
		$mode = "sys";
		$this->CI->session->set_userdata($this->CI->session_key('active_menu'),( ! empty($active_menu) ? $active_menu : 'dashboard'));
		
		$res_menu = $this->CI->def_model->get_menu_new($mode)->result_array();
		
		foreach($res_menu as $menu)
		{
			$id = $menu['id'];
			$parent_id = $menu['parent_id'] === NULL ? "NULL" : $menu['parent_id'];
			
			$data[$id] = $menu;
			$index[$parent_id][] = $id;
		}
		
		function get_child_nodes($parent_id, $level)
		{
			global $index, $data, $html, $array_level, $array_id, $array_menu, $x, $act_menu;
			$parent_id = $parent_id === NULL ? "NULL" : $parent_id;
			
			if(isset($index[$parent_id]))
			{
				$li_class = '';
				if($level != 1)
				{
					$html .= "<ul>";
				}
				
				if(count($index[$parent_id]) > 1)
				{
					
				}
				
				foreach($index[$parent_id] as $id)
				{
					if($level == 1)
					{
						$x = $x + 1;
						if($act_menu)
						{
							$li_class = $x == $act_menu ? 'active' : '';
						}
						/* else
						{
							$li_class = $x == 1 ? 'active' : '';
						} */
					}
					
					$target = '';
					if($data[$id]["url"] != "#")
					{
						$href = 'javascript: fnHitUrl(&quot;'.$url.'&quot;)';
					}
					else
					{
						$href = "#";
					}
					
					$html .= '<li class="'.$li_class.'"><a target="'.$target.'" title="'.$data[$id]["description"].'" href="'.$href.'">'.ucwords($data[$id]["description"]).'</a>';
					get_child_nodes($id,$level + 1);
				}
				
				$html .= "</li>";
				
				if($level != 1)
				{
					$html .= "</ul>";
				}
			}
			return $html;
		}
		
		return get_child_nodes(NULL,1);
	}
}