<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Def_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get_one($prm)
	{
		if(isset($prm['select']))
		{
			$this->db->select($prm['select']);
		}
		
		$this->db->from($prm['table']);
		
		if(isset($prm['join']))
		{
			foreach($prm['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		if(isset($prm['where']))
		{
			$this->db->where($prm['where']);
		}
		
		if(isset($prm['where_string']))
		{
			$this->db->where($prm['where_string']);
			
		}
		
		if(isset($prm['where_like']))
		{
			$this->db->like($prm['where_like']['field'],$prm['where_like']['match'],isset($prm['where_like']['side']) ? $prm['where_like']['side'] : 'both');
		}
		
		if(isset($prm['order_sort']))
		{
			if(is_array($prm['order_sort']))
			{
				foreach($prm['order_sort'] as $order)
				{
					$this->db->order_by($order['sort'], (isset($order['dir']) ? $order['dir'] : 'ASC'));
				}
			}
			else
			{
				$dir = (isset($prm['order_dir']) ? $prm['order_dir'] : 'ASC');
				$this->db->order_by($prm['order_sort'], $dir);
			}
		}
		
		if(isset($prm['start']))
		{
			$this->db->limit(1,$prm['start']);
		}
		else
		{
			$this->db->limit(1);
		}
		
		return $this->db->get();
	}
	
	function get_data($prm,$is_count = FALSE)
	{
		$this->db->from($prm['table']);
		
		if(isset($prm['join']))
		{
			foreach($prm['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		if(isset($prm['where']))
		{
			$this->db->where($prm['where'], FALSE, FALSE);
		}
		
		if(isset($prm['where_in']))
		{
			$this->db->where_in($prm['where_in'][0],$prm['where_in'][1]);
		}
		
		if(isset($prm['where_not_in']))
		{
			$this->db->where_not_in($prm['where_not_in'][0],$prm['where_not_in'][1]);
		}
		$pattern = "((0[1-9]|[12][0-9]|3[01])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d)";
		if(isset($prm['search']) && $prm['search'] != '')
        {
			if($prm['search'] == 'publish' || $prm['search'] == 'active' || $prm['search'] == 'accepted' || $prm['search'] == 'yes')
			{
				$prm['search'] = 1;
			}
			
			if($prm['search'] == 'unpublish' || $prm['search'] == 'inactive' || $prm['search'] == 'not accepted' || $prm['search'] == 'no')
			{
				$prm['search'] = 0;
			}
			
			if(preg_match($pattern, $prm['search'])) 
			{
				$prm['search'] = $this->date_format_Ymd($prm['search'],'/');
			}
			
			if(isset($prm['fields']) && ! empty($prm['fields']))
			{
				$i=0;
				foreach($prm['fields'] as $obj) { 
					if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
					else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
					$i++;
				}
				$where .= ')';
				$this->db->where($where, NULL, FALSE);
				unset($where);
			}
        }
		
		if(isset($prm['fields_2']) && $prm['fields_2'] != '')
		{
			$i=0;
			foreach($prm['fields_2'] as $key => $val) { 
				if($key != 'sys_user.is_active')
				{
					if($val == 'publish' || $val == 'active' || $val == 'accepted' || $val == 'yes')
					{
						$val = 1;
					}

					if($val == 'unpublish' || $val == 'inactive' || $val == 'not accepted' || $val == 'no')
					{
						$val = 0;
					}
				}
				
				$pattern = "((0[1-9]|[12][0-9]|3[01])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d)";
				if(preg_match($pattern, $val)) 
				{
					$val = $this->date_format_Ymd($val,'/');
				}
				
				if($i == 0) $where = '(upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
				else $where .=  'AND upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
				$i++;
			}
			$where .= ')';
			$this->db->where($where, NULL, FALSE);
			unset($where);
		}

		if($is_count)
		{
			$this->db->select('COUNT(*) AS cnt');
		}
		else
		{
			if(isset($prm['select']))
			{
				$this->db->select($prm['select'], isset($prm['escape_select']) ? $prm['escape_select'] : TRUE);
			}
			
			/* if(isset($prm['start']) && isset($prm['limit']))
			{
				if($prm['limit'] > 0)
				{
					$this->db->limit($prm['limit'], $prm['start']);
				}
				else
				{
					if(isset($prm['cnt']) && $prm['cnt'] > 500)
					{
						$this->db->limit(500, $prm['start']);
					}
				}
			} */
			
			if(isset($prm['limit']))
			{
				$prm['start'] = isset($prm['start']) ? $prm['start'] : 0;
				
				if($prm['limit'] > 0)
				{
					$this->db->limit($prm['limit'], $prm['start']);
				}
				else
				{
					if(isset($prm['cnt']) && $prm['cnt'] > 500)
					{
						$this->db->limit(500, $prm['start']);
					}
				}
			}
		}
		
		//sort by
		if(isset($prm['sort']))
		{
			$this->db->order_by($prm['sort'],isset($prm['dir']) ? $prm['dir'] : 'asc');
		}
		
		if(isset($prm['sort_array']))
		{
			foreach($prm['sort_array'] as $sort => $dir)
			{
				 $this->db->order_by($sort,$dir);
			}
		}
		//group by
		if(isset($prm['group_by']))
		{
			$this->db->group_by($prm['group_by']);
		}
		
		if(isset($prm['group_by_array']))
		{
			foreach($prm['group_by_array'] as $val)
			{
				$this->db->group_by($val);
			}
		}
	
		return $this->db->get();
	}
	
	function get_list($prm)
	{
		$this->db->from($prm['table']);
		
		if(isset($prm['join']))
		{
			foreach($prm['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		if(isset($prm['select']))
		{
			$this->db->select($prm['select'],isset($prm['quote']) ? $prm['quote'] : TRUE);
		}
		
		if(isset($prm['where']))
		{
			if(isset($prm['escape']))
			{
				$this->db->where($prm['where'], $prm['escape'], $prm['escape']);
			}
			else
			{
				$this->db->where($prm['where']);
			}
		}
		
		if(isset($prm['where_like']))
		{
			$this->db->like($prm['where_like']['field'],$prm['where_like']['match'],isset($prm['where_like']['side']) ? $prm['where_like']['side'] : 'both');
		}
		
		if(isset($prm['where_not_like']))
		{
			$this->db->not_like($prm['where_not_like']['field'],$prm['where_not_like']['match'],isset($prm['where_not_like']['side']) ? $prm['where_not_like']['side'] : 'both');
		}
		
		if(isset($prm['where_in']))
		{
			$this->db->where_in($prm['where_in'][0],$prm['where_in'][1]);
		}
		
		if(isset($prm['where_not_in']))
		{
			$this->db->where_not_in($prm['where_not_in'][0],$prm['where_not_in'][1]);
		}
		
		if(isset($prm['where_string']))
		{
			$this->db->where($prm['where_string']);
			
		}
		
		if(isset($prm['group_by']))
		{
			$this->db->group_by($prm['group_by']);
		}
		
		if(isset($prm['order_sort']))
		{
			if(is_array($prm['order_sort']))
			{
				foreach($prm['order_sort'] as $order)
				{
					$this->db->order_by($order['sort'], (isset($order['dir']) ? $order['dir'] : 'ASC'));
				}
			}
			else
			{
				$dir = (isset($prm['order_dir']) ? $prm['order_dir'] : 'ASC');
				$this->db->order_by($prm['order_sort'], $dir);
			}
		}
		
		if(isset($prm['limit']))
		{
			$this->db->limit($prm['limit']);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
	function get_max($prm)
	{
		if(isset($prm['select']))
		{
			$this->db->select_max($prm['select']);
		}
		
		if(isset($prm['join']))
		{
			foreach($prm['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		if(isset($prm['where']))
		{
			$this->db->where($prm['where']);
		}
		
		if(isset($prm['where_string']))
		{
			$this->db->where($prm['where_string']);
		}
		
		$query = $this->db->get($prm['table']);
		return $query->row_array();
	}
	
	function get_min($prm)
	{
		if(isset($prm['select']))
		{
			$this->db->select_min($prm['select']);
		}
		
		if(isset($prm['join']))
		{
			foreach($prm['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		if(isset($prm['where']))
		{
			$this->db->where($prm['where']);
		}
		
		if(isset($prm['where_string']))
		{
			$this->db->where($prm['where_string']);
		}
		
		$query = $this->db->get($prm['table']);
		return $query->row_array();
	}
	
	function get_sum($prm)
	{
		if(isset($prm['select']))
		{
			$this->db->select_sum($prm['select']);
		}
		
		if(isset($prm['join']))
		{
			foreach($prm['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		if(isset($prm['where']))
		{
			$this->db->where($prm['where']);
		}
		
		$query = $this->db->get($prm['table']);
		return $query->row_array();
	}
	
	function is_unique($prm)
	{
		$row = $this->count_rows($prm);
		if($row['CNT'] > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function count_rows($prm)
	{
		if(isset($prm['select']))
		{
			$this->db->select($prm['select']);
		}
		else
		{
			$this->db->select('COUNT(*) AS CNT');
		}
		
		if(isset($prm['join']))
		{
			foreach($prm['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		if(isset($prm['where']))
		{
			if(isset($prm['escape']))
			{
				$this->db->where($prm['where'], $prm['escape'], $prm['escape']);
			}
			else
			{
				$this->db->where($prm['where']);
			}
		}
		
		if(isset($prm['where_in']))
		{
			$this->db->where_in($prm['where_in'][0],$prm['where_in'][1]);
		}
		
		if(isset($prm['where_or']))
		{
			$this->db->or_where($prm['where_or']);
		}
		
		if(isset($prm['where_like']))
		{
			$this->db->where($prm['where_like']);
		}
		
		if(isset($prm['group_by']))
		{
			$this->db->group_by($prm['group_by']);
		}
		
		$query = $this->db->get($prm['table']);
		return $query->row_array();
	}
	
	function update($prm)
	{
		if(isset($prm['where']))
		{
			$this->db->where($prm['where']);
		}
		
		if(isset($prm['table']))
		{
			if(isset($prm['data']))
			{
				$res = $this->db->update($prm['table'],$prm['data']);
			}
			
			if($res)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	function trans_update($prm)
	{
		if(isset($prm['where']))
		{
			$this->db->where($prm['where']);
		}
		
		if(isset($prm['table']))
		{
			$this->db->trans_begin();
			if(isset($prm['data']))
			{
				$this->db->update($prm['table'],$prm['data']);
			}
			
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			
			return $this->db->trans_status();
		}
		else
		{
			return FALSE;
		}
	
	}
	
	function insert($prm)
	{
		if(isset($prm['table']))
		{
			if(isset($prm['data']))
			{
				$res = $this->db->insert($prm['table'],$prm['data']);
				//$id = $this->db->insert_id();
			}
			
			if($res)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	function insert_batch($prm)
	{
		if(isset($prm['table']))
		{
			
			$res = $this->db->insert_batch($prm['table'], $prm['data']); 
			if($res)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	function trans_insert($prm)
	{
		if(isset($prm['table']))
		{
			$this->db->trans_begin();
			if(isset($prm['data']))
			{
				$this->db->insert($prm['table'],$prm['data']);
				$id = $this->db->insert_id();
			}
			
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				return FALSE;
			}
			else
			{
				$this->db->trans_commit();
				return $id;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	function delete($prm)
	{
		if(isset($prm['table']))
		{
			if(isset($prm['where']))
			{
				$this->db->where($prm['where']);
				if(isset($prm['where_string']))
				{
					$this->db->where($prm['where_string']);
					
				}
				$del = $this->db->delete($prm['table']);
			}
			
			if($del)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	function trans_delete($prm)
	{
		if(isset($prm['table']))
		{
			$this->db->trans_begin();
			if(isset($prm['where']))
			{
				$this->db->delete($prm['table'],$prm['where']);
			}
			
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			
			return $this->db->trans_status();
		}
		else
		{
			return FALSE;
		}
	}
	
	function paralel_update($prm)
	{
		if(! empty($prm))
		{
			$this->db->trans_begin();
			foreach($prm['input'] as $key => $val)
			{
				if(is_array($val))
				{
					$arr_val = array();
					foreach($val as $real_val)
					{
						$arr_val[] = $real_val;
					}
					//$val = serialize($val);
					$val = json_encode($arr_val);
				}
				
				if(strlen($val) > 0)
				{
					$this->db->where($prm['column_1'], $key);
					$this->db->update($prm['table'], array($prm['column_2'] => trim($val)));
				}
			}
			
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			
			return $this->db->trans_status();
		}
		else
		{
			return FALSE;
		}
	}
	
	function update_batch($prm)
	{
		if(isset($prm['table']))
		{
			$this->db->update_batch($prm['table'], $prm['data'], $prm['where_key']);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function truncate($table)
	{
		$this->db->from($table);
		return $this->db->truncate(); 
	}

	function log_activity($data)
	{
		$prm['table'] = 'sys_user_log';
		$prm['data']  = $data;
		return $this->trans_insert($prm);
	}
	
	function login_logout($data,$id = '')
	{
		if(empty($id))
		{
			$prm['table'] = 'sys_user_login_logout';
			$prm['data']  = $data;
			$this->insert($prm);
			return $this->db->insert_id();
		}
		else
		{
			$prm['table'] = 'sys_user_login_logout';
			$prm['data']  = array('action' => $data['action'], 'logout_datetime' => $data['logout_datetime']);
			$prm['where'] = array('id' => $id);
			
			return $this->update($prm);
		}
	}
	
	///////////////////////////////////////////////// New menu Structure ////////////////////////////////////////////////
	
	function get_menu($prefix,$param = array())
	{
		$this->db->select('a.*,c.description, e.type link_type, f.target link_target');
		$this->db->from($prefix.'_menu a');
		$this->db->join($prefix.'_menu_description c', 'c.'.$prefix.'_menu_id = a.id');
		$this->db->join('sys_language d', 'c.'.$prefix.'_lang_id = d.id');
		$this->db->join('sys_link_type e', 'a.link_type_id = e.id');
		$this->db->join('sys_link_target f', 'a.link_target_id = f.id');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if(isset($param['where_in']))
		{
			$this->db->where_in($param['where_in'][0],$param['where_in'][1]);
		}
		
		$this->db->where('a.is_active', 1);
		$this->db->where('d.name', $this->session->userdata(APPLICATION.'_language'));
		
		$this->db->order_by('a.parent_id');
		$this->db->order_by('a.order_number');
		
		return $this->db->get();
	}
	
	function get_menu_group($prefix,$param = array(),$gc_id)
	{
		$this->db->select('a.*,b.is_active is_active_sett, b.active_function, c.description');
		$this->db->from($prefix.'_menu a');
		$this->db->join($prefix.'_menu_setting b', 'b.'.$prefix.'_menu_id = a.id and b.sys_group_child_id = "'.$gc_id.'"','left');
		$this->db->join($prefix.'_menu_description c', 'c.'.$prefix.'_menu_id = a.id');
		$this->db->join('sys_language d', 'c.'.$prefix.'_lang_id = d.id');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		$this->db->where('a.is_active', 1);
		$this->db->where('d.name', $this->session->userdata(APPLICATION.'_language'));
		
		$this->db->order_by('a.parent_id');
		$this->db->order_by('a.order_number');
		
		return $this->db->get();
	}
	
	function get_menu_user($prefix, $param = array())
	{	
		$this->db->select('a.*,b.is_active is_active_sett, b.active_function, c.description');
		$this->db->from($prefix.'_menu a');
		$this->db->join($prefix.'_menu_setting b', 'b.'.$prefix.'_menu_id = a.id','left');
		$this->db->join($prefix.'_menu_description c', 'c.'.$prefix.'_menu_id = a.id');
		$this->db->join('sys_language d', 'c.'.$prefix.'_lang_id = d.id');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		$this->db->where('a.is_active', 1);
		$this->db->where('b.sys_group_child_id', $this->session->userdata(APPLICATION.'_sys_group_child_id'));
		
		$this->db->where('b.is_active', 1);
		$this->db->where('d.name', $this->session->userdata(APPLICATION.'_language'));
		
		$this->db->order_by('a.parent_id');
		$this->db->order_by('a.order_number');
		
		return $this->db->get();
	}
	
	function delete_menu($mode,$id)
	{
		$query = $this->db->query("
			DELETE ".$mode."_menu, ".$mode."_menu_function, ".$mode."_menu_description, ".$mode."_menu_setting FROM ".$mode."_menu 
			LEFT JOIN ".$mode."_menu_function ON (".$mode."_menu.id = ".$mode."_menu_function.".$mode."_menu_id)
			LEFT JOIN ".$mode."_menu_description ON (".$mode."_menu.id = ".$mode."_menu_description.".$mode."_menu_id)
			LEFT JOIN ".$mode."_menu_setting ON (".$mode."_menu.id = ".$mode."_menu_setting.".$mode."_menu_id)
			WHERE ".$mode."_menu.path REGEXP '([^\"]*\"){$id}(\"[^\"]*)'
		");
		return $query;
	}
	
	function delete_group_child($list_id)
	{
		$query = $this->db->query("
			DELETE sys_group_child, sys_menu_setting, app_menu_setting FROM sys_group_child
			LEFT JOIN sys_menu_setting ON (sys_group_child.id = sys_menu_setting.sys_group_child_id)
			LEFT JOIN app_menu_setting ON (sys_group_child.id = app_menu_setting.sys_group_child_id)
			WHERE sys_group_child.id in ({$list_id}) and sys_group_child.is_deletable = '1'
		");
		return $query;
	}
	
	function delete_categories($id)
	{
		$query = $this->db->query("
			DELETE content_categories, content_categories_description FROM content_categories
			LEFT JOIN content_categories_description ON (content_categories.id = content_categories_description.content_categories_id)
			WHERE content_categories.path REGEXP '([^\"]*\"){$id}(\"[^\"]*)'
		");
		
		return $query;
	}
	
	function get_menu_sett($prefix, $prm)
	{
		$this->db->select("id, is_active, active_function");
        $this->db->from($prefix.'_menu_setting sett');
        $this->db->where($prefix.'_menu_id', $prm['menu_id'], FALSE);
        $this->db->where('sys_group_child_id', $prm['group_id'], TRUE);
        
        $query = $this->db->get();
		
        return $query->row_array();
	}
	
	function get_menu_function($prefix, $prm)
	{
		$this->db->select("a.is_active, b.name");
        $this->db->from($prefix.'_listener_setting a');
		$this->db->join($prefix.'_menu_listener b', 'a.'.$prefix.'_listeners_id = b.id');
		$this->db->join($prefix.'_menu c', 'b.'.$prefix.'_menu_id = c.id');
       
		$this->db->where('a.is_active', 1);
        $this->db->where('a.sys_group_child_id', $prm['group_id'], TRUE);
        $this->db->where('c.id', $prm['menu_id']);
        $query = $this->db->get();
		
        return $query->result_array();
	}
	
	function get_dashboard_setting($is_active,$group_child_id,$param=array(),$is_count = FALSE)
	{
        $this->db->from('sys_dashboard_setting a');
		$this->db->join('sys_dashboard b', 'a.sys_dashboard_id = b.id');
		$this->db->where('a.is_active', $is_active);
        $this->db->where('a.sys_group_child_id', $group_child_id, TRUE);
		$this->db->order_by('a.order_number');
		
		if($is_count)
		{
			$this->db->select('COUNT(*) AS cnt');
		}
		else
		{
			$this->db->select("b.*");
			
			if(isset($param['start']) && isset($param['limit']))
			{
				$this->db->limit($param['limit'], $param['start']);
			}
		}
		
        $query = $this->db->get();
		
        return $query;
	}
	
	function get_user_employee($param,$is_count = FALSE)
	{
		$this->db->from('sys_user a');
        $this->db->join('employee b','a.employee_id = b.id','left');
		$this->db->join('ref_cabang c','b.ref_cabang_id = c.id','left');
		$this->db->join('sys_group_child d','d.id = a.sys_group_child_id','left');
		$this->db->join('ref_jabatan e','e.id = b.ref_jabatan_id','left');
		
		$this->db->where('a.is_deleted', 0);
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}

		if(isset($param['search']) && $param['search'] != "")
        {
        	$i=0;
        	foreach($param['fields'] as $obj) {
			  if($obj == 'user_id') $obj = 'a.id';
			  if($obj == 'user_name') $obj = 'a.name';
			  if($obj == 'user_is_active') $obj = 'a.is_active';
			  if($obj == 'nrp') $obj = 'b.nrp';
			  if($obj == 'name') $obj = 'b.name';
			  if($obj == 'divisi') $obj = 'b.divisi';
			  if($obj == 'ref_cabang_id') $obj = 'b.ref_cabang_id';
			  if($obj == 'ref_status_id') $obj = 'b.ref_status_id';
			  if($obj == 'divisi') $obj = 'b.divisi';
			  if($obj == 'cabang') $obj = 'c.name';
			  if($obj == 'sys_group_child_name') $obj = 'd.name';
			  if($obj == 'sys_group_child_desc') $obj = 'd.description';
			  if($obj == 'jabatan_name') $obj = 'e.name';
			  if($obj == 'jabatan_glosarry') $obj = 'e.glosarry';
			  else $obj = 'b.'.$obj;
	
			  if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  $i++;
			}
            $where .= ')';
            $this->db->where($where, NULL, FALSE);
            unset($where);
        }
		
		$this->db->order_by('a.created_datetime','DESC');
		$this->db->order_by('b.name','ASC');
		
		if(isset($param['sort']) && isset($param['dir'])) 
		{
			$this->db->order_by($param['sort'],$param['dir']);
		}
		
		if($is_count)
		{
			$this->db->select('COUNT(*) AS cnt');
		}
		else
		{
			$this->db->select('
				a.id user_id, a.name user_name, a.last_login last_login, a.is_active user_is_active, a.sys_group_child_id, a.employee_id,
				b.nrp nrp, b.name, b.divisi, b.ref_cabang_id, b.ref_status_id,
				c.name cabang,
				d.name sys_group_child_name, d.description sys_group_child_desc,
				e.name jabatan_name, e.glosarry jabatan_glosarry
			');
			
			if(isset($param['limit']) && isset($param['start'])) 
			{
				$this->db->limit($param['limit'], $param['start']);
			}
		}
		
		return $this->db->get();
	}
	
	function get_articles($param = array(),$image_size = 'ori', $word_limiter = 20, $is_count = FALSE, $review = FALSE)
	{
		$this->db->from('content_articles_posts a');
		$this->db->join('content_articles_categories b','b.id = a.category_id');
		$this->db->join('sys_user c','c.id = a.author_id');
		$this->db->join('employee d','d.id = c.employee_id','left');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if(isset($param['where_sub_cat']))
		{
			$str_where = "sub_category_id REGEXP '([^\"]*\"){$param['where_sub_cat']}(\"[^\"]*)'";
			$this->db->where($str_where,FALSE,FALSE);
		}

		if(! $review)
		{
			$this->db->where('status',1);
		}
		
		if(isset($param['is_public']))
		{
			$this->db->where('b.is_public',$param['is_public']);
		}
		
		if( ! empty($param['search']))
        {
        	$i=0;
        	foreach($param['fields'] as $obj) { 
			  if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  $i++;
			}
            $where .= ')';
            $this->db->where($where, NULL, FALSE);
            unset($where);
        }
		
		if($is_count)
		{
			$this->db->select('COUNT(*) AS cnt');
			return $this->db->get()->row_array();
		}
		else
		{
			$this->db->select('a.*, b.title cat_title, b.slug cat_slug, c.name user_name, d.name employee_name');
			
			if(isset($param['sort']))
			{	
				$this->db->order_by($param['sort'],$param['dir'] ? $param['dir'] : 'asc');
			}
			
			if(isset($param['limit']))
			{
				if(isset($param['start']))
				{
					$this->db->limit($param['limit'], $param['start']);
				}
				else
				{
					$this->db->limit($param['limit']);
				}
			}
			
			$res_articles = $this->db->get()->result_array();
			$arr = array();
			
			foreach($res_articles as $articles)
			{
				$articles['title'] = $articles['title'];
				$articles['intro'] = word_limiter($articles['intro'], $word_limiter);
				$articles['url']   = site_url('web/posts/article/'.$articles['slug']);
				$articles['author'] =  ! empty($articles['employee_name']) ? ucwords($articles['employee_name']) : ucwords($articles['user_name']); 
				$date = date_create($articles['created_datetime']);
				$articles['date']  = date_format($date, 'd M Y'); 
				$articles['cat_title'] = $articles['cat_title'];
				$articles['sub_category_id'] = json_decode($articles['sub_category_id']);
				//$articles['content'] = html_entity_decode($articles['content']);
				$articles['content'] = fixUTF8($articles['content']);
				if(! empty($articles['keywords']))
				{
					$articles['keywords'] = implode(",",json_decode($articles['keywords']));
				}
				
				if(! empty($articles['file_url']))
				{
					$path_files = explode("/",$articles['file_url']);
					$ext = pathinfo($path_files[1], PATHINFO_EXTENSION);
					
					if($ext == 'xls' || $ext == 'xlsx' || $ext == 'csv')
					{
						$ext = 'xls';
					}
					elseif($ext == 'doc' || $ext == 'docx')
					{
						$ext = 'doc';
					}
					elseif($ext == 'ppt' || $ext == 'pptx')
					{
						$ext = 'ppt';
					}
					elseif($ext == 'rar')
					{
						$ext = 'rar';
					}
					elseif($ext == 'zip')
					{
						$ext = 'zip';
					}
					elseif($ext == 'pdf')
					{
						$ext = 'pdf';
					}
					else
					{
						$ext = 'unknown';
					}
					
					if(file_exists(ABSOLUTE_PATH.'assets/upload/files/'.$articles['file_url']))
					{
						$href = app_assets_url('upload/files/'.$articles['file_url']);
					}
					else
					{
						$href = "#";
					}
					
					
					
					if(! preg_match("/software/i", $articles['sub_category']))
					{					
						$articles['file'] = '<a class="'.$ext.'-button" href="'.$href.'" target="_blank"></a>';
					}
					else
					{
						$articles['file'] = '<a id="file" class="'.$ext.'-button" href="javascript: fnQuestion(&quot;'.$href.'&quot;);"></a>';
						
						$articles['file_question'] = $this->def_model->get_list(array('table' => 'content_random_question', 'order_sort' => 'rand()', 'limit' => 5))->result_array();
					}
				}
				
				if(! empty($articles['video_url']))
				{
					if(file_exists(ABSOLUTE_PATH.'assets/upload/videos/'.$articles['video_url']))
					{
						$articles['video_url'] = app_assets_url('upload/videos/'.$articles['video_url']);
					}
				}
				
				if(! empty($articles['image_url']))
				{
					if($image_size == 'ori')
					{
						if(file_exists(ABSOLUTE_PATH.'assets/upload/images/'.$articles['image_url']))
						{
							$articles['image_url'] = app_assets_url('upload/images/'.$articles['image_url']);
						}
						else
						{
							if(isset($param['no_images']))
							{
								app_assets_url('upload/images/no-images/'.$param['no_images']);
							}
							else
							{
								$articles['image_url'] = app_assets_url('upload/images/no-images/no-image640351.jpg');
							}
						}
					}
					else
					{
						$path = explode("/",$articles['image_url']);
						
						if(isset($path[1]))
						{
							$ext_file = pathinfo($path[1], PATHINFO_EXTENSION);
							$file_name = basename($path[1],".".$ext_file);
						}
						else
						{
							$file_name = '';
							$ext_file = '';
						}
						
						if(! empty($file_name) && ! empty($ext_file))
						{
							if(file_exists(ABSOLUTE_PATH.'assets/upload/images/'.$path[0].'/'.$file_name.'_thumb.'.$ext_file))
							{
								$articles['image_url'] = app_asset_url('upload/images/'.$path[0].'/'.$file_name.'_thumb.'.$ext_file);
							}
							else
							{
								if($image_size == 'thumb_6969')
								{
									$articles['image_url'] = app_asset_url('upload/images/no-images/no-image6969.jpg');
								}
								elseif($image_size == 'thumb_8787')
								{
									$articles['image_url'] = app_asset_url('upload/images/no-images/no-image8787.jpg');
								}
								elseif($image_size == 'thumb_146124')
								{
									$articles['image_url'] = app_asset_url('upload/images/no-images/no-image146124.jpg');
								}
								elseif($image_size == 'thumb_200149')
								{
									$articles['image_url'] = app_asset_url('upload/images/no-images/no-image201149.jpg');
								}
								elseif($image_size == 'thumb_280125')
								{
									$articles['image_url'] = app_asset_url('upload/images/no-images/no-image280125.jpg');
								}
							}
						}
					}
				}
				else
				{
					if($image_size == 'thumb_6969')
					{
						$articles['image_url'] = app_asset_url('upload/images/no-images/no-image6969.jpg');
					}
					elseif($image_size == 'thumb_8787')
					{
						$articles['image_url'] = app_asset_url('upload/images/no-images/no-image8787.jpg');
					}
					elseif($image_size == 'thumb_146124')
					{
						$articles['image_url'] = app_asset_url('upload/images/no-images/no-image146124.jpg');
					}
					elseif($image_size == 'thumb_200149')
					{
						$articles['image_url'] = app_asset_url('upload/images/no-images/no-image201149.jpg');
					}
					elseif($image_size == 'thumb_280125')
					{
						$articles['image_url'] = app_asset_url('upload/images/no-images/no-image280125.jpg');
					}
					else
					{
						$articles['image_url'] = app_asset_url('upload/images/no-images/no-image640351.jpg');
					}
				}
				
				$arr[] = $articles;
			}
			
			return $arr;
		}
	}
	
	function get_comments($param)
	{
		$this->db->select('a.*,b.slug, b.title, d.name author_name');
		$this->db->from('content_comments a');
		$this->db->join('content_articles_posts b','b.id = a.posts_id');
		$this->db->join('sys_user c','c.id = a.sys_user_id');
		$this->db->join('employee d','d.id = c.employee_id','left');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if(isset($param['order_sort']))
		{
			$this->db->order_by($param['order_sort'],isset($param['order_dir']) ? $param['order_dir'] : 'asc');
		}
		
		if(isset($param['limit']))
		{
			$this->db->limit($param['limit']);
		}
		
		return $this->db->get();
		
	}
	
	function date_format_Ymd($date,$splitter)
	{
		if(! isset($date) || $date == '') $date = '0000-00-00';
		$d = explode($splitter,$date);
		
		return $d[2].'-'.$d[1].'-'.$d[0];
	}
	
	function get_user_employee2($param)
	{
		$this->db->from('sys_user a');
        $this->db->join('employee b','a.employee_id = b.id','left');
		$this->db->join('sys_group_child c','a.sys_group_child_id = c.id','left');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		$this->db->select('
			a.*,
			b.id emp_id, b.nrp, b.name employee_name, b.divisi, b.email,
			c.name group_name
		');
		
		if(isset($param['limit']))
		{
			$start = isset($param['start']) ? $param['start'] : 0;
			$this->db->limit($param['limit'], $start);
		}
		
		return $this->db->get();
	}
	
	function set_def_pwd()
	{
		$this->db->set('password', 'md5(substr(name,3))', FALSE); 
		$this->db->where('employee_id !=', 0);
		return $this->db->update('sys_user'); 
	}
	
}