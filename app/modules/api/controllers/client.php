<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client extends API_Controller {

	function __construct()
	{
		parent::__construct();

		$this->api_server_sap = "http://frontendsap.ptpp.co.id/pponline/api/";
		//$this->api_server_sap = "http://frontendsap-qa.ptpp.co.id/pponline/api/";
		$this->api_key_sap = "befea63a-7c47-4323-afea-968a3168fe0a";

		$this->api_server_sap_qa = "http://frontendsap-qa.ptpp.co.id/pponline/api/";
		$this->api_key_sap_qa = "ca42e64111e3995229f88238214260c5";

		$this->load->helper(array('convert_date'));
	}
	
	# http://localhost/pp_epc_berkas/api/client/users_sync?usr=ret&pwd=0712f913ed30e7508380f596cf53a4ef&logout=1
	# http://apps.pp-epc.com/dao/api/client/users_sync?usr=ret&pwd=0712f913ed30e7508380f596cf53a4ef&logout=1
	# users
	function users_sync()
	{		
		ini_set('memory_limit','-1');
		@set_time_limit(-1);
		
		$table = 'sys_user';
		$api_module = 'users';
		$api_method = 'user_retrieve';
		
		if($this->input->is_ajax_request())
		{
			$http_usr = $this->http_usr_sdmonline;
			$http_pwd = $this->http_pwd_sdmonline;
			$request_by = $this->get_user_name();
		}
		else
		{
			$http_usr = isset($_REQUEST['usr']) ? $_REQUEST['usr'] : '';
			$http_pwd = isset($_REQUEST['pwd']) ? $_REQUEST['pwd'] : '';
			$request_by = 'cronjob';
		}
		
		// log sync
		$data_log = array(
			'table_sync' => $table,
			'api_key' => $this->api_key_sdmonline,
			'api_server' => $this->api_server_sdmonline.$api_module,
			'api_module' => $api_module,
			'http_usr' => $http_usr,
			'http_pwd' => $http_pwd,
			'request_datetime' => $this->get_date_time(),
			'request_by' => $request_by
		);
		
		// RESTclient webservice
		$this->load->library('rest', array(
			'server' => $this->api_server_sdmonline.$api_module,
			'http_user' => $http_usr,
			'http_pass' => $http_pwd,
			'api_key' => $this->api_key_sdmonline,
			'http_auth' => 'basic'
		));
		
		$users = $this->rest->get($api_method,array('select' => 'name,password,is_active,employee_id'),'application/json');
		
		if(! is_null($users) || ! empty($users))
		{
			if($users->status)
			{
				$data_insert = array();
				$count_insert = $count_update = 0;
				$arr_content = array(); 
				$total = count($users->data);
				$i = 0;
				
				$file_path = ABSOLUTE_PATH.'asset/tmp/'.$api_module.'_'.$this->input->post('sess_id').'.txt';
				foreach($users->data as $dt)
				{
					if($this->input->post('sess_id'))
					{
						$percent = round(($i/$total * 100)); 
						$arr_content['percent'] = $percent;
						$arr_content['message'] = sprintf($this->lang->line('global_data_processed'),$i);

						file_put_contents($file_path, json_encode($arr_content));
						session_write_close();
					}
					
					$res_check = $this->def_model->get_user_employee2(array('where' => array('a.name' => $dt->name), 'limit' => 1))->row_array();
					if(! empty($res_check))
					{
						$data_update = array(
							'password' => $dt->password,
							'employee_id' => $dt->employee_id,
							'is_active' => $dt->is_active						
						);
						
						if($res_check['divisi'] != 'EPC' || empty($dt->employee_id)) // bukan divisi EPC
						{
							$data_update['sys_group_child_id'] = 0;
						}
						
						$prm_update = array(
							'table' => $table,
							'data'  => $data_update,
							'where' => array('name' => $dt->name)
						);
						
						$res_update = $this->def_model->update($prm_update);
						
						if($res_update)
						{
							$count_update++;
						}
					}
					else
					{
						
						$data_insert[] = array(
							'name' => $dt->name,
							'password' => $dt->password,
							'employee_id' => $dt->employee_id,
							'is_active' => $dt->is_active
						);
					}
					
					$i++;
				}
				
				if(! empty($data_insert))
				{
					$prm_insert_batch = array(
						'table' => $table,
						'data'  => $data_insert
					);
					
					$this->def_model->insert_batch($prm_insert_batch);
					
					$count_insert = count($data_insert);
					unset($data_insert);
				}
				
				// sync log
				$data_log['total_data'] = $total;
				$data_log['finish_datetime'] = $this->get_date_time();
				$prm_log = array(
					'table' => 'sync_log',
					'data' => $data_log
				);
				$this->def_model->insert($prm_log);
				unset($prm_log);
				
				$message = sprintf($this->lang->line('global_data_sync_from_module'),$total, $api_module);
				echo $this->output->status_callback('json_success', $message);
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess', $users->error);
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess', 'Singkronisasi gagal.. Coba cek koneksi internet anda!');
		}
		
		if(isset($_REQUEST['logout']) && $_REQUEST['logout'] == 1)
		{
			$this->login_manager->logout();
		}
	}
	
	function users_sync_checker()
	{
		$api_module = 'users';
		
		if($this->input->post('sess_id'))
		{
			$file_path = ABSOLUTE_PATH.'asset/tmp/'.$api_module.'_'.$this->input->post('sess_id').'.txt';
		
			if (file_exists($file_path)) 
			{
				$text = file_get_contents($file_path);
				if(! empty($text))
				{
					$obj = json_decode($text);
					if(isset($obj->percent) && isset($obj->message))
					{
						 echo '{"success": true, "percent": '.$obj->percent.', "message": "'.$obj->message.'"}';
						
						 if (round(intval($obj->percent)) >= 100) 
						 {
							$this->unlink_file($file_path);
						 }
					}
				} 
				else
				{
					 echo '{"success": true, "percent": null, "message": null}';
				}
			}
			else 
			{
			  echo '{"success": true, "percent": null, "message": null}';
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
	
	# http://localhost/pp_epc_berkas/api/client/employees_sync?usr=ret&pwd=0712f913ed30e7508380f596cf53a4ef&logout=1
	# http://apps.pp-epc.com/dao/api/client/employees_sync?usr=ret&pwd=0712f913ed30e7508380f596cf53a4ef&logout=1
	# employees
	function employees_sync()
	{		
		ini_set('memory_limit','-1');
		@set_time_limit(-1);
		$table = 'employee';
		$api_module = 'employees';
		$api_method = 'employee_retrieve';
		
		if($this->input->is_ajax_request())
		{
			$http_usr = $this->http_usr_sdmonline;
			$http_pwd = $this->http_pwd_sdmonline;
			$request_by = $this->get_user_name();
		}
		else
		{
			$http_usr = isset($_REQUEST['usr']) ? $_REQUEST['usr'] : '';
			$http_pwd = isset($_REQUEST['pwd']) ? $_REQUEST['pwd'] : '';
			$request_by = 'cronjob';
		}
		
		// log sync
		$data_log = array(
			'table_sync' => $table,
			'api_key' => $this->api_key_sdmonline,
			'api_server' => $this->api_server_sdmonline.$api_module,
			'api_module' => $api_module,
			'http_usr' => $http_usr,
			'http_pwd' => $http_pwd,
			'request_datetime' => $this->get_date_time(),
			'request_by' => $request_by
		);
		
		// RESTclient webservice
		$this->load->library('rest', array(
			'server' => $this->api_server_sdmonline.$api_module,
			'http_user' => $http_usr,
			'http_pass' => $http_pwd,
			'api_key' => $this->api_key_sdmonline,
			'http_auth' => 'basic'
		));
		
		$employees = $this->rest->get($api_method,array(),'application/json');
		
		if(! is_null($employees) || ! empty($employees))
		{
			if($employees->status)
			{
				// truncate table
				$this->def_model->truncate($table);
				
				$data_insert = array();
				$count_insert = $count_update = 0;
				$arr_content = array(); 
				$total = count($employees->data);
				$i = 0;
				
				$file_path = ABSOLUTE_PATH.'asset/tmp/'.$api_module.'_'.$this->input->post('sess_id').'.txt';
				foreach($employees->data as $dt)
				{
					if($this->input->post('sess_id'))
					{
						$percent = round(($i/$total * 100)); 
						$arr_content['percent'] = $percent;
						$arr_content['message'] = sprintf($this->lang->line('global_data_processed'),$i);

						file_put_contents($file_path, json_encode($arr_content));
						session_write_close();
					}
					
					$data_insert[] = (array)$dt;
					
					$i++;
				}
				
				if(! empty($data_insert))
				{
					$prm_insert_batch = array(
						'table' => $table,
						'data'  => $data_insert
					);
					
					$this->def_model->insert_batch($prm_insert_batch);
					
					$count_insert = count($data_insert);
					unset($data_insert);
					
					// sync log
					$data_log['total_data'] = $total;
					$data_log['finish_datetime'] = $this->get_date_time();
					$prm_log = array(
						'table' => 'sync_log',
						'data' => $data_log
					);
					$this->def_model->insert($prm_log);
					unset($prm_log);
				}
				
				$message = sprintf($this->lang->line('global_data_sync_from_module'),$count_insert, $api_module);
				echo $this->output->status_callback('json_success', $message);
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess', $users->error);
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess', 'Singkronisasi gagal.. Coba cek koneksi internet anda!');
		}
		
		if(isset($_REQUEST['logout']) && $_REQUEST['logout'] == 1)
		{
			$this->login_manager->logout();
		}
	}
	
	function employees_sync_checker()
	{
		$api_module = 'employees';
		
		if($this->input->post('sess_id'))
		{
			$file_path = ABSOLUTE_PATH.'asset/tmp/'.$api_module.'_'.$this->input->post('sess_id').'.txt';
		
			if (file_exists($file_path)) 
			{
				$text = file_get_contents($file_path);
				if(! empty($text))
				{
					$obj = json_decode($text);
					if(isset($obj->percent) && isset($obj->message))
					{
						 echo '{"success": true, "percent": '.$obj->percent.', "message": "'.$obj->message.'"}';
						
						 if (round(intval($obj->percent)) >= 100) 
						 {
							$this->unlink_file($file_path);
						 }
					}
				} 
				else
				{
					 echo '{"success": true, "percent": null, "message": null}';
				}
			}
			else 
			{
				echo '{"success": true, "percent": null, "message": null}';
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}

	# http://localhost/pp_epc_berkas/api/client/references_sync?usr=ret&pwd=0712f913ed30e7508380f596cf53a4ef&logout=1
	# http://apps.pp-epc.com/dao/api/client/references_sync?usr=ret&pwd=0712f913ed30e7508380f596cf53a4ef&logout=1
	# references
	function references_sync()
	{		
		ini_set('memory_limit','-1');
		@set_time_limit(-1);
		$api_module = 'references';
		$api_method = 'reference_retrieve';
		
		if($this->input->is_ajax_request())
		{
			$http_usr = $this->http_usr_sdmonline;
			$http_pwd = $this->http_pwd_sdmonline;
			$request_by = $this->get_user_name();
		}
		else
		{
			$http_usr = isset($_REQUEST['usr']) ? $_REQUEST['usr'] : '';
			$http_pwd = isset($_REQUEST['pwd']) ? $_REQUEST['pwd'] : '';
			$request_by = 'cronjob';
		}
		
		$tables = array(
			'ref_divisi',
			'ref_cabang',
			'ref_proyek_type',
			'ref_proyek',
			'ref_jabatan'
		);
		
		// log sync
		$data_log = array(
			'table_sync' => json_encode($tables),
			'api_key' => $this->api_key_sdmonline,
			'api_server' => $this->api_server_sdmonline.$api_module,
			'api_module' => $api_module,
			'http_usr' => $http_usr,
			'http_pwd' => $http_pwd,
			'request_datetime' => $this->get_date_time(),
			'request_by' => $request_by
		);
		
		// RESTclient webservice
		$this->load->library('rest', array(
			'server' => $this->api_server_sdmonline.$api_module,
			'http_user' => $http_usr,
			'http_pass' => $http_pwd,
			'api_key' => $this->api_key_sdmonline,
			'http_auth' => 'basic'
		));
	
		$data_references = array();
		$total_count_insert = $total_count_update = 0;
		$total_all = 0;
		$t = array();
		foreach($tables as $table)
		{
			$data = $this->rest->get($api_method,array('tb' => $table),'application/json');
			
			if(! empty($data) && $data->status)
			{
				$data_references[$table] = $data;
				
				// truncate table
				$this->def_model->truncate($table);
				$t[$table] = count($data->data);
				$total_all = $total_all + count($data->data);
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess', 'Singkronisasi gagal.. Coba cek koneksi internet anda!');
				exit;
			}
			
		}
		
		$file_path = ABSOLUTE_PATH.'asset/tmp/'.$api_module.'_'.$this->input->post('sess_id').'.txt';
		$count_insert_all = 0;
		$i = 0;
		foreach($data_references as $key_table => $ref)
		{
			$data_insert = array();
			$arr_content = array(); 
			
			foreach($ref->data as $dt)
			{
				if($this->input->post('sess_id'))
				{
					$percent = round(($i/$total_all * 100)); 
					$arr_content['percent'] = $percent;
					$arr_content['message'] = sprintf($this->lang->line('global_data_processed'),$i);

					file_put_contents($file_path, json_encode($arr_content));
					session_write_close();
				}
				
				// khusus divisi epc
				if($key_table == 'ref_proyek' && $dt->id == 780000)
				{
					$dt->ref_cabang_id = 78;
				}
				
				$data_insert[] = (array)$dt;
				
				$i++;
			}
			
			if(! empty($data_insert))
			{
				$prm_insert_batch = array(
					'table' => $key_table,
					'data'  => $data_insert
				);
				
				$this->def_model->insert_batch($prm_insert_batch);
				
				$count_insert_all = $count_insert_all + count($data_insert);
			}
		}
		
		// sync log
		$data_log['total_data'] = $count_insert_all;
		$data_log['finish_datetime'] = $this->get_date_time();
		$prm_log = array(
			'table' => 'sync_log',
			'data' => $data_log
		);
		$this->def_model->insert($prm_log);
		unset($prm_log);
		
		$count_table = count($data_references);
		
		$message = sprintf($this->lang->line('global_data_sync_from_module'),$count_insert_all, $api_module);
		echo $this->output->status_callback('json_success', $message);
		
		
		if(isset($_REQUEST['logout']) && $_REQUEST['logout'] == 1)
		{
			$this->login_manager->logout();
		}
	}
	
	function references_sync_checker()
	{
		$api_module = 'references';
		
		if($this->input->post('sess_id'))
		{
			$file_path = ABSOLUTE_PATH.'asset/tmp/'.$api_module.'_'.$this->input->post('sess_id').'.txt';
		
			if(file_exists($file_path)) 
			{
				$text = file_get_contents($file_path);
				if(! empty($text))
				{
					$obj = json_decode($text);
					if(isset($obj->percent) && isset($obj->message))
					{
						 echo '{"success": true, "percent": '.$obj->percent.', "message": "'.$obj->message.'"}';
						
						 if (round(intval($obj->percent)) >= 100) 
						 {
							$this->unlink_file($file_path);
						 }
					}
				}
				else
				{
					 echo '{"success": true, "percent": null, "message": null}';
				}
			}
			else 
			{
			  echo '{"success": true, "percent": null, "message": null}';
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
	
	function sync_checker()
	{
		$api_module = $this->input->post('api_module');
		
		if($this->input->post('sess_id'))
		{
			$file_path = ABSOLUTE_PATH.'asset/tmp/'.$api_module.'_'.$this->input->post('sess_id').'.txt';
		
			if(file_exists($file_path)) 
			{
				$text = file_get_contents($file_path);
				if(! empty($text))
				{
					$obj = json_decode($text);
					if(isset($obj->percent) && isset($obj->message))
					{
						 echo '{"success": true, "percent": '.$obj->percent.', "message": "'.$obj->message.'"}';
						
						 if (round(intval($obj->percent)) >= 100) 
						 {
							$this->unlink_file($file_path);
						 }
					}
				}
				else
				{
					 echo '{"success": true, "percent": null, "message": null}';
				}
			}
			else 
			{
			  echo '{"success": true, "percent": null, "message": null}';
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}

	function sap_ref_sync()
	{
		ini_set('memory_limit','-1');
		$module = $_GET['mod'];
		$start_date = $this->input->get('start_date');
		$end_date = $this->input->get('end_date');

		if(empty($start_date) && empty($end_date)){
			$end_date = date('Ymd');

			$date = date_create();
			date_sub($date,date_interval_create_from_date_string("7 days"));
			$start_date = date_format($date,"Ymd");
		}

		$table = strtolower('sap_ref_'.$module);

		if(! empty($module) && $this->db->table_exists($table))
		{
			$api_module = $module;
			$api_method = $module;

			// RESTclient webservice
			$this->load->library('rest', array(
				'server' => $this->api_server_sap.$api_module,
				'api_key' => $this->api_key_sap,
				'http_auth' => 'basic'
			));
			$http_user = $this->input->get('usr');
			$http_pwd = $this->input->get('pwd');
			$request_by = '';

			if($this->input->is_cli_request()){
				$request_by = 'cron';
			}else{
				$request_by = $this->get_user_name();
			}

			$data_log = array(
				'table_sync' => $table,
				'api_key' => $this->api_key_sap,
				'api_server' => $this->api_server_sap.$api_module,
				'api_module' => $api_module,
				'http_usr' => $http_user,
				'http_pwd' => $http_pwd,
				'request_datetime' => $this->get_date_time(),
				'request_by' => $request_by
			);

			switch (strtolower($module)){
				CASE 'listpenyerapanpo':

					$res_data = $this->rest->get($api_method,array('im_begda' => $start_date, 'im_endda' => $end_date),'application/json');
					$res_data = json_decode($res_data, true);

					if(! is_null($res_data) && ! empty($res_data))
					{
						$list_filed = $this->db->list_fields($table);

						$data_insert = array();
						$data_insert_all_po = array();
						$i = 0;

						foreach($res_data['T_OUTPUT'] as $dt)
						{
							session_write_close();

							if(isset($dt['AEDAT']))
							{
								$dt['AEDAT'] = substr($dt['AEDAT'],0,4).'-'.substr($dt['AEDAT'],4,2).'-'.substr($dt['AEDAT'],6,2);
							}

							if(isset($dt['BEDAT']))
							{
								$dt['BEDAT'] = substr($dt['BEDAT'],0,4).'-'.substr($dt['BEDAT'],4,2).'-'.substr($dt['BEDAT'],6,2);
							}

							$unitId=null;
							$unitName=null;
							if(!empty($dt['PRCTR']))
							{
								$prctr= substr($dt['PRCTR'],4);
								$prm=array(
									'select' => 't1.ref_cabang_id, t2.name',
									'table' => 'ref_proyek t1',
									'where_string' => 'IFNULL(t1.sap_profit_center, t1.id) = \''.$prctr.'\'',
									'join' => array(
										array(
											'table' => 'ref_cabang t2',
											'on'    => 't2.id = t1.ref_cabang_id',
											'type'  => 'left'
										),
									),
								);
								$this->db->protect_identifier = FALSE;
								$getUnitId= $this->def_model->get_one($prm)->row_array();
								if(!empty($getUnitId))
								{
									$unitId=$getUnitId['ref_cabang_id'];
									$unitName=$getUnitId['name'];
								}
							}


							//CUSTOM DATA FOR all_po
							$po_start_date = NULL;
							$po_end_date = NULL;
							$incoterm = '';
							$incoterm_free_text = '';
							$nomor_kontrak = '';
							$tanggal_kontrak = NULL;
							$tanggal_kontrak_sap = '';
							$nomor_pr = '';

							if(isset($dt['ZSTRDATE']))
							{
								$po_start_date = convert_date($dt['ZSTRDATE'], 'Y-m-d');
							}

							if(isset($dt['ZENDDATE']))
							{
								$po_end_date = convert_date($dt['ZENDDATE'], 'Y-m-d');
							}

							if(isset($dt['INCO1']))
							{
								$incoterm = $dt['INCO1'];
							}

							if(isset($dt['INCO2']))
							{
								$incoterm_free_text = $dt['INCO2'];
							}

							if(isset($dt['TEXT_KONTRAK']))
							{
								$nomor_kontrak = $dt['TEXT_KONTRAK'];
							}

							if(isset($dt['TEXT_PR_MANUAL']))
							{
								$nomor_pr = $dt['TEXT_PR_MANUAL'];
							}

							if(isset($dt['TEXT_TGL_KONTRAK']) && !empty($dt['TEXT_TGL_KONTRAK']))
							{
								if(strpos($dt['TEXT_TGL_KONTRAK'], " ") !== false)
								{
									$tanggal_kontrak = $this->convert_fulldate(trim($dt['TEXT_TGL_KONTRAK']));
									if(!$tanggal_kontrak){
										$tanggal_kontrak = NULL;
										$tanggal_kontrak_sap = $dt['TEXT_TGL_KONTRAK'];
									}
								}else{
									$tanggal_kontrak = convert_date($dt['TEXT_TGL_KONTRAK'], 'Y-m-d');
									if(!$tanggal_kontrak){
										$tanggal_kontrak = NULL;
										$tanggal_kontrak_sap = $dt['TEXT_TGL_KONTRAK'];
									}
								}

							}

							if(substr($dt['LIFNR'],0,4) == '0210')
							{
								$jenis_pengadaan = 1;
							}else{
								$jenis_pengadaan = 0;
							}

							if(substr($dt['EBELN'],0,2) == '46')
							{
								$jenis_kontrak = 1;
							}elseif(substr($dt['EBELN'],0,2) == '41'){
								$jenis_kontrak = 0;
							}else{
								$jenis_kontrak = -3;
							}


							$dt_arr = $dt;
							$dt_arr = array_intersect_key($dt_arr,array_flip($list_filed));
							$data_insert[] = $dt_arr;

							$dt_arr['PO_TYPE']	= 'sap';
							$dt_arr['UNIT_ID']	= $unitId;
							$dt_arr['UNIT_NAME']= $unitName;
							$dt_arr['jenis_pengadaan']= $jenis_pengadaan;
							$dt_arr['jenis_kontrak']= $jenis_kontrak;
							$dt_arr['incoterm']= $incoterm;
							$dt_arr['incoterm_free_text']= $incoterm_free_text;
							$dt_arr['nomor_kontrak']= $nomor_kontrak;
							$dt_arr['nomor_pr']= $nomor_pr;
							$dt_arr['start_date']= $po_start_date;
							$dt_arr['end_date']= $po_end_date;
							$dt_arr['tanggal_kontrak']= $tanggal_kontrak;
							$dt_arr['tanggal_kontrak_sap']= $tanggal_kontrak_sap;
							$data_insert_all_po[] = $dt_arr;

							$i++;
						}

						$tots = 0;
						if(! empty($data_insert))
						{
							$this->db->protect_identifier = FALSE;
							$this->db->on_duplicate($table, $data_insert);
							$tots = $this->db->affected_rows();

						}


						if(! empty($data_insert_all_po))
						{
							$this->db->protect_identifier = FALSE;
							$this->db->on_duplicate('all_po', $data_insert_all_po);
						}

						$data_log['total_data'] = $tots;
						$data_log['finish_datetime'] = $this->get_date_time();
						$prm_log = array(
							'table' => 'sync_log',
							'data' => $data_log
						);
						$this->def_model->insert($prm_log);

						$message = "Singkronisasi ({$tots} data) dari modul: {$api_module}";
						echo '{"success": true, "message": "'.$message.'"}';
					}
					else
					{
						$message = 'Singkronisasi gagal.. Coba cek koneksi internet anda!';
						echo '{"success": false, "message": "'.$message.'"}';
					}

					BREAK;

				CASE 'listvendor':

					$res_data = $this->rest->get($api_method,array(),'application/json');

					if(! is_null($res_data) && ! empty($res_data))
					{
						$list_filed = $this->db->list_fields($table);

						$data_insert = array();
						$i = 0;

						foreach($res_data as $dt)
						{
							session_write_close();

							$dt_arr = (array)$dt;
							$dt_arr = array_intersect_key($dt_arr,array_flip($list_filed));

							$data_insert[] = $dt_arr;

							$i++;

						}

//                        echo json_encode($data_insert);
//                        die();

						if(! empty($data_insert))
						{
							$this->db->protect_identifier = FALSE;
							$this->db->on_duplicate($table, $data_insert);
							$tots = $this->db->affected_rows();
						}

						unset($data_insert);

						$data_log['total_data'] = $tots;
						$data_log['finish_datetime'] = $this->get_date_time();
						$prm_log = array(
							'table' => 'sync_log',
							'data' => $data_log
						);
						$this->def_model->insert($prm_log);

						$message = "Singkronisasi ({$tots} data) dari modul: {$api_module}";
						echo '{"success": true, "message": "'.$message.'"}';

					}
					else
					{
						$message = 'Singkronisasi gagal.. Coba cek koneksi internet anda!';
						echo '{"success": false, "message": "'.$message.'"}';
					}

					BREAK;

				CASE 'kinerjavendor':
					$res_data = $this->rest->get($api_method,array('im_begda' => $start_date, 'im_endda' => $end_date),'application/json');
					$res_data = json_decode($res_data, true);

					if(! is_null($res_data) && ! empty($res_data))
					{
						$list_filed = $this->db->list_fields($table);

						$data_insert = array();
						$i = 0;

						foreach($res_data['T_GRSESINV'] as $dt)
						{
							session_write_close();

							if(isset($dt['BEDAT']))
							{
								$dt['BEDAT'] = convert_date($dt['BEDAT'], 'Y-m-d');
							}

							if(isset($dt['EKBE_BUDAT']))
							{
								$dt['EKBE_BUDAT'] = convert_date($dt['EKBE_BUDAT'], 'Y-m-d');
							}

							$unitId=null;
							$unitName=null;
							if(!empty($dt['PRCTR']))
							{
								$prctr= substr($dt['PRCTR'],4);
								$prm=array(
									'select' => 't1.ref_cabang_id, t2.name',
									'table' => 'ref_proyek t1',
									'where_string' => 'IFNULL(t1.sap_profit_center, t1.id) = \''.$prctr.'\'',
									'join' => array(
										array(
											'table' => 'ref_cabang t2',
											'on'    => 't2.id = t1.ref_cabang_id',
											'type'  => 'left'
										),
									),
								);
								$this->db->protect_identifier = FALSE;
								$getUnitId= $this->def_model->get_one($prm)->row_array();
								if(!empty($getUnitId))
								{
									$unitId=$getUnitId['ref_cabang_id'];
									$unitName=$getUnitId['name'];
								}
							}

							if(substr($dt['LIFNR'],0,4) == '0210')
							{
								$jenis_pengadaan = 1;  //Material

							}else{

								$jenis_pengadaan = 2; //Servis

							}


							if(substr($dt['EBELN'],0,2) == '46')
							{
								$jenis_kontrak = 1; //OA

							}elseif(substr($dt['EBELN'],0,2) == '41'){

								$jenis_kontrak = 2; //Non-OA

							}else{

								$jenis_kontrak = 3; //Z

							}


							$dt_arr = $dt;
							$dt_arr = array_intersect_key($dt_arr,array_flip($list_filed));
							$dt_arr['unit_id']	= $unitId;
							$dt_arr['unit_name']= $unitName;
							$dt_arr['jenis_pengadaan']= $jenis_pengadaan;
							$dt_arr['jenis_kontrak']= $jenis_kontrak;
							$data_insert[] = $dt_arr;

							$i++;
						}

						$tots = 0;
						if(! empty($data_insert))
						{
							//unique key (EKBE_BELNR, EBELP)
							$this->db->protect_identifier = FALSE;
							$this->db->on_duplicate($table, $data_insert);
							$tots = $this->db->affected_rows();

							//truncate mode
//                            $this->def_model->truncate($table);
//                            $prm_insert_batch = array(
//                            'table' => $table,
//                            'data'  => $data_insert
//                            );
//                            $this->def_model->insert_batch($prm_insert_batch);
//                            $tots = $this->db->affected_rows();

						}

						$data_log['total_data'] = $tots;
						$data_log['finish_datetime'] = $this->get_date_time();
						$prm_log = array(
							'table' => 'sync_log',
							'data' => $data_log
						);
						$this->def_model->insert($prm_log);

						$message = "Singkronisasi ({$tots} data) dari modul: {$api_module}";
						echo '{"success": true, "message": "'.$message.'"}';
					}
					else
					{
						$message = 'Singkronisasi gagal.. Coba cek koneksi internet anda!';
						echo '{"success": false, "message": "'.$message.'"}';
					}

					break;

				DEFAULT:
					$res_data = $this->rest->get($api_method,array(),'application/json');

					if(! is_null($res_data) && ! empty($res_data))
					{
						$list_filed = $this->db->list_fields($table);

						$data_insert = array();
						$count_insert = $count_update = 0;
						$arr_content = array();
						$total = count($res_data);
						$i = 0;

						foreach($res_data as $dt)
						{
							session_write_close();

							if(isset($dt->begda))
							{
								$dt->begda = convert_date($dt->begda, 'Y-m-d');
							}

							if(isset($dt->endda))
							{
								$dt->endda = convert_date($dt->endda, 'Y-m-d');
							}

							$dt_arr = (array)$dt;
							$dt_arr = array_intersect_key($dt_arr,array_flip($list_filed));

							$data_insert[] = $dt_arr;

							$i++;
						}

						if(! empty($data_insert))
						{
							// truncate table
							$this->def_model->truncate($table);

							$prm_insert_batch = array(
								'table' => $table,
								'data'  => $data_insert
							);

							$this->def_model->insert_batch($prm_insert_batch);

							$count_insert = count($data_insert);
							unset($data_insert);

							$data_log['total_data'] = $count_insert;
							$data_log['finish_datetime'] = $this->get_date_time();
							$prm_log = array(
								'table' => 'sync_log',
								'data' => $data_log
							);
							$this->def_model->insert($prm_log);
						}

						$message = "Singkronisasi ({$count_insert}) dari modul: {$api_module}";
						echo '{"success": true, "message": "'.$message.'"}';
					}
					else
					{
						$message = 'Singkronisasi gagal.. Coba cek koneksi internet anda!';
						echo '{"success": false, "message": "'.$message.'"}';
					}

			}


			if(isset($_REQUEST['logout']) && $_REQUEST['logout'] == 1)
			{
				$this->login_manager->logout();
			}
		}
		else
		{
			$message = 'Singkronisasi gagal.. Module / Table tidak ditemukan!';
			echo '{"success": false, "message": "'.$message.'"}';
		}
	}
}
