<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My404 extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$http_code = 404;
		$output = json_encode(array('status' => FALSE, 'error' => 'Not Found !!!'));
		
		header('HTTP/1.1: ' . $http_code);
		header('Status: ' . $http_code);
		header('Content-Length: ' . strlen($output));
		header('Content-Type: application/json');

		exit($output);
	}
}