<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->arr_berkas_id = array();
	}
	
	function index()
	{
		$data = array();
		
		// statistic
		$res_pembayaran_hutang = $this->berkas_m->get_total_pembayaran_hutang(array('where' => array('a.payment_method' => $this->ref_payment_method[2])));
		
		$data['total_pembayaran'] = $this->money_format($res_pembayaran_hutang[0]);
		$data['total_hutang_valid'] = $this->money_format($res_pembayaran_hutang[1]);
		
		$res_pembayaran_hutang_ncl = $this->berkas_m->get_total_pembayaran_hutang(array('where' => array('a.payment_method' => $this->ref_payment_method[1])));
		
		$data['total_pembayaran_ncl'] = $this->money_format($res_pembayaran_hutang_ncl[0]);
		$data['total_hutang_valid_ncl'] = $this->money_format($res_pembayaran_hutang_ncl[1]);
		
		$prm = array('table' => 'berkas');
		$proyeks = $this->array_proyeks;
		if(! empty($proyeks))
		{
			$prm['where_in'] = array("project_code",$proyeks);
		}
		
		$prm['where'] = array('berkas_status' => $this->ref_berkas_status[2], 'is_active' => 1);
		$total_berkas_selesai =  $this->def_model->count_rows($prm);
		$data['total_berkas_selesai'] = $total_berkas_selesai['CNT'];
		
		$prm['where'] = array('berkas_status' => $this->ref_berkas_status[1], 'is_active' => 1);
		$total_berkas_proses = $this->def_model->count_rows($prm);
		$data['total_berkas_proses'] = $total_berkas_proses['CNT'];
		
		$res_menu = $this->def_model->get_menu_user('sys')->result_array();
		$short_cut = array();
		$berkas_list = $this->retrive_data_ref($res_menu,'name',array('description','location'),'berkas','');
		$data['short_cut']['berkas_list'] = ! empty($berkas_list) ? explode("-",trim($berkas_list)) : array();
		
		if(! $this->agent->is_mobile())
		{
			$data['stat_status_ncl'] = $this->_get_stat_status($this->ref_payment_method[1]);
			$data['stat_status_reg'] = $this->_get_stat_status($this->ref_payment_method[2]);
		}
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.pulsate.min.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript">var refVerifyStatus = '.json_encode($this->ref_verify_status).'</script>';
		$this->header['js'][]  = '<script type="text/javascript">var groupName = "'.$this->get_group_name2().'"</script>';
		$this->header['js'][]  = '<script type="text/javascript">var jabatanGlosarry = "'.$this->get_jabatan_glosarry().'"</script>';
		
		$data['CTR'] = $this;
		$this->load->view('global/header_view',$this->header);
		$this->load->view('dashboard_view', $data);
		$this->load->view('global/footer_view');
	}
	
	private function _get_stat_status($payment_method, $project_code = "")
	{
		$data = array();
		
		$stat_head = $this->get_verify_level($payment_method);
		$data['stat_head'] = $stat_head;
		
		$where = array('payment_method' => "'".$payment_method."'");
		if(! empty($project_code))
		{
			$where['project_code'] = $project_code;
		}
		
		$res_project = $this->berkas_m->get_data(array('group_by' => 'project_code', 'where' => $where, 'select' => 'a.project_code, `b`.`name` project_name, sum(is_rilis) total_rilis'))->result_array();
		
		$stat_status = array();
		$arr_sum = array();
		$sum_total_rilis = $sum_total_selesai = 0;
		foreach($res_project as $project)
		{
			$data_stat = array();
			
			$data_stat['project_code'] = $project['project_code'];
			$data_stat['project_name'] = char_limiter($project['project_name'],30);
			
			$prm['where'] = array('payment_method' => "'".$payment_method."'", 'project_code' => $project['project_code'], 'is_rilis' => 1);
			$total_rilis = $this->berkas_m->get_data($prm, TRUE)->row_array();
			$data_stat['total_rilis'] = $total_rilis['cnt'];
			$sum_total_rilis = $sum_total_rilis + $total_rilis['cnt'];
			
			$prm['where'] = array('payment_method' => "'".$payment_method."'", 'project_code' => $project['project_code'], 'is_rilis' => 1, 'berkas_status' => "'".$this->ref_berkas_status[2]."'");
			$total_selesai = $this->berkas_m->get_data($prm, TRUE)->row_array();
			$data_stat['total_selesai'] = $total_selesai['cnt'];
			$sum_total_selesai = $sum_total_selesai + $total_selesai['cnt'];
			
			$i = 1;
			foreach($stat_head as $head)
			{
				$arr_ver_status = array();
				
				foreach($this->ref_verify_status as $ver_status)
				{
					$total_status = $this->berkas_m->get_berkas_verify(array(
						'select' => 'count(*) as cnt',
						'where' => array('is_active' => 1,'payment_method' => $payment_method, 'project_code' => $project['project_code'], 'is_rilis' => 1, 'verify_level' => $head['verify_level'], 'verify_status' => $ver_status), 
						'order_sort' => array(
							array('sort' => 'berkas_id', 'dir' => 'asc'),
							array('sort' => 'verify_level_index', 'dir' => 'asc'),
						),
						'join' => array(
							array(
								'table' => "berkas b",
								'on'    => "a.berkas_id = b.id",
								'type'  => 'inner'
							)
						)
					))->row_array();
					
					$arr_ver_status[$ver_status] = $total_status['cnt'];
					
					if(! isset($arr_sum[$i]))
					{
						$arr_sum[$i] = $total_status['cnt'];
					}
					else
					{
						$arr_sum[$i] = $arr_sum[$i] + $total_status['cnt'];
					}
					
					$i++;
				}
				
				$data_stat['total_verify'][$head['verify_level']] = $arr_ver_status;
			}
			
			$stat_status[] = $data_stat;
		}
		
		$data['stat_status'] = $stat_status;
		$data['stat_sum_rilis'] = $sum_total_rilis;
		$data['stat_sum_selesai'] = $sum_total_selesai;
		$data['stat_sum_arr'] = $arr_sum;
		
		return $data;
	}
	
	function get_berkas_proyek()
	{
		$prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
		$prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;
		
		if($this->input->post('iSortCol_0'))
		{
			$sort_idx = $_POST['iSortCol_0'];
			if($_POST['bSortable_'.$sort_idx] == "true")
			{
				$prm['sort'] = $this->_get_select_name($_POST['mDataProp_'.$sort_idx]);
			}
			else
			{
				$prm['sort'] = "a.id";
			}
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC"; 
		}
		
		if($this->input->post('sSearch'))
		{
			$prm['search'] = $this->input->post('sSearch');
			
			if(strtolower($prm['search']) == 'no' || strtolower($prm['search']) == 'yes')
			{
				$prm['search'] = strtolower($prm['search']) == 'yes' ? '1' : '0';
			}
			
			for($i = 0; $i < $_POST['iColumns']; $i++)
			{
				if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
				{
					$prm['fields'][] =  $this->_get_select_name($_POST['mDataProp_'.$i]);
				}
			}
		}
		
		for($i = 0; $i < $_POST['iColumns']; $i++)
		{
			if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
			{
				if($this->input->post('sSearch_'.$i))
				{
					if($this->_get_select_name($_POST['mDataProp_'.$i]) == "a.is_rilis")
					{
						if(strtolower($this->input->post('sSearch_'.$i)) == 'no' || strtolower($this->input->post('sSearch_'.$i)) == 'yes')
						{
							$is_rilis = strtolower($this->input->post('sSearch_'.$i)) == 'yes' ? 1 : 0;
							$prm['fields_2'][$this->_get_select_name($_POST['mDataProp_'.$i])] = $is_rilis;
						}
					}
					else
					{
						$prm['fields_2'][$this->_get_select_name($_POST['mDataProp_'.$i])] = $this->input->post('sSearch_'.$i);
					}
				}
			}
		}
		
		$prm['group_by'] = "project_code";
		
		$res_cnt = $this->berkas_m->get_data($prm,TRUE)->result_array(); 
		$res_cnt['cnt'] = isset($res_cnt) ? count($res_cnt) : 0; // override for grouping
		
		$prm['cnt'] = $res_cnt['cnt'];
		$res_data = $this->berkas_m->get_data($prm)->result_array();
		
		$i = $prm['start'] + 1;
        $arr = array();
        foreach($res_data as $data) 
		{
			$data['DT_RowId'] = $data['id'];
			$data['rnum'] = $i;
			
			$prm_detail = base64_encode($data['project_code']);
			$data['project_codes'] = $data['project_code'];
			$data['project_code'] = "<a href='".app_backend_url('backend/berkas_proyek_detail/'.$prm_detail)."' title='".$this->lang->line('global_view')."'>".$data['project_code']."</a>";
			
			$data['project_name'] = "<span title='".$data['project_name']."'>".char_limiter($data['project_name'], 45)."</span>";
			
			$res_pembayaran_hutang = $this->berkas_m->get_total_pembayaran_hutang(array('where' => array('project_code' => $data['project_codes'])));
			$data['total_hutang_valid'] = $this->money_format($res_pembayaran_hutang[1]);
			$data['total_pembayaran'] = $this->money_format($res_pembayaran_hutang[0]);
			
			$prm = array('table' => 'berkas');
			$prm['where'] = array('berkas_status' => $this->ref_berkas_status[2], 'is_active' => 1, 'project_code' => $data['project_codes']);
			$total_berkas_selesai =  $this->def_model->count_rows($prm);
			$data['total_berkas_selesai'] = "<p class='center'><span class='btn mini green-stripe'>".$total_berkas_selesai['CNT']."</span></p>";
			
			$prm['where'] = array('berkas_status' => $this->ref_berkas_status[1], 'is_active' => 1, 'project_code' => $data['project_codes']);
			$total_berkas_proses = $this->def_model->count_rows($prm);
			$data['total_berkas_proses'] = "<p class='center'><span class='btn mini yellow-stripe'>".$total_berkas_proses['CNT']."</span></p>";
			
			$prm['where'] = array('berkas_status' => $this->ref_berkas_status[3], 'is_active' => 1, 'project_code' => $data['project_codes']);
			$total_berkas_batal = $this->def_model->count_rows($prm);
			$data['total_berkas_batal'] = "<p class='center'><span class='btn mini red-stripe'>".$total_berkas_batal['CNT']."</span></p>";
			
            $arr[] = $data;
            $i++;
        }

        echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
        unset($arr);
	}
	
	private function _get_select_name($val)
	{
		if($val == 'project_name') $r_val = "b.name";
		else $r_val = "a.".$val;
		
		return $r_val;
	}
	
	function berkas_proyek_detail($param)
	{
		$project_code = base64_decode($param);
		$check_berkas_proyek = $this->berkas_m->get_berkas(array('where' => array('a.project_code' => $project_code)))->result_array();
			
		if(! empty($check_berkas_proyek))
		{
			$data = array();
			
			$data['CTR'] = $this;
			
			$data['res_berkas_proyek'] = $check_berkas_proyek;
			
			$res_pembayaran_hutang = $this->berkas_m->get_total_pembayaran_hutang(array('where' => array('a.payment_method' => $this->ref_payment_method[2], 'project_code' => $check_berkas_proyek[0]['project_code'])));
		
			$data['total_pembayaran'] = $this->money_format($res_pembayaran_hutang[0]);
			$data['total_hutang_valid'] = $this->money_format($res_pembayaran_hutang[1]);
			
			$res_pembayaran_hutang_ncl = $this->berkas_m->get_total_pembayaran_hutang(array('where' => array('a.payment_method' => $this->ref_payment_method[1], 'project_code' => $check_berkas_proyek[0]['project_code'])));
			
			$data['total_pembayaran_ncl'] = $this->money_format($res_pembayaran_hutang_ncl[0]);
			$data['total_hutang_valid_ncl'] = $this->money_format($res_pembayaran_hutang_ncl[1]);
			
			$prm = array('table' => 'berkas');
			$prm['where'] = array('berkas_status' => $this->ref_berkas_status[2], 'is_active' => 1, 'project_code' => $check_berkas_proyek[0]['project_code']);
			$total_berkas_selesai =  $this->def_model->count_rows($prm);
			$data['total_berkas_selesai'] = $total_berkas_selesai['CNT'];
			
			$prm['where'] = array('berkas_status' => $this->ref_berkas_status[1], 'is_active' => 1, 'project_code' => $check_berkas_proyek[0]['project_code']);
			$total_berkas_proses = $this->def_model->count_rows($prm);
			$data['total_berkas_proses'] = $total_berkas_proses['CNT'];
			
			$data['param_filter'] = base64_encode("project_code={$check_berkas_proyek[0]['project_code']}");
			
			$data['stat_status_ncl'] = $this->_get_stat_status($this->ref_payment_method[1], $check_berkas_proyek[0]['project_code']);
			$data['stat_status_reg'] = $this->_get_stat_status($this->ref_payment_method[2], $check_berkas_proyek[0]['project_code']);
			
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';
			
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
			
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.pulsate.min.js').'"></script>';
			
			$this->header['js'][]  = '<script type="text/javascript">var refVerifyStatus = '.json_encode($this->ref_verify_status).'</script>';
			$this->header['js'][]  = '<script type="text/javascript">var groupName = "'.$this->get_group_name2().'"</script>';
			$this->header['js'][]  = '<script type="text/javascript">var jabatanGlosarry = "'.$this->get_jabatan_glosarry().'"</script>';
			
			$this->load->view('global/header_view',$this->header);
			$this->load->view('main/berkas_proyek_detail_view', $data);
			$this->load->view('global/footer_view');
		}
		else
		{
			redirect('backend/404');
		}
	}
	
	function get_berkas_proyek_list()
	{
		$prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
		$prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;
		
		if($this->input->post('iSortCol_0'))
		{
			$sort_idx = $_POST['iSortCol_0'];
			if($_POST['bSortable_'.$sort_idx] == "true")
			{
				$prm['sort'] = $this->_get_select_name2($_POST['mDataProp_'.$sort_idx]);
			}
			else
			{
				$prm['sort'] = "a.id";
			}
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC"; 
		}
		else
		{
			$prm['sort'] = "a.id";
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC"; 
		}
		
		if($this->input->post('sSearch'))
		{
			$prm['search'] = $this->input->post('sSearch');
			
			if(strtolower($prm['search']) == 'no' || strtolower($prm['search']) == 'yes')
			{
				$prm['search'] = strtolower($prm['search']) == 'yes' ? '1' : '0';
			}
			
			for($i = 0; $i < $_POST['iColumns']; $i++)
			{
				if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
				{
					$prm['fields'][] =  $this->_get_select_name2($_POST['mDataProp_'.$i]);
				}
			}
		}
		
		for($i = 0; $i < $_POST['iColumns']; $i++)
		{
			if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
			{
				if($this->input->post('sSearch_'.$i))
				{
					if($this->_get_select_name2($_POST['mDataProp_'.$i]) == "a.is_rilis")
					{
						if(strtolower($this->input->post('sSearch_'.$i)) == 'no' || strtolower($this->input->post('sSearch_'.$i)) == 'yes')
						{
							$is_rilis = strtolower($this->input->post('sSearch_'.$i)) == 'yes' ? 1 : 0;
							$prm['fields_2'][$this->_get_select_name2($_POST['mDataProp_'.$i])] = $is_rilis;
						}
					}
					else
					{
						$prm['fields_2'][$this->_get_select_name2($_POST['mDataProp_'.$i])] = $this->input->post('sSearch_'.$i);
					}
				}
			}
		}
		
		if($this->input->post('paramFilter'))
		{
			$param_filter = base64_decode($this->input->post('paramFilter'));
			list($col_filter,$val_filter) = explode("=",$param_filter);
			$prm['where'][$col_filter] = "'".$val_filter."'";
		}

		
		$res_cnt  = $this->berkas_m->get_data($prm,TRUE)->row_array();
		$res_cnt['cnt'] = isset($res_cnt['cnt']) ? $res_cnt['cnt'] : 0;
		$prm['cnt'] = $res_cnt['cnt'];
		$res_data = $this->berkas_m->get_data($prm)->result_array();
		
		$i = $prm['start'] + 1;
        $arr = array();
        foreach($res_data as $data) 
		{
			$data['DT_RowId'] = $data['id'];
			$data['rnum'] = $i;
			
			$data['vendor_name'] = "<span title='".$data['vendor_name']."'>".char_limiter($data['vendor_name'], 25)."</span>";
			
			
			$data['last_verify_statuss'] = $data['last_verify_status'];
			if($data['last_verify_status'] == $this->ref_verify_status[3])
			{
				$label_v_status = "label-important";
				
				$data['last_verify_status'] = "<p class='center'><span class='label ".$label_v_status." popovers' data-trigger='hover' data-placement='right' data-original-title='".$this->lang->line('berkas_rejected_note')."' data-html='true' data-content='".$this->sentence_case_ucfirst($data['rejected_note'], '<br>')."'>".$data['last_verify_status']."</span></p>";
			}
			else
			{	
				if($data['last_verify_status'] == $this->ref_verify_status[1])
				{
					$label_v_status = "label-warning";
				}
				else
				{
					$label_v_status = "label-success";
				}
				
				$data['last_verify_status'] = '<p class="center"><span class="label '.$label_v_status.'">'.$data['last_verify_status'].'</span></p>';
			}
			
			$data['is_riliss'] = $data['is_rilis'];
			if($data['is_riliss'])
			{
				$rilis_date = $this->convert_date($data['rilis_datetime'],'d M Y H:i');
				$data['is_rilis'] = "<p class='center'><span class='icon-ok popovers' data-trigger='hover' data-placement='right' data-original-title='".$this->lang->line('berkas_released_date')."' data-html='false' data-content='".$rilis_date."' ></span></p>";
			}
			else
			{
				$data['is_rilis'] = '<p class="center"><span class="icon-remove"></span></p>';
			}
			
			$label_b_status = "";
			if($data['berkas_status'] == $this->ref_berkas_status[1])
			{
				$label_b_status = "btn mini yellow-stripe";
			}
			elseif($data['berkas_status'] == $this->ref_berkas_status[2])
			{
				$label_b_status = "btn mini green-stripe";
			}
			elseif($data['berkas_status'] == $this->ref_berkas_status[3])
			{
				$label_b_status = "btn mini red-stripe";
			}
			
			$data['berkas_statuss'] = $data['berkas_status'];
			$data['berkas_status'] = '<p class="center"><span class="'.$label_b_status.'">'.(! empty($data['berkas_status']) ? $data['berkas_status'] : '-').'</span></p>';
			
			$data['revisions'] = $data['revision'];
			$data['revision'] = '<p class="center">'.(! empty($data['revision']) ? $data['revision'] : '-').'</p>';
			
			$prm_detail = base64_encode($data['id']);
			
			if(isset($val_filter))
			{
				$prm_detail .= "/".base64_encode(app_backend_url('berkas_proyek_detail/'));
			}
			
			$input_button = "";
			
			if($data['is_riliss'])
			{
				$cls_blink = '';
				if($data['last_verify_statuss'] == $this->ref_verify_status[1] && preg_match("/{$data['last_verify_level']}/i", $this->get_jabatan_glosarry()))
				{
					$cls_blink = 'blink';				
				}
				
				$input_button = "<a href='".app_backend_url('main/berkas/detail/'.$prm_detail)."' title='".$this->lang->line('global_detail')."'><span class='icon-eye-open {$cls_blink}'></span></a><br>";
				
				if(isset($this->site_config['module_function']['revision']) && $data['last_verify_statuss'] == $this->ref_verify_status[3])
				{
					$input_button .= "<a href='".app_backend_url('main/berkas/input/'.$prm_detail.'/1')."' title='".$this->lang->line('berkas_revision_need')."'><span class='icon-warning-sign'></span></a><br>";
				}
			}

			$data['actions']  = "<p class='center'>";
			$data['actions'] .= $input_button;
			$data['actions'] .= "</p>";
			
			$res_pembayaran_hutang = $this->berkas_m->get_total_pembayaran_hutang(array('where' => array('project_code' => $data['project_code'],'a.id' => $data['id'])));
			$data['total_hutang_valid'] = $this->money_format($res_pembayaran_hutang[1]);
			$data['total_pembayaran'] = $this->money_format($res_pembayaran_hutang[0]);
			
            $arr[] = $data;
            $i++;
        }

        echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
        unset($arr);
	}
	
	private function _get_select_name2($val)
	{
		if($val == 'project_name') $r_val = "b.name";
		else $r_val = "a.".$val;
		
		return $r_val;
	}
	
	function check_tracking()
	{
		$keyword = $this->input->post('keyword');
		
		$check_keyword = $this->berkas_m->get_berkas(array('where' => array('concat(project_code,berkas_number)' => $keyword)))->row_array();
		
		if(! empty($check_keyword))
		{
			echo $this->output->status_callback('json_success', base64_encode($check_keyword['id']));
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess', $this->lang->line('berkas_tracking_notfound'));
		}
	}
	
	function view_tracking($param = 0)
	{
		$id = base64_decode($param);
		$check_berkas = $this->berkas_m->get_berkas(array('where' => array('a.id' => $id)))->row_array();
		
		if(! empty($check_berkas))
		{
			$data = array();
			
			$data['res_berkas'] = $check_berkas;
			$data['id'] = $id;
			$data['CTR'] = $this;
			
			$ids = $this->get_all_id($id);
			$data['res_berkas_verify'] = $this->berkas_m->get_berkas_verify(array(
				'select' => 'b.revision, b.revision_datetime, a.*',
				'where_in' => array('berkas_id', $ids), 
				'order_sort' => array(
					array('sort' => 'berkas_id', 'dir' => 'asc'),
					array('sort' => 'verify_level_index', 'dir' => 'asc'),
				),
				'join' => array(
					array(
						'table' => "berkas b",
						'on'    => "a.berkas_id = b.id",
						'type'  => 'inner'
					)
				)
			))->result_array();
			
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
			
 			$this->load->view('global/header_view',$this->header);
			$this->load->view('main/berkas_tracking_view', $data);
			$this->load->view('global/footer_view');
		}
		else
		{
			redirect('backend/404');
		}
	}
	
	function get_all_id($id)
	{	
		$check_berkas = $this->def_model->get_one(array('table' => 'berkas','where' => array('id' => $id)))->row_array();
		
		$this->arr_berkas_id[] = $check_berkas['id'];
		if(! empty($check_berkas['parent_id']))
		{
			return $this->get_all_id($check_berkas['parent_id']);
		}
		else
		{	
			return array_reverse($this->arr_berkas_id);
		}
	}
	
	function get_vendor_by_project()
	{
		$prm['group_by'] = 'vendor_name';
		
		if($this->input->post('project_code'))
		{
			$prm['where'] = array('project_code' => $this->input->post('project_code'));
		}
		
		$ref_vendor = $this->berkas_m->get_data($prm)->result_array();
		
		$html = '';
		$html .= '<option value="">'.$this->lang->line('global_all').'</option>';
		foreach($ref_vendor as $dt)
		{
			$html .= '<option value="'.$dt['vendor_name'].'">'.$dt['vendor_name'].'</option>';
		}
		
		echo $html;
	}
	
	function reset_application_data()
	{
		$tables = array(
		  'berkas','berkas_files','berkas_files','berkas_log','berkas_pembayaran','berkas_verify','activities_log','sync_log','sys_user_log','sys_user_login_logout'
		);
		
		foreach($tables as $table)
		{
			$this->def_model->truncate($table);
		}
		
		echo $this->output->status_callback('json_success', "All Done...");
	}
	
	function set_default_pwd()
	{
		$res = $this->def_model->set_def_pwd();
		
		if($res)
		{
			echo $this->output->status_callback('json_success', "All Done...");
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess', $this->lang->line('global_error'));
		}
	}
}