<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function get_list()
	{
		$prm['table']   = 'employee';
		$prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
		$prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;
		
		if($this->input->post('iSortCol_0'))
		{
			$sort_idx = $_POST['iSortCol_0'];
			if($_POST['bSortable_'.$sort_idx] == "true")
			{
				$prm['sort'] = $this->_get_select_name($_POST['mDataProp_'.$sort_idx]);
			}
			else
			{
				$prm['sort'] = 'employee.name';
			}
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "ASC"; 
		}
		
		if($this->input->post('sSearch'))
		{
			$prm['search'] = $this->input->post('sSearch');
			
			for($i = 0; $i < $_POST['iColumns']; $i++)
			{
				if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true" )
				{
					$prm['fields'][] = $this->_get_select_name($_POST['mDataProp_'.$i]);
				}
			}
		}
		
		$prm['select'] = "employee.*,
		ref_status.name status, 
		ref_jabatan.glosarry jabatan_glosarry, 
		ref_cabang.name cabang";
	
		$prm['join'][] = array(
						'table' => "ref_status",
						'on'    => "ref_status.id = employee.ref_status_id",
						'type'  => 'left'
					);
		$prm['join'][] = array(
						'table' => "ref_jabatan",
						'on'    => "ref_jabatan.id = employee.ref_jabatan_id",
						'type'  => 'left'
					);
		$prm['join'][] = array(
						'table' => "ref_cabang",
						'on'    => "ref_cabang.id = employee.ref_cabang_id",
						'type'  => 'left'
					);
		
		$prm['where'] = array(
			"ref_status.ref_status_cat_id != " => 4
		);
		
		$res_data = $this->def_model->get_data($prm)->result_array(); //echo $this->db->last_query();die();
		$res_cnt  = $this->def_model->get_data($prm,TRUE)->row_array();
		
		$i = $prm['start'] + 1;
        $arr = array();
        foreach($res_data as $data) 
		{
			$data['DT_RowId'] = $data['id'];
			$data['rnum'] = $i;
		
            $arr[] = $data;
            $i++;
        }

        echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
        unset($arr);
	}
	
	private function _get_select_name($val)
	{
		if($val == 'jabatan_glosarry') $r_val = 'ref_jabatan.glosarry';
		elseif($val == 'status') $r_val = 'ref_status.name';
		elseif($val == 'cabang') $r_val = 'ref_cabang.name';
		else $r_val = 'employee.'.$val;
		
		return $r_val;
	}
}