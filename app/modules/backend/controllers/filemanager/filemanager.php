<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filemanager extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('file_manager');
		$file_manager = new File_manager();
	}
	
	function index()
	{
		$root_dir =  isset($_GET['root_dir']) ? $_GET['root_dir'] : $this->file_manager->root_dir;
		
		if($this->session->userdata($this->session_key('sys_group_child_id')) != 1)
		{
			//$this->session->set_userdata('filemanager_root_dir',$this->file_manager->root_dir.md5($this->get_user_name()).'/');
			$new_dir = $root_dir.'/'.md5($this->get_user_name());
			$this->create_dir($this->file_manager->base_path().$new_dir);
			$this->file_manager->set_root_dir($new_dir);
		}
		
		
		
		$type =  isset($_GET['type']) ? $_GET['type'] : '';
		$field_id = isset($_GET['field_id']) ? $_GET['field_id'] : '';
		$current_dir = $this->input->get('req_dir') ? $this->input->get('req_dir') : $this->file_manager->root_dir;
		$thumbs =  isset($_GET['thumbs']) ? $_GET['thumbs'] : '';
		
		$this->session->set_userdata('filemanager_current_dir',$current_dir);
		$this->session->set_userdata('filemanager_type',$type);
		$this->session->set_userdata('filemanager_field_id',$field_id);
		
		$data['js'][] = '<script type="text/javascript">
			APP_FILEMANAGER_URL = "'.app_backend_url('filemanager/').'",
			LANG_INVALID_EXT = "'.$this->lang->line('filemanager_error_invalid_ext').'",
			LANG_UPLOAD_ERROR = "'.$this->lang->line('filemanager_error_upload').'",
			LANG_SERVER_ERROR = "'.$this->lang->line('filemanager_error_server').'",
			MAX_UPLOAD_SIZE = '.$this->file_manager->max_upload_size.';
			'.$this->js_array($this->file_manager->images_ext,'images_ext').'
			'.$this->js_array($this->file_manager->allowed_ext(),'allowed_ext').'
		</script>';
	
		$data['content'] = $this->file_manager->get_content($current_dir, $type, $field_id, $thumbs, $root_dir);
		$data['root_dir_url'] = "?req_dir={$this->file_manager->root_dir}&type={$type}&field_id={$field_id}&thumbs={$thumbs}&root_dir={$root_dir}";
		$data['current_dir_url'] = "?req_dir={$current_dir}&type={$type}&field_id={$field_id}&thumbs={$thumbs}&root_dir={$root_dir}";
		$data['current_dir'] = $current_dir;
		$data['field_id'] = $field_id;
		$data['thumbs'] = $thumbs;
		$data['breadcrumb'] = $this->file_manager->get_breadcrumb($current_dir, $type, $field_id);
		$data['type'] = $type;

		$this->load->view('filemanager/filemanager_view', $data);
	}
	
	function get_content()
	{
		$html = $this->file_manager->get_content($this->input->post('type'));
		echo $this->output->status_callback('json_success', $html);
	}
	
	function force_download()
	{
		if($this->input->post('name') && $this->input->post('path'))
		{	
			$full_path =  $this->input->post('path').$this->input->post('name');
			header('Pragma: private');
			header('Cache-control: private, must-revalidate');
			header("Content-Type: application/octet-stream");
			header("Content-Length: " .(string)(filesize($full_path)) );
			header('Content-Disposition: attachment; filename="'.($this->input->post('name')).'"');
			readfile($full_path);
		}
	}
	
	function execute($action)
	{
		if(isset($action))
		{
			$cur_dir   = $this->input->post('cur_dir') ? $this->input->post('cur_dir') : $this->file_manager->root_dir;
			$item_name = $this->input->post('item_name') ? $this->input->post('item_name') : '';
			$item_old  = $this->input->post('item_old') ? $this->input->post('item_old') : '';
			
			$path     = $this->file_manager->base_path().$cur_dir.'/'.$item_name;
			$path_old = $this->file_manager->base_path().$cur_dir.'/'.$item_old;
			
			$message = '';
			$success = TRUE;
			
			switch($action)
			{
				case 'create_folder' :
				
					$this->create_dir($path);
					
					$path_thumbs = $this->file_manager->change_path_thumbs($path);
					
					$this->create_dir($path_thumbs);
				
				break;
				
				case 'rename_folder' :
				
					try{
						if(is_writable($path_old) == FALSE) 
						{
							throw new Exception(sprintf($this->lang->line('filemanager_error_perm_rename_folder'),htmlspecialchars($item_old)));
						}
						
						if(@rename($path_old, $path) == FALSE) 
						{
							
							throw new Exception(sprintf($this->lang->line('filemanager_error_rename_folder'),htmlspecialchars($item_old)));
						}
						else
						{
							$path_thumbs_old = $this->file_manager->change_path_thumbs($path_old);
							$path_thumbs = $this->file_manager->change_path_thumbs($path);
							rename($path_thumbs_old, $path_thumbs);
						}
						
					} catch(Exception $e) {
						$success = FALSE;
						$message = $e->getMessage();
					}
					
				break;
				
				case 'delete_folder' :
				
					try{
						
						if(is_writable($path) == FALSE) 
						{
							throw new Exception(sprintf($this->lang->line('filemanager_error_perm_delete_folder'),htmlspecialchars($item_name)));
							//throw new Exception("You do not have permission to delete '" . htmlspecialchars($item_name) . "'");
						}
						
						if(@rmdir($path) == FALSE) 
						{
							foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $p) 
							{
								if($p->isFile())
								{
									if(@unlink($p->getPathname()) == FALSE)
									{
										throw new Exception(sprintf($this->lang->line('filemanager_error_delete_file'),htmlspecialchars($item_name)));
										//throw new Exception("Could not delete file '" . htmlspecialchars($item_name) . "' ");
									}
									else
									{
										if(in_array($this->file_manager->get_file_ext($p->getFilename()), $this->file_manager->images_ext))
										{
											$this->file_manager->delete_thumbs($p->getPath(), $p->getFilename());
										}
									}
								}
								else
								{
									if(@rmdir($p->getPathname()) == FALSE) 
									{
										throw new Exception(sprintf($this->lang->line('filemanager_error_delete_folder'),htmlspecialchars($item_name)));
										//throw new Exception("Could not delete folder '" . htmlspecialchars($item_name) . "' ");
									}
									else
									{
										$path_thumbs = $this->file_manager->change_path_thumbs($p->getPathname());
										rmdir($path_thumbs);
									}
								}
							}
							
							if(@rmdir($path) == FALSE) 
							{
								throw new Exception(sprintf($this->lang->line('filemanager_error_delete_folder'),htmlspecialchars($item_name)));
								//throw new Exception("Could not delete folder '" . htmlspecialchars($item_name) . "' ");
							}
							else
							{
								$path_thumbs = $this->file_manager->change_path_thumbs($path);
								rmdir($path_thumbs);
							}
						}
						else
						{
							$path_thumbs = $this->file_manager->change_path_thumbs($path);
							rmdir($path_thumbs);
						}
						
					} catch(Exception $e) {
						$success = FALSE;
						$message = $e->getMessage();
					}
			
				break;
				
				case 'rename_file' :
				
					try{
						if(is_writable($path_old) == FALSE) 
						{
							throw new Exception(sprintf($this->lang->line('filemanager_error_perm_rename_file'),htmlspecialchars($item_old)));
							//throw new Exception("You do not have permission to rename '" . htmlspecialchars($item_old) . "'");
						}
						
						if(@rename($path_old, $path.'.'.$this->file_manager->get_file_ext($item_old)) == FALSE) 
						{
							throw new Exception(sprintf($this->lang->line('filemanager_error_rename_file'),htmlspecialchars($item_old)));
							//throw new Exception("Could not rename '" . htmlspecialchars($item_old) . "' ");
						}
						
					} catch(Exception $e) {
						$success = FALSE;
						$message = $e->getMessage();
					}
			
				break;
				
				case 'delete_file' :
				
					try{
						if(is_file($path) == FALSE) 
						{
							throw new Exception(sprintf($this->lang->line('filemanager_error_is_not_file'),htmlspecialchars($item_name)));
							//throw new Exception("'" . htmlspecialchars($path) . "' is not a file");
						}
						
						if(file_exists($path) == FALSE) 
						{
							throw new Exception(sprintf($this->lang->line('filemanager_error_is_not_file'),htmlspecialchars($path)));
							//throw new Exception("The file '" . htmlspecialchars($path) . "' does not exist");
						}
						
						if(is_writable($path) == FALSE) 
						{
							throw new Exception(sprintf($this->lang->line('filemanager_error_perm_delete_file'),htmlspecialchars($item_name)));
						}
						
						if(@unlink($path) == FALSE) {
							throw new Exception(sprintf($this->lang->line('filemanager_error_delete_file'),htmlspecialchars($item_name)));
							//throw new Exception("Could not delete file '" . htmlspecialchars($item_name) . "' ");
						}
						
						if(in_array($this->file_manager->get_file_ext($item_name), $this->file_manager->images_ext))
						{
							$this->file_manager->delete_thumbs($this->file_manager->base_path().$cur_dir.'/', $item_name);
						}
						
					} catch(Exception $e) {
						$success = FALSE;
						$message = $e->getMessage();
					}
			
				break;
				
				case 'extract' :
					$file_ext = $this->file_manager->get_file_ext($item_name);
					
					if($file_ext != 'rar')
					{
						$zip = new ZipArchive;
						$res = $zip->open($path);
						if ($res === TRUE) 
						{	
							$base_folder = $this->file_manager->base_path().$cur_dir.'/';
						
							//make all the folders
							for($i = 0; $i < $zip->numFiles; $i++) 
							{ 
								$only_file_name = $zip->getNameIndex($i);
								$full_file_name = $zip->statIndex($i);    
								if ($full_file_name['name'][strlen($full_file_name['name'])-1] == "/")
								{
									$this->create_dir($base_folder.$full_file_name['name']);
								}
								
								
							}
							
							//unzip into the folders
							for($i = 0; $i < $zip->numFiles; $i++) 
							{ 
								$only_file_name = $zip->getNameIndex($i);
								$full_file_name = $zip->statIndex($i);    
								
								if (! ($full_file_name['name'][strlen($full_file_name['name'])-1] =="/"))
								{
									$fileinfo = pathinfo($only_file_name);
									if(in_array(strtolower($fileinfo['extension']),$this->file_manager->allowed_ext()))
									{
										copy('zip://'. $path .'#'. $only_file_name , $base_folder.$full_file_name['name'] ); 
									} 
								}
							}
							$zip->close($res);
						}
						else 
						{
							$success = FALSE;
							$message = $this->lang->line('filemanager_error_open_file');
						}
					}
					else
					{
						$success = FALSE;
						$message = $this->lang->line('filemanager_error_open_file');
						/* $rar_file = rar_open($path);
						if($rar_file === TRUE)
						{
							$list = rar_list($rar_file);
							
							foreach($list as $file) 
							{
								$entry = rar_entry_get($rar_file, $item_name);
								$entry->extract("."); // extract to the current dir
							}
							
							rar_close($rar_file);
						}
						else
						{
							$success = FALSE;
							$message = 'Failed to open file';
						} */
					}
				break;
				
				default:
				
					$success = FALSE;
					$message = $this->lang->line('filemanager_error_unknown_action');
					
				break;
			}
			
			$data = array(
				'success'	=> $success,
				'message'	=> $message
			);
			
			echo json_encode($data);
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess', $this->lang->line('filemanager_error_something_wrong'));
		}
	}
	
	function upload_multi_files()
	{
		$dir  = $this->input->post('dir') ? $this->input->post('dir') : $this->file_manager->root_dir;
		$path = $this->file_manager->base_path().$dir;
		$type = $this->session->userdata('filemanager_type');
	
		$this->create_dir($path);
		//print_r($_FILES['file']['name']);die();
		$file_name = strtolower($_FILES['file']['name']);
		if($this->input->post('time_filename'))
		{
			$file_ext = array_pop(explode(".",$file_name));
			$file_name = md5(time().$file_name).".".$file_ext;
			$config['file_name'] = $file_name;
		}
		
		
		$config['upload_path'] 	 = $path;
		//$config['allowed_types'] = $this->file_manager->allowed_ext(TRUE);
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$config['overwrite']	 = FALSE;
		$config['remove_spaces'] = TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			//echo "{ success: false, message:'".$this->upload->display_errors()."' }";
			echo $this->output->status_callback('json_unsuccess',$this->upload->display_errors());
		}	
		else
		{
			$data = $this->upload->data();
			$data['url'] = app_asset_url($dir.'/'.$file_name);
			$data['path'] = $dir.'/'.$file_name;
			if(in_array(str_replace(".", "", $data['file_ext']), $this->file_manager->images_ext))
			{
				if( ! empty($this->file_manager->max_image_size))
				{
					ini_set('gd.jpeg_ignore_warning', 1);
					$this->load->library('image_lib');
					
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path.'/'.$data['file_name'];
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['quality'] = 100;
					
					if($data['image_width'] > $this->file_manager->max_image_size['width'])
					{
						$config['width'] = $this->file_manager->max_image_size['width'];
					}
					
					if($data['image_height'] > $this->file_manager->max_image_size['height'])
					{
						$config['height'] = $this->file_manager->max_image_size['height'];
					}
					
					$this->image_lib->initialize($config);  
					$this->image_lib->resize();
					$this->image_lib->clear();
				}
				
				//$this->file_manager->create_thumbs($dir, $data['file_name']);
			}
			
			//echo "{ success: true, info:".json_encode($data)." }";
			echo $this->output->status_callback('json_success',$data);
		}
	}
}