<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Globals extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function set_language()
	{	
		$this->set_lang($this->input->post('lang'));
		echo $this->output->status_callback('json_success');
	}
	
	function session_check()
	{
		echo ($this->sess_check() ? $this->output->status_callback('json_success') : $this->output->status_callback('json_unsuccess'));
	}
	
	function upload_multi_files($type = 'images', $mode = '')
	{
		$dir_name = md5($this->get_user_name());
		
		if(! empty($mode))
		{
			if($type == 'images')
			{
				$path = ABSOLUTE_PATH.'assets/upload/project/'.$type.'/'.$dir_name.'/';
			}
			else
			{
				$path = ABSOLUTE_PATH.'assets/upload/project/'.$type.'/'.$mode.'/'.$dir_name.'/';
			}
		}
		else
		{
			$path = ABSOLUTE_PATH.'assets/upload/'.$type.'/'.$dir_name.'/';
		}
		
		$this->create_dir($path);
		
		$config['upload_path'] 	 = $path;
		$config['allowed_types'] = $type == 'images' ? '*' : '*';
		$config['overwrite']	 = FALSE;
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('uploadField'))
		{
			echo "{ success: false, info:'".$this->upload->display_errors()."' }";
		}	
		else
		{
			$data = $this->upload->data();
			$data['url'] = app_assets_url('upload/'.$type.'/'.$dir_name.'/'.$data['file_name']);
			$data['file_url'] = $dir_name.'/'.$data['file_name'];
			
			if($type == 'images')
			{
				$config['image_library'] = 'gd2';
				$config['source_image'] = $path.$data['file_name'];
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = FALSE;
				$config['width'] = 200;
				$config['height'] = 149;
				
				ini_set('gd.jpeg_ignore_warning', 1);
				$this->load->library('image_lib');
				
				$this->image_lib->initialize($config); 
				$this->image_lib->resize();
				
				$this->image_lib->clear();
				
				$config['image_library'] = 'gd2';
				$config['source_image'] = $path.$data['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['width'] = 600;
				$config['height'] = 351;
				
				$this->image_lib->initialize($config);  
				$this->image_lib->resize();
				
				$file = explode(".",$data['file_name']);
				$data['file_thumb_url'] = $dir_name.'/'.$file[0].'_thumb.'.$file[1];
			}
			
			echo "{ success: true, info:".json_encode($data)." }";
		}
	}
}