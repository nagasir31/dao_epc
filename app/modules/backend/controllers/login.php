<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$this->words['PLEASE_WAIT'] = $this->lang->line('global_please_wait');  
		
		$this->header['js'][] = '<script type="text/javascript">
			var BASE_URL = "'.base_url().'",
			BASE_ASSET_URL = "'.base_asset_url().'",
			SITE_URL = "'.site_url().'",
			APP_BACKEND_URL = "'.app_backend_url().'",
			APP_NAME = "'.$this->site_config['app_name'].'",
			APP_ASSET_BACKEND_URL = "'.app_asset_backend_url().'",
			DEFAULT_THEME = "'.$this->site_config['app_def_theme'].'";
			'.$this->js_array($this->words,'LANGUAGE').'
		</script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap/css/bootstrap.min.css').'" />';
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap/css/bootstrap-responsive.min.css').'" />';
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/font-awesome/css/font-awesome.min.css').'" />';
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/style-metro.css').'" />';
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/style.css').'" />';
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/style-responsive.css').'" />';
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/themes/default.css').'" />';
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/uniform/css/uniform.default.css').'" />';
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
		$this->header['css'][] = '<link rel="shortcut icon" href="'.app_asset_backend_url('img/favicon.ico').'">';
		
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-1.10.1.min.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-migrate-1.2.1.min.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap/js/bootstrap.min.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js').'"></script>';
		$this->header['js'][] = '<!--[if lt IE 9]><script src="'.app_asset_backend_url('plugins/excanvas.min.js').'"></script><script src="'.app_asset_backend_url('plugins/respond.min.js').'"></script><![endif]-->';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-slimscroll/jquery.slimscroll.min.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.blockui.min.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.cookie.min.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/uniform/jquery.uniform.min.js').'"></script>';
		
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-validation/jquery.validate.min.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/backstretch/jquery.backstretch.min.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';
		
		$this->header['js'][] = '<script type="text/javascript" src="'.app_modules_backend_url('views/global/app.js').'"></script>';
		$this->header['js'][] = '<script type="text/javascript" src="'.app_modules_backend_url('views/global/header.js?n='.time()).'"></script>';

		//print_r($_SESSION);die('d');
		if($this->session->userdata(APPLICATION.'_user_name') != '')
		{
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/pages/lock.css').'" />';
			$this->load->view('lock_view', $this->header);
		}
		else
		{
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/pages/login-soft.css').'" />';
			$this->load->view('login_view', $this->header);
		}
	}
	
	function login_process()
	{
		if($this->input->post('usr') &&  $this->input->post('pwd'))
		{
			$process = $this->login_manager->login_process($this->input->post());
			
			if($process)
			{
				$last_url = $this->input->cookie('last_url_'.APPLICATION) ? base64_decode($this->input->cookie('last_url_'.APPLICATION)) : '';
				$this->log_activity('login');
				$this->login_logout('login');
				echo $this->output->status_callback('json_success', $last_url);
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess',$this->lang->line('global_user_not_found'));
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}	
	
	function logout()
	{
		
		$this->log_activity('logout');
		$this->login_logout('logout');
		$this->login_manager->logout();
		//unset($_COOKIE);
		redirect('backend');
	}
	
	function lock()
	{
		$this->log_activity('lock');
		$this->login_logout('logout');
		$this->login_manager->lock();
		redirect('backend');
	}
}