<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berkas extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->check_module();
		$this->table = $this->class;
		$this->lang->load('employee', $this->get_lang());
		$this->lang->load('users', $this->get_lang());
	}
	
	function index($param_filter = "")
	{
		$data = array();
		
		if(! empty($param_filter))
		{
			$data['param_filter'] = $param_filter;
		}
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.pulsate.min.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript">var refVerifyStatus = '.json_encode($this->ref_verify_status).'</script>';
		$this->header['js'][]  = '<script type="text/javascript">var groupName = "'.$this->get_group_name2().'"</script>';
		$this->header['js'][]  = '<script type="text/javascript">var jabatanGlosarry = "'.$this->get_jabatan_glosarry().'"</script>';
		
		$this->load->view('global/header_view',$this->header);
		$this->load->view('main/'.strtolower($this->class).'_list_view', $data);
		$this->load->view('global/footer_view');
	}
	
	function get_list()
	{
		$prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
		$prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;
		
		if($this->input->post('iSortCol_0'))
		{
			$sort_idx = $_POST['iSortCol_0'];
			if($_POST['bSortable_'.$sort_idx] == "true")
			{
				$prm['sort'] = $this->_get_select_name($_POST['mDataProp_'.$sort_idx]);
			}
			else
			{
				$prm['sort'] = "a.id";
			}
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC"; 
		}
		else
		{
			$prm['sort'] = "a.id";
			$prm['dir']  = "DESC"; 
		}
		
		if($this->input->post('sSearch'))
		{
			$prm['search'] = $this->input->post('sSearch');
			
			if(strtolower($prm['search']) == 'no' || strtolower($prm['search']) == 'yes')
			{
				$prm['search'] = strtolower($prm['search']) == 'yes' ? '1' : '0';
			}
			
			for($i = 0; $i < $_POST['iColumns']; $i++)
			{
				if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
				{
					$prm['fields'][] =  $this->_get_select_name($_POST['mDataProp_'.$i]);
				}
			}
		}
		
		for($i = 0; $i < $_POST['iColumns']; $i++)
		{
			if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
			{
				if($this->input->post('sSearch_'.$i))
				{
					if($this->_get_select_name($_POST['mDataProp_'.$i]) == "{$this->table}.is_rilis")
					{
						if(strtolower($this->input->post('sSearch_'.$i)) == 'no' || strtolower($this->input->post('sSearch_'.$i)) == 'yes')
						{
							$is_rilis = strtolower($this->input->post('sSearch_'.$i)) == 'yes' ? 1 : 0;
							$prm['fields_2'][$this->_get_select_name($_POST['mDataProp_'.$i])] = $is_rilis;
						}
					}
					else
					{
						$prm['fields_2'][$this->_get_select_name($_POST['mDataProp_'.$i])] = $this->input->post('sSearch_'.$i);
					}
				}
			}
		}
		
		if($this->input->post('paramFilter'))
		{
			$param_filter = base64_decode($this->input->post('paramFilter'));
			
			$arr_filter = explode(",",$param_filter);
			
			foreach($arr_filter as $dt)
			{
				list($col_filter,$val_filter) = explode("=",$dt);
				$prm['where'][$col_filter] = "'".$val_filter."'";
			}
		}
		
		$res_cnt  = $this->berkas_m->get_data($prm,TRUE)->row_array(); 
		$res_cnt['cnt'] = isset($res_cnt['cnt']) ? $res_cnt['cnt'] : 0;
		$prm['cnt'] = $res_cnt['cnt'];
		$res_data = $this->berkas_m->get_data($prm)->result_array();
		
		$i = $prm['start'] + 1;
        $arr = array();
        foreach($res_data as $data) 
		{
			$data['DT_RowId'] = $data['id'];
			$data['rnum'] = $i;
			
			$data['vendor_name'] = '<span data-original-title="'.$data['vendor_name'].'" data-placement="top" class="tooltips">'.char_limiter($data['vendor_name'],25).'</span>';
			$data['project_codes'] = $data['project_code'];
			$data['project_code'] = "<span style='color:#35AA47' class='popovers' data-trigger='hover' data-placement='right' data-original-title='".$this->lang->line('berkas_project_name')."' data-html='false' data-content='".$data['project_name']."'>".$data['project_code']."</span>";
						
			$data['last_verify_statuss'] = $data['last_verify_status'];
			if($data['last_verify_status'] == $this->ref_verify_status[3])
			{
				$label_v_status = "label-important";
				
				$data['last_verify_status'] = "<p class='center'><span class='label ".$label_v_status." popovers' data-trigger='hover' data-placement='right' data-original-title='".$this->lang->line('berkas_rejected_note')."' data-html='true' data-content='".$this->sentence_case_ucfirst($data['rejected_note'], '<br>')."'>".$data['last_verify_status']."</span></p>";
			}
			else
			{	
				if($data['last_verify_status'] == $this->ref_verify_status[1])
				{
					$label_v_status = "label-warning";
				}
				else
				{
					$label_v_status = "label-success";
				}
				
				$data['last_verify_status'] = '<p class="center"><span class="label '.$label_v_status.'">'.$data['last_verify_status'].'</span></p>';
			}
			
			$data['is_riliss'] = $data['is_rilis'];
			if($data['is_riliss'])
			{
				$rilis_date = $this->convert_date($data['rilis_datetime'],'d M Y H:i');
				$data['is_rilis'] = "<p class='center'><span class='icon-ok popovers' data-trigger='hover' data-placement='right' data-original-title='".$this->lang->line('berkas_released_date')."' data-html='false' data-content='".$rilis_date."' ></span></p>";
			}
			else
			{
				$data['is_rilis'] = '<p class="center"><span class="icon-remove"></span></p>';
			}
			
			$data['revisions'] = $data['revision'];
			$data['revision'] = '<p class="center">'.(! empty($data['revision']) ? $data['revision'] : '-').'</p>';
			
			
			$label_b_status = "";
			if($data['berkas_status'] == $this->ref_berkas_status[1])
			{
				$label_b_status = "btn mini yellow-stripe";
			}
			elseif($data['berkas_status'] == $this->ref_berkas_status[2])
			{
				$label_b_status = "btn mini green-stripe";
			}
			elseif($data['berkas_status'] == $this->ref_berkas_status[3])
			{
				$label_b_status = "btn mini red-stripe";
			}
			
			$data['berkas_statuss'] = $data['berkas_status'];
			$data['berkas_status'] = '<p class="center"><span class="'.$label_b_status.'">'.(! empty($data['berkas_status']) ? $data['berkas_status'] : '-').'</span></p>';
			
			$prm_detail = base64_encode($data['id']);
			$input_button = "";
			
			if($data['is_riliss'])
			{
				$cls_blink = '';
				if($data['last_verify_statuss'] == $this->ref_verify_status[1] && $data['last_verify_level'] == $this->get_group_name2())
				{
					$cls_blink = 'blink';				
				}
				
				$input_button = "<a href='".app_backend_url('main/berkas/detail/'.$prm_detail)."' title='".$this->lang->line('global_detail')."'><span class='icon-eye-open {$cls_blink}'></span></a><br>";
				
				if(isset($this->site_config['module_function']['revision']) && $data['last_verify_statuss'] == $this->ref_verify_status[3])
				{
					$input_button .= "<a href='".app_backend_url('main/berkas/input/'.$prm_detail.'/1')."' title='".$this->lang->line('berkas_revision_need')."'><span class='icon-warning-sign'></span></a><br>";
				}
			}
			else
			{
				if(isset($this->site_config['module_function']['edit']))
				{
					$input_button = "<a href='".app_backend_url('main/berkas/input/'.$prm_detail)."' title='".$this->lang->line('global_edit')."'><span class='icon-edit'></span></a><br>";
				}
				
				if(isset($this->site_config['module_function']['del']) && $data['revisions'] == 0)
				{
					$input_button .= "<a id='button-delete' data-id='".$data['id']."' data-berkas_number='".$data['berkas_number']."' title='".$this->lang->line('global_delete')."'><span class='icon-trash'></span></a><br>";
				}
			}
			
		
			$data['actions']  = "<p class='center'>";
			$data['actions'] .= $input_button;
			$data['actions'] .= "</p>";
			
            $arr[] = $data;
            $i++;
        }

        echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
        unset($arr);
	}
	
	private function _get_select_name($val)
	{
		return $val;
	}
	
	function delete()
	{
		if($this->input->post('id'))
		{	
			if($this->berkas_m->delete_berkas($this->input->post('id')))
			{
				$path = ABSOLUTE_PATH.'asset/berkas_files/'.md5($this->input->post('berkas_number'));
				
				$this->recursiveRemoveDirectory($path);
				
				echo $this->output->status_callback('json_success');	
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
	}
	
	function input($id = "", $is_revision = 0)
	{
		$data = array();
		$prm_proyek['table'] = 'ref_proyek';
		
		if(! empty($id))
		{
			
			$id = base64_decode($id);
			
			$data['res_data'] = $this->def_model->get_one(array('table' => $this->table,'where' => array('id' => $id)))->row_array();
			
		
			if(empty($data['res_data']))
			{
				redirect('backend/404');
			}
			else
			{
				$prm_proyek['where']['id'] = $data['res_data']['project_code'];
				
				if(isset($data['res_data']['invoice_date']) && $data['res_data']['invoice_date'] != '0000-00-00' && ! empty($data['res_data']['invoice_date'])) 
				{
					$data['res_data']['invoice_date'] =  $this->date_format_dmY($data['res_data']['invoice_date'],'-');
				}
				else
				{
					$data['res_data']['invoice_date'] = '';
				}
				
				if(isset($data['res_data']['ttap_date']) && $data['res_data']['ttap_date'] != '0000-00-00' && ! empty($data['res_data']['ttap_date'])) 
				{
					$data['res_data']['ttap_date'] =  $this->date_format_dmY($data['res_data']['ttap_date'],'-');
				}
				else
				{
					$data['res_data']['ttap_date'] = '';
				}
				
				if(isset($data['res_data']['faktur_pajak_date']) && $data['res_data']['faktur_pajak_date'] != '0000-00-00' && ! empty($data['res_data']['faktur_pajak_date'])) 
				{
					$data['res_data']['faktur_pajak_date'] =  $this->date_format_dmY($data['res_data']['faktur_pajak_date'],'-');
				}
				else
				{
					$data['res_data']['faktur_pajak_date'] = '';
				}

				if(isset($data['res_data']['faktur_pajak_date_2']) && $data['res_data']['faktur_pajak_date_2'] != '0000-00-00' && ! empty($data['res_data']['faktur_pajak_date_2']))
				{
					$data['res_data']['faktur_pajak_date_2'] =  $this->date_format_dmY($data['res_data']['faktur_pajak_date_2'],'-');
				}
				else
				{
					$data['res_data']['faktur_pajak_date_2'] = '';
				}

				if(isset($data['res_data']['surat_jalan_lkp_date']) && $data['res_data']['surat_jalan_lkp_date'] != '0000-00-00' && ! empty($data['res_data']['surat_jalan_lkp_date'])) 
				{
					$data['res_data']['surat_jalan_lkp_date'] =  $this->date_format_dmY($data['res_data']['surat_jalan_lkp_date'],'-');
				}
				else
				{
					$data['res_data']['surat_jalan_lkp_date'] = '';
				}
				
				if(isset($data['res_data']['spk_po_date']) && $data['res_data']['spk_po_date'] != '0000-00-00' && ! empty($data['res_data']['spk_po_date'])) 
				{
					$data['res_data']['spk_po_date'] =  $this->date_format_dmY($data['res_data']['spk_po_date'],'-');
				}
				else
				{
					$data['res_data']['spk_po_date'] = '';
				}
				
				if(isset($data['res_data']['ppn_val'])) 
				{
					$data['res_data']['ppn_val'] =  $this->money_format($data['res_data']['ppn_val']);
				}
				
				if(isset($data['res_data']['pph_val'])) 
				{
					$data['res_data']['pph_val'] =  $this->money_format($data['res_data']['pph_val']);
				}
				
				if(isset($data['res_data']['netto_cost_val'])) 
				{
					$data['res_data']['netto_cost_val'] =  $this->money_format($data['res_data']['netto_cost_val']);
				}
				
				if(isset($data['res_data']['bap_bpg_dub_number']) && ! empty($data['res_data']['bap_bpg_dub_number']))
				{
					$data['res_data']['bap_bpg_dub_number'] = json_decode($data['res_data']['bap_bpg_dub_number']);
				}
				
				if(isset($data['res_data']['bap_bpg_dub_date']) && ! empty($data['res_data']['bap_bpg_dub_date']))
				{
					$bap_bpg_dub_dates = json_decode($data['res_data']['bap_bpg_dub_date']);
					$data['res_data']['bap_bpg_dub_date'] = array();
					foreach($bap_bpg_dub_dates as $val)
					{
						if(! empty($val) && $val != '0000-00-00')
						{
							$data['res_data']['bap_bpg_dub_date'][] =  $this->date_format_dmY($val,'-');
						}
					}
				}
			}
		}
		else
		{
			$proyek_id_sess = $this->session->userdata($this->session_key('ref_proyek_id'));
			$proyeks_sess = $this->session->userdata($this->session_key('proyeks'));
			if(! empty($proyeks_sess)) 
			{
				if(! empty($proyek_id_sess))
				{
					$proyeks_sess[] = $proyek_id_sess;
				}
				
				$prm_proyek['where_in'] = array("id",$proyeks_sess);
			}
			else
			{
				if(! empty($proyek_id_sess))
				{
					$prm_proyek['where']['id'] = $proyek_id_sess;
				}
			}
		}
		
		$prm_proyek['where']['ref_cabang_id'] = '78';
		$data['ref_proyek'] = $this->def_model->get_list($prm_proyek)->result_array();
		
		$data['ref_vendor_type'] = $this->def_model->get_list(array('table' => 'ref_vendor_type'))->result_array();
		$data['ref_payment_method'] = $this->def_model->get_list(array('table' => 'ref_payment_method', 'order_sort' => 'id', 'order_dir' => 'DESC'))->result_array();
		$data['ref_berkas_type'] = $this->ref_berkas_type;

		if(isset($data['res_data']['vendor_type'])){
			$prm_code = array(
				'table' => 'ref_vendor_type',
				'where' => array(
					'name' => $data['res_data']['vendor_type']
				)
			);

			$vendor_type_code = $this->def_model->get_one($prm_code)->row();

			$data['ref_pph_percentage'] = $this->def_model->get_list(array('table' => 'ref_pph_percentage', 'where' => array('vendor_type' => $vendor_type_code->code )))->result_array();
		}

		if(isset($data['res_data']['ppn_code']) && !empty($data['res_data']['ppn_code'])){
			$data['ref_ppn_percentage'] = $this->def_model->get_list(array('table' => 'ref_ppn_percentage', 'where' => array('pajak_type' => $data['res_data']['pajak_type'] )))->result_array();
		}else{
			$data['ref_ppn_percentage'] = $this->def_model->get_list(array('table' => 'ref_ppn_percentage', 'where' => array('pajak_type' => "NON WAPU")))->result_array();
		}


		if($etc_files = $this->def_model->get_list(array('table' => 'berkas_files_etc', 'where' => array('berkas_id' => $id)))->result_array()){
			foreach ($etc_files as $etc){
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $etc['file_name']);
				$data['res_data']['etc_files'][$withoutExt] = $etc;
			}
		}
		
		$this->navigation_manager->rebuild_breadcrumb(
			$this->class,
			array("#" => $this->lang->line('global_input'))
		);
		
		$data['id'] = $id;
		$data['is_revision'] = $is_revision;
		
		$data['ref_years'] = $this->get_years(2,date("Y")+1);
		$data['ref_periods'] = array();
		foreach($this->date_indonesia->get_month() as $key => $val)
		{
			$key = sprintf('%02d', $key);
			$data['ref_periods'][] = $key.'/'.$key; 
		}
		$data['current_year'] = date('Y'); 
		$data['current_period'] = date('m').'/'.date('m'); 
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/chosen-bootstrap/chosen.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/chosen-bootstrap/chosen.jquery.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/jquery-tags-input/jquery.tagsinput.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-tags-input/jquery.tagsinput.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/jquery-multi-select/css/multi-select-metro.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-multi-select/js/jquery.multi-select.js').'"></script>';

		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/new-jquery-inputmask/jquery.inputmask.bundle.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-datepicker/css/datepicker.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js').'"></script>';
			
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-fileupload/bootstrap-fileupload.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-fileupload/bootstrap-fileupload.js').'"></script>';

		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/new-dropzone/min/dropzone.min.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/new-dropzone/min/dropzone.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js').'"></script>';
		
		$this->load->view('global/header_view',$this->header);
		$this->load->view('main/'.strtolower($this->class).'_input_view', $data);
		$this->load->view('global/footer_view');
	}
	
	function check_berkas_number()
	{
		$where['is_active'] = 1;
		$where['berkas_number'] = $_POST['berkas_number'];
		$where['project_code'] = $_POST['project_code'];
		
        if(isset($_POST['id']) && ! empty($_POST['id']))
		{
		  $where['id !='] = $_POST['id'];
		}
		
		$prm_chk['where'] = $where;
		$prm_chk['table'] = $this->table;
		
		if(! $this->def_model->is_unique($prm_chk))
		{
			if($this->input->post('direct_ajax'))
			{
				echo $this->output->status_callback('json_unsuccess',$this->lang->line('berkas_number_already_exist'));
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			if($this->input->post('direct_ajax'))
			{
				echo $this->output->status_callback('json_success');
			}
			else
			{
				return TRUE;
			}
		}
	}
	
	private function _update_vendor()
	{
		if($this->input->post('vendor_name'))
		{
			$table = 'ref_vendor';
			$prm = array(
				'table' => $table,
				'where' => array(
					'upper(name)' => strtoupper(trim($_POST['vendor_name']))
				)
			);
			
			$res_vendor = $this->def_model->get_list($prm)->result_array();
			
			$data = array();
			$data['name'] = strtoupper(trim($_POST['vendor_name']));
			
			if($this->input->post('vendor_code'))
			{
				$data['code'] = $this->input->post('code');
			}
			
			if($this->input->post('vendor_npwp'))
			{
				$data['npwp'] = $this->input->post('vendor_npwp');
			}
			
			if($this->input->post('vendor_bank_name'))
			{
				$data['bank_name'] = $this->input->post('vendor_bank_name');
			}
			
			if($this->input->post('vendor_bank_rek'))
			{
				$data['bank_rek'] = $this->input->post('vendor_bank_rek');
			}
			
			if($this->input->post('vendor_bank_account'))
			{
				$data['bank_account'] = $this->input->post('vendor_bank_account');
			}
			
			if($this->input->post('vendor_address'))
			{
				$data['address'] = $this->input->post('vendor_address');
			}
			
			if($this->input->post('vendor_city'))
			{
				$data['city'] = $this->input->post('vendor_city');
			}
			
			if($this->input->post('vendor_email'))
			{
				$data['email'] = $this->input->post('vendor_email');
			}
			
			if(! empty($res_vendor))
			{
				$data['modified_datetime'] = $this->get_date_time();
				$data['modified_by'] = $this->get_user_name();
				
				foreach($res_vendor as $vendor)
				{
					$prm_update = array(
						'table' => $table,
						'data' => $data,
						'where' => array('id' => $vendor['id'])
					);
					
					$res = $this->def_model->update($prm_update);
				}
			}
			else
			{
				$res_vendor_type = $this->def_model->get_one(
					array(
						'table' => 'ref_vendor_type',
						'where' => array('name' => $_POST['vendor_type'])
					)
				)->row_array();
				
				if(! empty($res_vendor_type))
				{
					$data['ref_vendor_type_id'] = $res_vendor_type['id'];
					$data['created_datetime'] = $this->get_date_time();
					$data['created_by'] = $this->get_user_name();
					
					$prm_insert = array(
						'table' => $table,
						'data' => $data
					);
					
					$res = $this->def_model->insert($prm_insert);
				}
				
			}
		}
	}
	
	function save()
	{		
		$berkas_files_id = $this->input->post('berkas_files_id');
		
		if(isset($_FILES['berkas_files']))
		{
			$temp_array = array();
			foreach($_FILES['berkas_files'] as $key => $val)
			{
				$i = 0;
				foreach($val as $new_key)
				{
					$temp_array[$berkas_files_id[$i]][$key] = $new_key;
					$i++;
				}
			}
			
			foreach($temp_array as $key => $val)
			{
				$val['file_id'] = $key;
				$_FILES['berkas_files_'.$key] = $val;
			}
			unset($_FILES['berkas_files']);
		}
		
		$encrypt_string = md5($this->input->post('berkas_number'));
		$path = ABSOLUTE_PATH.'asset/berkas_files/'.$encrypt_string;
		
		$this->create_dir($path);
		
		
		$config['upload_path'] 	 = $path;
		$config['allowed_types'] = 'pdf';
		$config['overwrite']	 = TRUE;
		$config['remove_spaces'] = TRUE;
		$config['max_size']      = '10000000000';
		
		$this->load->library('upload');
		
		$uplaod_status = TRUE;
		$error_upload = FALSE;
		$files = array();
		$etc_files = array();

		foreach($_FILES as $key => $value)
		{
			if( ! empty($value['name']))
            {
				if(isset($value['file_id'])){
					$config['file_name'] = strtolower($this->replace_space($this->ref_berkas_type[$value['file_id']]));

					$this->upload->initialize($config);

					if(! $this->upload->do_upload($key))
					{
						$data['upload_message'] = $this->upload->display_errors();
						$this->load->vars($data);

						$error_upload = TRUE;
					}
					else
					{
						$files[$value['file_id']] = $this->upload->data();
					}
				}else{

					$config['file_name'] = strtolower($this->replace_space($key));

					$this->upload->initialize($config);

					if(! $this->upload->do_upload($key))
					{
						$data['upload_message'] = $this->upload->display_errors();
						$this->load->vars($data);

						$error_upload = TRUE;
					}
					else
					{
						 $etc_files[$key] = $this->upload->data();
					}
				}
            }
			else
			{
				if(isset($value['file_id'])){
					$files[$value['file_id']] = array();
				}
			}
		}


		if(! $error_upload)
		{
			if($this->check_berkas_number() == FALSE)
			{
				echo $this->output->status_callback('json_unsuccess',$this->lang->line('berkas_number_already_exist'));
			}
			else
			{
				$date = $this->get_date();
				$date_time = $this->get_date_time();
				$user_name = $this->get_user_name();
				$user_id = $this->get_user_id();
				$employee_id = $this->get_employee_id();
				$employee_name = $this->get_employee_name();
				
				$id = $this->input->post('id') ? $this->input->post('id') : ''; 
				$is_revision = $this->input->post('is_revision') ? $this->input->post('is_revision') : 0; 
				$ref_verify_level = $this->get_verify_level($this->input->post('payment_method'));
				$verify_level_min_key = min(array_keys($ref_verify_level));
				$verify_level_min_val = $ref_verify_level[$verify_level_min_key];
				
				$data = array();
				foreach($this->input->post() as $key => $val)
				{
					if($key != 'id' && $key != 'berkas_files' && $key != 'berkas_files_id' && $key != 'is_revision') 
					{
						if($key == 'ttap_date' || $key == 'invoice_date' || $key == 'faktur_pajak_date' || $key == 'faktur_pajak_date_2' || $key == 'surat_jalan_lkp_date' || $key == 'spk_po_date')
						{
							if(! empty($val))
							{
								$data[$key] = $this->date_format_Ymd($val,'-');
							}
						}
						elseif($key == 'bap_bpg_dub_number')
						{
							$data[$key] = json_encode($val);
						}
						elseif($key == 'bap_bpg_dub_date')
						{
							$bap_bpg_dub_dates = array();
							foreach($val as $v)
							{
								if(! empty($v))
								{
									$bap_bpg_dub_dates[] = $this->date_format_Ymd($v,'-');
								}
							}
							
							$data[$key] = json_encode($bap_bpg_dub_dates);
						}
						elseif($key == 'real_cost' || $key == 'ppn')
						{
							$data[$key] = $this->replace_str_numeric($val,'_,.');
						}
						elseif($key == 'pph')
						{
							$pph = $this->replace_str_numeric($val,'_','0');
							$data[$key] = $this->replace_str_numeric($pph,',','.');
						}
						elseif($key == 'ppn_val' || $key == 'pph_val' || $key == 'netto_cost_val' || $key == 'fp_amount' || $key == 'dpp_amount' || $key == 'ppnbm_amount' || $key == 'fp_amount_2' || $key == 'ppnbm_amount_2' || $key == 'dpp_amount_2')
						{
							$data[$key] = $this->replace_str_numeric($val,'.');
						}
						elseif($key == 'ppn_code')
						{
							$prm = array(
								'where' => array(
									'code' => $val
								),
								'table' => 'ref_ppn_percentage'
							);
							$data[$key] = $val;
							$data['ppn'] = $this->def_model->get_one($prm)->row()->percentage;
						}
						elseif($key == 'pph_code')
						{
							$prm = array(
								'where' => array(
									'code' => $val
								),
								'table' => 'ref_pph_percentage'
							);
							$data[$key] = $val;
							$data['pph'] = $this->def_model->get_one($prm)->row()->percentage;
						}
						elseif($key == 'ref_vendor_id')
						{
							$val = intval($val);
						}
						elseif($key == 'uploaded_fp_filepath')
						{
							$folder = explode("/", $val);
							$folder[0];
							$tmp_file = ABSOLUTE_PATH.'asset/tmp/'.$val;
							$dest =  ABSOLUTE_PATH.'asset/faktur_pajak/'.$val;
							if(file_exists($tmp_file)){
								$this->create_dir(ABSOLUTE_PATH.'asset/faktur_pajak/'.$folder[0]);
								if(!file_exists($dest)){
									copy($tmp_file, $dest);
								}
							}

							$data[$key] = $val;
						}elseif($key == 'uploaded_fp_filepath_2')
						{
							$folder = explode("/", $val);
							$folder[0];
							$tmp_file = ABSOLUTE_PATH.'asset/tmp/'.$val;
							$dest =  ABSOLUTE_PATH.'asset/faktur_pajak/'.$val;
							if(file_exists($tmp_file)){
								$this->create_dir(ABSOLUTE_PATH.'asset/faktur_pajak/'.$folder[0]);
								if(!file_exists($dest)){
									copy($tmp_file, $dest);
								}
							}

							$data[$key] = $val;
						}
						else
						{
							if($key == 'is_rilis')
							{
								if(! empty($val))
								{
									$data['rilis_datetime'] = $date_time;
									$data['rilis_by'] = $user_name;
									$data['rilis_by_employee_id'] = $employee_id;
									
									$data['last_verify_level'] = $verify_level_min_val['verify_level'];
									$data['last_verify_level_index'] = $verify_level_min_key;
									$data['last_verify_status'] = $this->ref_verify_status[1];
									$data['last_verify_datetime'] = $date_time;
									
									$data['berkas_status'] = $this->ref_berkas_status[1];
								}
							}
							if(! is_array($val))
							{
								$val = trim($val);
							}
							$data[$key] = $val;
						}
					}
				}

				if(isset($data['faktur_pajak_number']) && !empty($data['faktur_pajak_number'])){
					$data['nsfp'] = substr($data['faktur_pajak_number'], 4);
				}

				if($data['tagihan_type'] == 'NON FP'){
					$data['faktur_pajak_number'] =  NULL;
					$data['nsfp'] =  NULL;
					$data['fp_amount'] =  NULL;
					$data['faktur_pajak_date'] =  NULL;
					$data['pajak_type'] =  NULL;
					$data['pajak_period'] =  NULL;
					$data['pajak_year'] =  NULL;
					$data['ppn_code'] =  NULL;
					$data['ppn_val'] =  NULL;
					$data['ppn'] =  NULL;

					$data['faktur_pajak_url'] =  NULL;
					$data['faktur_pajak_approval'] =  NULL;
					$data['faktur_pajak_status'] =  NULL;
					$data['dpp_amount'] =  NULL;
					$data['ppnbm_amount'] =  NULL;
					$data['npwp_pembeli'] =  NULL;
					$data['nama_pembeli'] =  NULL;
					$data['alamat_pembeli'] =  NULL;

					$data['npwp_penjual'] = NULL;
					$data['nama_penjual'] =  NULL;
					$data['alamat_penjual'] =  NULL;

					$data['uploaded_fp_filepath_2']  = NULL;
					$data['uploaded_fp_filename_2']  = NULL;
					$data['uploaded_fp_filesize_2']  = NULL;
					$data['faktur_pajak_url_2']      = NULL;
					$data['faktur_pajak_approval_2'] = NULL;
					$data['faktur_pajak_status_2']   = NULL;
					$data['faktur_pajak_number_2']   = NULL;
					$data['faktur_pajak_date_2']     = NULL;
					$data['fp_amount_2'] =  NULL;
					$data['dpp_amount_2'] =  NULL;
					$data['ppnbm_amount_2'] =  NULL;
					$data['pajak_type_2'] =  NULL;
				}
				
				$data['is_active'] = 1;
				
				if($is_revision)
				{
					$old_id = $id;
					$id = 0;
					
					// update old data to inactive
					$prm = array(
						'table' => $this->table,
						'data'  => array('is_active' => 0),
						'where' => array('id' => $old_id)
					);
					
					$this->def_model->update($prm);
					
					$data['parent_id'] = $old_id;
					$data['revision'] = $data['revision'] + 1;
					$data['revision_datetime'] = $date_time;
					$data['revision_by'] = $user_name;
					$data['revision_by_employee_id'] = $employee_id;
				}
				
				
				if(!$id) // add
				{
					$action_lang = 'added';
					$data['created_datetime'] = $date_time;
					$data['created_by'] = $user_name;
					
					$prm = array(
						'table' => $this->table,
						'data' => $data
					);
				
					$res = $this->def_model->insert($prm);
					$berkas_id = $this->db->insert_id();
					
					$files_id = array();
					foreach($berkas_files_id as $val)
					{
						$files_id[] = array(
							'berkas_id' => $berkas_id,
							'file_id' => $val,
							'last_update_datetime' => $date_time, 
							'last_update_by' => $user_name
						);
					}
					
					if(! empty($files_id)) 
					{
						$prm_insert_batch = array(
							'table' => 'berkas_files',
							'data'  => $files_id
						);
						
						$this->def_model->insert_batch($prm_insert_batch);
						
						if($is_revision)
						{
							$res_old_files = $this->def_model->get_list(array('table' => 'berkas_files', 'where' => array('berkas_id' => $old_id)))->result_array();
							
							foreach($res_old_files as $val)
							{
								$prm = array(
									'table' => 'berkas_files',
									'data'  => array(
										'file_name' => $val['file_name'], 
										'file_path' => $val['file_path'], 
										'file_size' => $val['file_size'], 
										'file_type' => $val['file_type'],
										'last_update_datetime' => $val['last_update_datetime'], 
										'last_update_by' => $val['last_update_by']
									),
									'where' => array('berkas_id' => $berkas_id, 'file_id' => $val['file_id'])
								);
								
								$this->def_model->update($prm);
							}
						}
					}
				}
				else
				{
					$action_lang = 'edited';
					$data['modified_datetime'] = $date_time;
					$data['modified_by'] = $user_name;

					$prm = array(
						'table' => $this->table,
						'data'  => $data,
						'where' => array('id' => $id)
					);
					
					$res = $this->def_model->update($prm);
					$berkas_id = $id;
				}
				
				
				// general log
				if($this->db->affected_rows())
				{				
					$this->log_activities(
						array(
							'module' => $this->class,
							'lang_subject' => 'berkas_data',
							'lang_item'    => $data['berkas_number'],
							'lang_action'  => '[global_activity_'.$action_lang.']'
						)
					);
				}
				
				// berkas verify & log
				$send_email_to = 'VER_M';
				if($data['is_rilis'])
				{
					//todo verify to last
					$verify_data = array();

					$prm_last_verify = array(
						'table' => 'berkas',
						'where' => array('id' => $berkas_id)
					);

					$current_berkas = $this->def_model->get_one($prm_last_verify)->row();
					$parent_id = $current_berkas->parent_id;

					if($parent_id > 0) {
						//Get Last Approved
						$prm_berkas_verify = array(
							'table' => 'berkas_verify',
							'where' => array(
								'berkas_id' => $parent_id,
								'verify_status' => 'Disetujui'
							),
						);

						if($old_verify = $this->def_model->get_list($prm_berkas_verify)->result()){

							foreach ($old_verify as $last_verify){
								$v_data = array(
									'berkas_id' => $berkas_id,
									'verify_level_index' => $last_verify->verify_level_index,
									'verify_level' => $last_verify->verify_level,
									'verify_status' => $last_verify->verify_status,
									'verify_datetime' => $last_verify->verify_datetime,
									'approval_datetime' => $last_verify->approval_datetime,
									'approval_by' => $last_verify->approval_by,
									'approval_by_employee_id' =>$last_verify->approval_by_employee_id,
									'email_sent_to' => $last_verify->email_sent_to,
								);

								$verify_data[] = $v_data;
							}

							//Get Last Rejected
							$prm_berkas_verify = array(
								'table' => 'berkas_verify',
								'where' => array(
									'berkas_id' => $parent_id,
									'verify_status' => 'Ditolak'
								),
							);
							if($current_verify = $this->def_model->get_one($prm_berkas_verify)->row()){
								$v_data = array(
									'berkas_id' => $berkas_id,
									'verify_level_index' => $current_verify->verify_level_index,
									'verify_level' => $current_verify->verify_level,
									'verify_status' => 'Menunggu',
									'verify_datetime' => $date_time,
									'approval_datetime' => NULL,
									'approval_by' => NULL,
									'approval_by_employee_id' => NULL,
									'email_sent_to' => NULL,
								);

								$verify_data[] = $v_data;

								$prm_update = array(
									'table' => 'berkas',
									'data'  => array(
										'last_verify_level' =>  $current_verify->verify_level,
										'last_verify_level_index' => $current_verify->verify_level_index
									),
									'where' => array(
										'id' => $berkas_id,
										'is_active' => 1
									)
								);

								$this->def_model->update($prm_update);
								$send_email_to = $current_verify->verify_level;

							}

							//Get Rest Data
							$prm_berkas_rest_verify = array(
								'table' => 'berkas_verify',
								'where' => array(
									'berkas_id' => $parent_id,
								),
								'where_string' => 'verify_status IS NULL'
							);

							if($rest_verify = $this->def_model->get_list($prm_berkas_rest_verify)->result()){

								foreach ($rest_verify as $rest){
									$v_data = array(
										'berkas_id' => $berkas_id,
										'verify_level_index' => $rest->verify_level_index,
										'verify_level' => $rest->verify_level,
										'verify_status' => NULL,
										'verify_datetime' => NULL,
										'approval_datetime' => NULL,
										'approval_by' => NULL,
										'approval_by_employee_id' => NULL,
										'email_sent_to' => NULL,
									);

									$verify_data[] = $v_data;
								}

							}
						}
					}

					if(empty($verify_data)){
						foreach($ref_verify_level as $key => $val)
						{
							$v_data = array(
								'berkas_id' => $berkas_id,
								'verify_level_index' => $key,
								'verify_level' => $val['verify_level'],
							);

							if($key == $verify_level_min_key)
							{
								$v_data['verify_status'] = $this->ref_verify_status[1];
								$v_data['verify_datetime'] = $date_time;
							}
							else
							{
								$v_data['verify_status'] = NULL;
								$v_data['verify_datetime'] = NULL;
							}

							$verify_data[] = $v_data;
						}
					}
					
								
					// create verify data
					if(! empty($verify_data)) 
					{
						$prm_insert_batch = array(
							'table' => 'berkas_verify',
							'data'  => $verify_data
						);
						
						$this->def_model->insert_batch($prm_insert_batch);
					}
					
					// create log berkas
					$log_by_name = ! empty($employee_name) ? $employee_name : $user_name;
					$this->berkas_m->log_berkas(
						array(
							'berkas_id' => $berkas_id,
							'berkas_number' => $data['berkas_number'],
							'activity' => $this->lang->line('berkas_name')." (".$data['berkas_number'].") ".$this->lang->line('berkas_release'),
							'current_status' => $this->lang->line('berkas_pending_at')." ".$send_email_to,
							'log_datetime'=> $date_time,
							'log_by_name' => $log_by_name,
							'log_by_userid' => $user_id
						)
					);

					// send email
					switch ($send_email_to){
						CASE 'TO':
							$res_employee = $this->def_model->get_user_employee2(array('where' => array('b.divisi' => 'EPC 2', 'c.name' => 'TO')))->result_array();
							if(! empty($res_employee) && ! empty($verify_data))
							{
								$is_email_sent = $this->send_email_berkas($berkas_id, $res_employee, 1, 'TO');

								if(! empty($is_email_sent))
								{
									$prm_update2 = array(
										'table' => $this->table.'_verify',
										'data'  => array(
											'email_sent_to' => json_encode($is_email_sent)
										),
										'where' => array('berkas_id' => $berkas_id, 'verify_level_index' => 2)
									);

									$this->def_model->update($prm_update2);
								}
							}

							break;

						CASE 'PCO':
							$res_employee = $this->def_model->get_user_employee2(array('where' => array('b.divisi' => 'EPC 2', 'c.name' => 'PCO')))->result_array();
							if(! empty($res_employee) && ! empty($verify_data))
							{
								$is_email_sent = $this->send_email_berkas($berkas_id, $res_employee, 1, 'PCO');

								if(! empty($is_email_sent))
								{
									$prm_update2 = array(
										'table' => $this->table.'_verify',
										'data'  => array(
											'email_sent_to' => json_encode($is_email_sent)
										),
										'where' => array('berkas_id' => $berkas_id, 'verify_level_index' => 3)
									);

									$this->def_model->update($prm_update2);
								}
							}

							break;
						CASE 'KBA':
							$res_employee = $this->def_model->get_user_employee2(array('where' => array('b.divisi' => 'EPC 2', 'c.name' => 'KBA')))->result_array();
							if(! empty($res_employee) && ! empty($verify_data))
							{
								$is_email_sent = $this->send_email_berkas($berkas_id, $res_employee, 1, 'KBA');

								if(! empty($is_email_sent))
								{
									$prm_update2 = array(
										'table' => $this->table.'_verify',
										'data'  => array(
											'email_sent_to' => json_encode($is_email_sent)
										),
										'where' => array('berkas_id' => $berkas_id, 'verify_level_index' => 4)
									);

									$this->def_model->update($prm_update2);
								}
							}
							break;

						default :
							$res_employee = $this->def_model->get_user_employee2(array('where' => array('b.divisi' => 'EPC 2', 'c.name' => 'VER_M')))->result_array();
							if(! empty($res_employee) && ! empty($verify_data))
							{
								$is_email_sent = $this->send_email_berkas($berkas_id, $res_employee, 1, $verify_level_min_val['verify_level']);

								if(! empty($is_email_sent))
								{
									$prm_update2 = array(
										'table' => $this->table.'_verify',
										'data'  => array(
											'email_sent_to' => json_encode($is_email_sent)
										),
										'where' => array('berkas_id' => $berkas_id, 'verify_level_index' => 1)
									);

									$this->def_model->update($prm_update2);
								}
							}
					}

				}
				
				if(! empty($files))
				{
					$str = "";
					foreach($files as $key => $val)
					{
						if(! empty($val))
						{
							$data_files = array();
							
							$file_path = 'berkas_files/'.$encrypt_string.'/'.$val['file_name'];
							
							$data_files = array(
								'berkas_id' => $berkas_id,
								'file_id' => $key, 
								'file_name' => $this->ref_berkas_type[$key], 
								'file_path' => $file_path, 
								'file_size' => round($val['file_size']), 
								'file_type' => $val['file_type'],
								'last_update_datetime' => $date_time, 
								'last_update_by' => $user_name
							);
							
							$prm = array(
								'table' => 'berkas_files',
								'data'  => $data_files
							);
							
							$check_data = $this->def_model->get_one(array('table' => 'berkas_files', 'where' => array('berkas_id' => $berkas_id, 'file_id' => $key)))->row_array();
									
							if(! empty($check_data))
							{
								$prm['where'] = array('id' => $check_data['id']);
								
								$this->def_model->update($prm);
							}
							else
							{
								$this->def_model->insert($prm);
							}
						}
					}
				}

				if($is_revision){
					$prm = array(
						'table' => 'berkas_files_etc',
						'where' => array(
							'berkas_id' => $old_id
						)
					);

					if($old_files = $this->def_model->get_list($prm)->result()){
						foreach ($old_files as $file){
							if($file->file_name){
								$data_files = array(
									'berkas_id' => $berkas_id,
									'file_name' => $file->file_name,
									'file_path' => $file->file_path,
									'file_size' => round($file->file_size),
									'file_type' => $file->file_type,
									'last_update_datetime' => $date_time,
									'last_update_by' => $user_name
								);

								$prm = array(
									'table' => 'berkas_files_etc',
									'data'  => $data_files
								);

								$this->def_model->insert($prm);
							}
						}
					}
				}

				if(! empty($etc_files))
				{
					$str = "";
					foreach($etc_files as $key => $val)
					{
						if(! empty($val))
						{
							$data_files = array();

							$file_path = 'berkas_files/'.$encrypt_string.'/'.$val['file_name'];

							$data_files = array(
								'berkas_id' => $berkas_id,
								'file_name' => $val['file_name'],
								'file_path' => $file_path,
								'file_size' => round($val['file_size']),
								'file_type' => $val['file_type'],
								'last_update_datetime' => $date_time,
								'last_update_by' => $user_name
							);

							$prm = array(
								'table' => 'berkas_files_etc',
								'data'  => $data_files
							);

							$check_data = $this->def_model->get_one(array('table' => 'berkas_files_etc', 'where' => array('berkas_id' => $berkas_id, 'file_name' => $val['file_name'])))->row_array();

							if(! empty($check_data))
							{
								$prm['where'] = array('id' => $check_data['id']);

								$this->def_model->update($prm);
							}
							else
							{
								$this->def_model->insert($prm);
							}
						}
					}
				}
				
				if($res)
				{
					$this->_update_vendor();
					echo $this->output->status_callback('json_success');	
				}
				else
				{
					echo $this->output->status_callback('json_unsuccess', $this->lang->line('global_error'));	
				}
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess',$this->lang->line('global_upload_error'));
		}
	}
	
	function send_email_berkas($berkas_id, $res_employee_arr, $email_type, $verify_level = "", $reminder = FALSE)	
	{
		$res_berkas = $this->berkas_m->get_berkas(array('where' => array('a.id' => $berkas_id)))->row_array();
		$is_email_sent = array();
		if(! empty($res_berkas) && ! empty($res_employee_arr) && ! empty($email_type) && $this->site_config['is_email_system_active'])
		{	
			// only for testing periode
			if($this->site_config['is_email_test_active'] && $this->is_email_valid($this->site_config['email_test']))
			{
				$res_employee_arr = array();
				$res_employee_arr[] = array(
					'emp_id' => '0',
					'email' => $this->site_config['email_test'],
					'employee_name' => 'Admin Testing',
					'nrp' => 'Unknown'
				);
			}
			
			
			foreach($res_employee_arr as $res_employee)
			{
				if(! empty($res_employee['email']) && $this->is_email_valid($res_employee['email']))
				{
					if(isset($this->site_config['app_admin_email']))
					{
						$from_email = $this->site_config['app_admin_email'];
					}
					else
					{
						$server = $_SERVER['HTTP_HOST'] == 'localhost' ? 'local.com' : $_SERVER['HTTP_HOST'];
						$from_email = "webmaster@".$server;
					}
					
					if(isset($this->site_config['app_admin_email_name']))
					{
						$from_email_name = $this->site_config['app_admin_email_name'];
					}
					else
					{
						$from_email_name = "webmaster";
					}
					
					if($email_type == 3)
					{
						$subject = $this->site_config['email_template_rejected_berkas_subject'];
						$content = $this->site_config['email_template_rejected_berkas'];
					}
					elseif($email_type == 2)
					{
						$subject = $this->site_config['email_template_complete_berkas_subject'];
						$content = $this->site_config['email_template_complete_berkas'];
					}
					else
					{
						$subject = $this->site_config['email_template_verify_berkas_subject'];
						$content = $this->site_config['email_template_verify_berkas'];
					}
					
					
					$to_name = ! empty($res_employee['employee_name']) ? ucwords($res_employee['employee_name']).' ('.$res_employee['nrp'].')' : 'Unknown';
					
					$content = str_replace('[to_name]',$to_name,$content);
					$content = str_replace('[verify_level]',$verify_level,$content);
					
					$content = str_replace('[berkas_number]',$this->lang->line('berkas_number'),$content);
					$content = str_replace('[berkas_num]',$res_berkas['berkas_number'],$content);
					
					$content = str_replace('[berkas_project_code]',$this->lang->line('berkas_project_code'),$content);
					$content = str_replace('[project_code]',$res_berkas['project_code'],$content);
					
					$content = str_replace('[berkas_project_name]',$this->lang->line('berkas_project_name'),$content);
					$content = str_replace('[project_name]',$res_berkas['project_name'],$content);
					
					$content = str_replace('[berkas_vendor_name]',$this->lang->line('berkas_vendor_name'),$content);
					$content = str_replace('[vendor_name]',$res_berkas['vendor_name'],$content);
					
					$content = str_replace('[berkas_rejected_note]',$this->lang->line('berkas_rejected_note'),$content);
					$content = str_replace('[rejected_note]',$res_berkas['rejected_note'],$content);
					
					$verify_link = "<a href='".app_backend_url('main/berkas/detail/'.base64_encode($res_berkas['id']))."'>Disini</a>";
					$content = str_replace('[verify_link]',$verify_link,$content);
					
					$content_html = "<htm><body>".$content."</body></html>"; 
					
					$to = array();
					$to[$res_employee['employee_name']] = $res_employee['email'];
					
					$prm_email = array(
						'from_email' => $from_email,
						'from_name'  => $from_email_name,
						'subject'    => $subject,
						'to'		 => $to,
						'content'	 => $content_html
					);
					
					if($reminder)
					{
						$prm_email['priority'] = 1;
					}
					
					$is_sent = 0;
					if($this->send_email($prm_email))
					{
						$is_sent = 1;
					}
					
					$is_email_sent[$res_employee['emp_id']] = array(
						'is_email_sent' => $is_sent,
						'email_sent' => $res_employee['email']
					);
				}
			}
		}
		
		return $is_email_sent;
	}
	
	function delete_file()
	{
		if($this->input->post('id'))
		{	
			$prm_update = array(
				'table' => 'berkas_files',
				'data' => array(
					'file_name' => NULL,
					'file_path' => NULL,
					'file_type' => NULL,
					'file_size' => NULL,
					'last_update_datetime' => $this->get_date_time(),
					'last_update_by' => $this->get_user_name()
				),
				'where' => array("id" => $this->input->post('id'))
			);
			
			if($this->def_model->update($prm_update))
			{
				$path_file = ABSOLUTE_PATH.'asset/'.$this->input->post('path');
				
				$this->unlink_file($path_file);
				
				echo $this->output->status_callback('json_success');	
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}

	function delete_etc_file()
	{
		if($this->input->post('id'))
		{
			$prm_update = array(
				'table' => 'berkas_files_etc',
				'data' => array(
					'file_name' => NULL,
					'file_path' => NULL,
					'file_type' => NULL,
					'file_size' => NULL,
					'last_update_datetime' => $this->get_date_time(),
					'last_update_by' => $this->get_user_name()
				),
				'where' => array("id" => $this->input->post('id'))
			);

			if($this->def_model->update($prm_update))
			{
				$path_file = ABSOLUTE_PATH.'asset/'.$this->input->post('path');

				$this->unlink_file($path_file);

				echo $this->output->status_callback('json_success');
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
	
	function detail($id = 0, $url_back = "")
	{
		$id = base64_decode($id);
		$check_berkas = $this->berkas_m->get_berkas(array('where' => array('a.id' => $id)))->row_array();
		
		if(! empty($check_berkas))
		{
			$data = array();
			
			$data['url_back'] = $url_back;
			
			$data['CTR'] = $this;
			
			if(! empty($check_berkas['ttap_date']))
			{
				$check_berkas['ttap_date'] = $this->convert_date($check_berkas['ttap_date'],'d M Y');
			}
			
			if(! empty($check_berkas['invoice_date']))
			{
				$check_berkas['invoice_date'] = $this->convert_date($check_berkas['invoice_date'],'d M Y');
			}
			
			if(! empty($check_berkas['rilis_datetime']))
			{
				$check_berkas['rilis_datetime'] = $this->convert_date($check_berkas['rilis_datetime'],'d M Y');
			}
			
			if(! empty($check_berkas['faktur_pajak_date']))
			{
				$check_berkas['faktur_pajak_date'] = $this->convert_date($check_berkas['faktur_pajak_date'],'d M Y');
			}
			
			if(! empty($check_berkas['surat_jalan_lkp_date']))
			{
				$check_berkas['surat_jalan_lkp_date'] = $this->convert_date($check_berkas['surat_jalan_lkp_date'],'d M Y');
			}
			
			if(! empty($check_berkas['spk_po_date']))
			{
				$check_berkas['spk_po_date'] = $this->convert_date($check_berkas['spk_po_date'],'d M Y');
			}
			
			if(! empty($check_berkas['bap_bpg_dub_number']) && is_array(json_decode($check_berkas['bap_bpg_dub_number'])))
			{
				$bap_bpg_dub_number = json_decode($check_berkas['bap_bpg_dub_number']);
				$bap_bpg_dub_date = json_decode($check_berkas['bap_bpg_dub_date']);
				$bap_bpg_dub_number_date = array();
				
				foreach($bap_bpg_dub_number as $key => $val)
				{
					$bap_bpg_dub_number_date[] = $val.' ( '.(isset($bap_bpg_dub_date[$key]) ? $this->convert_date($bap_bpg_dub_date[$key],'d M Y') : '-').' )';
				}
				
				if(! empty($bap_bpg_dub_number_date))
				{
					$check_berkas['bap_bpg_dub_number_date'] = implode(", ",$bap_bpg_dub_number_date);
				}
			}
			
			$check_berkas['last_verify_statuss'] = $check_berkas['last_verify_status'];
			if(! empty($check_berkas['last_verify_status']))
			{
				$label_v_status = "";
				if($check_berkas['last_verify_status'] == $this->ref_verify_status[3])
				{
					$label_v_status = "label-important";
				}
				elseif($check_berkas['last_verify_status'] == $this->ref_verify_status[1])
				{
					$label_v_status = "label-warning";
				}
				elseif($check_berkas['last_verify_status'] == $this->ref_verify_status[2])
				{
					$label_v_status = "label-success";
				}
				
				if(! empty($check_berkas['last_verify_datetime']))
				{
					$check_berkas['last_verify_datetime'] = $this->convert_date($check_berkas['last_verify_datetime'],'d M Y H:i');
				}
				else
				{
					$check_berkas['last_verify_datetime'] = '-';
				}
				
				if(! empty($check_berkas['last_approval_datetime']))
				{
					$check_berkas['last_approval_datetime'] = $this->convert_date($check_berkas['last_approval_datetime'],'d M Y H:i');
				}
				
				$since = ! empty($check_berkas['last_approval_datetime']) ? $check_berkas['last_approval_datetime'] : $check_berkas['last_verify_datetime'];
				
				$check_berkas['last_verify_status'] = '<span class="label '.$label_v_status.'">'.$check_berkas['last_verify_status'].'</span> - '.$check_berkas['last_verify_level'].' ( <i>'.$this->lang->line('global_since').': '.$since.'</i> )';
			}

			if(! empty($check_berkas['hard_copy_arrived']))
			{
				$check_berkas['hard_copy_arrived_pretty'] = $this->convert_date($check_berkas['hard_copy_arrived'],'d M Y');
				$check_berkas['hard_copy_arrived'] = $this->date_format_dmY($check_berkas['hard_copy_arrived'],'-');
			}

			if(! empty($check_berkas['hard_copy_approved']))
			{
				$check_berkas['hard_copy_approved_pretty'] = $this->convert_date($check_berkas['hard_copy_approved'],'d M Y');
				$check_berkas['hard_copy_approved'] = $this->date_format_dmY($check_berkas['hard_copy_approved'],'-');
			}

			if(! empty($check_berkas['hard_copy_description']))
			{
				$arr = explode(' -',trim($check_berkas['hard_copy_description']));
				$check_berkas['declined_reason'] = trim($arr[0]);
				if(isset($arr[1]) && !empty($arr[1])){
					$check_berkas['declined_text'] = trim($arr[1]);
				}else{
					$check_berkas['declined_text'] = '';
				}
			}

			$data['res_hard_copy'] = array(
				array("name" => "DITOLAK"),
				array("name" => "DISETUJUI")
			);

			$data['res_declined_reason'] = array(
				array("name" => "Berkas tidak lengkap"),
				array("name" => "Berkas tidak sesuai dengan soft copy"),
				array("name" => "BAP dan/atau BASTB/BAPS tidak sesuai dengan dokumen swift"),
				array("name" => "Tanggal BAP dan/atau BASTB/BAPS tidak conform dengan tanggal dokumen diterima"),
				array("name" => "Lain-Lain")
			);

			$data['res_berkas'] = $check_berkas;
			
			$data['group_name'] = $this->get_group_name2();
			
			$data['res_berkas_verify'] = $this->berkas_m->get_berkas_verify(array('where' => array('berkas_id' => $check_berkas['id']), 'order_sort' => 'verify_level_index'))->result_array();
			
			$data['res_berkas_files'] = $this->berkas_m->get_berkas_files(array('where' => array('berkas_id' => $check_berkas['id']), 'order_sort' => 'file_id'))->result_array();

			$prm_etc = array(
				'table' => 'berkas_files_etc',
				'where' => array(
					'berkas_id' => $check_berkas['id'],
					'file_type' => 'application/pdf'
				)
			);

			$data['res_berkas_files_etc'] = $this->def_model->get_list($prm_etc)->result_array();
			
			$ref_verify_level = $this->get_verify_level($check_berkas['payment_method']);
			$data['ref_verify_level'] = $ref_verify_level;
			$data['max_level_berkas'] = count($ref_verify_level);
			
			$this->navigation_manager->rebuild_breadcrumb(
				$this->class,
				array("#" => $this->lang->line('global_detail'))
			);
			
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-inputmask/jquery.inputmask.bundle.min.js').'"></script>';
			
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-datepicker/css/datepicker.css').'" />';
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js').'"></script>';
			
			$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-wizard/jquery.bootstrap.wizard.custom.css').'" />';
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js').'"></script>';
			
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
			$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.pulsate.min.js').'"></script>';
			
			$this->header['js'][]  = '<script type="text/javascript">var refBerkasStatus = '.json_encode($this->ref_berkas_status).'</script>';
			

			$this->load->view('global/header_view',$this->header);
			$this->load->view('main/berkas_detail_view', $data);
			$this->load->view('global/footer_view');
		}
		else
		{
			redirect('backend/404');
		}
	}
	
	function approval()
	{
		$data = array();
		$data_verify = array();
		
		$berkas_id = $this->input->post('berkas_id');
		$verify_status_id = $this->input->post('verify_status_id');
		$verify_level_index = $this->input->post('verify_level_index');
		$payment_method = $this->input->post('payment_method');
		$ref_verify_level = $this->get_verify_level($payment_method);
				
		$date_time = $this->get_date_time();
		$user_name = $this->get_user_name();
		$user_id = $this->get_user_id();
		$employee_id = $this->get_employee_id();
		$employee_name = $this->get_employee_name();
		
		$res_berkas = $this->berkas_m->get_berkas(array('where' => array('a.id' => $berkas_id)))->row_array();
		
		if($verify_status_id == 3)
		{
			$data['last_verify_status'] = $this->ref_verify_status[$verify_status_id];
			$data['last_approval_datetime'] = $date_time;
			$data['rejected_note'] = trim($this->input->post('rejected_note'));
			$data['berkas_status'] = $this->ref_berkas_status[3];
			
			
			$data_verify['verify_status'] = $this->ref_verify_status[$verify_status_id];
			$data_verify['approval_datetime'] = $date_time;
			$data_verify['approval_by'] = $user_name;
			$data_verify['approval_by_employee_id'] = $employee_id;
			
			$log_activity = $this->lang->line('berkas_name')." (".$res_berkas['berkas_number'].") ".$this->lang->line('berkas_rejected')." Oleh ".$ref_verify_level[$verify_level_index]['verify_level'];
			$log_current_status = $this->lang->line('berkas_name')." ".$this->lang->line('berkas_back_to_originator')."  (".trim($this->input->post('rejected_note')).")";
		
			//$res_employee = $this->get_employee(array('a.id' => $res_berkas['rilis_by_employee_id']));
			$res_employee = $this->def_model->get_user_employee2(array('where' => array('b.divisi' => 'EPC', 'b.id' => $res_berkas['rilis_by_employee_id'])))->result_array();
			$verify_level = $ref_verify_level[$verify_level_index]['verify_level'];
			$email_type = 3;
		}
		else
		{
			$max_level = $this->def_model->get_max(array('table' => $this->table.'_verify', 'where' => array('berkas_id' => $berkas_id), 'select' => 'verify_level_index'));
			$max_level_index = $max_level['verify_level_index'];
			
			if($verify_level_index == $max_level_index)
			{
				// close berkas
				$data['last_verify_status'] = $this->ref_verify_status[$verify_status_id];
				$data['last_approval_datetime'] = $date_time;
				$data['berkas_status'] = $this->ref_berkas_status[2];
				
				$data_verify['verify_status'] = $this->ref_verify_status[$verify_status_id];
				$data_verify['approval_datetime'] = $date_time;
				$data_verify['approval_by'] = $user_name;
				$data_verify['approval_by_employee_id'] = $employee_id;
				
				$log_activity = $this->lang->line('berkas_name')." (".$res_berkas['berkas_number'].") ".$this->lang->line('berkas_approved')." Oleh ".$ref_verify_level[$verify_level_index]['verify_level'];
				$log_current_status = $this->lang->line('berkas_done_at')." ".$ref_verify_level[$verify_level_index]['verify_level'];
			
				$res_employee = $this->def_model->get_user_employee2(array('where' => array('b.divisi' => 'EPC', 'b.id' => $res_berkas['rilis_by_employee_id'])))->result_array();
				$verify_level = "";
				$email_type = 2;
			}
			else
			{
				// update utk level saat ini
				$prm_update3 = array(
					'table' => $this->table.'_verify',
					'data'  => array(
						'verify_status' => $this->ref_verify_status[$verify_status_id],
						'approval_datetime' => $date_time,
						'approval_by' => $user_name,
						'approval_by_employee_id' => $employee_id
					),
					'where' => array('berkas_id' => $berkas_id, 'verify_level_index' => $verify_level_index)
				);
			
				$this->def_model->update($prm_update3);
				
				$log_activity = $this->lang->line('berkas_name')." (".$res_berkas['berkas_number'].") ".$this->lang->line('berkas_approved')." Oleh ".$ref_verify_level[$verify_level_index]['verify_level'];
					
				// update utk level berikutnya
				$verify_level_index = $verify_level_index + 1;
				
				$data['last_verify_level'] = $ref_verify_level[$verify_level_index]['verify_level'];
				$data['last_verify_level_index'] = $verify_level_index;
				$data['last_verify_datetime'] = $date_time;
				
				$data_verify['verify_level'] = $ref_verify_level[$verify_level_index]['verify_level'];
				$data_verify['verify_level_index'] = $verify_level_index;
				$data_verify['verify_datetime'] = $date_time;
				$data_verify['verify_status'] = $this->ref_verify_status[1];
				
				$log_current_status = $this->lang->line('berkas_pending_at')." ".$ref_verify_level[$verify_level_index]['verify_level'];
				
				//$res_employee = $this->get_employee(array('a.divisi' => 'EPC', 'b.glosarry' => $ref_verify_level[$verify_level_index]));$verify_level_min_val['group_child_id']
				$res_employee = $this->def_model->get_user_employee2(array('where' => array('b.divisi' => 'EPC', 'a.sys_group_child_id' => $ref_verify_level[$verify_level_index]['group_child_id'])))->result_array();
				$verify_level = $ref_verify_level[$verify_level_index]['verify_level'];
				$email_type = 1;
			}
		}
		
		$data['modified_datetime'] = $date_time;
		$data['modified_by'] = $user_name;
		 
		$prm_update = array(
			'table' => $this->table,
			'data'  => $data,
			'where' => array('id' => $berkas_id)
		);
		
		if($this->def_model->update($prm_update))
		{
			// create log berkas
			$log_by_name = ! empty($employee_name) ? $employee_name : $user_name;
			$this->berkas_m->log_berkas(
				array(
					'berkas_id' => $berkas_id,
					'berkas_number' => $res_berkas['berkas_number'],
					'activity' => $log_activity,
					'current_status' => $log_current_status,
					'log_datetime'=> $date_time,
					'log_by_name' => $log_by_name,
					'log_by_userid' => $user_id
				)
			);
			
			// override param $res_employee
			if($this->site_config['is_email_test_active'] && $this->is_email_valid($this->site_config['email_test']))
			{
				$res_employee = array();
				$res_employee[] = array(
					'emp_id' => '0',
					'email' => $this->site_config['email_test'],
					'employee_name' => 'Admin Testing',
					'nrp' => 'Unknown'
				);
				
			}
			
			if(! empty($res_employee))
			{
				$is_email_sent = $this->send_email_berkas($berkas_id, $res_employee, $email_type, $verify_level);	
				
				if(! empty($is_email_sent))
				{
					$data_verify['email_sent_to'] = json_encode($is_email_sent);
				}
			}
			
			$prm_update2 = array(
				'table' => $this->table.'_verify',
				'data'  => $data_verify,
				'where' => array('berkas_id' => $berkas_id, 'verify_level_index' => $verify_level_index)
			);
			
			if($this->def_model->update($prm_update2))
			{
				echo $this->output->status_callback('json_success');
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess', $this->lang->line('global_db_error_save'));
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess', $this->lang->line('global_error'));
		}
	}
	
	function save_pembayaran()
	{
		$date_time = $this->get_date_time();
		$user_name = $this->get_user_name();
		$employee_id = $this->get_employee_id();
		
		$id = $this->input->post('pembayaran_id') ? $this->input->post('pembayaran_id') : ''; 
		
		$data = array();
		foreach($this->input->post() as $key => $val)
		{
			if($key != 'pembayaran_id') 
			{
				if($key == 'payment_date')
				{
					$data[$key] =  $this->date_format_Ymd($val,'-');
				}
				elseif($key == 'nominal')
				{
					$data[$key] = $this->replace_str_numeric($val,'_,.');
				}
				else
				{
					$data[$key] = trim($val);
				}
			}
		}
		
		if(!$id) // add
		{
			$action_lang = 'added';
			
			$prm_max = array(
				'table' => $this->table.'_pembayaran',
				'select' => 'termin', 
				'where' => array('berkas_id' => $data['berkas_id'])
			);
			$res_max = $this->def_model->get_max($prm_max);
			
			$data['termin'] = ! empty($res_max['termin']) ? ($res_max['termin'] + 1) : 1;
			$data['created_datetime'] = $date_time;
			$data['created_by'] = $user_name;
			
			$prm = array(
				'table' => $this->table.'_pembayaran',
				'data' => $data
			);
		
			$res = $this->def_model->insert($prm);
			$pembayaran_id = $this->db->insert_id();
		}
		else
		{
			$action_lang = 'edited';
			$data['modified_datetime'] = $date_time;
			$data['modified_by'] = $user_name;

			$prm = array(
				'table' => $this->table.'_pembayaran',
				'data'  => $data,
				'where' => array('id' => $id)
			);
			
			$res = $this->def_model->update($prm);
			$pembayaran_id = $id;
		}
		
		if($res)
		{
			// general log
			if($this->db->affected_rows())
			{				
				$this->log_activities(
					array(
						'module' => $this->class,
						'lang_subject' => 'berkas_payment',
						'lang_item'    => $data['berkas_id'],
						'lang_action'  => '[global_activity_'.$action_lang.']'
					)
				);
			}
			
			echo $this->output->status_callback('json_success');	
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess', $this->lang->line('global_error'));	
		}
	}
	
	function get_pembayaran_hutang()
	{
		$berkas_id = $this->input->post('berkas_id');
		
		$res_pembayaran_hutang = $this->berkas_m->get_total_pembayaran_hutang(array('where' => array('a.id' => $berkas_id)));
		
		$arr2 = array();
		foreach($res_pembayaran_hutang as $val)
		{
			$arr2[] = $this->money_format($val); 
		}
		
		$res_pembayaran = $this->berkas_m->get_pembayaran(array('where' => array('berkas_id' => $berkas_id)))->result_array();
		
		$arr = array();
		foreach($res_pembayaran as $data)
		{
			$data['nominal'] = $this->money_format($data['nominal']);
			$payment_date = $data['payment_date'];
			$data['payment_date'] = $this->convert_date($payment_date, 'd M Y');
			$data['payment_dates'] = $this->convert_date($payment_date, 'd-m-Y');
			
			$invoice_date = $data['invoice_date'];
			$data['invoice_date'] = $this->convert_date($invoice_date, 'd M Y');
			$data['invoice_dates'] = $this->convert_date($invoice_date, 'd-m-Y');
			$arr[] = $data;
		}
		
		echo '{ "success":true, "data": '.json_encode($arr).', "data2": '.json_encode($arr2).' }';
	}
	
	function delete_pembayaran()
	{
		if($this->input->post('id'))
		{	
			$prm_delete = array(
				'table' => $this->table.'_pembayaran',
				'where' => array('id' => $this->input->post('id'))
			);
			
			if($this->def_model->delete($prm_delete))
			{
				echo $this->output->status_callback('json_success');	
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
	
	function export_report($ids = 0)
	{
		$id = base64_decode($ids);
		$res_berkas = $this->berkas_m->get_berkas(array('where' => array('a.id' => $id)))->row_array();
		if(! empty($res_berkas))
		{
			$check_list = strtoupper($this->lang->line('berkas_check_list'));
			$check_list2 = strtolower($this->replace_space($check_list));
			
			$file_name = $res_berkas['vendor_name'].'_'.$res_berkas['project_code'].$res_berkas['berkas_number'];
			$path_pdf = ABSOLUTE_PATH.'asset/berkas_files/'.md5($res_berkas['berkas_number']).'/'.$check_list2.'_'.$file_name.'.pdf';

			if(! file_exists($path_pdf))
			{
				$this->create_dir(ABSOLUTE_PATH.'asset/berkas_files/'.md5($res_berkas['berkas_number']));
				
				$bg_path = ABSOLUTE_PATH.'asset/backend/img/bg_report.jpg';
				$header = "<html><head>
				<style>
				body {
					font-family: 'RobotoRegular','Helvetica Neue',Helvetica,sans-serif;
					font-size: 12px;
				}

				h2,h3,h4{
					line-height: normal;  
				}

				h4 {
					font-size: 20px;
					line-height: 5px;  
					text-decoration: underline;
					font-weight: bold;
				}
				
				h3 {
					font-size: 22px;
				}

				h2 {
					font-size: 31.5px;
					text-align: right;
				}

				strong{
					font-family: 'RobotoRegular','Helvetica Neue',Helvetica,sans-serif;
					font-weight: bold;
					line-height: 21px;
					color:#666666;
				}

				.clear {
					clear: both;
					display: block;
					height: 0;
					overflow: hidden;
					visibility: hidden;
					width: 0;
				}
				
				p {
					margin: 15px 0;
				}
				
				table {
					border-collapse: collapse;
					font-size: 12px;
				}
				
			
				table td {
					padding: 5px;
				}
				

				.table{
					width: 100%;
					border: 1px solid black;
					border-spacing: 0px;
					cellspacing: 0px;
				}

				.table th {
					font-weight: normal;
					text-transform: uppercase;
					padding:5px;
					text-align: center;
					border: 1px solid black;
					border-spacing: 0px;
				}

				.table td {
					border: 1px solid black;
					line-height: 20px;
				}

				table td.center, table th.center {
					text-align: center;
				}
				
				.table td {
					border-top: 1px solid black;
					line-height: 20px;
					padding: 10px;
					text-align: left;
					vertical-align: top;
				}
				
				.center {
					float: none;
					margin-left: auto;
					margin-right: auto;
					text-align: center;
				}
				
				span.symbol  {
					font-weight:bold;
					font-size:16px;
					margin-right: 10px;
					color:#111111;
				}
				
				.gradient {
					border:0.1mm solid #220044; 
					background-color: #f0f2ff;
					background-gradient: linear #c7cdde #f0f2ff 0 1 0 0.5;
				}
				.radialgradient {
					border:0.1mm solid #220044; 
					background-color: #f0f2ff;
					background-gradient: radial #00FFFF #FFFF00 0.5 0.5 0.5 0.5 0.65;
					margin: auto;
				}
				.rounded {
					border:0.1mm solid #220044; 
					background-color: #f0f2ff;
					background-gradient: linear #c7cdde #f0f2ff 0 1 0 0.5;
					border-radius: 2mm;
					background-clip: border-box;
				}
				
				div {
					padding:0em; 
					margin-bottom: 0em;
					text-align:justify; 
					font-size: 12px;
				}
				.example pre {
					background-color: #d5d5d5; 
					margin: 1em 1cm;
					padding: 0 0.3cm;
				}

				pre { text-align:left }
				pre.code { font-family: monospace }
				
				.background {
					background: transparent url({$bg_path}) repeat fixed center center; background-color:#ccffff; 
				}
				
				.myfixed1 { 
					position: absolute; 
					overflow: visible; 
					left: 0; 
					bottom: 0; 
					border: 1px solid #880000; 
					background-color: #FFEEDD; 
					background-gradient: linear #dec7cd #fff0f2 0 1 0 0.5;  
					padding: 1.5em; 
					font-family:sans; 
					margin: 0;
				}
				</style>
				</head><body>";
				$footer = "</body></html>";
				
				$this->load->library('mpdf/mpdf');
				$logo_path = ABSOLUTE_PATH.'asset/backend/img/logo_pp.png';
				
				$strHtml = '<div style="padding: 20px 5px 20px 5px;">';
				$strHtml .= '<table border="0" width="100%">
					<tr>
						<td width="10%" rowspan="2"><img width="100px;" src="'.$logo_path.'"/></td>
						<td class="center">
							<h3>'.$check_list.'</h3>
							<h3>'.$this->site_config["app_author"].'</h3>
							<h3>'.$res_berkas["payment_method"].'</h3>
						</td>
					</tr>
				</table>
				<div style="border-top: 2px solid;"></div>';
				
				$res_berkas['invoice_date'] = ! empty($res_berkas['invoice_date']) ? $this->convert_date($res_berkas['invoice_date'], 'd M Y') : '';
				$res_berkas['ttap_date'] = ! empty($res_berkas['ttap_date']) ? $this->convert_date($res_berkas['ttap_date'], 'd M Y') : '';
				$res_berkas['faktur_pajak_date'] = ! empty($res_berkas['faktur_pajak_date']) ? $this->convert_date($res_berkas['faktur_pajak_date'], 'd M Y') : '';
				$res_berkas['surat_jalan_lkp_date'] = ! empty($res_berkas['surat_jalan_lkp_date']) ? $this->convert_date($res_berkas['surat_jalan_lkp_date'], 'd M Y') : '';
				$res_berkas['spk_po_date'] = ! empty($res_berkas['spk_po_date']) ? $this->convert_date($res_berkas['spk_po_date'], 'd M Y') : '';
				
				if(! empty($res_berkas['bap_bpg_dub_number']) && is_array(json_decode($res_berkas['bap_bpg_dub_number'])))
				{
					$bap_bpg_dub_number = json_decode($res_berkas['bap_bpg_dub_number']);
					$bap_bpg_dub_date = json_decode($res_berkas['bap_bpg_dub_date']);
					$bap_bpg_dub_number_date = array();
					
					foreach($bap_bpg_dub_number as $key => $val)
					{
						$bap_bpg_dub_number_date[] = $val.(isset($bap_bpg_dub_date[$key]) ? ' ( '.$this->convert_date($bap_bpg_dub_date[$key],'d M Y').' )' : '');
					}
					
					if(! empty($bap_bpg_dub_number_date))
					{
						$res_berkas['bap_bpg_dub_number_date'] = implode(", ",$bap_bpg_dub_number_date);
					}
				}
				
				$res_berkas_verify = $this->berkas_m->get_berkas_verify(array('where' => array('berkas_id' => $res_berkas['id']), 'order_sort' => 'verify_level_index'))->result_array();
				$res_berkas_files = $this->berkas_m->get_berkas_files(array('where' => array('berkas_id' => $res_berkas['id']), 'order_sort' => 'file_id'))->result_array();
				
				$strHtml .= '
					<h3 class="center">'.$res_berkas['project_code'].$res_berkas['berkas_number'].'</h3>
					<div>
						<div style="float: left; width: 65%;">
							<table border="0" style="width: 65%;">
								<tbody>
									<div>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_project_name').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['project_name']) ? $res_berkas['project_name'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_vendor_name').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['vendor_name']) ? $res_berkas['vendor_name'] : '-').' '.(! empty($res_berkas['vendor_code']) ? ' ( '.$res_berkas['vendor_code'].' )' : '').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_vendor_type').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['vendor_type']) ? $res_berkas['vendor_type'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_npwp').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['vendor_npwp']) ? $res_berkas['vendor_npwp'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_barang_jasa').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['barang_jasa_name']) ? $res_berkas['barang_jasa_name'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_invoice_number').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['invoice_number']) ? $res_berkas['invoice_number'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_invoice_date').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['invoice_date']) ? $res_berkas['invoice_date'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_faktur_pajak_number').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['faktur_pajak_number']) ? $res_berkas['faktur_pajak_number'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_faktur_pajak_date').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['faktur_pajak_date']) ? $res_berkas['faktur_pajak_date'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_pajak_period').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['pajak_period']) && ! empty($res_berkas['pajak_year']) ? $res_berkas['pajak_period'].' - '.$res_berkas['pajak_year'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_bap_bpg_dub').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(isset($res_berkas['bap_bpg_dub_number_date']) ? $res_berkas['bap_bpg_dub_number_date'] : '-').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_surat_jalan_lkp').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['surat_jalan_lkp_number']) ? $res_berkas['surat_jalan_lkp_number'] : '-').' '.(! empty($res_berkas['surat_jalan_lkp_date']) ? ' ( '.$res_berkas['surat_jalan_lkp_date'].' )' : '').'</td>
									</tr>
									<tr style="border: 1px solid;">
										<td style="width:80pt;"><b>'.$this->lang->line('berkas_spk_po').'</b></td>
										<td style="width:2pt;">:</td>
										<td style="width:300pt;">'.(! empty($res_berkas['spk_po_number']) ? $res_berkas['spk_po_number'] : '-').' '.(! empty($res_berkas['spk_po_date']) ? ' ( '.$res_berkas['spk_po_date'].' )' : '').'</td>
									</tr>
								</tbody>
							</table>
							<br>
							<table border="1">
								<tbody>
									<tr>
										<th style="width:130pt;"><b>'.$this->lang->line('berkas_real_cost').'</b></th>
										<th style="width:65pt;"><b>'.$this->lang->line('berkas_ppn').' ('.$res_berkas['ppn'].'%)</b></th>
										<th style="width:65pt;"><b>'.$this->lang->line('berkas_pph').' ('.$res_berkas['pph'].'%)</b></th>
										<th style="width:130pt;"><b>'.$this->lang->line('berkas_netto_cost').'</b></th>
									</tr>
									<tr>
										<td style="text-align:right;">'.(! empty($res_berkas['real_cost']) ? $this->money_format($res_berkas['real_cost']) : '-').'</td>
										<td style="text-align:right;">'.(! empty($res_berkas['ppn_val']) ? $this->money_format($res_berkas['ppn_val']) : '-').'</td>
										<td style="text-align:right;">'.(! empty($res_berkas['pph_val']) ? $this->money_format($res_berkas['pph_val']) : '-').'</td>
										<td style="text-align:right;">'.(! empty($res_berkas['netto_cost_val']) ? $this->money_format($res_berkas['netto_cost_val']) : '-').'</td>
									</tr>
								</tbody>
							</table>
							
							<br>
							<table border="1">
								<tbody>
									<tr>
										<th style="width:130pt;"><b>'.$this->lang->line('berkas_verification_done').'</b></th>
										<th style="width:130pt;"><b>'.$this->lang->line('global_date_long').'</b></th>
										<th style="width:130pt;"><b>'.$this->lang->line('global_signature2').'</b></th>
									</tr>';
									
									$path = ABSOLUTE_PATH.'asset/';
									foreach($res_berkas_verify as $dt)
									{
										$res_usr = $this->def_model->get_one(array(
											'table' => 'sys_user',
											'where' => array(
												'name' => $dt['approval_by']
											)
										))->row_array();
										
										$signature = "";
										
										if(isset($res_usr['signature_path']) && ! empty($res_usr['signature_path']) && file_exists($path.$res_usr['signature_path']))
										{
											$signature = '<img width="50pt;" src="'.$path.$res_usr['signature_path'].'"/>';
										}
										
										$dt['approval_datetime'] = ! empty($dt['approval_datetime']) ? $this->convert_date($dt['approval_datetime'], 'd M Y') : '';
										$strHtml .= '<tr>
											<td style="text-align:center;">'.(! empty($dt['verify_level']) ? $dt['verify_level'] : '-').'</td>
											<td style="text-align:center;">'.(! empty($dt['approval_datetime']) ? $dt['approval_datetime'] : '-').'</td>
											<td style="text-align:center;">'.(! empty($signature) ? $signature : '-').'</td>
										</tr>';
									}
								$strHtml .= '</tbody>
							</table>
						</div>

						<div style="float: right; width: 33%;">
							<table border="1">
								<tbody>
									<tr>
										<th style="width:100pt;"><b>'.$this->lang->line('berkas_project_code').'</b></th>
										<th style="width:100pt;"><b>'.$this->lang->line('berkas_number').'</b></th>
									</tr>
									<tr>
										<td style="width:100pt;text-align:center;">'.(! empty($res_berkas['project_code']) ? $res_berkas['project_code'] : '-').'</td>
										<td style="width:100pt;text-align:center;">'.(! empty($res_berkas['berkas_number']) ? $res_berkas['berkas_number'] : '-').'</td>
									</tr>
								</tbody>
							</table>
							<br>
							<table>
								<tbody>
									<tr style="border: 1px solid;">
										<td style="width:50pt;"><b>'.$this->lang->line('berkas_ttap_date').'</b></td>
										<td style="width:5pt;">:</td>
										<td style="width:150pt;">'.(! empty($res_berkas['ttap_date']) ? $res_berkas['ttap_date'] : '-').'</td>
									</tr>
								</tbody>
							</table>
							<br>
							<p>'.$this->lang->line('berkas_check_list_lengkap').':</p>
							<table style="font-size:10px;">
								<tbody>';
									foreach($res_berkas_files as $dt)
									{
										$strHtml .= '<tr>
											<td style="width:20pt;border: 1px solid;text-align:center;padding:10px;">&radic;</td>
											<td style="width:3pt;">&nbsp;</td>
											<td style="width:150pt;">'.$dt['file_name'].'</td>
										</tr><br>';
									}
								$strHtml .= '</tbody>
							</table>
							
						</div>

						<div style="clear: both; margin: 0pt; padding: 0pt; "></div>
					</div>
				';
				$strHtml .= '</div>';
				
				$mpdf = new mPDF('c','A4','','' , 10 , 10 , 0 , 0);
				$mpdf->SetDisplayMode('fullpage');
				$mpdf->simpleTables = false;
				$mpdf->packTableData = false;
				$mpdf->WriteHTML($header);
				$mpdf->WriteHTML($strHtml);
				$mpdf->WriteHTML($footer);
				
				$mpdf->Output($path_pdf,"F");
		    }
			
			$res_berkas_files = $this->berkas_m->get_berkas_files(array('where' => array('berkas_id' => $id), 'order_sort' => 'file_id'))->result_array();
			
			$this->load->library('zip');
			
			$this->zip->read_file($path_pdf);
			
			$path = ABSOLUTE_PATH.'asset/';
			foreach($res_berkas_files as $file)
			{
				$full_path = $path.'/'.$file['file_path'];
				
				$this->zip->read_file($full_path);
			}
		
			$file_name_zip = 'REPORT_'.$file_name.'.zip';
			$this->zip->download($file_name_zip);
		}
		else
		{
			die($this->lang->line('global_file').' '.$this->lang->line('global_not_found'));
		}
	}
	
	function update_hard_copy()
	{
		$berkas_id = $this->input->post('berkas_id');
		$value = $this->input->post('value');
		
		if(! empty($berkas_id))
		{
			$res = $this->def_model->update(
				array(
					'table' => $this->table, 
					'data' => array('is_hard_copy' => $value), 
					'where' => array(
						'id' => $berkas_id
					)
				)
			);
			
			if($res)
			{
				echo $this->output->status_callback('json_success');
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
	
	function get_berkas_vendor_type()
	{
		$berkas_id = $this->input->post('berkas_id');
		$vendor_type_name = $this->input->post('vendor_type_name');
		
		$res_berkas_vendor_type = $this->berkas_m->get_berkas_vendor_type(array('where' => array('b.name' => $vendor_type_name), 'order_sort' => 'ref_berkas_type_id', 'order_dir' => 'DESC'))->result_array();
		$html = "";
		foreach($res_berkas_vendor_type as $val)
		{

			if($val['berkas_type_name'] == 'BERITA ACARA PROGRESS SKBDN' || $val['berkas_type_name'] == 'BERITA ACARA PEMBAYARAN SKBDN' || $val['berkas_type_name'] == 'INVOICE SKBDN'){
				continue;
			}

			$html .= '
				<div class="control-group">
					<label class="control-label">'.$val['berkas_type_name'].'</label>
					<div class="controls">
						<div data-provides="fileupload" class="fileupload fileupload-new">
							<input type="hidden" value="'.$val['berkas_type_id'].'" name="berkas_files_id[]">
							<div class="input-append ">
								<div class="uneditable-input">
									<i class="icon-file fileupload-exists"></i> 
									<span class="fileupload-preview"></span>
								</div>
								<span class="btn btn-file" data-title="'.$val['berkas_type_name'].'">
									<span class="fileupload-new">'.$this->lang->line('global_select_file').'</span>
									<span class="fileupload-exists">'.$this->lang->line('global_change').'</span>
									<input type="file" class="default" name="berkas_files[]">
								</span>
								<a data-dismiss="fileupload" class="btn fileupload-exists" href="#">'.$this->lang->line('global_remove').'</a>
							</div>
						</div>
						<div id="file_link" style="line-height:10px;">';
						if(! empty($berkas_id))
						{
							$check_files = $this->def_model->get_one(array('table' => 'berkas_files', 'where' => array('berkas_id' => $berkas_id, 'file_id' => $val['berkas_type_id'])))->row_array();
						
							if(isset($check_files['file_path']) && ! empty($check_files['file_path']))
							{
								$html .= '<span><a target="_blank" href="'.app_asset_url($check_files['file_path']).'">'.$this->lang->line('global_view_file').'</a> | <a class="delete_file" data-id="'.$check_files['id'].'" data-path="'.$check_files['file_path'].'" href="javascript:;">'.$this->lang->line('global_delete_file').'</a></span>';
							}
						}
						$html .= '</div>
					</div>
				</div>
			';
		}
		echo $html;
	}

	function get_pph_code($vendor_type){
		$where['vendor_type'] = urldecode($vendor_type);
		$prm = array(
			'where' => $where,
			'table' => 'ref_pph_percentage',
		);
		header('Content-type: application/json');
		echo json_encode($this->def_model->get_list($prm)->result());
	}

	function upload_faktur_pajak(){
		ini_set('memory_limit','-1');
		@set_time_limit(-1);
		$folder = md5(time() . rand() . $this->get_user_name());
		$path = ABSOLUTE_PATH.'asset/tmp/'.$folder;
		$this->create_dir($path);

		$config['upload_path'] 	 = ABSOLUTE_PATH.'asset/tmp/'.$folder.'/';
		$config['allowed_types'] = 'pdf';
		$config['overwrite']	 = TRUE;
		$config['remove_spaces'] = TRUE;
		$config['max_size']      = '10000000000';

		$this->load->library('upload');
		$this->load->library('cmd_lib');

		if (!empty($_FILES)){
			if( ! empty($_FILES['faktur_pajak']['name']) && $_FILES['faktur_pajak']['size'] > 0 )
			{
				//convert .PDF to .pdf
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $_FILES['faktur_pajak']['name']);
				$config['file_name'] = substr(md5($withoutExt), 0,6).'.pdf';

				$this->upload->initialize($config);

				if(!$this->upload->do_upload('faktur_pajak')){
					$response =  array(
						'error' => $this->upload->display_errors(),
						'uploaded' => array(
							'filename'  => NULL,
							'filesize'  => NULL,
							'filepath'  => NULL
						)
					);
					http_response_code(403);
					header( 'Content-Type: application/json; charset=utf-8' );
					echo json_encode( $response);
				}else{

					$uploaded = $this->upload->data();
					$removedExtenstion = preg_replace('/\\.[^.\\s]{3,4}$/', '', $uploaded['file_name']);
					$converted_to_img = $removedExtenstion.'.png';
					$converted_to_text = $removedExtenstion.'.txt';

					$file_source = ABSOLUTE_PATH.'asset/tmp/'.$folder.'/'. $uploaded['file_name'];
					$file_out = ABSOLUTE_PATH.'asset/tmp/'.$folder.'/'.$converted_to_img;
					$text_out = ABSOLUTE_PATH.'asset/tmp/'.$folder.'/'.$converted_to_text;
					$url = $this->cmd_lib->get_url_from_qr($file_source, $file_out, $text_out);

					if(!$url){
						$file_path = $folder.'/'.$uploaded['file_name'];
						$response =  array(
							'error' => 'Kode QR pada faktur pajak tidak terdeteksi',
							'uploaded' => array(
								'filename'  => $uploaded['file_name'],
								'filesize'  => $_FILES['faktur_pajak']['size'],
								'filepath'  => $file_path
							)
						);

						http_response_code(403);
						header( 'Content-Type: application/json; charset=utf-8' );
						echo json_encode( $response);

					}else{

						$file_path = $folder.'/'.$uploaded['file_name'];

						try {
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, trim($url));
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							curl_setopt($ch, CURLOPT_HTTPHEADER, array(
								'Content-type: text/xml'
							));
							$xmls = curl_exec($ch);
							$error  = curl_error($ch);
							curl_close($ch);
							if(!empty($error)){

								$response =  array(
									'error' => $error,
									'uploaded' => array(
										'filename'  => $uploaded['file_name'],
										'filesize'  => $_FILES['faktur_pajak']['size'],
										'filepath'  => $file_path
									)
								);
								http_response_code(403);
								header( 'Content-Type: application/json; charset=utf-8' );
								echo json_encode( $response);
								die();
							}
							$xmlData  = new SimpleXMLElement($xmls);

						}
						catch(Exception $e){
							$response =  array(
								'error' => $e,
								'uploaded' => array(
									'filename'  => $uploaded['file_name'],
									'filesize'  => $_FILES['faktur_pajak']['size'],
									'filepath'  => $file_path
								)
							);
							http_response_code(403);
							header( 'Content-Type: application/json; charset=utf-8' );
							echo json_encode( $response);
							die();
						}


						if(! empty($xmlData))
						{
							$xml_array = xml_to_array($xmlData);
							$file_path = $folder.'/'.$uploaded['file_name'];

							if($xml_array['statusApproval'] == 'Faktur Valid, Sudah Diapprove oleh DJP'){
								$response = array(
									'url'   => $url,
									'data'  => $xml_array,
									'uploaded' => array(
										'filename'  => $uploaded['file_name'],
										'filesize'  => $_FILES['faktur_pajak']['size'],
										'filepath'  => $file_path
									)
								);

								http_response_code(200);
								header( 'Content-Type: application/json; charset=utf-8' );
								echo json_encode( $response);
							}else{
								$response =  array(
									'error' => $xml_array['statusApproval'],
									'uploaded' => array(
										'filename'  => $uploaded['file_name'],
										'filesize'  => $_FILES['faktur_pajak']['size'],
										'filepath'  => $file_path
									)
								);
								http_response_code(403);
								header( 'Content-Type: application/json; charset=utf-8' );
								echo json_encode( $response);
							}
						}else{
							$response =  array(
								'error' => 'Kode QR pada faktur pajak tidak terdeteksi',
								'uploaded' => array(
									'filename'  => $uploaded['file_name'],
									'filesize'  => $_FILES['faktur_pajak']['size'],
									'filepath'  => $file_path
								)
							);
							http_response_code(403);
							header( 'Content-Type: application/json; charset=utf-8' );
							echo json_encode( $response);
						}

					}
				}
			}else{
				$response =  array(
					'error' => 'Kode QR pada faktur pajak tidak terdeteksi',
					'uploaded' => array(
						'filename'  => NULL,
						'filesize'  => NULL,
						'filepath'  => NULL
					)
				);
				http_response_code(403);
				header( 'Content-Type: application/json; charset=utf-8' );
				echo json_encode($response);
			}
		}else{
			$response =  array(
				'error' => 'Kode QR pada faktur pajak tidak terdeteksi',
				'uploaded' => array(
					'filename'  => NULL,
					'filesize'  => NULL,
					'filepath'  => NULL
				)
			);
			http_response_code(403);
			header( 'Content-Type: application/json; charset=utf-8' );
			echo json_encode($response);
		}
	}

	function delete_faktur_pajak(){

		if($folder = $this->input->post('path')){

			$parent =  explode('/', $folder);

			$temp_file = ABSOLUTE_PATH.'asset/tmp/'.$this->input->post('path');
			$faktur_file = ABSOLUTE_PATH.'asset/faktur_pajak/'.$this->input->post('path');

			if(file_exists($temp_file)){
				$this->recursiveRemoveDirectory(ABSOLUTE_PATH.'asset/tmp/'.$parent[0]);
			}

			if(file_exists($faktur_file)){
				$this->recursiveRemoveDirectory(ABSOLUTE_PATH.'asset/faktur_pajak/'.$parent[0]);
				$data =  array(
					'uploaded_fp_filepath'  => NULL,
					'uploaded_fp_filename'  => NULL,
					'uploaded_fp_filesize'  => NULL,
					'faktur_pajak_url'      => NULL,
					'faktur_pajak_approval' => NULL,
					'faktur_pajak_status'   => NULL,
				);

				//todo clear upload_fp_filepath in db
				$prm_update = array(
					'table' => 'berkas',
					'data' => $data,
					'where' => array('uploaded_fp_filepath' => $folder)
				);

				$this->def_model->update($prm_update);
			}

			echo $this->output->status_callback('json_success', 'Dokumen Faktur Pajak berhasil dihapus');
		}

	}

	function delete_faktur_pajak_2(){

		if($folder = $this->input->post('path')){

			$parent =  explode('/', $folder);

			$temp_file = ABSOLUTE_PATH.'asset/tmp/'.$this->input->post('path');
			$faktur_file = ABSOLUTE_PATH.'asset/faktur_pajak/'.$this->input->post('path');

			if(file_exists($temp_file)){
				$this->recursiveRemoveDirectory(ABSOLUTE_PATH.'asset/tmp/'.$parent[0]);
			}

			if(file_exists($faktur_file)){
				$this->recursiveRemoveDirectory(ABSOLUTE_PATH.'asset/faktur_pajak/'.$parent[0]);
				$data =  array(
					'uploaded_fp_filepath_2'  => NULL,
					'uploaded_fp_filename_2'  => NULL,
					'uploaded_fp_filesize_2'  => NULL,
					'faktur_pajak_url_2'      => NULL,
					'faktur_pajak_approval_2' => NULL,
					'faktur_pajak_status_2'   => NULL,
					'faktur_pajak_number_2'   => NULL,
					'faktur_pajak_date_2'       => NULL,
				);

				//todo clear upload_fp_filepath in db
				$prm_update = array(
					'table' => 'berkas',
					'data' => $data,
					'where' => array('uploaded_fp_filepath_2' => $folder)
				);

				$this->def_model->update($prm_update);
			}

			echo $this->output->status_callback('json_success', 'Dokumen Faktur Pajak berhasil dihapus');
		}

	}

	function get_ppn_code($pajak_type){
		$where['pajak_type'] = urldecode($pajak_type);
		$prm = array(
			'where' => $where,
			'table' => 'ref_ppn_percentage',
		);
		header('Content-type: application/json');
		echo json_encode($this->def_model->get_list($prm)->result());
	}

	function get_scanned_url(){
		if($url = $this->input->post("barcode_faktur_url")){
			try {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, trim($url));
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-type: text/xml'
				));
				$xmls = curl_exec($ch);
				$error  = curl_error($ch);
				curl_close($ch);
				if(!empty($error)){

					echo $this->output->status_callback('json_unsuccess', $error);
					die();
				}
				$xmlData  = new SimpleXMLElement($xmls);
			}
			catch(Exception $e){
				echo $this->output->status_callback('json_unsuccess', $e);
				die();
			}

			if(isset($xmlData) && !empty($xmlData))
			{
				$xml_array = xml_to_array($xmlData);
				$message = "QR Code Terbaca";
				echo '{"success": true, "message": "'.$message.'", "data": '.json_encode($xml_array).'}';

			}else{
				echo $this->output->status_callback('json_unsuccess', 'Gagal Mengambil Data');
			}
		}

	}

	function submit_doc_sap(){
		$data = array();
		if($this->input->post('no_doc_sap')){
			$data['no_doc_sap'] = $this->input->post('no_doc_sap');
		}

		$berkas_id = $this->input->post('berkas_id');
		$prm_update = array(
			'table' => $this->table,
			'data' => $data,
			'where' => array(
				'id' => $berkas_id,
			)
		);

		if($this->def_model->update($prm_update))
		{
			//Delete checklist berkas
			$res_berkas = $this->berkas_m->get_berkas(array('where' => array('a.id' => $berkas_id)))->row_array();
			$check_list = strtoupper($this->lang->line('berkas_check_list'));
			$check_list2 = strtolower($this->replace_space($check_list));

			$file_name = $this->replace_space($res_berkas['vendor_name']).'_'.$res_berkas['berkas_number'];
			$path_pdf = ABSOLUTE_PATH.'asset/berkas_files/'.md5($res_berkas['berkas_number']).'/'.$check_list2.'_'.$file_name.'.pdf';
			if(file_exists($path_pdf)){
				$this->unlink_file($path_pdf);
			}
			echo $this->output->status_callback('json_success');
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess', $this->lang->line('global_db_error_save'));
		}

	}

	function submit_hard_copy(){
		$data = array();
		$data['hard_copy_status'] = trim($this->input->post('hard_copy_status'));


		$data['hard_copy_arrived'] = $this->input->post('hard_copy_arrived') ? $this->date_format_Ymd($this->input->post('hard_copy_arrived'),'-') : NULL;
		$data['hard_copy_approved'] = $this->input->post('hard_copy_approved') ? $this->date_format_Ymd($this->input->post('hard_copy_approved'),'-') : NULL;
		if($this->input->post('hard_copy_status') == "DITOLAK"){
			$data['hard_copy_approved'] = NULL;
		}
		$data['hard_copy_description'] = "";

		if( $this->input->post('declined_reason') == 'Lain-Lain'){
			$data['hard_copy_description'] = $this->input->post('declined_reason') . ' - ' .trim($this->input->post('hard_copy_description_required'));
		}else{
			$data['hard_copy_description'] = $this->input->post('declined_reason') . ' - ' .trim($this->input->post('hard_copy_description'));
		}

		if($this->input->post('hard_copy_status') == "DISETUJUI"){
			$data['hard_copy_description'] = "";
		}

		$berkas_id = $this->input->post('berkas_id');
		$prm_update = array(
			'table' => $this->table,
			'data' => $data,
			'where' => array(
				'id' => $berkas_id,
			)
		);

		if($this->def_model->update($prm_update))
		{
			//Delete checklist berkas
			$res_berkas = $this->berkas_m->get_berkas(array('where' => array('a.id' => $berkas_id)))->row_array();
			$check_list = strtoupper($this->lang->line('berkas_check_list'));
			$check_list2 = strtolower($this->replace_space($check_list));

			$file_name = $this->replace_space($res_berkas['vendor_name']).'_'.$res_berkas['berkas_number'];
			$path_pdf = ABSOLUTE_PATH.'asset/berkas_files/'.md5($res_berkas['berkas_number']).'/'.$check_list2.'_'.$file_name.'.pdf';
			if(file_exists($path_pdf)){
				$this->unlink_file($path_pdf);
			}

			echo $this->output->status_callback('json_success');
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess', $this->lang->line('global_db_error_save'));
		}

	}
}