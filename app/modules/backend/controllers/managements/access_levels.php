<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access_levels extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->check_module();
	}
	
	function index()
	{
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/jquery-nestable/jquery.nestable.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-nestable/jquery.nestable.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-validation/jquery.validate.min.js').'"></script>';
		
		$data['group'] = $this->def_model->get_list(array('table' => 'sys_group_child', 'order_sort' => 'id'))->result_array();
	
		$this->load->view('global/header_view',$this->header);
		$this->load->view('managements/'.strtolower($this->class).'_view', $data);
		$this->load->view('global/footer_view');
	}
	
	function get_menu_group($mode)
	{
		$html = $this->navigation_manager->get_menu_group($mode, $this->input->post('groupID'));
		echo $this->output->status_callback('json_success', $html);
	}
	
	function disable_enable_acl($mode)
	{
		$is_active = $this->input->post('isActive');
		$group_child_id =  $this->input->post('groupID');
		$menu_id = $this->input->post('menuID');
		
		$prm = array(
			'table' => $mode.'_menu_setting',
			'data'  => array('is_active' => $this->input->post('isActive'))
		);
		
		// check if data available
		$prm_where = array('menu_id' => $menu_id, 'group_id' => $group_child_id);
		$sett_menu = $this->def_model->get_menu_sett($mode, $prm_where);
		
		if( ! empty($sett_menu))
		{
			$prm['data']['modified_datetime'] = $this->get_date_time();
			$prm['data']['modified_by'] = $this->get_user_name();
			$prm['where'] = array('id' => $sett_menu['id']);

			$res = $this->def_model->update($prm);
		}
		else
		{	
			$prm['data']['created_datetime'] = $this->get_date_time();
			$prm['data']['created_by'] = $this->get_user_name();
			$prm['data'][$mode.'_menu_id'] = $menu_id;
			$prm['data']['sys_group_child_id'] = $group_child_id;
			
			$res = $this->def_model->insert($prm);
		}
		
		if($res)
		{
			echo $this->output->status_callback('json_success');
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
	
	function get_function_acl($mode)
	{
		$group_child_id =  $this->input->post('groupID');
		$menu_id = $this->input->post('menuID');
		$menu_name = $this->input->post('menuName');
		
		$prm_where = array('menu_id' => $menu_id, 'group_id' => $group_child_id);
		$sett_menu = $this->def_model->get_menu_sett($mode, $prm_where);
		
		$active_function = array();
		if(! empty($sett_menu['active_function']))
		{
			$active_function = json_decode($sett_menu['active_function']);
		}
		
		$prm_function = array(
			'table' => $mode.'_menu_function', 
			'where' => array($mode.'_menu_id' => $menu_id)
		);
		
		$res_function = $this->def_model->get_list($prm_function)->result_array();
		
		$html = '<h4>'.$this->lang->line('global_functions').' '.$menu_name.' :</h4>';
		if(! empty($res_function))
		{
			
			$html .= '<form id="form-'.$mode.'-function-'.$group_child_id.'" class="stdform" method="post"><ul class="list-nostyle" style="margin-left:10px;">';
			foreach($res_function as $function)
			{
				$checked = '';
				if(in_array($function['name'], $active_function))
				{
					$checked = 'checked="checked"';
				}
				
				$html .= '<li><input value="'.$function['name'].'" type="checkbox" name="function[]" '.$checked.' />&nbsp'.$function['name'].'</li>';
			}
			$html .= '</ul>';
			$html .= '<input type="hidden" name="is_ajax" value="1" />';
			$html .= '<input type="hidden" name="menuID" value="'.$menu_id.'" />';
			$html .= '<input type="hidden" name="groupID" value="'.$group_child_id.'" />';
			$html .= '<br/><button class="btn blue"><i class="icon-ok"></i>'.$this->lang->line("global_save").'</button></form>';
		}
		else
		{
			$html .= '<br/><h5><strong>'.$this->lang->line('global_not_available').'</strong></h5>';
		}
		echo $this->output->status_callback('json_success', $html);
	}
	
	function save_function_acl($mode)
	{
		$group_child_id =  $this->input->post('groupID');
		$menu_id = $this->input->post('menuID');
		
		// check if data available
		$prm_where = array('menu_id' => $menu_id, 'group_id' => $group_child_id);
		$sett_menu = $this->def_model->get_menu_sett($mode, $prm_where);
		
		if(empty($sett_menu))
		{
			$prm_insert = array(
				'table' => $mode.'_menu_setting',
				'data'  => array(
					'created_datetime' => $this->get_date_time(),
					'created_by' => $this->get_user_name(),
					$mode.'_menu_id' => $menu_id,
					'sys_group_child_id' => $group_child_id
				)
			);
			
			$res = $this->def_model->insert($prm_insert);
		}
		
		if($this->input->post('function'))
		{	
			$data['active_function'] = json_encode($this->input->post('function'));
		}
		else
		{
			$data['active_function'] = "";
		}
		
		$prm = array(
			'table' => $mode.'_menu_setting',
			'data'  => $data,
			'where' => array($mode.'_menu_id' => $menu_id, 'sys_group_child_id' => $group_child_id)
		);
		
		if($this->def_model->update($prm))
		{
			echo $this->output->status_callback('json_success');
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
}