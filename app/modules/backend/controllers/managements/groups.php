<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->check_module();
	}
	
	function index()
	{
		$data = array();
		
		$data['res_group_parent'] = $this->def_model->get_list(array('table' => 'sys_group_parent'))->result_array();
		
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
		
		$this->load->view('global/header_view',$this->header);
		$this->load->view('managements/'.strtolower($this->class).'_view', $data);
		$this->load->view('global/footer_view');
	}
	
	function get_list()
	{
		$prm['table']   = 'sys_group_child';
		$prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
		$prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;
		
		if($this->input->post('iSortCol_0'))
		{
			$sort_idx = $_POST['iSortCol_0'];
			if($_POST['bSortable_'.$sort_idx] == "true")
			{
				$prm['sort'] = $this->_get_select_name($_POST['mDataProp_'.$sort_idx]);
			}
			else
			{
				$prm['sort'] = 'sys_group_child.id';
			}
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC"; 
		}
		
		if($this->input->post('sSearch'))
		{
			$prm['search'] = $this->input->post('sSearch');
			
			for($i = 0; $i < $_POST['iColumns']; $i++)
			{
				if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true" )
				{
					$prm['fields'][] =  $this->_get_select_name($_POST['mDataProp_'.$i]);
				}
			}
		}
		
		$prm['select'] = 'sys_group_child.id, sys_group_child.name, sys_group_child.description, sys_group_parent.name parent_name,  sys_group_child.sys_group_parent_id';
		
		$prm['join'][] = array(
						'table' => 'sys_group_parent',
						'on'    => 'sys_group_child.sys_group_parent_id = sys_group_parent.id',
						'type'  => 'left'
					);
		
		$res_data = $this->def_model->get_data($prm)->result_array(); //echo $this->db->last_query();
		$res_cnt  = $this->def_model->get_data($prm,TRUE)->row_array();
		$res_cnt['cnt'] = isset($res_cnt['cnt']) ? $res_cnt['cnt'] : 0;
		
		$i = $prm['start'] + 1;
        $arr = array();
        foreach($res_data as $data) 
		{
			$data['DT_RowId'] = $data['id'];
			$data['rnum'] = $i;
			$data['cbox'] = '<p style="text-align:center;" ><input type="checkbox" class="checkboxes" value="'.$data['id'].'" /></p>';
			
            $arr[] = $data;
            $i++;
        }

        echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
        unset($arr);
	}
	
	private function _get_select_name($val)
	{
		if($val == 'name') $r_val = 'sys_group_child.name';
		elseif($val == 'parent_name') $r_val = 'sys_group_parent.name';
		else $r_val = 'sys_group_child.'.$val;
		
		return $r_val;
	}
	
	function delete()
	{
		if($this->input->post('ids'))
		{
			$list_id = implode(",", $this->input->post('ids'));
				
			if($this->def_model->delete_group_child($list_id))
			{
				echo $this->output->status_callback('json_success');	
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
	}
	
	function save()
	{
		if($this->input->post('name'))
		{
			$where['name'] = $_POST['name'];
			
			if(isset($_POST['id']))
			{
			  $where['id !='] = $_POST['id'];
			}
			
			$prm_chk['where'] = $where;
			$prm_chk['table'] = 'sys_group_child';
			
			
			if($this->def_model->is_unique($prm_chk))
			{
				$id = '';
				foreach($this->input->post() as $key => $val)
				{
					if($key == 'id')
					{
						$id = $val;
					}
					else
					{
						if($key != 'is_ajax')
						{
							$data[$key] = strtoupper($val);
						}
					}
				}
				
				if(!$id)
				{
					$data['created_datetime'] = $this->get_date_time();
					$data['created_by'] = $this->get_user_name();
					
					$prm = array(
						'table' => 'sys_group_child',
						'data' => $data
					);
					
					$res = $this->def_model->insert($prm);
				}
				else
				{
					$data['modified_datetime'] = $this->get_date_time();
					$data['modified_by'] = $this->get_user_name();
					
					$prm = array(
						'table' => 'sys_group_child',
						'data'  => $data,
						'where' => array('id' => $id)
					);
					
					$res = $this->def_model->update($prm);
				}
				
				if($res)
				{
					echo $this->output->status_callback('json_success');	
				}
				else
				{
					echo $this->output->status_callback('json_unsuccess');
				}
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess',sprintf($this->lang->line('global_error_unique_field'), $this->lang->line('global_name')));
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
}