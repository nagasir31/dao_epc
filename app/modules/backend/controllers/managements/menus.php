<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menus extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->check_module();
	}
	
	function index()
	{
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/jquery-nestable/jquery.nestable.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-nestable/jquery.nestable.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/jquery-tags-input/jquery.tagsinput.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-tags-input/jquery.tagsinput.min.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-validation/jquery.validate.min.js').'"></script>';
		
		$data['menu_backend_config'] = $this->navigation_manager->get_menu_config('sys');
		$data['menu_frontend_config'] = $this->navigation_manager->get_menu_config('app');
	
		$this->load->view('global/header_view',$this->header);
		$this->load->view('managements/'.strtolower($this->class).'_view', $data);
		$this->load->view('global/footer_view');
	}
	
	function set_menu_order($mode)
	{
		global $return;
		$return = array();
	
		$array = json_decode($this->input->post('data'),true);
		$res = $this->flatten($array, NULL, 1, $return );
		
		$this->navigation_manager->set_menu_order($mode, $res);
		
		echo $this->output->status_callback('json_success');
	}
	
	function flatten($array, $parent_id)
	{
		global $return;
		
		foreach($array as $key => $val)
		{
			$data = array();
			if(is_array($array[$key]))
			{
				$this->flatten($array[$key], $parent_id, $return);
			}
			else
			{
				$return[] = array(
					'item_id' => $array[$key],
					'parent_id' => $parent_id
				);
				
				$parent_id = $array[$key];
			}
			
		}
		
		return $return;
	}
	
	function get_menu_detail($mode)
	{
		$id = $this->input->post('id');
		
		$res_menu = $this->def_model->get_menu($mode, array('where' => array('a.id' => $id)))->row_array();
		
		$html  = '<p><h4>'.(! empty($res_menu['description']) ? ucwords($res_menu['description']) : "-").'</h4></p><h5>'.$this->lang->line("global_details").' :</h5>';
		$html .= '<table class="table table-hover table-striped table-bordered">
			<tbody>
				<tr>
					<td style="width:30%;">'.$this->lang->line("global_id").'</td>
					<td>'.(! empty($res_menu['id']) ? $res_menu['id'] : "-").'</td>
				</tr>
				<tr>
					<td>'.$this->lang->line("menus_parent_id").'</td>
					<td>'.(! empty($res_menu['parent_id']) ? $res_menu['parent_id'] : "-").'</td>
				</tr>
				<tr>
					<td>'.$this->lang->line("global_name").'</td>
					<td>'.(! empty($res_menu['name']) ? $res_menu['name'] : "-").'</td>
				</tr>
				<tr>
					<td>'.$this->lang->line("menus_link_target").'</td>
					<td>'.(! empty($res_menu['link_target']) ? $res_menu['link_target'] : "-").'</td>
				</tr>
				<tr>
					<td>'.$this->lang->line("menus_link_type").'</td>
					<td>'.(! empty($res_menu['link_type']) ? $res_menu['link_type'] : "-").'</td>
				</tr>
				<tr>
					<td>'.$this->lang->line("menus_location").'</td>
					<td>'.(! empty($res_menu['location']) ? $res_menu['location'] : "-").'</td>
				</tr>
				<tr>
					<td>'.$this->lang->line("menus_cls").'</td>
					<td>'.(! empty($res_menu['cls']) ? $res_menu['cls'] : "-").'</td>
				</tr>
			</tbody>
		</table>';
		
		if(isset($this->site_config['module_function']['edit']))
		{
			$html .= '<button onclick="fnAddEditMenuForm(&quot;'.$mode.'&quot;);return false;" class="btn yellow">'.$this->lang->line("global_edit").'</button> &nbsp';
		}
		
		if(isset($this->site_config['module_function']['del']))
		{
			$html .= '<button onclick="fnDeleteMenu(&quot;'.$mode.'&quot;,&quot;'.$id.'&quot;);return false;"  class="btn red">'.$this->lang->line("global_delete").'</button> &nbsp';
		}	
		
		$html .= '</p>';

		echo $this->output->status_callback('json_success', $html);
	}
	
	function delete_menu($mode)
	{
		if($this->def_model->delete_menu($mode, $this->input->post('id')))
		{
			$this->session->set_flashdata('success_alert', $this->lang->line("global_success_delete"));
			echo $this->output->status_callback('json_success');
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess');
		}
	}
	
	function add_edit_menu_form($mode, $add = FALSE)
	{	
		$html = '';
		$res_menu_link_type = $this->def_model->get_list(array('table' => 'sys_link_type'))->result_array();
		$res_menu_link_target = $this->def_model->get_list(array('table' => 'sys_link_target'))->result_array();
		
		if($add)
		{
			$parent_id = $this->input->post('id');
		}
		else
		{
			$id = $this->input->post('id');
			
			$res_menu = $this->def_model->get_menu($mode, array('where' => array('a.id' => $id)))->row_array();
			
			$parent_id = $res_menu['parent_id'];
			
			$res_menu_funtion = $this->def_model->get_list(array('table' => $mode.'_menu_function', 'where' => array($mode.'_menu_id' => $id)))->result_array();
		}
		
		$html .= '<form id="form_'.$mode.'_menu" class="form-horizontal" method="post" action="menus/save/'.$mode.'/'.(isset($id) ? $id : "").'" >';
		$html .= '<div class="tabbable tabbable-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#a-1" data-toggle="tab">'.$this->lang->line("global_details").'</a></li>
					<li><a href="#a-2" data-toggle="tab">'.$this->lang->line("global_descriptions").'</a></li>
					<li><a href="#a-3" data-toggle="tab">'.$this->lang->line("global_functions").'</a></li>
				</ul>';
		$html .= '<div class="tab-content">';
		$html .= '<div class="tab-pane active" id="a-1">';
		
		$html .= '<div class="control-group"><label class="control-label">'.$this->lang->line("menus_parent_id").' :</label><div class="controls">'.(! empty($parent_id) ? $parent_id : "-").'</div></div>';
		$html .= '<div class="control-group"><label class="control-label" for="'.$mode.'_menus_name">'.$this->lang->line("global_name").' :</label><div class="controls"><input type="text" name="'.$mode.'_menu_name" id="'.$mode.'_menu_name" class="input-large" value="'.(isset($res_menu['name']) ? $res_menu['name'] : "").'" /></div></div>';
		$html .= '<div class="control-group"><label class="control-label" for="'.$mode.'_menus_link_type">'.$this->lang->line("menus_link_type").' :</label><div class="controls">';
		foreach($res_menu_link_type as $link_type)
		{
			$checked = "";
			if(isset($res_menu['link_type_id']))
			{
				if($res_menu['link_type_id'] == $link_type['id']) $checked = "checked='checked'";
			}
			else
			{
				if($link_type['id'] == 1) $checked = "checked='checked'";
			}
			$html .= '<input type="radio" name="'.$mode.'_menu_link_type" id="'.$mode.'_menu_link_type" value="'.$link_type['id'].'" '.$checked.' />'.$link_type['type'].'&nbsp; &nbsp;';
		}
		$html .=	'</div></div>';
		$html .= '<div class="control-group"><label class="control-label">'.$this->lang->line("menus_location").' :</label><div class="controls"><input type="text" name="'.$mode.'_menu_link_location" id="'.$mode.'_menu_link_location"class="input-large" value="'.(isset($res_menu['location']) ? $res_menu['location'] : "").'" /></div></div>';
		$html .= '<div class="control-group"><label class="control-label" for="'.$mode.'_menu_link_target">'.$this->lang->line("menus_link_target").' :</label><div class="controls"><select name="'.$mode.'_menu_link_target" class="uniformselect">';
		foreach($res_menu_link_target as $link_target)
		{
			$selected = "";
			if(isset($res_menu['link_target_id']))
			{
				if($res_menu['link_target_id'] == $link_target['id']) $selected = "selected='selected'";
			}
			else
			{
				if($link_target['id'] == 1) $selected = "selected='selected'";
			}
		
			$html .= '<option value="'.$link_target['id'].'" '.$selected.'>'.$link_target['target'].'</option>';
		}
			
		$html .= '</select></div></div>';
		$html .= '<div class="control-group"><label class="control-label">'.$this->lang->line("menus_cls").' :</label><div class="controls"><input type="text" name="'.$mode.'_menu_link_cls" class="input-large" value="'.(isset($res_menu['cls']) ? $res_menu['cls'] : "").'" /></div></div>';
		
		$html .= '<input type="hidden" name="'.$mode.'_menu_parent_id" value="'.$parent_id.'" />';
		$html .= '</div>';
		
		$html .= '<div class="tab-pane" id="a-2">';
		$html .= '<div class="tabbable tabbable-custom">';
		$html .= '<ul class="nav nav-tabs">';
		$i = 0;
		foreach($this->header['languages'] as $lang)
		{
			$li_cls = ($i == 0) ? 'active' : '';
			
			$html .= '<li class="'.$li_cls.'"><a href="#b-'.$lang['id'].'" data-toggle="tab">'.ucwords($lang['name']).'</a></li>';	
			$i++;
		}
		$html .= '</ul>';
		$i = 0;
		
		$html .= '<div class="tab-content">';
		foreach($this->header['languages'] as $lang)
		{
			if(isset($res_menu['description']))
			{
				$res_lang_desc = $this->def_model->get_list(array('table' => $mode.'_menu_description', 'where' => array($mode.'_menu_id' => $res_menu['id'], $mode.'_lang_id' => $lang['id'])))->row_array();
				$description = isset($res_lang_desc['description']) ? $res_lang_desc['description'] : '';
			}
			
			$li_cls = ($i == 0) ? 'active' : '';

			$html .= '<div class="tab-pane '.$li_cls.'" id="b-'.$lang['id'].'"><div class="control-group"><label class="control-label" for="'.$mode.'_menu_lang_desc_'.$lang['id'].'">'.$this->lang->line("global_desc").' :</label><div class="controls"><input type="text" name="'.$mode.'_menu_lang_desc_'.$lang['id'].'" id="'.$mode.'_menu_lang_desc_'.$lang['id'].'" class="input-large" value="'.(isset($description) ? $description : "").'" /></div></div></div>';
			$i++;
		}
		
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div class="tab-pane" id="a-3">';
		
		$functions = '';
		if(isset($res_menu_funtion))
		{
			$f = 1;
			foreach($res_menu_funtion as $function)
			{
				$functions .= $function['name'];
				if($f <= count($res_menu_funtion)-1)
				{
					$functions .= ',';
				}
				$f++;
			}
		}
		 
		$html .= '<input name="'.$mode.'_function" id="'.$mode.'_function" class="m-wra tags" value="'.$functions.'" />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<p align="center">
					<button class="btn blue"><i class="icon-ok"></i>'.$this->lang->line("global_save").'</button>
					<button onclick="ceki.fnRefresh();return false;"  class="btn btn-primary">'.$this->lang->line("global_cancel").'</button>
				</p>';
		$html .= '</form>';
		
		echo $this->output->status_callback('json_success', $html);
	}
	
	function save($mode,$id = '')
	{
		$this->form_validation->set_rules($mode.'_menu_name', $this->lang->line("global_name"), 'required');
		$this->form_validation->set_rules($mode.'_menu_link_type', $this->lang->line("menu_link_type"), 'required');
		$this->form_validation->set_rules($mode.'_menu_link_target', $this->lang->line("menu_link_target"), 'required');
		$this->form_validation->set_rules($mode.'_menu_link_location', $this->lang->line("menu_link_location"), 'required');
		
		foreach($this->header['languages'] as $lang)
		{
			$this->form_validation->set_rules($mode.'_menu_lang_desc_'.$lang['id'], $this->lang->line("global_desc"), 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('error_alert', validation_errors());
		}
		else
		{
			$data = array(
				'name' => $this->input->post($mode.'_menu_name'),
				'link_type_id' => $this->input->post($mode.'_menu_link_type'),
				'link_target_id' => $this->input->post($mode.'_menu_link_target'),
				'location' => $this->input->post($mode.'_menu_link_location'),
				'cls' => $this->input->post($mode.'_menu_link_cls')
			);
			
			$parent_id = $this->input->post($mode.'_menu_parent_id');
			if(! empty($parent_id))
			{
				$parent = $this->def_model->get_menu($mode, array('where' => array('a.id' => $parent_id)))->row_array();
				$data['parent_id'] = $parent_id;
				$data['level'] = $parent['level'] + 1;
				
				$path_arr = json_decode($parent['path']);
				$prm_max['where']['parent_id'] = $parent_id;
			}
			else
			{
				$data['level'] = 1;
				
				$path_arr = array((string)0);
			}
			
			$prm_max['where']['level'] = $data['level'];
			$prm_max = array(
				'table' => $mode.'_menu', 
				'select' => 'order_number', 
				'where' => $prm_max['where']
			);
			$res_max = $this->def_model->get_max($prm_max);
			
			if(!$id) // add
			{
				$data['order_number'] = $res_max['order_number'] + 1;
				$data['created_datetime'] = $this->get_date_time();
				$data['created_by'] = $this->get_user_name();
				
				$param_insert = array(
					'table' => $mode.'_menu',
					'data' => $data
				);
				
				$this->def_model->insert($param_insert);
				$id = $this->db->insert_id();
				
				array_push($path_arr,(string)$id);
				
				$param_update = array(
					'table' => $mode.'_menu',
					'data'  => array('path' => json_encode($path_arr)),
					'where' => array('id' => $id)
				);
				
				if($this->def_model->update($param_update))
				{
					foreach($this->header['languages'] as $lang)
					{
						$param_insert = array(
							'table' => $mode.'_menu_description',
							'data'  => array(
								$mode.'_menu_id' => $id,
								$mode.'_lang_id' => $lang['id'],
								'description' => $this->input->post($mode.'_menu_lang_desc_'.$lang['id'])
							)
						);
						
						$this->def_model->insert($param_insert);
					}
				}
				
				$this->session->set_flashdata('success_alert', $this->lang->line("global_success_add"));
			}
			else
			{
				array_push($path_arr,(string)$id);
				
				$data['path'] = json_encode($path_arr);
				$data['modified_datetime'] = $this->get_date_time();
				$data['modified_by'] = $this->get_user_name();
				
				$param_update = array(
					'table' => $mode.'_menu',
					'data'  => $data,
					'where' => array('id' => $id)
				);
				
				if($this->def_model->update($param_update))
				{
					foreach($this->header['languages'] as $lang)
					{
						$param = array();
						$param['table'] = $mode.'_menu_description';
						$param['where'] = array($mode.'_menu_id' => $id, $mode.'_lang_id' => $lang['id']);
						$check_data = $this->def_model->count_rows($param);
						
						$param['data'] = array(
							$mode.'_menu_id' => $id,
							$mode.'_lang_id' => $lang['id'],
							'description' => $this->input->post($mode.'_menu_lang_desc_'.$lang['id'])
						);
						
						if($check_data['CNT'] > 0)
						{
							$this->def_model->update($param);
						}
						else
						{
							unset($param['where']);
							$this->def_model->insert($param);
						}
						/* $param_update = array();
						$param_update = array(
							'table' => $mode.'_menu_description',
							'data'  => array(
								$mode.'_menu_id' => $id,
								$mode.'_lang_id' => $lang['id'],
								'description' => $this->input->post($mode.'_menu_lang_desc_'.$lang['id']),
							),
							'where' => array($mode.'_menu_id' => $id, $mode.'_lang_id' => $lang['id'])
						);
						
						$this->def_model->update($param_update); */
					}
				}
				
				$this->session->set_flashdata('success_alert', $this->lang->line("global_success_edit"));
			}
			
			$del = $this->def_model->delete(array('table' => $mode.'_menu_function', 'where' => array($mode.'_menu_id' => $id)));
			
			if($this->input->post($mode.'_function'))
			{
				$explode_function = explode(',',$this->input->post($mode.'_function'));
				
				$data_function = array();
				foreach($explode_function as $function)
				{
					$data_function[strtolower($function)] = $id; 
				}
					
				
				$data_batch = array();
				foreach($data_function as $key => $val)
				{
					$data_batch[] = array($mode.'_menu_id' => $val, 'name' => $key);
				}
				
				
				
				if(! empty($data_batch))
				{
					if($del)
					{
						$param_insert_batch = array(
							'table' => $mode.'_menu_function',
							'data'  => $data_batch
						);
						
						$this->def_model->insert_batch($param_insert_batch);
					}
				}
			}
			else
			{
				$del = $this->def_model->delete(array('table' => $mode.'_menu_function', 'where' => array($mode.'_menu_id' => $id)));
			}
		}
		
		redirect(app_backend_url('managements/menus'));
	}
}