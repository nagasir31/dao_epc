<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->check_module();
	}
	
	function index()
	{
		$data = array();
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-switch/bootstrap-switch.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-switch/bootstrap-switch.min.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.cb.serialize.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/tinymce/jquery.tinymce.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-fileupload/bootstrap-fileupload.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-fileupload/bootstrap-fileupload.js').'"></script>';
		
		$this->load->view('global/header_view',$this->header);
		$this->load->view('managements/'.strtolower($this->class).'_view', $data);
		$this->load->view('global/footer_view');
	}
	
	function save()
	{		
		$this->form_validation->set_rules('app_name', $this->lang->line("site_app_name"), 'required|min_length[5]');
		$this->form_validation->set_rules('app_author', $this->lang->line("site_app_author"), 'required|min_length[5]');
		$this->form_validation->set_rules('company_address', $this->lang->line("global_address"), 'required|min_length[5]');
		$this->form_validation->set_rules('app_admin_email', $this->lang->line("site_app_admin_email"), 'required|valid_email');
		
		$this->form_validation->set_message('required', $this->lang->line('global_error_required_field'));
		$this->form_validation->set_message('min_length', $this->lang->line('global_error_minlength_field'));
		$this->form_validation->set_message('email', $this->lang->line('global_email_invalid'));
		
		
		if ($this->form_validation->run() == FALSE)
		{
			if($this->input->post('is_ajax'))
			{
				echo $this->output->status_callback('json_unsuccess',validation_errors());	
			}
			else
			{
				$this->session->set_flashdata('error_alert', validation_errors());
			}
		}
		else
		{
			$data = array();
			$id = '';
			foreach($this->input->post() as $key => $val)
			{
				if($key == 'app_logo')
				{
					if(! empty($val))
					{
						$data[$key] = basename($val);
					}
				}
				else
				{
					if($key != 'is_ajax')
					{
						if($val == "true") $val = 1;
						elseif($val == "false") $val = 0;
						$data[$key] = $val;
					}
				}
			}
			
			$prm = array(
				'table' => 'sys_config',
				'input' => $data,
				'column_1' => 'keyword',
				'column_2' => 'value'
			);
			
			if($this->def_model->paralel_update($prm))
			{
				if($this->input->post('is_ajax'))
				{
					echo $this->output->status_callback('json_success');	
				}
				else
				{
					$this->session->set_flashdata('success_alert', $this->lang->line("global_success_update"));
				}
			}
			else
			{
				if($this->input->post('is_ajax'))
				{
					echo $this->output->status_callback('json_unsuccess');
				}
				else
				{
					$this->session->set_flashdata('error_alert', $this->lang->line('global_error'));
				}
			}
		}
		
		if(! $this->input->post('is_ajax'))
		{
			redirect(app_backend_url('managements/site'));
		}
	}
}