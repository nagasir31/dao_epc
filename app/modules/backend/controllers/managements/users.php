<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->check_module();
		$this->lang->load('employee', $this->get_lang());
		$this->lang->load('users', $this->get_lang());
	}
	
	function index()
	{
		$data = array();
		
		$data['res_group_child'] = $this->def_model->get_list(array('table' => 'sys_group_child'))->result_array();
		$data['ref_proyek'] = $this->def_model->get_list(array('table' => 'ref_proyek', 'where' => array('ref_cabang_id' => 78)))->result_array();
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/chosen-bootstrap/chosen.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/chosen-bootstrap/chosen.jquery.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-fileupload/bootstrap-fileupload.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-fileupload/bootstrap-fileupload.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
		
		$this->load->view('global/header_view',$this->header);
		$this->load->view('managements/'.strtolower($this->class).'_view', $data);
		$this->load->view('global/footer_view');
	}
	
	function get_list()
	{
		$prm['table']   = 'sys_user';
		$prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
		$prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;
		
		if($this->input->post('iSortCol_0'))
		{
			$sort_idx = $_POST['iSortCol_0'];
			if($_POST['bSortable_'.$sort_idx] == "true")
			{
				$prm['sort'] = $this->_get_select_name($_POST['mDataProp_'.$sort_idx]);
			}
			else
			{
				$prm['sort'] = 'sys_user.id';
			}
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC"; 
		}
		
		if($this->input->post('sSearch'))
		{
			$prm['search'] = $this->input->post('sSearch');
			
			if(strtolower($prm['search']) == 'no' || strtolower($prm['search']) == 'yes')
			{
				$prm['search'] = strtolower($prm['search']) == 'yes' ? '1' : '0';
			}
			
			for($i = 0; $i < $_POST['iColumns']; $i++)
			{
				if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
				{
					$prm['fields'][] =  $this->_get_select_name($_POST['mDataProp_'.$i]);
				}
			}
		}
		
		for($i = 0; $i < $_POST['iColumns']; $i++)
		{
			if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
			{
				if($this->input->post('sSearch_'.$i))
				{
					if($this->_get_select_name($_POST['mDataProp_'.$i]) == 'sys_user.is_active')
					{
						if(strtolower($this->input->post('sSearch_'.$i)) == 'no' || strtolower($this->input->post('sSearch_'.$i)) == 'yes')
						{
							$is_active = strtolower($this->input->post('sSearch_'.$i)) == 'yes' ? 1 : 0;
							$prm['fields_2'][$this->_get_select_name($_POST['mDataProp_'.$i])] = $is_active;
						}
					}
					else
					{
						$prm['fields_2'][$this->_get_select_name($_POST['mDataProp_'.$i])] = $this->input->post('sSearch_'.$i);
					}
				}
			}
		}
		
		$prm['select'] = 'sys_user.id, sys_user.name, sys_user.is_active, sys_user.proyeks, sys_user.signature_path,
		sys_user.sys_group_child_id, sys_group_child.description group_name,
		employee.name employee_name, employee.nrp employee_nrp, employee.divisi employee_divisi, ref_jabatan.glosarry employee_jabatan, ref_cabang.name employee_cabang,employee.ref_proyek_id employee_ref_proyek_id, employee.picture, employee.email employee_email';
		
		$prm['join'][] = array(
						'table' => 'sys_group_child',
						'on'    => 'sys_group_child.id = sys_user.sys_group_child_id',
						'type'  => 'left'
					);
		$prm['join'][] = array(
						'table' => 'employee',
						'on'    => 'employee.id = sys_user.employee_id',
						'type'  => 'left'
					);
		$prm['join'][] = array(
						'table' => 'ref_jabatan',
						'on'    => 'ref_jabatan.id = employee.ref_jabatan_id',
						'type'  => 'left'
					);
		$prm['join'][] = array(
						'table' => 'ref_cabang',
						'on'    => 'ref_cabang.id = employee.ref_cabang_id',
						'type'  => 'left'
					);
		
		$prm['where']['employee.divisi'] = "'EPC'";
		$res_cnt  = $this->def_model->get_data($prm,TRUE)->row_array();
		$res_cnt['cnt'] = isset($res_cnt['cnt']) ? $res_cnt['cnt'] : 0;
		$prm['cnt'] = $res_cnt['cnt'];
		$res_data = $this->def_model->get_data($prm)->result_array(); //echo $this->db->last_query();
		
		$i = $prm['start'] + 1;
        $arr = array();
        foreach($res_data as $data) 
		{
			$data['DT_RowId'] = $data['id'];
			$data['rnum'] = $i;
			$data['cbox'] = '<p class="center"><input type="checkbox" value="'.$data['id'].'" /></p>';
			
			$user_photo = app_asset_url('backend/img/no-photo.jpg');
			if(! empty($data['picture']))
			{
				$user_photo = $this->get_pic_url().$data['picture'];
			}
			
			$photo = '<div style="width: 150px; height: auto;" class="thumbnail">
				<img id="photo_thumb" alt="'.$this->lang->line('global_photo').'" src="'.$user_photo.'">
			</div>';
			$data['names'] = $data['name'];
			$data['name'] = "<span style='color:#35AA47' class='popovers' data-trigger='hover' data-placement='right' data-original-title='".$this->lang->line('global_photo')."' data-html='true' data-content='".$photo."' >".char_limiter($data['name'],15)."</span>";
			
			$data['employee_name'] = $this->convert_str_upper($data['employee_name']);
			
			if($data['is_active'])
			{
				$icon_cls = 'icon-ok';
			}
			else
			{
				$icon_cls = 'icon-remove';
			}
			
			$data['proyeks'] = json_decode($data['proyeks']);
			
			if(empty($data['employee_ref_proyek_id']))
			{
				$data['employee_ref_proyek_id'] = "-";
			}
			else
			{
				if(($data['employee_ref_proyek_id']) == '1')
				{
					$data['employee_ref_proyek_id'] = "Office";
				}
				elseif(($data['employee_ref_proyek_id']) == '2')
				{
					if(! empty($data['proyeks']))
					{
						$data['employee_ref_proyek_id'] = $data['proyeks'];
					}
					else
					{
						$data['employee_ref_proyek_id'] = "Proyek";
					}
					
				}
			}
			
			$data['group_name'] = !empty($data['group_name']) ? $data['group_name'] : '-';
			$data['employee_email'] = "<span title='".$data['employee_email']."'>".char_limiter($data['employee_email'],10)."</span>";
			
			$data['is_actives'] = $data['is_active'];
			$data['is_active'] = '<p class="center"><span class="'.$icon_cls.'"></span></p>';
			
            $arr[] = $data;
            $i++;
        }

        echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
        unset($arr);
	}
	
	private function _get_select_name($val)
	{
		if($val == 'name') $r_val = 'sys_user.name';
		elseif($val == 'group_name') $r_val = 'sys_group_child.description';
		elseif($val == 'employee_name') $r_val = 'employee.name';
		elseif($val == 'employee_nrp') $r_val = 'employee.nrp';
		elseif($val == 'employee_divisi') $r_val = 'employee.divisi';
		elseif($val == 'employee_jabatan') $r_val = 'ref_jabatan.glosarry';
		elseif($val == 'employee_cabang') $r_val = 'ref_cabang.name';
		elseif($val == 'employee_ref_proyek_id') $r_val = 'employee.ref_proyek_id';
		elseif($val == 'employee_email') $r_val = 'employee.email';
		
		else $r_val = 'sys_user.'.$val;
		
		return $r_val;
	}
	
	function delete()
	{
		if($this->input->post('ids'))
		{
			$list = implode(",", $this->input->post('ids'));
				
			$prm_delete = array(
				'table' => 'sys_user',
				'where' => "id in(".$list.") and is_deletable = '1'"
			);
			
			if($this->def_model->delete($prm_delete))
			{
				echo $this->output->status_callback('json_success');	
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
	}
	
	function _username_check()
	{
		$where['name'] = $_POST['name'];
        if(isset($_POST['id']))
		{
		  $where['id !='] = $_POST['id'];
		}
		
		$prm_chk['where'] = $where;
		$prm_chk['table'] = 'sys_user';
		
		if(! $this->def_model->is_unique($prm_chk))
		{
			$this->form_validation->set_message('_username_check', $this->lang->line('global_error_unique_field'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function _email_check()
	{
		$where['email'] = $_POST['email'];
        if(isset($_POST['id']))
		{
		  $where['id !='] = $_POST['id'];
		}
		
		$prm_chk['where'] = $where;
		$prm_chk['table'] = 'sys_user';
		
		if(! $this->def_model->is_unique($prm_chk))
		{
			$this->form_validation->set_message('_email_check', $this->lang->line('global_error_unique_field'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function _password_old_check()
	{
		if(isset($_POST['password_old']))
		{
			$where['password'] = md5($_POST['password_old']);
			if(isset($_POST['id']))
			{
			  $where['id'] = $_POST['id'];
			}
			
			$prm_chk['where'] = $where;
			$prm_chk['table'] = 'sys_user';
			
			$res = $this->def_model->get_one($prm_chk)->row_array();
			
			if(empty($res))
			{
				$this->form_validation->set_message('_password_old_check', $this->lang->line('users_password_old_wrong'));
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			$this->form_validation->set_message('_password_old_check', $this->lang->line('users_password_old_required'));
			return FALSE;
		}
	}
	
	function save()
	{		
		$this->form_validation->set_rules('is_active', $this->lang->line("users_active"), 'required');
		
		if(! $this->input->post('id'))
		{
			$this->form_validation->set_rules('name', $this->lang->line("users_name"), 'required|min_length[5]|max_length[12]|callback__username_check');
			$this->form_validation->set_rules('password', $this->lang->line("users_password"), 'required');
			$this->form_validation->set_rules('password_conf', $this->lang->line("users_password_conf"), 'required|matches[password]');
			
			$this->form_validation->set_message('min_length', $this->lang->line('global_error_minlength_field'));
		}
		else
		{
			if($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line("users_password"), 'required');
				$this->form_validation->set_rules('password_conf', $this->lang->line("users_password_conf"), 'required|matches[password]');
				
				if($this->input->post('id'))
				{
					$this->form_validation->set_rules('password_old', $this->lang->line("users_password_old"), 'callback__password_old_check');
				}
			}
		}
		
		$this->form_validation->set_message('required', $this->lang->line('global_error_required_field'));
		$this->form_validation->set_message('matches', $this->lang->line('users_password_equalto'));
		
		
		if ($this->form_validation->run() == FALSE)
		{
			if($this->input->post('is_ajax'))
			{
				echo $this->output->status_callback('json_unsuccess',validation_errors());	
			}
			else
			{
				$this->session->set_flashdata('error_alert', validation_errors());
			}
		}
		else
		{
			$data = array();
			$id = '';
			$proyeks = array();
			foreach($this->input->post() as $key => $val)
			{
				if($key == 'id')
				{
					$id = $val;
				}
				elseif($key == 'password')
				{
					if(! empty($val))
					{
						$data[$key] = md5($val);
					}
				}
				elseif($key == 'picture')
				{
					if(! empty($val))
					{
						$this->set_user_picture($val);
						$data[$key] = $val;
					}
				}
				elseif($key == 'proyeks')
				{
					$data[$key] = json_encode($val);
					$proyeks = json_encode($val);
				}
				else
				{
					if($key != 'password_conf' && $key != 'password_old' && $key != 'is_ajax' && $key != 'signature')
					{
						if($key == 'sys_group_child_id' && empty($val))
						{
							$val = 0;
						}
						
						$data[$key] = strtolower($val);
					}
				}
			}
			
			if(empty($proyeks))
			{
				$data['proyeks'] = "";
			}
		
			
			if(!$id) // add
			{
				$data['created_datetime'] = $this->get_date_time();
				$data['created_by'] = $this->get_user_name();
				
				$prm = array(
					'table' => 'sys_user',
					'data' => $data
				);
				
				$res = $this->def_model->insert($prm);
				$id = $this->db->insert_id();
			}
			else
			{
				$data['modified_datetime'] = $this->get_date_time();
				$data['modified_by'] = $this->get_user_name();
				
				$prm = array(
					'table' => 'sys_user',
					'data'  => $data,
					'where' => array('id' => $id)
				);
				
				$res = $this->def_model->update($prm);
			}
			
			if($res)
			{
				$dir = 'signatures/'.md5($id);
				$path = ABSOLUTE_PATH.'asset/'.$dir;
				
				$this->create_dir($path);
				
				$config['upload_path'] 	 = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['overwrite']	 = TRUE;
				$config['remove_spaces'] = TRUE;
				$config['max_size']      = '5000';
				$config['file_name'] = 'signature_'.md5($id);
				
				$this->load->library('upload');
				
				$this->upload->initialize($config);
				
				if($this->upload->do_upload('signature'))
				{
					$data_upload = $this->upload->data();
					$full_path = $dir.'/'.$data_upload['file_name'];
					
					$prm = array(
						'table' => 'sys_user',
						'data'  => array('signature_path' => $full_path),
						'where' => array('id' => $id)
					);
				
					$this->def_model->update($prm);
				}
				
				if($this->input->post('is_ajax'))
				{
					echo $this->output->status_callback('json_success');	
				}
				else
				{
					$this->session->set_flashdata('success_alert', $this->lang->line("global_success_update"));
				}
			}
			else
			{
				if($this->input->post('is_ajax'))
				{
					echo $this->output->status_callback('json_unsuccess',$this->lang->line('global_error'));
				}
				else
				{
					$this->session->set_flashdata('error_alert', $this->lang->line('global_error'));
				}
			}
			
		}
		
		if(! $this->input->post('is_ajax'))
		{
			redirect(app_backend_url('profile'));
		}
	}
}