<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My404 extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('css/pages/error.css').'" />';
		$this->load->view('global/header_view',$this->header);
		$this->load->view('global/my404_view');
		$this->load->view('global/footer_view');
	}
}