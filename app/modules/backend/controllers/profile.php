<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->lang->load('users', $this->get_lang());
	}
	
	function index()
	{
		$this->session->set_userdata($this->session_key('active_breadcrumb_lang'),"<li>".$this->lang->line('global_view_profile')."</li>");
		
		$lang = $this->get_lang();
		$this->lang->load('users', $lang);	
		$this->lang->load('employee', $lang);	
		
		$prm['where'] = array('a.id' => $this->get_user_id());
		$data['res_user_employee'] = $this->def_model->get_user_employee($prm)->row_array();
		
		$picture = $this->session->userdata(APPLICATION.'_picture');
		if(preg_match("/avatar.png/i", $picture))
		{
			$picture = app_asset_url('backend/img/no-photo.jpg');
		}
		$data['picture'] = $picture;
		//echo $this->db->last_query();die();
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.pwstrength.bootstrap/src/pwstrength.js').'"></script>';
		
		$this->load->view('global/header_view',$this->header);
		$this->load->view('profile_view', $data);
		$this->load->view('global/footer_view');
	}

}