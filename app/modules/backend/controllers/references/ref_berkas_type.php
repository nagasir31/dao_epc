<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ref_berkas_type extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->check_module();
		$this->table = $this->class;
	}
	
	function index()
	{
		$data = array();
		$data['ref_vendor_type'] = $this->def_model->get_list(array('table' => 'ref_vendor_type'))->result_array();
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/chosen-bootstrap/chosen.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/chosen-bootstrap/chosen.jquery.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
		
		$this->load->view('global/header_view',$this->header);
		$this->load->view('references/'.strtolower($this->class).'_view', $data);
		$this->load->view('global/footer_view');
	}
	
	function get_list()
	{
		$prm['table']   = $this->table;
		$prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
		$prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;
		
		if($this->input->post('iSortCol_0'))
		{
			$sort_idx = $_POST['iSortCol_0'];
			if($_POST['bSortable_'.$sort_idx] == "true")
			{
				$prm['sort'] = $this->_get_select_name($_POST['mDataProp_'.$sort_idx]);
			}
			else
			{
				$prm['sort'] = 'id';
			}
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC"; 
		}
		else
		{
			$prm['sort'] = 'id';
			$prm['dir']  = 'DESC';
		}
		
		if($this->input->post('sSearch'))
		{
			$prm['search'] = $this->input->post('sSearch');
			
			for($i = 0; $i < $_POST['iColumns']; $i++)
			{
				if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true" )
				{
					$prm['fields'][] = $this->_get_select_name($_POST['mDataProp_'.$i]);
				}
			}
		}
		
		$res_data = $this->def_model->get_data($prm)->result_array(); //echo $this->db->last_query();die();
		$res_cnt  = $this->def_model->get_data($prm,TRUE)->row_array();
		$res_cnt['cnt'] = isset($res_cnt['cnt']) ? $res_cnt['cnt'] : 0;
		
		$i = $prm['start'] + 1;
        $arr = array();
        foreach($res_data as $data) 
		{
			$data['DT_RowId'] = $data['id'];
			$data['rnum'] = $i;
			$data['cbox'] = '<p class="center"><input type="checkbox" value="'.$data['id'].'" /></p>';
			
			$res_berkas_vendor_type = $this->berkas_m->get_berkas_vendor_type(array('where' => array('a.ref_berkas_type_id' => $data['id'])))->result_array();
			//$data['res_berkas_vendor_type'] = json_decode($res_berkas_vendor_type);
			
			$vendor_type_name = $vendor_type_id = array();
			foreach($res_berkas_vendor_type as $dt)
			{
				$vendor_type_name[] = $dt['vendor_type_name'];
				$vendor_type_id[] = $dt['vendor_type_id'];
			}
			
			$data['vendor_type_name'] = implode(", ",$vendor_type_name);
			$data['vendor_type_id'] = $vendor_type_id;
			
            $arr[] = $data;
            $i++;
        }

        echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
        unset($arr);
	}
	
	private function _get_select_name($val)
	{
		return $val;
	}
	
	function delete()
	{
		if($this->input->post('ids'))
		{
			$list_id = implode(",", $this->input->post('ids'));
			
			if($this->berkas_m->delete_berkas_type($list_id))
			{
				echo $this->output->status_callback('json_success');	
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
	}
	
	function save()
	{
		if($this->input->post('name'))
		{
			$where['name'] = $_POST['name'];
			
			if(isset($_POST['id']))
			{
			  $where['id !='] = $_POST['id'];
			}
			
			$prm_chk['where'] = $where;
			$prm_chk['table'] = $this->table;
			
			
			if($this->def_model->is_unique($prm_chk))
			{
				$id = '';
				$vendor_types = array();
				foreach($this->input->post() as $key => $val)
				{
					if($key == 'id')
					{
						$id = $val;
					}
					elseif($key == 'vendor_types')
					{
						$vendor_types = $val;
					}
					else
					{
						$data[$key] = strtoupper($val);
					}
				}
				
				if(!$id)
				{	
					$data['created_datetime'] = $this->get_date_time();
					$data['created_by'] = $this->get_user_name();
				
					$prm = array(
						'table' => $this->table,
						'data' => $data
					);
					
					$res = $this->def_model->insert($prm);
					$id = $this->db->insert_id();
				}
				else
				{
					$data['modified_datetime'] = $this->get_date_time();
					$data['modified_by'] = $this->get_user_name();
				
					$prm = array(
						'table' => $this->table,
						'data'  => $data,
						'where' => array('id' => $id)
					);
					
					$res = $this->def_model->update($prm);
				}
				
				if($res)
				{
					if(! empty($vendor_types))
					{
						$prm_delete = array(
							'table' => 'ref_berkas_vendor_type',
							'where' => array('ref_berkas_type_id' => $id)
						);
				
						$del = $this->def_model->delete($prm_delete);
						
						$data_insert = array();
						foreach($vendor_types as $dt)
						{
							$data_insert[] = array(
								'ref_berkas_type_id' => $id,
								'ref_vendor_type_id' => $dt
							);
						}
					
						$prm_insert_batch = array(
							'table' => 'ref_berkas_vendor_type',
							'data'  => $data_insert
						);
						
						$this->def_model->insert_batch($prm_insert_batch);
					}
					
					echo $this->output->status_callback('json_success');	
				}
				else
				{
					echo $this->output->status_callback('json_unsuccess');
				}
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess',sprintf($this->lang->line('global_error_unique_field'), $this->lang->line('global_name')));
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess',$this->lang->line('global_error_form'));
		}
	}
}