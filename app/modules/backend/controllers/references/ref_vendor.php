<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ref_vendor extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->table = $this->class;
	}
	
	function index()
	{
		$this->check_module();
		$data = array();
		$data['ref_vendor_type'] = $this->def_model->get_list(array('table' => 'ref_vendor_type'))->result_array();
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/chosen-bootstrap/chosen.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/chosen-bootstrap/chosen.jquery.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery-inputmask/jquery.inputmask.bundle.min.js').'"></script>';
		
		$this->load->view('global/header_view',$this->header);
		$this->load->view('references/'.strtolower($this->class).'_view', $data);
		$this->load->view('global/footer_view');
	}
	
	function get_list()
	{
		$prm['table']   = $this->table;
		$prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
		$prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;
		
		if($this->input->post('iSortCol_0'))
		{
			$sort_idx = $_POST['iSortCol_0'];
			if($_POST['bSortable_'.$sort_idx] == "true")
			{
				$prm['sort'] = $this->_get_select_name($_POST['mDataProp_'.$sort_idx]);
			}
			else
			{
				$prm['sort'] = "{$this->table}.id";
			}
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC"; 
		}
		
		if($this->input->post('sSearch'))
		{
			$prm['search'] = $this->input->post('sSearch');
			
			if(strtolower($prm['search']) == 'no' || strtolower($prm['search']) == 'yes')
			{
				$prm['search'] = strtolower($prm['search']) == 'yes' ? '1' : '0';
			}
			
			for($i = 0; $i < $_POST['iColumns']; $i++)
			{
				if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
				{
					$prm['fields'][] =  $this->_get_select_name($_POST['mDataProp_'.$i]);
				}
			}
		}
		
		for($i = 0; $i < $_POST['iColumns']; $i++)
		{
			if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
			{
				if($this->input->post('sSearch_'.$i))
				{
					$prm['fields_2'][$this->_get_select_name($_POST['mDataProp_'.$i])] = $this->input->post('sSearch_'.$i);
				}
			}
		}
		
		$prm['select'] = "{$this->table}.*, ref_vendor_type.name vendor_type";
		
		$prm['join'][] = array(
						'table' => "ref_vendor_type",
						'on'    => "ref_vendor_type.id = {$this->table}.ref_vendor_type_id",
						'type'  => "left"
					);
		
		$res_cnt  = $this->def_model->get_data($prm,TRUE)->row_array();
		$res_cnt['cnt'] = isset($res_cnt['cnt']) ? $res_cnt['cnt'] : 0;
		$prm['cnt'] = $res_cnt['cnt'];
		$res_data = $this->def_model->get_data($prm)->result_array(); //echo $this->db->last_query();
		
		$i = $prm['start'] + 1;
        $arr = array();
        foreach($res_data as $data) 
		{
			$data['DT_RowId'] = $data['id'];
			$data['rnum'] = $i;
			$data['cbox'] = '<p class="center"><input type="checkbox" value="'.$data['id'].'" /></p>';
			
			$data['bank_accounts'] = $data['bank_account'];
			$data['bank_account'] = char_limiter($data['bank_account'],15);
			
            $arr[] = $data;
            $i++;
        }

        echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
        unset($arr);
	}
	
	private function _get_select_name($val)
	{
		if($val == 'vendor_type') $r_val = 'ref_vendor_type.name';
		else $r_val = "{$this->table}.".$val;
		
		return $r_val;
	}

	private function _get_select_name_new($val)
	{
//		if($val == 'vendor_type') $r_val = 'ref_vendor_type.code';
//		else $r_val = "sap_ref_listvendor.".$val;

		return $val;
	}
	
	function delete()
	{
		if($this->input->post('ids'))
		{
			$list_id = implode(",", $this->input->post('ids'));
			
			$prm_delete = array(
				'table' => $this->table,
				'where' => "id in(".$list_id.")"
			);
			
			if($this->def_model->delete($prm_delete))
			{
				echo $this->output->status_callback('json_success');	
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess');
			}
		}
	}
	
	function save()
	{
		if($this->input->post('name'))
		{
			$where['upper(name)'] = strtoupper($_POST['name']);
			$where['ref_vendor_type_id'] = $_POST['ref_vendor_type_id'];
			
			if(isset($_POST['id']))
			{
			  $where['id !='] = $_POST['id'];
			}
			
			$prm_chk['where'] = $where;
			$prm_chk['table'] = $this->table;
			
			
			if($this->def_model->is_unique($prm_chk))
			{
				$id = '';
				foreach($this->input->post() as $key => $val)
				{
					if($key == 'id')
					{
						$id = $val;
					}
					else
					{
						if(! empty($val))
						{
							if($key == 'name' || $key == 'bank_name' || $key == 'bank_account' || $key == 'city')
							{
								$val = strtoupper($val);
							}
							
							$data[$key] = $val;
						}
					}
				}
				
				if(!$id)
				{	
					$data['created_datetime'] = $this->get_date_time();
					$data['created_by'] = $this->get_user_name();
				
					$prm = array(
						'table' => $this->table,
						'data' => $data
					);
					
					$res = $this->def_model->insert($prm);
				}
				else
				{
					$data['modified_datetime'] = $this->get_date_time();
					$data['modified_by'] = $this->get_user_name();
				
					$prm = array(
						'table' => $this->table,
						'data'  => $data,
						'where' => array('id' => $id)
					);
					
					$res = $this->def_model->update($prm);
				}
				
				if($res)
				{
					echo $this->output->status_callback('json_success');	
				}
				else
				{
					echo $this->output->status_callback('json_unsuccess');
				}
			}
			else
			{
				echo $this->output->status_callback('json_unsuccess',sprintf($this->lang->line('global_error_unique_field'), $this->lang->line('global_name')));
			}
		}
		else
		{
			echo $this->output->status_callback('json_unsuccess',$this->lang->line('global_error_form'));
		}
	}

	function get_list_new()
	{
		if($this->input->post('code')){
			$where['sap_ref_listvendor.ktokk'] = "'".$this->input->post('code')."'";
			$prm['where'] = $where;
		}

		$prm['table']   = 'sap_ref_listvendor';
		$prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
		$prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;

		if($this->input->post('iSortCol_0'))
		{
			$sort_idx = $_POST['iSortCol_0'];
			if($_POST['bSortable_'.$sort_idx] == "true")
			{
				$prm['sort'] = $this->_get_select_name_new($_POST['mDataProp_'.$sort_idx]);
			}
			else
			{
				$prm['sort'] = "sap_ref_listvendor.id";
			}
			$prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC";
		}

		if($this->input->post('sSearch'))
		{
			$prm['search'] = $this->input->post('sSearch');

			if(strtolower($prm['search']) == 'no' || strtolower($prm['search']) == 'yes')
			{
				$prm['search'] = strtolower($prm['search']) == 'yes' ? '1' : '0';
			}

			for($i = 0; $i < $_POST['iColumns']; $i++)
			{
				if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
				{
					$prm['fields'][] =  $this->_get_select_name_new($_POST['mDataProp_'.$i]);
				}
			}
		}

		for($i = 0; $i < $_POST['iColumns']; $i++)
		{
			if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
			{
				if($this->input->post('sSearch_'.$i))
				{
					$prm['fields_2'][$this->_get_select_name_new($_POST['mDataProp_'.$i])] = $this->input->post('sSearch_'.$i);
				}
			}
		}


		$prm['select'] = "sap_ref_listvendor.id as vendor_id, sap_ref_listvendor.partner, sap_ref_listvendor.bukrs, sap_ref_listvendor.name1, sap_ref_listvendor.ktokk, sap_ref_listvendor.stcd1, sap_ref_listvendor.ad_name1, sap_ref_listvendor.smtp_addr, sap_ref_listvendor.city1, sap_ref_listvendor.street, sap_ref_listvendor.post_code1, sap_ref_listvendor.addrnumber, ref_vendor_type.code vendor_type, sap_ref_partnerbanktype.lifnr, sap_ref_partnerbanktype.id, sap_ref_partnerbanktype.banks, sap_ref_partnerbanktype.bankl, sap_ref_partnerbanktype.bankn, sap_ref_partnerbanktype.bvtyp, sap_ref_partnerbanktype.koinh, sap_ref_partnerbanktype.banka, sap_ref_partnerbanktype.brnch, sap_ref_partnerbanktype.ort01";

		$prm['join'][] = array(
			'table' => "ref_vendor_type",
			'on'    => "ref_vendor_type.code = sap_ref_listvendor.ktokk",
			'type'  => "left"
		);

		$prm['join'][] = array(
			'table' => "sap_ref_partnerbanktype",
			'on'    => "sap_ref_partnerbanktype.lifnr = sap_ref_listvendor.partner",
			'type'  => "INNER"
		);

		$res_cnt  = $this->def_model->get_data($prm,TRUE)->row_array();
		$res_cnt['cnt'] = isset($res_cnt['cnt']) ? $res_cnt['cnt'] : 0;
		$prm['cnt'] = $res_cnt['cnt'];
		$res_data = $this->def_model->get_data($prm)->result_array(); //echo $this->db->last_query();

		$i = $prm['start'] + 1;
		$arr = array();
		foreach($res_data as $data)
		{
			$data['DT_RowId'] = $data['id'];
			$data['rnum'] = $i;
			$data['cbox'] = '<p class="center"><input type="checkbox" value="'.$data['id'].'" /></p>';

			$arr[] = $data;
			$i++;
		}

		echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
		unset($arr);
	}
}