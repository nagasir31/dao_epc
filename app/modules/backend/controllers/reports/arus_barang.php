<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arus_barang extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->check_module();

	}

    function index()
    {
        $data = array();

        $this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
        $this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';

        $this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-fileupload/bootstrap-fileupload.css').'" />';
        $this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-fileupload/bootstrap-fileupload.js').'"></script>';

        $this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
        $this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
        $this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
        $this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/dataTablesFixedColumns.min.js').'"></script>';

        $this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/bootstrap-datepicker/css/datepicker.css').'" />';
        $this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js').'"></script>';

        $this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
        $this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.pulsate.min.js').'"></script>';

        $this->header['js'][]  = '<script type="text/javascript">var groupName = "'.$this->get_group_name2().'"</script>';
        $this->header['js'][]  = '<script type="text/javascript">var jabatanGlosarry = "'.$this->get_jabatan_glosarry().'"</script>';

        $this->load->view('global/header_view',$this->header);
        $this->load->view('reports/'.strtolower($this->class), $data);
        $this->load->view('global/footer_view');
    }

    function get_list()
    {
        $prm['start']   = $this->input->post('iDisplayStart') ? $this->input->post('iDisplayStart') : 0;
        $prm['limit']   = $this->input->post('iDisplayLength') ? $this->input->post('iDisplayLength') : 10;

        $prm['table'] = 'arus_barang a';
        if($this->input->post('iSortCol_0'))
        {
            $sort_idx = $_POST['iSortCol_0'];
            if($_POST['bSortable_'.$sort_idx] == "true")
            {
                $prm['sort'] = $this->_get_select_name($_POST['mDataProp_'.$sort_idx]);
            }
            else
            {
                $prm['sort'] = "a.id";
            }
            $prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC";
        }
        else
        {
            $prm['sort'] = "a.id";
            $prm['dir']  = "DESC";
        }

        if($this->input->post('sSearch'))
        {
            $prm['search'] = $this->input->post('sSearch');

            if(strtolower($prm['search']) == 'no' || strtolower($prm['search']) == 'yes')
            {
                $prm['search'] = strtolower($prm['search']) == 'yes' ? '1' : '0';
            }

            for($i = 0; $i < $_POST['iColumns']; $i++)
            {
                if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
                {
                    $prm['fields'][] =  $this->_get_select_name($_POST['mDataProp_'.$i]);
                }
            }
        }

        for($i = 0; $i < $_POST['iColumns']; $i++)
        {
            if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
            {
                if($this->input->post('sSearch_'.$i))
                {
                    $prm['fields_2'][$this->_get_select_name($_POST['mDataProp_'.$i])] = $this->input->post('sSearch_'.$i);
                }
            }
        }

//        if($this->session->userdata($this->session_key('group_parent_id')) != 1){
//            if($this->session->userdata($this->session_key('group_name')) == 'PROYEK'){
//                $prm['where_in'] = array('project_code', $this->session->userdata($this->session_key('proyeks')));
//
//            }else{
//                $prm['where'] = array(
//                    'unit_id' => $this->session->userdata($this->session_key('cabang_id'))
//                );
//            }
//        }

        $this->load->model('arus_barang_model');

        $res_cnt  = $this->arus_barang_model->get_data_research($prm,TRUE)->row_array();
        $res_cnt['cnt'] = isset($res_cnt['cnt']) ? $res_cnt['cnt'] : 0;
        $prm['cnt'] = $res_cnt['cnt'];

        $res_data = $this->arus_barang_model->get_data_research($prm)->result_array();
        $i = $prm['start'] + 1;
        $arr = array();
        foreach($res_data as $data)
        {
            $data['DT_RowId'] = $data['id'];
            $data['rnum'] = $i;
            $data['cbox'] = '<p class="center"><input type="checkbox" value="'.$data['id'].'" /></p>';

            if($data['faktur_pajak_date'])
            {
                $data['faktur_pajak_date'] = $this->date_format_dmY($data['faktur_pajak_date'],'-');
            }

            if($data['surat_jalan_lkp_date'])
            {
                $data['surat_jalan_lkp_date'] = $this->date_format_dmY($data['surat_jalan_lkp_date'],'-');
            }

            if($data['ntpn_date'])
            {
                $data['ntpn_date'] = $this->date_format_dmY($data['ntpn_date'],'-');
            }

            if(! empty($data['dpp_amount']))
            {
                $data['dpp_amount'] =  $this->money_format($data['dpp_amount']);
            }

            if(! empty($data['fp_amount']))
            {
                $data['fp_amount'] =  $this->money_format($data['fp_amount']);
            }


            if($data['spk_po_number'])
            {
                $raw = explode(',', $data['spk_po_number']);
                $dets_data = array();
                foreach ($raw as $dets){
                    $dets_data[]=stripslashes($dets);
                }

                $data['spk_po_number'] = implode(', ', $dets_data);

            }

            if($data['spk_po_date'])
            {
                $raw = explode(',', $data['spk_po_date']);
                $dets_data = array();
                foreach ($raw as $dets){
                    $formatted_date = $this->date_format_dmY($dets,'-');
                    $dets_data[] = $formatted_date;
                }
                $data['spk_po_date'] = implode(', ', $dets_data);
            }

            $arr[] = $data;
            $i++;
        }

        echo '{ "sEcho": '.$_POST['sEcho'].', "iTotalRecords": '.$res_cnt['cnt'].', "iTotalDisplayRecords": '.$res_cnt['cnt'].', "aaData": '.json_encode($arr).' }';
        unset($arr);
    }

    private function _get_select_name($val)
    {

        return $val;

    }

    function extract_berkas(){
        if($this->db->truncate('arus_barang')){
            $prm = array(
                'table' => 'berkas t1',
                'select' => 't1.id as berkas_id',
                'where_string' => 't1.ppn_val > 0 AND t1.is_active = 1',
            );
            $berkases = $this->def_model->get_list($prm)->result_array();
            $berkas_data = array();
            foreach ($berkases as $berkas){
                $berkas['created_by'] = 'superadmin';
                $berkas['created_datetime'] = $this->get_date_time();
                $berkas_data[] = $berkas;
            }

            $prm_insert_batch = array(
                'table' => 'arus_barang',
                'data'  => $berkas_data
            );

            $this->def_model->insert_batch($prm_insert_batch);
        }
    }

    function import(){

        $config['upload_path'] 	 = ABSOLUTE_PATH.'asset/tmp/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite']	 = TRUE;
        $config['remove_spaces'] = TRUE;
        $config['max_size']      = '10000000000';

        $this->load->library('upload');

        $files = array();

        foreach($_FILES as $key => $value)
        {
            if( ! empty($value['name']))
            {

                $config['file_name'] = 'data_ntpn';

                $this->upload->initialize($config);

                if(! $this->upload->do_upload($key))
                {
                    $error_upload = TRUE;

                }
                else
                {
                    ini_set('memory_limit','-1');

                    $file_path = ABSOLUTE_PATH.'asset/tmp/data_ntpn.xlsx';

                    $cnt_top_row = 1;

                    if(! empty($file_path))
                    {
                        //Check File Type
                        $file_type = mime_content_type($file_path);
                        $allowed_type = array('.csv', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel');
                        if(!in_array($file_type, $allowed_type)){
                            $this->print_die( $this->lang->line('global_upload_error'));
                        }

                        $this->load->library('pxl_1_8');

                        $input_file_type = PHPExcel_IOFactory::identify($file_path);
                        $obj_reader = PHPExcel_IOFactory::createReader($input_file_type);
                        $obj_reader->setReadDataOnly(true);
                        $obj_reader->setLoadSheetsOnly(0);
                        $obj_php_excel = $obj_reader->load($file_path);

                        $obj_php_excel->setActiveSheetIndex(0);
                        $obj_worksheet = $obj_php_excel->getActiveSheet();
                        $highest_row = $obj_worksheet->getHighestRow();

                        if($highest_row <= $cnt_top_row)
                        {
                            $this->print_die( $this->lang->line('global_upload_error'));
                        }
                        else
                        {
                            $rs = $cnt_top_row+1;
                            $re = $highest_row;

                            $update_data =  array();
                            $failed_data =  array();

                            for($i = $rs;$i <= $re; $i++)
                            {
                                $data = array();

                                //Faktur Pajak Number
                                $col = "A";
                                $fp_number_col = $obj_worksheet->getCell($col.$i);
                                $fp_number = fixUTF8(trim($fp_number_col->getCalculatedValue()));
                                $data['faktur_pajak_number'] = NULL;
                                if(! empty($fp_number))
                                {
                                    //todo recreate faktur pajak
                                    $fp_number = '0'.$fp_number;
                                    $fp_number = str_replace(array('.', '-'), '', $fp_number);
                                    $fp_number = substr($fp_number,0,3).'.'.substr($fp_number,3,3).'-'.substr($fp_number,6,2).'.'.substr($fp_number,8,8);

                                    $data['faktur_pajak_number'] = $fp_number;

                                }

                                //Nomor NTPN
                                $col = 'B';
                                $ntpn_number_col = $obj_worksheet->getCell($col.$i);
                                $ntpn_number = fixUTF8(trim($ntpn_number_col->getCalculatedValue()));
                                $data['ntpn_number'] = NULL;
                                if(! empty($ntpn_number))
                                {
                                    $data['ntpn_number'] = $ntpn_number;
                                }

                                $col = 'C';
                                $ntpn_date_col = $obj_worksheet->getCell($col.$i);
                                $ntpn_date = $ntpn_date_col->getValue();
                                $data['ntpn_date'] = NULL;
                                if(! empty($ntpn_date))
                                {
                                    $ntpn_date =  substr($ntpn_date,0,8);
                                    if(strlen($ntpn_date) == 8){

                                        $ntpn_day = substr($ntpn_date,6,2);
                                        $ntpn_month = substr($ntpn_date,4,2);
                                        $ntpn_year = substr($ntpn_date,0,4);

                                        $ntpn_date = $ntpn_year.'-'.$ntpn_month.'-'.$ntpn_day;

                                        if($this->check_date_ymd($ntpn_date)){
                                            $data['ntpn_date'] = $ntpn_date;
                                        }else{
                                            $data['ntpn_date'] = NULL;
                                        }
                                    }

                                }

                                if( $data['faktur_pajak_number'] == NULL ){
                                    $failed_data[] = $data;
                                    continue;
                                }

                                if($data){
                                    $find_prm = array(
                                        'table' => 'arus_barang a',
                                        'select' =>  'a.*',
                                        'where' => array(
                                            'b.faktur_pajak_number' => $data['faktur_pajak_number'],
                                        ),
                                        'join'  => array(
                                            array(
                                                'table' => 'berkas b',
                                                'on'    => 'a.berkas_id = b.id AND b.is_active = 1',
                                                'type'  => 'inner'
                                            )
                                        )
                                    );

                                    $exist = $this->def_model->get_one($find_prm);

                                    if ($exist->num_rows() > 0 ){
                                        $data['id'] = $exist->row()->id;
                                        $data['modified_datetime'] = $this->get_date_time();
                                        $data['modified_by'] = $this->get_user_name();
                                        unset($data['faktur_pajak_number']);
                                        $update_data[] = $data;
                                    }
                                }

                            }

                            $processed = FALSE;
                            if( count($update_data) > 0 ){
                                $param = array(
                                    'table' => 'arus_barang',
                                    'data' => $update_data,
                                    'where_key' => 'id'
                                );
                                $processed = $this->def_model->update_batch($param);
                            }

                            if( count($update_data) == 0 ){
                                $message = $this->lang->line('global_data_not_found');
                                echo $this->output->status_callback('json_unsuccess', $message);
                                die();
                            }

                            if($processed){
                                echo $this->output->status_callback('json_success', 'Data Update: '.count($update_data) . ' - Data Gagal: '.count($failed_data));
                            }else{
                                echo $this->output->status_callback('json_unsuccess', ' - Data Gagal: '.count($failed_data));
                            }
                        }
                    }
                    else
                    {
                        echo $this->output->status_callback('json_unsuccess');
                    }
                }

            }
        }
    }

    function export_xls(){
        ini_set('memory_limit','-1');
        $prm['table'] = 'arus_barang a';
        if($this->input->post('iSortCol_0'))
        {
            $sort_idx = $_POST['iSortCol_0'];
            if($_POST['bSortable_'.$sort_idx] == "true")
            {
                $prm['sort'] = $this->_get_select_name($_POST['mDataProp_'.$sort_idx]);
            }
            else
            {
                $prm['sort'] = "a.id";
            }
            $prm['dir']  = isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : "DESC";
        }
        else
        {
            $prm['sort'] = "a.id";
            $prm['dir']  = "DESC";
        }

        if($this->input->post('sSearch'))
        {
            $prm['search'] = $this->input->post('sSearch');

            if(strtolower($prm['search']) == 'no' || strtolower($prm['search']) == 'yes')
            {
                $prm['search'] = strtolower($prm['search']) == 'yes' ? '1' : '0';
            }

            for($i = 0; $i < $_POST['iColumns']; $i++)
            {
                if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
                {
                    $prm['fields'][] =  $this->_get_select_name($_POST['mDataProp_'.$i]);
                }
            }
        }

        for($i = 0; $i < $_POST['iColumns']; $i++)
        {
            if(isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true")
            {
                if($this->input->post('sSearch_'.$i))
                {
                    $prm['fields_2'][$this->_get_select_name($_POST['mDataProp_'.$i])] = $this->input->post('sSearch_'.$i);
                }
            }
        }

//        if($this->session->userdata($this->session_key('group_parent_id')) != 1){
//            if($this->session->userdata($this->session_key('group_name')) == 'PROYEK'){
//                $prm['where_in'] = array('project_code', $this->session->userdata($this->session_key('proyeks')));
//
//            }else{
//                $prm['where'] = array(
//                    'unit_id' => $this->session->userdata($this->session_key('cabang_id'))
//                );
//            }
//        }

//        $prm['select'] = 'a.ntpn_number, a.ntpn_date, b.*, c.name division_name, if(e.sap_profit_center != "", e.name, d.name ) project_name, f.name vendor_type_name';
//        $prm['join'] = array(
//            array(
//                'table' => 'berkas b',
//                'on'    => 'a.berkas_id = b.id AND b.is_active = 1',
//                'type'  => 'inner'
//            ),
//            array(
//                'table' => 'ref_cabang c',
//                'on'    => "c.id = b.unit_id",
//                'type'  => 'inner'
//            ),
//            array(
//                'table' => 'ref_proyek d',
//                'on'    => "b.project_code = d.id",
//                'type'  => 'left'
//            ),
//            array(
//                'table' => 'ref_proyek e',
//                'on'    => "b.project_code = e.sap_profit_center",
//                'type'  => 'left'
//            ),
//            array(
//                'table' => 'ref_vendor_type f',
//                'on'    => "b.vendor_type = f.code",
//                'type'  => 'inner'
//            )
//        );

        $this->load->model('arus_barang_model');

        $res_cnt  = $this->arus_barang_model->get_data_research($prm,TRUE)->row_array();

        $res_cnt['cnt'] = isset($res_cnt['cnt']) ? $res_cnt['cnt'] : 0;
        $prm['cnt'] = $res_cnt['cnt'];

        $res_data = $this->arus_barang_model->get_data_research($prm)->result_array();

        $file_name = "arus_barang_".date("d-m-Y");
        $title = "Data";

        $this->load->library('pxl_1_8');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getDefaultStyle()->applyFromArray(array(
            'fill'	=>array(
                'type'	=> 'solid',
                'border' => 0,
                'color'	=>	array(
                    'rgb' => 'FFFFFF'
                )
            )
        ));

        $objPHPExcel->getProperties()->setTitle('title')->setDescription("description");
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
        $objPHPExcel->getActiveSheet()->setTitle("{$title}");

        //header
        $sc = "A";
        $sr = "1";
        $s_col = $sc;
        $s_row = $sr;

        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('global_no')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(10);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('fiscal year'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(15);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('doc. number'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(30);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Account (Utang)'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(30);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Account (Biaya Fisik)'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(30);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_vendor_type')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_project_code')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_project_name')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Nomor NPWP'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Nama Penjual'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Alamat Penjual'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Nomor Faktur'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Tanggal Faktur'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Masa Pajak'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Tahun Pajak'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('DPP'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('PPN'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Nomor NTPN'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Tgl Bayar NTPN'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Nama Bank PP'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('NO. Rekening PP'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Nilai Giro'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_number')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_barang_jasa_name')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_spk_po_number')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_spk_po_date')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_surat_jalan_lkp_number')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_surat_jalan_lkp_date')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_invoice_number')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_payment_method')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('No. Bukti Bayar TT'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('NO. BUKTI BAYAR PAJAK (NON WAPU)'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Nilai Bayar DPP'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Tanggal Bayar'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('PPH'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_vendor_code')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_pajak_type')));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('No. Rekening Vendor'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Bank Vendor'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('Nama Rekening Vendor'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_row++;

        $rnum = 1;
        foreach($res_data as $dt)
        {

            $d_col = $sc;
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $rnum);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col = 'F';
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['vendor_type_name']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['project_code']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['project_name']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['vendor_npwp']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['vendor_name']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['vendor_address']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['faktur_pajak_number']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $dt['faktur_pajak_date'] = ! empty($dt['faktur_pajak_date']) ? $this->date_format_dmY($dt['faktur_pajak_date'],'/') : '';
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['faktur_pajak_date']);
            $objValidation3 = $objPHPExcel->getActiveSheet()->getCell($d_col.$s_row)->getDataValidation();
            $objValidation3->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation3->setAllowBlank(true);
            $objValidation3->setShowInputMessage(true);
            $objValidation3->setShowDropDown(false);
            $objValidation3->setPromptTitle('Format Tanggal');
            $objValidation3->setPrompt('DD-MM-YYYY');
            $objValidation3->setErrorTitle('Format Format Tanggal Salah');
            $objValidation3->setShowErrorMessage('Format Format Tanggal Salah');
            $objValidation3->setError('Format Format Tanggal Salah');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['pajak_period']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['pajak_year']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['dpp_amount']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getNumberFormat()->setFormatCode('#,##0.00');

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['fp_amount']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getNumberFormat()->setFormatCode('#,##0.00');

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['ntpn_number']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $dt['ntpn_date'] = ! empty($dt['ntpn_date']) ? $this->date_format_dmY($dt['ntpn_date'],'/') : '';
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['ntpn_date']);
            $objValidation3 = $objPHPExcel->getActiveSheet()->getCell($d_col.$s_row)->getDataValidation();
            $objValidation3->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation3->setAllowBlank(true);
            $objValidation3->setShowInputMessage(true);
            $objValidation3->setShowDropDown(false);
            $objValidation3->setPromptTitle('Format Tanggal');
            $objValidation3->setPrompt('DD-MM-YYYY');
            $objValidation3->setErrorTitle('Format Format Tanggal Salah');
            $objValidation3->setShowErrorMessage('Format Format Tanggal Salah');
            $objValidation3->setError('Format Format Tanggal Salah');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $d_col = 'W';
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['berkas_number']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['barang_jasa_name']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, !empty($dt['spk_po_number']) ? $dt['spk_po_number'] : '');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$d_col++;
			$dt['spk_po_date'] = ! empty($dt['spk_po_date']) ? $this->date_format_dmY($dt['spk_po_date'],'/') : '';
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['spk_po_date']);
			$objValidation3 = $objPHPExcel->getActiveSheet()->getCell($d_col.$s_row)->getDataValidation();
			$objValidation3->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
			$objValidation3->setAllowBlank(true);
			$objValidation3->setShowInputMessage(true);
			$objValidation3->setShowDropDown(false);
			$objValidation3->setPromptTitle('Format Tanggal');
			$objValidation3->setPrompt('DD-MM-YYYY');
			$objValidation3->setErrorTitle('Format Format Tanggal Salah');
			$objValidation3->setShowErrorMessage('Format Format Tanggal Salah');
			$objValidation3->setError('Format Format Tanggal Salah');
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			$d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['surat_jalan_lkp_number']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $dt['surat_jalan_lkp_date'] = ! empty($dt['surat_jalan_lkp_date']) ? $this->date_format_dmY($dt['surat_jalan_lkp_date'],'/') : '';
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['surat_jalan_lkp_date']);
            $objValidation3 = $objPHPExcel->getActiveSheet()->getCell($d_col.$s_row)->getDataValidation();
            $objValidation3->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation3->setAllowBlank(true);
            $objValidation3->setShowInputMessage(true);
            $objValidation3->setShowDropDown(false);
            $objValidation3->setPromptTitle('Format Tanggal');
            $objValidation3->setPrompt('DD-MM-YYYY');
            $objValidation3->setErrorTitle('Format Format Tanggal Salah');
            $objValidation3->setShowErrorMessage('Format Format Tanggal Salah');
            $objValidation3->setError('Format Format Tanggal Salah');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['invoice_number']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['payment_method']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col = 'AJ';
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['vendor_code']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['pajak_type']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['vendor_bank_rek']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['vendor_bank_name']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['vendor_bank_account']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$rnum++;
            $s_row++;
            session_write_close();
        }

        $s_row--;

        $objPHPExcel->getActiveSheet()->setAutoFilter( $objPHPExcel->getActiveSheet()
            ->calculateWorksheetDimension());

        #STYLE Header
        $objPHPExcel->getActiveSheet()->getStyle($sc.($sr).":".$s_col.($sr))->applyFromArray(
            array(
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => 'FFFFFF')
                ),
                'fill'	=>array(
                    'type'	=> 'solid',
                    'border' => 1,
                    'color'	=>	array(
                        'rgb' => '4c87b9'
                    ),
                ),
                'alignment'	=> array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle($sc.($sr).":".$s_col.($sr))->getAlignment()->setWrapText(true);

        #STYLE Content
        $objPHPExcel->getActiveSheet()->getStyle($sc.($sr+1).":".$s_col.($s_row))->applyFromArray(
            array(
                'alignment'	=> array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrapText' => true
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$file_name.'.xlsx');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        $this->log_activities(
            array(
                'module' => $this->class,
                'lang_subject' => 'arus_barang',
                'lang_item' => '',
                'lang_action'  => '[global_activity_export]'
            )
        );
        exit;
    }

    function revisi(){
        ini_set('memory_limit','-1');

        $file_path = ABSOLUTE_PATH.'asset/tmp/revisi_arus_barang.xlsx';

        $cnt_top_row = 1;

        if(! empty($file_path)) {
            //Check File Type
            $file_type = mime_content_type($file_path);
            $allowed_type = array('.csv', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel');
            if (!in_array($file_type, $allowed_type)) {
                $this->print_die($this->lang->line('global_upload_error'));
            }

            $this->load->library('pxl_1_8');

            $input_file_type = PHPExcel_IOFactory::identify($file_path);
            $obj_reader = PHPExcel_IOFactory::createReader($input_file_type);
            $obj_reader->setReadDataOnly(true);
            $obj_reader->setLoadSheetsOnly(0);
            $obj_php_excel = $obj_reader->load($file_path);

            $obj_php_excel->setActiveSheetIndex(0);
            $obj_worksheet = $obj_php_excel->getActiveSheet();
            $highest_row = $obj_worksheet->getHighestRow();

            if ($highest_row <= $cnt_top_row) {
                $this->print_die($this->lang->line('global_upload_error'));
            } else {
                $rs = $cnt_top_row + 1;
                $re = $highest_row;

                $update_data = array();
                $failed_data =  array();

                for ($i = $rs; $i <= $re; $i++) {
                    $data = array();

                    //Berkas Number
                    $col = "A";
                    $berkas_number_col = $obj_worksheet->getCell($col . $i);
                    $berkas_number = fixUTF8(trim($berkas_number_col->getCalculatedValue()));
                    $data['berkas_number'] = NULL;
                    if (!empty($berkas_number)) {
                        $data['berkas_number'] = $berkas_number;
                    }

                    //Vendor Kode
                    $col = 'D';
                    $vendor_code_col = $obj_worksheet->getCell($col . $i);
                    $vendor_code = fixUTF8(trim($vendor_code_col->getCalculatedValue()));
                    $data['vendor_code'] = NULL;
                    if (!empty($vendor_code)) {
                        $data['vendor_code'] = $vendor_code;
                    }

                    //Vendor Name
                    $col = 'E';
                    $vendor_name_col = $obj_worksheet->getCell($col . $i);
                    $vendor_name = fixUTF8(trim($vendor_name_col->getCalculatedValue()));
                    $data['vendor_name'] = NULL;
                    if (!empty($vendor_name)) {
                        $data['vendor_name'] = $vendor_name;
                    }

                    //Vendor NPWP
                    $col = 'F';
                    $vendor_npwp_col = $obj_worksheet->getCell($col . $i);
                    $vendor_npwp = fixUTF8(trim($vendor_npwp_col->getCalculatedValue()));
                    $data['vendor_npwp'] = NULL;
                    if (!empty($vendor_npwp)) {
                        $vendor_npwp = str_replace(array('.', '-'), '', $vendor_npwp);
                        $vendor_npwp = substr($vendor_npwp,0,2).'.'.substr($vendor_npwp,2,3).'.'.substr($vendor_npwp,5,3).'.'.substr($vendor_npwp,8,1).'.'.substr($vendor_npwp,9,3).'.'.substr($vendor_npwp,12,3);
                        $data['vendor_npwp'] = $vendor_npwp;
                    }

                    if( $data['berkas_number'] == NULL ){
                        $failed_data[] = $data;
                        continue;
                    }


                    if ($data) {
                        $find_prm = array(
                            'table' => 'berkas',
                            'select' => '*',
                            'where' => array(
                                'berkas_number' => strtoupper($data['berkas_number']),
                                'is_active' => 1
                            ),
                        );

                        $exist = $this->def_model->get_one($find_prm);

                        if ($exist->num_rows() > 0) {
                            $data['id'] = $exist->row()->id;
                            unset($data['berkas_number']);
                            if($data['vendor_code'] == NULL){
                                unset($data['vendor_code']);
                                unset($data['vendor_name']);
                            }
                            $update_data[] = $data;
                        }
                    }
                }

                $processed = FALSE;
                if (count($update_data) > 0) {
                    $param = array(
                        'table' => 'berkas',
                        'data' => $update_data,
                        'where_key' => 'id'
                    );
                    $processed = $this->def_model->update_batch($param);
                }

                if (count($update_data) == 0) {
                    $message = $this->lang->line('global_data_not_found');
                    $this->print_die($message);
                }

                if ($processed) {
                    echo 'Data Update: '.count($update_data) . ' - Data Gagal: '.count($failed_data);
                } else {
                    echo $this->output->status_callback('json_unsuccess');
                }
            }
        }
    }

    function export_sap_deprecated()
    {
        ini_set('memory_limit','-1');

        $this->load->model('arus_barang_model');

        $start_date = $this->input->post('start');
        $end_date = $this->input->post('end');

        if(!empty($start_date) && !empty($end_date)){
            $start_date =  $this->date_format_Ymd($this->input->post('start'), '-');
            $end_date =  $this->date_format_Ymd($this->input->post('end'), '-');
        }

        $faktur_start = $this->input->post('faktur_start');
        $faktur_end = $this->input->post('faktur_end');

        if(!empty($faktur_start) && !empty($faktur_end)){
            $faktur_start =  $this->date_format_Ymd($this->input->post('faktur_start'), '-');
            $faktur_end =  $this->date_format_Ymd($this->input->post('faktur_end'), '-');
        }

        $berkas_status = $this->input->post('berkas_status');

        $finish_start = $this->input->post('finish_start');
        $finish_end = $this->input->post('finish_end');

        if(!empty($finish_start) && !empty($finish_end)){
            $finish_start =  $this->date_format_Ymd($this->input->post('finish_start'), '-');
            $finish_end =  $this->date_format_Ymd($this->input->post('finish_end'), '-');
        }

        $division = '';

        if($this->session->userdata($this->session_key('group_name')) == 'TO'){
            $division = $this->session->userdata($this->session_key('cabang_id'));
        }

        $data_tt =  $this->arus_barang_model->get_gr_sap_advanced($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);
        $data_la =  $this->arus_barang_model->get_berkas_la($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);
        $data_lu =  $this->arus_barang_model->get_berkas_lu($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);
        $data_ttret =  $this->arus_barang_model->get_berkas_ttret($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);
        $data_ttum =  $this->arus_barang_model->get_berkas_ttum($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);

        $data = array_merge($data_tt->result_array(), $data_la, $data_lu, $data_ttret->result_array(), $data_ttum->result_array()) ;

        $this->load->library('pxl_1_8');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export_sap")->setDescription("none");

        $column = 'A';
        $headings=array('Kode Proyek','Nomor Berkas', 'Nomor Faktur Pajak', 'Tanggal Faktur', 'Vendor', 'No. PO', 'GR/SES', 'Surat Jalan', 'Currency', 'Nilai PPN', 'URL FP');
        for($c=0;$c<count($headings);$c++)
        {
            $objPHPExcel->getActiveSheet()->setCellValue($column.'1',$headings[$c]); // Add column heading data
            if($c==count($headings)-1)
            {
                break;// Need to terminate the loop when coumn letter reachs max
            }
            $column++;
        }

        $j = 2;
        foreach ($data as $sap){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$j,trim(iconv("UTF-8","ISO-8859-1", $sap['project_code'])," \t\n\r\0\x0B\xA0"));
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,trim(iconv("UTF-8","ISO-8859-1", $sap['berkas_number'])," \t\n\r\0\x0B\xA0"));
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,trim(iconv("UTF-8","ISO-8859-1", $sap['faktur_pajak_number'])," \t\n\r\0\x0B\xA0"));
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,trim(iconv("UTF-8","ISO-8859-1", $this->date_format_dmy($sap['faktur_pajak_date'],'.'))," \t\n\r\0\x0B\xA0"));
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$j,trim(iconv("UTF-8","ISO-8859-1", $sap['vendor_code'])," \t\n\r\0\x0B\xA0"));
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$j,trim(iconv("UTF-8","ISO-8859-1", $sap['nomor_po'])," \t\n\r\0\x0B\xA0"));
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$j,trim(iconv("UTF-8","ISO-8859-1", $sap['no_gr_ses'])," \t\n\r\0\x0B\xA0"));
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$j,trim(iconv("UTF-8","ISO-8859-1", $sap['surat_jalan_lkp_number'])," \t\n\r\0\x0B\xA0"));
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$j,'IDR');
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$j,trim(iconv("UTF-8","ISO-8859-1", $this->money_format($sap['fp_amount'], ','))," \t\n\r\0\x0B\xA0"));
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$j,trim(iconv("UTF-8","ISO-8859-1", $sap['faktur_pajak_url'])," \t\n\r\0\x0B\xA0"));;
            $j++;
        }

//        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename="export_sap.csv"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->setDelimiter('|');
        $objWriter->setEnclosure('');
        $objWriter->setLineEnding("\r\n");
        $objWriter->setSheetIndex(0);
        $objWriter->save('php://output');
        exit();

    }

    function export_sap()
    {
        ini_set('memory_limit','-1');

        $this->load->model('arus_barang_model');

        $start_date = $this->input->post('start');
        $end_date = $this->input->post('end');

        if(!empty($start_date) && !empty($end_date)){
            $start_date =  $this->date_format_Ymd($this->input->post('start'), '-');
            $end_date =  $this->date_format_Ymd($this->input->post('end'), '-');
        }

        $faktur_start = $this->input->post('faktur_start');
        $faktur_end = $this->input->post('faktur_end');

        if(!empty($faktur_start) && !empty($faktur_end)){
            $faktur_start =  $this->date_format_Ymd($this->input->post('faktur_start'), '-');
            $faktur_end =  $this->date_format_Ymd($this->input->post('faktur_end'), '-');
        }

        $berkas_status = $this->input->post('berkas_status');

        $finish_start = $this->input->post('finish_start');
        $finish_end = $this->input->post('finish_end');

        if(!empty($finish_start) && !empty($finish_end)){
            $finish_start =  $this->date_format_Ymd($this->input->post('finish_start'), '-');
            $finish_end =  $this->date_format_Ymd($this->input->post('finish_end'), '-');
        }

        $division = '';

        if($this->session->userdata($this->session_key('group_name')) == 'TO'){
            $division = $this->session->userdata($this->session_key('cabang_id'));
        }

        $data_tt =  $this->arus_barang_model->get_gr_sap_advanced($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);
        $data_la =  $this->arus_barang_model->get_berkas_la($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);
        $data_lu =  $this->arus_barang_model->get_berkas_lu($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);
        $data_ttret =  $this->arus_barang_model->get_berkas_ttret($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);
        $data_ttum =  $this->arus_barang_model->get_berkas_ttum($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);

        $data = array_merge($data_tt->result_array(), $data_la, $data_lu, $data_ttret->result_array(), $data_ttum->result_array()) ;

        $this->load->library('pxl_1_8');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export_sap")->setDescription("none");

        $column = 'A';
        $headings=array('Kode Proyek','Nomor Berkas', 'Nomor Faktur Pajak', 'Tanggal Faktur', 'Vendor', 'No. PO', 'GR/SES', 'Surat Jalan', 'Currency', 'Nilai PPN', 'URL FP');
        for($c=0;$c<count($headings);$c++)
        {
            $objPHPExcel->getActiveSheet()->setCellValue($column.'1',$headings[$c]); // Add column heading data
            if($c==count($headings)-1)
            {
                break;// Need to terminate the loop when coumn letter reachs max
            }
            $column++;
        }

        $j = 2;
        foreach ($data as $sap){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$sap['project_code']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$sap['berkas_number']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$sap['faktur_pajak_number']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$this->date_format_dmy($sap['faktur_pajak_date']));
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$j,$sap['vendor_code']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$j,$sap['nomor_po']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$j,$sap['no_gr_ses']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$j,$sap['surat_jalan_lkp_number']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$j,'IDR');
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$j,$this->money_format($sap['fp_amount'], ','));
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$j,$sap['faktur_pajak_url']);
            $j++;
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=export_sap.xlsx');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit();

    }

    function export_tarra_fm(){
        ini_set('memory_limit','-1');

        $this->load->model('arus_barang_model');

        $start_date = $this->input->post('start_tarra');
        $end_date = $this->input->post('end_tarra');

        if(!empty($start_date) && !empty($end_date)){
            $start_date =  $this->date_format_Ymd($this->input->post('start_tarra'), '-');
            $end_date =  $this->date_format_Ymd($this->input->post('end_tarra'), '-');
        }

        $faktur_start = $this->input->post('faktur_start_tarra');
        $faktur_end = $this->input->post('faktur_end_tarra');

        if(!empty($faktur_start) && !empty($faktur_end)){
            $faktur_start =  $this->date_format_Ymd($this->input->post('faktur_start_tarra'), '-');
            $faktur_end =  $this->date_format_Ymd($this->input->post('faktur_end_tarra'), '-');
        }

        $berkas_status = $this->input->post('berkas_status_tarra');

        $finish_start = $this->input->post('finish_start_tarra');
        $finish_end = $this->input->post('finish_end_tarra');

        if(!empty($finish_start) && !empty($finish_end)){
            $finish_start =  $this->date_format_Ymd($this->input->post('finish_start_tarra'), '-');
            $finish_end =  $this->date_format_Ymd($this->input->post('finish_end_tarra'), '-');
        }

        $division = '';

        if($this->session->userdata($this->session_key('group_name')) == 'TO'){
            $division = $this->session->userdata($this->session_key('cabang_id'));
        }

        $res_data = $this->arus_barang_model->get_all_berkas($start_date, $end_date, $faktur_start, $faktur_end, $finish_start, $finish_end, $berkas_status, $division);

        $file_name = "tarra_fm_".date("d-m-Y");
        $title = "Data";

        $this->load->library('pxl_1_8');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getDefaultStyle()->applyFromArray(array(
            'fill'	=>array(
                'type'	=> 'solid',
                'border' => 0,
                'color'	=>	array(
                    'rgb' => 'FFFFFF'
                )
            )
        ));

        $objPHPExcel->getProperties()->setTitle('title')->setDescription("description");
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
        $objPHPExcel->getActiveSheet()->setTitle("{$title}");

        //header
        $sc = "A";
        $sr = "1";
        $s_col = $sc;
        $s_row = $sr;

        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('FM'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(5);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('kd_jenis_transaksi'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(15);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('fg_pengganti'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(5);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('nomor_faktur'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('masa_pajak'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(5);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('tahun_pajak'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(10);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('tanggal_faktur'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(10);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('npwp'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('nama'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('alamat_lengkap'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('jumlah_dpp'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('jumlah_ppn'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('jumlah_ppnbm'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('is_creditable'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(5);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('field_tambahan_1'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('field_tambahan_2'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('field_tambahan_3'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('field_tambahan_4'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_col++;
        $objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper('field_tambahan_5'));
        $objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

        $s_row++;

        $rnum = 1;
        foreach($res_data as $dt)
        {

            $d_col = $sc;
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, 'FM');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,substr($dt['faktur_pajak_number'], 0,2));
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,substr($dt['faktur_pajak_number'], 2,1));
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$this->replace_str_numeric(substr($dt['faktur_pajak_number'], 4), '.-',''));
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row, substr($dt['pajak_period'], 3)/1);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['pajak_year']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $dt['faktur_pajak_date'] = ! empty($dt['faktur_pajak_date']) ? convert_date($dt['faktur_pajak_date'],'d/m/y') : '';
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['faktur_pajak_date']);
            $objValidation3 = $objPHPExcel->getActiveSheet()->getCell($d_col.$s_row)->getDataValidation();
            $objValidation3->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation3->setAllowBlank(true);
            $objValidation3->setShowInputMessage(true);
            $objValidation3->setShowDropDown(false);
            $objValidation3->setPromptTitle('Format Tanggal');
            $objValidation3->setPrompt('DD-MM-YYYY');
            $objValidation3->setErrorTitle('Format Format Tanggal Salah');
            $objValidation3->setShowErrorMessage('Format Format Tanggal Salah');
            $objValidation3->setError('Format Format Tanggal Salah');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$this->replace_str_numeric($dt['vendor_npwp'], '.-', ''));
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['nama_penjual']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,$dt['alamat_penjual']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['dpp_amount']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getNumberFormat()->setFormatCode('#,##0.00');

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['fp_amount']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getNumberFormat()->setFormatCode('#,##0.00');

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['ppnbm_amount']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getNumberFormat()->setFormatCode('#,##0.00');

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, 1);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getNumberFormat()->setFormatCode('#,##0.00');

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,'');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row, $dt['project_code']);
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,'');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,'');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $d_col++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row,'');
            $objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $rnum++;
            $s_row++;
            session_write_close();
        }

        $s_row--;

        $objPHPExcel->getActiveSheet()->setAutoFilter( $objPHPExcel->getActiveSheet()
            ->calculateWorksheetDimension());

        #STYLE Header
        $objPHPExcel->getActiveSheet()->getStyle($sc.($sr).":".$s_col.($sr))->applyFromArray(
            array(
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => 'FFFFFF')
                ),
                'fill'	=>array(
                    'type'	=> 'solid',
                    'border' => 1,
                    'color'	=>	array(
                        'rgb' => '4c87b9'
                    ),
                ),
                'alignment'	=> array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle($sc.($sr).":".$s_col.($sr))->getAlignment()->setWrapText(false);

        #STYLE Content
        $objPHPExcel->getActiveSheet()->getStyle($sc.($sr+1).":".$s_col.($s_row))->applyFromArray(
            array(
                'alignment'	=> array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrapText' => true
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$file_name.'.xlsx');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        $this->log_activities(
            array(
                'module' => $this->class,
                'lang_subject' => 'arus_barang',
                'lang_item' => '',
                'lang_action'  => '[global_activity_export]'
            )
        );
        exit;

    }
}