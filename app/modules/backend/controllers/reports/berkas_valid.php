<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berkas_valid extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->check_module();
		
		$this->ref_months = $this->date_indonesia->get_month();
	}
	
	function index()
	{
		$data = array();
		
		$data['ref_payment_method'] = $this->def_model->get_list(array('table' => 'ref_payment_method', 'order_sort' => 'id', 'order_dir' => 'DESC'))->result_array();
		$data['ref_proyek'] = $this->berkas_m->get_data(array('group_by' => 'project_code'))->result_array();
		$data['ref_vendor'] = $this->berkas_m->get_data(array('group_by' => 'vendor_name'))->result_array();
		
		$data['ref_months'] = $this->ref_months;
		$data['ref_years'] = $this->get_years(2);
		
		$data['current_year'] = date('Y'); 
		$data['current_month'] = date('m'); 
			
		$data['CTR'] = $this;
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/select2/select2_metro.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/select2/select2.min.js').'"></script>';
		
		$this->header['css'][] = '<link rel="stylesheet" type="text/css" href="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.css').'" />';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/jquery.dataTables.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/data-tables/DT_bootstrap.js').'"></script>';
		
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.floatThead.min.js').'"></script>';
		$this->header['js'][]  = '<script type="text/javascript" src="'.app_asset_backend_url('plugins/jquery.pulsate.min.js').'"></script>';
		
		$this->load->view('global/header_view',$this->header);
		$this->load->view('reports/berkas_valid_view', $data);
		$this->load->view('global/footer_view'); 
	}
	
	function get_report($start = 0, $limit = 0)
	{
		$report_type = $this->input->post('report_type');
		$limit = 10;
		$res = $this->_query_data($start, $limit);

		$this->load->library('pagination');
		$config = array();
		$config["base_url"] = app_backend_url('reports/berkas_valid/get_report');
		$config["total_rows"] = ! empty($res[1]) ? $res[1] : 0;
		$config["per_page"] = $limit;
		$config['uri_segment'] = 5;

		$config['full_tag_open'] = "<div class='spm_report pagination pagination-centered'><ul>";
		$config['full_tag_close'] = '</ul></div>';

		$config['first_link'] = "&laquo; First";
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$pagination = $this->pagination->create_links();

		$results_html = $this->_results_html($res[0], $start, $res[2], (isset($pagination) ? $pagination : FALSE));
		
		echo $results_html;
	}

	private function _query_data($start, $limit)
	{
		$data = array();

		if($limit)
		{
			$prm['limit'] = $limit;
		}

		$prm['start'] = $start;

		if($this->input->post('project_code'))
		{
			$prm['where']['project_code'] = $this->input->post('project_code');
		}

		if($this->input->post('vendor_name'))
		{
			$prm['where']['vendor_name'] = $this->input->post('vendor_name');
		}

		$period_text = '';

		$prm['where']['berkas_status'] = 'Selesai';

		$prm['title'] = 'berkas_valid - ('.$period_text.')';

		$res_data = $this->berkas_m->recap_berkas($this->input->post('weekly_year'), $this->input->post('weekly_month'), $prm);
		$res_cnt  = $this->berkas_m->recap_berkas_count($this->input->post('weekly_year'), $this->input->post('weekly_month'), $prm);

		return array($res_data,$res_cnt,$prm);
	}

	private function _results_html($results, $start, $prm, $pagination, $export = FALSE)
	{
		$html = '';
		if(! empty($results))
		{
			$header = '';
			$body = '';
			$footer = '';

			if(isset($prm['vendor_name']))
			{
				$prm['title'] = $prm['title'].' / '.$prm['vendor_name'];
			}

			$prm['report_type'] = '1';

			if(! $export)
			{
				$xls_css = $prm['report_type'] == '1' ? 'display:none;' : '';
				$header .= '
					<div style="float:left;margin-bottom:10px;">
						<h4><b>'.$prm['title'].'</b></h4>
					</div>
					<div style="float:right;margin-bottom:10px;">
						<a data-type="xls"  style="float:left;margin-right:10px;color:#fff;" class="btn green export" href="#"><i class="icon-file"></i> XLS</a>
					</div>
					<div class="clear">';
			}

			$header .= '
				<table class="table-bordered table-striped table-hover table-condensed cf">
				<thead>
					<tr>
						<th style="width:1%;text-align:left;" class="hidden-phone">'.$this->lang->line('global_no').'</th>
						<th style="width:25%;text-align:left;" class="hidden-phone">'.$this->lang->line('berkas_project_name').'</th>
						<th style="width:5%;text-align:left;">'.$this->lang->line('berkas_project_code').'</th>
						<th style="width:5%;text-align:left;">'.$this->lang->line('berkas_number').'</th>
						<th style="width:5%;text-align:left;">'.$this->lang->line('berkas_vendor_code').'</th>
						<th style="width:5%;text-align:left;">'.$this->lang->line('berkas_vendor_name').'</th>
						<th style="width:5%;text-align:left;">'.$this->lang->line('berkas_barang_jasa_name').'</th>
						<th style="width:5%;text-align:left;">'.$this->lang->line('berkas_invoice_number').'</th>
						<th style="width:5%;text-align:left;">'.$this->lang->line('berkas_invoice_date').'</th>';


			$header .= '</tr>
				</thead>
			';

			$body .= '<tbody>';
			$now = date('Y-m-d');
			$rnum = $start + 1;

			if($prm['report_type'] == '1')
			{
				$p_sum = array();
				$p_sum_all = 0;
				foreach($results as $dt)
				{
					$p_sum_proyek = 0;
					$body .= '<tr>';

					$body .= '<td class="hidden-phone">'.$rnum.'</td>';
					$body .= '<td class="hidden-phone">'.char_limiter($dt['project_name'],30).'</td>';
					$body .= '<td >'.$dt['project_code'].'</td>';

					$body .= '<td style="width:12%;">'.$dt['berkas_number'].'</td>';
					$body .= '<td style="width:12%;">'.$dt['vendor_code'].'</td>';
					$body .= '<td style="width:12%;">'.$dt['vendor_name'].'</td>';
					$body .= '<td style="width:12%;">'.$dt['barang_jasa_name'].'</td>';
					$body .= '<td style="width:12%;">'.$dt['invoice_number'].'</td>';
					$body .= '<td style="width:12%;">'.$dt['invoice_date'].'</td>';
					$body .= '</tr>';

					$rnum++;
					session_write_close();
				}
			}

			$body .= '</tbody>';
			$body .= '<tfoot class="hidden-phone">';
			$body .= '<tr>';
			$body .= '</tr>';
			$body .= '</tfoot>';

			$footer .= '</table>';

			if(! $export)
			{
				if($pagination)
				{
					$footer .= '</div><div class="space5"></div>';
					$footer .= $pagination;
				}
			}


			$html .= $header.$body.$footer;
		}
		else
		{
			$html .= '<div class="report-search-notfound">
				<h1 align="center">
					'.$this->lang->line('global_not_found').'
				</h1>
			</div>';
		}

		return $html;
	}

	private function _query_data_old($start, $limit)
	{
		$data = array();

		if($limit)
		{
			$prm['limit'] = $limit;
		}
		
		$prm['start'] = $start;

		$prm['group_by'] = "project_code";

		$report_type = $this->input->post('report_type');
		if($report_type == '1')
		{
			$title_report = $this->lang->line('global_report_summary');
		}
		else
		{
			$title_report = $this->lang->line('global_report_detail');
			$prm['join'][] = array(
								'table' => "berkas_pembayaran c",
								'on'    => "a.id = c.berkas_id",
								'type'  => 'inner'
							);
		}
		$prm['report_type'] = $report_type;


		if($this->input->post('project_code'))
		{
			$prm['where']['project_code'] = $this->input->post('project_code');
		}
		
		if($this->input->post('vendor_name'))
		{
			$prm['where']['vendor_name'] = "'".$this->input->post('vendor_name')."'";
			$prm['vendor_name'] = $this->input->post('vendor_name');
		}

		if($this->input->post('payment_method'))
		{
			$prm['where']['payment_method'] = "'".$this->input->post('payment_method')."'";
			$prm['payment_method'] = $this->input->post('payment_method');
		}


		if($this->input->post('period') == 'weekly')
		{
			$weekly_month = $this->input->post('weekly_month');
			$weekly_year = $this->input->post('weekly_year');

			$prm_day = $prm_month = "";

			if(! in_array(intval($weekly_month), $this->month_triwulan))
			{
				$prm_day = "day(payment_date)<=25 and";
				$prm_month = "month(payment_date)={$weekly_month} and";
			}

			$period = array();
			$p = $this->get_weeks_by_year_month($weekly_year,$weekly_month);
			$current_p = current($p);
			$end_p = end($p);
			$x = 0;
			foreach($p as $w)
			{
				$p_day = $prm_day;
				$p_month = $prm_month;

				$w = intval($w);

				$prm_week = "and week(payment_date,3) in ({$w})";
				if($x == 0)
				{
					$mprev = $weekly_month - 1;
					if(! in_array(intval($mprev), $this->month_triwulan) )
					{
						$wprev = $w-1;
						$p_day = "";
						$p_month = "";
						$dateprev = $weekly_year.'-'.$mprev.'-25';
						$prm_week = "and week(payment_date,3) in ({$wprev},{$w}) and date(payment_date)>'{$dateprev}'";
					}
				}


				$period[$w] = array(
					'text' => $this->lang->line('global_week').' '.$w,
					'q' =>  "({$p_day} {$p_month} year(payment_date)={$weekly_year} {$prm_week})"
				);
				$x++;
			}
			$prm['period'] = $period;

			$period_text = $this->ref_months[$weekly_month].' '.$weekly_year;
			$period_text2 = $current_p.' - '.$end_p;
			$prm['title'] = $title_report.' - '.$this->lang->line('berkas_spm').' '.$this->lang->line('global_weekly').' '.$period_text2.' ('.$period_text.')';
		}

		if($this->input->post('period') == 'monthly')
		{
			$monthly_year = $this->input->post('monthly_year');

			$period = array();
			$p = $this->ref_months;
			$current_p = substr(current($p),0,3);
			$end_p = substr(end($p),0,3);
			foreach($p as $k => $m)
			{
				$prm_day = "";
				if(! in_array(intval($k), $this->month_triwulan))
				{
					$prm_day = "day(payment_date)<=25 and";
				}

				$prm_month = "month(payment_date)={$k} and";
				$mprev = $k - 1;
				if(! in_array(intval($mprev), $this->month_triwulan) )
				{
					if($k != 1)
					{
						$dateprev = $monthly_year.'-'.$mprev.'-25';
						$datecurt = $monthly_year.'-'.$k.'-26';

						if(! empty($prm_day))
						{
							$prm_day = "";
							$prm_date = "(date(payment_date)>'{$dateprev}' and date(payment_date)<'{$datecurt}') and";
						}
						else
						{
							$prm_date = "date(payment_date)>'{$dateprev}' and";
						}


						$prm_month = "month(payment_date) in({$mprev},{$k}) and {$prm_date}";
					}
				}


				$period[$m] = array(
					'text' => substr($m,0,3),
					'q' =>  "({$prm_day} {$prm_month} year(payment_date)={$monthly_year})"
				);
			}
			$prm['period'] = $period;

			$period_text = $monthly_year;
			$period_text2 = $current_p.' - '.$end_p;
			$prm['title'] = $title_report.' - '.$this->lang->line('berkas_spm').' '.$this->lang->line('global_monthly').' '.$period_text2.'  ('.$period_text.')';
		}

		if($this->input->post('period') == 'yearly')
		{
			$yearly_syear = $this->input->post('yearly_syear');
			$yearly_eyear = $this->input->post('yearly_eyear');

			$period = array();
			$p = $this->ref_months;
			for($y = $yearly_syear; $y <= $yearly_eyear; $y++)
			{
				$period[$y] = array(
					'text' =>$y,
					'q' =>  "(year(payment_date)={$y})"
				);
			}
			$prm['period'] = $period;

			if($yearly_syear != $yearly_eyear)
			{
				$period_text = $yearly_syear.' - '.$yearly_eyear;
			}
			else
			{
				$period_text = $yearly_syear;
			}

			$prm['title'] = $title_report.' - '.$this->lang->line('berkas_spm').' '.$this->lang->line('global_yearly').' ('.$period_text.')';
		}

		$res_data = $this->berkas_m->get_data($prm)->result_array(); //echo $this->debug_query();
		$res_cnt  = $this->berkas_m->get_data($prm,TRUE)->result_array(); 
		
		return array($res_data,$res_cnt,$prm);
	}
	
	private function _results_html_old($results, $start, $prm, $pagination, $export = FALSE)
	{
		$html = '';
		if(! empty($results))
		{
			$header = '';
			$body = '';
			$footer = '';
			
			if(isset($prm['vendor_name']))
			{
				$prm['title'] = $prm['title'].' / '.$prm['vendor_name'];
			}
			
			if(! $export)
			{
				$xls_css = $prm['report_type'] == '1' ? 'display:none;' : '';
				$header .= '
					<div style="float:left;margin-bottom:10px;">
						<h4><b>'.$prm['title'].'</b></h4>
					</div>
					<div style="float:right;margin-bottom:10px;">
						<a data-type="xls" data-report_type="'.$prm['report_type'].'" style="float:left;margin-right:10px;color:#fff;'.$xls_css.'" class="btn green export" href="#"><i class="icon-file"></i> XLS</a>
						<a data-type="pdf" data-report_type="'.$prm['report_type'].'" style="float:left;color:#fff" class="btn red export" href="#"><i class="icon-file"></i> PDF</a>
					</div>
					<div class="clear">';
			}
			
			$header .= '
				<table class="table-bordered table-striped table-hover table-condensed cf">
				<thead>
					<tr>
						<th style="width:1%;text-align:left;" class="hidden-phone">'.$this->lang->line('global_no').'</th>
						<th style="width:25%;text-align:left;" class="hidden-phone">'.$this->lang->line('berkas_project_name').'</th>
						<th style="width:5%;text-align:left;">'.$this->lang->line('berkas_project_code').'</th>';
						
						if($prm['report_type'] == '1')
						{
							foreach($prm['period'] as $val)
							{
								$header .= '<th style="width:5%;text-align:left;">'.$val['text'].'</th>';
							}
							$header .= '<th style="width:10%;text-align:left;">'.$this->lang->line('global_total').'</th>';
						}
						else
						{
							$header .= '<th style="width:5%;text-align:left;">'.$this->lang->line('berkas_number').'</th>';
							$header .= '<th style="width:7%;text-align:center;" class="hidden-phone">'.$this->lang->line('berkas_invoice_date').'</th>';
							$header .= '<th style="width:7%;text-align:center;" class="hidden-phone">'.$this->lang->line('berkas_payment_date').'</th>';
							$header .= '<th style="width:12%;text-align:left;" class="hidden-phone">'.$this->lang->line('berkas_total_biaya').'</th>';
							$header .= '<th style="width:7%;text-align:left;" class="hidden-phone">'.$this->lang->line('berkas_ppn').'</th>';
							$header .= '<th style="width:7%;text-align:left;" class="hidden-phone">'.$this->lang->line('berkas_pph').'</th>';
							$header .= '<th style="width:12%;text-align:left;" class="hidden-phone">'.$this->lang->line('berkas_netto_cost').'</th>';
							$header .= '<th style="width:12%;text-align:left;" >'.$this->lang->line('berkas_total_pembayaran').'</th>';	
						}
						
						
					$header .= '</tr>
				</thead>
			';
			
			$body .= '<tbody>';
			$now = date('Y-m-d');
			$rnum = $start + 1;
			
			if($prm['report_type'] == '1')
			{
				$p_sum = array();
				$p_sum_all = 0;
				foreach($results as $dt)
				{
					$p_sum_proyek = 0;
					$body .= '<tr>';
					
					$body .= '<td class="hidden-phone">'.$rnum.'</td>';
					$body .= '<td class="hidden-phone">'.char_limiter($dt['project_name'],30).'</td>';
					$body .= '<td >'.$dt['project_code'].'</td>';
					
					
					$param['where'] = array('project_code' => $dt['project_code']);
					
					if(isset($prm['vendor_name']))
					{
						$param['where']['vendor_name'] = $prm['vendor_name'];
					}
					
					if(isset($prm['payment_method']))
					{
						$param['where']['payment_method'] = $prm['payment_method'];
					}
					
					
					foreach($prm['period'] as $key => $val)
					{		
						$param['where_str'] = $val['q'];
						$res_pembayaran_hutang = $this->berkas_m->get_total_pembayaran_hutang($param);
						
						$total_pembayaran = '-';
						if(! empty($res_pembayaran_hutang[0]))
						{	
							$total_pembayaran = '<a href="javascript:;" data-w_str="'.base64_encode($val['q']).'" data-period="'.$val['text'].'" data-project_code="'.$dt['project_code'].'" class="get_detail_pembayaran">'.$this->money_format($res_pembayaran_hutang[0]).'</a>';
							
							if(! isset($p_sum[$key]))
							{
								$p_sum[$key] = $res_pembayaran_hutang[0];
							}
							else
							{
								$p_sum[$key] = $p_sum[$key] + $res_pembayaran_hutang[0];
							}
							
							$p_sum_proyek = $p_sum_proyek +  $res_pembayaran_hutang[0];
						}
						
						$body .= '<td style="width:5%;">';
						$body .= $total_pembayaran;
						$body .= '</td>';
					}
					
					$p_sum_all = $p_sum_all + $p_sum_proyek;
					$body .= '<td style="width:12%;">'.$this->money_format($p_sum_proyek).'</td>';
					$body .= '</tr>';
					 
					$rnum++;
					session_write_close();
				}
			}
			else
			{
				$p_sum_all = array();
				$sum = array();
				foreach($results as $data)
				{
					$param['where'] = array('project_code' => $data['project_code']);
					
					if(isset($prm['vendor_name']))
					{
						$param['where']['vendor_name'] = $prm['vendor_name'];
					}
					
					if(isset($prm['payment_method']))
					{
						$param['where']['payment_method'] = $prm['payment_method'];
					}
					
					$p_sum = array();
					foreach($prm['period'] as $key => $val)
					{		
						$param['where_str'] = $val['q'];
						$res_pembayaran_hutang = $this->berkas_m->get_pembayaran($param)->result_array();
						
						$i = 0;
						foreach($res_pembayaran_hutang as $dt)
						{
							if(! empty($dt))
							{
								$body .= '<tr>';
					
					
								$payment_date = $this->convert_date($dt['payment_date'], 'd M Y');
								$invoice_date = $this->convert_date($dt['invoice_date'], 'd M Y');
								
								$biaya_kotor = $this->money_format($dt['real_cost']+$dt['ppn_val']+$dt['pph_val']);
								$sum[1] = isset($sum[1]) ? $sum[1]+($dt['real_cost']+$dt['ppn_val']+$dt['pph_val']) : ($dt['real_cost']+$dt['ppn_val']+$dt['pph_val']);
								
								$ppn_val = $this->money_format($dt['ppn_val']);
								$sum[2] = isset($sum[2]) ? $sum[2]+($dt['ppn_val']) : ($dt['ppn_val']);
								
								$pph_val = $this->money_format($dt['pph_val']);
								$sum[3] = isset($sum[3]) ? $sum[3]+($dt['pph_val']) : ($dt['pph_val']);
								
								$netto_cost = $this->money_format($dt['netto_cost_val']);
								$sum[4] = isset($sum[4]) ? $sum[4]+($dt['netto_cost_val']) : ($dt['netto_cost_val']);
								
								$nominal_bayar = $this->money_format($dt['nominal']);
								$sum[5] = isset($sum[5]) ? $sum[5]+($dt['nominal']) : ($dt['nominal']);
								
								if($i == 0)
								{
									$body .= '<td class="hidden-phone">'.$rnum.'</td>';
									$body .= '<td class="hidden-phone">'.char_limiter($data['project_name'],50).'</td>';
									
									$rnum++;
								}
								else
								{
									$body .= '<td colspan="2"></td>';
								}
								
								$body .= '<td >'.$dt['project_code'].'</td>';
								$body .= '<td>'.$dt['berkas_number'].'</td>';
								$body .= '<td class="hidden-phone" style="width:7%;text-align:center;">'.$invoice_date.'</td>';
								$body .= '<td class="hidden-phone" style="width:7%;text-align:center;">'.$payment_date.'</td>';
								$body .= '<td class="hidden-phone">'.$biaya_kotor.'</td>';
								$body .= '<td class="hidden-phone">'.$ppn_val.'</td>';
								$body .= '<td class="hidden-phone">'.$pph_val.'</td>';
								$body .= '<td class="hidden-phone">'.$netto_cost.'</td>';
								$body .= '<td class="hidden-phone">'.$nominal_bayar.'</td>';
								
								$body .= '</tr>';
								
								
								$i++;
							}
						}
					}
					session_write_close();					
				}
			}
			
			$body .= '</tbody>';
			$body .= '<tfoot class="hidden-phone">';
			$body .= '<tr>';
			if($prm['report_type'] == '1')
			{
				$body .= '<td colspan="3" style="text-align:right;font-weight:bold;">'.$this->lang->line('global_total').' :</td>';
				foreach($prm['period'] as $key => $val)
				{
					$body .= '<td style="font-weight:bold;">'.(isset($p_sum[$key]) ? $this->money_format($p_sum[$key]) : 0).'</td>';
				}
				
				$body .= '<td style="font-weight:bold;">'.$this->money_format($p_sum_all).'</td>';
			}
			else
			{
				$body .= '<td colspan="10" style="text-align:right;font-weight:bold;">'.$this->lang->line('global_total').' :</td>';
				$body .= '<td style="font-weight:bold;">'.(isset($sum[5]) ? $this->money_format($sum[5]) : 0).'</td>';
			}
			$body .= '</tr>';
			$body .= '</tfoot>';
		
			$footer .= '</table>';
			
			if(! $export)
			{
				if($pagination)
				{
					$footer .= '</div><div class="space5"></div>';
					$footer .= $pagination;
				}
			}
			
			$html .= $header.$body.$footer;
		}
		else
		{
			$html .= '<div class="report-search-notfound">
				<h1 align="center">
					'.$this->lang->line('global_not_found').'
				</h1>
			</div>';
		}
		
		return $html;
	}
	
	function get_detail_pembayaran()
	{
		$where = array();
		foreach($this->input->post() as $key => $val)
		{
			if($key == 'w_str')
			{
				$prm['where_str'] = base64_decode($val);
			}
			else
			{
				$where[$key] = $val;
			}
		}
		
		$prm['where'] = $where;
		$prm['order_sort'] = "payment_date";
		$prm['order_dir'] = "DESC";
		
		$res_pembayaran = $this->berkas_m->get_pembayaran($prm)->result_array();
		
		$arr = array();
		$total_nominal = 0;
		foreach($res_pembayaran as $data)
		{
			$total_nominal = $total_nominal + $data['nominal'];
			$data['nominal'] = $this->money_format($data['nominal']);
			$payment_date = $data['payment_date'];
			$data['payment_date'] = $this->convert_date($payment_date, 'd M Y');
			$data['payment_dates'] = $this->convert_date($payment_date, 'd-m-Y');
			$data['vendor_names'] = char_limiter($data['vendor_name'], 30);
			
			$invoice_date = $data['invoice_date'];
			$data['invoice_date'] = $this->convert_date($invoice_date, 'd M Y');
			$data['invoice_dates'] = $this->convert_date($invoice_date, 'd-m-Y');
			$arr[] = $data;
		}
		
		echo '{ "success":true, "data": '.json_encode($arr).', "total_nominal": "'.$this->money_format($total_nominal).'" }';
	}
	
	function export_old($type, $report_type)
	{
		$res = $this->_query_data(0,FALSE);
		$cnt = count($res[1]);
		$prm = $res[2];
		$title = $prm['title'];
		
		if(isset($prm['vendor_name']))
		{
			$title = $title.' / '.$prm['vendor_name'];
		}
		
		if(! empty($cnt))
		{
			@set_time_limit(-1);
			if($type == 'pdf')
			{
				$this->load->library('mpdf/mpdf');
				$logo_path = ABSOLUTE_PATH.'asset/backend/img/logo_pp.png';
				
				$header = "<html><head>
				<style>
				body {
					font-family: 'RobotoRegular','Helvetica Neue',Helvetica,sans-serif;
					font-size: 12px;
				}

				h2,h4{
					font-weight: normal;
					line-height: normal;  
					color:#1D65B1;
				}

				h4 {
					font-size: 16px;
				}

				h4.middle{
					text-align: center;
				}

				h2 {
					font-size: 31.5px;
					text-align: right;
				}

				strong{
					font-family: 'RobotoRegular','Helvetica Neue',Helvetica,sans-serif;
					font-weight: bold;
					line-height: 21px;
					color:#666666;
				}

				.clear {
					clear: both;
					display: block;
					height: 0;
					overflow: hidden;
					visibility: hidden;
					width: 0;
				}
				
				p {
					margin: 15px 0;
				}

				.table-bordered{
					width: 100%;
				}

				.table-bordered th {
					background-color: #666666;
					border-top: 0 none;
					color: #FFFFFF;
					font-size: 12px;
					font-weight: normal;
					text-transform: uppercase;
					font-family: 'LatoBold','Helvetica Neue',Helvetica,sans-serif;
					font-weight: normal;
					padding:0px;
				}
				
				.table-bordered th {
					border-top: 0px solid #DDDDDD;
					line-height: 20px;
					padding: 8px;
					text-align: left;
					vertical-align: middle;
				}
				
				.table-bordered th {
					border: 1px solid #DDDDDD;
				}

				.table-bordered td {
					border: 1px solid #DDDDDD;
				}

				table-bordered td.center, table th.center {
					text-align: center;
				}
				
				.table-bordered td {
					border-top: 1px solid #666666;
					line-height: 20px;
					padding: 8px;
					text-align: left;
					vertical-align: top;
				}
				
				.center {
					float: none;
					margin-left: auto;
					margin-right: auto;
					text-align: center;
				}
				
				span.symbol  {
					font-weight:bold;
					font-size:16px;
					margin-right: 10px;
					color:#111111;
				}
				
				</style>
				</head><body>";
				$footer = "</body></html>";
				$strHtml .= '<table border="0" width="100%">
					<tr>
						<td width="20%" rowspan="2"><img width="100px;" src="'.$logo_path.'"/></td>
						<td><h2>'.$this->lang->line('berkas_report').' '.$this->lang->line('berkas_spm').'</h2></td>
					</tr>
					<tr>
						<td><h4>'.$title.'</h4></td>
					</tr>
				</table>
				<div style="border-top: 2px solid;padding-bottom:5px;margin-top:10px;"></div>';
				
				$strHtml .= $this->_results_html($res[0], 0, $prm, NULL, TRUE);
			
				$mpdf = new mPDF('c','A4','','' , 5 , 5 , 5 , 5 , 0 , 0);
				$mpdf->SetDisplayMode('fullpage');
				$mpdf->simpleTables = false;
				$mpdf->packTableData = false;
				$mpdf->WriteHTML($header);
				$mpdf->WriteHTML($strHtml);
				$mpdf->WriteHTML($footer);
				$mpdf->Output($this->lang->line('berkas_report').'_'.$title.'.pdf','D');
				exit;
			}
			else
			{
				
				$this->load->library('pxl');
				$objPHPExcel = new PHPExcel();
				
				$objPHPExcel->getDefaultStyle()->applyFromArray(array(
					'fill'	=>array(
						'type'	=> 'solid',
						'border' => 0,
						'color'	=>	array(
							'rgb' => 'FFFFFF'
						)
					)
				));
				
				$objPHPExcel->getProperties()->setTitle('title')->setDescription("description");
				
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
				$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(40);
				$objPHPExcel->getActiveSheet()->setTitle($this->lang->line('berkas_report').' '.$this->lang->line('berkas_spm')); 
				
				$sc = "A";
				$hc = "K";
				$rs = 3;
				$rc = $rs;
				
				
				//title
				$objPHPExcel->getActiveSheet()->setCellValue($sc."1", $title);
				$objPHPExcel->getActiveSheet()->mergeCells($sc."1".":".$hc."1");
				
				$objPHPExcel->getActiveSheet()->setCellValue($sc."2", "");
				$objPHPExcel->getActiveSheet()->mergeCells($sc."2".":".$hc."2");
				
				
				//header
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->setCellValue("A{$rs}", strtoupper($this->lang->line('global_no')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
				$objPHPExcel->getActiveSheet()->setCellValue("B{$rs}", strtoupper($this->lang->line('berkas_project_name')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
				$objPHPExcel->getActiveSheet()->setCellValue("C{$rs}", strtoupper($this->lang->line('berkas_project_code')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->setCellValue("D{$rs}", strtoupper($this->lang->line('berkas_number')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->setCellValue("E{$rs}", strtoupper($this->lang->line('berkas_invoice_date')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->setCellValue("F{$rs}", strtoupper($this->lang->line('berkas_payment_date')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				$objPHPExcel->getActiveSheet()->setCellValue("G{$rs}", strtoupper($this->lang->line('berkas_total_biaya')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
				$objPHPExcel->getActiveSheet()->setCellValue("H{$rs}", strtoupper($this->lang->line('berkas_ppn')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
				$objPHPExcel->getActiveSheet()->setCellValue("I{$rs}", strtoupper($this->lang->line('berkas_pph')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
				$objPHPExcel->getActiveSheet()->setCellValue("J{$rs}", strtoupper($this->lang->line('berkas_netto_cost')));
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
				$objPHPExcel->getActiveSheet()->setCellValue("K{$rs}", strtoupper($this->lang->line('berkas_total_pembayaran')));

				
				
				$rnum = 1;
				$row = $rc+1;
				$p_sum_all = array();
				
				foreach($res[0] as $data)
				{
					$param['where'] = array('project_code' => $data['project_code']);
					
					if(isset($prm['vendor_name']))
					{
						$param['where']['vendor_name'] = $prm['vendor_name'];
					}
					
					if(isset($prm['payment_method']))
					{
						$param['where']['payment_method'] = $prm['payment_method'];
					}
					
					$p_sum = array();
					foreach($prm['period'] as $key => $val)
					{		
						$param['where_str'] = $val['q'];
						$res_pembayaran_hutang = $this->berkas_m->get_pembayaran($param)->result_array();
						
						$i = 0;
						$p_sum_proyek = array();
						foreach($res_pembayaran_hutang as $dt)
						{
							if(! empty($dt))
							{
								$payment_date = $this->convert_date($dt['payment_date'], 'd M Y');
								$invoice_date = $this->convert_date($dt['invoice_date'], 'd M Y');
								
								$biaya_kotor = $dt['real_cost']+$dt['ppn_val']+$dt['pph_val'];
								$sum[1] = isset($sum[1]) ? $sum[1]+($dt['real_cost']+$dt['ppn_val']+$dt['pph_val']) : ($dt['real_cost']+$dt['ppn_val']+$dt['pph_val']);
								
								$ppn_val = $dt['ppn_val'];
								$sum[2] = isset($sum[2]) ? $sum[2]+($dt['ppn_val']) : ($dt['ppn_val']);
								
								$pph_val = $dt['pph_val'];
								$sum[3] = isset($sum[3]) ? $sum[3]+($dt['pph_val']) : ($dt['pph_val']);
								
								$netto_cost = $dt['netto_cost_val'];
								$sum[4] = isset($sum[4]) ? $sum[4]+($dt['netto_cost_val']) : ($dt['netto_cost_val']);
								
								$nominal_bayar = $dt['nominal'];
								$sum[5] = isset($sum[5]) ? $sum[5]+($dt['nominal']) : ($dt['nominal']);
								
								$p_sum_proyek[] = "K".$row;
								
								if($i == 0)
								{
									$objPHPExcel->getActiveSheet()->setCellValue("A".$row, $rnum);
									$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $data['project_name']);
									
									$rnum++;
								}
								
								$objPHPExcel->getActiveSheet()->setCellValue("C".$row, $data['project_code']);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $data['berkas_number']);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $invoice_date);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $payment_date);
								$objPHPExcel->getActiveSheet()->setCellValue("G".$row, $biaya_kotor);
								$objPHPExcel->getActiveSheet()->setCellValue("H".$row, $ppn_val);
								$objPHPExcel->getActiveSheet()->setCellValue("I".$row, $pph_val);
								$objPHPExcel->getActiveSheet()->setCellValue("J".$row, $netto_cost);
								$objPHPExcel->getActiveSheet()->setCellValue("K".$row, $nominal_bayar);
								
								
									
								$i++;
								$row++;
							}
						}
						
						if(! empty($p_sum_proyek))
						{
							$objPHPExcel->getActiveSheet()->setCellValue("J".$row, $this->lang->line('global_total'));
							$objPHPExcel->getActiveSheet()->getStyle("J".$row)->getFont()->setBold(true);
							$objPHPExcel->getActiveSheet()->getStyle("J".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); 
						
							$c_sum = current($p_sum_proyek);
							$e_sum = end($p_sum_proyek);
							$objPHPExcel->getActiveSheet()->setCellValue($hc.$row, "=SUM({$c_sum}:{$e_sum})");
							
							$objPHPExcel->getActiveSheet()->getStyle($hc.$row)->getFont()->setBold(true);
						
							$p_sum_all[] = $hc.$row;
							
							$objPHPExcel->getActiveSheet()->getStyle("{$sc}{$row}:".$hc.$row)->applyFromArray(
								array(
									'fill' => array(
										'type' => PHPExcel_Style_Fill::FILL_SOLID,
										'color' => array('rgb' => 'F0F8FF')
									)
								)	
							);
							$row++;
						}
					}
					
					session_write_close();
				}
				
				if(! empty($p_sum_all))
				{
					$objPHPExcel->getActiveSheet()->setCellValue("J".$row, $this->lang->line('global_grand_total'));
					$objPHPExcel->getActiveSheet()->getStyle("J".$row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("J".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); 
					
					$a_p_sum_all = implode(",",$p_sum_all);
					$objPHPExcel->getActiveSheet()->setCellValue($hc.$row, "=SUM({$a_p_sum_all})");
					$objPHPExcel->getActiveSheet()->getStyle($hc.$row)->getFont()->setBold(true);
					
					$objPHPExcel->getActiveSheet()->getStyle("{$sc}{$row}:".$hc.$row)->applyFromArray(
						array(
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'd6f5f5')
							)
						)	
					);
				}
				
				$sharedStyle1 = new PHPExcel_Style();
				$sharedStyle1->applyFromArray(
					array('borders' => array(
						'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
						'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
						'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
					)
				));

				$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "{$sc}{$rc}:{$hc}{$rc}");
				
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}{$rc}:{$hc}{$rc}")->applyFromArray(
					array(
						'fill'	=>array(
							'type'	=> 'solid',
							'border' => 0,
							'color'	=>	array(
								'rgb' => '444444'
							)
							),
						'alignment'	=> array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							'wrapText' => true
							),
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						)
					)	
				);
				
				// title
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}1:{$hc}1")->getAlignment()->setWrapText(true); 
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}1:{$hc}1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}1:{$hc}1")->getFont()->setSize(20);
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}1:{$hc}1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
				
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}{$rc}:{$hc}{$rc}")->getAlignment()->setWrapText(true); 
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}{$rc}:{$hc}{$rc}")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}{$rc}:{$hc}{$rc}")->getFont()->getColor()->applyFromArray(array("argb" => 'FFFFFF'));
				
				$objPHPExcel->getActiveSheet()->setAutoFilter("B{$rc}:{$hc}{$rc}");
				
				$row = $row;
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}{$rc}:{$hc}".$row)->getFont()->setName('Arial');
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}{$rc}:{$hc}".$row)->getFont()->setSize(8);
				$objPHPExcel->getActiveSheet()->getStyle("G{$rc}:{$hc}".$row)->getNumberFormat()->setFormatCode('#,##0');
				
				
				
				$objPHPExcel->getActiveSheet()->getStyle("{$sc}{$rc}:{$hc}".$row)->applyFromArray(
					array(
						'alignment'	=> array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							'wrapText' => true
						)
					)	
				);
				
				$objPHPExcel->getActiveSheet()->getStyle("B{$rc}:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
				
				$objPHPExcel->getActiveSheet()->freezePane("A4");
				//$objPHPExcel->getActiveSheet()->freezePane("G1");
				
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename='.$this->lang->line('berkas_report').'_'.$this->replace_space($title).'.xlsx');
				header('Cache-Control: max-age=0');
				$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objWriter->save('php://output');
				unset($res_data);
				unset($data);
			}
		}
		else
		{
			echo $this->lang->line('global_data_not_found');
		}
	}

	function export()
	{
		if($this->input->post('project_code'))
		{
			$prm['where']['project_code'] = $this->input->post('project_code');
		}

		if($this->input->post('vendor_name'))
		{
			$prm['where']['vendor_name'] = $this->input->post('vendor_name');
		}

		$period_text = '';

		$prm['where']['berkas_status'] = 'Selesai';

		$prm['title'] = 'berkas_valid - ('.$period_text.')';

		$res_data = $this->berkas_m->recap_berkas($this->input->post('weekly_year'), $this->input->post('weekly_month'), $prm);

		$this->load->library('pxl_1_8');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getDefaultStyle()->applyFromArray(array(
			'fill'	=>array(
				'type'	=> 'solid',
				'border' => 0,
				'color'	=>	array(
					'rgb' => 'FFFFFF'
				)
			)
		));

		$objPHPExcel->getProperties()->setTitle('title')->setDescription("description");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
		$objPHPExcel->getActiveSheet()->setTitle("Rekap Kinerja");

		//header
		$sc = "A";
		$sr = "1";
		$s_col = $sc;
		$s_row = $sr;

		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('global_no')));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(10);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_project_name')));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(30);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_project_code')));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(30);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_number')));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(10);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_vendor_code')));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(10);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_vendor_name')));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(30);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_barang_jasa_name')));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(30);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_invoice_number')));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(30);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, strtoupper($this->lang->line('berkas_invoice_date')));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(10);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_ppn'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(5);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_pph'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(5);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, 'DPP');
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_faktur_pajak_number'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_faktur_pajak_date'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_pajak_period'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(15);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, 'Nilai PPN FP');
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_npwp'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(20);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_hard_copy_arrived'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(15);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_hard_copy_approved'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(15);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_hard_copy_status'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(15);

		$s_col++;
		$objPHPExcel->getActiveSheet()->setCellValue($s_col.$s_row, $this->lang->line('berkas_hard_copy_description'));
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle($s_col.$s_row .':'.$s_col. ''.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension($s_col)->setWidth(30);

		$s_row++;
		$rnum = 1;
		foreach($res_data as $dt)
		{

			$d_col = $sc;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $rnum);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row, $dt['project_name']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row, $dt['project_code']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row, $dt['berkas_number']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row, $dt['vendor_code']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row, $dt['vendor_name']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row, $dt['barang_jasa_name']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($d_col.$s_row, $dt['invoice_number']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row,  $this->convert_date($dt['invoice_date'],'d M Y'));
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['ppn'] ?  $dt['ppn']. '%' : '');
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['pph'] ?  $dt['pph']. '%' : '');
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['real_cost']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getNumberFormat()->setFormatCode('#,##0');

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['faktur_pajak_number']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, !empty($dt['faktur_pajak_date']) ? $this->convert_date($dt['faktur_pajak_date'],'d M Y') : '');
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, !empty($dt['pajak_period']) ? $dt['pajak_period'] . ' - ' . $dt['pajak_year'] : '');
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['fp_amount']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getNumberFormat()->setFormatCode('#,##0');

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['vendor_npwp']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row,  !empty($dt['hard_copy_arrived']) ? $this->convert_date($dt['hard_copy_arrived'],'d M Y') : '');
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row,  !empty($dt['hard_copy_approved']) ? $this->convert_date($dt['hard_copy_approved'],'d M Y') : '');
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['hard_copy_status']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$d_col++;
			$objPHPExcel->getActiveSheet()->setCellValue($d_col.$s_row, $dt['hard_copy_description']);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($d_col.$s_row)->getAlignment()->setWrapText(true);

			$rnum++;
			$s_row++;
			session_write_close();
		}

		$s_row--;

		$objPHPExcel->getActiveSheet()->setAutoFilter( $objPHPExcel->getActiveSheet()
			->calculateWorksheetDimension());

		#STYLE Header
		$objPHPExcel->getActiveSheet()->getStyle($sc.($sr).":".$s_col.($sr))->applyFromArray(
			array(
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => 'FFFFFF')
				),
				'fill'	=>array(
					'type'	=> 'solid',
					'border' => 1,
					'color'	=>	array(
						'rgb' => '4c87b9'
					),
				),
				'alignment'	=> array(
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				),
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);

		$objPHPExcel->getActiveSheet()->getStyle($sc.($sr).":".$s_col.($sr))->getAlignment()->setWrapText(true);

		#STYLE Content
		$objPHPExcel->getActiveSheet()->getStyle($sc.($sr+1).":".$s_col.($s_row))->applyFromArray(
			array(
				'alignment'	=> array(
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'wrapText' => true
				),
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);

		$file_name = "berkas_valid_".time();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='.$file_name.'.xlsx');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;

	}
}