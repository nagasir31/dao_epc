<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arus_barang_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = 'arus_barang';
    }

    function get_gr_sap($start_date, $end_date)
    {
        $sql = 'SELECT
                    `b`.created_datetime,
                    `b`.id,
                    `b`.project_code,
                    `b`.berkas_number,
                     CAST( `b`.rilis_datetime AS DATE) as rilis_datetime,
                    `b`.faktur_pajak_number,
                    `b`.faktur_pajak_date,
                    `b`.vendor_code,
                    `b`.surat_jalan_lkp_number,
                    `b`.fp_amount,
                    `x`.note as \'nomor_po\',
                    `x`.no_gr_ses
                FROM
                    ( `arus_barang` a )
                    INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id` 
                    AND b.is_active = 1
                    INNER JOIN `berkas_pembayaran_items` x ON `b`.`id` = `x`.`berkas_id` 
                    WHERE b.created_datetime BETWEEN ? AND ?	
                UNION
                SELECT
                    `b`.created_datetime,	
                    `b`.id,
                    `b`.project_code,
                    `b`.berkas_number,
                    CAST( `b`.rilis_datetime AS DATE) as rilis_datetime,
                    `b`.faktur_pajak_number,
                    `b`.faktur_pajak_date,
                    `b`.vendor_code,
                    `b`.surat_jalan_lkp_number,
                    `b`.fp_amount,
                    `x`.note as \'nomor_po\',
                    `x`.no_pg_ab
                FROM
                    ( `arus_barang` a )
                    INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id` 
                    AND b.is_active = 1 
                    INNER JOIN `berkas_pembayaran_items_pg` x ON `b`.`id` = `x`.`berkas_id` 	
                    WHERE b.created_datetime BETWEEN ? AND ?
                 ORDER by 1 ASC  
                ';

        if($query = $this->db->query($sql, array($start_date, $end_date, $start_date, $end_date))){
            return $query;
        }
        return false;
    }

    function get_gr_sap_advanced($start_date = "", $end_date = "", $faktur_start = "", $faktur_end = "", $finish_start = "", $finish_end = "", $berkas_status = "", $division = "")
    {

        $arr_bind = array();
        $sql = 'SELECT
                    `b`.created_datetime,
                    `b`.last_approval_datetime,
                    `b`.id,
                    `b`.project_code,
                    `b`.berkas_number,
                    `b`.faktur_pajak_number,
                    `b`.faktur_pajak_date,
                    `b`.vendor_code,
                    CASE
                        WHEN `b`.vendor_code = "ZV01" THEN `x`.note
                        ELSE `b`.nomor_bal_bast
                    END AS \'surat_jalan_lkp_number\',
                    `b`.fp_amount,
                    `x`.no_po_sap as \'nomor_po\',
                    `x`.no_gr_ses,
                    `b`.faktur_pajak_url
                FROM
                    ( `arus_barang` a )
                    INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id` 
                    AND b.is_active = 1
                    INNER JOIN `berkas_pembayaran_items` x ON `b`.`id` = `x`.`berkas_id`
                    WHERE 1';

        if(!empty($start_date) && !empty($end_date)){
            $sql .= ' AND DATE(`b`.created_datetime) BETWEEN ? AND ?';
            array_push($arr_bind, $start_date, $end_date);

        }

        if(!empty($faktur_start) && !empty($faktur_end)){
            $sql .= ' AND `b`.faktur_pajak_date BETWEEN ? AND ?';
            array_push($arr_bind, $faktur_start, $faktur_end);
        }

        if(!empty($berkas_status)){
            if($berkas_status == 'selesai'){
                $sql .= ' AND `b`.berkas_status = "Selesai"';
            }else{
                $sql .= ' AND (`b`.berkas_status <> "Selesai" OR `b`.berkas_status IS NULL)';
            }
        }

        if(!empty($finish_start) && !empty($finish_end)){
            $sql .= ' AND DATE(`b`.last_approval_datetime) BETWEEN ? AND ? AND `b`.last_verify_level = "TO" AND `b`.last_verify_status = "Disetujui" ';
            array_push($arr_bind, $finish_start, $finish_end);
        }

        if(!empty($division)){
            $sql .= ' AND `b`.unit_id = ? ';
            array_push($arr_bind, $division);
        }


        $sql .= ' GROUP BY `x`.no_gr_ses';

        $sql .= ' UNION ALL
                SELECT
                    `b`.created_datetime,
                    `b`.last_approval_datetime,
                    `b`.id,
                    `b`.project_code,
                    `b`.berkas_number,
                    `b`.faktur_pajak_number,
                    `b`.faktur_pajak_date,
                    `b`.vendor_code,
                     CASE
                        WHEN `b`.vendor_code = "ZV01" THEN `x`.note
                        ELSE `b`.nomor_bal_bast
                    END AS \'surat_jalan_lkp_number\',
                    `b`.fp_amount,
                    `x`.no_po_pg as \'nomor_po\',
                    `x`.no_pg_ab,
                    `b`.faktur_pajak_url
                FROM
                    ( `arus_barang` a )
                    INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id`
                    AND b.is_active = 1
                    INNER JOIN `berkas_pembayaran_items_pg` x ON `b`.`id` = `x`.`berkas_id`
                    WHERE 1';
        if(!empty($start_date) && !empty($end_date)){
            $sql .= ' AND DATE(`b`.created_datetime) BETWEEN ? AND ?';
            array_push($arr_bind, $start_date, $end_date);
        }

        if(!empty($faktur_start) && !empty($faktur_end)){
            $sql .= ' AND `b`.faktur_pajak_date BETWEEN ? AND ?';
            array_push($arr_bind, $faktur_start, $faktur_end);
        }

        if(!empty($berkas_status)){
            if($berkas_status == 'selesai'){
                $sql .= ' AND `b`.berkas_status = "Selesai"';
            }else{
                $sql .= ' AND (`b`.berkas_status <> "Selesai" OR `b`.berkas_status IS NULL)';
            }
        }

        if(!empty($finish_start) && !empty($finish_end)){
            $sql .= ' AND DATE(`b`.last_approval_datetime) BETWEEN ? AND ? AND `b`.last_verify_level = "TO" AND `b`.last_verify_status = "Disetujui" ';
            array_push($arr_bind, $finish_start, $finish_end);
        }

        if(!empty($division)){
            $sql .= ' AND `b`.unit_id = ? ';
            array_push($arr_bind, $division);
        }


        $sql .= ' GROUP BY `x`.no_pg_ab';

        $sql .= ' ORDER by 1 ASC';

        if($query = $this->db->query($sql, $arr_bind)){
            return $query;
        }
        return false;
    }

    function get_berkas_la($start_date = "", $end_date = "", $faktur_start = "", $faktur_end = "", $finish_start = "", $finish_end = "", $berkas_status = "", $division = ""){
        $arr_bind = array();
        $sql = 'SELECT
                    `b`.created_datetime,
                    `b`.last_approval_datetime,
                    `b`.id,
                    `b`.project_code,
                    `b`.berkas_number,
                    `b`.faktur_pajak_number,
                    `b`.faktur_pajak_date,
                    `b`.vendor_code,
                    `b`.nomor_berita_acara_alat as \'surat_jalan_lkp_number\',
                    `b`.fp_amount,
                    `b`.spk_po_number as  \'nomor_po\',
                    `b`.spk_po_number_manual as  \'nomor_po_manual\',
                    `b`.nomor_berita_acara_alat as  \'no_gr_ses\',
                    `b`.faktur_pajak_url
                FROM
                    ( `arus_barang` a )
                    INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id` 
                    AND b.is_active = 1
                    WHERE 1';

        if(!empty($start_date) && !empty($end_date)){
            $sql .= ' AND DATE(`b`.created_datetime) BETWEEN ? AND ?';
            array_push($arr_bind, $start_date, $end_date);

        }

        if(!empty($faktur_start) && !empty($faktur_end)){
            $sql .= ' AND `b`.faktur_pajak_date BETWEEN ? AND ?';
            array_push($arr_bind, $faktur_start, $faktur_end);
        }

        if(!empty($berkas_status)){
            if($berkas_status == 'selesai'){
                $sql .= ' AND `b`.berkas_status = "Selesai"';
            }else{
                $sql .= ' AND (`b`.berkas_status <> "Selesai" OR `b`.berkas_status IS NULL)';
            }
        }

        if(!empty($finish_start) && !empty($finish_end)){
            $sql .= ' AND DATE(`b`.last_approval_datetime) BETWEEN ? AND ? AND `b`.last_verify_level = "TO" AND `b`.last_verify_status = "Disetujui" ';
            array_push($arr_bind, $finish_start, $finish_end);
        }

        if(!empty($division)){
            $sql .= ' AND `b`.unit_id = ? ';
            array_push($arr_bind, $division);
        }

        $sql .= ' AND `b`.document_type_id = 2';

        $results =  array();

        if($query = $this->db->query($sql, $arr_bind)){

            if($query->num_rows() > 0){

                foreach ($query->result_array() as $data){

                    $po_sap = is_array(json_decode($data['nomor_po'], true)) ? json_decode($data['nomor_po'], true): array();
                    $po_manual = is_array(json_decode($data['nomor_po_manual'], true)) ? json_decode($data['nomor_po_manual'], true): array();

                    $po_sap =  array_filter($po_sap);
                    $po_manual = array_filter($po_manual);

                    $po_numbers = array_merge($po_sap, $po_manual);

                    foreach ($po_numbers as $po){
                        $results[] =  array(
                            'created_datetime'          => $data['created_datetime'],
                            'last_approval_datetime'    => $data['last_approval_datetime'],
                            'id'                        => $data['id'],
                            'project_code'              => $data['project_code'],
                            'berkas_number'             => $data['berkas_number'],
                            'faktur_pajak_number'       => $data['faktur_pajak_number'],
                            'faktur_pajak_date'         => $data['faktur_pajak_date'],
                            'vendor_code'               => $data['vendor_code'],
                            'surat_jalan_lkp_number'    => $data['surat_jalan_lkp_number'],
                            'fp_amount'                 => $data['fp_amount'],
                            'nomor_po'                  => $po,
                            'no_gr_ses'                 => $data['no_gr_ses'],
                            'faktur_pajak_url'          => $data['faktur_pajak_url'],
                        );
                    }
                }
            }
        }

        return $results;
    }

    function get_berkas_lu($start_date = "", $end_date = "", $faktur_start = "", $faktur_end = "", $finish_start = "", $finish_end = "", $berkas_status = "", $division = ""){
        $arr_bind = array();
        $sql = 'SELECT
                    `b`.created_datetime,
                    `b`.last_approval_datetime,
                    `b`.id,
                    `b`.project_code,
                    `b`.berkas_number,
                    `b`.faktur_pajak_number,
                    `b`.faktur_pajak_date,
                    `b`.vendor_code,
                    `b`.nomor_surat_jalan_terakhir as \'surat_jalan_lkp_number\',
                    `b`.fp_amount,
                    `b`.spk_po_number as  \'nomor_po\',
                    `b`.spk_po_number_manual as  \'nomor_po_manual\',
                    `b`.nomor_surat_jalan_terakhir as  \'no_gr_ses\',
                    `b`.faktur_pajak_url
                FROM
                    ( `arus_barang` a )
                    INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id` 
                    AND b.is_active = 1
                    WHERE 1';

        if(!empty($start_date) && !empty($end_date)){
            $sql .= ' AND DATE(`b`.created_datetime) BETWEEN ? AND ?';
            array_push($arr_bind, $start_date, $end_date);

        }

        if(!empty($faktur_start) && !empty($faktur_end)){
            $sql .= ' AND `b`.faktur_pajak_date BETWEEN ? AND ?';
            array_push($arr_bind, $faktur_start, $faktur_end);
        }

        if(!empty($berkas_status)){
            if($berkas_status == 'selesai'){
                $sql .= ' AND `b`.berkas_status = "Selesai"';
            }else{
                $sql .= ' AND (`b`.berkas_status <> "Selesai" OR `b`.berkas_status IS NULL)';
            }
        }

        if(!empty($finish_start) && !empty($finish_end)){
            $sql .= ' AND DATE(`b`.last_approval_datetime) BETWEEN ? AND ? AND `b`.last_verify_level = "TO" AND `b`.last_verify_status = "Disetujui" ';
            array_push($arr_bind, $finish_start, $finish_end);
        }

        if(!empty($division)){
            $sql .= ' AND `b`.unit_id = ? ';
            array_push($arr_bind, $division);
        }

        $sql .= ' AND `b`.document_type_id = 3';

        $results =  array();

        if($query = $this->db->query($sql, $arr_bind)){

            if($query->num_rows() > 0){

                foreach ($query->result_array() as $data){

                    $po_sap = is_array(json_decode($data['nomor_po'], true)) ? json_decode($data['nomor_po'], true): array();
                    $po_manual = is_array(json_decode($data['nomor_po_manual'], true)) ? json_decode($data['nomor_po_manual'], true): array();
                    $po_sap =  array_filter($po_sap);
                    $po_manual = array_filter($po_manual);

                    $po_numbers = array_merge($po_sap, $po_manual);

                    foreach ($po_numbers as $po){
                        $results[] =  array(
                            'created_datetime'          => $data['created_datetime'],
                            'last_approval_datetime'    => $data['last_approval_datetime'],
                            'id'                        => $data['id'],
                            'project_code'              => $data['project_code'],
                            'berkas_number'             => $data['berkas_number'],
                            'faktur_pajak_number'       => $data['faktur_pajak_number'],
                            'faktur_pajak_date'         => $data['faktur_pajak_date'],
                            'vendor_code'               => $data['vendor_code'],
                            'surat_jalan_lkp_number'    => $data['surat_jalan_lkp_number'],
                            'fp_amount'                 => $data['fp_amount'],
                            'nomor_po'                  => $po,
                            'no_gr_ses'                 => $data['no_gr_ses'],
                            'faktur_pajak_url'          => $data['faktur_pajak_url'],
                        );
                    }
                }
            }
        }

        return $results;
    }


    function get_berkas_ttret($start_date = "", $end_date = "", $faktur_start = "", $faktur_end = "", $finish_start = "", $finish_end = "", $berkas_status = "", $division = ""){
        $arr_bind = array();
        $sql = 'SELECT
                    `b`.created_datetime,
                    `b`.last_approval_datetime,
                    `b`.id,
                    `b`.project_code,
                    `b`.berkas_number,
                    `b`.faktur_pajak_number,
                    `b`.faktur_pajak_date,
                    `b`.vendor_code,
                    `b`.nomor_bast_2 as \'surat_jalan_lkp_number\',
                    `b`.fp_amount,
                    `b`.contract_number as  \'nomor_po\',
                    `b`.nomor_bast_2 as  \'no_gr_ses\',
                    `b`.faktur_pajak_url
                FROM
                    ( `arus_barang` a )
                    INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id` 
                    AND b.is_active = 1
                    WHERE 1';

        if(!empty($start_date) && !empty($end_date)){
            $sql .= ' AND DATE(`b`.created_datetime) BETWEEN ? AND ?';
            array_push($arr_bind, $start_date, $end_date);

        }

        if(!empty($faktur_start) && !empty($faktur_end)){
            $sql .= ' AND `b`.faktur_pajak_date BETWEEN ? AND ?';
            array_push($arr_bind, $faktur_start, $faktur_end);
        }

        if(!empty($berkas_status)){
            if($berkas_status == 'selesai'){
                $sql .= ' AND `b`.berkas_status = "Selesai"';
            }else{
                $sql .= ' AND (`b`.berkas_status <> "Selesai" OR `b`.berkas_status IS NULL)';
            }
        }

        if(!empty($finish_start) && !empty($finish_end)){
            $sql .= ' AND DATE(`b`.last_approval_datetime) BETWEEN ? AND ? AND `b`.last_verify_level = "TO" AND `b`.last_verify_status = "Disetujui" ';
            array_push($arr_bind, $finish_start, $finish_end);
        }

        if(!empty($division)){
            $sql .= ' AND `b`.unit_id = ? ';
            array_push($arr_bind, $division);
        }

        $sql .= ' AND `b`.document_type_id = 6';

        if($query = $this->db->query($sql, $arr_bind)){
            return $query;
        }
        return false;
    }

    function get_berkas_ttum($start_date = "", $end_date = "", $faktur_start = "", $faktur_end = "", $finish_start = "", $finish_end = "", $berkas_status = "", $division = ""){
        $arr_bind = array();
        $sql = 'SELECT
                    `b`.created_datetime,
                    `b`.last_approval_datetime,
                    `b`.id,
                    `b`.project_code,
                    `b`.berkas_number,
                    `b`.faktur_pajak_number,
                    `b`.faktur_pajak_date,
                    `b`.vendor_code,
                    `b`.contract_number as \'surat_jalan_lkp_number\',
                    `b`.fp_amount,
                    `b`.contract_number as  \'nomor_po\',
                    `b`.contract_number as  \'no_gr_ses\',
                    `b`.faktur_pajak_url
                FROM
                    ( `arus_barang` a )
                    INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id` 
                    AND b.is_active = 1
                    WHERE 1';

        if(!empty($start_date) && !empty($end_date)){
            $sql .= ' AND DATE(`b`.created_datetime) BETWEEN ? AND ?';
            array_push($arr_bind, $start_date, $end_date);

        }

        if(!empty($faktur_start) && !empty($faktur_end)){
            $sql .= ' AND `b`.faktur_pajak_date BETWEEN ? AND ?';
            array_push($arr_bind, $faktur_start, $faktur_end);
        }

        if(!empty($berkas_status)){
            if($berkas_status == 'selesai'){
                $sql .= ' AND `b`.berkas_status = "Selesai"';
            }else{
                $sql .= ' AND (`b`.berkas_status <> "Selesai" OR `b`.berkas_status IS NULL)';
            }
        }

        if(!empty($finish_start) && !empty($finish_end)){
            $sql .= ' AND DATE(`b`.last_approval_datetime) BETWEEN ? AND ? AND `b`.last_verify_level = "TO" AND `b`.last_verify_status = "Disetujui" ';
            array_push($arr_bind, $finish_start, $finish_end);
        }

        if(!empty($division)){
            $sql .= ' AND `b`.unit_id = ? ';
            array_push($arr_bind, $division);
        }

        $sql .= ' AND `b`.document_type_id = 7';

        if($query = $this->db->query($sql, $arr_bind)){
            return $query;
        }
        return false;
    }

    function get_data_deprecated($prm = array(), $is_count = FALSE)
    {
        $sub_query_form = '(SELECT
	        `b`.id,
	        `f`.`name` vendor_type_name,
	        `b`.project_code,
	        IF(e.sap_profit_center != "", `e`.`name`, `d`.`name`) project_name,
	        `b`.vendor_npwp,
	        `b`.vendor_name,
	        `b`.vendor_address,
	        `b`.faktur_pajak_number,
	        `b`.faktur_pajak_date,
	        `b`.pajak_period,
	        `b`.pajak_year,
	        `b`.real_cost,
	        `b`.fp_amount,
	        `a`.`ntpn_number`,
	        `a`.`ntpn_date`,
	        `b`.berkas_number,
	        `b`.barang_jasa_name,
	        CASE
                WHEN document_type_id = 6 OR document_type_id = 7 THEN 
                    IF(b.contract_number IS NOT NULL, CONCAT(\'["\', b.contract_number, \'"]\'), \'\')
                ELSE CONCAT(
                    \'[\', 
                    TRIM(trailing \']\' from trim(leading \'[\' from b.spk_po_number)), 
                    IF(b.spk_po_number_manual IS NULL OR b.spk_po_number_manual = \'[""]\' OR b.spk_po_number_manual = \'[]\',\'\', \',\'),
                    IF(b.spk_po_number_manual IS NULL OR b.spk_po_number_manual = \'[""]\' OR b.spk_po_number_manual = \'[]\',\'\', TRIM( TRAILING \']\' FROM trim( LEADING \'[\' FROM b.spk_po_number_manual ))), 
                    \']\'
                )
            END AS spk_po_number,	
	        CASE
                WHEN document_type_id = 6 OR document_type_id = 7 THEN 
                    IF(b.contract_date IS NOT NULL, CONCAT(\'["\', b.contract_date, \'"]\'), \'\')
                ELSE CONCAT(
                \'[\', 
                TRIM(trailing \']\' from trim(leading \'[\' from b.spk_po_date)), 
                IF(b.spk_po_date_manual IS NULL OR b.spk_po_date_manual = \'[""]\' OR b.spk_po_date_manual = \'[]\',\'\', \',\'),
                IF(b.spk_po_date_manual IS NULL OR b.spk_po_date_manual = \'[""]\' OR b.spk_po_date_manual = \'[]\',\'\', TRIM( TRAILING \']\' FROM trim( LEADING \'[\' FROM b.spk_po_date_manual ))),
                \']\'
                )
            END AS spk_po_date,	
	        `b`.invoice_number,
	        `b`.payment_method,
	        `b`.vendor_code,
	        `b`.pajak_type,
	        `c`.`name` division_name,
            CASE
                WHEN document_type_id = 1 AND vendor_type = \'ZV01\' THEN b.nomor_surat_jalan_terakhir
                    WHEN document_type_id = 1 AND vendor_type = \'ZV02\' THEN b.nomor_bal_bast
                    WHEN document_type_id = 2 THEN b.nomor_berita_acara_alat
                    WHEN document_type_id = 3 THEN b.nomor_surat_jalan_terakhir
                    WHEN document_type_id = 6 THEN b.nomor_bast_2
                ELSE ""
            END AS surat_jalan_lkp_number,	
            CASE
                WHEN document_type_id = 1 AND vendor_type = \'ZV01\' THEN b.nomor_surat_jalan_terakhir_date
                    WHEN document_type_id = 1 AND vendor_type = \'ZV02\' THEN b.nomor_bal_bast_date
                    WHEN document_type_id = 2 THEN b.nomor_berita_acara_alat_date
                    WHEN document_type_id = 3 THEN b.nomor_surat_jalan_terakhir_date
                    WHEN document_type_id = 6 THEN b.nomor_bast_2_date
                ELSE ""
            END AS surat_jalan_lkp_date	
            FROM
            ( `arus_barang` a )
            INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id` 
            AND b.is_active = 1
            INNER JOIN `ref_cabang` c ON `c`.`id` = `b`.`unit_id`
            LEFT JOIN `ref_proyek` d ON `b`.`project_code` = `d`.`id`
            LEFT JOIN `ref_proyek` e ON `b`.`project_code` = `e`.`sap_profit_center`
            INNER JOIN `ref_vendor_type` f ON `b`.`vendor_type` = `f`.`code` 
		) as a';

        $this->db->_protect_identifiers=false;
        $this->db->from($sub_query_form);
        if(isset($prm['join']))
        {
            foreach($prm['join'] as $join)
            {
                if(isset($join['type']))
                {
                    $this->db->join($join['table'], $join['on'], $join['type']);
                }
                else
                {
                    $this->db->join($join['table'], $join['on']);
                }
            }
        }

        if(isset($prm['where_string']))
        {
            $this->db->where($prm['where_string']);
        }

        if(isset($prm['where']))
        {
            $this->db->where($prm['where'], FALSE, FALSE);
        }

        if(isset($prm['where_str']))
        {
            $this->db->where($prm['where_str']);
        }

        if(isset($prm['where_in']))
        {
            $this->db->where_in($prm['where_in'][0],$prm['where_in'][1]);
        }

        if(isset($prm['where_not_in']))
        {
            $this->db->where_not_in($prm['where_not_in'][0],$prm['where_not_in'][1]);
        }

        $pattern = "((0[1-9]|[12][0-9]|3[01])[-](0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d)";
        $pattern2 = "((0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d)";

        if(isset($prm['search']) && $prm['search'] != '')
        {
            if($prm['search'] == 'publish' || $prm['search'] == 'active' || $prm['search'] == 'accepted' || $prm['search'] == 'yes')
            {
                $prm['search'] = 1;
            }

            if($prm['search'] == 'unpublish' || $prm['search'] == 'inactive' || $prm['search'] == 'not accepted' || $prm['search'] == 'no')
            {
                $prm['search'] = 0;
            }

            if(preg_match($pattern, $prm['search']))
            {
                list($d,$m,$y) = explode("-",$prm['search']);
                $prm['search'] = $y.'-'.$m.'-'.$d;

            }

            if(preg_match($pattern2, $prm['search']))
            {
                list($m,$y) = explode("-",$prm['search']);
                $prm['search'] = $y.'-'.$m;

            }

            if(isset($prm['fields']) && ! empty($prm['fields']))
            {
                $i=0;
                foreach($prm['fields'] as $obj) {
                    if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
                    else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
                    $i++;
                }
                $where .= ')';
                $this->db->where($where, NULL, FALSE);
                unset($where);
            }
        }

        if(isset($prm['fields_2']) && $prm['fields_2'] != '')
        {
            $i=0;
            foreach($prm['fields_2'] as $key => $val) {
                if($val === 'publish' || $val === 'active' || $val === 'accepted' || $val === 'yes')
                {
                    $val = 1;
                }

                if($val === 'unpublish' || $val === 'inactive' || $val === 'not accepted' || $val === 'no')
                {
                    $val = 0;
                }


                if(preg_match($pattern, $val))
                {
                    list($d,$m,$y) = explode("-",$val);
                    $val = $y.'-'.$m.'-'.$d;
                }

                if(preg_match($pattern2, $val))
                {
                    list($m,$y) = explode("-",$val);
                    $val = $y.'-'.$m;

                }

                if($val === 'empty'|| $val === 'not empty'){

                    if($val === 'empty'){
                        if($i == 0) $where = '(upper('.$key.') = ""';
                        else $where .=  'AND upper('.$key.') = "" ';
                        $i++;
                    }else{
                        if($i == 0) $where = '(upper('.$key.') <> ""';
                        else $where .=  'AND upper('.$key.') <> "" ';
                        $i++;
                    }

                }else{
                    if($i == 0) $where = '(upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
                    else $where .=  'AND upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
                    $i++;
                }
            }
            $where .= ')';
            $this->db->where($where, NULL, FALSE);
            unset($where);
        }

        // default where
//        $this->db->where('a.is_active', 1);


        if(isset($prm['group_by']))
        {
            $this->db->group_by($prm['group_by']);
        }

        if(isset($prm['sort']) && isset($prm['dir']))
        {
            $this->db->order_by($prm['sort'],$prm['dir']);
        }

        if($is_count)
        {
            $this->db->select('COUNT(*) AS cnt');
        }
        else
        {
            if(isset($prm['select']))
            {
                $this->db->select($prm['select']);
            }
            else
            {
                $this->db->select('
					*
				');

            }

            if(isset($prm['limit']))
            {
                $prm['start'] = isset($prm['start']) ? $prm['start'] : 0;

                if($prm['limit'] > 0)
                {
                    $this->db->limit($prm['limit'], $prm['start']);
                }
                else
                {
                    if(isset($prm['cnt']) && $prm['cnt'] > 500)
                    {
                        $this->db->limit(500, $prm['start']);
                    }
                }
            }

        }

        return $this->db->get();
    }

    function get_data($prm = array(), $is_count = FALSE){
        $sub_query_form = '(SELECT
	`b`.id,
	`g`.`name` vendor_type_name,
	`b`.project_code,
	IF(f.sap_profit_center != "", `f`.`name`, `e`.`name`) project_name,
	`b`.unit_id,
	`b`.vendor_npwp,
	`b`.vendor_name,
	`b`.vendor_address,
	`b`.faktur_pajak_number,
	`b`.faktur_pajak_date,
	`b`.pajak_period,
	`b`.pajak_year,
	`b`.real_cost,
	`b`.fp_amount,
	`a`.`ntpn_number`,
	`a`.`ntpn_date`,
	`b`.berkas_number,
	`b`.barang_jasa_name,
	`b`.invoice_number,
	`b`.payment_method,
	`b`.vendor_code,
	`b`.pajak_type,
	`d`.`name` division_name,
	CASE 
		WHEN b.document_type_id = 6 OR b.document_type_id = 7 THEN b.contract_number
		WHEN b.document_type_id = 2 OR b.document_type_id = 3 THEN concat(REPLACE(trim(TRAILING \']\' FROM trim( LEADING \'[\' FROM b.spk_po_number ) ),\'"\', \'\' ), IF(REPLACE(trim(TRAILING \']\' FROM trim( LEADING \'[\' FROM b.spk_po_number_manual ) ),\'"\', \'\' ) = \'\' OR b.spk_po_number_manual IS NULL, \'\', \',\'), REPLACE(trim(TRAILING \']\' FROM trim( LEADING \'[\' FROM b.spk_po_number_manual ) ),\'"\', \'\' ) )
		ELSE
			IF(b.spk_po_number_manual IS NULL or b.spk_po_number_manual = \'\', REPLACE(trim(TRAILING \']\' FROM trim( LEADING \'[\' FROM b.spk_po_number ) ),\'"\', \'\' ), c.po_number)
		END as spk_po_number,
	CASE 
		WHEN b.document_type_id = 6 OR b.document_type_id = 7 THEN b.contract_date
		WHEN b.document_type_id = 2 OR b.document_type_id = 3 THEN concat(REPLACE(trim(TRAILING \']\' FROM trim( LEADING \'[\' FROM b.spk_po_date ) ),\'"\', \'\' ), IF(REPLACE(trim(TRAILING \']\' FROM trim( LEADING \'[\' FROM b.spk_po_date_manual ) ),\'"\', \'\' ) = \'\' OR b.spk_po_number_manual IS NULL, \'\', \',\'), REPLACE(trim(TRAILING \']\' FROM trim( LEADING \'[\' FROM b.spk_po_date_manual ) ),\'"\', \'\' ) )
		ELSE c.po_date
		END as spk_po_date,
	 CASE
        WHEN document_type_id = 1 AND vendor_type = \'ZV01\' THEN b.nomor_surat_jalan_terakhir
            WHEN document_type_id = 1 AND vendor_type = \'ZV02\' THEN b.nomor_bal_bast
            WHEN document_type_id = 2 THEN b.nomor_berita_acara_alat
            WHEN document_type_id = 3 THEN b.nomor_surat_jalan_terakhir
            WHEN document_type_id = 6 THEN b.nomor_bast_2
        ELSE ""
    END AS surat_jalan_lkp_number,	
    CASE
        WHEN document_type_id = 1 AND vendor_type = \'ZV01\' THEN b.nomor_surat_jalan_terakhir_date
            WHEN document_type_id = 1 AND vendor_type = \'ZV02\' THEN b.nomor_bal_bast_date
            WHEN document_type_id = 2 THEN b.nomor_berita_acara_alat_date
            WHEN document_type_id = 3 THEN b.nomor_surat_jalan_terakhir_date
            WHEN document_type_id = 6 THEN b.nomor_bast_2_date
        ELSE ""
    END AS surat_jalan_lkp_date
    FROM
	arus_barang a
	INNER JOIN berkas b ON a.berkas_id = b.id AND b.is_active = 1 
	LEFT JOIN (
			SELECT
				A.berkas_id,
				GROUP_CONCAT( A.po_number ) AS po_number,
				GROUP_CONCAT( A.po_date ) AS po_date
				FROM
					( SELECT berkas_id, GROUP_CONCAT( DISTINCT no_po_sap ) AS po_number, REPLACE(trim(TRAILING \']\' FROM trim( LEADING \'[\' FROM berkas.spk_po_date ) ),\'"\', \'\' ) as po_date FROM berkas_pembayaran_items INNER JOIN berkas on berkas.id = berkas_pembayaran_items.berkas_id GROUP BY berkas_id UNION ALL SELECT berkas_id, GROUP_CONCAT( DISTINCT no_po_pg ), GROUP_CONCAT(po_date_pg) FROM berkas_pembayaran_items_pg GROUP BY berkas_id ) AS A 
				GROUP BY A.berkas_id 
	) AS c ON b.id = c.berkas_id 
	INNER JOIN `ref_cabang` d ON `d`.`id` = `b`.`unit_id`
	LEFT JOIN `ref_proyek` e ON `b`.`project_code` = `e`.`id`
	LEFT JOIN `ref_proyek` f ON `b`.`project_code` = `f`.`sap_profit_center`
	INNER JOIN `ref_vendor_type` g ON `b`.`vendor_type` = `g`.`code` 
		) as a';

        $this->db->_protect_identifiers=false;
        $this->db->from($sub_query_form);
        if(isset($prm['join']))
        {
            foreach($prm['join'] as $join)
            {
                if(isset($join['type']))
                {
                    $this->db->join($join['table'], $join['on'], $join['type']);
                }
                else
                {
                    $this->db->join($join['table'], $join['on']);
                }
            }
        }

        if(isset($prm['where_string']))
        {
            $this->db->where($prm['where_string']);
        }

        if(isset($prm['where']))
        {
            $this->db->where($prm['where'], FALSE, FALSE);
        }

        if(isset($prm['where_str']))
        {
            $this->db->where($prm['where_str']);
        }

        if(isset($prm['where_in']))
        {
            $this->db->where_in($prm['where_in'][0],$prm['where_in'][1]);
        }

        if(isset($prm['where_not_in']))
        {
            $this->db->where_not_in($prm['where_not_in'][0],$prm['where_not_in'][1]);
        }

        $pattern = "((0[1-9]|[12][0-9]|3[01])[-](0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d)";
        $pattern2 = "((0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d)";

        if(isset($prm['search']) && $prm['search'] != '')
        {
            if($prm['search'] == 'publish' || $prm['search'] == 'active' || $prm['search'] == 'accepted' || $prm['search'] == 'yes')
            {
                $prm['search'] = 1;
            }

            if($prm['search'] == 'unpublish' || $prm['search'] == 'inactive' || $prm['search'] == 'not accepted' || $prm['search'] == 'no')
            {
                $prm['search'] = 0;
            }

            if(preg_match($pattern, $prm['search']))
            {
                list($d,$m,$y) = explode("-",$prm['search']);
                $prm['search'] = $y.'-'.$m.'-'.$d;

            }

            if(preg_match($pattern2, $prm['search']))
            {
                list($m,$y) = explode("-",$prm['search']);
                $prm['search'] = $y.'-'.$m;

            }

            if(isset($prm['fields']) && ! empty($prm['fields']))
            {
                $i=0;
                foreach($prm['fields'] as $obj) {
                    if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
                    else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
                    $i++;
                }
                $where .= ')';
                $this->db->where($where, NULL, FALSE);
                unset($where);
            }
        }

        if(isset($prm['fields_2']) && $prm['fields_2'] != '')
        {
            $i=0;
            foreach($prm['fields_2'] as $key => $val) {
                if($val === 'publish' || $val === 'active' || $val === 'accepted' || $val === 'yes')
                {
                    $val = 1;
                }

                if($val === 'unpublish' || $val === 'inactive' || $val === 'not accepted' || $val === 'no')
                {
                    $val = 0;
                }


                if(preg_match($pattern, $val))
                {
                    list($d,$m,$y) = explode("-",$val);
                    $val = $y.'-'.$m.'-'.$d;
                }

                if(preg_match($pattern2, $val))
                {
                    list($m,$y) = explode("-",$val);
                    $val = $y.'-'.$m;

                }

                if($val === 'empty'|| $val === 'not empty'){

                    if($val === 'empty'){
                        if($i == 0) $where = '(upper('.$key.') = ""';
                        else $where .=  'AND upper('.$key.') = "" ';
                        $i++;
                    }else{
                        if($i == 0) $where = '(upper('.$key.') <> ""';
                        else $where .=  'AND upper('.$key.') <> "" ';
                        $i++;
                    }

                }else{
                    if($i == 0) $where = '(upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
                    else $where .=  'AND upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
                    $i++;
                }
            }
            $where .= ')';
            $this->db->where($where, NULL, FALSE);
            unset($where);
        }

        // default where
//        $this->db->where('a.is_active', 1);


        if(isset($prm['group_by']))
        {
            $this->db->group_by($prm['group_by']);
        }

        if(isset($prm['sort']) && isset($prm['dir']))
        {
            $this->db->order_by($prm['sort'],$prm['dir']);
        }

        if($is_count)
        {
            $this->db->select('COUNT(*) AS cnt');
        }
        else
        {
            if(isset($prm['select']))
            {
                $this->db->select($prm['select']);
            }
            else
            {
                $this->db->select('
					*
				');

            }

            if(isset($prm['limit']))
            {
                $prm['start'] = isset($prm['start']) ? $prm['start'] : 0;

                if($prm['limit'] > 0)
                {
                    $this->db->limit($prm['limit'], $prm['start']);
                }
                else
                {
                    if(isset($prm['cnt']) && $prm['cnt'] > 500)
                    {
                        $this->db->limit(500, $prm['start']);
                    }
                }
            }

        }

        return $this->db->get();
    }

    function get_data_research($prm = array(), $is_count = FALSE){
        $sub_query_form = '(SELECT
	`b`.id,
	`g`.`name` vendor_type_name,
	`b`.project_code,
	`e`.`name` project_name,
	`b`.vendor_npwp,
	`b`.vendor_name,
	`b`.vendor_address,
	`b`.faktur_pajak_number,
	`b`.faktur_pajak_date,
	`b`.pajak_period,
	`b`.pajak_year,
	`b`.real_cost,
	`b`.dpp_amount,
	`b`.fp_amount,
	`a`.`ntpn_number`,
	`a`.`ntpn_date`,
	`b`.berkas_number,
	`b`.barang_jasa_name,
	`b`.invoice_number,
	`b`.payment_method,
	`b`.vendor_code,
	`b`.pajak_type,
	`b`.vendor_bank_name,
	`b`.vendor_bank_rek,
	`b`.vendor_bank_account,
	`b`.spk_po_number,
	`b`.spk_po_date,	
	`b`.surat_jalan_lkp_number,	
    `b`.surat_jalan_lkp_date
    FROM
	arus_barang a
	INNER JOIN berkas b ON a.berkas_id = b.id AND b.is_active = 1 
	LEFT JOIN `ref_proyek` e ON `b`.`project_code` = `e`.`id`
	INNER JOIN `ref_vendor_type` g ON `b`.`vendor_type` = `g`.`name` 
		) as a';

        $this->db->_protect_identifiers=false;
        $this->db->from($sub_query_form);
        if(isset($prm['join']))
        {
            foreach($prm['join'] as $join)
            {
                if(isset($join['type']))
                {
                    $this->db->join($join['table'], $join['on'], $join['type']);
                }
                else
                {
                    $this->db->join($join['table'], $join['on']);
                }
            }
        }

        if(isset($prm['where_string']))
        {
            $this->db->where($prm['where_string']);
        }

        if(isset($prm['where']))
        {
            $this->db->where($prm['where'], FALSE, FALSE);
        }

        if(isset($prm['where_str']))
        {
            $this->db->where($prm['where_str']);
        }

        if(isset($prm['where_in']))
        {
            $this->db->where_in($prm['where_in'][0],$prm['where_in'][1]);
        }

        if(isset($prm['where_not_in']))
        {
            $this->db->where_not_in($prm['where_not_in'][0],$prm['where_not_in'][1]);
        }

        $pattern = "((0[1-9]|[12][0-9]|3[01])[-](0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d)";
        $pattern2 = "((0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d)";

        if(isset($prm['search']) && $prm['search'] != '')
        {
            if($prm['search'] == 'publish' || $prm['search'] == 'active' || $prm['search'] == 'accepted' || $prm['search'] == 'yes')
            {
                $prm['search'] = 1;
            }

            if($prm['search'] == 'unpublish' || $prm['search'] == 'inactive' || $prm['search'] == 'not accepted' || $prm['search'] == 'no')
            {
                $prm['search'] = 0;
            }

            if(preg_match($pattern, $prm['search']))
            {
                list($d,$m,$y) = explode("-",$prm['search']);
                $prm['search'] = $y.'-'.$m.'-'.$d;

            }

            if(preg_match($pattern2, $prm['search']))
            {
                list($m,$y) = explode("-",$prm['search']);
                $prm['search'] = $y.'-'.$m;

            }

            if(isset($prm['fields']) && ! empty($prm['fields']))
            {
                $i=0;
                foreach($prm['fields'] as $obj) {
                    if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
                    else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
                    $i++;
                }
                $where .= ')';
                $this->db->where($where, NULL, FALSE);
                unset($where);
            }
        }

        if(isset($prm['fields_2']) && $prm['fields_2'] != '')
        {
            $i=0;
            foreach($prm['fields_2'] as $key => $val) {
                if($val === 'publish' || $val === 'active' || $val === 'accepted' || $val === 'yes')
                {
                    $val = 1;
                }

                if($val === 'unpublish' || $val === 'inactive' || $val === 'not accepted' || $val === 'no')
                {
                    $val = 0;
                }


                if(preg_match($pattern, $val))
                {
                    list($d,$m,$y) = explode("-",$val);
                    $val = $y.'-'.$m.'-'.$d;
                }

                if(preg_match($pattern2, $val))
                {
                    list($m,$y) = explode("-",$val);
                    $val = $y.'-'.$m;

                }

                if($val === 'empty'|| $val === 'not empty'){

                    if($val === 'empty'){
                        if($i == 0) $where = '(upper('.$key.') = ""';
                        else $where .=  'AND upper('.$key.') = "" ';
                        $i++;
                    }else{
                        if($i == 0) $where = '(upper('.$key.') <> ""';
                        else $where .=  'AND upper('.$key.') <> "" ';
                        $i++;
                    }

                }else{
                    if($i == 0) $where = '(upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
                    else $where .=  'AND upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
                    $i++;
                }
            }
            $where .= ')';
            $this->db->where($where, NULL, FALSE);
            unset($where);
        }

        // default where
//        $this->db->where('a.is_active', 1);


        if(isset($prm['group_by']))
        {
            $this->db->group_by($prm['group_by']);
        }

        if(isset($prm['sort']) && isset($prm['dir']))
        {
            $this->db->order_by($prm['sort'],$prm['dir']);
        }

        if($is_count)
        {
            $this->db->select('COUNT(*) AS cnt');
        }
        else
        {
            if(isset($prm['select']))
            {
                $this->db->select($prm['select']);
            }
            else
            {
                $this->db->select('
					*
				');

            }

            if(isset($prm['limit']))
            {
                $prm['start'] = isset($prm['start']) ? $prm['start'] : 0;

                if($prm['limit'] > 0)
                {
                    $this->db->limit($prm['limit'], $prm['start']);
                }
                else
                {
                    if(isset($prm['cnt']) && $prm['cnt'] > 500)
                    {
                        $this->db->limit(500, $prm['start']);
                    }
                }
            }

        }

        return $this->db->get();
    }

    function get_all_berkas($start_date = "", $end_date = "", $faktur_start = "", $faktur_end = "", $finish_start = "", $finish_end = "", $berkas_status = "", $division = ""){
        $arr_bind = array();
        $sql = 'SELECT
                    `b`.created_datetime,
                    `b`.last_approval_datetime,
                    `b`.id,
                    `b`.project_code,
                    `b`.berkas_status,
                    `b`.berkas_number,
                    `b`.faktur_pajak_number,
                    `b`.faktur_pajak_date,
                    `b`.vendor_code,
                    `b`.fp_amount,
                    `b`.berkas_status,
                    `b`.pajak_period,
                    `b`.pajak_year,
                    `b`.vendor_npwp,
                    `b`.pajak_period,
                    `b`.alamat_penjual,
                    `b`.nama_penjual,
                    `b`.dpp_amount,
                    `b`.ppnbm_amount
                FROM
                    ( `arus_barang` a )
                    INNER JOIN `berkas` b ON `a`.`berkas_id` = `b`.`id` 
                    AND b.is_active = 1
                    WHERE 1';

        if(!empty($start_date) && !empty($end_date)){
            $sql .= ' AND DATE(`b`.created_datetime) BETWEEN ? AND ?';
            array_push($arr_bind, $start_date, $end_date);

        }

        if(!empty($faktur_start) && !empty($faktur_end)){
            $sql .= ' AND `b`.faktur_pajak_date BETWEEN ? AND ?';
            array_push($arr_bind, $faktur_start, $faktur_end);
        }

        if(!empty($berkas_status)){
            if($berkas_status == 'selesai'){
                $sql .= ' AND `b`.berkas_status = "Selesai"';
            }else{
                $sql .= ' AND (`b`.berkas_status <> "Selesai" OR `b`.berkas_status IS NULL)';
            }
        }

        if(!empty($finish_start) && !empty($finish_end)){
            $sql .= ' AND DATE(`b`.last_approval_datetime) BETWEEN ? AND ? AND `b`.last_verify_level = "TO" AND `b`.last_verify_status = "Disetujui" ';
            array_push($arr_bind, $finish_start, $finish_end);
        }

        if(!empty($division)){
            $sql .= ' AND `b`.unit_id = ? ';
            array_push($arr_bind, $division);
        }

        $results =  array();

        if($query = $this->db->query($sql, $arr_bind)){

            if($query->num_rows() > 0){
                return $query->result_array();
            }
        }

        return $results;
    }

}