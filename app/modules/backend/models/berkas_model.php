<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berkas_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'berkas';
	}
	
	function get_data($prm,$is_count = FALSE)
	{
		$this->db->from("{$this->table} a");
        $this->db->join('ref_proyek b','a.project_code = b.id','left');
		
		if(isset($prm['join']))
		{
			foreach($prm['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		if(isset($prm['where']))
		{
			$this->db->where($prm['where'], FALSE, FALSE);
		}
		
		if(isset($prm['where_str']))
		{
			$this->db->where($prm['where_str']);
		}
		
		if(isset($prm['where_in']))
		{
			$this->db->where_in($prm['where_in'][0],$prm['where_in'][1]);
		}
		
		if(isset($prm['where_not_in']))
		{
			$this->db->where_not_in($prm['where_not_in'][0],$prm['where_not_in'][1]);
		}
		
		$pattern = "((0[1-9]|[12][0-9]|3[01])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d)";
		$pattern2 = "((0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d)";
		
		if(isset($prm['search']) && $prm['search'] != '')
        {
			if($prm['search'] == 'publish' || $prm['search'] == 'active' || $prm['search'] == 'accepted' || $prm['search'] == 'yes')
			{
				$prm['search'] = 1;
			}
			
			if($prm['search'] == 'unpublish' || $prm['search'] == 'inactive' || $prm['search'] == 'not accepted' || $prm['search'] == 'no')
			{
				$prm['search'] = 0;
			}
			
			if(preg_match($pattern, $prm['search'])) 
			{
				$prm['search'] = $this->date_format_Ymd($prm['search'],'/');
			}
			
			if(preg_match($pattern2, $prm['search'])) 
			{
				list($m,$y) = explode("/",$prm['search']);
				$prm['search'] = $y.'-'.$m;
			}
			
			if(isset($prm['fields']) && ! empty($prm['fields']))
			{
				$i=0;
				foreach($prm['fields'] as $obj) { 
					if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
					else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
					$i++;
				}
				$where .= ')';
				$this->db->where($where, NULL, FALSE);
				unset($where);
			}
        }
		
		if(isset($prm['fields_2']) && $prm['fields_2'] != '')
		{
			$i=0;
			foreach($prm['fields_2'] as $key => $val) { 
				if($val === 'publish' || $val === 'active' || $val === 'accepted' || $val === 'yes')
				{
					$val = 1;
				}
				
				if($val === 'unpublish' || $val === 'inactive' || $val === 'not accepted' || $val === 'no')
				{
					$val = 0;
				}
				
				
				if(preg_match($pattern, $val)) 
				{
					$val = $this->date_format_Ymd($val,'/');
				}
				
				
				if(preg_match($pattern2, $val)) 
				{
					list($m,$y) = explode("/",$val);
					$val = $y.'-'.$m;
				}
				
				if($i == 0) $where = '(upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
				else $where .=  'AND upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
				$i++;
			}
			$where .= ')';
			$this->db->where($where, NULL, FALSE);
			unset($where);
		}
		
		// default where
		$this->db->where('is_active', 1);
		
		// filter by proyek
		$proyeks = $this->array_proyeks;
		if(! empty($proyeks))
		{
			$this->db->where_in("project_code",$proyeks);
		}
		
		if(isset($prm['group_by']))
		{
			$this->db->group_by($prm['group_by']);
		}

		if(isset($prm['sort']) && isset($prm['dir']))
		{
			$this->db->order_by($prm['sort'],$prm['dir']);
		}
        
		if($is_count)
		{
			$this->db->select('COUNT(*) AS cnt');
		}
		else
		{
			if(isset($prm['select']))
			{
				$this->db->select($prm['select']);
			}
			else
			{
				$this->db->select('
					a.*,
					b.name project_name, b.lokasi1 project_location
				');
		
			}
			
			if(isset($prm['limit']))
			{
				$prm['start'] = isset($prm['start']) ? $prm['start'] : 0;
				
				if($prm['limit'] > 0)
				{
					$this->db->limit($prm['limit'], $prm['start']);
				}
				else
				{
					if(isset($prm['cnt']) && $prm['cnt'] > 500)
					{
						$this->db->limit(500, $prm['start']);
					}
				}
			}
			
		}
	
		return $this->db->get();
	}
	
	function get_pembayaran($param = array())
	{
		$this->db->select('
			a.termin, a.nominal, a.note, a.payment_date, a.id pembayaran_id,
			b.*
		');
		
		$this->db->from("{$this->table}_pembayaran a");
		$this->db->join("{$this->table} b",'b.id = a.berkas_id','inner');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if(isset($param['where_str']))
		{
			$this->db->where($param['where_str']);
		}
		
		if(isset($param['where_in']))
		{
			$this->db->where_in($param['where_in'][0],$param['where_in'][1]);
		}
		
		if(isset($param['limit']))
		{
			$start = isset($param['start']) ? $param['start'] : 0;
			$this->db->limit($param['limit'], $start);
		}
		
		if(isset($param['order_sort']))
		{
			$dir = (isset($param['order_dir']) ? $param['order_dir'] : 'ASC');
			$this->db->order_by($param['order_sort'], $dir);
		}
		
		return $this->db->get();
	}

	function get_total_pembayaran_hutang($param = array())
	{
		$this->db->select('
			netto_cost_val, ppn_val, pph_val , sum(nominal) total_pembayaran, ((netto_cost_val+ppn_val+pph_val+pph_val)-(IF(sum(nominal) is NULL,0,sum(nominal)))) total_hutang_valid
		',FALSE, FALSE);
		
		$this->db->from("{$this->table} a");
		$this->db->join("{$this->table}_pembayaran b",'a.id = b.berkas_id','left');
        
		$this->db->where('a.is_active', 1);
		$this->db->where('a.berkas_status', $this->ref_berkas_status[2]);
		
		// filter by proyek
		$proyeks = $this->array_proyeks;
		if(! empty($proyeks))
		{
			$this->db->where_in("project_code",$proyeks);
		}
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if(isset($param['where_in']))
		{
			$this->db->where_in($param['where_in'][0],$param['where_in'][1]);
		}
		
		if(isset($param['where_str']))
		{
			$this->db->where($param['where_str']);
		}
		
		$this->db->group_by('a.id');
		
		$res = $this->db->get()->result_array();
		
		$total_pembayaran = $total_hutang_valid = 0;
		foreach($res as $val)
		{
			$total_pembayaran = $total_pembayaran + $val['total_pembayaran'];
			
			$total_hutang_valid = $total_hutang_valid + $val['total_hutang_valid'];
		}
		
		return array($total_pembayaran,$total_hutang_valid);
	}
	
	function get_berkas($param = array())
	{
		$this->db->select('
			a.*,
			b.name project_name, b.lokasi1 project_location
		');
		
		$this->db->from("{$this->table} a");
        $this->db->join('ref_proyek b','a.project_code = b.id','left');
        
		$this->db->where('is_active', 1);
		
		// filter by proyek
		$proyeks = $this->array_proyeks;
		if(! empty($proyeks))
		{
			$this->db->where_in("project_code",$proyeks);
		}
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if(isset($param['where_in']))
		{
			$this->db->where_in($param['where_in'][0],$param['where_in'][1]);
		}
		
		if(isset($param['where_string']))
		{
			if(is_array($param['where_string']))
			{
				foreach($param['where_string'] as $string)
				{
					$this->db->where($string);
				}
			}
			else
			{
				$this->db->where($param['where_string']);
			}
		}
		
		if(isset($param['limit']))
		{
			$start = isset($param['start']) ? $param['start'] : 0;
			$this->db->limit($param['limit'], $start);
		}
		
		if(isset($param['group_by']))
		{
			$this->db->group_by($param['group_by']);
		}
		
		if(isset($param['order_sort']))
		{
			$dir = (isset($param['order_dir']) ? $param['order_dir'] : 'ASC');
			$this->db->order_by($param['order_sort'], $dir);
		}
		
		return $this->db->get();
	}
	
	function get_berkas_verify($param = array())
	{
		if(isset($param['select']))
		{
			$this->db->select($param['select']);
		}
			
		$this->db->from("{$this->table}_verify a");
		
		if(isset($param['join']))
		{
			foreach($param['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if(isset($param['where_in']))
		{
			$this->db->where_in($param['where_in'][0],$param['where_in'][1]);
		}
		
		if(isset($param['limit']))
		{
			$start = isset($param['start']) ? $param['start'] : 0;
			$this->db->limit($param['limit'], $start);
		}
		
		if(isset($param['order_sort']))
		{
			if(is_array($param['order_sort']))
			{
				foreach($param['order_sort'] as $order)
				{
					$this->db->order_by($order['sort'], (isset($order['dir']) ? $order['dir'] : 'ASC'));
				}
			}
			else
			{
				$dir = (isset($param['order_dir']) ? $param['order_dir'] : 'ASC');
				$this->db->order_by($param['order_sort'], $dir);
			}
		}
		
		return $this->db->get();
	}
	
	function get_berkas_files($param = array())
	{
		if(isset($param['select']))
		{
			$this->db->select($param['select']);
		}
			
		$this->db->from("{$this->table}_files a");
		
		if(isset($param['join']))
		{
			foreach($param['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}
		
		$this->db->where('file_path !=', '');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if(isset($param['where_in']))
		{
			$this->db->where_in($param['where_in'][0],$param['where_in'][1]);
		}
		
		if(isset($param['limit']))
		{
			$start = isset($param['start']) ? $param['start'] : 0;
			$this->db->limit($param['limit'], $start);
		}
		
		if(isset($param['order_sort']))
		{
			if(is_array($param['order_sort']))
			{
				foreach($param['order_sort'] as $order)
				{
					$this->db->order_by($order['sort'], (isset($order['dir']) ? $order['dir'] : 'ASC'));
				}
			}
			else
			{
				$dir = (isset($param['order_dir']) ? $param['order_dir'] : 'ASC');
				$this->db->order_by($param['order_sort'], $dir);
			}
		}
		
		return $this->db->get();
	}
	
	function delete_berkas($id)
	{
		$query = $this->db->query("
			DELETE {$this->table}, berkas_files FROM {$this->table}
			LEFT JOIN berkas_files ON ({$this->table}.id = berkas_files.berkas_id)
			WHERE {$this->table}.id in ({$id})
		");
		return $query;
	}
	
	function log_berkas($data)
	{
		return $this->db->insert('berkas_log', $data);
	}
	
	function get_berkas_vendor_type($param = array())
	{
		$this->db->select('
			b.name vendor_type_name, b.id vendor_type_id,
			c.name berkas_type_name, c.id berkas_type_id
		');
		
		$this->db->from("ref_berkas_vendor_type a");
        $this->db->join('ref_vendor_type b','a.ref_vendor_type_id = b.id','inner');
		$this->db->join('ref_berkas_type c','a.ref_berkas_type_id = c.id','inner');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if(isset($param['where_in']))
		{
			$this->db->where_in($param['where_in'][0],$param['where_in'][1]);
		}
		
		if(isset($param['limit']))
		{
			$start = isset($param['start']) ? $param['start'] : 0;
			$this->db->limit($param['limit'], $start);
		}
		
		if(isset($param['order_sort']))
		{
			$dir = (isset($param['order_dir']) ? $param['order_dir'] : 'ASC');
			$this->db->order_by($param['order_sort'], $dir);
		}
		
		return $this->db->get();
	}
	
	function delete_berkas_type($id)
	{
		$query = $this->db->query("
			DELETE ref_berkas_type, ref_berkas_vendor_type FROM ref_berkas_type
			LEFT JOIN ref_berkas_vendor_type ON (ref_berkas_type.id = ref_berkas_vendor_type.ref_berkas_type_id)
			WHERE ref_berkas_type.id in ({$id})
		");
		return $query;
	}

	function report_berkas_valid($prm,$is_count = FALSE)
	{
		$this->db->from("{$this->table} a");
		$this->db->join('ref_proyek b','a.project_code = b.id','left');

		if(isset($prm['join']))
		{
			foreach($prm['join'] as $join)
			{
				if(isset($join['type']))
				{
					$this->db->join($join['table'], $join['on'], $join['type']);
				}
				else
				{
					$this->db->join($join['table'], $join['on']);
				}
			}
		}

		if(isset($prm['where']))
		{
			$this->db->where($prm['where'], FALSE, FALSE);
		}

		if(isset($prm['where_str']))
		{
			$this->db->where($prm['where_str']);
		}

		if(isset($prm['where_in']))
		{
			$this->db->where_in($prm['where_in'][0],$prm['where_in'][1]);
		}

		if(isset($prm['where_not_in']))
		{
			$this->db->where_not_in($prm['where_not_in'][0],$prm['where_not_in'][1]);
		}

		$pattern = "((0[1-9]|[12][0-9]|3[01])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d)";
		$pattern2 = "((0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d)";

		if(isset($prm['search']) && $prm['search'] != '')
		{
			if($prm['search'] == 'publish' || $prm['search'] == 'active' || $prm['search'] == 'accepted' || $prm['search'] == 'yes')
			{
				$prm['search'] = 1;
			}

			if($prm['search'] == 'unpublish' || $prm['search'] == 'inactive' || $prm['search'] == 'not accepted' || $prm['search'] == 'no')
			{
				$prm['search'] = 0;
			}

			if(preg_match($pattern, $prm['search']))
			{
				$prm['search'] = $this->date_format_Ymd($prm['search'],'/');
			}

			if(preg_match($pattern2, $prm['search']))
			{
				list($m,$y) = explode("/",$prm['search']);
				$prm['search'] = $y.'-'.$m;
			}

			if(isset($prm['fields']) && ! empty($prm['fields']))
			{
				$i=0;
				foreach($prm['fields'] as $obj) {
					if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
					else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($prm['search']). '%\' ';
					$i++;
				}
				$where .= ')';
				$this->db->where($where, NULL, FALSE);
				unset($where);
			}
		}

		if(isset($prm['fields_2']) && $prm['fields_2'] != '')
		{
			$i=0;
			foreach($prm['fields_2'] as $key => $val) {
				if($val === 'publish' || $val === 'active' || $val === 'accepted' || $val === 'yes')
				{
					$val = 1;
				}

				if($val === 'unpublish' || $val === 'inactive' || $val === 'not accepted' || $val === 'no')
				{
					$val = 0;
				}


				if(preg_match($pattern, $val))
				{
					$val = $this->date_format_Ymd($val,'/');
				}


				if(preg_match($pattern2, $val))
				{
					list($m,$y) = explode("/",$val);
					$val = $y.'-'.$m;
				}

				if($i == 0) $where = '(upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
				else $where .=  'AND upper('.$key.') LIKE \'%' .strtoupper($val). '%\' ';
				$i++;
			}
			$where .= ')';
			$this->db->where($where, NULL, FALSE);
			unset($where);
		}

		// default where
		$this->db->where('is_active', 1);

		// filter by proyek
		$proyeks = $this->array_proyeks;
		if(! empty($proyeks))
		{
			$this->db->where_in("project_code",$proyeks);
		}

		if(isset($prm['group_by']))
		{
			$this->db->group_by($prm['group_by']);
		}

		if(isset($prm['sort']) && isset($prm['dir']))
		{
			$this->db->order_by($prm['sort'],$prm['dir']);
		}

		if($is_count)
		{
			$this->db->select('COUNT(*) AS cnt');
		}
		else
		{
			if(isset($prm['select']))
			{
				$this->db->select($prm['select']);
			}
			else
			{
				$this->db->select('
					a.*,
					b.name project_name, b.lokasi1 project_location
				');

			}

			if(isset($prm['limit']))
			{
				$prm['start'] = isset($prm['start']) ? $prm['start'] : 0;

				if($prm['limit'] > 0)
				{
					$this->db->limit($prm['limit'], $prm['start']);
				}
				else
				{
					if(isset($prm['cnt']) && $prm['cnt'] > 500)
					{
						$this->db->limit(500, $prm['start']);
					}
				}
			}

		}

		return $this->db->get();
	}

	function recap_berkas($year, $month, $prm = array())
	{
		$binding = array();

		$sql = 'SELECT a.*, b.name project_name, b.lokasi1 project_location
				FROM (berkas a)
				LEFT JOIN ref_proyek b ON a.project_code = b.id
				WHERE YEAR(last_approval_datetime) = ? ';
		$binding[] = $year;

		if($month){
			$sql .= 'AND MONTH(last_approval_datetime) = ?';
			$binding[] = $month;
		}

		if(isset($prm['where'])){
			foreach ($prm['where'] as $key => $param){
				$sql .=' AND '.$key. ' = ?';
				$binding[] = $param;
			}
		}

		$sql .=	' AND is_active = 1';
		$sql .= ' AND (is_hard_copy = 1 OR hard_copy_arrived IS NOT NULL)';


		if(isset($prm['limit']))
		{
			$prm['start'] = isset($prm['start']) ? $prm['start'] : 0;
			$sql .= ' LIMIT '.$prm['start'].', '. $prm['limit'];
		}

		$query = $this->db->query($sql, $binding);
		if($query){
			return $query->result_array();
		}

		return array();

	}

	function recap_berkas_count($year, $month, $prm = array())
	{
		$binding = array();

		$sql = 'SELECT a.*, b.name project_name, b.lokasi1 project_location
				FROM (berkas a)
				LEFT JOIN ref_proyek b ON a.project_code = b.id
				WHERE YEAR(last_approval_datetime) = ? ';
		$binding[] = $year;

		if($month){
			$sql .= 'AND MONTH(last_approval_datetime) = ?';
			$binding[] = $month;
		}

		if(isset($prm['where'])){
			foreach ($prm['where'] as $key => $param){
				$sql .=' AND '.$key. ' = ?';
				$binding[] = $param;
			}
		}

		$sql .=	' AND is_active = 1';
		$sql .= ' AND (is_hard_copy = 1 OR hard_copy_arrived IS NOT NULL)';

		$query = $this->db->query($sql, $binding);
		if($query){
			return $query->num_rows();
		}

		return 0;
	}

}