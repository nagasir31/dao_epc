<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get_data($param,$is_count = FALSE)
	{
		$this->db->from('employee b');
        $this->db->join('sys_user a','a.employee_id = b.id','inner');
		$this->db->join('ref_cabang c','b.ref_cabang_id = c.id','left');
		$this->db->join('sys_group_child d','d.id = a.sys_group_child_id','left');
		
		$this->db->where('a.is_deleted', 0);
		$this->db->where('a.is_active', 1);
		//$this->db->where('b.divisi !=', '');
		//$this->db->where_not_in('b.ref_status_id', array('4','6','8','9','10'));
		
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}

		if(isset($param['search']) && $param['search'] != "")
        {
        	$i=0;
        	foreach($param['fields'] as $obj) {
			  if($obj == 'user_id') $obj = 'a.id';
			  if($obj == 'user_name') $obj = 'a.name';
			  if($obj == 'user_is_active') $obj = 'a.is_active';
			  if($obj == 'nrp') $obj = 'b.nrp';
			  if($obj == 'name') $obj = 'b.name';
			  if($obj == 'divisi') $obj = 'b.divisi';
			  if($obj == 'cabang') $obj = 'c.name';
			  if($obj == 'sys_group_child_name') $obj = 'd.name';
			  if($obj == 'sys_group_child_desc') $obj = 'd.description';
	
			  if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  $i++;
			}
            $where .= ')';
            $this->db->where($where, NULL, FALSE);
            unset($where);
        }
		
		$this->db->order_by('a.created_datetime','DESC');
		$this->db->order_by('c.name','ASC');
		
		if(isset($param['sort']) && isset($param['dir']))
		{
			$this->db->order_by($param['sort'],$param['dir']);
		}
        
		if($is_count)
		{
			$this->db->select('COUNT(*) AS cnt');
		}
		else
		{
			$this->db->select('
				a.id user_id, a.name user_name, a.last_login last_login, a.is_active user_is_active, a.sys_group_child_id, a.employee_id,
				b.nrp nrp, b.name, b.divisi, b.email,
				c.name cabang,
				d.name sys_group_child_name, d.description sys_group_child_desc
			');
			
			if(isset($param['limit']))
			{
				$start = isset($param['start']) ? $param['start'] : 0;
				$this->db->limit($param['limit'], $start);
			}
		}
		
		return $this->db->get();
	}
	
	function get_data_2($param,$is_count = FALSE)
	{
		$this->db->from('sys_user a');
        $this->db->join('employee b','a.employee_id = b.id','left');
		
		$this->db->where('a.is_deleted', 0);
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}

		if(isset($param['search']) && $param['search'] != "")
        {
        	$i=0;
        	foreach($param['fields'] as $obj) {
			  if($obj == 'user_id') $obj = 'a.id';
			  if($obj == 'user_name') $obj = 'a.name';
			  if($obj == 'user_is_active') $obj = 'a.is_active';
			  if($obj == 'nrp') $obj = 'b.nrp';
			  if($obj == 'name') $obj = 'b.name';
			  if($obj == 'divisi') $obj = 'b.divisi';
	
			  if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  $i++;
			}
            $where .= ')';
            $this->db->where($where, NULL, FALSE);
            unset($where);
        }
		
		$this->db->order_by('a.created_datetime','DESC');
		
		if(isset($param['sort']) && isset($param['dir']))
		{
			$this->db->order_by($param['sort'],$param['dir']);
		}
        
		if($is_count)
		{
			$this->db->select('COUNT(*) AS cnt');
		}
		else
		{
			$this->db->select('
				a.id user_id, a.name user_name, a.last_login last_login, a.is_active user_is_active, a.sys_group_child_id, a.employee_id,
				b.nrp nrp, b.name, b.divisi, b.email
			');
			
			if(isset($param['limit']) && isset($param['start']))
			{
				$this->db->limit($param['limit'], $param['start']);
			}
		}
		
		return $this->db->get();
	}
	
	function get_data_employee($param,$is_count = FALSE)
	{
		$this->db->from('employee a');
		$this->db->where('a.divisi !=', '');
		$this->db->where_not_in('a.ref_status_id', array('6','8'));
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}

		if( ! empty($param['search']))
        {
        	$i=0;
        	foreach($param['fields'] as $obj) {
			  if($i == 0) $where = '(upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  else $where .=  'OR upper('.$obj.') LIKE \'%' .strtoupper($param['search']). '%\' ';
			  $i++;
			}
            $where .= ')';
            $this->db->where($where, NULL, FALSE);
            unset($where);
        }
		
        $this->db->order_by($param['sort'],$param['dir']);
		
		if($is_count)
		{
			$this->db->select('COUNT(*) AS cnt');
		}
		else
		{
			$this->db->select('
				a.id, a.nrp, a.name
			');
			$this->db->limit($param['limit'], $param['start']);
		}
		
		return $this->db->get();
	}
	
	function get_user_employee($param)
	{
		$this->db->from('sys_user a');
        $this->db->join('employee b','a.employee_id = b.id','left');
		
		if(isset($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		$this->db->select('
			a.*,
			b.nrp, b.name employee_name, b.divisi, b.email
		');
		
		if(isset($param['limit']))
		{
			$start = isset($param['start']) ? $param['start'] : 0;
			$this->db->limit($param['limit'], $start);
		}
		
		return $this->db->get();
	}
	
}