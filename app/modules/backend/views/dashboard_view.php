<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<div class="span4 responsive">
				<div class="dashboard-stat grey">
					<div class="visual">
						<i class="icon-idr"></i>
					</div>
					<div class="details">
						<div class="number" style="font-size:22px;font-weight:bold;">
							Rp. <?php echo $total_hutang_valid_ncl; ?>
						</div>
						<div class="desc">                           
							<?php echo $this->lang->line('berkas_total_hutang_valid'); ?> <?php echo $this->ref_payment_method[1]; ?>
						</div>
					</div>   
					<?php
						$param = base64_encode("payment_method={$this->ref_payment_method[1]}");
					?>
					<a class="more" href="<?php echo (! empty($short_cut['berkas_list']) ? app_backend_url(trim($short_cut['berkas_list'][1]).'/index/'.$param) : "#"); ?>">
					<?php echo $this->lang->line('global_view_more'); ?> <i class="m-icon-swapright m-icon-white"></i>
					</a>	  
				</div>
			</div>
			
			<div class="span4 responsive">
				<div class="dashboard-stat red">
					<div class="visual">
						<i class="icon-idr"></i>
					</div>
					<div class="details">
						<div class="number" style="font-size:22px;font-weight:bold;">
							Rp. <?php echo $total_hutang_valid; ?>
						</div>
						<div class="desc">                           
							<?php echo $this->lang->line('berkas_total_hutang_valid'); ?>
						</div>
					</div>   
					<?php
						$param = base64_encode("payment_method={$this->ref_payment_method[2]}");
					?>
					<a class="more" href="<?php echo (! empty($short_cut['berkas_list']) ? app_backend_url(trim($short_cut['berkas_list'][1]).'/index/'.$param) : "#"); ?>">
					<?php echo $this->lang->line('global_view_more'); ?> <i class="m-icon-swapright m-icon-white"></i>
					</a>	  
				</div>
			</div>
			
			<div class="span4 responsive">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class="icon-check"></i>
					</div>
					<div class="details">
						<div class="number" style="font-size:22px;font-weight:bold;">
							<?php echo $total_berkas_selesai; ?>
						</div>
						<div class="desc">                           
							<?php echo $this->lang->line('berkas_total_status_selesai'); ?>
						</div>
					</div>
					<?php
						$param = base64_encode("berkas_status={$this->ref_berkas_status[2]}");
					?>
					<a class="more" href="<?php echo (! empty($short_cut['berkas_list']) ? app_backend_url(trim($short_cut['berkas_list'][1]).'/index/'.$param) : "#"); ?>">
					<?php echo $this->lang->line('global_view_more'); ?> <i class="m-icon-swapright m-icon-white"></i>
					</a>	           
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="span4 responsive">
				<div class="dashboard-stat purple">
					<div class="visual">
						<i class="icon-money"></i>
					</div>
					<div class="details">
						<div class="number" style="font-size:22px;font-weight:bold;">
							Rp. <?php echo $total_pembayaran_ncl; ?>
						</div>
						<div class="desc">                           
							<?php echo $this->lang->line('berkas_total_pembayaran'); ?> NCL
						</div>
					</div>  
					<?php
						$param = base64_encode("payment_method={$this->ref_payment_method[1]}");
					?>
					<a class="more" href="<?php echo (! empty($short_cut['berkas_list']) ? app_backend_url(trim($short_cut['berkas_list'][1]).'/index/'.$param) : "#"); ?>">
					<?php echo $this->lang->line('global_view_more'); ?> <i class="m-icon-swapright m-icon-white"></i>
					</a>	    				
				</div>
			</div>
			
			<div class="span4 responsive">
				<div class="dashboard-stat blue">
					<div class="visual">
						<i class="icon-money"></i>
					</div>
					<div class="details">
						<div class="number" style="font-size:22px;font-weight:bold;">
							Rp. <?php echo $total_pembayaran; ?>
						</div>
						<div class="desc">                           
							<?php echo $this->lang->line('berkas_total_pembayaran'); ?>
						</div>
					</div>  
					<?php
						$param = base64_encode("payment_method={$this->ref_payment_method[2]}");
					?>
					<a class="more" href="<?php echo (! empty($short_cut['berkas_list']) ? app_backend_url(trim($short_cut['berkas_list'][1]).'/index/'.$param) : "#"); ?>">
					<?php echo $this->lang->line('global_view_more'); ?> <i class="m-icon-swapright m-icon-white"></i>
					</a>	    				
				</div>
			</div>
			
			<div class="span4 responsive">
				<div class="dashboard-stat yellow">
					<div class="visual">
						<i class="icon-retweet"></i>
					</div>
					<div class="details">
						<div class="number" style="font-size:22px;font-weight:bold;">
							<?php echo $total_berkas_proses; ?>
						</div>
						<div class="desc">                           
							<?php echo $this->lang->line('berkas_total_status_proses'); ?>
						</div>
					</div>     
					<?php
						$param = base64_encode("berkas_status={$this->ref_berkas_status[1]}");
					?>
					<a class="more" href="<?php echo (! empty($short_cut['berkas_list']) ? app_backend_url(trim($short_cut['berkas_list'][1]).'/index/'.$param) : "#"); ?>">
					<?php echo $this->lang->line('global_view_more'); ?> <i class="m-icon-swapright m-icon-white"></i>
					</a>			
				</div>
			</div>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption"><i class="icon-cogs"></i><?php echo $this->lang->line('berkas_project_list'); ?></div>
					<div class="tools">
						<a class="config" href="javascript:;"></a>
						<a class="reload" href="javascript:;" id="#button-refresh"></a>
						<a class="collapse" href="javascript:;"></a>
					</div>
				</div>
				<div class="portlet-body">
					<table class="table table-bordered table-hover" id="table1">
						<thead>
							<tr>
								<th style="width:2%;"><?php echo $this->lang->line('global_no'); ?></th>
								<th style="width:5%;"><?php echo $this->lang->line('berkas_project_code'); ?></th>
								<th style="width:15%;" class="hidden-480"><?php echo $this->lang->line('berkas_project_name'); ?></th>
								<th style="width:10%;" class="hidden-480"><?php echo $this->lang->line('berkas_total_hutang_valid'); ?> ( Rp )</th>
								<th style="width:10%;" class="hidden-480"><?php echo $this->lang->line('berkas_total_pembayaran'); ?> ( Rp )</th>
								<th style="width:7%;text-align:center;" class="hidden-480 hidden-767 hidden-979"><?php echo $this->lang->line('berkas_total_status_selesai'); ?></th>
								<th style="width:7%;text-align:center;" class="hidden-480 hidden-767 hidden-979"><?php echo $this->lang->line('berkas_total_status_proses'); ?></th>
								<th style="width:7%;text-align:center;" class="hidden-480 hidden-767 hidden-979"><?php echo $this->lang->line('berkas_total_status_batal'); ?></th>
							</tr>
						</thead>
						<tbody>
							<td colspan="8" class="dataTables_empty"><?php echo $this->lang->line('global_loading_data'); ?></td>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<?php if(isset($stat_status_ncl['stat_status']) && ! empty($stat_status_ncl['stat_status'])): ?>
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption"><i class="icon-dashboard"></i><?php echo $this->lang->line('berkas_stat_status'); ?> - <?php echo $this->ref_payment_method[1]; ?></div>
					<div class="tools">
						<a href="javascript:;" class="collapse"></a>
					</div>
				</div>
				<div class="portlet-body no-more-tables">
					<table class="table-bordered table-striped table-hover table-condensed cf">
						<thead class="cf">
							<tr>
								<th rowspan="2" style="text-align: left;width:3%;"><?php echo $this->lang->line('global_no'); ?></th>
								<th rowspan="2" style="text-align: left;width:5%;"><?php echo $this->lang->line('berkas_project_code'); ?></th>
								<th rowspan="2" style="text-align: left;width:20%;"><?php echo $this->lang->line('berkas_project_name'); ?></th>
								<th rowspan="2">
									<div class="label label" title="<?php echo $this->lang->line('berkas_release'); ?>">                        
										<i class="icon-plus"></i>
									</div>
								</th>
								<?php foreach($stat_status_ncl['stat_head'] as $head): ?>
								<th colspan="3"><?php echo $head['verify_level']; ?></th>
								<?php endforeach; ?>
								<th rowspan="2">
									<div class="label label-info" title="<?php echo $this->ref_berkas_status[2]; ?>">                        
										<i class="icon-ok"></i>
									</div>
								</th>
							</tr>
							<tr>
								<?php foreach($stat_status_ncl['stat_head'] as $head): ?>
								<th>
									<div class="label label-warning" title="<?php echo $this->ref_verify_status[1]; ?>">                        
										<i class="icon-signin"></i>
									</div>
								</th>
								<th>
									<div class="label label-success" title="<?php echo $this->ref_verify_status[2]; ?>">                        
										<i class="icon-thumbs-up"></i>
									</div> 
								</th>
								<th>
									<div class="label label-important" title="<?php echo $this->ref_verify_status[3]; ?>">                        
										<i class="icon-thumbs-down"></i>
									</div> 
								</th>
								<?php endforeach; ?>
							</tr>
						</thead>
						<tbody>
							<?php $rnum=1;foreach($stat_status_ncl['stat_status'] as $dt): ?>
							<tr>
								<td data-title="<?php echo $this->lang->line('global_no'); ?>"><?php echo $rnum; ?></td>
								<td data-title="<?php echo $this->lang->line('berkas_project_code'); ?>"><a href="<?php echo app_backend_url('backend/berkas_proyek_detail/'.base64_encode($dt['project_code'])); ?>"><?php echo $dt['project_code']; ?></a></td>
								<td data-title="<?php echo $this->lang->line('berkas_project_name'); ?>"><?php echo $dt['project_name']; ?></td>

								<td data-title="<?php echo $this->lang->line('berkas_release'); ?>" class="center"><?php echo $dt['total_rilis']; ?></td>
								
								<?php foreach($dt['total_verify'] as $key => $val): ?>
									<?php foreach($val as $k => $v): ?>
									<td data-title="<?php echo $k; ?> - <?php echo $key; ?>" class="center"><?php echo $v; ?></td>
									<?php endforeach; ?>
								<?php endforeach; ?>
								
								<td data-title="<?php echo $this->ref_berkas_status[2]; ?>" class="center"><?php echo $dt['total_selesai']; ?></td>
							</tr>
							<?php $rnum++;endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" style="border-top:1px solid #ddd;text-align:right;font-weight:bold;"><?php echo $this->lang->line('global_total'); ?></td>
								<td style="border-top:1px solid #ddd;text-align:right;font-weight:bold;" class="center"><?php echo $stat_status_ncl['stat_sum_rilis']; ?></td>
								<?php foreach($stat_status_ncl['stat_sum_arr'] as $sum): ?>
								<td style="border-top:1px solid #ddd;text-align:right;font-weight:bold;" class="center"><?php echo $sum; ?></td>
								<?php endforeach; ?>
								<td style="border-top:1px solid #ddd;text-align:right;font-weight:bold;" class="center"><?php echo $stat_status_ncl['stat_sum_selesai']; ?></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<?php if(isset($stat_status_reg['stat_status']) && ! empty($stat_status_reg['stat_status'])): ?>
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption"><i class="icon-dashboard"></i><?php echo $this->lang->line('berkas_stat_status'); ?> - <?php echo $this->ref_payment_method[2]; ?></div>
					<div class="tools">
						<a href="javascript:;" class="collapse"></a>
					</div>
				</div>
				<div class="portlet-body no-more-tables">
					<table class="table-bordered table-striped table-hover table-condensed cf">
						<thead class="cf">
							<tr>
								<th rowspan="2" style="text-align: left;width:3%;"><?php echo $this->lang->line('global_no'); ?></th>
								<th rowspan="2" style="text-align: left;width:5%;"><?php echo $this->lang->line('berkas_project_code'); ?></th>
								<th rowspan="2" style="text-align: left;width:20%;"><?php echo $this->lang->line('berkas_project_name'); ?></th>
								<th rowspan="2">
									<div class="label label" title="<?php echo $this->lang->line('berkas_release'); ?>">                        
										<i class="icon-plus"></i>
									</div>
								</th>
								<?php foreach($stat_status_reg['stat_head'] as $head): ?>
								<th colspan="3"><?php echo $head['verify_level']; ?></th>
								<?php endforeach; ?>
								<th rowspan="2">
									<div class="label label-info" title="<?php echo $this->ref_berkas_status[2]; ?>">                        
										<i class="icon-ok"></i>
									</div>
								</th>
							</tr>
							<tr>
								<?php foreach($stat_status_reg['stat_head'] as $head): ?>
								<th>
									<div class="label label-warning" title="<?php echo $this->ref_verify_status[1]; ?>">                        
										<i class="icon-signin"></i>
									</div>
								</th>
								<th>
									<div class="label label-success" title="<?php echo $this->ref_verify_status[2]; ?>">                        
										<i class="icon-thumbs-up"></i>
									</div> 
								</th>
								<th>
									<div class="label label-important" title="<?php echo $this->ref_verify_status[3]; ?>">                        
										<i class="icon-thumbs-down"></i>
									</div> 
								</th>
								<?php endforeach; ?>
							</tr>
						</thead>
						<tbody>
							<?php $rnum=1;foreach($stat_status_reg['stat_status'] as $dt): ?>
							<tr>
								<td data-title="<?php echo $this->lang->line('global_no'); ?>"><?php echo $rnum; ?></td>
								<td data-title="<?php echo $this->lang->line('berkas_project_code'); ?>"><a href="<?php echo app_backend_url('backend/berkas_proyek_detail/'.base64_encode($dt['project_code'])); ?>"><?php echo $dt['project_code']; ?></a></td>
								<td data-title="<?php echo $this->lang->line('berkas_project_name'); ?>"><?php echo $dt['project_name']; ?></td>

								<td data-title="<?php echo $this->lang->line('berkas_release'); ?>" class="center"><?php echo $dt['total_rilis']; ?></td>
								
								<?php foreach($dt['total_verify'] as $key => $val): ?>
									<?php foreach($val as $k => $v): ?>
									<td data-title="<?php echo $k; ?> - <?php echo $key; ?>" class="center"><?php echo $v; ?></td>
									<?php endforeach; ?>
								<?php endforeach; ?>
								
								<td data-title="<?php echo $this->ref_berkas_status[2]; ?>" class="center"><?php echo $dt['total_selesai']; ?></td>
							</tr>
							<?php $rnum++;endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" style="border-top:1px solid #ddd;text-align:right;font-weight:bold;"><?php echo $this->lang->line('global_total'); ?></td>
								<td style="border-top:1px solid #ddd;text-align:right;font-weight:bold;" class="center"><?php echo $stat_status_reg['stat_sum_rilis']; ?></td>
								<?php foreach($stat_status_reg['stat_sum_arr'] as $sum): ?>
								<td style="border-top:1px solid #ddd;text-align:right;font-weight:bold;" class="center"><?php echo $sum; ?></td>
								<?php endforeach; ?>
								<td style="border-top:1px solid #ddd;text-align:right;font-weight:bold;" class="center"><?php echo $stat_status_reg['stat_sum_selesai']; ?></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
</div>

<script>
jQuery(document).ready(function(){
	// init grid
	var oTable = jQuery('#table1');
	App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
	oTable.dataTable({
		"sPaginationType": "bootstrap",
		"aaSorting": [],
		"aoColumnDefs": [
			{"bSortable": false, "aTargets": [0,3,4,5,6,7], "bSearchable": false, "aTargets": [0,3,4,5,6,7]}
		],
		"bFilter" : true,
		"bServerSide": true,
		"sAjaxSource": "<?php echo app_backend_url('backend/get_berkas_proyek'); ?>",
		"sServerMethod": "POST",
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { 
			ceki.fnSaveDtView(oSettings, oData); 
		},
		"fnStateLoad": function(oSettings) { 
			dataFN = JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname));
			return ceki.fnLoadDtView(oSettings); 
		},
		"oLanguage": {
			"sLengthMenu": "<?php echo $this->lang->line('global_DT_sLengthMenu'); ?>",
			"sZeroRecords": "<?php echo $this->lang->line('global_DT_sZeroRecords'); ?>",
			"sInfo": "<?php echo $this->lang->line('global_DT_sInfo'); ?>",
			"sInfoEmpty": "<?php echo $this->lang->line('global_DT_sInfoEmpty'); ?>",
			"sInfoFiltered": "(<?php echo $this->lang->line('global_DT_sInfoFiltered'); ?>)",
			"sSearch": "<?php echo $this->lang->line('global_search'); ?> :",
			"oPaginate": {
				"sPrevious": "<?php echo $this->lang->line('global_prev'); ?>",
				"sNext": "<?php echo $this->lang->line('global_next'); ?>"
			}
		},
		"fnServerParams": function (aoData) {
			aoData.push({"name": "is_ajax", "value": true});
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			jQuery.post(sSource, aoData, function(obj) {
				if(obj.success == false){
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: obj.message,
						sticky: false,
						time: 3000
					});
					
					if(obj.expired) {
						setTimeout(function(){window.location= '<?php echo app_backend_url('login'); ?>'},1000);
					}
				}
				else{
					fnCallback(obj)
				}
			}, 'json');
		},
		"fnDrawCallback": function(oSettings) {
			if(!jQuery(this).children('.blockUI').length){
				App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
			}
			
			App.initUniform();
			App.initPopovers();
			App.initTooltips();
			
			App.unblockUI(jQuery(this));
			
			oTable.floatThead({
				top: ceki.pageTop
			});
			
			jQuery('.blink').pulsate({
				color: "#bf1c56"
			});	
		},
		"aoColumns": [
			{ "mData": "rnum" },
			{ "mData": "project_code" },
			{ "mData": "project_name", "sClass": "hidden-480" },
			{ "mData": "total_hutang_valid", "sClass": "hidden-480" },
			{ "mData": "total_pembayaran", "sClass": "hidden-480" },
			{ "mData": "total_berkas_selesai", "sClass": "hidden-480 hidden-767 hidden-979" },
			{ "mData": "total_berkas_proses", "sClass": "hidden-480 hidden-767 hidden-979" },
			{ "mData": "total_berkas_batal", "sClass": "hidden-480 hidden-767 hidden-979" }
		],
		"aLengthMenu": [
			[5, 15, 20, 50, 100, -1],
			[5, 15, 20, 50, 100, "All"]
		],
		"iDisplayLength": 5,
		"sDom": "<'dataTables_toolbar'><'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"fnPreDrawCallback": function () { 
			if (jQuery('.dataTables_length select').val() == -1) {
				if(oTable.fnSettings().fnRecordsTotal() > 500) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_limit_record_page'); ?>',
						sticky: false,
						time: 3000
					});
					return false;
				}
			}				
		},
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		}
	});
	
	jQuery('#table1_wrapper .dataTables_filter input').addClass("m-wrap");
	jQuery('#table1_wrapper .dataTables_length select').addClass("m-wrap small"); 
	jQuery('#table1_wrapper .dataTables_length select').select2(); 
	
	jQuery('#table1_filter input').unbind();
	jQuery('#table1_filter input').bind('keyup', function(e) {
		if(e.keyCode == 13) {
			App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
			oTable.fnFilter(this.value);   
		}
	}); 
	
	jQuery('.reload').click(function(){
		App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
		oTable.fnDraw();
	});
	
	jQuery('.config').click(function(){
		ceki.fnResetDtView();
		ceki.fnRefresh();
	});
});
</script>