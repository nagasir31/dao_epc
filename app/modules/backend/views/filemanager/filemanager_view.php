<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="robots" content="noindex,nofollow">
    <title><?php echo $this->lang->line('global_filemanager'); ?></title>
	<link rel="shortcut icon" href="<?php echo app_asset_backend_url('responsiveFilemanager/img/ico/favicon.ico'); ?>">
    <link href="<?php echo app_asset_backend_url('responsiveFilemanager/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo app_asset_backend_url('responsiveFilemanager/css/bootstrap-responsive.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo app_asset_backend_url('responsiveFilemanager/css/bootstrap-lightbox.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo app_asset_backend_url('responsiveFilemanager/css/style.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo app_asset_backend_url('responsiveFilemanager/css/dropzone.min.css'); ?>" type="text/css" rel="stylesheet" />
	<link href="<?php echo app_asset_backend_url('responsiveFilemanager/css/jquery.contextMenu.min.css'); ?>" rel="stylesheet" type="text/css" />	
	<link href="<?php echo app_asset_backend_url('responsiveFilemanager/css/bootstrap-modal.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo app_asset_backend_url('responsiveFilemanager/jPlayer/skin/blue.monday/jplayer.blue.monday.css'); ?>" rel="stylesheet" type="text/css">
	<!--[if lt IE 8]><style>
	.img-container span, .img-container-mini span {
	    display: inline-block;
	    height: 100%;
	}
	</style><![endif]-->
	<script type="text/javascript" src="<?php echo base_asset_url('js/jquery/jquery-1.9.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/bootstrap-lightbox.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/dropzone.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/jquery.touchSwipe.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/modernizr.custom.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/bootbox.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/bootstrap-modal.min.js'); ?>"></script>   
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/bootstrap-modalmanager.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/jPlayer/jquery.jplayer.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/imagesloaded.pkgd.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/jquery.queryloader2.min.js'); ?>"></script>
	<!--script type="text/javascript" src="//dme0ih8comzn4.cloudfront.net/js/feather.js"></script-->
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
	<![endif]-->
	<script src="<?php echo app_asset_backend_url('responsiveFilemanager/js/jquery.ui.position.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo app_asset_backend_url('responsiveFilemanager/js/jquery.contextMenu.min.js'); ?>" type="text/javascript"></script>    
	
	<?php
		if(isset($js))
		{
			foreach($js as $key => $val)
			{
				echo $val."\n";
			}
		}
	?>
	<script type="text/javascript" src="<?php echo app_asset_backend_url('responsiveFilemanager/js/include.min.js'); ?>"></script>
</head>
<body>
	<input type="hidden" id="ok" value="<?php echo $this->lang->line('global_ok'); ?>" />
	<input type="hidden" id="cancel" value="<?php echo $this->lang->line('global_cancel'); ?>" />
	<input type="hidden" id="rename" value="<?php echo $this->lang->line('filemanager_rename'); ?>" />
	<input type="hidden" id="base_url" value="<?php echo app_asset_url(); ?>" />
	<input type="hidden" id="view" />
	<input type="hidden" id="insert_folder_name" value="<?php echo $this->lang->line('filemanager_create_folder'); ?>" />
	<input type="hidden" id="new_folder" value="<?php echo $this->lang->line('filemanager_new_folder'); ?>" />
	<input type="hidden" id="current_dir" value="<?php echo $current_dir; ?>" />
	<input type="hidden" id="lang_show_url" value="<?php echo $this->lang->line('filemanager_show_url'); ?>" />
	<input type="hidden" id="lang_extract" value="<?php echo $this->lang->line('filemanager_extract'); ?>" />
	<input type="hidden" id="lang_file_info" value="<?php echo $this->lang->line('filemanager_file_info'); ?>" />

	<!----- uploader div start ------->
	<div class="uploader">
		<center><button class="btn btn-inverse close-uploader"><i class="icon-backward icon-white"></i><?php echo $this->lang->line('filemanager_dropzone_return'); ?></button></center>
		<div class="space10"></div><div class="space10"></div>
		<form action="/upload_multi_files" method="post" enctype="multipart/form-data" id="myAwesomeDropzone" class="dropzone">
		    <input type="hidden" name="dir" value="<?php echo $current_dir; ?>" />
			<input name="is_ajax" type="hidden" value="1" />
			<input name="field_id" type="hidden" value="<?php echo $field_id; ?>" />
		    <div class="fallback">
				<?php echo $this->lang->line('global_upload'); ?>:<br/>
				<input name="file" type="file" />
				<input type="submit" name="submit" value="Upload" />
		    </div>
		</form>
		<div class="upload-help"><?php echo $this->lang->line('filemanager_dropzone_upload_help'); ?></div>
	</div>
	<!----- uploader div end ------->
	
	<div class="container-fluid">
		<!----- header div start ------->
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container-fluid">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<div class="brand"></div>
					<div class="nav-collapse collapse">
						<div class="filters">
							<div class="row-fluid">
								<div class="span3 half">
									<span><?php echo $this->lang->line('global_actions'); ?>:</span>
									<button class="tip btn upload-btn" title="<?php echo $this->lang->line('filemanager_upload_files'); ?>"><i class="icon-plus"></i><i class="icon-file"></i></button> 
									<button class="tip btn new-folder" title="<?php echo $this->lang->line('filemanager_create_folder'); ?>"><i class="icon-plus"></i><i class="icon-folder-open"></i></button> 
								</div>
								<div class="span3 half view-controller">
									<span><?php echo $this->lang->line('global_views'); ?>:</span>
									<button class="btn tip btn-inverse" id="view0" data-value="0" title="<?php echo $this->lang->line('filemanager_box_view'); ?>"><i class="icon-th icon-white"></i></button>
									<button class="btn tip" id="view1" data-value="1" title="<?php echo $this->lang->line('filemanager_list_view'); ?>"><i class="icon-align-justify"></i></button>
									<button class="btn tip" id="view2" data-value="2" title="<?php echo $this->lang->line('filemanager_column_view'); ?>"><i class="icon-fire"></i></button>
								</div>
							
								<div class="span6 types">
									<span><?php echo $this->lang->line('global_filters'); ?>:</span>
									<?php if($type == 2 || $type == 4): ?>
									<input type="radio" class="hide" checked="checked" data-item="ff-item-type-1" name="radio-sort" id="select-type-1">
									<label class="tip btn ff-label-type-1" for="select-type-1" title="<?php echo $this->lang->line('global_files'); ?>" id="ff-item-type-1"><i class="icon-file"></i></label>
									<?php endif; ?>
									<?php if($type == 1 || $type == 4): ?>
									<input type="radio" class="hide" data-item="ff-item-type-2" name="radio-sort" id="select-type-2">
									<label class="tip btn ff-label-type-2" for="select-type-2" title="<?php echo $this->lang->line('global_images'); ?>" id="ff-item-type-2"><i class="icon-picture"></i></label>
									<?php endif; ?>
									<?php if($type == 2 || $type == 4): ?>
									<input type="radio" class="hide" data-item="ff-item-type-3" name="radio-sort" id="select-type-3">
									<label class="tip btn ff-label-type-3" for="select-type-3" title="<?php echo $this->lang->line('filemanager_archives'); ?>" id="ff-item-type-3"><i class="icon-inbox"></i></label>
									<?php endif; ?>
									<?php if($type == 2 || $type == 3 || $type == 4): ?>
									<input type="radio" class="hide" data-item="ff-item-type-4" name="radio-sort" id="select-type-4">
									<label class="tip btn ff-label-type-4" for="select-type-4" title="<?php echo $this->lang->line('filemanager_videos'); ?>" id="ff-item-type-4"><i class="icon-film"></i></label>
									<?php endif; ?>
									<?php if($type == 2 || $type == 3 || $type == 4): ?>
									<input type="radio" class="hide" data-item="ff-item-type-5" name="radio-sort" id="select-type-5">
									<label class="tip btn ff-label-type-5" for="select-type-5" title="<?php echo $this->lang->line('filemanager_audios'); ?>" id="ff-item-type-5"><i class="icon-music"></i></label>
									<?php endif; ?>
									
									<input type="text" value="" placeholder="<?php echo $this->lang->line('filemanager_text_filter'); ?>" name="filter" id="filter-input" class="filter-input" accesskey="f">							
									<input type="radio" class="hide" data-item="ff-item-type-all" name="radio-sort" id="select-type-all">
									<label class="tip btn btn-inverse ff-label-type-all" style="margin-rigth:0px;" for="select-type-all" data-item="ff-item-type-all" title="<?php echo $this->lang->line('global_all'); ?>" id="ff-item-type-all" <?php if($type == 1) echo "style='visibility: hidden;'"; ?> ><i class="icon-align-justify icon-white"></i></label>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!----- header div end ------->
		
		<!----- breadcrumb div start ------->
		<div class="row-fluid">
			<ul class="breadcrumb">	
				<li class="pull-left"><a href="<?php echo $root_dir_url; ?>"><i class="icon-home"></i></a></li>
				<?php echo $breadcrumb; ?>
				<!--li class="pull-right"><a id="info" href="javascript:void('')" class="btn-small"><i class="icon-question-sign"></i></a></li-->
				<li class="pull-right"><a href="<?php echo $current_dir_url; ?>" class="btn-small" id="refresh"><i class="icon-refresh"></i></a></li>
				<li class="pull-right">
					<div class="btn-group">
						<a href="#" data-toggle="dropdown" class="btn dropdown-toggle sorting-btn">
							<i class="icon-signal"></i> 
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu pull-left sorting">
							<li><center><strong><?php echo $this->lang->line('filemanager_sorting'); ?></strong></center></li>
							<li><a data-sort="name" href="javascript:void('')" class="sorter sort-name "><?php echo $this->lang->line('filemanager_filename'); ?></a></li>
							<li><a data-sort="date" href="javascript:void('')" class="sorter sort-date "><?php echo $this->lang->line('filemanager_date'); ?></a></li>
							<li><a data-sort="size" href="javascript:void('')" class="sorter sort-size "><?php echo $this->lang->line('filemanager_size'); ?></a></li>
							<li><a data-sort="extension" href="javascript:void('')" class="sorter sort-extension "><?php echo $this->lang->line('filemanager_type'); ?></a></li>
						</ul>
					  </div>
				</li>
			</ul>
		</div>
		<!----- breadcrumb div end ------->
		
		<!----- content div start ------->
		<div class="row-fluid ff-container">
			<div class="span12">	    
				<div class="sorter-container list-view0">
					<div class="file-name"><a class="sorter sort-name" href="javascript:void('')" data-sort="name"><?php echo $this->lang->line('filemanager_filename'); ?></a></div>
					<div class="file-date"><a class="sorter sort-date" href="javascript:void('')" data-sort="date"><?php echo $this->lang->line('filemanager_date'); ?></a></div>
					<div class="file-size"><a class="sorter sort-size" href="javascript:void('')" data-sort="size"><?php echo $this->lang->line('filemanager_size'); ?></a></div>
					<div class='img-dimension'><?php echo $this->lang->line('filemanager_dimensions'); ?></div>
					<div class='file-extension'><a class="sorter sort-extension" href="javascript:void('')" data-sort="extension"><?php echo $this->lang->line('filemanager_type'); ?></a></div>
					<div class='file-operations'><?php echo $this->lang->line('filemanager_operations'); ?></div>
				</div>
				<h4 id="help"><?php echo $this->lang->line('filemanager_help_swipe'); ?></h4>
				<ul id="content" class="grid cs-style-2 list-view0">
					<?php echo $content; ?>
				</ul>
			</div>
		</div>
	</div>
	
	<!----- lightbox div start ------->    
    <div id="previewLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
		<div class='lightbox-content'>
		    <img id="full-img" src="">
	    </div>    
    </div>
    <!----- lightbox div end ------->

    <!----- loading div start ------->  
    <div id="loading_container" style="display:none;">
	    <div id="loading" style="background-color:#000; position:fixed; width:100%; height:100%; top:0px; left:0px;z-index:100000"></div>
	    <img id="loading_animation" src="<?php echo app_asset_backend_url('responsiveFilemanager/img/storing_animation.gif'); ?>" alt="loading" style="z-index:10001; margin-left:-32px; margin-top:-32px; position:fixed; left:50%; top:50%"/>
    </div>
    <!----- loading div end ------->
    
    <!----- player div start ------->
    <div class="modal hide fade" id="previewAV">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo $this->lang->line('global_preview'); ?></h3>
		</div>
		<div class="modal-body">
			<div class="row-fluid body-preview"></div>
		</div>
    </div>
    <!----- player div end ------->
    <img id='aviary_img' src='' class="hide"/>
</body>
</html>