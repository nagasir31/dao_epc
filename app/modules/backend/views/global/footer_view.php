<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">
		<span><?php echo (date("Y") == $this->site_config['app_year'] ? date("Y") : $this->site_config['app_year']." - ".date("Y"))." &copy; ".$this->site_config['app_author']; ?>. <span class="hidden-480"><?php echo $this->lang->line('global_reserved'); ?>.</span></span>
	</div>
	<div class="footer-tools">
		<span class="go-top">
			<i class="icon-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->
</body>
</html>