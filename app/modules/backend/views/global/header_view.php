<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<title><?php echo ($this->site_config['app_name'] ? $this->site_config['app_name'] : APPLICATION); ?></title>
<?php
if(isset($css))
{
	foreach($css as $key => $val)
	{
		echo $val."\n";
	}
}
if(isset($js))
{
	foreach($js as $key => $val)
	{
		echo $val."\n";
	}
}
?>
<script>
if(IS_SOCK == 1){
	var sock = new SockJS('http://localhost:9999/notif');
}
jQuery(document).ready(function(){
	NProgress.configure({ parent: '.navbar-inner',showSpinner: true, trickleRate: 0.02, trickleSpeed: 800 });
	NProgress.start();
	App.init();
	
	if(IS_SOCK == 1){
		sock.onopen = function() {
			console.log('open');
		};

		sock.onclose = function() {
			console.log('close');
		};
		
		sock.onmessage = function(e) {
			var data = JSON.parse(e.data);
			console.log(data[0].id);
			jQuery.gritter.add({
				title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
				text: data[0].activity+'<br> Status saat ini: '+data[0].current_status,
				sticky: false,
				time: 15000
			});
		};
	}
});
</script>
</head>
<body class="page-header-fixed page-footer-fixed page-full-width">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-fixed-top">
		<!--div style="background-color:#00A2E8;height:5px;"></div-->
		<div class="navbar-inner">
			<div class="container-fluid">
				<div id="head_logo">
					<a class="brand" href="<?php echo app_backend_url(); ?>">
						<img src="<?php echo app_asset_url("backend/img/logo_pp.png"); ?>" alt="logo" />
					</a>
				</div>
				<div id="head_title" class="hidden-phone">
					<div style="float:left;margin-top:5px;margin-right:30px;">
						<h3><strong><?php echo $this->site_config['company_name']; ?></strong></h3>
						<h4><?php echo $this->site_config['app_name']; ?></h4>
						<span class="label label-important center" style=""><?php echo ! $this->site_config['is_backend_online'] ? $this->lang->line('global_running_on_maintainance_mode') : ""; ?></span>
					</div>
				</div>
				<!-- BEGIN HORIZANTAL MENU -->
				<div class="navbar hor-menu hidden-phone hidden-tablet">
					<div class="navbar-inner">
						<ul class="nav">
							<?php echo $menu; ?>
							<li>
								<span class="hor-menu-search-form-toggler">&nbsp;</span>
								<div class="search-form hidden-phone hidden-tablet">
									<form class="form-search" id="form-search">
										<div class="input-append">
											<input type="text" id="keyword" name="keyword" class="m-wrap" placeholder="<?php echo $this->lang->line('global_search'); ?> <?php echo $this->lang->line('berkas_tracking_number'); ?>...">
											<button type="submit" class="btn" type="button"></button>
										</div>
									</form>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<!-- END HORIZANTAL MENU -->
				
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
					<img src="<?php echo app_asset_url("backend/img/menu-toggler.png"); ?>" alt="" />
				</a>
				
				
				<div class="btn green datetime hidden-phone hidden-tablet" style="padding-right:15px;margin-top:3px;float:right;min-width:130px;font-size:11px;font-weight:bold"><?php echo $this->site_config['display_date']; ?></div>
				<div style="margin-right:10px;float:right;margin-top:5px;">
				<?php foreach ($languages as $language): ?>
					<div style="margin-left:5px;float:left;">
						<img onclick="javascript: ceki.fnSetLang('<?php echo strtolower($language['name']); ?>');" style="border:2px solid <?php echo ($language['name'] == $this->session->userdata(APPLICATION.'_language')) ? "#ccc" : "transparent"; ?>;display:<?php echo ($this->session->userdata(APPLICATION.'_is_multilingual')) ? "block;" : "none"; ?>;" id="<?php echo strtolower($language['name']); ?>"  title="<?php echo strtolower($language['name']); ?>" src="<?php echo app_asset_backend_url('img/flags/'.$language['icon']); ?>" />
					</div>
				<?php endforeach; ?>
				</div>
				
				<div style="margin-top:45px;"></div>
				
				<ul class="nav pull-right">		
					<!-- BEGIN TODO DROPDOWN -->
					<?php $cnt_new_task = count($res_new_task); ?>
					<?php if($cnt_new_task > 0): ?>
					<li class="dropdown" id="header_task_bar">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="icon-tasks"></i>
						<span class="badge"><?php echo $cnt_new_task; ?></span>
						</a>
						<?php if($cnt_new_task > 0): ?>
						<ul class="dropdown-menu extended tasks">
							<li>
								<p style="font-size:11px;font-weight:bold;"><?php echo sprintf($this->lang->line('berkas_notif_pending_task'),$cnt_new_task); ?></p>
							</li>
							<li>
								<ul class="dropdown-menu-list scroller"  style="height:250px">
									<?php foreach($res_new_task as $dt): ?>
									<?php 
										$verify_level = $MCTR->get_verify_level($dt['payment_method']);
										$max_verify_level = count($verify_level);
										$percent = calculate_percentage($dt['last_verify_level_index'],$max_verify_level);
										
										$group_name = $MCTR->get_group_name2();
										if($group_name == 'PROYEK')
										{
											$prog_class = 'progress-danger';
											$param = base64_encode("last_verify_status={$MCTR->ref_verify_status[3]}");
										}
										else
										{
											$prog_class = $dt['payment_method'] == $MCTR->ref_payment_method[1] ? 'progress-success' : 'progress-info';
											$param = base64_encode("last_verify_level={$group_name},last_verify_status={$MCTR->ref_verify_status[1]}");
										}	
									?>
									<li>
										<a href="<?php echo app_backend_url('main/berkas/detail/'.base64_encode($dt['id'])); ?>">
											<span class="task">
												<span class="desc"><?php echo $dt['project_code'].$dt['berkas_number']; ?></span>
												<span class="percent"><?php echo $percent; ?>%</span>
											</span>
											<span class="progress <?php echo $prog_class; ?>">
												<span style="width: <?php echo $percent; ?>%;" class="bar"></span>
											</span>
											<?php echo relative_time(strtotime($dt['last_verify_datetime'])); ?>
										</a>
									</li>
									<?php endforeach; ?>
								</ul>
							</li>
							<li class="external">
								<a href="<?php echo app_backend_url('main/berkas/index/'.$param); ?>"><?php echo $this->lang->line('berkas_view_all_task'); ?> <i class="m-icon-swapright"></i></a>
							</li>
						</ul>
						<?php endif; ?>
					</li>
					<?php endif; ?>
					<!-- END TODO DROPDOWN -->     
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img class="thumbnail" alt="" align="center" style="width:40px;height:40px;display:inline;" src="<?php echo $this->session->userdata(APPLICATION.'_picture'); ?>" />
						<span class="username" style="font-size:12px;padding:5px;"><?php echo $this->session->userdata(APPLICATION.'_name') ? char_limiter($this->session->userdata(APPLICATION.'_name'),15) : char_limiter($this->session->userdata(APPLICATION.'_user_name'),15);  ?></span>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo app_backend_url('profile'); ?>"><i class="icon-user"></i> <?php echo $this->lang->line('global_view_profile'); ?></a></li>
							<li class="divider"></li>
							<li><a href="javascript:;" id="trigger_fullscreen"><i class="icon-move"></i> <?php echo $this->lang->line('global_fullscreen'); ?></a></li>
							<li><a href="<?php echo app_backend_url("login/lock"); ?>"><i class="icon-lock"></i> <?php echo $this->lang->line('global_lockscreen'); ?></a></li>
							<li><a href="<?php echo app_backend_url("login/logout"); ?>"><i class="icon-key"></i> <?php echo $this->lang->line('global_logout'); ?></a></li>
						</ul>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<!-- END HEADER -->

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<div class="page-sidebar nav-collapse collapse visible-phone visible-tablet">
			<ul class="page-sidebar-menu">
				<?php echo $menu2; ?>
			</ul>
		</div>
		
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title"></h3>
						<ul class="breadcrumb">
							<li>
								<a href="<?php echo app_backend_url(); ?>"><i class="icon-home"></i> <?php echo $this->lang->line('global_dashboard'); ?></a>
								<?php echo $this->session->userdata(APPLICATION.'_active_breadcrumb_lang') ? '<i class="icon-angle-right"></i>' : ''; ?>
							</li>
							<?php echo $this->session->userdata(APPLICATION.'_active_breadcrumb_lang'); ?>
							<!--li class="pull-right no-text-shadow"></li-->
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->