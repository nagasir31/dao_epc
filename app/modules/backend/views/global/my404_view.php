<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!-- BEGIN PAGE -->
<div class="page-content">
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12">
			
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12 page-404">
				<div class="number">
					404
				</div>
				<div class="details">
					<h3><?php echo $this->lang->line('global_oops_lost'); ?></h3>
					<p>
						<h3><?php echo $this->lang->line('global_page_not_found'); ?>.</h3>
					</p>
					<p>
						<a onclick="history.back()" class="btn green"><?php echo $this->lang->line('global_goto_previous_page'); ?> <i class="m-icon-swapright m-icon-white"></i></a>
					</p>
					<a href="<?php echo app_backend_url(); ?>" class="btn blue"><?php echo $this->lang->line('global_goto_dashboard_page'); ?> <i class="icon-home"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		  App.init();
	});
</script>