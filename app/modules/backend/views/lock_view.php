<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<title><?php echo ($this->site_config['app_name'] ? $this->site_config['app_name'] : APPLICATION); ?></title>
<?php
if(isset($css))
{
	foreach($css as $key => $val)
	{
		echo $val."\n";
	}
}
if(isset($js))
{
	foreach($js as $key => $val)
	{
		echo $val."\n";
	}
}
?>
<script type="text/javascript">
	jQuery(document).ready(function () {
		App.init();
		if(!ceki.is_mobile) {
			jQuery.backstretch([
				"<?php echo app_asset_url("backend/img/bg/1.jpg"); ?>",
				"<?php echo app_asset_url("backend/img/bg/2.jpg"); ?>",
				"<?php echo app_asset_url("backend/img/bg/3.jpg"); ?>",
				"<?php echo app_asset_url("backend/img/bg/4.jpg"); ?>",
				], {
				  fade: 1000,
				  duration: 8000
			});
		}
	});
 
   function fnSubmitLogin() {
		App.blockUI(jQuery('.page-body'));
		
		var u = jQuery('#username').val();
		var p = jQuery('#password').val();
		if(p == '') {
			jQuery('.alerts').html('<div class="alert alert-error"><button class="close" data-dismiss="alert"></button> <?php echo $this->lang->line('global_blank_pwd'); ?></div>');
			jQuery('.login-alert').fadeIn();
		}else{
			var cAjax = new ceki.fnAjax({
				url : APP_BACKEND_URL + 'login',
				method : '/login_process',
				data : ({
					usr : u,
					pwd : p
				}),
				successCallBack : function(obj) {
					window.location = (typeof obj.message != 'undefined') ? obj.message : APP_BACKEND_URL;
				},
				successErrorCallBack : function(obj) {
					jQuery('.alerts').html('<div class="alert alert-error"><button class="close" data-dismiss="alert"></button> <?php echo $this->lang->line('global_invalid_pwd'); ?></div>');
					jQuery('.login-alert').fadeIn();
				}
			});
		} 
		App.unblockUI(jQuery('.page-body'));
	}
</script>
</head>
<body class="lock">
	<div class="page-lock">
		<div class="page-logo">
			<div id="head_title">
				<div class="center">
					<img style="width:130px;" src="<?php echo app_asset_url("backend/img/logo_pp.png"); ?>" alt="" /> 
					<div id="head_title_login">
						<h3 style="color:#FFFFFF;font-weight:bold;line-height:30px;"><?php echo $this->site_config['app_name']; ?></h3>
					</div>
				</div>
				
			</div>
		</div>
		<div class="page-body">
			<div class="login-alert hide">
				<div class="alerts"></div>
			</div>
			<img class="page-lock-img" src="<?php echo $this->session->userdata(APPLICATION.'_picture'); ?>" alt="photo">
			<div class="page-lock-info">
				<?php if($this->session->userdata(APPLICATION.'_name') != ''): ?>
				<span><strong><?php echo char_limiter($this->session->userdata(APPLICATION.'_name'),15); ?>/<?php echo $this->session->userdata(APPLICATION.'_nrp'); ?></strong></span>
				<?php endif; ?>
				<span style="color:#FFF;"><strong><?php echo char_limiter($this->session->userdata(APPLICATION.'_user_name'),20); ?></strong></span>
				<span style="color:black;"><strong><?php echo $this->lang->line('global_locked'); ?></strong> <i class="icon-lock"></i></span>
				<form class="form-search" id="login" action="javascript: fnSubmitLogin();" method="post">
					<div class="input-append">
						<input type="hidden" name="username" id="username" value="<?php echo $this->session->userdata(APPLICATION.'_user_name'); ?>">
						<input type="password" autocomplete="off" class="m-wrap" placeholder="<?php echo $this->lang->line('global_password'); ?>" name="password" id="password">
						<button type="submit" class="btn blue icn-only"><i class="m-icon-swapright m-icon-white"></i></button>
					</div>
				</form>
			</div>
		</div>
		<div class="page-footer">
			<p style="text-align:center;color:#FFFFFF;"><?php echo date("Y")." &copy; ".$this->site_config['app_author']; ?></p>
		</div>
	</div>
</body>
</html>