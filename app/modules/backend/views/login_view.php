<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<title><?php echo ($this->site_config['app_name'] ? $this->site_config['app_name'] : APPLICATION); ?></title>
<?php
if(isset($css))
{
	foreach($css as $key => $val)
	{
		echo $val."\n";
	}
}
if(isset($js))
{
	foreach($js as $key => $val)
	{
		echo $val."\n";
	}
}
?>
<script type="text/javascript">
	jQuery(document).ready(function () {
		App.init();
		
		if(!ceki.is_mobile) {
			jQuery.backstretch([
				"<?php echo app_asset_url("backend/img/bg/1.jpg"); ?>",
				"<?php echo app_asset_url("backend/img/bg/2.jpg"); ?>",
				"<?php echo app_asset_url("backend/img/bg/3.jpg"); ?>",
				"<?php echo app_asset_url("backend/img/bg/4.jpg"); ?>",
				], {
				  fade: 1000,
				  duration: 8000
			});
		}
	});
 
   function fnSubmitLogin() {
		App.blockUI(jQuery('.content'));
		
		var u = jQuery('#username').val();
		var p = jQuery('#password').val();
		if(u == '' || p == '') {
			jQuery('.alerts').html('<div class="alert alert-error"><button class="close" data-dismiss="alert"></button> <?php echo $this->lang->line('global_blank_uname_pwd'); ?></div>');
			jQuery('.login-alert').fadeIn();
			App.unblockUI(jQuery('.content'));
		}else{
			var cAjax = new ceki.fnAjax({
				url : APP_BACKEND_URL + 'login',
				method : '/login_process',
				data : ({
					usr : u,
					pwd : p
				}),
				successCallBack : function(obj) {
					window.location = (typeof obj.message != 'undefined') ? obj.message : APP_BACKEND_URL;
				},
				successErrorCallBack : function(obj) {	
					msg = (typeof obj.message != 'undefined') ? obj.message : '<?php echo $this->lang->line('global_invalid_uname_pwd'); ?>';
					jQuery('.alerts').html('<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>'+msg+'<div>');
					jQuery('.login-alert').fadeIn();
					App.unblockUI(jQuery('.content'));
				}
			});
		} 
		
	}
</script>
</head>
<body class="login">
	<div class="logo">
		<!--img style="width:100px;float:left;margin-right:10px;" src="<?php echo app_asset_url("global/images/".$this->site_config['app_logo']); ?>" alt="" /> 
		<h4 style="color:#606060;"><strong><?php echo $this->site_config['company_name']; ?></strong></h4>
		<h4 style="color:#CCC;font-weight:bold;line-height:20px;"><?php echo $this->site_config['app_name']; ?></h4-->
		
		<img id="head_logo_login" src="<?php echo app_asset_url("backend/img/logo_pp.png"); ?>" alt="" /> 
		<div id="head_title_login">
			<h3 style="color:#FFFFFF;font-weight:bold;line-height:30px;"><?php echo $this->site_config['app_name']; ?></h3>
		</div>
	</div>
	<!-- BEGIN LOGIN -->
	<div class="content">
		<form class="form-vertical login-form" id="login" action="javascript: fnSubmitLogin();" method="post">
			<h4 class="form-title center hidden-480"><strong><?php echo $this->lang->line('global_login_to_your_account'); ?></strong></h4>
			<div class="login-alert hide">
				<div class="alerts"></div>
			</div>
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9"><?php echo $this->lang->line('global_username'); ?></label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="<?php echo $this->lang->line('global_username'); ?>" name="username" id="username" />
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9"><?php echo $this->lang->line('global_password'); ?></label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<input class="m-wrap placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php echo $this->lang->line('global_password'); ?>" name="password" id="password" />
					</div>
				</div>
			</div>
			<div class="form-actions">
				<button type="submit" name="submit" class="btn blue pull-right">
				<?php echo $this->lang->line('global_login'); ?> <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
		</form>
	</div>
	<!-- END LOGIN -->
	<div class="copyright">
	<?php echo date("Y")." &copy; ".$this->site_config['app_author']; ?>
	</div>
</body>
</html>