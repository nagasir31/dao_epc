<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
body .modal#rejected_note_modal {
  width: 50%; /* desired relative width */
  left: 25%; /* (100%-width)/2 */
  /* place center */
  margin-left:auto;
  margin-right:auto; 
}

body .modal#detail_pembayaran_modal {
  width: 70%; /* desired relative width */
  left: 15%; /* (100%-width)/2 */
  /* place center */
  margin-left:auto;
  margin-right:auto; 
}

/* #detail_pembayaran_modal .modal-body {
    max-height: 420px;
    overflow-y: auto;
}
 */
.thumb-img{
	height:200px;
	margin-left:auto;
	margin-right:auto;
}
#table_detail_pembayaran tfoot {
	border-top: 2px solid black;
}

.nav-tabs > li, .nav-pills > li {
    float:none;
    display:inline-block;
    *display:inline; /* ie7 fix */
     zoom:1; /* hasLayout ie7 trigger */
}

.nav-tabs, .nav-pills {
    text-align:center;
}
</style>
<div id="main-container">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-credit-card"></i><?php echo $this->lang->line('global_detail'); ?> <?php echo $this->lang->line('berkas_name'); ?></div>
		</div>
		<div class="portlet-body form">
			<div class="row-fluid">
				<div class="span12">
					<p style="text-align:right;">
						<?php if($res_berkas['berkas_status'] == $CTR->ref_berkas_status[2]): ?>
						<a id="export_report" target="_blank" href="<?php echo app_backend_url('main/berkas/export_report/'.base64_encode($res_berkas['id'])); ?>" class="btn red" style="color:#fff;" data-id="<?php echo base64_encode($res_berkas['id']); ?>"><i class="icon-file"></i> <span class="hidden-480"><?php echo $this->lang->line('global_report'); ?></span></a>
						<?php endif; ?>
					</p>
					
					<h3 class="block center hidden-480">
						<?php echo $res_berkas['vendor_name']."  ( ".$res_berkas['project_code'].$res_berkas['berkas_number']." )"; ?>
					</h3>
			
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><?php echo $this->lang->line('global_info'); ?> <?php echo $this->lang->line('berkas_name'); ?></div>
							<div class="tools">
								<a class="collapse" href="javascript:;"></a>
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-hover table-striped table-bordered">
								<tbody>
                                    <tr>
                                        <td class="center" colspan="2" style="background-color:#FA8072;color:#fff;"><b><?php echo $this->lang->line('global_data'); ?> <?php echo $this->lang->line('berkas_hard_copy'); ?></b>
                                            <button type="submit" class="btn-sm button-submit blue" id="edit_hard_copy"><i class="icon-edit"></i> Ubah</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="right" style="width:30%;"><b>Hard Copy Datang</b></td>
                                        <td><?php echo ! empty($res_berkas['hard_copy_arrived_pretty']) ? $res_berkas['hard_copy_arrived_pretty'] : '-'; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="right" style="width:30%;"><b>Status Berkas Hard Copy</b></td>
                                        <td><?php echo ! empty($res_berkas['hard_copy_status']) ? $res_berkas['hard_copy_status'] : '-'; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="right" style="width:30%;"><b>Hard Copy Approved</b></td>
                                        <td><?php echo ! empty($res_berkas['hard_copy_approved_pretty']) ? $res_berkas['hard_copy_approved_pretty'] : '-'; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="right" style="width:30%;"><b>Ket. Berkas Hard Copy</b></td>
                                        <td><?php echo ! empty($res_berkas['hard_copy_description']) ? $res_berkas['hard_copy_description'] : '-'; ?></td>
                                    </tr>
									<!-- Start Berkas -->
									<tr>
										<td class="center" colspan="2" style="background-color:#FA8072;color:#fff;"><b><?php echo $this->lang->line('global_data'); ?> <?php echo $this->lang->line('berkas_name'); ?></b></td>
									</tr>
                                    <tr>
                                        <td class="right" style="width:30%;"><b>Nomor Doc SAP</b></td>
                                        <td><?php echo $res_berkas['no_doc_sap'] ? $res_berkas['no_doc_sap'] : '-'?>
                                            <button type="submit" class="btn-sm button-submit blue" id="edit_doc_sap"><i class="icon-edit"></i> Ubah</button>
                                        </td>
                                    </tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_project'); ?></b></td>
										<td><?php echo ! empty($res_berkas['project_name']) ? $res_berkas['project_name']." ( ".$res_berkas['project_code']." )" : $res_berkas['project_code']; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_number'); ?></b></td>
										<td><?php echo ! empty($res_berkas['berkas_number']) ? $res_berkas['berkas_number'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_ttap_date'); ?></b></td>
										<td><?php echo ! empty($res_berkas['ttap_date']) ? $res_berkas['ttap_date'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_bap_bpg_dub_number_date'); ?></b></td>
										<td>
											<?php if(isset($res_berkas['bap_bpg_dub_number_date']) && ! empty($res_berkas['bap_bpg_dub_number_date'])): ?>
												<?php echo $res_berkas['bap_bpg_dub_number_date']; ?>
											<?php else: ?>
												- &nbsp; ( - )
											<?php endif; ?>
										</td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_surat_jalan_lkp_number_date'); ?></b></td>
										<td>
											<?php echo ! empty($res_berkas['surat_jalan_lkp_number']) ? $res_berkas['surat_jalan_lkp_number'] : '-'; ?>
											&nbsp; 
											( <?php echo ! empty($res_berkas['surat_jalan_lkp_date']) ? $res_berkas['surat_jalan_lkp_date'] : '-'; ?> )
										</td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_spk_po_number_date'); ?></b></td>
										<td>
											<?php echo ! empty($res_berkas['spk_po_number']) ? $res_berkas['spk_po_number'] : '-'; ?>
											&nbsp;
											( <?php echo ! empty($res_berkas['spk_po_date']) ? $res_berkas['spk_po_date'] : '-'; ?> )
										</td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_released_date'); ?></b></td>
										<td><?php echo ! empty($res_berkas['rilis_datetime']) ? $res_berkas['rilis_datetime'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_verify_status'); ?></b></td>
										<td><?php echo ! empty($res_berkas['last_verify_status']) ? $res_berkas['last_verify_status'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_status'); ?></b></td>
										<td><?php echo $res_berkas['berkas_status'].", ".$this->lang->line('berkas_revision').": ".(! empty($res_berkas['revision']) ? $res_berkas['revision'] : '-'); ?></td>
									</tr>
<!--									--><?php //if($res_berkas['berkas_status'] == $CTR->ref_berkas_status[2]): ?>
<!--									<tr>-->
<!--										<td class="right" style="width:30%;"><b>--><?php //echo $this->lang->line('berkas_hard_copy'); ?><!--</b></td>-->
<!--										<td>-->
<!--											--><?php //$s_id = ($res_berkas['berkas_status'] == 'Selesai' && isset($CTR->module_functions['input_payment'])) ? 'update_hard_copy' : 'hard_copy'; ?>
<!--											--><?php //echo $res_berkas['is_hard_copy'] ? '<span id="'.$s_id.'" data-value="1" class="icon-ok"></span>' : '<span id="'.$s_id.'" data-value="0" class="icon-remove"></span>'; ?>
<!--										</td>-->
<!--									</tr>-->
<!--									--><?php //endif; ?>
									<!-- End Berkas -->
									
									<!-- Start Vendor -->
									<tr>
										<td class="center" colspan="2" style="background-color:#FA8072;color:#fff;"><b><?php echo $this->lang->line('global_data'); ?> <?php echo $this->lang->line('berkas_vendor'); ?></b></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_vendor_type'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_type']) ? $res_berkas['vendor_type'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_vendor_name'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_name']) ? $res_berkas['vendor_name'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_vendor_code'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_code']) ? $res_berkas['vendor_code'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_npwp'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_npwp']) ? $res_berkas['vendor_npwp'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_bank_name'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_bank_name']) ? $res_berkas['vendor_bank_name'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_bank_rek'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_bank_rek']) ? $res_berkas['vendor_bank_rek'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_bank_account'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_bank_account']) ? $res_berkas['vendor_bank_account'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('global_address'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_address']) ? $res_berkas['vendor_address'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('global_city'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_city']) ? $res_berkas['vendor_city'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('global_email'); ?></b></td>
										<td><?php echo ! empty($res_berkas['vendor_email']) ? $res_berkas['vendor_email'] : '-'; ?></td>
									</tr>
                                    <tr>
                                        <td class="right" style="width:30%;"><b>NIK KTP</b></td>
                                        <td><?php echo ! empty($res_berkas['nik_ktp']) ? $res_berkas['nik_ktp'] : '-'; ?></td>
                                    </tr>
									<!-- End Vendor -->
									
									<!-- Start Biaya & Pajak -->
									<tr>
										<td class="center" colspan="2" style="background-color:#FA8072;color:#fff;"><b><?php echo $this->lang->line('global_data'); ?> <?php echo $this->lang->line('berkas_biaya_pajak'); ?></b></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_payment_method'); ?></b></td>
										<td><?php echo ! empty($res_berkas['payment_method']) ? $res_berkas['payment_method'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_barang_jasa_name'); ?></b></td>
										<td><?php echo ! empty($res_berkas['barang_jasa_name']) ? $res_berkas['barang_jasa_name'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_invoice_number_date'); ?></b></td>
										<td>
											<?php echo ! empty($res_berkas['invoice_number']) ? $res_berkas['invoice_number'] : '-'; ?>
											&nbsp;
											( <?php echo ! empty($res_berkas['invoice_date']) ? $res_berkas['invoice_date'] : '-'; ?> )
										</td>
									</tr>
									<?php if(isset($res_berkas['uploaded_fp_filepath'])):?>
                                        <tr>
                                            <td class="right" style="width:30%;"><b>URL FP Normal/Normal-Pengganti</b></td>
                                            <td>
                                                <input type="text" value="<?php echo ! empty($res_berkas['faktur_pajak_url']) ? $res_berkas['faktur_pajak_url'] : '-'; ?>" class="m-wrap span6" id="faktur_pajak_url" name="faktur_pajak_url" readonly>
                                                <button type="button" class="btn yellow" id="btn_copy">Copy</button>
                                            </td>
                                        </tr>
									<?php endif; ?>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_faktur_pajak_number_date'); ?>  Normal/Normal-Pengganti</b></td>
										<td>
											<?php echo ! empty($res_berkas['faktur_pajak_number']) ? $res_berkas['faktur_pajak_number'] : '-'; ?>
											&nbsp;
											( <?php echo ! empty($res_berkas['faktur_pajak_date']) ? $res_berkas['faktur_pajak_date'] : '-'; ?> )
										</td>
									</tr>
                                    <tr>
                                        <td class="right" style="width:30%;"><b>Nilai PPN FP Normal/Normal-Pengganti</b></td>
                                        <td>
                                            Rp. <?php echo ! empty($res_berkas['fp_amount']) ? $CTR->money_format($res_berkas['fp_amount']) : '-'; ?>
                                        </td>
                                    </tr>
									<?php if(isset($res_berkas['uploaded_fp_filepath_2'])):?>
                                        <tr>
                                            <td class="right" style="width:30%;"><b>URL <?php echo $res_berkas['faktur_pajak_status_2']?></b></td>
                                            <td>
                                                <input type="text" value="<?php echo ! empty($res_berkas['faktur_pajak_url_2']) ? $res_berkas['faktur_pajak_url_2'] : '-'; ?>" class="m-wrap span6" id="faktur_pajak_url_2" name="faktur_pajak_url_2" readonly>
                                                <button type="button" class="btn yellow" id="btn_copy_2">Copy</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right" style="width:30%;"><b>No. & Tgl <?php echo $res_berkas['faktur_pajak_status_2']?></b></td>
                                            <td>
												<?php echo ! empty($res_berkas['faktur_pajak_number_2']) ? $res_berkas['faktur_pajak_number_2'] : '-'; ?>
                                                &nbsp;
                                                ( <?php echo ! empty($res_berkas['faktur_pajak_date_2']) ? $res_berkas['faktur_pajak_date_2'] : '-'; ?> )
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right" style="width:30%;"><b>Nilai PPN <?php echo $res_berkas['faktur_pajak_status_2']?></b></td>
                                            <td>
                                                Rp. <?php echo ! empty($res_berkas['fp_amount_2']) ? $CTR->money_format($res_berkas['fp_amount_2']) : '-'; ?>
                                            </td>
                                        </tr>
									<?php endif;?>
                                    <tr>
                                        <td class="right" style="width:30%;"><b>Jenis Pajak</b></td>
                                        <td>
											<?php echo ! empty($res_berkas['pajak_type']) && ! empty($res_berkas['pajak_type']) ? $res_berkas['pajak_type'] : '-'; ?>
                                        </td>
                                    </tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_pajak_period'); ?></b></td>
										<td><?php echo ! empty($res_berkas['pajak_period']) && ! empty($res_berkas['pajak_year']) ? $res_berkas['pajak_period'].' - '.$res_berkas['pajak_year'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_real_cost'); ?></b></td>
										<td>Rp. <?php echo ! empty($res_berkas['real_cost']) ? $CTR->money_format($res_berkas['real_cost']) : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_ppn'); ?> <?php echo ! empty($res_berkas['ppn']) ? "( ".$res_berkas['ppn']."% )" : ""; ?></b></td>
										<td>Rp. <?php echo ! empty($res_berkas['ppn_val']) ? $CTR->money_format($res_berkas['ppn_val']) : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_pph'); ?> <?php echo ! empty($res_berkas['pph']) ? "( ".$res_berkas['pph']."% )" : ""; ?></b></td>
										<td>Rp. <?php echo ! empty($res_berkas['pph_val']) ? $CTR->money_format($res_berkas['pph_val']) : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_netto_cost'); ?></b></td>
										<td><b>Rp. <?php echo ! empty($res_berkas['netto_cost_val']) ? $CTR->money_format($res_berkas['netto_cost_val']) : '-'; ?></b></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_total_biaya'); ?></b></td>
										<td><b>Rp. <?php echo ! empty($res_berkas['real_cost']) ? $CTR->money_format($res_berkas['real_cost'] + $res_berkas['ppn_val'] + $res_berkas['pph_val']).' <i>(termasuk PPN & PPH)</i>' : '-'; ?></b></td>
									</tr>
									<?php if($res_berkas['berkas_status'] == $CTR->ref_berkas_status[2]): ?>
									<input type="hidden" id="payment_status" value="1" />
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_total_pembayaran'); ?></b></td>
										<td>
											<b>Rp. <span id="total_pembayaran">-</span></b> &nbsp; | <span class='icon-money blink' title="<?php echo $this->lang->line('berkas_detail_pembayaran'); ?>"></span>
										</td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_total_hutang_valid'); ?></b></td>
										<td style="color:#d84a38"><b>Rp. <span id="total_hutang_valid">-</span></b></td>
									</tr>
									<?php endif; ?>
									<!-- End Biaya & Pajak -->
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><?php echo $this->lang->line('berkas_verify_status'); ?></div>
							<div class="tools">
								<a class="collapse" href="javascript:;"></a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="rootwizard" >
								<ul class="gsi-step-indicator triangle gsi-style-1 gsi-transition" align="center">
									<?php foreach($res_berkas_verify as $dt): ?>
									<?php 
										$li_class = "";
										if($dt['verify_status'] == $CTR->ref_verify_status[1])
										{
											$li_class = "active";
										}
										elseif($dt['verify_status'] == $CTR->ref_verify_status[2])
										{
											$li_class = "done";
										}
										elseif($dt['verify_status'] == $CTR->ref_verify_status[3])
										{
											$li_class = "rejected";
										}
										
										$durasi = "";
										if($li_class == "done" || $li_class == "rejected")
										{
											$start_date = $dt['verify_datetime'];
											$end_date = $dt['approval_datetime'];
											$durasi = ", Durasi Approval: ".$CTR->date_indonesia->get_diff_datetime($start_date,$end_date);
										}
									?>
									<li class="<?php echo $li_class; ?>">
										<a href="#tab<?php echo $dt['verify_level_index']; ?>" data-toggle="tab">
											<span class="number"><?php echo $dt['verify_level_index']; ?></span>
											<span class="desc" title="<?php echo $dt['verify_status'].$durasi; ?>">
												<?php echo $dt['verify_level']; ?>
											</span>
										</a>
									</li>
									<?php endforeach; ?>
								</ul>
								
								<?php
									if($res_berkas['berkas_status'] == $CTR->ref_berkas_status[2])
									{
										$prog_cls = "";
										$width = 100;
									}
									else
									{
										$prog_cls = "active";
										$width = 0;
									}
								?>
								<div class="progress progress-striped <?php echo $prog_cls; ?>" id="bar">
									
									<div class="bar" style="width: <?php echo $width; ?>%;"></div>
								</div>
								
								<?php if($res_berkas['berkas_status'] == $CTR->ref_berkas_status[2]): ?>
								<?php
									$start_date = $res_berkas_verify[0]['verify_datetime'];
									$end_date = $res_berkas_verify[$max_level_berkas-1]['approval_datetime'];
									$durasi_all = $CTR->date_indonesia->get_diff_datetime($start_date,$end_date);
								?>
								<div class="center">
									<h4 id="label-done" style="color:#d84a38;font-weight:bold;"><?php echo $CTR->ref_berkas_status[2]; ?> Dalam <?php echo $durasi_all; ?></h4>
								</div>
								<?php endif; ?>
								
								<div class="tab-content" align="center">
									<?php foreach($res_berkas_verify as $dt): ?>
									<?php	
										$div_class = "";
										if($dt['verify_level'] == $res_berkas['last_verify_level'])
										{
											$div_class = "active";
										}
										
										$show_approval = FALSE;
										if(isset($CTR->module_functions['approval']) && $dt['verify_status'] == $CTR->ref_verify_status[1])
										{
											if($this->session->userdata(APPLICATION.'_sys_group_child_id') != 1)
											{
												if(isset($group_name) && $group_name == $dt['verify_level'])
												{
													$show_approval = TRUE;
												}
											}
											else
											{
												$show_approval = TRUE;
											}
										}
									?>
									<div id="tab<?php echo $dt['verify_level_index']; ?>" class="tab-pane <?php echo $div_class; ?>">
										<div class="clearfix space10"></div>
										
										<p id="button-approval" class="center" style="<?php echo $show_approval ? 'display:block;' : 'display:none;'; ?>">
											<a href="javascript:;" class="btn green button-next">
												<?php echo $this->lang->line('berkas_approve2'); ?> <i class="m-icon-swapright m-icon-white"></i>
											</a>
											&nbsp;
											<a href="javascript:;" class="btn red button-reject">
												<?php echo $this->lang->line('berkas_reject2'); ?> <i class="m-icon-swapright m-icon-white"></i>
											</a>
										</p>
									</div>
									<?php endforeach; ?>
								</div>
							</div>
							<?php if($res_berkas['last_verify_statuss'] == $CTR->ref_verify_status[3]): ?>
							<span style="color:#d84a38; font-weight:bold;"><?php echo $this->lang->line('berkas_rejected_note'); ?> </span>:  <?php echo $res_berkas['rejected_note']; ?>
							<?php endif; ?>
						</div>
					</div>
					
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><?php echo $this->lang->line('global_file_list'); ?></div>
							<div class="tools">
								<a class="collapse" href="javascript:;"></a>
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th><?php echo $this->lang->line('berkas_type'); ?></th>
										<th class="hidden-480"><?php echo $this->lang->line('global_file_type'); ?></th>
										<th class="hidden-480"><?php echo $this->lang->line('global_file_size'); ?></th>
										<th><?php echo $this->lang->line('global_files'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php $rnum=1;foreach($res_berkas_files as $dt): ?>
									<?php
										$bytes = (1024 * $dt['file_size']);
										$file_size = $CTR->formatSizeUnits($bytes);
									?>
									<tr>
										<td><?php echo $rnum; ?></td>
										<td ><?php echo $dt['file_name']; ?></td>
										<td class="hidden-480"><?php echo $dt['file_type']; ?></td>
										<td class="hidden-480"><?php echo $file_size; ?></td>
										<td><a target="_blank" href="<?php echo app_asset_url($dt['file_path']); ?>"><?php echo $this->lang->line('global_view_file'); ?></a></td>
									</tr>
									<?php $rnum++;endforeach; ?>
									<?php if(isset($res_berkas['uploaded_fp_filepath'])) :?>
										<?php
										$bytes = $res_berkas['uploaded_fp_filesize'];
										$file_size = $CTR->formatSizeUnits($bytes);
										?>
                                        <tr>
                                            <td><?php echo $rnum; ?></td>
                                            <td><?php echo $res_berkas['uploaded_fp_filename']; ?> (<?php echo $res_berkas['faktur_pajak_status']; ?>)</td>
                                            <td class="hidden-480">application/pdf</td>
                                            <td class="hidden-480"><?php echo $file_size; ?></td>
                                            <td>
                                                <a target="_blank" href="<?php echo app_asset_url('faktur_pajak/'.$res_berkas['uploaded_fp_filepath']."?t=".time()); ?>"><?php echo $this->lang->line('global_view_file'); ?></a>
                                            </td>
                                        </tr>
										<?php $rnum++; ?>
									<?php endif;?>
									<?php if(isset($res_berkas['uploaded_fp_filepath_2'])) :?>
										<?php
										$bytes = $res_berkas['uploaded_fp_filesize_2'];
										$file_size = $CTR->formatSizeUnits($bytes);
										?>
                                        <tr>
                                            <td><?php echo $rnum; ?></td>
                                            <td ><?php echo $res_berkas['uploaded_fp_filename_2']; ?> (<?php echo $res_berkas['faktur_pajak_status_2']; ?>)</td>
                                            <td class="hidden-480">application/pdf</td>
                                            <td class="hidden-480"><?php echo $file_size; ?></td>
                                            <td>
                                                <a target="_blank" href="<?php echo app_asset_url('faktur_pajak/'.$res_berkas['uploaded_fp_filepath_2']."?t=".time()); ?>"><?php echo $this->lang->line('global_view_file'); ?></a>
                                            </td>
                                        </tr>
										<?php $rnum++; ?>
									<?php endif;?>
									<?php foreach($res_berkas_files_etc as $dt): ?>
										<?php
										$bytes = (1024 * $dt['file_size']);
										$file_size = $CTR->formatSizeUnits($bytes);
										?>
                                        <tr>
                                            <td><?php echo $rnum; ?></td>
                                            <td ><?php echo $dt['file_name']; ?></td>
                                            <td class="hidden-480"><?php echo $dt['file_type']; ?></td>
                                            <td class="hidden-480"><?php echo $file_size; ?></td>
                                            <td><a target="_blank" href="<?php echo app_asset_url($dt['file_path']); ?>"><?php echo $this->lang->line('global_view_file'); ?></a></td>
                                        </tr>
                                    <?php $rnum++;endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>   
				</div>
			</div>
		</div> 
	</div>
	<p class="center">
		<button id="btn_back" type="button" class="btn"><?php echo $this->lang->line('global_back'); ?></button>
	</p>
	<div class="clearfix space20"></div>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade in" id="rejected_note_modal">
	<form id="rejected_note-form" class="form-horizontal" method="post" autocomplete="off">
		<div class="modal-header">
			<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
			<h3><?php echo $this->lang->line('berkas_rejected_note'); ?> :</h3>
		</div>
		<div class="modal-body">
			<textarea rows="10" style="width: 98%;font-size: 12px;" id="rejected_note" name="rejected_note"></textarea>
		</div>
		
		<div class="modal-footer">
			<input type="hidden" id="berkas_id" name="berkas_id" value="<?php echo $res_berkas['id']; ?>" />
			<input type="hidden" id="verify_level_index" name="verify_level_index" value="<?php echo $res_berkas['last_verify_level_index']; ?>" />
			<input type="hidden" id="payment_method" name="payment_method" value="<?php echo $res_berkas['payment_method']; ?>" />
			<input type="hidden" disabled="disabled" id="berkas_status" name="berkas_status" value="<?php echo $res_berkas['berkas_status']; ?>" />
			<input type="hidden" name="verify_status_id" value=3 />
			<button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo $this->lang->line('global_submit'); ?></button>
			<button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
		</div>
	</form>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade in" id="detail_pembayaran_modal">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
		<h3><?php echo $this->lang->line('berkas_detail_pembayaran'); ?></h3>
	</div>
	<div class="modal-body">
		<div class="scroller" style="height:400px" data-always-visible="1" data-rail-visible1="1">
		<table class="table table-hover" id="table_detail_pembayaran" >
			<thead>
				<tr>
					<th style="width:10%;text-align:center;"><?php echo $this->lang->line('berkas_termin'); ?></th>
					<th style="width:15%;text-align:left;"><?php echo $this->lang->line('berkas_nominal'); ?> ( Rp )</th>
					<th style="width:30%;text-align:left;" class="hidden-480"><?php echo $this->lang->line('global_note'); ?></th>
					<th style="width:15%;text-align:center;" class="hidden-480"><?php echo $this->lang->line('berkas_payment_date'); ?></th>
					<th style="width:15%;text-align:center;" class="hidden-480"><?php echo $this->lang->line('berkas_invoice_date'); ?></th>
					<th style="width:10%;text-align:center;">#</th>
				</tr>
			</thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<td style="width:10%;text-align:center;"><?php echo $this->lang->line('global_total'); ?>:</td>
					<td><b><span id="total_nominal_pembayaran">-</span></b></td>
					<td colspan="3" class="hidden-480"></td>
				</tr>
			</tfoot>
		</table>
		</div>
	</div>
	<div class="modal-footer">
		<?php if(isset($CTR->module_functions['input_payment'])): ?>
		<button id="button-add" class="btn green" data-toggle="modal" href="#input_pembayaran_modal" style="float:left;"><?php echo $this->lang->line('berkas_input_pembayaran'); ?></button>
		<?php endif; ?>
		<button class="btn purple" id="button-refresh"><i class="icon-refresh"></i> <?php echo $this->lang->line('global_refresh'); ?></button>
		<button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_close'); ?></button>
	</div>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade in" data-focus-on="input:first" id="input_pembayaran_modal">
	<form id="add_edit-form" class="form-horizontal" method="post" autocomplete="off">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h3><?php echo $this->lang->line('berkas_input_pembayaran'); ?></h3>
		</div>
		<div class="modal-body">
			<div class="row-fluid">
				<div class="span12">
					<div class="control-group">
						<label class="control-label"><?php echo $this->lang->line('berkas_nominal'); ?></label>
						<div class="controls">
							<input type="text" name="nominal" id="nominal" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label"><?php echo $this->lang->line('global_note'); ?></label>
						<div class="controls">
							<textarea rows="5" style="width: 60%;font-size: 12px;" id="note" name="note"></textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label"><?php echo $this->lang->line('berkas_payment_date'); ?></label>
						<div class="controls">
							<input type="text" id="payment_date" name="payment_date" class="m-wrap m-ctrl-medium date-picker" size="16" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="berkas_id" value="<?php echo $res_berkas['id']; ?>" />
			<button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
			<button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
		</div>
	</form>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade in" id="input_doc_sap_modal">
    <form id="input_doc_sap_form" class="form-horizontal" method="post" autocomplete="off">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
            <h3><?php echo $this->lang->line('global_edit'); ?> :</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <label class="control-label">No Doc SAP </label>
                        <div class="controls">
                            <input type="text" name="no_doc_sap" id="no_doc_sap" class="span12" value="<?php echo $res_berkas['no_doc_sap'] ? $res_berkas['no_doc_sap'] : '' ; ?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="berkas_id" name="berkas_id" value="<?php echo $res_berkas['id']; ?>" />
            <button type="submit" class="btn blue" id="submit_doc_sap"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
            <button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
        </div>
    </form>
</div>

<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade in" id="input_hard_copy">
    <form id="hard_copy_form" class="form-horizontal" method="post" autocomplete="off">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
            <h3><?php echo $this->lang->line('berkas_hard_copy'); ?> :</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <label class="control-label">Hard Copy Datang</label>
                        <div class="controls">
                            <input type="text" name="hard_copy_arrived" id="hard_copy_arrived" class="m-wrap m-ctrl-medium date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" value="<?php echo $res_berkas['hard_copy_arrived'] ? $res_berkas['hard_copy_arrived'] : '' ; ?>"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Status Hard Copy</label>
                        <div class="controls">
							<?php if(isset($res_berkas['hard_copy_status']) && ! empty($res_berkas['hard_copy_status'])): ?>
                                <select id="hard_copy_status" name="hard_copy_status" class="span12">
                                    <option value="">Pilih</option>
									<?php foreach($res_hard_copy as $dt): ?>
                                        <option <?php echo isset($res_berkas) && $res_berkas['hard_copy_status'] == $dt['name'] ? 'selected="selected"': ''; ?> value="<?php echo $dt['name']; ?>"><?php echo $dt['name']; ?></option>
									<?php endforeach; ?>
                                </select>
							<?php else :?>
                                <select name="hard_copy_status" id="hard_copy_status" class="span12">
                                    <option value="">Pilih</option>
                                    <option value="DISETUJUI">DISETUJUI</option>
                                    <option value="DITOLAK">DITOLAK</option>
                                </select>
							<?php endif; ?>
                        </div>
                    </div>
                    <div class="control-group declined">
                        <label class="control-label">Alasan Penolakan</label>
                        <div class="controls">
							<?php if(isset($res_berkas['declined_reason']) && ! empty($res_berkas['declined_reason'])): ?>
                                <select id="declined_reason" name="declined_reason" class="span12">
                                    <option value="">Pilih</option>
									<?php foreach($res_declined_reason as $dt): ?>
                                        <option <?php echo isset($res_berkas) && $res_berkas['declined_reason'] == $dt['name'] ? 'selected="selected"': ''; ?> value="<?php echo $dt['name']; ?>"><?php echo $dt['name']; ?></option>
									<?php endforeach; ?>
                                </select>
							<?php else :?>
                                <select name="declined_reason" id="declined_reason" class="span12">
                                    <option value="">Pilih</option>
                                    <option value="Berkas tidak lengkap">Berkas tidak lengkap</option>
                                    <option value="Berkas tidak sesuai dengan soft copy">Berkas tidak sesuai dengan soft copy </option>
                                    <option value="BAP dan/atau BASTB/BAPS tidak sesuai dengan dokumen swift">BAP dan/atau BASTB/BAPS tidak sesuai dengan dokumen swift</option>
                                    <option value="Tanggal BAP dan/atau BASTB/BAPS tidak conform dengan tanggal dokumen diterima">Tanggal BAP dan/atau BASTB/BAPS tidak conform dengan tanggal dokumen diterima</option>
                                    <option value="Lain-Lain">Lain - Lain</option>
                                </select>
							<?php endif; ?>
                        </div>
                    </div>
                    <div class="control-group approved">
                        <label class="control-label">Hard Copy Approved</label>
                        <div class="controls">
                            <input type="text" name="hard_copy_approved" id="hard_copy_approved" class="m-wrap m-ctrl-medium date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" value="<?php echo $res_berkas['hard_copy_approved'] ? $res_berkas['hard_copy_approved'] : '' ; ?>"/>
                        </div>
                    </div>
                    <div class="control-group declined-not-required">
                        <label class="control-label"><?php echo $this->lang->line('berkas_hard_copy_description2'); ?> (optional)</label>
                        <div class="controls">
                            <textarea rows="5" class="span12" id="hard_copy_description" name="hard_copy_description"><?php echo isset($res_berkas['declined_text']) && ! empty($res_berkas['declined_text']) ? $res_berkas['declined_text'] : '' ; ?></textarea>
                        </div>
                    </div>
                    <div class="control-group declined-required">
                        <label class="control-label"><?php echo $this->lang->line('berkas_hard_copy_description2'); ?> (mandatory)</label>
                        <div class="controls">
                            <textarea rows="5" class="span12" id="hard_copy_description_required" name="hard_copy_description_required"><?php echo isset($res_berkas['declined_text']) && ! empty($res_berkas['declined_text'])  ? $res_berkas['declined_text'] : '' ; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="berkas_id" name="berkas_id" value="<?php echo $res_berkas['id']; ?>" />
            <button type="submit" class="btn blue" id="submit_hard_copy"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
            <button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
        </div>
    </form>
</div>

<input type="hidden" id="urlBack" name="urlBack" value="<?php echo ! empty($url_back) ? base64_decode($url_back).base64_encode($res_berkas['project_code']) : ""; ?>" />
<script>
	jQuery(document).ready(function(){
		
		if(jQuery('#payment_status').val()){
			get_pembayaran_hutang();
			jQuery('.blink').pulsate({
				color: "#35aa47"
			});	
		}
		
		var $table = jQuery('table#table_detail_pembayaran');
		$table.floatThead({
			scrollContainer: function($table){
				return $table.closest('.modal-body');
			}
		});
		
		jQuery('.icon-money').click(function() {
			jQuery('#detail_pembayaran_modal').modal('show');
		});

        jQuery('#edit_doc_sap').click(function() {
            jQuery('#input_doc_sap_modal').modal('show');
        });
		
		jQuery('#detail_pembayaran_modal').on('shown', function () {});
		
		jQuery('.button-reject').click(function() {
			jQuery('#rejected_note_modal').modal('show');
		});
		
		if (!jQuery().bootstrapWizard) {
			return;
		}
		
		// default form wizard
		$('#rootwizard').bootstrapWizard({
			'nextSelector': '.button-next',
			onTabClick: function (tab, navigation, index) {
				return false;
			},
			onNext: function (tab, navigation, index) {
				var li_list = navigation.find('li');
				var total = navigation.find('li').length;
				var current = index + 1;
						
				var confirmed = confirm('<?php echo $this->lang->line('berkas_confirm_approve'); ?>');
				
				if(confirmed) {
					var data = {
						berkas_id: jQuery('#berkas_id').val(),
						verify_level_index: jQuery('#verify_level_index').val(),
						payment_method: jQuery('#payment_method').val(),
						verify_status_id: 2
					};
					
					fnSubmitApproval(data);
					
					var $percent = (current / total) * 100;
					$('#rootwizard').find('.bar').css({
						width: $percent + '%'
					});
					
					// set done steps
					jQuery('li', $('#rootwizard')).removeClass("done");
					for (var i = 0; i < index; i++) {
						jQuery(li_list[i]).addClass("done");
					}
				}
				
				return confirmed;
			},
			onPrevious: function (tab, navigation, index) {
			},
			onTabShow: function (tab, navigation, index) {
				if(jQuery('#berkas_status').val() != refBerkasStatus[2]) {
					var total = navigation.find('li').length;
					var current = index + 1;
					
					var $percent = (current / total) * 100;
					$('#rootwizard').find('.bar').css({
						width: $percent + '%'
					});
				}
			}
		});
		
		// init form rejected note
		
		var rules = {};
		var messages = {};
		
		rules["rejected_note"] = { required: true};
		messages["rejected_note"] = { required: "<?php echo $this->lang->line('global_error_required_field2'); ?>" };
		
		jQuery("#rejected_note-form").validate({
			onsubmit: false,
			ignore: "",
			rules: rules,
			messages: messages,
			highlight: function(label) {},
			success: function(label) {},
			invalidHandler: function(e, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error_form'); ?>',
						sticky: false,
						time: 3000
					});
					return false;
				}
				e.preventDefault();
			}
		});
		
		jQuery('#rejected_note-form').submit(function(e) {
			if(jQuery(this).valid()){
				jQuery('#rejected_note_modal').modal('hide');
				var data = jQuery(this).serialize();
				fnSubmitApproval(data);
			}
			e.preventDefault();
		});
		
		function fnSubmitApproval(data) {
			jQuery('#button-approval').each(function(){
				jQuery(this).find(':button').attr("disabled","disabled");
			});
			
			App.blockUI(jQuery("#main-container"), true, '<?php echo $this->lang->line('global_please_wait'); ?>');
			var cAjax = new ceki.fnAjax({
				url : APP_BACKEND_URL + 'main/berkas/approval',
				data : data,
				successCallBack : function() {
					
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_success_submited'); ?>',
						sticky: false,
						time: 3000
					});
					
					if(typeof sock != 'undefined' && IS_SOCK == 1) {
						sock.send();
					}
					
					setTimeout(function(){
						ceki.fnLocation('<?php echo app_backend_url('main/berkas'); ?>');
					},5000);
				},
				successErrorCallBack : function(obj) {
					App.unblockUI(jQuery("#main-container"));
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: obj.message,
						sticky: false,
						time: 3000
					});
				}
			});
		}
		
		// init form pembayaran
		
		jQuery('.date-picker').datepicker({
			format: 'dd-mm-yyyy'
		});
		
		jQuery.validator.addMethod(
			"indonesianDate",
			function(value, element) {
				return value.match(/^\d\d?\-\d\d?\-\d\d\d\d$/);
			},
			"<?php echo $this->lang->line('global_error_date'); ?>"
		);
		
		jQuery("#nominal").inputmask('999.999.999.999.999', { 
			numericInput: true, 
			rightAlignNumerics: false, 
			greedy: true,
			clearMaskOnLostFocus: true
		});
		
		var rules = {};
		var messages = {};
		
		rules["nominal"] = { required: true };
		messages["nominal"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>"
		};
		
		rules["payment_date"] = { required: true, indonesianDate: true };
		messages["payment_date"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
		};
		
		rules["note"] = { required: false };
		messages["note"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>"	
		};
		
		jQuery("#add_edit-form").validate({
			onsubmit: false,
			ignore: "",
			rules: rules,
			messages: messages,
			highlight: function(label) {},
			success: function(label) {},
			invalidHandler: function(e, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error_form'); ?>',
						sticky: false,
						time: 3000
					});
					return false;
				}
				e.preventDefault();
			}
		});
	
		jQuery('#add_edit-form').submit(function(e) {
			if(jQuery(this).valid()){
				App.blockUI(jQuery("#input_pembayaran_modal"), true, '<?php echo $this->lang->line('global_saving_data'); ?>');
				var cAjax = new ceki.fnAjax({
					url :  APP_BACKEND_URL + 'main/berkas/save_pembayaran',
					data : jQuery(this).serialize(),
					successCallBack : function(obj) {
						jQuery('#input_pembayaran_modal').modal('hide');
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_success_update'); ?>',
							sticky: false,
							time: 3000
						});
						App.unblockUI(jQuery("#input_pembayaran_modal"));
						get_pembayaran_hutang();
					},
					successErrorCallBack : function(obj) {
						if(jQuery("#alert").length > 0) {
							jQuery("#alert").remove();  
						}
						msg = '<div id="alert" class="alert alert-error"><button data-dismiss="alert" class="close" type="button"></button>'+obj.message+'</div>';
						jQuery("#input_pembayaran_modal.modal-header").after(msg);
						
						App.unblockUI(jQuery("#input_pembayaran_modal"));
					}
				});
			}
			e.preventDefault();
		});
		
		jQuery('#button-add').click(function(){
			fnEmptyForm();
		});
		
		jQuery('.icon-edit').live('click',function(){
			var pembayaranId = jQuery(this).attr('data-id');
			var nominal = jQuery(this).attr('data-nominal');
			var note = jQuery(this).attr('data-note');
			var paymentDate = jQuery(this).attr('data-paymentDate');
			
			jQuery('#nominal').val(nominal);
			jQuery('#note').val(note);
			jQuery('#payment_date').val(paymentDate);
			
			if(jQuery("#pembayaran_id").length > 0) {
				jQuery("#pembayaran_id").remove();  
			}
			jQuery('#add_edit-form').append('<input type="hidden" id="pembayaran_id" name="pembayaran_id" value="'+pembayaranId+'" />');
			
			jQuery('#input_pembayaran_modal').modal('show');
		});
			
		jQuery('.icon-trash').live('click',function(){
			var pembayaranId = jQuery(this).attr('data-id');
			jConfirm('<?php echo $this->lang->line('global_confirm_delete'); ?>', '<?php echo $this->lang->line('global_confirm_delete_header'); ?>', function(r) {
				if(r){
					var cAjax = new ceki.fnAjax({
						url :  APP_BACKEND_URL + 'main/berkas/delete_pembayaran',
						data : ({
							id: pembayaranId
						}),
						successCallBack : function() {
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_success_delete'); ?>',
								sticky: false,
								time: 3000
							});
							
							get_pembayaran_hutang();
						}
					});
				}
			});
		});
				
		function fnEmptyForm(){
			if(jQuery("#pembayaran_id").length > 0) {
				jQuery("#pembayaran_id").remove();  
			}
			
			if(jQuery("#alert").length > 0) {
				jQuery("#alert").remove();  
			}
			
			jQuery('#nominal').val('');
			jQuery('#note').val('');
			jQuery('#payment_date').val('');
			
			jQuery('#add_edit-form')[0].reset();
		}
		
		jQuery('#btn_back').click(function(e) {
			if(jQuery('#urlBack').val() != '') {
				ceki.fnLocation(jQuery('#urlBack').val());
			} else {
				ceki.fnLocation('<?php echo app_backend_url('main/berkas'); ?>');
			}
		});
		
		function get_pembayaran_hutang() {
			App.blockUI('#table_detail_pembayaran', true, '<?php echo $this->lang->line('global_loading_data'); ?>');
			jQuery.ajax({
				url :  APP_BACKEND_URL + 'main/berkas/get_pembayaran_hutang',
				type: 'POST',
				data : ({
					berkas_id: jQuery('#berkas_id').val(),
				}),
				dataType: 'JSON',
				success: function (obj) {
					if(obj.success == true) {
						var data = obj.data;
						var data2 = obj.data2;
						htmlData = "";
						for(var i = 0; i < data.length; i++) {
							htmlData += '<tr>' +
								'<td style="width:10%;text-align:center;">'+ data[i].termin +'</td>' +
								'<td style="width:15%;text-align:left;">'+ data[i].nominal +'</td>' +
								'<td style="width:30%;text-align:left;" class="hidden-480">'+ data[i].note +'</td>' +
								'<td style="width:15%;text-align:center;" class="hidden-480">'+ data[i].payment_date +'</td>' +
								'<td style="width:15%;text-align:center;" class="hidden-480">'+ data[i].invoice_date +'</td>' +
								'<td style="width:10%;text-align:center;"><span class="icon-trash" data-id="'+ data[i].pembayaran_id +'"></span> | <span class="icon-edit" data-id="'+ data[i].pembayaran_id +'" data-nominal="'+ data[i].nominal +'" data-note="'+ data[i].note +'" data-paymentDate="'+ data[i].payment_dates +'"></span></td>' +
							'</tr>';
						}
						
						jQuery('#table_detail_pembayaran').find('tbody').html('');
						jQuery('#table_detail_pembayaran').find('tbody').append(htmlData);	
						jQuery('#total_pembayaran').text(data2[0]);
						jQuery('#total_hutang_valid').text(data2[1]);
						
						var total_hutang_valid = data2[1].replace(/\./g,'');
						if(total_hutang_valid < 0){
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('berkas_total_hutang_valid_minus'); ?>',
								sticky: false,
								time: 3000
							});
						}
						
						jQuery('#total_nominal_pembayaran').text(data2[0]);
						
						App.unblockUI(jQuery("#table_detail_pembayaran"));
						
						
					} else {
						App.unblockUI(jQuery("#table_detail_pembayaran"));
					}
				} 
			});
		}
		
		jQuery('#button-refresh').click(function(){
			get_pembayaran_hutang();
		});
		
		$blinking = jQuery('#label-done');
		setInterval(function() {
			visibility = $blinking.css('visibility');

			if (visibility === 'hidden') {
				$blinking.css('visibility', 'visible');
			} else {
				$blinking.css('visibility', 'hidden');
			}
		}, 1000);
		
		jQuery('#update_hard_copy').live('click', function() {
			var berkas_id = jQuery('#berkas_id').val();
			var value = jQuery(this).hasClass('icon-remove') ? 1 : 0;
			
			jQuery.alerts.okButton = '<?php echo $this->lang->line('global_yes'); ?>';
			jQuery.alerts.cancelButton = '<?php echo $this->lang->line('global_noo'); ?>';
			jConfirm('<?php echo $this->lang->line('global_confirm_change_status'); ?>', '<?php echo $this->lang->line('global_confirm_change_status_header'); ?>', function(r) {
				if(r) {
					App.blockUI('.main-container', true, '<?php echo $this->lang->line('global_please_wait'); ?>');
					var cAjax = new ceki.fnAjax({
						url :  "<?php echo app_backend_url('main/berkas/update_hard_copy'); ?>",
						data : {
							berkas_id: berkas_id,
							value: value
						},
						successCallBack : function(obj) {
							App.unblockUI(jQuery(".main-container"));
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_success_update'); ?>',
								sticky: false,
								time: 3000
							});
							
							if(value == 1) {
								jQuery('#update_hard_copy').removeClass('icon-remove');
								jQuery('#update_hard_copy').addClass('icon-ok');
							} else {
								jQuery('#update_hard_copy').removeClass('icon-ok');
								jQuery('#update_hard_copy').addClass('icon-remove');
							}
							
						}, 
						successErrorCallBack : function(obj) {
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_error'); ?>',
								sticky: false,
								time: 3000
							});
							App.unblockUI(jQuery(".main-container"));
						}
					});
				}
			});
		});

        var piRules = {};
        var piMessages = {};

        piRules["no_doc_sap"] = { required: true, };
        piMessages["no_doc_sap"] = { required: "<?php echo $this->lang->line('global_error_required_field2'); ?>" };

        jQuery("#input_doc_sap_form").validate({
            onsubmit: false,
            ignore: "",
            rules: piRules,
            messages: piMessages,
            highlight: function(label) {},
            success: function(label) {},
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: '<?php echo $this->lang->line('global_error_form'); ?>',
                        sticky: false,
                        time: 3000
                    });
                    return false;
                }
                e.preventDefault();
            }
        });

        jQuery('#input_doc_sap_form').submit(function(e) {
            if(jQuery(this).valid()){
                jQuery('#input_doc_sap_modal').modal('hide');
                var data = jQuery(this).serialize();
                fnSubmitDocSAP(data);
            }
            e.preventDefault();
        });

        function fnSubmitDocSAP(data) {
            jQuery('#submit_doc_sap').each(function(){
                jQuery(this).find(':button').attr("disabled","disabled");
            });

            App.blockUI(jQuery("#main-container"), true, '<?php echo $this->lang->line('global_please_wait'); ?>');
            var cAjax = new ceki.fnAjax({
                url : APP_BACKEND_URL + 'main/berkas/submit_doc_sap',
                data : data,
                successCallBack : function() {
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: '<?php echo $this->lang->line('global_success_submited'); ?>',
                        sticky: false,
                        time: 3000
                    });
                    App.unblockUI(jQuery("#main-container"));
                    setTimeout(function(){
                        location.reload(true);
                    },1000);
                },
                successErrorCallBack : function(obj) {
                    App.unblockUI(jQuery("#main-container"));
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: obj.message,
                        sticky: false,
                        time: 3000
                    });
                }
            });
        }

        $('#btn_copy').click(function () {
            var copyText = document.getElementById("faktur_pajak_url");
            copyText.select();
            document.execCommand("copy");
            jQuery.gritter.add({
                title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                text: 'Berhasil Dicopy',
                sticky: false,
                time: 3000
            });
        });

        $('#btn_copy_2').click(function () {
            var copyText = document.getElementById("faktur_pajak_url_2");
            copyText.select();
            document.execCommand("copy");
            jQuery.gritter.add({
                title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                text: 'Berhasil Dicopy',
                sticky: false,
                time: 3000
            });
        });

        jQuery('#edit_hard_copy').click(function() {
            jQuery('#input_hard_copy').modal('show');
        });

        var hardCopyRules = {};
        var hardCopyMessages = {};

        hardCopyRules["hard_copy_arrived"] = { indonesianDate: true };
        hardCopyMessages["hard_copy_arrived"] = {
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        hardCopyRules["hard_copy_approved"] = { indonesianDate: true };
        hardCopyMessages["hard_copy_approved"] = {
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        hardCopyRules["declined_reason"] = { required: true };
        hardCopyMessages["declined_reason"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>"
        };

        hardCopyRules["hard_copy_description_required"] = { required: true };
        hardCopyMessages["hard_copy_description_required"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>"
        };

        $('#hard_copy_status').change(function () {
            if(this.value == 'DISETUJUI'){
                $('.approved').each(function () {
                    $(this).show()
                });
                $('.declined').find(':input').each(function () {
                    $(this).prop('disabled', true)
                });
                $('.declined').each(function () {
                    $(this).hide()
                });

                $('.declined-not-required').hide();
                $('.declined-required').hide();
                $('.declined-required').find(':input').each(function () {
                    $(this).prop('disabled', true)
                });

            }else if(this.value == 'DITOLAK'){
                $('.declined').each(function () {
                    $(this).show()
                });
                $('.declined').find(':input').each(function () {
                    $(this).prop('disabled', false)
                });
                $('.approved').each(function () {
                    $(this).hide()
                });

                $('.declined-not-required').show();
                $('.declined-required').hide();
                $('.declined-required').find(':input').each(function () {
                    $(this).prop('disabled', true)
                });

            }else{
                $('.approved').each(function () {
                    $(this).hide()
                });

                $('.declined').each(function () {
                    $(this).hide()
                });

                $('.declined').find(':input').each(function () {
                    $(this).prop('disabled', true)
                });

                $('.declined-not-required').hide();
                $('.declined-required').hide();
                $('.declined-required').find(':input').each(function () {
                    $(this).prop('disabled', true)
                });
            }
        })

        if($('#hard_copy_status option:selected').val() == 'DISETUJUI' ){
            $('.declined-not-required').hide();
            $('.declined-required').hide();
            $('.declined-required').find(':input').each(function () {
                $(this).prop('disabled', true)
            });

            $('.approved').each(function () {
                $(this).show()
            });
            $('.declined').each(function () {
                $(this).hide()
            });
            $('.declined').find(':input').each(function () {
                $(this).prop('disabled', true)
            });

        }else if($('#hard_copy_status option:selected').val() == 'DITOLAK' ){
            $('.declined').each(function () {
                $(this).show()
            });
            $('.approved').each(function () {
                $(this).hide()
            });
            $('.declined').find(':input').each(function () {
                $(this).prop('disabled', false)
            });

            $('.declined-not-required').show();
            $('.declined-required').hide();
            $('.declined-required').find(':input').each(function () {
                $(this).prop('disabled', true)
            });

        }else{
            $('.approved').each(function () {
                $(this).hide()
            });
            $('.declined').each(function () {
                $(this).hide()
            });

            $('.declined').find(':input').each(function () {
                $(this).prop('disabled', true)
            });

            $('.declined-not-required').hide();
            $('.declined-required').hide();
            $('.declined-required').find(':input').each(function () {
                $(this).prop('disabled', true)
            });
        }

        $('#declined_reason').change(function () {
            console.log(this.value);
            if(this.value == 'Lain-Lain'){
                $('.declined-required').show()
                $('.declined-not-required').hide()
                $('.declined-required').find(':input').each(function () {
                    $(this).prop('disabled', false)
                });
            }else if(this.value == ''){
                $('.declined-required').hide()
                $('.declined-not-required').show()
                $('.declined-required').find(':input').each(function () {
                    $(this).prop('disabled', true)
                });
            }else{
                $('.declined-required').hide()
                $('.declined-not-required').show()
                $('.declined-required').find(':input').each(function () {
                    $(this).prop('disabled', true)
                });
            }

            $('.declined-required, .declined-not-required').find(':input').each(function () {
                $(this).text('');
            })
        })

        if($('#declined_reason option:selected').val() == 'Lain-Lain' ){
            $('.declined-required').show();
            $('.declined-not-required').hide()
            $('.declined-required').find(':input').each(function () {
                $(this).prop('disabled', false)
            });

        }else if($('#declined_reason option:selected').val() == '' ){
            $('.declined-required').hide()
            $('.declined-required').find(':input').each(function () {
                $(this).prop('disabled', true)
            });

        }else{
            $('.declined-required').hide();
            $('.declined-not-required').show()
            $('.declined-required').find(':input').each(function () {
                $(this).prop('disabled', true)
            });
        }

        jQuery("#hard_copy_form").validate({
            onsubmit: false,
            ignore: "",
            rules: hardCopyRules,
            messages: hardCopyMessages,
            highlight: function(label) {},
            success: function(label) {},
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: '<?php echo $this->lang->line('global_error_form'); ?>',
                        sticky: false,
                        time: 3000
                    });
                    return false;
                }
                e.preventDefault();
            }
        });

        jQuery('#hard_copy_form').submit(function(e) {
            if(jQuery(this).valid()){
                jQuery('#input_hard_copy').modal('hide');
                var data = jQuery(this).serialize();
                fnSubmitHardCopy(data);
            }
            e.preventDefault();
        });

        function fnSubmitHardCopy(data) {
            jQuery('#submit_hard_copy').each(function(){
                jQuery(this).find(':button').attr("disabled","disabled");
            });

            App.blockUI(jQuery("#main-container"), true, '<?php echo $this->lang->line('global_please_wait'); ?>');
            var cAjax = new ceki.fnAjax({
                url : APP_BACKEND_URL + 'main/berkas/submit_hard_copy',
                data : data,
                successCallBack : function() {
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: '<?php echo $this->lang->line('global_success_submited'); ?>',
                        sticky: false,
                        time: 3000
                    });
                    App.unblockUI(jQuery("#main-container"));
                    setTimeout(function(){
                        location.reload(true);
                    },1000);
                },
                successErrorCallBack : function(obj) {
                    App.unblockUI(jQuery("#main-container"));
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: obj.message,
                        sticky: false,
                        time: 3000
                    });
                }
            });
        }
	});
</script>