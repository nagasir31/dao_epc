<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
div.tagsinput {
	height: 85px !important;
}

.form-horizontal .control-label {
	width:250px;
	margin-left: -10px;
}

.form-horizontal .controls {
    padding-left: 100px;
}


#upload_field .control-group .control-label   {
	width:350px;
}

#upload_field .controls {
	padding-left: 200px;
}

.validate-inline {
	color: #b94a48;
}

body .modal#qrcode_modal {
    width: 30%; /* desired relative width */
    left: 35%; /* (100%-width)/2 */
    /* place center */
    margin-left:auto;
    margin-right:auto;
}

body .modal#qrcode_modal_2 {
    width: 30%; /* desired relative width */
    left: 35%; /* (100%-width)/2 */
    /* place center */
    margin-left:auto;
    margin-right:auto;
}

.form-horizontal .custom-form .controls{
    padding-left: 0px;
    margin-left: 0px;
}

.form-horizontal .custom-form .control-label{
    width: auto;
    margin-left: 0px;
}

.form-horizontal .custom-form .control-group{
    margin-bottom: 5px !important;
}


</style>
<div id="main-container">
	<div class="portlet box blue" id="form_wizard_1">
		<div class="portlet-title">
			<div class="caption"><i class="icon-credit-card"></i><?php echo $this->lang->line('global_form'); ?> <?php echo $this->lang->line('berkas_input'); ?></div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form id="form_save" class="form-horizontal" action="#">
				<div class="form-wizard">
					<div class="navbar steps">
						<div class="navbar-inner">
							<ul class="row-fluid">
								<li class="span2 offset1">
									<a href="#tab1" data-toggle="tab" class="step">
									<span class="number">1</span>
									<span class="desc"><i class="icon-ok"></i> <?php echo $this->lang->line('global_data'); ?> <?php echo $this->lang->line('berkas_name'); ?></span>   
									</a>
								</li>
								
								<li class="span2">
									<a href="#tab2" data-toggle="tab" class="step">
									<span class="number">2</span>
									<span class="desc"><i class="icon-ok"></i> <?php echo $this->lang->line('global_data'); ?> <?php echo $this->lang->line('berkas_vendor'); ?></span>   
									</a>
								</li>
								
								<li class="span2">
									<a href="#tab3" data-toggle="tab" class="step">
									<span class="number">3</span>
									<span class="desc"><i class="icon-ok"></i> <?php echo $this->lang->line('global_data'); ?> <?php echo $this->lang->line('berkas_biaya_pajak'); ?></span>   
									</a>
								</li>
								
								<li class="span2">
									<a href="#tab4" data-toggle="tab" class="step">
									<span class="number">4</span>
									<span class="desc"><i class="icon-ok"></i> <?php echo $this->lang->line('berkas_files'); ?></span>   
									</a>
								</li>
								
								<li class="span2">
									<a href="#tab5" data-toggle="tab" class="step">
									<span class="number">5</span>
									<span class="desc"><i class="icon-ok"></i> <?php echo $this->lang->line('global_confirmation'); ?></span>   
									</a> 
								</li>
							</ul>
						</div>
					</div>
					<div id="bar" class="progress progress-success progress-striped">
						<div class="bar"></div>
					</div>
					<div class="tab-content">
						<div class="alert alert-error hide">
							<button class="close" data-dismiss="alert"></button>
							<?php echo $this->lang->line('global_error_form'); ?>
						</div>
						<div class="alert alert-success hide"></div>
						
						<?php echo validation_errors(); ?>
						<?php if($this->session->flashdata('success_alert')): ?>
							<div class="alert alert-success">
								<button data-dismiss="alert" class="close"></button>
								<?php echo $this->session->flashdata('success_alert'); ?>
							</div>
						<?php endif; ?>
						<?php if($this->session->flashdata('error_alert')): ?>
							<div class="alert alert-error">
								<button data-dismiss="alert" class="close"></button>
								<?php echo $this->session->flashdata('error_alert'); ?>
							</div>
						<?php endif; ?>
						
					
						<div class="tab-pane" id="tab1">
							<h4 class="form-section"><?php echo $this->lang->line('global_detail').' '.$this->lang->line('berkas_name'); ?></h4>
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_number'); ?> <span class="required"> *</span></label>
								<div class="controls">
									<input placeholder="<?php echo $this->lang->line('berkas_number'); ?>" type="text" id="berkas_number" name="berkas_number" value="<?php echo isset($res_data['berkas_number']) ? $res_data['berkas_number'] : ''; ?>" class="m-wrap span8" MAXLENGTH="8"/>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_ttap_date'); ?> <span class="required"> *</span></label>
								<div class="controls">
									<input type="text" id="ttap_date" name="ttap_date" value="<?php echo isset($res_data['ttap_date']) ? $res_data['ttap_date'] : date("d-m-Y"); ?>" class="m-wrap m-ctrl-medium date-picker" size="16" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_project_name'); ?> <span class="required"> *</span></label>
								<div class="controls">
									<?php if(isset($res_data['revision']) && $res_data['revision'] > 0): ?>
									<input type="text" id="project_code" name="project_code" readonly="readonly" value="<?php echo isset($res_data['project_code']) ? $res_data['project_code'] : ''; ?>" class="m-wrap span8" />
									<?php else: ?>
									<select id="project_code" name="project_code" class="m-wrap span8">
										<option value=""><?php echo $this->lang->line('global_choose_one'); ?></option>
										<?php foreach($ref_proyek as $dt): ?>
										<option <?php echo isset($res_data) && $res_data['project_code'] == $dt['id'] ? 'selected="selected"': ''; ?> value="<?php echo $dt['id']; ?>"><?php echo $dt['name']; ?> (<?php echo $dt['id']; ?>)</option>
										<?php endforeach; ?>
									</select>
									<?php endif; ?>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_payment_method'); ?> <span class="required"> *</span></label>
								<div class="controls">
									<?php if(isset($res_data['revision']) && $res_data['revision'] > 0): ?>
									<input type="text" id="payment_method" name="payment_method" readonly="readonly" value="<?php echo isset($res_data['payment_method']) ? $res_data['payment_method'] : ''; ?>" class="m-wrap span8" />
									<?php else: ?>
									<select id="payment_method" name="payment_method" class="m-wrap span8">
										<?php foreach($ref_payment_method as $dt): ?>
										<option <?php echo isset($res_data) && $res_data['payment_method'] == $dt['name'] ? 'selected="selected"': ''; ?> value="<?php echo $dt['name']; ?>"><?php echo $dt['name']; ?></option>
										<?php endforeach; ?>
									</select>
									<?php endif; ?>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_invoice_number_date'); ?> </label>
								<div class="controls">
									<input placeholder="<?php echo $this->lang->line('berkas_invoice_number'); ?>" type="text" id="invoice_number" name="invoice_number" value="<?php echo isset($res_data['invoice_number']) ? $res_data['invoice_number'] : ''; ?>" class="m-wrap large" />
									&nbsp;
									<input type="text" id="invoice_date" name="invoice_date" value="<?php echo isset($res_data['invoice_date']) ? $res_data['invoice_date'] : ''; ?>" class="m-wrap m-ctrl-medium date-picker" size="16" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_bap_bpg_dub_number_date'); ?></label>
								<div class="bap_bpg_dub">
									<?php if(isset($res_data['bap_bpg_dub_number']) && ! empty($res_data['bap_bpg_dub_number'])): ?>
										<?php foreach($res_data['bap_bpg_dub_number'] as $key => $val): ?>
										<div class="controls">
											<input type="text" name="bap_bpg_dub_number[]" value="<?php echo $val; ?>" class="m-wrap large" />
											&nbsp;
											<input type="text" name="bap_bpg_dub_date[]" value="<?php echo isset($res_data['bap_bpg_dub_date'][$key]) ? $res_data['bap_bpg_dub_date'][$key] : ''; ?>" class="m-wrap m-ctrl-medium date-picker" size="16" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" />
											<?php if($key != 0): ?>
											&nbsp;<i class="icon-minus remove_bap_bpg_dub"></i>
											<?php endif; ?>
											<br><br>
										</div>
										<?php endforeach; ?>
									<?php else: ?>
									<div class="controls">
										<input type="text" name="bap_bpg_dub_number[]" class="m-wrap large" />
										&nbsp;
										<input type="text" name="bap_bpg_dub_date[]" class="m-wrap m-ctrl-medium date-picker" size="16" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" />
										<br><br>
									</div>
									<?php endif; ?>
								</div>
								<div class="controls">
									<span class="span6" style=""><button type="button" class="btn" id="add_bap_bpg_dub"  style="float:left;"><i class="icon-plus"></i></button></span>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_surat_jalan_lkp_number_date'); ?></label>
								<div class="controls">
									<input type="text" id="surat_jalan_lkp_number" name="surat_jalan_lkp_number" value="<?php echo isset($res_data['surat_jalan_lkp_number']) ? $res_data['surat_jalan_lkp_number'] : ''; ?>" class="m-wrap large" />
									&nbsp;
									<input type="text" id="surat_jalan_lkp_date" name="surat_jalan_lkp_date" value="<?php echo isset($res_data['surat_jalan_lkp_date']) ? $res_data['surat_jalan_lkp_date'] : ''; ?>" class="m-wrap m-ctrl-medium date-picker" size="16" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_spk_po_number_date'); ?></label>
								<div class="controls">
									<input type="text" id="spk_po_number" name="spk_po_number" value="<?php echo isset($res_data['spk_po_number']) ? $res_data['spk_po_number'] : ''; ?>" class="m-wrap large" />
									&nbsp;
									<input type="text" id="spk_po_date" name="spk_po_date" value="<?php echo isset($res_data['spk_po_date']) ? $res_data['spk_po_date'] : ''; ?>" class="m-wrap m-ctrl-medium date-picker" size="16" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" />
								</div>
							</div>

                            <div class="control-group">
                                <label class="control-label">No. Doc SAP</label>
                                <div class="controls">
                                    <input type="text" id="no_doc_sap" name="no_doc_sap" value="<?php echo isset($res_data['no_doc_sap']) ? $res_data['no_doc_sap'] : ''; ?>" class="m-wrap large" />
                                </div>
                            </div>
						</div>
						
						<div class="tab-pane" id="tab2">
							<h4 class="form-section"><?php echo $this->lang->line('global_detail').' '.$this->lang->line('berkas_vendor'); ?></h4>
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_vendor_type'); ?> <span class="required"> *</span></label>
								<div class="controls">
									<?php if(isset($res_data['revision']) && $res_data['revision'] > 0): ?>
									<input type="text" id="vendor_type" name="vendor_type" readonly="readonly" value="<?php echo isset($res_data['vendor_type']) ? $res_data['vendor_type'] : ''; ?>" class="m-wrap span8" />
									<?php else: ?>
									<select id="vendor_type" name="vendor_type" class="m-wrap span8">
										<option value=""><?php echo $this->lang->line('global_choose_one'); ?></option>
										<?php foreach($ref_vendor_type as $dt): ?>
										<option data-code="<?php echo $dt['code']; ?>" data-id="<?php echo $dt['id']; ?>" <?php echo isset($res_data) && $res_data['vendor_type'] == $dt['name'] ? 'selected="selected"': ''; ?> value="<?php echo $dt['name']; ?>"><?php echo $dt['name']; ?></option>
										<?php endforeach; ?>
									</select>
									<?php endif; ?>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_vendor_name'); ?> <span class="required"> *</span></label>
								<div class="controls">
									<input placeholder="<?php echo $this->lang->line('berkas_vendor_name'); ?>" type="text" id="vendor_name" name="vendor_name" value="<?php echo isset($res_data['vendor_name']) ? $res_data['vendor_name'] : ''; ?>" class="m-wrap span7" />
									<span id="vendor_tools">
										<a href="#" id="btn_vendor" class="btn"><i id="search_vendor" class="icon-ok"></i> <?php echo $this->lang->line('global_search'); ?> <?php echo $this->lang->line('berkas_vendor'); ?></a>
										<a href="#" id="btn_vendor_clear" class="btn"  style="display:none;"><i id="clear_vendor" class="icon-remove"></i></a>
									</span>
									<input type="hidden" id="ref_vendor_id" name="ref_vendor_id" value="<?php echo isset($res_data['ref_vendor_id']) ? $res_data['ref_vendor_id'] : ''; ?>" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_vendor_code'); ?></label>
								<div class="controls">
									<input placeholder="" type="text" id="vendor_code" name="vendor_code" value="<?php echo isset($res_data['vendor_code']) ? $res_data['vendor_code'] : ''; ?>" class="m-wrap span8" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_npwp'); ?></label>
								<div class="controls">
									<input placeholder="" type="text" id="vendor_npwp" name="vendor_npwp" value="<?php echo isset($res_data['vendor_npwp']) ? $res_data['vendor_npwp'] : ''; ?>" class="m-wrap span8" />
								</div>
							</div>
                            <div class="control-group npwp-file-wrapper">
                                <label class="control-label">File NPWP</label>
                                <div class="controls">
                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                        <div class="input-append ">
                                            <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file" data-title="Surat Pernyataan">
                                                <span class="fileupload-new">Pilih File</span>
                                                <span class="fileupload-exists">Ubah</span>
                                                <input type="file" class="default" name="npwp_file">
                                            </span>
                                            <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Hapus</a>
                                        </div>
                                    </div>
                                    <div id="file_link" style="line-height:10px;">
										<?php if(isset($res_data['etc_files']) && ! empty($res_data['etc_files']['npwp_file'])) : ?>
                                            <span><a target="_blank" href="<?php echo app_asset_url($res_data['etc_files']['npwp_file']['file_path']) .'?time='.time()?>"><?php echo $this->lang->line('global_view_file')?></a> | <a class="delete_etc_file" href="javascript:;" data-id="<?php echo $res_data['etc_files']['npwp_file']['id']?>" data-path="<?php echo$res_data['etc_files']['npwp_file']['file_path'] ?>"> <?php echo $this->lang->line('global_delete_file') ?></a></span>
										<?php endif; ?>
                                    </div>
                                </div>
                            </div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_bank_name'); ?></label>
								<div class="controls">
									<input placeholder="" type="text" id="vendor_bank_name" name="vendor_bank_name" value="<?php echo isset($res_data['vendor_bank_name']) ? $res_data['vendor_bank_name'] : ''; ?>" class="m-wrap span8" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_bank_rek'); ?></label>
								<div class="controls">
									<input placeholder="" type="text" id="vendor_bank_rek" name="vendor_bank_rek" value="<?php echo isset($res_data['vendor_bank_rek']) ? $res_data['vendor_bank_rek'] : ''; ?>" class="m-wrap span8" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('berkas_bank_account'); ?></label>
								<div class="controls">
									<input placeholder="" type="text" id="vendor_bank_account" name="vendor_bank_account" value="<?php echo isset($res_data['vendor_bank_account']) ? $res_data['vendor_bank_account'] : ''; ?>" class="m-wrap span8" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('global_address'); ?></label>
								<div class="controls">
									<textarea style="width:65%" rows="3" id="vendor_address" name="vendor_address" class="m-wrap"><?php echo isset($res_data['vendor_address']) ? $res_data['vendor_address'] : ''; ?></textarea>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('global_city'); ?></label>
								<div class="controls">
									<input type="text" name="vendor_city" id="vendor_city" value="<?php echo isset($res_data['vendor_city']) ? $res_data['vendor_city'] : ''; ?>" class="m-wrap span8" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label"><?php echo $this->lang->line('global_email'); ?></label>
								<div class="controls">
									<input type="text" name="vendor_email" id="vendor_email" value="<?php echo isset($res_data['vendor_email']) ? $res_data['vendor_email'] : ''; ?>" class="m-wrap span8" />
								</div>
							</div>
                            <h4 class="form-section">Lain - Lain</h4>
                            <div class="control-group">
                                <label class="control-label">Nik KTP</label>
                                <div class="controls">
                                    <input type="text" name="nik_ktp" id="nik_ktp" value="<?php echo isset($res_data['nik_ktp']) ? $res_data['nik_ktp'] : ''; ?>" class="m-wrap span8" />
                                </div>
                            </div>
                            <div class="control-group ktp-file-wrapper">
                                <label class="control-label">File KTP</label>
                                <div class="controls">
                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                        <div class="input-append ">
                                            <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file" data-title="File KTP">
                                                <span class="fileupload-new">Pilih File</span>
                                                <span class="fileupload-exists">Ubah</span>
                                                <input type="file" class="default" name="ktp_file">
                                            </span>
                                            <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Hapus</a>
                                        </div>
                                    </div>
                                    <div id="file_link" style="line-height:10px;">
										<?php if(isset($res_data['etc_files']) && ! empty($res_data['etc_files']['ktp_file'])) : ?>
                                            <span><a target="_blank" href="<?php echo app_asset_url($res_data['etc_files']['ktp_file']['file_path']) .'?time='.time()?>"><?php echo $this->lang->line('global_view_file')?></a> | <a class="delete_etc_file" href="javascript:;" data-id="<?php echo $res_data['etc_files']['ktp_file']['id']?>" data-path="<?php echo$res_data['etc_files']['ktp_file']['file_path'] ?>"> <?php echo $this->lang->line('global_delete_file') ?></a></span>
										<?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group siujk-file-wrapper">
                                <label class="control-label">File SIUJK</label>
                                <div class="controls">
                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                        <div class="input-append ">
                                            <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file" data-title="File SIUJK">
                                                <span class="fileupload-new">Pilih File</span>
                                                <span class="fileupload-exists">Ubah</span>
                                                <input type="file" class="default" name="siujk_file">
                                            </span>
                                            <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Hapus</a>
                                        </div>
                                    </div>
                                    <div id="file_link" style="line-height:10px;">
										<?php if(isset($res_data['etc_files']) && ! empty($res_data['etc_files']['siujk_file'])) : ?>
                                            <span><a target="_blank" href="<?php echo app_asset_url($res_data['etc_files']['siujk_file']['file_path']) .'?time='.time() ?>"><?php echo $this->lang->line('global_view_file')?></a> | <a class="delete_etc_file" href="javascript:;" data-id="<?php echo $res_data['etc_files']['siujk_file']['id']?>" data-path="<?php echo$res_data['etc_files']['siujk_file']['file_path'] ?>"> <?php echo $this->lang->line('global_delete_file') ?></a></span>
										<?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group skb-file-wrapper">
                                <label class="control-label">File SKB</label>
                                <div class="controls">
                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                        <div class="input-append ">
                                            <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file" data-title="File SKB">
                                                <span class="fileupload-new">Pilih File</span>
                                                <span class="fileupload-exists">Ubah</span>
                                                <input type="file" class="default" name="skb_file">
                                            </span>
                                            <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Hapus</a>
                                        </div>
                                    </div>
                                    <div id="file_link" style="line-height:10px;">
										<?php if(isset($res_data['etc_files']) && ! empty($res_data['etc_files']['skb_file'])) : ?>
                                            <span><a target="_blank" href="<?php echo app_asset_url($res_data['etc_files']['skb_file']['file_path']) .'?time='.time() ?>"><?php echo $this->lang->line('global_view_file')?></a> | <a class="delete_etc_file" href="javascript:;" data-id="<?php echo $res_data['etc_files']['skb_file']['id']?>" data-path="<?php echo$res_data['etc_files']['skb_file']['file_path'] ?>"> <?php echo $this->lang->line('global_delete_file') ?></a></span>
										<?php endif; ?>
                                    </div>
                                </div>
                            </div>
						</div>
						
						<div class="tab-pane" id="tab3">
                            <h4 class="form-section"><?php echo $this->lang->line('global_detail').' '.$this->lang->line('berkas_biaya_pajak'); ?></h4>
                            <div class="control-group" id="tagihan_type_parent">
                                <label class="control-label">Jenis Tagihan<span class="required"> *</span></label>
                                <div class="controls" id="tagihan_type_wrapper">
									<?php if(isset($res_data['tagihan_type']) && ! empty($res_data['tagihan_type'])): ?>
                                        <label class="radio">
                                            <input name="tagihan_type" data-title="FP" value="FP" type="radio" <?php echo isset($res_data['tagihan_type']) && $res_data['tagihan_type'] == "FP" ? "checked='checked'" : ''; ?>>
                                            FP
                                        </label>
                                        <label class="radio">
                                            <input name="tagihan_type" data-title="NON FP" value="NON FP" type="radio" <?php echo isset($res_data['tagihan_type']) && $res_data['tagihan_type'] == "NON FP" ? "checked='checked'" : ''; ?>>
                                            NON FP
                                        </label>
									<?php else: ?>
                                        <label class="radio">
                                            <input name="tagihan_type" data-title="FP" value="FP" type="radio" checked>
                                            FP
                                        </label>
                                        <label class="radio">
                                            <input name="tagihan_type" data-title="NON FP" value="NON FP" type="radio">
                                            NON FP
                                        </label>
									<?php endif; ?>
                                    <div id="form_tagihan_type_error"></div>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="span5" id="first_faktur">
                                    <div class="control-group fp-only">
                                        <label class="control-label">Upload Dokumen Faktur Pajak<span class="required"> *</span></label>
                                        <div class="controls">
                                            <div class="dropzone dropzone-previews" id="my-awesome-dropzone" style="width: 200px"></div>
                                        </div>
                                    </div>
                                </div>
								<?php if(isset($res_data['uploaded_fp_filepath_2'])):?>
									<?php
									$pajak_real_status = '';
									if($res_data['faktur_pajak_status_2'] == "Faktur Diganti"){
										$pajak_real_status = 'Diganti';
									}else if($res_data['faktur_pajak_status_2'] = "Faktur Dibatalkan"){
										$pajak_real_status = 'Dibatalkan';
									}
									?>
                                    <div class="span5 secondary-fp">
                                        <div class="control-group fp-only">
                                            <label class="control-label">Upload Dokumen Faktur Pajak <?php echo $pajak_real_status?><span class="required"> *</span></label>
                                            <div class="controls">
                                                <div class="dropzone dropzone-previews" id="my-awesome-dropzone2" style="width: 200px; height: 215px"></div>
                                            </div>
                                        </div>
                                    </div>
								<?php endif;?>
                            </div>

                            <h4 class="form-section" id="main_faktur">Faktur Pajak Normal/Normal-Pengganti</h4>
                            <div class="custom-form fp-only">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label" for="faktur_pajak_url">Tautan Faktur Pajak<span class="required"> *</span></label>
                                            <div class="controls">
                                                <div class="input-append span12">
                                                    <input class="m-wrap" id="faktur_pajak_url" name="faktur_pajak_url" type="text" style="width: 88.2%;" value="<?php echo isset($res_data['faktur_pajak_url']) ? $res_data['faktur_pajak_url'] : ''; ?>" readonly>
                                                    <button type="button" class="btn yellow" id="detail_transaksi">Detail Transaksi</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span4 ">
                                        <div class="control-group">
                                            <label class="control-label" for="faktur_pajak_status">Status Faktur<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="faktur_pajak_status" name="faktur_pajak_status" class="m-wrap span12" value="<?php echo isset($res_data['faktur_pajak_status']) ? $res_data['faktur_pajak_status'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <div class="control-group">
                                            <label class="control-label" for="faktur_pajak_approval">Status Approval<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="faktur_pajak_approval" name="faktur_pajak_approval" class="m-wrap span12" value="<?php echo isset($res_data['faktur_pajak_approval']) ? $res_data['faktur_pajak_approval'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <div class="control-group">
                                            <label class="control-label" for="pajak_type">Jenis Pajak<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" name="pajak_type" id="pajak_type" class="m-wrap span12" value="<?php echo isset($res_data['pajak_type']) ? $res_data['pajak_type'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span4 ">
                                        <div class="control-group">
                                            <label class="control-label" for="faktur_pajak_number">No. Faktur Pajak<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="faktur_pajak_number" name="faktur_pajak_number" class="m-wrap span12" value="<?php echo isset($res_data['faktur_pajak_number']) ? $res_data['faktur_pajak_number'] : ''; ?>" readonly>
                                                <input type="hidden" id="uploaded_fp_filepath" name="uploaded_fp_filepath" value="<?php echo isset($res_data['uploaded_fp_filepath']) ? $res_data['uploaded_fp_filepath'] : ''; ?>" class="m-wrap notempty large" readonly/>
                                                <input type="hidden" id="uploaded_fp_filename" name="uploaded_fp_filename" value="<?php echo isset($res_data['uploaded_fp_filename']) ? $res_data['uploaded_fp_filename'] : ''; ?>" class="m-wrap large" readonly/>
                                                <input type="hidden" id="uploaded_fp_filesize" name="uploaded_fp_filesize" value="<?php echo isset($res_data['uploaded_fp_filesize']) ? $res_data['uploaded_fp_filesize'] : ''; ?>" class="m-wrap large" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <div class="control-group">
                                            <label class="control-label" for="faktur_pajak_date">Tanggal Faktur Pajak<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="faktur_pajak_date" name="faktur_pajak_date" class="m-wrap span12" value="<?php echo isset($res_data['faktur_pajak_date']) ? $res_data['faktur_pajak_date'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <div class="control-group">
                                            <label class="control-label" for="pajak_period">Periode Pajak<span class="required"> *</span></label>
                                            <div class="controls">
                                                <div class="input-append span12">
                                                    <input type="text" name="pajak_period" id="pajak_period" value="<?php echo isset($res_data['pajak_period']) ? $res_data['pajak_period'] : ''; ?>" class="m-wrap span2" style="border-right: 1px solid #ccc !important;" readonly />
                                                    <span style="margin-left: 10px">
                                                        <input type="text" name="pajak_year" id="pajak_year" value="<?php echo isset($res_data['pajak_year']) ? $res_data['pajak_year'] : ''; ?>" class="m-wrap span4" style="border-right: 1px solid #ccc !important;" readonly />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span4 ">
                                        <div class="control-group">
                                            <label class="control-label" for="dpp_amount">Jumlah DPP<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="dpp_amount" name="dpp_amount" class="m-wrap span12" value="<?php echo isset($res_data['dpp_amount']) ? $res_data['dpp_amount'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <div class="control-group">
                                            <label class="control-label" for="fp_amount">Jumlah PPN<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="fp_amount" name="fp_amount" class="m-wrap span12" value="<?php echo isset($res_data['fp_amount']) ? $res_data['fp_amount'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <div class="control-group">
                                            <label class="control-label" for="ppnbm_amount">Jumlah PPnBM<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" name="ppnbm_amount" id="ppnbm_amount" class="m-wrap span12" value="<?php echo isset($res_data['ppnbm_amount']) ? $res_data['ppnbm_amount'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <h4 class="form-section" id="main_faktur">Data Penjual & Pembeli</h4>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label" for="nama_penjual">Nama Penjual<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="nama_penjual" name="nama_penjual" class="m-wrap span12" value="<?php echo isset($res_data['nama_penjual']) ? $res_data['nama_penjual'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label" for="nama_pembeli">Nama Pembeli<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="nama_pembeli" name="nama_pembeli" class="m-wrap span12" value="<?php echo isset($res_data['nama_pembeli']) ? $res_data['nama_pembeli'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label" for="npwp_penjual">NPWP Penjual<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="npwp_penjual" name="npwp_penjual" class="m-wrap span12" value="<?php echo isset($res_data['npwp_penjual']) ? $res_data['npwp_penjual'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label" for="npwp_pembeli">NPWP Pembeli<span class="required"> *</span></label>
                                            <div class="controls">
                                                <input type="text" id="npwp_pembeli" name="npwp_pembeli" class="m-wrap span12" value="<?php echo isset($res_data['npwp_pembeli']) ? $res_data['npwp_pembeli'] : ''; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label" for="alamat_penjual">Alamat Penjual<span class="required"> *</span></label>
                                            <div class="controls">
                                                <textarea name="alamat_penjual" id="alamat_penjual" rows="3" class="m-wrap span12" readonly><?php echo isset($res_data['alamat_penjual']) ? $res_data['alamat_penjual'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label" for="alamat_pembeli">Alamat Pembeli<span class="required"> *</span></label>
                                            <div class="controls">
                                                <textarea name="alamat_pembeli" id="alamat_pembeli" rows="3" class="m-wrap span12" readonly><?php echo isset($res_data['alamat_pembeli']) ? $res_data['alamat_pembeli'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br><br>

							<?php if(isset($res_data['uploaded_fp_filepath_2'])):?>
                                <h4 class="form-section secondary-fp">Faktur Pajak <?php echo $pajak_real_status?></h4>
                                <div class="custom-form fp-only secondary-fp">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="control-group">
                                                <label class="control-label" for="faktur_pajak_url_2">Tautan Faktur Pajak<span class="required"> *</span></label>
                                                <div class="controls">
                                                    <div class="input-append span12">
                                                        <input class="m-wrap" id="faktur_pajak_url_2" name="faktur_pajak_url_2" type="text" style="width: 88.2%;" value="<?php echo isset($res_data['faktur_pajak_url_2']) ? $res_data['faktur_pajak_url_2'] : ''; ?>" readonly>
                                                        <button type="button" class="btn yellow" id="detail_transaksi">Detail Transaksi</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span4 ">
                                            <div class="control-group">
                                                <label class="control-label" for="faktur_pajak_status_2">Status Faktur<span class="required"> *</span></label>
                                                <div class="controls">
                                                    <input type="text" id="faktur_pajak_status_2" name="faktur_pajak_status_2" class="m-wrap span12" value="<?php echo isset($res_data['faktur_pajak_status_2']) ? $res_data['faktur_pajak_status_2'] : ''; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            <div class="control-group">
                                                <label class="control-label" for="faktur_pajak_approval_2">Status Approval<span class="required"> *</span></label>
                                                <div class="controls">
                                                    <input type="text" id="faktur_pajak_approval_2" name="faktur_pajak_approval_2" class="m-wrap span12" value="<?php echo isset($res_data['faktur_pajak_approval_2']) ? $res_data['faktur_pajak_approval_2'] : ''; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            <div class="control-group">
                                                <label class="control-label" for="pajak_type_2">Jenis Pajak<span class="required"> *</span></label>
                                                <div class="controls">
                                                    <input type="text" name="pajak_type_2" id="pajak_type_2" class="m-wrap span12" value="<?php echo isset($res_data['pajak_type_2']) ? $res_data['pajak_type_2'] : ''; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span4 ">
                                            <div class="control-group">
                                                <label class="control-label" for="faktur_pajak_number_2">No. Faktur Pajak<span class="required"> *</span></label>
                                                <div class="controls">
                                                    <input type="text" id="faktur_pajak_number_2" name="faktur_pajak_number_2" class="m-wrap span12" value="<?php echo isset($res_data['faktur_pajak_number_2']) ? $res_data['faktur_pajak_number_2'] : ''; ?>" readonly>
                                                    <input type="hidden" id="uploaded_fp_filepath_2" name="uploaded_fp_filepath_2" value="<?php echo isset($res_data['uploaded_fp_filepath_2']) ? $res_data['uploaded_fp_filepath_2'] : ''; ?>" class="m-wrap notempty large" readonly/>
                                                    <input type="hidden" id="uploaded_fp_filename_2" name="uploaded_fp_filename_2" value="<?php echo isset($res_data['uploaded_fp_filename_2']) ? $res_data['uploaded_fp_filename_2'] : ''; ?>" class="m-wrap large" readonly/>
                                                    <input type="hidden" id="uploaded_fp_filesize_2" name="uploaded_fp_filesize_2" value="<?php echo isset($res_data['uploaded_fp_filesize_2']) ? $res_data['uploaded_fp_filesize_2'] : ''; ?>" class="m-wrap large" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            <div class="control-group">
                                                <label class="control-label" for="faktur_pajak_date_2">Tanggal Faktur Pajak<span class="required"> *</span></label>
                                                <div class="controls">
                                                    <input type="text" id="faktur_pajak_date_2" name="faktur_pajak_date_2" class="m-wrap span12" value="<?php echo isset($res_data['faktur_pajak_date_2']) ? $res_data['faktur_pajak_date_2'] : ''; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span4 ">
                                            <div class="control-group">
                                                <label class="control-label" for="dpp_amount_2">Jumlah DPP<span class="required"> *</span></label>
                                                <div class="controls">
                                                    <input type="text" id="dpp_amount_2" name="dpp_amount_2" class="m-wrap span12" value="<?php echo isset($res_data['dpp_amount_2']) ? $res_data['dpp_amount_2'] : ''; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            <div class="control-group">
                                                <label class="control-label" for="fp_amount_2">Jumlah PPN<span class="required"> *</span></label>
                                                <div class="controls">
                                                    <input type="text" id="fp_amount_2" name="fp_amount_2" class="m-wrap span12" value="<?php echo isset($res_data['fp_amount_2']) ? $res_data['fp_amount_2'] : ''; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            <div class="control-group">
                                                <label class="control-label" for="ppnbm_amount_2">Jumlah PPnBM<span class="required"> *</span></label>
                                                <div class="controls">
                                                    <input type="text" name="ppnbm_amount_2" id="ppnbm_amount_2" class="m-wrap span12" value="<?php echo isset($res_data['ppnbm_amount_2']) ? $res_data['ppnbm_amount_2'] : ''; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php endif;?>
                            <br>
                            <br>
                            <div class="control-group">
                                <label class="control-label"><?php echo $this->lang->line('berkas_barang_jasa_name'); ?></label>
                                <div class="controls">
                                    <input type="text" id="barang_jasa_name" name="barang_jasa_name" value="<?php echo isset($res_data['barang_jasa_name']) ? $res_data['barang_jasa_name'] : ''; ?>" class="m-wrap span8" />
                                </div>
                            </div>
							<h4 class="form-section"><?php echo $this->lang->line('global_detail').' '.$this->lang->line('berkas_biaya_pajak'); ?></h4>

							<table id="table_cost" class="table table-striped table-bordered table-hover table-condensed" style="font-size:12px;">
								<thead>
									<tr>
										<th style="width:25%;text-align:center;"><?php echo $this->lang->line('berkas_real_cost'); ?></th>
										<th style="width:25%;text-align:center;" colspan="3"><?php echo $this->lang->line('berkas_ppn'); ?></th>
										<th style="width:25%;text-align:center;" colspan="3"><?php echo $this->lang->line('berkas_pph'); ?></th>
										<th style="width:25%;text-align:center;"><?php echo $this->lang->line('berkas_netto_cost'); ?></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:20%;text-align:center;" >
											<input type="text" id="real_cost" name="real_cost" value="<?php echo isset($res_data['real_cost']) ? $res_data['real_cost'] : ''; ?>" class="span12" />
										</td>
                                        <td style="width:10%;text-align:center;" class="fp-only">
                                            <select name="ppn_code" id="ppn_code" class="span12">
                                                <option value="" data-percentage="0"><?php echo $this->lang->line('global_choose_one'); ?></option>
												<?php foreach($ref_ppn_percentage as $dt): ?>
                                                    <option <?php echo isset($res_data) && $res_data['ppn_code'] == $dt['code'] ? 'selected="selected"': ''; ?>  value="<?php echo $dt['code']; ?>" data-percentage="<?php echo $dt['percentage']; ?>"><?php echo $dt['code']; ?> - <?php echo $dt['description']; ?></option>
												<?php endforeach; ?>
                                            </select>
                                        </td>
                                        <td id="ppn_percentage_text" style="width:5%;text-align:center;">
                                            <input type="text" id="ppn_percentage" name="ppn_percentage" value="&nbsp;&nbsp;<?php echo isset($res_data['ppn']) ? $res_data['ppn'] . ' %' : ''; ?>" class="span12" disabled="disabled"/>
                                        </td>
                                        <td id="ppn_text" style="width:15%;text-align:center;" class="fp-only">
                                            <input type="text" id="ppn_val" name="ppn_val" value="<?php echo isset($res_data['ppn_val']) ? $res_data['ppn_val'] : ''; ?>" class="span12" />
                                        </td>

<!--										<td style="width:10%;text-align:center;">-->
<!--											<input type="text" id="pph" name="pph" value="--><?php //echo isset($res_data['pph']) ? $res_data['pph'] : '2.0'; ?><!--" class="span8" /> %-->
<!--										</td>-->
<!--										<td id="pph_text" style="width:15%;text-align:center;">-->
<!--											<input type="text" readonly="readonly" id="pph_val" name="pph_val" value="--><?php //echo isset($res_data['pph_val']) ? $res_data['pph_val'] : ''; ?><!--" class="span12" />	-->
<!--										</td>-->
                                        <td style="width:10%;text-align:center;">
                                            <select name="pph_code" id="pph_code" class="span12">
                                                <option value="" data-percentage=""><?php echo $this->lang->line('global_choose_one'); ?></option>
												<?php if(isset($ref_pph_percentage)):?>
													<?php foreach($ref_pph_percentage as $dt): ?>
                                                        <option <?php echo isset($res_data) && $res_data['pph_code'] == $dt['code'] ? 'selected="selected"': ''; ?>  value="<?php echo $dt['code']; ?>" data-percentage="<?php echo $dt['percentage']; ?>"><?php echo $dt['code']; ?> - <?php echo $dt['description']; ?></option>
													<?php endforeach; ?>
												<?php endif;?>
                                            </select>
                                        </td>
                                        <td id="pph_percentage_text" style="width:5%;text-align:center;">
                                            <input type="text" id="pph_percentage" name="pph_percentage" value="&nbsp;&nbsp;&nbsp;<?php echo isset($res_data['pph']) ? $res_data['pph'] . ' %' : ''; ?>" class="span12" disabled="disabled"/>
                                        </td>
                                        <td id="pph_text" style="width:15%;text-align:center;">
                                            <input type="text" id="pph_val" name="pph_val" value="<?php echo isset($res_data['pph_val']) ? $res_data['pph_val'] : ''; ?>" class="span12" />
                                        </td>
										<td id="netto_cost_text" style="width:25%;text-align:center;">
											<input type="text" readonly="readonly" id="netto_cost_val" name="netto_cost_val" value="<?php echo isset($res_data['netto_cost_val']) ? $res_data['netto_cost_val'] : ''; ?>" class="span12" />	
										</td>
									</tr>
								</tbody>
							</table>
							<br>
                            <div class="control-group surat-pernyataan-wrapper">
                                <label class="control-label">Surat Pernyataan</label>
                                <div class="controls">
                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                        <div class="input-append ">
                                            <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file" data-title="Surat Pernyataan">
                                                <span class="fileupload-new">Pilih File</span>
                                                <span class="fileupload-exists">Ubah</span>
                                                <input type="file" class="default" name="surat_pernyataan">
                                            </span>
                                            <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Hapus</a>
                                        </div>
                                    </div>
                                    <div id="file_link" style="line-height:10px;">
                                        <?php if(isset($res_data['etc_files']) && ! empty($res_data['etc_files']['surat_pernyataan'])) : ?>
                                            <span><a target="_blank" href="<?php echo app_asset_url($res_data['etc_files']['surat_pernyataan']['file_path']) .'?time='.time() ?>"><?php echo $this->lang->line('global_view_file')?></a> | <a class="delete_etc_file" href="javascript:;" data-id="<?php echo $res_data['etc_files']['surat_pernyataan']['id']?>" data-path="<?php echo$res_data['etc_files']['surat_pernyataan']['file_path'] ?>"> <?php echo $this->lang->line('global_delete_file') ?></a></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
<!--							<div class="control-group">-->
<!--								<label class="control-label">--><?php //echo $this->lang->line('berkas_faktur_pajak_number_date'); ?><!--</label>-->
<!--								<div class="controls">-->
<!--									<input placeholder="--><?php //echo $this->lang->line('berkas_faktur_pajak_number'); ?><!--" type="text" id="faktur_pajak_number" name="faktur_pajak_number" value="--><?php //echo isset($res_data['faktur_pajak_number']) ? $res_data['faktur_pajak_number'] : ''; ?><!--" class="m-wrap large" />-->
<!--									&nbsp;-->
<!--									<input type="text" id="faktur_pajak_date" name="faktur_pajak_date" value="--><?php //echo isset($res_data['faktur_pajak_date']) ? $res_data['faktur_pajak_date'] : ''; ?><!--" class="m-wrap m-ctrl-medium date-picker" size="16" placeholder="--><?php //echo $this->lang->line('global_format_date'); ?><!--: --><?php //echo $this->site_config['app_def_date_format']; ?><!--" />-->
<!--								</div>-->
<!--							</div>-->
<!--							-->
<!--							<div class="control-group">-->
<!--								<label class="control-label">--><?php //echo $this->lang->line('berkas_pajak_period'); ?><!--</label>-->
<!--								<div class="controls">-->
<!--									<select name="pajak_period" id="pajak_period" class="m-wrap small">-->
<!--										<option value="">--><?php //echo $this->lang->line('global_none'); ?><!--</option>-->
<!--										--><?php //foreach($ref_periods as $dt): ?>
<!--										<option --><?php //echo isset($res_data) && $res_data['pajak_period'] == $dt ? 'selected="selected"': ''; ?><!-- value="--><?php //echo $dt; ?><!--">--><?php //echo $dt; ?><!--</option>-->
<!--										--><?php //endforeach; ?>
<!--									</select>-->
<!--									--->
<!--									<select name="pajak_year" id="pajak_year" class="m-wrap small">-->
<!--										<option value="">--><?php //echo $this->lang->line('global_none'); ?><!--</option>-->
<!--										--><?php //foreach($ref_years as $dt): ?>
<!--										<option --><?php //echo isset($res_data) && $res_data['pajak_year'] == $dt ? 'selected="selected"': ''; ?><!--  value="--><?php //echo $dt; ?><!--">--><?php //echo $dt; ?><!--</option>-->
<!--										--><?php //endforeach; ?>
<!--									</select>-->
<!--								</div>-->
<!--							</div>-->
<!--							-->

						</div>
						
						<div class="tab-pane" id="tab4">
							<div id="upload_field">
								<h4 class="form-section" id="upload_title"><?php echo $this->lang->line('berkas_upload'); ?> <span class="upload_format">(<?php echo $this->lang->line('global_format_file'); ?>: PDF )</span></h4> 
								<div id="upload_div"></div>
							</div>
						</div>
						
						<div class="tab-pane" id="tab5">
							<div class="row-fluid">
								<div class="span12">
									<div class="span6">
										<div class="portlet">
											<div class="portlet-title">
												<div class="caption"><?php echo $this->lang->line('global_detail').' '.$this->lang->line('berkas_name'); ?></div>
												<div class="tools">
													<a class="collapse" href="javascript:;"></a>
												</div>
											</div>
											<div class="portlet-body">
										
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_number'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="berkas_number"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_ttap_date'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="ttap_date"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_project_code'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="project_code"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_payment_method'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="payment_method"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_invoice_number_date'); ?> :</label>
													<div class="controls">
														<span class="text display-value inline" data-display="invoice_number"></span> ( <span class="text display-value inline" data-display="invoice_date"></span> )
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_bap_bpg_dub_number_date'); ?> :</label>
													<div class="controls">
														<span class="text display-value inline" data-display="bap_bpg_dub_number_date"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_surat_jalan_lkp_number_date'); ?> :</label>
													<div class="controls">
														<span class="text display-value inline" data-display="surat_jalan_lkp_number"></span> ( <span class="text display-value inline" data-display="surat_jalan_lkp_date"></span> )
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_spk_po_number_date'); ?> :</label>
													<div class="controls">
														<span class="text display-value inline" data-display="spk_po_number"></span> ( <span class="text display-value inline" data-display="spk_po_date"></span> )
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="span6">
										<div class="portlet">
											<div class="portlet-title">
												<div class="caption"><?php echo $this->lang->line('global_detail').' '.$this->lang->line('berkas_vendor'); ?></div>
												<div class="tools">
													<a class="collapse" href="javascript:;"></a>
												</div>
											</div>
											<div class="portlet-body">
										
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_vendor_type'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_type"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_vendor_name'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_name"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_vendor_code'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_code"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_npwp'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_npwp"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_bank_name'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_bank_name"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_bank_rek'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_bank_rek"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_bank_account'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_bank_account"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('global_address'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_address"></span>
													</div>
												</div>
									
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('global_city'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_city"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('global_email'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="vendor_email"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row-fluid">
								<div class="span12">
									<div class="span6">
										<div class="portlet">
											<div class="portlet-title">
												<div class="caption"><?php echo $this->lang->line('global_detail').' '.$this->lang->line('berkas_biaya_pajak'); ?></div>
												<div class="tools">
													<a class="collapse" href="javascript:;"></a>
												</div>
											</div>
											<div class="portlet-body">
										
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_real_cost'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="real_cost"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_ppn'); ?> :</label>
													<div class="controls">
														<span class="text display-value inline" data-display="ppn_val"></span> (<span class="text display-value inline" data-display="ppn"></span> %) 
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_pph'); ?> :</label>
													<div class="controls">
														<span class="text display-value inline" data-display="pph_val"></span> ( <span class="text display-value inline" data-display="pph"></span> % ) 
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_netto_cost'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="netto_cost_val"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_faktur_pajak_number_date'); ?> :</label>
													<div class="controls">
														<span class="text display-value inline" data-display="faktur_pajak_number"></span> ( <span class="text display-value inline" data-display="faktur_pajak_date"></span> )
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_pajak_period'); ?> :</label>
													<div class="controls">
														<span class="text display-value inline" data-display="pajak_period"></span> - <span class="text display-value inline" data-display="pajak_year"></span>
													</div>
												</div>
												
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_barang_jasa_name'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="barang_jasa_name"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="span6">
										<div class="portlet">
											<div class="portlet-title">
												<div class="caption"><?php echo $this->lang->line('berkas_upload'); ?></div>
												<div class="tools">
													<a class="collapse" href="javascript:;"></a>
												</div>
											</div>
											<div class="portlet-body">
										
												<div class="control-group">
													<label class="control-label"><?php echo $this->lang->line('berkas_files'); ?> :</label>
													<div class="controls">
														<span class="text display-value" data-display="berkas_files"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="space20"></div>
					
					<div class="form-actions clearfix center">
						<div style="float:left;margin-left:10px;"><button id="btn_back" type="button" class="btn"><?php echo $this->lang->line('global_back'); ?></button></div>
						<input type="hidden" id="id" name="id" value="<?php echo isset($id) ? $id : ''; ?>" />
						<input type="hidden" id="is_rilis" name="is_rilis" value="<?php echo isset($res_data['is_rilis']) ? $res_data['is_rilis'] : 0; ?>" />
						<input type="hidden" id="is_revision" name="is_revision" value="<?php echo isset($is_revision) ? $is_revision : 0; ?>" />
						<input type="hidden" id="revision" name="revision" value="<?php echo isset($res_data['revision']) ? $res_data['revision'] : 0; ?>" />
						
						
						<a href="javascript:;" class="btn button-previous">
						<i class="m-icon-swapleft"></i> <?php echo $this->lang->line('global_previous'); ?> 
						</a>
						<a href="javascript:;" class="btn blue button-next">
						<?php echo $this->lang->line('global_next2'); ?> <i class="m-icon-swapright m-icon-white"></i>
						</a>
						
						
						<button type="submit" class="btn button-submit blue" value="0"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
						<?php if(!$is_revision): ?>
						<button type="submit" class="btn button-submit green" value="1" style="<?php echo isset($res_data['is_rilis']) && $res_data['is_rilis'] == 1 ? 'display:none;' : ''; ?>"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save_release'); ?></button>
						<?php else: ?>
						<button type="submit" class="btn button-submit green" value="1" ><i class="icon-ok"></i> <?php echo $this->lang->line('global_save_release'); ?></button>
						<?php endif; ?>
					</div>
				</div>
			</form>
			<!-- END FORM-->                
		</div>
	</div>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal fullwidth hide fade in" id="vendor_modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h3><?php echo $this->lang->line('global_list'); ?> <?php echo $this->lang->line('berkas_vendor'); ?></h3>
	</div>
	<div class="modal-body">
		<div class="scroller" style="height:400px" data-always-visible="1" data-rail-visible1="1">
		<table class="table table-bordered table-hover" id="table1">
			<thead>
				<tr>
					<th style="width:2%;text-align:center;"><input type="checkbox" class="checkall" data-set="#table1 .checkboxes" /></th>
					<th style="width:2%;" class="hidden-480"><?php echo $this->lang->line('global_no'); ?></th>
					<th style="width:30%;" ><?php echo $this->lang->line('global_name'); ?></th>
					<th style="width:10%;" class="hidden-480"><?php echo $this->lang->line('berkas_npwp'); ?></th>
					<th style="width:30%;" class="hidden-480"><?php echo $this->lang->line('berkas_bank_name'); ?></th>
					<th style="width:10%;" class="hidden-480"><?php echo $this->lang->line('berkas_bank_rek'); ?></th>
					<th style="width:10%;" class="hidden-480"><?php echo $this->lang->line('berkas_vendor_type'); ?></th>
				</tr>
			</thead>
			<tbody>
				<td colspan="5" class="dataTables_empty"><?php echo $this->lang->line('global_loading_data'); ?></td>
			</tbody>
			<tfoot>
				<tr>
					<td width="2%"><input type="hidden" /></td>
					<td width="2%"><input type="hidden" /></td>
					<td width="30%"></td>
					<td width="10%"></td>
					<td width="30%"></td>
					<td width="10%"></td>
					<td width="10%"></td>
				</tr>
			</tfoot>
		</table>
		</div>
	</div>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade in" id="progress-modal">
    <div class="modal-body">
        <div class="row-fluid">
            <div class="span12"
            <p style="text-align: center">Sedang Mengambil Data, Harap Tunggu Sebentar</p>
            <div class="progress progress-striped active">
                <div style="width: 100%;" class="bar"></div>
            </div>
        </div>
    </div>
</div>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal fullwidth hide fade in" id="qrcode_modal">
    <form id="qrcode-form" class="form-horizontal" method="post" autocomplete="off">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
            <h3>Scan QRCode Faktur Pajak</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span12">
                    <p>Letakan kursor pada inputan dibawah ini dan mulai scan</p>
                    <input type="text" name="barcode_faktur_url" id="barcode_faktur_url" class="span12" autofocus />
                </div>
            </div>
        </div>
    </form>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal fullwidth hide fade in" id="qrcode_modal_2">
    <form id="qrcode-form-2" class="form-horizontal" method="post" autocomplete="off">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
            <h3>Scan QRCode Faktur Pajak</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span12">
                    <p>Letakan kursor pada inputan dibawah ini dan mulai scan</p>
                    <input type="text" name="barcode_faktur_url_2" id="barcode_faktur_url_2" class="span12" autofocus />
                </div>
            </div>
        </div>
    </form>
</div>
<script>
	jQuery(document).ready(function(){
		// init grid
		var aSelected = [];
		var asInitVals = [];
		
		var oTable = jQuery('#table1');
		
		oTable.dataTable({
            "sPaginationType": "bootstrap",
			"aaSorting": [[1,"desc"]],
			"aoColumnDefs": [
				{"bSortable": false, "aTargets": [0,1], "bSearchable": false, "aTargets": [0,1,-1]}
		    ],
			"bFilter" : true,
			"bServerSide": true,
			"sAjaxSource": "<?php echo app_backend_url('references/ref_vendor/get_list_new'); ?>",
			"sServerMethod": "POST",
			"bStateSave": false,
			"fnStateSave": function(oSettings, oData) { 
				ceki.fnSaveDtView(oSettings, oData); 
			},
			"fnStateLoad": function(oSettings) { 
				dataFN = JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname));
				if (dataFN != null) {
					jQuery('#search_name').val(dataFN.aoSearchCols[2].sSearch);
					jQuery('#search_npwp').val(dataFN.aoSearchCols[3].sSearch);
					jQuery('#search_bank_name').val(dataFN.aoSearchCols[4].sSearch);
					jQuery('#search_bank_rek').val(dataFN.aoSearchCols[5].sSearch);
					jQuery('#search_vendor_type').val(dataFN.aoSearchCols[6].sSearch);
				}
				return ceki.fnLoadDtView(oSettings); 
			},
			"oLanguage": {
				"sLengthMenu": "<?php echo $this->lang->line('global_DT_sLengthMenu'); ?>",
				"sZeroRecords": "<?php echo $this->lang->line('global_DT_sZeroRecords'); ?>",
				"sInfo": "<?php echo $this->lang->line('global_DT_sInfo'); ?>",
				"sInfoEmpty": "<?php echo $this->lang->line('global_DT_sInfoEmpty'); ?>",
				"sInfoFiltered": "(<?php echo $this->lang->line('global_DT_sInfoFiltered'); ?>)",
				"sSearch": "<?php echo $this->lang->line('global_search'); ?> :",
				"oPaginate": {
					"sPrevious": "<?php echo $this->lang->line('global_prev'); ?>",
					"sNext": "<?php echo $this->lang->line('global_next'); ?>"
				}
			},
			"fnServerParams": function (aoData) {
				aoData.push({"name": "is_ajax", "value": true});
                aoData.push({"name": "code", "value": jQuery("#vendor_type option:selected").attr('data-code')})
			},
            "fnDrawCallback": function(oSettings) {
				if(!jQuery(this).children('.blockUI').length){
					App.blockUI(jQuery(this), true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				}
				
				App.initUniform();
				aSelected = [];
				jQuery(this).find('input[type=checkbox]').click(function(){
					var id = this.value;
					var checked = jQuery(this).is(':checked');
					fnSelected(this, id, checked);
					fnTbarDisableEnable();
				});
				
				App.unblockUI(jQuery(this));
				fnTbarDisableEnable();
				jQuery('.checkall').parent().removeClass('checked');
            },
			"aoColumns": [
				{ "mData": "cbox", "sClass": "hidden-480" },
				{ "mData": "rnum", "sClass": "hidden-480" },
				{ "mData": "name1" },
				{ "mData": "stcd1", "sClass": "hidden-480" },
				{ "mData": "banka", "sClass": "hidden-480" },
				{ "mData": "bankn", "sClass": "hidden-480" },
				{ "mData": "vendor_type", "sClass": "hidden-480" }
			],
			"aLengthMenu": [
				[5, 15, 20, 50, 100, -1],
				[5, 15, 20, 50, 100, "All"]
			],
            "iDisplayLength": 5,
			"sDom": "<'dataTables_toolbar'><'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"fnPreDrawCallback": function () { 
				if (jQuery('.dataTables_length select').val() == -1) {
					if(oTable.fnSettings().fnRecordsTotal() > 500) {
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_limit_record_page'); ?>',
							sticky: false,
							time: 3000
						});
						return false;
					}
				}				
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {}
        });
		
		jQuery('#table1_filter input').unbind();
	    jQuery('#table1_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				oTable.fnFilter(this.value);   
			}
		}); 
		
		jQuery("#table1 tbody").click(function(e) {
			fnRemoveTableSelected(oTable);
			jQuery(e.target.parentNode).addClass('row_selected');
			var id = jQuery(e.target.parentNode).closest('tr')[0].id;
			fnSelected(jQuery(e.target.parentNode), id, true);
			fnTbarDisableEnable();
		});
		
		jQuery('#table1 tbody').bind('dblclick', function(e) {
			var id = jQuery(e.target.parentNode).closest('tr')[0].id;
			fnRowSelected(id);
		}); 
		
		
		function fnRemoveTableSelected(oT) {
			aSelected = [];
			jQuery(oT.fnSettings().aoData).each(function (){
				jQuery(this.nTr).removeClass('row_selected');
			});
		}
		
		jQuery("div.dataTables_toolbar").html(' '+
		'<button id="button-refresh" class="btn purple"><i class="icon-refresh"></i> <?php echo $this->lang->line('global_refresh'); ?></button>&nbsp;' +
		'<button id="button-select" class="btn" disabled="disabled"><i class="icon-magic"></i> <?php echo $this->lang->line('global_select'); ?></button>&nbsp;' +
		'<div style="float:right;"><button id="button-clear" class="btn black" style="visibility:hidden;"><i class="icon-share"></i></a></div>');
		
	
		jQuery('#button-refresh').click(function(){
			App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
			oTable.fnDraw();
		});
		
		jQuery('#button-clear').click(function(){
			ceki.fnResetDtView();
			ceki.fnRefresh();
		});
		
		jQuery('#button-select').click(function(){
			id = aSelected[0];
			fnRowSelected(id);
		});
		
		function fnRowSelected(id) {
			var rowIndex = oTable.fnGetPosition(jQuery('#'+id).closest('tr')[0]);
			var data = oTable.fnGetData(rowIndex);
			console.log(data);
			if(typeof data != 'undefined') {
				jQuery('#ref_vendor_id').val(data.id);
				jQuery('#vendor_name').val(data.name1);
				jQuery('#vendor_npwp').val(data.stcd1);
				jQuery('#vendor_bank_name').val(data.banka);
				jQuery('#vendor_bank_rek').val(data.bankn);
				jQuery('#vendor_bank_account').val(data.koinh);
				
				jQuery('#vendor_code').val(data.partner);
				jQuery('#vendor_address').val(data.street);
				jQuery('#vendor_city').val(data.city1);
				jQuery('#vendor_email').val(data.smtp_addr);
				
				jQuery('#vendor_name').attr("readonly", "readonly");
				
				jQuery('#btn_vendor_clear').show();
				
				jQuery('#vendor_modal').modal('hide');
				fnRemoveTableSelected(oTable);
				fnTbarDisableEnable();
			}
		}
		
		function fnSelected(cmp, id, checked, all){
			var index = jQuery.inArray(id, aSelected);
			if(id == 'on') return;		 
			if ( index === -1 ) {
				if(all){
					if(checked){
						aSelected.push(id);
					}
				}else{
					aSelected.push(id);
				}
			} else {
				if(all){
					if(!checked){
						aSelected.splice(index, 1);
					}
				}else{
					aSelected.splice(index, 1);
				}
			}
			
			if(checked) {	
				jQuery(cmp).attr('checked',true);
				jQuery(cmp).parent().addClass('checked');
				jQuery(cmp).parents('tr').addClass('selected');
			}else{
				jQuery(cmp).attr('checked',false);
				jQuery(cmp).parent().removeClass('checked');
				jQuery(cmp).parents('tr').removeClass('selected');
			}
		}
		
		function fnTbarDisableEnable(){
			var rowSelected = aSelected.length;
			
			if(rowSelected > 0){
				if(rowSelected > 1){
					jQuery('#button-select').attr("disabled", "disabled");
				}else{
					jQuery('#button-select').removeAttr("disabled");
				}
			}else{
				jQuery('#button-select').attr("disabled", "disabled");
			}
		}
		// end init grid
		
		// init modal
		jQuery('#btn_vendor').click(function() {
			// oTable.fnFilter(jQuery("#vendor_type option:selected").attr('data-code'), 6);
            oTable.fnDraw();
			jQuery('#vendor_modal').modal('show');
		});
		
		jQuery('#btn_vendor_clear').click(function() {
			jQuery('#vendor_name').removeAttr("readonly");
			
			jQuery('#ref_vendor_id').val("");
			jQuery('#vendor_name').val("");
			jQuery('#vendor_npwp').val("");
			jQuery('#vendor_bank_name').val("");
			jQuery('#vendor_bank_rek').val("");
			jQuery('#vendor_bank_account').val("");
			
			jQuery('#vendor_code').val("");
			jQuery('#vendor_address').val("");
			jQuery('#vendor_city').val("");
			jQuery('#vendor_email').val("");
			
			jQuery('#btn_vendor_clear').hide();
		});
		
		if(jQuery('#ref_vendor_id').val() != '') {
			jQuery('#vendor_name').attr("readonly", "readonly");
			
			jQuery('#btn_vendor_clear').show();
		}
		// end init modal
		
		// init form
		
		var form = $('#form_save');
		var error = $('.alert-error', form);
		var success = $('.alert-success', form);
		
		var displayConfirm = function() {
			$('.display-value', form).each(function(){
				var input = $('[name="'+$(this).attr("data-display")+'"]', form);
				if (input.is(":text") || input.is("textarea")) {
					if ($(this).attr("data-display") == 'real_cost') {
						var val = input.val().replace(/_./g,"");
					} else if ($(this).attr("data-display") == 'ppn' || $(this).attr("data-display") == 'pph') {
						var val = input.val().replace(/,_/g,"");
					}else {
						var val = input.val() != '' ? input.val() : '-';
					}
					$(this).html(val);
				} else if (input.is("select")) {
					var val = input.find('option:selected').text() != '' ? input.find('option:selected').text() : '-';
					$(this).html(val);
				} else if (input.is(":radio") && input.is(":checked")) {
					var val = input.attr("data-title") != '' ? input.attr("data-title") : '-';
					$(this).html(val);
				} else if ($(this).attr("data-display") == 'bap_bpg_dub_number_date') {
					var bap_bpg_dub_number_date = [];
					var bap_bpg_dub_dates = $('[name="bap_bpg_dub_date[]"]');
					var i = 0;
					$('[name="bap_bpg_dub_number[]"]').each(function(){
						var bap_bpg_dub_date = bap_bpg_dub_dates[i].value;
						
						bap_bpg_dub_number_date.push($(this).val()+' ( '+bap_bpg_dub_date+' )');
						
						i++;
					});
					$(this).html(bap_bpg_dub_number_date.join(", "));
				} else if ($(this).attr("data-display") == 'berkas_files') {
					var berkas_files = [];
					$('[name="berkas_files[]"]').each(function(){
						
						if($(this).val() != '') {
							berkas_files.push($(this).closest('.control-group').find('label').html());
						} else {
							link = $(this).closest('.controls').find('#file_link').find('a')[0];
							if(typeof link != 'undefined') {
								berkas_files.push($(this).closest('.control-group').find('label').html()+' - <a href="'+link+'" target="_blank"><?php echo $this->lang->line('global_view_file'); ?></a>');
							}
						}
						
						
					});
					$(this).html(berkas_files.join("<br>"));
				}
				
			});
		}




		if(jQuery('#id').val() != '') {
			jQuery('#berkas_number').attr("readonly", "readonly");

			if(jQuery('#revision').val() > 0) {
				jQuery('#vendor_name').attr("readonly", "readonly");
				jQuery('#vendor_tools').hide();
			}
		}
		
		// jQuery("#real_cost").keypress(function(event) {
		//   if ( event.which == 45 || event.which == 189 ) {
		// 	  event.preventDefault();
		//    }
		// });
		//
		// jQuery("#real_cost").live('keyup',function () {
		// 	pressValue();
		// });
		//

        jQuery('#real_cost').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
            oncomplete : function () {
                pressValue();
            }
        })

		jQuery("#ppn").keypress(function(event) {
		  if ( event.which == 45 || event.which == 189 ) {
			  event.preventDefault();
		   }
		});
		
		jQuery("#ppn").live('keyup',function () {
			pressValue();
		});
		
		jQuery("#pph").keypress(function(event) {
		  if ( event.which == 45 || event.which == 189 ) {
			  event.preventDefault();
		   }
		});
		
		jQuery("#pph").live('keyup',function () {
			pressValue();
		});

        jQuery("#pph_code").on('change',function () {
            $('#pph_percentage').val($(this).find(':selected').attr('data-percentage')  + ' %');
            pressValue();
        });

		function pressValueOld() {
			var realCost = jQuery('#real_cost').val();
			realCost = realCost.replace(/[._]/g,'');
			realCost = parseInt(realCost);

			var ppn = jQuery('#ppn').val();
			ppn = ppn.replace(/[._]/g,'');
			ppn = parseFloat(ppn);
			if(!isNaN(ppn)) {
				ppn = ppn/100;
			} else {
				ppn = '';
			}

			// var pph = jQuery('#pph').val();
            var pph = jQuery('#pph_code').find(':selected').attr('data-percentage');
			pph = pph.replace(/[_]/g,'');
			pph = pph.replace(/[,]/g,'.');
			pph = parseFloat(pph);
			if(!isNaN(pph)) {
				pph = pph/100;
			} else {
				pph = '';
			}

			if(!isNaN(realCost)) {
				calculateCost(realCost,ppn,pph);
			} else {
				jQuery("#ppn_val").val('');
				jQuery("#pph_val").val('');
				jQuery("#netto_cost_val").val('');
			}
		}

        function pressValue() {
            var realCost = jQuery('#real_cost').val();
            realCost = realCost.replace(/[._]/g,'');
            realCost = parseInt(realCost);

            var ppn = jQuery('#ppn_code').find(':selected').attr('data-percentage');
            ppn = ppn.replace(/[._]/g,'');
            ppn = parseFloat(ppn);
            if(!isNaN(ppn)) {
                ppn = ppn/100;
            } else {
                ppn = '';
            }

            var pph = jQuery('#pph_code').find(':selected').attr('data-percentage');
            pph = pph.replace(/[_]/g,'');
            pph = pph.replace(/[,]/g,'.');
            pph = parseFloat(pph);
            if(!isNaN(pph)) {
                pph = pph/100;
            } else {
                pph = '';
            }

            if(!isNaN(realCost)) {
                calculateCost(realCost,ppn,pph);
            } else {
                jQuery("#ppn_val").val('');
                jQuery("#pph_val").val('');
                jQuery("#netto_cost_val").val('');
            }
        }
		
		function calculateCost(realCost,ppn,pph) {
			var ppn_val = 0;
			if(ppn != '') {
				ppn_val = parseFloat(realCost * ppn);
			}
			
			var pph_val = 0;
			if(pph != '') {
				pph_val = parseFloat(realCost * pph);
			}
			
			var netto_val = realCost - pph_val;
			
			jQuery("#ppn_val").val(ceki.fnFormatNumber(Math.round(ppn_val)));
			jQuery("#pph_val").val(ceki.fnFormatNumber(Math.round(pph_val)));
			jQuery("#netto_cost_val").val(ceki.fnFormatNumber(Math.round(netto_val)));
		}
		
		jQuery("#add_bap_bpg_dub").live('click',function () {
			var str = '<div class="controls">' +
				'<input type="text" name="bap_bpg_dub_number[]" class="m-wrap large" /> &nbsp;' +
				'&nbsp;' +
				'<input type="text" name="bap_bpg_dub_date[]" class="m-wrap m-ctrl-medium date-picker" size="16" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" />' +
				'&nbsp;<i class="icon-minus remove_bap_bpg_dub"></i>' +
			'<br><br></div>';
			
			jQuery('.bap_bpg_dub').append(str);
			
			jQuery('.date-picker').datepicker({
				format: 'dd-mm-yyyy'
			});
		});
		
		jQuery(".remove_bap_bpg_dub").live('click',function () {
			jQuery(this).closest('.controls').fadeOut(function(){
				jQuery(this).remove();
			});
		});
		
		jQuery('.date-picker').datepicker({
			format: 'dd-mm-yyyy'
		});
		
		jQuery.validator.addMethod(
			"indonesianDate",
			function(value, element) {
				if(value != ''){
					return value.match(/^\d\d?\-\d\d?\-\d\d\d\d$/);
				} else {
					return true;
				}
			},
			"<?php echo $this->lang->line('global_error_date'); ?>"
		);

        jQuery.validator.addMethod('filesize', function (value, element, arg) {
            console.log(element.files[0]);
            if(typeof element.files[0] != 'undefined'){
                var fileSize = element.files[0].size;
                return fileSize <= arg
            }
            return true
        }, 'File size must be less than {0}');

        jQuery.validator.addMethod(
            "allowAbove500",
            function(value, element, arg) {
                if(value != ''){
                    var theAmount = $(element).inputmask('unmaskedvalue');
                    var crntFile = $('input[name="surat_pernyataan"]').val();
                    var uploaded = $('.surat-pernyataan-wrapper').find('#file_link').find('span');
                    console.log(uploaded)
                    if(crntFile || uploaded.length){
                        return true;
                    }
                    return theAmount <= 500000000;
                } else {
                    return true;
                }
            },
            "Maksimal 500 Juta, lebih dari itu harus menyertakan surat pernyataan dibawah ini"
        );
        
        jQuery.validator.addMethod(
            "equalFaktur",
            function(value, element) {
                if(value != ''){
                    var fakturOne = $('#faktur_pajak_number').inputmask("unmaskedvalue").substr(3);
                    var fakturTwo = $('#faktur_pajak_number_2').inputmask("unmaskedvalue").substr(3);
                    console.log(fakturOne);
                    console.log(fakturTwo);
                    return fakturOne == fakturTwo;
                } else {
                    return true;
                }
            },
            "Nomor Faktur Pajak Tidak Sama Dengan Faktur Lainnya"
        );
		
		// jQuery("#vendor_npwp").inputmask({"mask": "99.999.999.9.999.999"});
		// jQuery("#faktur_pajak_number").inputmask({"mask": "999.999.99.99999999"});

        // $("#real_cost").on('input propertychange', function(e){
        //     console.log("yooo");
        //     e.preventDefault();
        // });


		// jQuery("#real_cost").inputmask('999.999.999.999.999', {
		// 	numericInput: true,
		// 	rightAlignNumerics: false,
		// 	greedy: true,
		// 	clearMaskOnLostFocus: true,
		// });


		// jQuery("#pph").inputmask({"mask": "9,9"});
		jQuery("#ppn").inputmask({"mask": "99"}); 
		
		var rules = {};
		var messages = {};
		
		rules["berkas_number"] = { required: true, maxlength: 8};
		messages["berkas_number"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),7); ?>",
			remote: "<?php echo $this->lang->line('berkas_number_already_exist') ?>"
		};
		
		rules["ttap_date"] = { required: true, indonesianDate: true};
		messages["ttap_date"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
		};
		
		rules["project_code"] = { required: true};
		messages["project_code"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>"	
		};
		
		rules["vendor_type"] = { required: true};
		messages["vendor_type"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>"	
		};
		
		rules["vendor_name"] = { required: true, maxlength: 100 };
		messages["vendor_name"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),100); ?>"
		};

        rules["npwp_file"] = { required: false, filesize: 2000000, extension : 'pdf' };
        messages["npwp_file"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
            filesize: "<?php echo $this->lang->line('global_error_file_size')." 2MB"; ?>",
            extension: "<?php echo $this->lang->line('global_format_file'); ?><?php echo $this->lang->line('global_must_be'); ?> pdf",
        };

        rules["ktp_file"] = { required: false, filesize: 2000000, extension : 'pdf' };
        messages["ktp_file"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
            filesize: "<?php echo $this->lang->line('global_error_file_size')." 2MB"; ?>",
            extension: "<?php echo $this->lang->line('global_format_file'); ?><?php echo $this->lang->line('global_must_be'); ?> pdf",
        };

        rules["siujk_file"] = { required: false, filesize: 2000000, extension : 'pdf' };
        messages["siujk_file"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
            filesize: "<?php echo $this->lang->line('global_error_file_size')." 2MB"; ?>",
            extension: "<?php echo $this->lang->line('global_format_file'); ?><?php echo $this->lang->line('global_must_be'); ?> pdf",
        };

        rules["skb_file"] = { required: false, filesize: 2000000, extension : 'pdf' };
        messages["skb_file"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
            filesize: "<?php echo $this->lang->line('global_error_file_size')." 2MB"; ?>",
            extension: "<?php echo $this->lang->line('global_format_file'); ?><?php echo $this->lang->line('global_must_be'); ?> pdf",
        };

        rules["vendor_npwp"] = { required: false, maxlength: 45 };
		messages["vendor_npwp"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),45); ?>"
		};
		
		rules["vendor_email"] = { email: false, maxlength: 45, email: true };
		messages["vendor_email"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),45); ?>",
			email: "<?php echo $this->lang->line('global_email_invalid'); ?>"
		};
		
		//rules["faktur_pajak_number"] = { required: false, maxlength: 45 };
		//messages["faktur_pajak_number"] = { 
		//	required: "<?php //echo $this->lang->line('global_error_required_field2') ?>//",
		//	maxlength: "<?php //echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),45); ?>//"
		//};
		//
		//rules["faktur_pajak_date"] = { required: false, indonesianDate: true };
		//messages["faktur_pajak_date"] = { 
		//	required: "<?php //echo $this->lang->line('global_error_required_field2') ?>//",
		//	indonesianDate: "<?php //echo $this->lang->line('global_error_date'); ?>//"
		//};
		
		rules["invoice_number"] = { required: false, maxlength: 45 };
		messages["invoice_number"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),45); ?>"
		};
		
		rules["invoice_date"] = { required: true, indonesianDate: true};
		messages["invoice_date"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
		};
		
		rules["payment_method"] = { required: false, maxlength: 45 };
		messages["payment_method"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),45); ?>"
		};
		
		rules["real_cost"] = { required: true, allowAbove500 :true };
		messages["real_cost"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>"
		};
		
		rules["ppn"] = { required: false,  number: false };
		messages["ppn"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>", 
			range: "<?php echo $this->lang->line('berkas_ppn_0_30_error'); ?>", 
			number: "<?php echo $this->lang->line('global_error_number'); ?>" 
		};
		
		rules["ppn_val"] = { required: true,  number: false };
		messages["ppn_val"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>", 
			range: "<?php echo $this->lang->line('berkas_ppn_0_30_error'); ?>", 
			number: "<?php echo $this->lang->line('global_error_number'); ?>" 
		};
		
		rules["pph"] = { required: false, number: false };
		messages["pph"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>", 
			range: "<?php echo $this->lang->line('berkas_pph_0_10_error'); ?>", 
			number: "<?php echo $this->lang->line('global_error_number'); ?>" 
		};
		
		rules["pph_val"] = { required: true, number: false };
		messages["pph_val"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>", 
			range: "<?php echo $this->lang->line('berkas_pph_0_10_error'); ?>", 
			number: "<?php echo $this->lang->line('global_error_number'); ?>" 
		};

        rules["pph_code"] = { required: true};
        messages["pph_code"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
		
		//rules["berkas_files[]"] = { required: false, extension: "pdf", filesize: 2000000};
		//messages["berkas_files[]"] = {
		//	required: "<?php //echo $this->lang->line('global_error_required_upload'); ?>//",
		//	extension: "<?php //echo $this->lang->line('global_format_file'); ?>// <?php //echo $this->lang->line('global_must_be'); ?>// pdf",
		//	filesize: "<?php //echo $this->lang->line('global_error_file_size')." 2MB"; ?>//"
		//};
		
		rules["bap_bpg_dub_number[]"] = { required: false, maxlength: 45 };
		messages["bap_bpg_dub_number[]"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),45); ?>"
		};
		
		rules["bap_bpg_dub_date[]"] = { required: false, indonesianDate: true };
		messages["bap_bpg_dub_date[]"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
		};
		
		rules["surat_jalan_lkp_number"] = { required: false, maxlength: 45 };
		messages["surat_jalan_lkp_number"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),45); ?>"
		};
		
		rules["surat_jalan_lkp_date"] = { required: false, indonesianDate: true };
		messages["surat_jalan_lkp_date"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
		};
		
		rules["spk_po_number"] = { required: false, maxlength: 45 };
		messages["spk_po_number"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),45); ?>"
		};
		
		rules["spk_po_date"] = { required: false, indonesianDate: true };
		messages["spk_po_date"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
			indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
		};
		

		
		jQuery('#btn_back').click(function(e) {
			ceki.fnLocation('<?php echo app_backend_url('main/berkas'); ?>');
		});
		
		jQuery('.delete_file').live('click',function(e) {
			var _this = this; 
			var id = jQuery(this).attr('data-id');
			var path = jQuery(this).attr('data-path');
			jConfirm('<?php echo $this->lang->line('global_confirm_delete'); ?>', '<?php echo $this->lang->line('global_confirm_delete_header'); ?>', function(r) {
				if(r){
					var cAjax = new ceki.fnAjax({
						url : '<?php echo app_backend_url('main/berkas/delete_file'); ?>',
						data : ({
							id: id,
							path: path
						}),
						successCallBack : function() {
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_success_delete'); ?>',
								sticky: false,
								time: 3000
							});
							
							jQuery(_this).parents('span').fadeOut(function(){
								jQuery(this).remove();
							});
						}
					});
				}
			});
		});
		
		function getUploadFile() {
			var cAjax = new ceki.fnAjax({
				url : '<?php echo app_backend_url('main/berkas/get_berkas_vendor_type'); ?>',
				dataType : 'html',
				data: {
					berkas_id: jQuery('#id').val(),
					vendor_type_name: jQuery("#vendor_type").val()
				},
				successCallBack : function(html) {
					console.log(html);
				},
				processHtml: function(html) {
					jQuery('#upload_div').html(html);
                    jQuery("input[name='berkas_files[]']").each(function () {
                        $(this).rules("add",{
                            required: false,
                            extension: "pdf",
                            filesize: 2000000,
                            messages: {
                                required: "<?php echo $this->lang->line('global_error_required_upload'); ?>",
                                extension: "<?php echo $this->lang->line('global_format_file'); ?> <?php echo $this->lang->line('global_must_be'); ?> pdf",
                                filesize: "<?php echo $this->lang->line('global_error_file_size')." 2MB"; ?>"
                            }
                        })
                    })
				},
				successErrorCallBack : function(obj) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error'); ?>',
						sticky: false,
						time: 3000
					});
				}
			});
		}
		
		getUploadFile();
		jQuery('#vendor_type').change(function(){
            if($(this).val()){
                jQuery.get('<?php echo app_backend_url('main/berkas/get_pph_code/'); ?>'+$('#vendor_type option:selected').attr('data-code'), function (data) {
                    $('#pph_code').empty();
                    $('#pph_percentage').val("");
                    $('#pph_code').append('<option value="" data-percentage="">Pilih Satu</option>');
                    data.forEach(function (t) {
                        $('#pph_code').append('<option value='+t.code+' data-percentage='+t.percentage+'>'+t.code+' - '+t.description+'</option>')
                    })
                })
            }
			getUploadFile();
		});
		
		function checkBerkasNumber() {
			var r = jQuery.ajax({
				url : '<?php echo app_backend_url('main/berkas/check_berkas_number'); ?>',
				dataType : 'json',
				type: 'POST',
				async: false,
				data: {
					id: jQuery('#id').val(),
					project_code: jQuery("#project_code").val(),
					berkas_number: jQuery("#berkas_number").val(),
					direct_ajax: true
				},
				success: function (obj) {
					if(obj.success) {
						return true;
					} else {
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: obj.message,
							sticky: false,
							time: 3000
						});
						return false;
					}
				}
			});
			
			return r;
		}

        jQuery('.delete_etc_file').live('click',function(e) {
            var _this = this;
            var id = jQuery(this).attr('data-id');
            var path = jQuery(this).attr('data-path');
            jConfirm('<?php echo $this->lang->line('global_confirm_delete'); ?>', '<?php echo $this->lang->line('global_confirm_delete_header'); ?>', function(r) {
                if(r){
                    var cAjax = new ceki.fnAjax({
                        url : '<?php echo app_backend_url('main/berkas/delete_etc_file'); ?>',
                        data : ({
                            id: id,
                            path: path
                        }),
                        successCallBack : function() {
                            jQuery.gritter.add({
                                title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                text: '<?php echo $this->lang->line('global_success_delete'); ?>',
                                sticky: false,
                                time: 3000
                            });
                            $(_this).parent().parent().prev().find('input[type=file]').attr('data-file', '');
                            jQuery(_this).parents('span').fadeOut(function(){
                                jQuery(this).remove();
                            });
                        }
                    });
                }
            });
        });

		//JS Page 3
        var myDropzone;
        Dropzone.options.myAwesomeDropzone = false;
        Dropzone.autoDiscover = false;

        var myMainDropzone = new Dropzone("div#my-awesome-dropzone",{
            addRemoveLinks: true,
            url: '<?php echo app_backend_url('main/berkas/upload_faktur_pajak'); ?>',
            paramName: "faktur_pajak", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            acceptedFiles : 'application/pdf,.pdf',
            createImageThumbnails: false,
            maxFiles : 1,
            dictDefaultMessage: "Drop file/klik disini untuk upload",
            dictRemoveFileConfirmation: "Seluruh data & dokumen Faktur Pajak akan direset, Anda yakin ingin menghapus dokumen Faktur Pajak ini?",
            dictRemoveFile: "Hapus File",
            timeout: 120000,
            init: function() {
                //preload file if any
				<?php if(isset($res_data['uploaded_fp_filepath'])):?>
                var mockFile = {
                    name: '<?php echo $res_data['uploaded_fp_filename'] ?>',
                    size: '<?php echo $res_data['uploaded_fp_filesize'] ?>'
                };

                this.options.addedfile.call(this, mockFile);
                this.emit("complete", mockFile);
                this.options.maxFiles = this.options.maxFiles - 1;
                this.files.push(mockFile);

                $(this.files[0].previewElement).on("click", function () {
                    //download
                    var urls =  $('#uploaded_fp_filepath').val();
                    var crtUrl = "<?php echo app_asset_url('faktur_pajak/')?>"+urls;
                    window.open(crtUrl, "_blank");
                });

				<?php endif;?>

                this.on("success", function(file, res) {
                    $('#progress-modal').modal('hide');
                    $(file.previewElement).on('click', function () {
                        var crtUrl = "<?php echo app_asset_url('tmp/')?>"+res.uploaded.filepath;
                        window.open(crtUrl, "_blank");
                    })

                    if(typeof res.data != 'undefined'){
                        var statusFaktur = res.data.statusFaktur;
                        switch (statusFaktur){
                            case 'Faktur Pajak Normal':
                                var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                $('#faktur_pajak_url').val(res.url);
                                $('#faktur_pajak_number').val(fpNumber);
                                $('#faktur_pajak_approval').val(res.data.statusApproval);
                                $('#faktur_pajak_status').val(res.data.statusFaktur);
                                $('#faktur_pajak_date').val(fpDate);
                                $('#fp_amount').val(res.data.jumlahPpn);
                                $('#dpp_amount').val(res.data.jumlahDpp);
                                $('#ppnbm_amount').val(res.data.jumlahPpnBm);

                                $('#vendor_npwp').val(res.data.npwpPenjual);
                                $('#vendor_name').val(res.data.namaPenjual);
                                $('#vendor_address').val(res.data.alamatPenjual);

                                $('#npwp_penjual').val(res.data.npwpPenjual);
                                $('#nama_penjual').val(res.data.namaPenjual);
                                $('#alamat_penjual').val(res.data.alamatPenjual);

                                $('#npwp_pembeli').val(res.data.npwpLawanTransaksi);
                                $('#nama_pembeli').val(res.data.namaLawanTransaksi);
                                $('#alamat_pembeli').val(res.data.alamatLawanTransaksi);

                                $('#uploaded_fp_filepath').val(res.uploaded.filepath);
                                $('#uploaded_fp_filename').val(res.uploaded.filename);
                                $('#uploaded_fp_filesize').val(res.uploaded.filesize);

                                setPajakType();
                                setPajakPeriodVal();
                                break;
                            case 'Faktur Pajak Normal-Pengganti':
                                var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                $('#faktur_pajak_url').val(res.url);
                                $('#faktur_pajak_number').val(fpNumber);
                                $('#faktur_pajak_status').val(res.data.statusFaktur);
                                $('#faktur_pajak_approval').val(res.data.statusApproval);
                                $('#faktur_pajak_date').val(fpDate);
                                $('#fp_amount').val(res.data.jumlahPpn);
                                $('#dpp_amount').val(res.data.jumlahDpp);
                                $('#ppnbm_amount').val(res.data.jumlahPpnBm);

                                $('#pajak_period').val("");
                                $('#pajak_year').val("");

                                $('#vendor_npwp').val(res.data.npwpPenjual);
                                $('#vendor_name').val(res.data.namaPenjual);
                                $('#vendor_address').val(res.data.alamatPenjual);

                                $('#npwp_pembeli').val(res.data.npwpLawanTransaksi);
                                $('#nama_pembeli').val(res.data.namaLawanTransaksi);
                                $('#alamat_pembeli').val(res.data.alamatLawanTransaksi);

                                $('#npwp_penjual').val(res.data.npwpPenjual);
                                $('#nama_penjual').val(res.data.namaPenjual);
                                $('#alamat_penjual').val(res.data.alamatPenjual);

                                $('#uploaded_fp_filepath').val(res.uploaded.filepath);
                                $('#uploaded_fp_filename').val(res.uploaded.filename);
                                $('#uploaded_fp_filesize').val(res.uploaded.filesize);

                                setPajakType();
//                                setPajakPeriodVal();

                                var str = '<div class="span6"><div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" style="text-align:left">Upload Faktur Diganti<span class="required"> *</span></label>'+
                                    '<div class="controls" style="padding-left:0px">' +
                                    '<div class="dropzone dropzone-previews" id="my-awesome-dropzone2" style="width: 200px; height: 215px"></div>'+
                                    '</div>'+
                                    '</div></div>';

                                $(str).insertAfter("#first_faktur");

                                var nextFp = '<br class="secondary-fp"><br class="secondary-fp"><h4 class="form-section secondary-fp">Faktur Pajak Diganti</h4>'+
                                    '<div class="custom-form fp-only secondary-fp">';

                                nextFp += '<div class="row-fluid">' +
                                    '<div class="span12">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label">URL Faktur Pajak Diganti<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<div class="input-append span12">'+
                                    '<input type="text" id="faktur_pajak_url_2" name="faktur_pajak_url_2" value="" class="m-wrap" readonly style="width: 88.2%">' +
                                    '<button type="button" class="btn yellow" id="detail_transaksi_2">Detail Transaksi</button>' +
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                nextFp +=   '<div class="row-fluid">' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_status_2">Status Faktur<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_status_2" name="faktur_pajak_status_2" class="m-wrap span12" readonly>'+
                                    '                                                    </div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_approval_2">Status Approval<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_approval_2" name="faktur_pajak_approval_2" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="pajak_type_2">Jenis Pajak<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="pajak_type_2" name="pajak_type_2" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                                nextFp += '<div class="row-fluid">' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_number_2">No. Faktur Pajak<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_number_2" name="faktur_pajak_number_2" class="m-wrap span12" readonly>'+
                                    '<input type="hidden" id="uploaded_fp_filepath_2" name="uploaded_fp_filepath_2" class="m-wrap notempty large" readonly/>'+
                                    '<input type="hidden" id="uploaded_fp_filename_2" name="uploaded_fp_filename_2" class="m-wrap large" readonly/>' +
                                    '<input type="hidden" id="uploaded_fp_filesize_2" name="uploaded_fp_filesize_2" class="m-wrap large" readonly/>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_date_2">Tanggal Faktur Pajak<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_date_2" name="faktur_pajak_date_2" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                nextFp +=   '<div class="row-fluid">' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="dpp_amount_2">Jumlah DPP<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="dpp_amount_2" name="dpp_amount_2" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="fp_amount_2">Jumlah PPN<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="fp_amount_2" name="fp_amount_2" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="ppnbm_amount_2">Jumlah PPnBM<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="ppnbm_amount_2" name="ppnbm_amount_2" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                nextFp +='</div>';

                                $(nextFp).insertAfter(".custom-form");

                                $("#form_save").validate();
                                $('input[name="faktur_pajak_url_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="faktur_pajak_status_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });


                                $('input[name="faktur_pajak_approval_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="pajak_type_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="faktur_pajak_number_2"]').rules("add",{
                                    required : true,
                                    equalFaktur: true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                        equalWithFaktur : "Nomor Faktur Pajak Tidak Sama"
                                    }
                                });

                                $('input[name="faktur_pajak_date_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="dpp_amount_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="fp_amount_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="ppnbm_amount_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                jQuery("#faktur_pajak_number_2").inputmask({
                                    "mask": "999.999-99.99999999",
                                });

                                jQuery('#fp_amount_2').inputmask('decimal',{
                                    groupSeparator: ".",
                                    radixPoint: ",",
                                    digits: 0,
                                    autoGroup: true,
                                });

                                jQuery('#dpp_amount_2').inputmask('decimal',{
                                    groupSeparator: ".",
                                    radixPoint: ",",
                                    digits: 0,
                                    autoGroup: true,
                                });

                                jQuery('#ppnbm_amount_2').inputmask('decimal',{
                                    groupSeparator: ".",
                                    radixPoint: ",",
                                    digits: 0,
                                    autoGroup: true,
                                });

                                myDropzone = new Dropzone("div#my-awesome-dropzone2",
                                    {
                                        addRemoveLinks: true,
                                        url: '<?php echo app_backend_url('main/berkas/upload_faktur_pajak'); ?>',
                                        paramName: "faktur_pajak", // The name that will be used to transfer the file
                                        maxFilesize: 5, // MB
                                        acceptedFiles : 'application/pdf,.pdf',
                                        createImageThumbnails: false,
                                        maxFiles : 1,
                                        dictDefaultMessage: "Drop file/klik disini untuk upload",
                                        dictRemoveFileConfirmation: "Anda yakin ingin menghapus dokumen Faktur Pajak ini?",
                                        dictRemoveFile: "Hapus File",
                                        timeout : 120000,
                                    }
                                );

                                myDropzone.on('success', function (file, res) {
                                    $('#progress-modal').modal('hide');
                                    $(file.previewElement).on('click', function () {
                                        var crtUrl = "<?php echo app_asset_url('tmp/')?>"+res.uploaded.filepath;
                                        window.open(crtUrl, "_blank");
                                    });

                                    $('#uploaded_fp_filepath_2').val(res.uploaded.filepath);
                                    $('#uploaded_fp_filename_2').val(res.uploaded.filename);
                                    $('#uploaded_fp_filesize_2').val(res.uploaded.filesize);

                                    if(typeof res.data != 'undefined'){

                                        //todo NSFP harus sama
                                        var first_nsfp = $('#faktur_pajak_number').inputmask("unmaskedvalue");
                                        var firstFp = first_nsfp.substr(3);
                                        var second_nsfp = res.data.nomorFaktur;


                                        if(firstFp == second_nsfp){
                                            var statusFaktur = res.data.statusFaktur;
                                            switch (statusFaktur) {
                                                case 'Faktur Diganti':
                                                    var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                                    var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                                    $('#faktur_pajak_url_2').val(res.url);
                                                    $('#faktur_pajak_number_2').val(fpNumber);
                                                    $('#faktur_pajak_status_2').val(res.data.statusFaktur);
                                                    $('#faktur_pajak_approval_2').val(res.data.statusApproval);
                                                    $('#faktur_pajak_date_2').val(fpDate);

                                                    $('#fp_amount_2').val(res.data.jumlahPpn);
                                                    $('#dpp_amount_2').val(res.data.jumlahDpp);
                                                    $('#ppnbm_amount_2').val(res.data.jumlahPpnBm);

                                                    setPajakType2();
                                                    setPajakPeriodValGanti();
                                                    break;
                                                default :
                                                    alert("Status FP yang diupload adalah '"+res.data.statusFaktur+"', harap upload Faktur Diganti. Data ini akan dihapus, silahkan upload ulang.");
                                                    this.removeAllFiles();
                                            }
                                        }else{
                                            alert("NSFP berbeda dengan faktur pajak sebelumnya. Dokumen akan dihapus, harap upload ulang.");
                                            this.removeAllFiles();
                                        }

                                    }

                                });

                                myDropzone.on("removedfile", function(file) {

                                    if(this.files.length == 0){
                                        this.options.maxFiles = 1;

                                        if(typeof $('#uploaded_fp_filepath_2').val() != 'undefined'){

                                            var cAjax = new ceki.fnAjax({
                                                url : '<?php echo app_backend_url('main/berkas/delete_faktur_pajak'); ?>',
                                                data : ({
                                                    path: $('#uploaded_fp_filepath_2').val()
                                                }),
                                                successCallBack : function(resp) {
                                                    jQuery.gritter.add({
                                                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                                        text: resp.message,
                                                        sticky: false,
                                                        time: 3000
                                                    });
                                                }
                                            });

                                            $("#pajak_period").val("");
                                            $("#pajak_year").val("");

                                            $('#uploaded_fp_filepath_2').val("");
                                            $('#uploaded_fp_filename_2').val("");
                                            $('#uploaded_fp_filesize_2').val("");


                                            // if(typeof $('#uploaded_fp_filepath').val() != 'undefined' && $('#uploaded_fp_filepath').val() != '') {
                                            //     myMainDropzone.removeAllFiles();
                                            // }
                                        }
                                    }
                                });

                                myDropzone.on("maxfilesexceeded", function(file){
                                    alert("Upload hanya boleh 1 file");
                                    this.removeFile(file);
                                });

                                myDropzone.on("sending", function handleTimeout(file, xhr, formData) {
                                    xhr.ontimeout = function (e) {
                                        alert('Upload Dibatalkan karena proses terlalu lama');
                                        this.removeFile(file);
                                        $('#progress-modal').modal('hide');
                                    };
                                });

                                myDropzone.on("uploadprogress", function(file, progress, bytesSent){
                                    console.log(progress);
                                    if(progress > 99){
                                        $('#progress-modal').modal('show');
                                    }
                                });

                                myDropzone.on("error", function(file, res) {
                                    $('#progress-modal').modal('hide');
                                    if(typeof res.uploaded!= 'undefined'){
                                        $('#uploaded_fp_filepath_2').val(res.uploaded.filepath);
                                        $('#uploaded_fp_filename_2').val(res.uploaded.filename);
                                        $('#uploaded_fp_filesize_2').val(res.uploaded.filesize);
                                    }
                                });

                                break;
                            case 'Faktur Diganti':
                                $('#faktur_pajak_url').val("");
                                $('#faktur_pajak_number').val("");
                                $('#faktur_pajak_approval').val("");
                                $('#faktur_pajak_status').val("");
                                $('#faktur_pajak_date').val("");
                                $('#fp_amount').val("");
                                $('#dpp_amount').val("");
                                $('#ppnbm_amount').val("");
                                $('#pajak_type').val("");

                                // $('#vendor_npwp').val("");
                                $('#nama_penjual').val("");
                                $('#alamat_penjual').val("");

                                $('#npwp_pembeli').val("");
                                $('#nama_pembeli').val("");
                                $('#alamat_pembeli').val("");

                                var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                var str = '<div class="span6"><div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" style="text-align:left">Upload Faktur Normal-Pengganti<span class="required"> *</span></label>'+
                                    '<div class="controls" style="padding-left:60px">' +
                                    '<div class="dropzone dropzone-previews" id="my-awesome-dropzone2" style="width: 200px; height: 215px"></div>'+
                                    '</div>'+
                                    '</div></div>';

                                $(str).insertAfter("#first_faktur");

                                var nextFp = '<br class="secondary-fp"><br class="secondary-fp"><h4 class="form-section secondary-fp">Faktur Pajak Diganti</h4>'+
                                    '<div class="custom-form fp-only secondary-fp">';

                                nextFp += '<div class="row-fluid">' +
                                    '<div class="span12">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label">URL Faktur Pajak Diganti<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<div class="input-append span12">'+
                                    '<input type="text" id="faktur_pajak_url_2" name="faktur_pajak_url_2" value="'+res.url+'" class="m-wrap" readonly style="width: 88.2%">' +
                                    '<button type="button" class="btn yellow" id="detail_transaksi_2">Detail Transaksi</button>' +
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                nextFp +=   '<div class="row-fluid">' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_status_2">Status Faktur<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_status_2" name="faktur_pajak_status_2" value="'+res.data.statusFaktur+'" class="m-wrap span12" readonly>'+
                                    '                                                    </div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_approval_2">Status Approval<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_approval_2" name="faktur_pajak_approval_2" value="'+res.data.statusApproval+'" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="pajak_type_2">Jenis Pajak<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="pajak_type_2" name="pajak_type_2" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                                nextFp += '<div class="row-fluid">' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_number_2">No. Faktur Pajak<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_number_2" name="faktur_pajak_number_2" class="m-wrap span12" value="'+fpNumber+'" readonly>'+
                                    '<input type="hidden" id="uploaded_fp_filepath_2" name="uploaded_fp_filepath_2" class="m-wrap notempty large" value="'+res.uploaded.filepath+'" readonly/>'+
                                    '<input type="hidden" id="uploaded_fp_filename_2" name="uploaded_fp_filename_2" class="m-wrap large" value="'+res.uploaded.filename+'" readonly/>' +
                                    '<input type="hidden" id="uploaded_fp_filesize_2" name="uploaded_fp_filesize_2" class="m-wrap large" value="'+res.uploaded.filesize+'" readonly/>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_date_2">Tanggal Faktur Pajak<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_date_2" name="faktur_pajak_date_2" class="m-wrap span12" value="'+fpDate+'"readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                nextFp +=   '<div class="row-fluid">' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="dpp_amount_2">Jumlah DPP<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="dpp_amount_2" name="dpp_amount_2" class="m-wrap span12" value="'+res.data.jumlahDpp+'" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="fp_amount_2">Jumlah PPN<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="fp_amount_2" name="fp_amount_2" class="m-wrap span12" value="'+res.data.jumlahPpn+'" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="ppnbm_amount_2">Jumlah PPnBM<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="ppnbm_amount_2" name="ppnbm_amount_2" class="m-wrap span12" value="'+res.data.jumlahPpnBm+'" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                nextFp +='</div>';

                                $(nextFp).insertAfter(".custom-form");
                                setPajakPeriodValGanti();
                                setPajakType2();

                                $("#form_save").validate();
                                $('input[name="faktur_pajak_url_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="faktur_pajak_status_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });


                                $('input[name="faktur_pajak_approval_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="pajak_type_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="faktur_pajak_number_2"]').rules("add",{
                                    required : true,
                                    equalFaktur: true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                        equalWithFaktur : "Nomor Faktur Pajak Tidak Sama"
                                    }
                                });

                                $('input[name="faktur_pajak_date_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="dpp_amount_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="fp_amount_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="ppnbm_amount_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                jQuery("#faktur_pajak_number_2").inputmask({
                                    "mask": "999.999-99.99999999",
                                });

                                jQuery('#fp_amount_2').inputmask('decimal',{
                                    groupSeparator: ".",
                                    radixPoint: ",",
                                    digits: 0,
                                    autoGroup: true,
                                });

                                jQuery('#dpp_amount_2').inputmask('decimal',{
                                    groupSeparator: ".",
                                    radixPoint: ",",
                                    digits: 0,
                                    autoGroup: true,
                                });

                                jQuery('#ppnbm_amount_2').inputmask('decimal',{
                                    groupSeparator: ".",
                                    radixPoint: ",",
                                    digits: 0,
                                    autoGroup: true,
                                });


                                myDropzone = new Dropzone("div#my-awesome-dropzone2",
                                    {
                                        addRemoveLinks: true,
                                        url: '<?php echo app_backend_url('main/berkas/upload_faktur_pajak'); ?>',
                                        paramName: "faktur_pajak", // The name that will be used to transfer the file
                                        maxFilesize: 5, // MB
                                        acceptedFiles : 'application/pdf,.pdf',
                                        createImageThumbnails: false,
                                        maxFiles : 1,
                                        dictDefaultMessage: "Drop file/klik disini untuk upload",
                                        dictRemoveFileConfirmation: "Anda yakin ingin menghapus dokumen Faktur Pajak ini?",
                                        dictRemoveFile: "Hapus File",
                                        timeout : 120000,
                                    }
                                );

                                myDropzone.on('success', function (file, res) {
                                    $('#progress-modal').modal('hide');
                                    $(file.previewElement).on('click', function () {
                                        var crtUrl = "<?php echo app_asset_url('tmp/')?>"+res.uploaded.filepath;
                                        window.open(crtUrl, "_blank");
                                    });

                                    $('#uploaded_fp_filepath').val(res.uploaded.filepath);
                                    $('#uploaded_fp_filename').val(res.uploaded.filename);
                                    $('#uploaded_fp_filesize').val(res.uploaded.filesize);

                                    //Todo check NSFP harus sama

                                    var second_nsfp = $('#faktur_pajak_number_2').inputmask("unmaskedvalue");
                                    var second_nsfp = second_nsfp.substr(3);
                                    var first_nsfp = res.data.nomorFaktur;

                                    if(first_nsfp == second_nsfp){
                                        var statusFaktur = res.data.statusFaktur;
                                        switch (statusFaktur) {
                                            case 'Faktur Pajak Normal-Pengganti':
                                                var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                                var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                                $('#faktur_pajak_url').val(res.url);
                                                $('#faktur_pajak_number').val(fpNumber);
                                                $('#faktur_pajak_status').val(res.data.statusFaktur);
                                                $('#faktur_pajak_approval').val(res.data.statusApproval);
                                                $('#faktur_pajak_date').val(fpDate);
                                                $('#fp_amount').val(res.data.jumlahPpn);
                                                $('#dpp_amount').val(res.data.jumlahDpp);
                                                $('#ppnbm_amount').val(res.data.jumlahPpnBm)

                                                $('#vendor_npwp').val(res.data.npwpPenjual);
                                                $('#vendor_name').val(res.data.namaPenjual);
                                                $('#vendor_address').val(res.data.alamatPenjual);

                                                $('#npwp_pembeli').val(res.data.npwpLawanTransaksi);
                                                $('#nama_pembeli').val(res.data.namaLawanTransaksi);
                                                $('#alamat_pembeli').val(res.data.alamatLawanTransaksi);

                                                $('#npwp_penjual').val(res.data.npwpPenjual);
                                                $('#nama_penjual').val(res.data.namaPenjual);
                                                $('#alamat_penjual').val(res.data.alamatPenjual);

                                                setPajakType();
                                                break;
                                            default:
                                                alert("Status FP yang diupload adalah '"+res.data.statusFaktur+"', harap upload Faktur Pajak Normal-Pengganti. Data ini akan dihapus, silahkan upload ulang.")
                                                this.removeAllFiles();
                                        }
                                    }else{
                                        alert("NSFP berbeda dengan faktur pajak sebelumnya. Dokumen akan dihapus, harap upload ulang.");
                                        this.removeAllFiles();
                                    }

                                });

                                myDropzone.on("removedfile", function(file) {

                                    if(this.files.length == 0){
                                        this.options.maxFiles = 1

                                        var cAjax = new ceki.fnAjax({
                                            url : '<?php echo app_backend_url('main/berkas/delete_faktur_pajak'); ?>',
                                            data : ({
                                                path: $('#uploaded_fp_filepath').val()
                                            }),
                                            successCallBack : function(resp) {
                                                jQuery.gritter.add({
                                                    title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                                    text: resp.message,
                                                    sticky: false,
                                                    time: 3000
                                                });
                                            }
                                        });

                                        //clear hidden input value uploaded_fp
                                        $("#uploaded_fp_filepath").val("");
                                        $("#uploaded_fp_filename").val("");
                                        $("#uploaded_fp_filesize").val("");
                                        $('#faktur_pajak_url').val("");
                                        $("#faktur_pajak_approval").val("");

                                        var fakturStatus = $('#faktur_pajak_status').val();
                                        if(fakturStatus){
                                            if(fakturStatus == 'Faktur Pajak Normal-Pengganti'){
                                                $("#faktur_pajak_number").val("");
                                                $("#faktur_pajak_date").val("");
                                                $("#pajak_type").val("");
                                                $("#fp_amount").val("");
                                                $("#dpp_amount").val("");
                                                $('#ppnbm_amount').val("");

                                                $("#npwp_pembeli").val("");
                                                $("#nama_pembeli").val("");
                                                $("#alamat_pembeli").val("");

                                                $('#npwp_penjual').val("");
                                                $('#nama_penjual').val("");
                                                $('#alamat_penjual').val("");

                                            }
                                        }

                                        $("#faktur_pajak_status").val("");

                                    }
                                });

                                myDropzone.on("maxfilesexceeded", function(file){
                                    alert("Upload hanya boleh 1 file");
                                    this.removeFile(file);
                                });

                                myDropzone.on("sending", function handleTimeout(file, xhr, formData) {
                                    xhr.ontimeout = function (e) {
                                        alert('Upload Dibatalkan karena proses terlalu lama');
                                        this.removeFile(file);
                                        $('#progress-modal').modal('hide');
                                    };
                                });

                                myDropzone.on("uploadprogress", function(file, progress, bytesSent){
                                    console.log(progress);
                                    if(progress > 99){
                                        $('#progress-modal').modal('show');
                                    }
                                });

                                myDropzone.on("error", function(file, res) {
                                    $('#progress-modal').modal('hide');
                                    if(typeof res.uploaded!= 'undefined'){
                                        $('#uploaded_fp_filepath_2').val(res.uploaded.filepath);
                                        $('#uploaded_fp_filename_2').val(res.uploaded.filename);
                                        $('#uploaded_fp_filesize_2').val(res.uploaded.filesize);
                                    }
                                });
                                break;

                            case 'Faktur Dibatalkan' :
                                $('#faktur_pajak_url').val("");
                                $('#faktur_pajak_number').val("");
                                $('#faktur_pajak_approval').val("");
                                $('#faktur_pajak_status').val("");
                                $('#faktur_pajak_date').val("");
                                $('#fp_amount').val("");
                                $('#dpp_amount').val("");
                                $('#ppnbm_amount').val("");
                                $('#pajak_type').val("");

                                $('#npwp_penjual').val("");
                                $('#nama_penjual').val("");
                                $('#alamat_penjual').val("");

                                $('#npwp_pembeli').val("");
                                $('#nama_pembeli').val("");
                                $('#alamat_pembeli').val("");

                                var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                var str = '<div class="span6"><div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" style="text-align:left">Upload Faktur Normal/Normal-Pengganti<span class="required"> *</span></label>'+
                                    '<div class="controls" style="padding-left:70px">' +
                                    '<div class="dropzone dropzone-previews" id="my-awesome-dropzone2" style="width: 200px; height: 215px"></div>'+
                                    '</div>'+
                                    '</div></div>';

                                $(str).insertAfter("#first_faktur");

                                var nextFp = '<br class="secondary-fp"><br class="secondary-fp"><h4 class="form-section secondary-fp">Faktur Pajak Dibatalkan</h4>'+
                                    '<div class="custom-form fp-only secondary-fp">';

                                nextFp += '<div class="row-fluid">' +
                                    '<div class="span12">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label">URL Faktur Pajak Dibatalkan<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<div class="input-append span12">'+
                                    '<input type="text" id="faktur_pajak_url_2" name="faktur_pajak_url_2" value="'+res.url+'" class="m-wrap" readonly style="width: 88.2%">' +
                                    '<button type="button" class="btn yellow" id="detail_transaksi_2">Detail Transaksi</button>' +
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                nextFp +=   '<div class="row-fluid">' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_status_2">Status Faktur<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_status_2" name="faktur_pajak_status_2" value="'+res.data.statusFaktur+'" class="m-wrap span12" readonly>'+
                                    '                                                    </div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_approval_2">Status Approval<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_approval_2" name="faktur_pajak_approval_2" value="'+res.data.statusApproval+'" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="pajak_type_2">Jenis Pajak<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="pajak_type_2" name="pajak_type_2" class="m-wrap span12" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                                nextFp += '<div class="row-fluid">' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_number_2">No. Faktur Pajak<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_number_2" name="faktur_pajak_number_2" class="m-wrap span12" value="'+fpNumber+'" readonly>'+
                                    '<input type="hidden" id="uploaded_fp_filepath_2" name="uploaded_fp_filepath_2" class="m-wrap notempty large" value="'+res.uploaded.filepath+'" readonly/>'+
                                    '<input type="hidden" id="uploaded_fp_filename_2" name="uploaded_fp_filename_2" class="m-wrap large" value="'+res.uploaded.filename+'" readonly/>' +
                                    '<input type="hidden" id="uploaded_fp_filesize_2" name="uploaded_fp_filesize_2" class="m-wrap large" value="'+res.uploaded.filesize+'" readonly/>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="faktur_pajak_date_2">Tanggal Faktur Pajak<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="faktur_pajak_date_2" name="faktur_pajak_date_2" class="m-wrap span12" value="'+fpDate+'"readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                nextFp +=   '<div class="row-fluid">' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="dpp_amount_2">Jumlah DPP<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="dpp_amount_2" name="dpp_amount_2" class="m-wrap span12" value="'+res.data.jumlahDpp+'" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="fp_amount_2">Jumlah PPN<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="fp_amount_2" name="fp_amount_2" class="m-wrap span12" value="'+res.data.jumlahPpn+'" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '<div class="span4">' +
                                    '<div class="control-group fp-only secondary-fp">' +
                                    '<label class="control-label" for="ppnbm_amount_2">Jumlah PPnBM<span class="required"> *</span></label>' +
                                    '<div class="controls">' +
                                    '<input type="text" id="ppnbm_amount_2" name="ppnbm_amount_2" class="m-wrap span12" value="'+res.data.jumlahPpnBm+'" readonly>'+
                                    '</div>'+
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                nextFp +='</div>';

                                $(nextFp).insertAfter(".custom-form");
                                setPajakType2();

                                $("#form_save").validate();
                                $('input[name="faktur_pajak_url_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="faktur_pajak_status_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });


                                $('input[name="faktur_pajak_approval_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="pajak_type_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="faktur_pajak_number_2"]').rules("add",{
                                    required : true,
                                    equalFaktur: true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                        equalWithFaktur : "Nomor Faktur Pajak Tidak Sama"
                                    }
                                });

                                $('input[name="faktur_pajak_date_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="dpp_amount_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="fp_amount_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                $('input[name="ppnbm_amount_2"]').rules("add",{
                                    required : true,
                                    messages : {
                                        required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                    }
                                });

                                jQuery("#faktur_pajak_number_2").inputmask({
                                    "mask": "999.999-99.99999999",
                                });

                                jQuery('#fp_amount_2').inputmask('decimal',{
                                    groupSeparator: ".",
                                    radixPoint: ",",
                                    digits: 0,
                                    autoGroup: true,
                                });

                                jQuery('#dpp_amount_2').inputmask('decimal',{
                                    groupSeparator: ".",
                                    radixPoint: ",",
                                    digits: 0,
                                    autoGroup: true,
                                });

                                jQuery('#ppnbm_amount_2').inputmask('decimal',{
                                    groupSeparator: ".",
                                    radixPoint: ",",
                                    digits: 0,
                                    autoGroup: true,
                                });


                                myDropzone = new Dropzone("div#my-awesome-dropzone2",
                                    {
                                        addRemoveLinks: true,
                                        url: '<?php echo app_backend_url('main/berkas/upload_faktur_pajak'); ?>',
                                        paramName: "faktur_pajak", // The name that will be used to transfer the file
                                        maxFilesize: 5, // MB
                                        acceptedFiles : 'application/pdf,.pdf',
                                        createImageThumbnails: false,
                                        maxFiles : 1,
                                        dictDefaultMessage: "Drop file/klik disini untuk upload",
                                        dictRemoveFileConfirmation: "Anda yakin ingin menghapus dokumen Faktur Pajak ini?",
                                        dictRemoveFile: "Hapus File",
                                        timeout : 120000,
                                    }
                                );

                                myDropzone.on('success', function (file, res) {
                                    $('#progress-modal').modal('hide');
                                    $(file.previewElement).on('click', function () {
                                        var crtUrl = "<?php echo app_asset_url('tmp/')?>"+res.uploaded.filepath;
                                        window.open(crtUrl, "_blank");
                                    });

                                    $('#uploaded_fp_filepath').val(res.uploaded.filepath);
                                    $('#uploaded_fp_filename').val(res.uploaded.filename);
                                    $('#uploaded_fp_filesize').val(res.uploaded.filesize);

                                    var statusFaktur = res.data.statusFaktur;
                                    switch (statusFaktur) {
                                        case 'Faktur Pajak Normal-Pengganti':
                                            var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                            var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                            $('#faktur_pajak_url').val(res.url);
                                            $('#faktur_pajak_number').val(fpNumber);
                                            $('#faktur_pajak_status').val(res.data.statusFaktur);
                                            $('#faktur_pajak_approval').val(res.data.statusApproval);
                                            $('#faktur_pajak_date').val(fpDate);
                                            $('#fp_amount').val(res.data.jumlahPpn);
                                            $('#dpp_amount').val(res.data.jumlahDpp);
                                            $('#ppnbm_amount').val(res.data.jumlahPpnBm);

                                            $('#vendor_npwp').val(res.data.npwpPenjual);
                                            $('#vendor_name').val(res.data.namaPenjual);
                                            $('#vendor_address').val(res.data.alamatPenjual);

                                            $('#npwp_pembeli').val(res.data.npwpLawanTransaksi);
                                            $('#nama_pembeli').val(res.data.namaLawanTransaksi);
                                            $('#alamat_pembeli').val(res.data.alamatLawanTransaksi);

                                            $('#npwp_penjual').val(res.data.npwpPenjual);
                                            $('#nama_penjual').val(res.data.namaPenjual);
                                            $('#alamat_penjual').val(res.data.alamatPenjual);

                                            setPajakType();
                                            setPajakPeriodVal();
                                            break;
                                        case 'Faktur Pajak Normal':
                                            var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                            var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                            $('#faktur_pajak_url').val(res.url);
                                            $('#faktur_pajak_number').val(fpNumber);
                                            $('#faktur_pajak_status').val(res.data.statusFaktur);
                                            $('#faktur_pajak_approval').val(res.data.statusApproval);
                                            $('#faktur_pajak_date').val(fpDate);
                                            $('#fp_amount').val(res.data.jumlahPpn);
                                            $('#dpp_amount').val(res.data.jumlahDpp);
                                            $('#ppnbm_amount').val(res.data.jumlahPpnBm);

                                            $('#vendor_npwp').val(res.data.npwpPenjual);
                                            $('#vendor_name').val(res.data.namaPenjual);
                                            $('#vendor_address').val(res.data.alamatPenjual);

                                            $('#npwp_pembeli').val(res.data.npwpLawanTransaksi);
                                            $('#nama_pembeli').val(res.data.namaLawanTransaksi);
                                            $('#alamat_pembeli').val(res.data.alamatLawanTransaksi);

                                            $('#npwp_penjual').val(res.data.npwpPenjual);
                                            $('#nama_penjual').val(res.data.namaPenjual);
                                            $('#alamat_penjual').val(res.data.alamatPenjual);

                                            setPajakType();
                                            setPajakPeriodVal();
                                            break;

                                        default:
                                            alert("Faktur Pajak yang harus diupload adalah Faktur Pajak Normal atau Normal-Pengganti");
                                            this.removeAllFiles();
                                    }

                                });

                                myDropzone.on("removedfile", function(file) {

                                    if(this.files.length == 0){
                                        this.options.maxFiles = 1

                                        var cAjax = new ceki.fnAjax({
                                            url : '<?php echo app_backend_url('main/berkas/delete_faktur_pajak'); ?>',
                                            data : ({
                                                path: $('#uploaded_fp_filepath').val()
                                            }),
                                            successCallBack : function(resp) {
                                                jQuery.gritter.add({
                                                    title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                                    text: resp.message,
                                                    sticky: false,
                                                    time: 3000
                                                });
                                            }
                                        });

                                        //clear hidden input value uploaded_fp
                                        $("#uploaded_fp_filepath").val("");
                                        $("#uploaded_fp_filename").val("");
                                        $("#uploaded_fp_filesize").val("");
                                        $('#faktur_pajak_url').val("");
                                        $("#faktur_pajak_approval").val("");

                                        var fakturStatus = $('#faktur_pajak_status').val();
                                        if(fakturStatus){
                                            if(fakturStatus == 'Faktur Pajak Normal-Pengganti' || fakturStatus == 'Faktur Pajak Normal'){
                                                $("#faktur_pajak_number").val("");
                                                $("#faktur_pajak_date").val("");
                                                $("#pajak_type").val("");
                                                $("#pajak_period").val("");
                                                $("#pajak_year").val("");
                                                $("#fp_amount").val("");
                                                $("#dpp_amount").val("");
                                                $("#ppnbm_amount").val("");

                                                $("#npwp_pembeli").val("");
                                                $("#nama_pembeli").val("");
                                                $("#alamat_pembeli").val("");

                                                $('#npwp_penjual').val('');
                                                $('#nama_penjual').val("");
                                                $('#alamat_penjual').val("");

                                            }
                                        }

                                        $("#faktur_pajak_status").val("");

                                    }
                                });

                                myDropzone.on("maxfilesexceeded", function(file){
                                    alert("Upload hanya boleh 1 file");
                                    this.removeFile(file);
                                });

                                myDropzone.on("sending", function handleTimeout(file, xhr, formData) {
                                    xhr.ontimeout = function (e) {
                                        alert('Upload Dibatalkan karena proses terlalu lama');
                                        this.removeFile(file);
                                        $('#progress-modal').modal('hide');
                                    };
                                });

                                myDropzone.on("uploadprogress", function(file, progress, bytesSent){
                                    console.log(progress);
                                    if(progress > 99){
                                        $('#progress-modal').modal('show');
                                    }
                                });

                                myDropzone.on("error", function(file, res) {
                                    $('#progress-modal').modal('hide');
                                    if(typeof res.uploaded!= 'undefined'){
                                        $('#uploaded_fp_filepath_2').val(res.uploaded.filepath);
                                        $('#uploaded_fp_filename_2').val(res.uploaded.filename);
                                        $('#uploaded_fp_filesize_2').val(res.uploaded.filesize);
                                    }
                                });

                                jQuery("#faktur_pajak_number_2").inputmask({
                                    "mask": "999.999-99.99999999",
                                });
                                break;
                            default:
                        }
                    }

                });

                this.on("error", function(file, res) {
                    $('#progress-modal').modal('hide');
                    if(typeof res.uploaded!= 'undefined'){
                        $('#uploaded_fp_filepath').val(res.uploaded.filepath);
                        $('#uploaded_fp_filename').val(res.uploaded.filename);
                        $('#uploaded_fp_filesize').val(res.uploaded.filesize);

                        var btnScanner = '<button type="button" class="btn yellow" id="barcode_scanner">&nbsp;Gunakan Barcode Scanner&nbsp;&nbsp;</button>';
                        $(btnScanner).insertAfter('#my-awesome-dropzone');
                    }

                });

                this.on("removedfile", function(file) {

                    if(this.files.length == 0){
                        this.options.maxFiles = 1;
                        if(typeof $('#uploaded_fp_filepath_2').val() != 'undefined' && $('#uploaded_fp_filepath_2').val() != ''){
                            var cAjax = new ceki.fnAjax({
                                url : '<?php echo app_backend_url('main/berkas/delete_faktur_pajak_2'); ?>',
                                data : ({
                                    path: $('#uploaded_fp_filepath_2').val()
                                }),
                                successCallBack : function(resp) {
                                    jQuery.gritter.add({
                                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                        text: resp.message,
                                        sticky: false,
                                        time: 3000
                                    });
                                }
                            });

                            if(typeof $('#uploaded_fp_filepath').val() != 'undefined' && $('#uploaded_fp_filepath').val() != '') {
                                var cAjax = new ceki.fnAjax({
                                    url: '<?php echo app_backend_url('main/berkas/delete_faktur_pajak'); ?>',
                                    data: ({
                                        path: $('#uploaded_fp_filepath').val()
                                    }),
                                    successCallBack: function (resp) {
                                        jQuery.gritter.add({
                                            title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                            text: resp.message,
                                            sticky: false,
                                            time: 3000
                                        });
                                    }
                                });
                            }

                            //clear hidden input value uploaded_fp
                            $("#uploaded_fp_filepath").val("");
                            $("#uploaded_fp_filename").val("");
                            $("#uploaded_fp_filesize").val("");
                            $('#faktur_pajak_url').val("");
                            $("#faktur_pajak_approval").val("");

                            console.log($('#faktur_pajak_status').val())
                            console.log($('#faktur_pajak_number').val())

                            var fakturStatus = $('#faktur_pajak_status').val();
                            if(fakturStatus){
                                if(fakturStatus == 'Faktur Pajak Normal'){
                                    $("#faktur_pajak_number").val("");
                                    $("#faktur_pajak_date").val("");
                                    $("#pajak_type").val("");
                                    $("#pajak_period").val("");
                                    $("#pajak_year").val("");
                                    $("#fp_amount").val("");
                                    $("#dpp_amount").val("");
                                    $("#ppnbm_amount").val("");

                                    $("#npwp_pembeli").val("");
                                    $("#nama_pembeli").val("");
                                    $("#alamat_pembeli").val("");

                                    $('#npwp_penjual').val("");
                                    $('#nama_penjual').val("");
                                    $('#alamat_penjual').val("");
                                    setPajakPeriodVal();

                                }else if(fakturStatus == 'Faktur Pajak Normal-Pengganti'){
                                    $("#faktur_pajak_number").val("");
                                    $("#faktur_pajak_date").val("");
                                    $("#pajak_type").val("");
                                    $("#fp_amount").val("");
                                    $("#dpp_amount").val("");
                                    $("#ppnbm_amount").val("");

                                    $("#npwp_pembeli").val("");
                                    $("#nama_pembeli").val("");
                                    $("#alamat_pembeli").val("");

                                    $('#npwp_penjual').val("");
                                    $('#nama_penjual').val("");
                                    $('#alamat_penjual').val("");
                                }
                            }else{
                                $("#pajak_period").val("");
                                $("#pajak_year").val("");
                            }

                            $("#faktur_pajak_status").val("");

                            $('.secondary-fp').remove();

                        }else{
                            var cAjax = new ceki.fnAjax({
                                url : '<?php echo app_backend_url('main/berkas/delete_faktur_pajak'); ?>',
                                data : ({
                                    path: $('#uploaded_fp_filepath').val()
                                }),
                                successCallBack : function(resp) {
                                    jQuery.gritter.add({
                                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                        text: resp.message,
                                        sticky: false,
                                        time: 3000
                                    });
                                }
                            });

                            //clear hidden input value uploaded_fp
                            $("#uploaded_fp_filepath").val("");
                            $("#uploaded_fp_filename").val("");
                            $("#uploaded_fp_filesize").val("");
                            $('#faktur_pajak_url').val("");
                            $("#faktur_pajak_approval").val("");

                            var fakturStatus = $('#faktur_pajak_status').val();
                            if(fakturStatus){
                                if(fakturStatus == 'Faktur Pajak Normal'){
                                    $("#faktur_pajak_number").val("");
                                    $("#faktur_pajak_date").val("");
                                    $("#pajak_type").val("");
                                    $("#pajak_period").val("");
                                    $("#pajak_year").val("");
                                    $("#fp_amount").val("");
                                    $("#dpp_amount").val("");
                                    $("#ppnbm_amount").val("");

                                    $("#npwp_pembeli").val("");
                                    $("#nama_pembeli").val("");
                                    $("#alamat_pembeli").val("");

                                    $('#npwp_penjual').val("");
                                    $('#nama_penjual').val("");
                                    $('#alamat_penjual').val("");
                                    setPajakPeriodVal();

                                }else if(fakturStatus == 'Faktur Pajak Normal-Pengganti'){
                                    $("#faktur_pajak_number").val("");
                                    $("#faktur_pajak_date").val("");
                                    $("#pajak_type").val("");
                                    $("#fp_amount").val("");
                                    $("#dpp_amount").val("");
                                    $("#ppnbm_amount").val("");

                                    $("#npwp_pembeli").val("");
                                    $("#nama_pembeli").val("");
                                    $("#alamat_pembeli").val("");

                                    $('#npwp_penjual').val("");
                                    $('#nama_penjual').val("");
                                    $('#alamat_penjual').val("");

                                }

                            }
                            $('.secondary-fp').remove();

                            $("#faktur_pajak_status").val("");

                        }

                        $('#barcode_scanner').remove();
                    }
                });

                this.on("maxfilesexceeded", function(file){
                    alert("Upload hanya boleh 1 file");
                    this.removeFile(file);
                });

                this.on("sending", function handleTimeout(file, xhr, formData) {
                    // $('#progress-modal').modal('show');
                    xhr.ontimeout = function (e) {
                        alert('Upload Dibatalkan karena proses terlalu lama');
                        $('#progress-modal').modal('hide');
                        this.removeFile(file);
                    };
                });

                this.on("uploadprogress", function(file, progress, bytesSent){
                    console.log(progress);
                    if(progress > 99){
                        $('#progress-modal').modal('show');
                    }
                })

                if(jQuery('input[type=radio][name="tagihan_type"]:checked').val() == 'FP'){
                    this.enable();
                    $(this.element).css('background', '#ffffff')
                }else{
                    this.disable();
                    $(this.element).css('background', '#F9F9F9')
                }
            }
        });


		<?php if(isset($res_data['uploaded_fp_filepath_2'])):?>
        var mySecondDropzone = new Dropzone("div#my-awesome-dropzone2",
            {
                addRemoveLinks: true,
                url: '<?php echo app_backend_url('main/berkas/upload_faktur_pajak'); ?>',
                paramName: "faktur_pajak", // The name that will be used to transfer the file
                maxFilesize: 5, // MB
                acceptedFiles : 'application/pdf,.pdf',
                createImageThumbnails: false,
                maxFiles : 1,
                dictDefaultMessage: "Drop file/klik disini untuk upload",
                dictRemoveFileConfirmation: "Seluruh data & dokumen Faktur Pajak akan direset, Anda yakin ingin menghapus dokumen Faktur Pajak ini?",
                dictRemoveFile: "Hapus File",
                timeout : 120000,
                init: function() {
                    //preload file if any
                    var mockFile = {
                        name: '<?php echo $res_data['uploaded_fp_filename_2'] ?>',
                        size: '<?php echo $res_data['uploaded_fp_filesize_2'] ?>'
                    };

                    this.options.addedfile.call(this, mockFile);
                    this.emit("complete", mockFile);
                    this.options.maxFiles = this.options.maxFiles - 1;
                    this.files.push(mockFile);

                    $(this.files[0].previewElement).on("click", function () {
                        //download
                        var urls = $('#uploaded_fp_filepath_2').val();
                        var crtUrl = "<?php echo app_asset_url('faktur_pajak/')?>" + urls;
                        window.open(crtUrl, "_blank");
                    });

                    this.on("maxfilesexceeded", function(file){
                        alert("Upload hanya boleh 1 file");
                        this.removeFile(file);
                    });

                    this.on("removedfile", function(file) {

                        if(this.files.length == 0){
                            this.options.maxFiles = 1

                            if(typeof $('#uploaded_fp_filepath_2').val() != 'undefined'){

                                if(typeof $('#uploaded_fp_filepath').val() != 'undefined' && $('#uploaded_fp_filepath').val() != '') {
                                    myMainDropzone.removeAllFiles();
                                }
                            }
                        }
                    });

                }
            }
        );

        jQuery("#faktur_pajak_number_2").inputmask({
            "mask": "999.999-99.99999999",
        });

        jQuery('#fp_amount_2').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
        });

        jQuery('#dpp_amount_2').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
        });

        jQuery('#ppnbm_amount_2').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
        });


        rules["faktur_pajak_url_2"] = { required: true };
        messages["faktur_pajak_url_2"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };

        rules["faktur_pajak_status_2"] = { required: true };
        messages["faktur_pajak_status_2"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };

        rules["faktur_pajak_approval_2"] = { required: true };
        messages["faktur_pajak_approval_2"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };

		<?php if($res_data['faktur_pajak_status_2'] == "Faktur Dibatalkan"):?>
        rules["faktur_pajak_number_2"] = { required: true };
        messages["faktur_pajak_number_2"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
		<?php else:?>
        rules["faktur_pajak_number_2"] = { required: true,  equalFaktur: true, };
        messages["faktur_pajak_number_2"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
            equalFaktur : "Nomor Faktur Pajak Tidak Sama"
        };
		<?php endif;?>

        rules["faktur_pajak_date_2"] = { required: true };
        messages["faktur_pajak_date_2"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",

        };

		<?php endif;?>

        jQuery('#detail_transaksi').live('click', function(){
            jQuery('#result').html("");
            jQuery('#detail_fp_modal').modal('show');
            App.blockUI(jQuery('#result'), true, '<?php echo $this->lang->line('global_loading_data'); ?>');
            var fpUrl = $('#faktur_pajak_url').val();
            if(fpUrl){
                jQuery.get('<?php echo app_backend_url('main/berkas/get_detail_fp?'); ?>'+'url=' + encodeURIComponent(fpUrl), function (html) {
                    App.unblockUI(jQuery('#result'));
                    jQuery('#result').html(html);
                })
            }else{
                App.unblockUI(jQuery('#result'));
                jQuery('#result').html('<div class="report-search-notfound"><h1 align="center">Tidak Ada Data</h1></div>');
            }
        });

        jQuery('#detail_transaksi_2').live('click', function(){
            jQuery('#result').html("");
            jQuery('#detail_fp_modal').modal('show');
            App.blockUI(jQuery('#result'));
            var fpUrl = $('#faktur_pajak_url_2').val();
            if(fpUrl){
                jQuery.get('<?php echo app_backend_url('main/berkas/get_detail_fp?'); ?>'+'url=' + encodeURIComponent(fpUrl), function (html) {
                    App.unblockUI(jQuery('#result'));
                    jQuery('#result').html(html);
                })
            }else{
                App.unblockUI(jQuery('#result'));
                jQuery('#result').html('<div class="report-search-notfound"><h1 align="center">Tidak Ada Data</h1></div>');
            }
        });

        jQuery("#npwp_pembeli").inputmask({"mask": "99.999.999.9-999.999"});
        jQuery("#npwp_penjual").inputmask({"mask": "99.999.999.9-999.999"});
        // jQuery("#vendor_npwp").inputmask({"mask": "99.999.999.9-999.999"});


        jQuery("#faktur_pajak_number").inputmask({
            "mask": "999.999-99.99999999",
            oncomplete: function () {
                setPajakType();
            },
            oncleared: function () {
                $('input[name="pajak_type"]').val("");
                $('#faktur_pajak_date').val("");
            },
        });

        function setPajakType() {
            var fp = $('#faktur_pajak_number').inputmask("unmaskedvalue")
            var kodeTransaksi = fp.substr(0,2)
            var nonWapu = [1,4,6,7,8,9];
            var wapu =[2,3];

            if(nonWapu.indexOf(Number(kodeTransaksi)) > -1){
                $('#pajak_type').val('NON WAPU')
                setPpnCode('NON WAPU')
            }else if(wapu.indexOf(Number(kodeTransaksi)) > -1){
                $('#pajak_type').val('WAPU')
                setPpnCode('WAPU')
            }else{
                $('#pajak_type').val('')
            }

        }

        function setPajakType2() {
            var fp = $('#faktur_pajak_number_2').inputmask("unmaskedvalue")
            var kodeTransaksi = fp.substr(0,2)
            var nonWapu = [1,4,6,7,8,9];
            var wapu =[2,3];

            if(nonWapu.indexOf(Number(kodeTransaksi)) > -1){
                $('#pajak_type_2').val('NON WAPU')

            }else if(wapu.indexOf(Number(kodeTransaksi)) > -1){
                $('#pajak_type_2').val('WAPU')

            }else{
                $('#pajak_type_2').val('')
            }

        }

        function setPpnCode(jenisPajak){
            jQuery.get('<?php echo app_backend_url('main/berkas/get_ppn_code/'); ?>'+jenisPajak, function (data) {
                $('#ppn_code').empty();
                $('#ppn_percentage').val("");
                $('#ppn_code').append('<option value="" data-percentage="0">Pilih Satu</option>');
                data.forEach(function (t) {
                    $('#ppn_code').append('<option value='+t.code+' data-percentage='+t.percentage+'>'+t.code+' - '+t.description+'</option>')
                })

                if($('#vendor_type option:selected').val() == 'ZV13'){
//                    $("#ppn_code option[value='V2']").remove();
//                    $("#ppn_code option[value='V3']").remove();
                }else{
                    $("#ppn_code option[value='V5']").remove();
                    $("#ppn_code option[value='V6']").remove();
                }

                $('#ppn_val').val("0");
                pressValue();
            })
        }


        function setPajakPeriodVal() {
            if($('#faktur_pajak_date').val()){
                var from = $('#faktur_pajak_date').val().split("-");
                var year = from[2];
                var month = from[1];

                $('#pajak_period').val(month+'/'+month);
                $('#pajak_year').val(year)
            }
        }

        function setPajakPeriodValGanti() {
            if($('#faktur_pajak_date_2').val()){
                var from = $('#faktur_pajak_date_2').val().split("-");
                var year = from[2];
                var month = from[1];

                $('#pajak_period').val(month+'/'+month);
                $('#pajak_year').val(year)
            }
        }

        jQuery('#ppn_val').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
            oncomplete : function () {
                updateTotal(true);
                pressValueWithoutPercentage();
            },
            oncleared : function () {
                updateTotal(true);
                pressValueWithoutPercentage();
            }
        });

        jQuery('#pph_val').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
            oncomplete : function () {
                updateTotal(true);
                pressValueWithoutPercentage();
            },
            oncleared : function () {
                updateTotal(true);
                pressValueWithoutPercentage();
            }
        })

        jQuery('#real_cost').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
            oncomplete : function () {
                pressValue();
            }
        })

        jQuery('#fp_amount').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
        });

        jQuery('#dpp_amount').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
        });

        jQuery('#ppnbm_amount').inputmask('decimal',{
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0,
            autoGroup: true,
        })


        $('#fp_amount').on('input propertychange paste', function (e) {
            var reg = /^0+/gi;
            if (this.value.match(reg)) {
                this.value = this.value.replace(reg, '');
            }
        });

        $('#ppn_val').on('input propertychange paste', function (e) {
            var reg = /^0+/gi;
            if (this.value.match(reg)) {
                this.value = this.value.replace(reg, '');
            }
        });

        jQuery("#ppn_code").on('change',function () {
            $('#ppn_percentage').val($(this).find(':selected').attr('data-percentage') + ' %');
            pressValue();
        });
        jQuery("#pph_code").on('change',function () {
            $('#pph_percentage').val($(this).find(':selected').attr('data-percentage')  + ' %');
            pressValue();
        });
        jQuery('#tagihan_type_wrapper input[name="tagihan_type"]').click(function () {
            if($(this).val() == 'FP'){
                $('.fp-only').find(':input').each(function () {
                    $(this).prop('disabled', false)
                });

                myMainDropzone.enable();
                $("#my-awesome-dropzone").css('background', '#ffffff');

                //check again
            }else if($(this).val() == 'NON FP'){
                if(myMainDropzone.files.length > 0){
                    var r = confirm("Seluruh data & dokumen Faktur Pajak akan direset, Anda yakin ingin mengubah jenis tagihan?");
                    if(r != true){
                        $(this).parent().removeClass("checked");
                        $('#tagihan_type_wrapper input[name="tagihan_type"][value="FP"]').parent().addClass("checked");
                        return false;
                    }
                    if(r){
                        myMainDropzone.removeAllFiles(true);
                    }
                }

                myMainDropzone.disable();
                $('.fp-only').find(':input').each(function () {
                    $(this).val("");
                    $(this).prop('disabled', true)
                });
                $('input[name="ppn_percentage"]').val("");
                $("#my-awesome-dropzone").css('background', '#F9F9F9');

            }

            getUploadFile();
        });

        if(jQuery('input[type=radio][name="tagihan_type"]:checked').val() == 'FP'){
            $('.fp-only').find(':input').each(function () {
                $(this).prop('disabled', false)
            });
        }else{
            $('.fp-only').find(':input').each(function () {
                $(this).val("");
                $(this).prop('disabled', true)
            });
            $('input[name="ppn_percentage"]').val("")
        }

        function updatePajakNett(){
            var realCost = Number($('#real_cost').inputmask('unmaskedvalue'))
            var ppn_percentage = Number(jQuery('#ppn_code').find(':selected').attr('data-percentage'))
            var pph_percentage = Number(jQuery('#pph_code').find(':selected').attr('data-percentage'))

            var ppn_val = realCost * ppn_percentage/100;
            var pph_val = realCost * pph_percentage/100;

            var netto_val = realCost - pph_val;

            if($('input[name=pajak_type]').val() == 'NON WAPU'){
                netto_val = netto_val + ppn_val;
            }

            jQuery("#ppn_val").val(ceki.fnFormatNumber(Math.round(ppn_val)));
            if(jQuery('input[type=radio][name="tagihan_type"]:checked').val() == 'NON FP' || ppn_val < 1){
                jQuery("#ppn_val").val("");
            }
            jQuery("#pph_val").val(ceki.fnFormatNumber(Math.round(pph_val)));
            jQuery("#netto_cost_val").val(ceki.fnFormatNumber(Math.round(netto_val)));

        }

        function pressValue() {
            var realCost = jQuery('#real_cost').val();
            realCost = realCost.replace(/[._]/g,'');
            realCost = parseInt(realCost);

            var ppn = jQuery('#ppn_code').find(':selected').attr('data-percentage');
            ppn = ppn.replace(/[._]/g,'');
            ppn = parseFloat(ppn);
            if(!isNaN(ppn)) {
                ppn = ppn/100;
            } else {
                ppn = '';
            }

            var pph = jQuery('#pph_code').find(':selected').attr('data-percentage');
            pph = pph.replace(/[_]/g,'');
            pph = pph.replace(/[,]/g,'.');
            pph = parseFloat(pph);
            if(!isNaN(pph)) {
                pph = pph/100;
            } else {
                pph = '';
            }

            if(!isNaN(realCost)) {
                calculateCost(realCost,ppn,pph);
            } else {
                jQuery("#ppn_val").val('');
                jQuery("#pph_val").val('');
                jQuery("#netto_cost_val").val('');
            }
        }

        function pressValueWithoutPercentage() {
            var realCost = jQuery('#real_cost').val();
            realCost = realCost.replace(/[._]/g,'');
            realCost = parseInt(realCost);

            var ppn_val = jQuery('#ppn_val').val();
            ppn_val = ppn_val.replace(/[._]/g,'');
            ppn_val = parseInt(ppn_val);

            var pph_val = jQuery('#pph_val').val();
            pph_val = pph_val.replace(/[._]/g,'');
            pph_val = parseInt(pph_val);

            if(!isNaN(realCost)) {
                calculateCostNoPercentage(realCost,ppn_val,pph_val);
            } else {
                jQuery("#ppn_val").val('');
                jQuery("#pph_val").val('');
                jQuery("#netto_cost_val").val('');
            }
        }
        function calculateCostNoPercentage(realCost,ppn_val,pph_val) {
            if(isNaN(ppn_val)){
                ppn_val = 0;
            }

            if(isNaN(pph_val)){
                pph_val = 0;
            }

            var netto_val = realCost - pph_val;

            if($('input[name=pajak_type]').val() == 'NON WAPU'){
                netto_val = netto_val + ppn_val;
            }

            jQuery("#netto_cost_val").val(ceki.fnFormatNumber(Math.round(netto_val)));
        }
        function calculateCost(realCost,ppn,pph) {
            var ppn_val = 0;
            if(ppn != '') {
                ppn_val = parseFloat(realCost * ppn);
            }

            var pph_val = 0;
            if(pph != '') {
                pph_val = parseFloat(realCost * pph);
            }

            var netto_val = realCost - pph_val;

            if($('input[name=pajak_type]').val() == 'NON WAPU'){
                netto_val = netto_val + ppn_val;
            }

            jQuery("#ppn_val").val(ceki.fnFormatNumber(Math.round(ppn_val)));
            if(jQuery('input[type=radio][name="tagihan_type"]:checked').val() == 'NON FP' || ppn_val < 1){
                jQuery("#ppn_val").val("");
            }
            jQuery("#pph_val").val(ceki.fnFormatNumber(Math.round(pph_val)));
            jQuery("#netto_cost_val").val(ceki.fnFormatNumber(Math.round(netto_val)));
        }
        function updateTotal(noPercentage) {
            if($('#detail_cost > tbody > tr').length > 0 || $('#detail_cost_pg > tbody > tr').length > 0){
                var values = $("input[name^='item_pembayaran_nilai']").map(function(){return $(this).inputmask('unmaskedvalue');}).get();
                var total = 0;
                var um_amount = 0;
                var retention_amount = 0;
                values.forEach(function (val) {
                    total += Number(val);
                })

                if(noPercentage){
                    um_amount = Number($('#um_amount').inputmask('unmaskedvalue'))
                    retention_amount = Number($('#retention_amount').inputmask('unmaskedvalue'));
                }else{
                    // um_amount = total * Number($('#um_percentage').inputmask('unmaskedvalue')) / 100;
                    var umPercentage = $('#um_percentage').inputmask('unmaskedvalue')
                    umPercentage = umPercentage.replace(",", ".")
                    um_amount = total * Number(umPercentage) / 100;
                    retention_amount = total * Number($('#retention_percentage').inputmask('unmaskedvalue')) / 100;

                    jQuery('#um_amount').val(ceki.fnFormatNumber(Math.round(um_amount)))
                    jQuery('#retention_amount').val(ceki.fnFormatNumber(Math.round(retention_amount)))

                }

                var allTots = total - um_amount - retention_amount;

                jQuery('#real_cost').val(ceki.fnFormatNumber(Math.round(allTots)))
            }else{
                if($('#document_type_id option:selected').attr('data-name') == 'LA' || $('#document_type_id option:selected').attr('data-name') == 'LU' || $('#document_type_id option:selected').attr('data-name') == 'TTRET' || $('#document_type_id option:selected').attr('data-name') == 'TTUM'){

                    var total = Number($('#raw_amount').inputmask('unmaskedvalue'))
                    if(noPercentage){
                        um_amount = Number($('#um_amount').inputmask('unmaskedvalue'))
                        retention_amount = Number($('#retention_amount').inputmask('unmaskedvalue'));
                    }else{
                        // um_amount = total * Number($('#um_percentage').inputmask('unmaskedvalue')) / 100;
                        var umPercentage = $('#um_percentage').inputmask('unmaskedvalue')
                        umPercentage = umPercentage.replace(",", ".")
                        um_amount = total * Number(umPercentage) / 100;
                        retention_amount = total * Number($('#retention_percentage').inputmask('unmaskedvalue')) / 100;

                        jQuery('#um_amount').val(ceki.fnFormatNumber(Math.round(um_amount)))
                        jQuery('#retention_amount').val(ceki.fnFormatNumber(Math.round(retention_amount)))

                    }

                    var allTots = total - um_amount - retention_amount;

                    jQuery('#real_cost').val(ceki.fnFormatNumber(Math.round(allTots)))

                }else{
                    jQuery('#real_cost').val(ceki.fnFormatNumber(Math.round(0)))
                }
            }

        }

        jQuery.validator.addMethod(
            "validateKodeTransaksi",
            function(value, element) {
                if(value != ''){
                    var fp = value.replace(/[._]/g,'');
                    var kodeTransaksi = fp.substr(0,2)
                    var validCode = [1,3,4,6,7,8];
                    return validCode.indexOf(Number(kodeTransaksi)) > -1
                } else {
                    return true;
                }
            },
            "<?php echo $this->lang->line('global_error_date'); ?>"
        );

        jQuery.validator.addMethod(
            "validateKodeStatus",
            function(value, element) {
                if(value != ''){
                    var fp = value.replace(/[._]/g,'');
                    var kodeStatus = fp.substr(2,1)
                    var validCode = [0,1];
                    return validCode.indexOf(Number(kodeStatus)) > -1
                } else {
                    return true;
                }
            },
            "<?php echo $this->lang->line('global_error_date'); ?>"
        );

        jQuery.validator.addMethod(
            "validatePajakDateYear",
            function(value, element) {
                if(value != ''){
                    var from = value.split("-");
                    var yr = from[2];
                    var endYear = yr.substr(2);

                    var fpRaw = $('#faktur_pajak_number').val();
                    var fp = fpRaw.replace(/[-._]/g,'')
                    var kodeStatus = fp.charAt(2)
                    var kodeYear = fp.substr(6,2);

                    if(kodeStatus == '0'){
                        return endYear == kodeYear
                    }else{
                        return true
                    }


                } else {
                    return true;
                }
            },
            "<?php echo $this->lang->line('global_error_date'); ?>"
        );

        jQuery.validator.addMethod(
            "validateNpwpPtpp",
            function(value, element) {
                if(value != ''){
                    return value == '01.001.613.7-093.000'
                } else {
                    return true;
                }
            },
            "Nomor NPWP Pembeli Salah"
        );

        jQuery.validator.addMethod(
            "validateApproval",
            function(value, element) {
                if(value != ''){
                    return value == 'Faktur Valid, Sudah Diapprove oleh DJP'
                } else {
                    return true;
                }
            },
            "FP Belum DiApprove DJP"
        );

        rules["faktur_pajak_number"] = { required: true, maxlength: 45, validateKodeTransaksi: true, validateKodeStatus: true  };
        messages["faktur_pajak_number"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
            maxlength: "<?php echo sprintf($this->lang->line('global_error_maxlength_field'),$this->lang->line('global_error_this_field'),45); ?>",
            validateKodeTransaksi : 'Kode Transaksi Salah',
            validateKodeStatus : 'Kode Status Salah'
        };

        rules["faktur_pajak_date"] = { required: true, indonesianDate: true, validatePajakDateYear: true };
        messages["faktur_pajak_date"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>",
            validatePajakDateYear : "Tanggal Faktur Pajak tidak Sesuai"
        };
        
        rules["pajak_period"] = { required: true};
        messages["pajak_period"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["pajak_year"] = { required: true};
        messages["pajak_year"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["barang_jasa_name"] = { required: true};
        messages["barang_jasa_name"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        
        rules["fp_amount"] = { required: true };
        messages["fp_amount"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>"
        };
        
        rules["ppn_code"] = { required: true};
        messages["ppn_code"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["ppn_val"] = { required: true};
        messages["ppn_val"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["pph_code"] = { required: true};
        messages["pph_code"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["pph_val"] = { required: true};
        messages["pph_val"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["pajak_type"] = { required: true };
        messages["pajak_type"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["faktur_pajak_status"] = { required: true};
        messages["faktur_pajak_status"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["faktur_pajak_url"] = { required: true};
        messages["faktur_pajak_url"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["faktur_pajak_approval"] = { required: true, validateApproval: true };
        messages["faktur_pajak_approval"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
            validateApproval : "FP Belum DiApprove DJP"
        };
        
        rules["dpp_amount"] = { required: true };
        messages["dpp_amount"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>"
        };
        
        rules["ppnbm_amount"] = { required: true };
        messages["ppnbm_amount"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["npwp_pembeli"] = { required: true, validateNpwpPtpp: true };
        messages["npwp_pembeli"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
            validateNpwpPtpp: "Nomor NPWP PT PP Salah"
        };
        
        rules["nama_pembeli"] = { required: true };
        messages["nama_pembeli"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["alamat_pembeli"] = { required: true };
        messages["alamat_pembeli"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["npwp_penjual"] = { required: true};
        messages["npwp_penjual"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["nama_penjual"] = { required: true };
        messages["nama_penjual"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["alamat_penjual"] = { required: true };
        messages["alamat_penjual"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };
        
        rules["uploaded_fp_filepath"] = { required: true};
        messages["uploaded_fp_filepath"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
        };

        var form = $('#form_save');
        var error = $('.alert-error', form);
        var success = $('.alert-success', form);

        $('#form_wizard_1').bootstrapWizard({
            'nextSelector': '.button-next',
            'previousSelector': '.button-previous',
            onTabClick: function (tab, navigation, index) {
                return false;
            },
            onNext: function (tab, navigation, index) {
                App.blockUI(jQuery("#form_wizard_1"), true, '<?php echo $this->lang->line('global_please_wait'); ?>');
                success.hide();
                error.hide();

                if(index == 1){
                    var obj = checkBerkasNumber().responseText;
                    json = jQuery.parseJSON(obj);
                    if(json.success == false){
                        App.unblockUI(jQuery("#form_wizard_1"));
                        error.show();
                        return false;
                    }
                }
                if(index == 4){
                    var allowNext = true;
                    var allowNext2 = true;
                    $('input[name^="berkas_files["]').each(function(){
                        if($(this).val()){
                            if(typeof this.files[0] != 'undefined'){
                                console.log(this.files[0].type)
                                return allowNext = this.files[0].type == 'application/pdf'
                            }
                        }
                    });

                    $('input[name^="berkas_files["]').each(function(){
                        if($(this).val()){
                            if(typeof this.files[0] != 'undefined'){
                                var fileSize = this.files[0].size;
                                return allowNext2 = fileSize <= 2000000
                            }
                        }
                    });
                    if(!allowNext){
                        App.unblockUI(jQuery("#form_wizard_1"));
                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: "Tipe file yang harus diupload adalah PDF",
                            sticky: false,
                            time: 3000
                        });
                        error.show();
                        return false;
                    }

                    if(!allowNext2){
                        App.unblockUI(jQuery("#form_wizard_1"));
                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: "Maximum ukuran files masing-masing adalah 2MB",
                            sticky: false,
                            time: 3000
                        });
                        error.show();
                        return false;
                    }
                }

                if (form.valid() == false) {
                    return false;
                }

                var total = navigation.find('li').length;
                var current = index + 1;

                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);

                jQuery('li', $('#form_wizard_1')).removeClass("done");

                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            },
            onPrevious: function (tab, navigation, index) {
                App.blockUI(jQuery("#form_wizard_1"), true, '<?php echo $this->lang->line('global_please_wait'); ?>');
                success.hide();
                error.hide();

                var total = navigation.find('li').length;
                var current = index + 1;

                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);

                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }

                App.scrollTo($('.page-title'));
            },
            onTabShow: function (tab, navigation, index) {
                App.unblockUI(jQuery("#form_wizard_1"));
                var total = navigation.find('li').length;
                var current = index + 1;
                var $percent = (current / total) * 100;
                $('#form_wizard_1').find('.bar').css({
                    width: $percent + '%'
                });
            }
        });

        $('#form_wizard_1').find('.button-previous').hide();
        $('#form_wizard_1 .button-submit').hide();

        jQuery("#form_save").validate({
            onsubmit: false,
            doNotHideMessage: true,
            rules: rules,
            messages: messages,
            errorElement: 'span',
            errorClass: 'validate-inline',
            errorPlacement: function(error, element) {
                if (element.attr("name") == "vendor_name") {
                    error.addClass("no-left-padding").insertAfter("#vendor_tools");
                } else if (element.attr("name") == "berkas_files[]") {
                    error.insertAfter(".upload_format");
                } else {
                    error.insertAfter(element);
                }

            },
            highlight: function (element) {
                $(element).removeClass('ok');
                $(element).closest('.control-group').removeClass('success').addClass('error');
            },
            unhighlight: function (element) {
                $(element).closest('.control-group').removeClass('error');
            },
            success: function (label) {
                if (label.attr("for") == "invoice_number" || label.attr("for") == "faktur_pajak_number" || label.attr("for") == "bap_bpg_dub_number[]"  || label.attr("for") == "surat_jalan_lkp_number" || label.attr("for") == "spk_po_number" || label.attr("for") == "berkas_file[]") {
                    label.closest('.control-group').removeClass('error').addClass('success');
                    label.remove();
                } else {
                    label.addClass('valid ok').closest('.control-group').removeClass('error').addClass('success');
                }
            },
            invalidHandler: function(e, validator) {
                App.unblockUI(jQuery("#form_wizard_1"));
                success.hide();
                error.show();
                App.scrollTo(error, -200);

                var errors = validator.numberOfInvalids();
                if (errors) {
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: '<?php echo $this->lang->line('global_error_form'); ?>',
                        sticky: false,
                        time: 3000
                    });

                    return false;
                }
                e.preventDefault();
            }
        });

        jQuery('button[type=submit]').click(function (e) {
            var button = jQuery(this);
            buttonForm = button.closest('form');
            buttonForm.data('submittedBy', button);
        });

        jQuery('#form_save').submit(function(e) {
            e.preventDefault();
            if(jQuery(this).valid()){
                var form = jQuery(this);
                var submittedBy = form.data('submittedBy');
                if(submittedBy.val() == 1) {
                    jConfirm('<?php echo $this->lang->line('berkas_confirm_rilis'); ?>', '<?php echo $this->lang->line('global_info'); ?>', function(r) {
                        if(r){
                            jQuery(".btn.button-submit.green").text('Merilis data, mohon tunggu..');
                            App.blockUI(jQuery("#main-container"), true, '<?php echo $this->lang->line('global_saving_data'); ?>');

                            jQuery('#is_rilis').val(1);
                            ajaxSubmit(form);
                        }
                    });
                } else {
                    jQuery(".btn.button-submit.blue").text('Merilis data, mohon tunggu..');
                    App.blockUI(jQuery("#main-container"), true, '<?php echo $this->lang->line('global_saving_data'); ?>');
                    jQuery('#is_rilis').val(0);
                    ajaxSubmit(form);
                }
            }
        });

        function ajaxSubmit(form) {
            jQuery('.form-actions').each(function(){
                jQuery(this).find(':button').attr("disabled","disabled");
            });

            var formData = new FormData(form[0]);
            jQuery.ajax({
                url: '<?php echo app_backend_url('main/berkas/save'); ?>',
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function (obj) {
                    if(obj.success == true) {
                        App.unblockUI(jQuery("#main-container"));
                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: '<?php echo $this->lang->line('global_success_save'); ?>',
                            sticky: false,
                            time: 3000
                        });

                        jQuery('.save').attr("disabled","disabled");

                        if(typeof sock != 'undefined' && IS_SOCK == 1) {
                            sock.send();
                        }

                        setTimeout(function(){window.location= '<?php echo app_backend_url('main/berkas/'); ?>'},1000);

                    } else {
                        jQuery(".btn.button-submit.green").text('<?php echo $this->lang->line('global_save_release'); ?>');
                        jQuery('.form-actions').each(function(){
                            jQuery(this).find(':button').removeAttr("disabled");
                        });

                        App.unblockUI(jQuery("#main-container"));
                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: obj.message,
                            sticky: true,
                            time: 3000
                        });

                        if(obj.expired) {
                            setTimeout(function(){window.location= '<?php echo app_backend_url('login'); ?>'},1000);
                        }
                    }
                }
            });
        }

        jQuery('#qrcode-form').submit(function(e) {
            if(jQuery('#barcode_faktur_url').val() != '' && $("#qrcode_modal").data('blockUI.isBlocked') != 1){
                App.blockUI(jQuery("#qrcode_modal"), true, '<?php echo $this->lang->line('global_please_wait'); ?>');
                setTimeout(function(){
                    jQuery.ajax({
                        url: '<?php echo app_backend_url('main/berkas/get_scanned_url'); ?>',
                        type: 'POST',
                        data: {
                            barcode_faktur_url: jQuery('#barcode_faktur_url').val()
                        },
                        dataType: 'JSON',
                        success: function (obj) {
                            if(obj.success == true) {
                                App.unblockUI(jQuery("#qrcode_modal"));
                                jQuery.gritter.add({
                                    title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                    text: obj.message,
                                    sticky: false,
                                    time: 3000
                                });
                                console.log(obj.data);

                                $("#qrcode_modal").modal('hide');

                                if(typeof obj.data != 'undefined'){
                                    var statusFaktur = obj.data.statusFaktur;
                                    switch (statusFaktur){
                                        case 'Faktur Pajak Normal':
                                            var fpNumber = obj.data.kdJenisTransaksi + obj.data.fgPengganti + obj.data.nomorFaktur;
                                            var fpDate =  obj.data.tanggalFaktur.split('/').join("-");
                                            $('#faktur_pajak_url').val($("#barcode_faktur_url").val());
                                            $('#faktur_pajak_number').val(fpNumber);
                                            $('#faktur_pajak_approval').val(obj.data.statusApproval);
                                            $('#faktur_pajak_status').val(obj.data.statusFaktur);
                                            $('#faktur_pajak_date').val(fpDate);
                                            $('#fp_amount').val(obj.data.jumlahPpn);
                                            $('#dpp_amount').val(obj.data.jumlahDpp);
                                            $('#ppnbm_amount').val(obj.data.jumlahPpnBm);

                                            $('#vendor_npwp').val(obj.data.npwpPenjual);
                                            $('#vendor_name').val(obj.data.namaPenjual);
                                            $('#vendor_addobjs').val(obj.data.alamatPenjual);

                                            $('#npwp_penjual').val(obj.data.npwpPenjual);
                                            $('#nama_penjual').val(obj.data.namaPenjual);
                                            $('#alamat_penjual').val(obj.data.alamatPenjual);

                                            $('#npwp_pembeli').val(obj.data.npwpLawanTransaksi);
                                            $('#nama_pembeli').val(obj.data.namaLawanTransaksi);
                                            $('#alamat_pembeli').val(obj.data.alamatLawanTransaksi);

                                            setPajakType();
                                            setPajakPeriodVal();
                                            break;

                                        case 'Faktur Pajak Normal-Pengganti':
                                            var fpNumber = obj.data.kdJenisTransaksi + obj.data.fgPengganti + obj.data.nomorFaktur;
                                            var fpDate =  obj.data.tanggalFaktur.split('/').join("-");
                                            $('#faktur_pajak_url').val($("#barcode_faktur_url").val());
                                            $('#faktur_pajak_number').val(fpNumber);
                                            $('#faktur_pajak_approval').val(obj.data.statusApproval);
                                            $('#faktur_pajak_status').val(obj.data.statusFaktur);
                                            $('#faktur_pajak_date').val(fpDate);
                                            $('#fp_amount').val(obj.data.jumlahPpn);
                                            $('#dpp_amount').val(obj.data.jumlahDpp);
                                            $('#ppnbm_amount').val(obj.data.jumlahPpnBm);

                                            $('#vendor_npwp').val(obj.data.npwpPenjual);
                                            $('#vendor_name').val(obj.data.namaPenjual);
                                            $('#vendor_addobjs').val(obj.data.alamatPenjual);

                                            $('#npwp_penjual').val(obj.data.npwpPenjual);
                                            $('#nama_penjual').val(obj.data.namaPenjual);
                                            $('#alamat_penjual').val(obj.data.alamatPenjual);

                                            $('#npwp_pembeli').val(obj.data.npwpLawanTransaksi);
                                            $('#nama_pembeli').val(obj.data.namaLawanTransaksi);
                                            $('#alamat_pembeli').val(obj.data.alamatLawanTransaksi);

                                            setPajakType();

                                            var filePath2 = "";
                                            var fileName2 = "";
                                            var fileSize2 = "";

                                            if($('#uploaded_fp_filepath_2').val()){
                                                var filePath2 = $('#uploaded_fp_filepath_2').val();
                                                var fileName2 = $('#uploaded_fp_filename_2').val();
                                                var fileSize2 = $('#uploaded_fp_filesize_2').val();

                                                $('#uploaded_fp_filepath').val(filePath2);
                                                $('#uploaded_fp_filename').val(fileName2);
                                                $('#uploaded_fp_filesize').val(fileSize2);
                                            }

                                            $('.secondary-fp').remove();

                                            var str = '<div class="span6 secondary-fp"><div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" style="text-align:left">Upload Faktur Diganti<span class="required"> *</span></label>'+
                                                '<div class="controls" style="padding-left:0px">' +
                                                '<div class="dropzone dropzone-previews" id="my-awesome-dropzone2" style="width: 200px; height: 215px"></div>'+
                                                '</div>'+
                                                '</div></div>';

                                            $(str).insertAfter("#first_faktur");

                                            var nextFp = '<br class="secondary-fp"><br class="secondary-fp"><h4 class="form-section secondary-fp">Faktur Pajak Diganti</h4>'+
                                                '<div class="custom-form fp-only secondary-fp">';

                                            nextFp += '<div class="row-fluid">' +
                                                '<div class="span12">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label">URL Faktur Pajak Diganti<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<div class="input-append span12">'+
                                                '<input type="text" id="faktur_pajak_url_2" name="faktur_pajak_url_2" value="" class="m-wrap" readonly style="width: 88.2%">' +
                                                '<button type="button" class="btn yellow" id="detail_transaksi_2">Detail Transaksi</button>' +
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>' +
                                                '</div>';

                                            nextFp +=   '<div class="row-fluid">' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_status_2">Status Faktur<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_status_2" name="faktur_pajak_status_2" class="m-wrap span12" readonly>'+
                                                '                                                    </div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_approval_2">Status Approval<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_approval_2" name="faktur_pajak_approval_2" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="pajak_type_2">Jenis Pajak<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="pajak_type_2" name="pajak_type_2" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>';
                                            nextFp += '<div class="row-fluid">' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_number_2">No. Faktur Pajak<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_number_2" name="faktur_pajak_number_2" class="m-wrap span12" readonly>'+
                                                '<input type="hidden" id="uploaded_fp_filepath_2" name="uploaded_fp_filepath_2" class="m-wrap notempty large" readonly/>'+
                                                '<input type="hidden" id="uploaded_fp_filename_2" name="uploaded_fp_filename_2" class="m-wrap large" readonly/>' +
                                                '<input type="hidden" id="uploaded_fp_filesize_2" name="uploaded_fp_filesize_2" class="m-wrap large" readonly/>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_date_2">Tanggal Faktur Pajak<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_date_2" name="faktur_pajak_date_2" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>';

                                            nextFp +=   '<div class="row-fluid">' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="dpp_amount_2">Jumlah DPP<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="dpp_amount_2" name="dpp_amount_2" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="fp_amount_2">Jumlah PPN<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="fp_amount_2" name="fp_amount_2" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="ppnbm_amount_2">Jumlah PPnBM<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="ppnbm_amount_2" name="ppnbm_amount_2" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>';

                                            nextFp +='</div>';

                                            $(nextFp).insertAfter(".custom-form");

                                            $("#form_save").validate();
                                            $('input[name="faktur_pajak_url_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="faktur_pajak_status_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });


                                            $('input[name="faktur_pajak_approval_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="pajak_type_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="faktur_pajak_number_2"]').rules("add",{
                                                required : true,
                                                equalFaktur: true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                    equalWithFaktur : "Nomor Faktur Pajak Tidak Sama"
                                                }
                                            });

                                            $('input[name="faktur_pajak_date_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="dpp_amount_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="fp_amount_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="ppnbm_amount_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            jQuery("#faktur_pajak_number_2").inputmask({
                                                "mask": "999.999-99.99999999",
                                            });

                                            jQuery('#fp_amount_2').inputmask('decimal',{
                                                groupSeparator: ".",
                                                radixPoint: ",",
                                                digits: 0,
                                                autoGroup: true,
                                            });

                                            jQuery('#dpp_amount_2').inputmask('decimal',{
                                                groupSeparator: ".",
                                                radixPoint: ",",
                                                digits: 0,
                                                autoGroup: true,
                                            });

                                            jQuery('#ppnbm_amount_2').inputmask('decimal',{
                                                groupSeparator: ".",
                                                radixPoint: ",",
                                                digits: 0,
                                                autoGroup: true,
                                            });

                                            myDropzone = new Dropzone("div#my-awesome-dropzone2",
                                                {
                                                    addRemoveLinks: true,
                                                    url: '<?php echo app_backend_url('main/berkas/upload_faktur_pajak'); ?>',
                                                    paramName: "faktur_pajak", // The name that will be used to transfer the file
                                                    maxFilesize: 5, // MB
                                                    acceptedFiles : 'application/pdf,.pdf',
                                                    createImageThumbnails: false,
                                                    maxFiles : 1,
                                                    dictDefaultMessage: "Drop file/klik disini untuk upload",
                                                    dictRemoveFileConfirmation: "Anda yakin ingin menghapus dokumen Faktur Pajak ini?",
                                                    dictRemoveFile: "Hapus File",
                                                    timeout : 120000,
                                                }
                                            );

                                            myDropzone.on('success', function (file, res) {
                                                $('#progress-modal').modal('hide');
                                                $(file.previewElement).on('click', function () {
                                                    var crtUrl = "<?php echo app_asset_url('tmp/')?>"+res.uploaded.filepath;
                                                    window.open(crtUrl, "_blank");
                                                });

                                                $('#uploaded_fp_filepath_2').val(res.uploaded.filepath);
                                                $('#uploaded_fp_filename_2').val(res.uploaded.filename);
                                                $('#uploaded_fp_filesize_2').val(res.uploaded.filesize);

                                                if(typeof res.data != 'undefined'){

                                                    //todo NSFP harus sama
                                                    var first_nsfp = $('#faktur_pajak_number').inputmask("unmaskedvalue");
                                                    var firstFp = first_nsfp.substr(3);
                                                    var second_nsfp = res.data.nomorFaktur;


                                                    if(firstFp == second_nsfp){
                                                        var statusFaktur = res.data.statusFaktur;
                                                        switch (statusFaktur) {
                                                            case 'Faktur Diganti':
                                                                var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                                                var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                                                $('#faktur_pajak_url_2').val(res.url);
                                                                $('#faktur_pajak_number_2').val(fpNumber);
                                                                $('#faktur_pajak_status_2').val(res.data.statusFaktur);
                                                                $('#faktur_pajak_approval_2').val(res.data.statusApproval);
                                                                $('#faktur_pajak_date_2').val(fpDate);

                                                                $('#fp_amount_2').val(res.data.jumlahPpn);
                                                                $('#dpp_amount_2').val(res.data.jumlahDpp);
                                                                $('#ppnbm_amount_2').val(res.data.jumlahPpnBm);

                                                                setPajakType2();
                                                                setPajakPeriodValGanti();
                                                                break;
                                                            default :
                                                                alert("Status FP yang diupload adalah '"+res.data.statusFaktur+"', harap upload Faktur Diganti. Data ini akan dihapus, silahkan upload ulang.");
                                                                this.removeAllFiles();
                                                        }
                                                    }else{
                                                        alert("NSFP berbeda dengan faktur pajak sebelumnya. Dokumen akan dihapus, harap upload ulang.");
                                                        this.removeAllFiles();
                                                    }
                                                }
                                            });

                                            myDropzone.on("removedfile", function(file) {

                                                if(this.files.length == 0){
                                                    this.options.maxFiles = 1;

                                                    if(typeof $('#uploaded_fp_filepath_2').val() != 'undefined'){

                                                        var cAjax = new ceki.fnAjax({
                                                            url : '<?php echo app_backend_url('main/berkas/delete_faktur_pajak'); ?>',
                                                            data : ({
                                                                path: $('#uploaded_fp_filepath_2').val()
                                                            }),
                                                            successCallBack : function(resp) {
                                                                jQuery.gritter.add({
                                                                    title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                                                    text: resp.message,
                                                                    sticky: false,
                                                                    time: 3000
                                                                });
                                                            }
                                                        });

                                                        $("#pajak_period").val("");
                                                        $("#pajak_year").val("");

                                                        $('#uploaded_fp_filepath_2').val("");
                                                        $('#uploaded_fp_filename_2').val("");
                                                        $('#uploaded_fp_filesize_2').val("");

                                                    }

                                                    $('#barcode_scanner_2').remove();
                                                }
                                            });

                                            myDropzone.on("maxfilesexceeded", function(file){
                                                alert("Upload hanya boleh 1 file");
                                                this.removeFile(file);
                                            });

                                            myDropzone.on("sending", function handleTimeout(file, xhr, formData) {
                                                xhr.ontimeout = function (e) {
                                                    alert('Upload Dibatalkan karena proses terlalu lama');
                                                    this.removeFile(file);
                                                    $('#progress-modal').modal('hide');
                                                };
                                            });

                                            myDropzone.on("uploadprogress", function(file, progress, bytesSent){
                                                if(progress > 99){
                                                    $('#progress-modal').modal('show');
                                                }
                                            });

                                            myDropzone.on("error", function(file, res) {
                                                $('#progress-modal').modal('hide');
                                                if(typeof res.uploaded!= 'undefined'){
                                                    $('#uploaded_fp_filepath_2').val(res.uploaded.filepath);
                                                    $('#uploaded_fp_filename_2').val(res.uploaded.filename);
                                                    $('#uploaded_fp_filesize_2').val(res.uploaded.filesize);

                                                    var btnScanner = '<button type="button" class="btn yellow" id="barcode_scanner_2">&nbsp;Gunakan Barcode Scanner&nbsp;&nbsp;</button>';
                                                    $(btnScanner).insertAfter('#my-awesome-dropzone2');
                                                }
                                            });

                                            break;

                                        case 'Faktur Diganti':
                                            $('#faktur_pajak_url').val("");
                                            $('#faktur_pajak_number').val("");
                                            $('#faktur_pajak_approval').val("");
                                            $('#faktur_pajak_status').val("");
                                            $('#faktur_pajak_date').val("");
                                            $('#fp_amount').val("");
                                            $('#dpp_amount').val("");
                                            $('#ppnbm_amount').val("");
                                            $('#pajak_type').val("");

                                            // $('#vendor_npwp').val("");
                                            $('#nama_penjual').val("");
                                            $('#alamat_penjual').val("");
                                            $('#npwp_penjual').val("");

                                            $('#npwp_pembeli').val("");
                                            $('#nama_pembeli').val("");
                                            $('#alamat_pembeli').val("");

                                            var filePath2 = "";
                                            var fileName2 = "";
                                            var fileSize2 = "";

                                            if($('#uploaded_fp_filepath_2')){
                                                var filePath2 = $('#uploaded_fp_filepath_2').val();
                                                var fileName2 = $('#uploaded_fp_filename_2').val();
                                                var fileSize2 = $('#uploaded_fp_filesize_2').val();

                                            }

                                            $('.secondary-fp').remove();

                                            var fpNumber = obj.data.kdJenisTransaksi + obj.data.fgPengganti + obj.data.nomorFaktur;
                                            var fpDate =  obj.data.tanggalFaktur.split('/').join("-");
                                            var str = '<div class="span6 secondary-fp"><div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" style="text-align:left">Upload Faktur Normal-Pengganti<span class="required"> *</span></label>'+
                                                '<div class="controls" style="padding-left:70px">' +
                                                '<div class="dropzone dropzone-previews" id="my-awesome-dropzone2" style="width: 200px; height: 215px"></div>'+
                                                '</div>'+
                                                '</div></div>';

                                            $(str).insertAfter("#first_faktur");

                                            var nextFp = '<br class="secondary-fp"><br class="secondary-fp"><h4 class="form-section secondary-fp">Faktur Pajak Diganti</h4>'+
                                                '<div class="custom-form fp-only secondary-fp">';

                                            var gantiUrl = $('#barcode_faktur_url').val();

                                            nextFp += '<div class="row-fluid">' +
                                                '<div class="span12">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label">URL Faktur Pajak Diganti<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<div class="input-append span12">'+
                                                '<input type="text" id="faktur_pajak_url_2" name="faktur_pajak_url_2" value="'+gantiUrl+'" class="m-wrap" readonly style="width: 88.2%">' +
                                                '<button type="button" class="btn yellow" id="detail_transaksi_2">Detail Transaksi</button>' +
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>' +
                                                '</div>';

                                            nextFp +=   '<div class="row-fluid">' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_status_2">Status Faktur<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_status_2" name="faktur_pajak_status_2" value="'+obj.data.statusFaktur+'" class="m-wrap span12" readonly>'+
                                                '                                                    </div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_approval_2">Status Approval<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_approval_2" name="faktur_pajak_approval_2" value="'+obj.data.statusApproval+'" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="pajak_type_2">Jenis Pajak<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="pajak_type_2" name="pajak_type_2" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>';



                                            if($('#uploaded_fp_filepath').val()){
                                                var filePath2 = $('#uploaded_fp_filepath').val();
                                                var fileName2 = $('#uploaded_fp_filename').val();
                                                var fileSize2 = $('#uploaded_fp_filesize').val();

                                                $('#uploaded_fp_filepath').val("");
                                                $('#uploaded_fp_filename').val("");
                                                $('#uploaded_fp_filesize').val("");
                                            }

                                            nextFp += '<div class="row-fluid">' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_number_2">No. Faktur Pajak<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_number_2" name="faktur_pajak_number_2" class="m-wrap span12" value="'+fpNumber+'" readonly>'+
                                                '<input type="hidden" id="uploaded_fp_filepath_2" name="uploaded_fp_filepath_2" class="m-wrap notempty large" value="'+filePath2+'" readonly/>'+
                                                '<input type="hidden" id="uploaded_fp_filename_2" name="uploaded_fp_filename_2" class="m-wrap large" value="'+fileName2+'" readonly/>' +
                                                '<input type="hidden" id="uploaded_fp_filesize_2" name="uploaded_fp_filesize_2" class="m-wrap large" value="'+fileSize2+'" readonly/>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_date_2">Tanggal Faktur Pajak<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_date_2" name="faktur_pajak_date_2" class="m-wrap span12" value="'+fpDate+'"readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>';

                                            nextFp +=   '<div class="row-fluid">' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="dpp_amount_2">Jumlah DPP<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="dpp_amount_2" name="dpp_amount_2" class="m-wrap span12" value="'+obj.data.jumlahDpp+'" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="fp_amount_2">Jumlah PPN<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="fp_amount_2" name="fp_amount_2" class="m-wrap span12" value="'+obj.data.jumlahPpn+'" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="ppnbm_amount_2">Jumlah PPnBM<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="ppnbm_amount_2" name="ppnbm_amount_2" class="m-wrap span12" value="'+obj.data.jumlahPpnBm+'" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>';

                                            nextFp +='</div>';

                                            $(nextFp).insertAfter(".custom-form");
                                            setPajakType2();
                                            setPajakPeriodValGanti();

                                            $("#form_save").validate();
                                            $('input[name="faktur_pajak_url_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="faktur_pajak_status_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="faktur_pajak_approval_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="pajak_type_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="faktur_pajak_number_2"]').rules("add",{
                                                required : true,
                                                equalFaktur: true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                    equalWithFaktur : "Nomor Faktur Pajak Tidak Sama"
                                                }
                                            });

                                            $('input[name="faktur_pajak_date_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="dpp_amount_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="fp_amount_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="ppnbm_amount_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            jQuery("#faktur_pajak_number_2").inputmask({
                                                "mask": "999.999-99.99999999",
                                            });

                                            jQuery('#fp_amount_2').inputmask('decimal',{
                                                groupSeparator: ".",
                                                radixPoint: ",",
                                                digits: 0,
                                                autoGroup: true,
                                            });

                                            jQuery('#dpp_amount_2').inputmask('decimal',{
                                                groupSeparator: ".",
                                                radixPoint: ",",
                                                digits: 0,
                                                autoGroup: true,
                                            });

                                            jQuery('#ppnbm_amount_2').inputmask('decimal',{
                                                groupSeparator: ".",
                                                radixPoint: ",",
                                                digits: 0,
                                                autoGroup: true,
                                            });


                                            myDropzone = new Dropzone("div#my-awesome-dropzone2",
                                                {
                                                    addRemoveLinks: true,
                                                    url: '<?php echo app_backend_url('main/berkas/upload_faktur_pajak'); ?>',
                                                    paramName: "faktur_pajak", // The name that will be used to transfer the file
                                                    maxFilesize: 5, // MB
                                                    acceptedFiles : 'application/pdf,.pdf',
                                                    createImageThumbnails: false,
                                                    maxFiles : 1,
                                                    dictDefaultMessage: "Drop file/klik disini untuk upload",
                                                    dictRemoveFileConfirmation: "Anda yakin ingin menghapus dokumen Faktur Pajak ini?",
                                                    dictRemoveFile: "Hapus File",
                                                    timeout : 120000,
                                                }
                                            );

                                            myDropzone.on('success', function (file, res) {
                                                $('#progress-modal').modal('hide');
                                                $(file.previewElement).on('click', function () {
                                                    var crtUrl = "<?php echo app_asset_url('tmp/')?>"+res.uploaded.filepath;
                                                    window.open(crtUrl, "_blank");
                                                });

                                                $('#uploaded_fp_filepath').val(res.uploaded.filepath);
                                                $('#uploaded_fp_filename').val(res.uploaded.filename);
                                                $('#uploaded_fp_filesize').val(res.uploaded.filesize);

                                                var statusFaktur = res.data.statusFaktur;
                                                switch (statusFaktur) {
                                                    case 'Faktur Pajak Normal-Pengganti':
                                                        var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                                        var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                                        $('#faktur_pajak_url').val(res.url);
                                                        $('#faktur_pajak_number').val(fpNumber);
                                                        $('#faktur_pajak_status').val(res.data.statusFaktur);
                                                        $('#faktur_pajak_approval').val(res.data.statusApproval);
                                                        $('#faktur_pajak_date').val(fpDate);
                                                        $('#fp_amount').val(res.data.jumlahPpn);
                                                        $('#dpp_amount').val(res.data.jumlahDpp);
                                                        $('#ppnbm_amount').val(res.data.jumlahPpnBm);

                                                        $('#vendor_npwp').val(res.data.npwpPenjual);
                                                        $('#vendor_name').val(res.data.namaPenjual);
                                                        $('#vendor_address').val(res.data.alamatPenjual);

                                                        $('#npwp_pembeli').val(res.data.npwpLawanTransaksi);
                                                        $('#nama_pembeli').val(res.data.namaLawanTransaksi);
                                                        $('#alamat_pembeli').val(res.data.alamatLawanTransaksi);

                                                        $('#npwp_penjual').val(res.data.npwpPenjual);
                                                        $('#nama_penjual').val(res.data.namaPenjual);
                                                        $('#alamat_penjual').val(res.data.alamatPenjual);

                                                        setPajakType();

                                                        break;
                                                    default:
                                                        alert("Faktur Pajak yang harus diupload adalah Faktur Pajak Normal-Pengganti");
                                                        this.removeAllFiles();
                                                }

                                            });

                                            myDropzone.on("removedfile", function(file) {

                                                if(this.files.length == 0){
                                                    this.options.maxFiles = 1

                                                    var cAjax = new ceki.fnAjax({
                                                        url : '<?php echo app_backend_url('main/berkas/delete_faktur_pajak'); ?>',
                                                        data : ({
                                                            path: $('#uploaded_fp_filepath').val()
                                                        }),
                                                        successCallBack : function(resp) {
                                                            jQuery.gritter.add({
                                                                title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                                                text: resp.message,
                                                                sticky: false,
                                                                time: 3000
                                                            });
                                                        }
                                                    });

                                                    //clear hidden input value uploaded_fp
                                                    $("#uploaded_fp_filepath").val("");
                                                    $("#uploaded_fp_filename").val("");
                                                    $("#uploaded_fp_filesize").val("");
                                                    $('#faktur_pajak_url').val("");
                                                    $("#faktur_pajak_approval").val("");

                                                    var fakturStatus = $('#faktur_pajak_status').val();
                                                    if(fakturStatus){
                                                        if(fakturStatus == 'Faktur Pajak Normal-Pengganti' || fakturStatus == 'Faktur Pajak Normal'){
                                                            $("#faktur_pajak_number").val("");
                                                            $("#faktur_pajak_date").val("");
                                                            $("#pajak_type").val("");

                                                            $("#fp_amount").val("");
                                                            $("#dpp_amount").val("");
                                                            $("#ppnbm_amount").val("");

                                                            $("#npwp_pembeli").val("");
                                                            $("#nama_pembeli").val("");
                                                            $("#alamat_pembeli").val("");

                                                            $('#npwp_penjual').val('');
                                                            $('#nama_penjual').val("");
                                                            $('#alamat_penjual').val("");

                                                        }

                                                        if(fakturStatus == 'Faktur Pajak Normal'){
                                                            $("#pajak_period").val("");
                                                            $("#pajak_year").val("");
                                                        }
                                                    }

                                                    $("#faktur_pajak_status").val("");
                                                    $('#barcode_scanner_2').remove();

                                                }
                                            });

                                            myDropzone.on("maxfilesexceeded", function(file){
                                                alert("Upload hanya boleh 1 file");
                                                this.removeFile(file);
                                            });

                                            myDropzone.on("sending", function handleTimeout(file, xhr, formData) {
                                                xhr.ontimeout = function (e) {
                                                    alert('Upload Dibatalkan karena proses terlalu lama');
                                                    this.removeFile(file);
                                                    $('#progress-modal').modal('hide');
                                                };
                                            });

                                            myDropzone.on("uploadprogress", function(file, progress, bytesSent){
                                                console.log(progress);
                                                if(progress > 99){
                                                    $('#progress-modal').modal('show');
                                                }
                                            });

                                            myDropzone.on("error", function(file, res) {
                                                $('#progress-modal').modal('hide');
                                                if(typeof res.uploaded!= 'undefined'){
                                                    $('#uploaded_fp_filepath').val(res.uploaded.filepath);
                                                    $('#uploaded_fp_filename').val(res.uploaded.filename);
                                                    $('#uploaded_fp_filesize').val(res.uploaded.filesize);

                                                    var btnScanner = '<button type="button" class="btn yellow" id="barcode_scanner_2">&nbsp;Gunakan Barcode Scanner&nbsp;&nbsp;</button>';
                                                    $(btnScanner).insertAfter('#my-awesome-dropzone2');
                                                }
                                            });

                                            jQuery("#faktur_pajak_number_2").inputmask({
                                                "mask": "999.999-99.99999999",
                                            });
                                            break;

                                        case 'Faktur Dibatalkan' :
                                            $('#faktur_pajak_url').val("");
                                            $('#faktur_pajak_number').val("");
                                            $('#faktur_pajak_approval').val("");
                                            $('#faktur_pajak_status').val("");
                                            $('#faktur_pajak_date').val("");
                                            $('#fp_amount').val("");
                                            $('#dpp_amount').val("");
                                            $('#ppnbm_amount').val("");
                                            $('#pajak_type').val("");

                                            $('#npwp_penjual').val("");
                                            $('#nama_penjual').val("");
                                            $('#alamat_penjual').val("");

                                            $('#npwp_pembeli').val("");
                                            $('#nama_pembeli').val("");
                                            $('#alamat_pembeli').val("");

                                            var fpNumber = obj.data.kdJenisTransaksi + obj.data.fgPengganti + obj.data.nomorFaktur;
                                            var fpDate =  obj.data.tanggalFaktur.split('/').join("-");
                                            var str = '<div class="span6 secondary-fp"><div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" style="text-align:left">Upload Faktur Normal/Normal-Pengganti<span class="required"> *</span></label>'+
                                                '<div class="controls" style="padding-left:70px">' +
                                                '<div class="dropzone dropzone-previews" id="my-awesome-dropzone2" style="width: 200px; height: 215px"></div>'+
                                                '</div>'+
                                                '</div></div>';

                                            $(str).insertAfter("#first_faktur");

                                            var nextFp = '<br class="secondary-fp"><br class="secondary-fp"><h4 class="form-section secondary-fp">Faktur Pajak Dibatalkan</h4>'+
                                                '<div class="custom-form fp-only secondary-fp">';

                                            var batalUrl = $('#barcode_faktur_url').val();

                                            nextFp += '<div class="row-fluid">' +
                                                '<div class="span12">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label">URL Faktur Pajak Dibatalkan<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<div class="input-append span12">'+
                                                '<input type="text" id="faktur_pajak_url_2" name="faktur_pajak_url_2" value="'+batalUrl+'" class="m-wrap" readonly style="width: 88.2%">' +
                                                '<button type="button" class="btn yellow" id="detail_transaksi_2">Detail Transaksi</button>' +
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>' +
                                                '</div>';

                                            nextFp +=   '<div class="row-fluid">' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_status_2">Status Faktur<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_status_2" name="faktur_pajak_status_2" value="'+res.data.statusFaktur+'" class="m-wrap span12" readonly>'+
                                                '                                                    </div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_approval_2">Status Approval<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_approval_2" name="faktur_pajak_approval_2" value="'+res.data.statusApproval+'" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="pajak_type_2">Jenis Pajak<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="pajak_type_2" name="pajak_type_2" class="m-wrap span12" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>';
                                            nextFp += '<div class="row-fluid">' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_number_2">No. Faktur Pajak<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_number_2" name="faktur_pajak_number_2" class="m-wrap span12" value="'+fpNumber+'" readonly>'+
                                                '<input type="hidden" id="uploaded_fp_filepath_2" name="uploaded_fp_filepath_2" class="m-wrap notempty large" value="'+res.uploaded.filepath+'" readonly/>'+
                                                '<input type="hidden" id="uploaded_fp_filename_2" name="uploaded_fp_filename_2" class="m-wrap large" value="'+res.uploaded.filename+'" readonly/>' +
                                                '<input type="hidden" id="uploaded_fp_filesize_2" name="uploaded_fp_filesize_2" class="m-wrap large" value="'+res.uploaded.filesize+'" readonly/>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="faktur_pajak_date_2">Tanggal Faktur Pajak<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="faktur_pajak_date_2" name="faktur_pajak_date_2" class="m-wrap span12" value="'+fpDate+'"readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>';

                                            nextFp +=   '<div class="row-fluid">' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="dpp_amount_2">Jumlah DPP<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="dpp_amount_2" name="dpp_amount_2" class="m-wrap span12" value="'+res.data.jumlahDpp+'" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="fp_amount_2">Jumlah PPN<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="fp_amount_2" name="fp_amount_2" class="m-wrap span12" value="'+res.data.jumlahPpn+'" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '<div class="span4">' +
                                                '<div class="control-group fp-only secondary-fp">' +
                                                '<label class="control-label" for="ppnbm_amount_2">Jumlah PPnBM<span class="required"> *</span></label>' +
                                                '<div class="controls">' +
                                                '<input type="text" id="ppnbm_amount_2" name="ppnbm_amount_2" class="m-wrap span12" value="'+res.data.jumlahPpnBm+'" readonly>'+
                                                '</div>'+
                                                '</div>' +
                                                '</div>' +
                                                '</div>';

                                            nextFp +='</div>';

                                            $(nextFp).insertAfter(".custom-form");
                                            setPajakType2();

                                            $("#form_save").validate();
                                            $('input[name="faktur_pajak_url_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="faktur_pajak_status_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });


                                            $('input[name="faktur_pajak_approval_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="pajak_type_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="faktur_pajak_number_2"]').rules("add",{
                                                required : true,
                                                equalFaktur: true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                    equalWithFaktur : "Nomor Faktur Pajak Tidak Sama"
                                                }
                                            });

                                            $('input[name="faktur_pajak_date_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="dpp_amount_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="fp_amount_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            $('input[name="ppnbm_amount_2"]').rules("add",{
                                                required : true,
                                                messages : {
                                                    required: "<?php echo $this->lang->line('global_error_required_field2') ?>",
                                                }
                                            });

                                            jQuery("#faktur_pajak_number_2").inputmask({
                                                "mask": "999.999-99.99999999",
                                            });

                                            jQuery('#fp_amount_2').inputmask('decimal',{
                                                groupSeparator: ".",
                                                radixPoint: ",",
                                                digits: 0,
                                                autoGroup: true,
                                            });

                                            jQuery('#dpp_amount_2').inputmask('decimal',{
                                                groupSeparator: ".",
                                                radixPoint: ",",
                                                digits: 0,
                                                autoGroup: true,
                                            });

                                            jQuery('#ppnbm_amount_2').inputmask('decimal',{
                                                groupSeparator: ".",
                                                radixPoint: ",",
                                                digits: 0,
                                                autoGroup: true,
                                            });


                                            myDropzone = new Dropzone("div#my-awesome-dropzone2",
                                                {
                                                    addRemoveLinks: true,
                                                    url: '<?php echo app_backend_url('main/berkas/upload_faktur_pajak'); ?>',
                                                    paramName: "faktur_pajak", // The name that will be used to transfer the file
                                                    maxFilesize: 5, // MB
                                                    acceptedFiles : 'application/pdf,.pdf',
                                                    createImageThumbnails: false,
                                                    maxFiles : 1,
                                                    dictDefaultMessage: "Drop file/klik disini untuk upload",
                                                    dictRemoveFileConfirmation: "Anda yakin ingin menghapus dokumen Faktur Pajak ini?",
                                                    dictRemoveFile: "Hapus File",
                                                    timeout : 120000,
                                                }
                                            );

                                            myDropzone.on('success', function (file, res) {
                                                $('#progress-modal').modal('hide');
                                                $(file.previewElement).on('click', function () {
                                                    var crtUrl = "<?php echo app_asset_url('tmp/')?>"+res.uploaded.filepath;
                                                    window.open(crtUrl, "_blank");
                                                });

                                                $('#uploaded_fp_filepath').val(res.uploaded.filepath);
                                                $('#uploaded_fp_filename').val(res.uploaded.filename);
                                                $('#uploaded_fp_filesize').val(res.uploaded.filesize);

                                                var statusFaktur = res.data.statusFaktur;
                                                switch (statusFaktur) {
                                                    case 'Faktur Pajak Normal-Pengganti':
                                                        var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                                        var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                                        $('#faktur_pajak_url').val(res.url);
                                                        $('#faktur_pajak_number').val(fpNumber);
                                                        $('#faktur_pajak_status').val(res.data.statusFaktur);
                                                        $('#faktur_pajak_approval').val(res.data.statusApproval);
                                                        $('#faktur_pajak_date').val(fpDate);
                                                        $('#fp_amount').val(res.data.jumlahPpn);
                                                        $('#dpp_amount').val(res.data.jumlahDpp);
                                                        $('#ppnbm_amount').val(res.data.jumlahPpnBm);

                                                        $('#vendor_npwp').val(res.data.npwpPenjual);
                                                        $('#vendor_name').val(res.data.namaPenjual);
                                                        $('#vendor_address').val(res.data.alamatPenjual);

                                                        $('#npwp_pembeli').val(res.data.npwpLawanTransaksi);
                                                        $('#nama_pembeli').val(res.data.namaLawanTransaksi);
                                                        $('#alamat_pembeli').val(res.data.alamatLawanTransaksi);

                                                        $('#npwp_penjual').val(res.data.npwpPenjual);
                                                        $('#nama_penjual').val(res.data.namaPenjual);
                                                        $('#alamat_penjual').val(res.data.alamatPenjual);

                                                        setPajakType();
                                                        setPajakPeriodVal();
                                                        break;
                                                    case 'Faktur Pajak Normal':
                                                        var fpNumber = res.data.kdJenisTransaksi + res.data.fgPengganti + res.data.nomorFaktur;
                                                        var fpDate =  res.data.tanggalFaktur.split('/').join("-");
                                                        $('#faktur_pajak_url').val(res.url);
                                                        $('#faktur_pajak_number').val(fpNumber);
                                                        $('#faktur_pajak_status').val(res.data.statusFaktur);
                                                        $('#faktur_pajak_approval').val(res.data.statusApproval);
                                                        $('#faktur_pajak_date').val(fpDate);
                                                        $('#fp_amount').val(res.data.jumlahPpn);
                                                        $('#dpp_amount').val(res.data.jumlahDpp);
                                                        $('#ppnbm_amount').val(res.data.jumlahPpnBm);

                                                        $('#vendor_npwp').val(res.data.npwpPenjual);
                                                        $('#vendor_name').val(res.data.namaPenjual);
                                                        $('#vendor_address').val(res.data.alamatPenjual);

                                                        $('#npwp_pembeli').val(res.data.npwpLawanTransaksi);
                                                        $('#nama_pembeli').val(res.data.namaLawanTransaksi);
                                                        $('#alamat_pembeli').val(res.data.alamatLawanTransaksi);

                                                        $('#npwp_penjual').val(res.data.npwpPenjual);
                                                        $('#nama_penjual').val(res.data.namaPenjual);
                                                        $('#alamat_penjual').val(res.data.alamatPenjual);

                                                        setPajakType();
                                                        setPajakPeriodVal();
                                                        break;

                                                    default:
                                                        alert("Faktur Pajak yang harus diupload adalah Faktur Pajak Normal atau Normal-Pengganti");
                                                        this.removeAllFiles();
                                                }

                                            });

                                            myDropzone.on("removedfile", function(file) {

                                                if(this.files.length == 0){
                                                    this.options.maxFiles = 1

                                                    var cAjax = new ceki.fnAjax({
                                                        url : '<?php echo app_backend_url('main/berkas/delete_faktur_pajak'); ?>',
                                                        data : ({
                                                            path: $('#uploaded_fp_filepath').val()
                                                        }),
                                                        successCallBack : function(resp) {
                                                            jQuery.gritter.add({
                                                                title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                                                text: resp.message,
                                                                sticky: false,
                                                                time: 3000
                                                            });
                                                        }
                                                    });

                                                    //clear hidden input value uploaded_fp
                                                    $("#uploaded_fp_filepath").val("");
                                                    $("#uploaded_fp_filename").val("");
                                                    $("#uploaded_fp_filesize").val("");
                                                    $('#faktur_pajak_url').val("");
                                                    $("#faktur_pajak_approval").val("");

                                                    var fakturStatus = $('#faktur_pajak_status').val();
                                                    if(fakturStatus){
                                                        if(fakturStatus == 'Faktur Pajak Normal-Pengganti' || fakturStatus == 'Faktur Pajak Normal'){
                                                            $("#faktur_pajak_number").val("");
                                                            $("#faktur_pajak_date").val("");
                                                            $("#pajak_type").val("");

                                                            $("#fp_amount").val("");
                                                            $("#dpp_amount").val("");
                                                            $("#ppnbm_amount").val("");

                                                            $("#npwp_pembeli").val("");
                                                            $("#nama_pembeli").val("");
                                                            $("#alamat_pembeli").val("");

                                                            $('#npwp_penjual').val('');
                                                            $('#nama_penjual').val("");
                                                            $('#alamat_penjual').val("");

                                                        }

                                                        if(fakturStatus == 'Faktur Pajak Normal'){
                                                            $("#pajak_period").val("");
                                                            $("#pajak_year").val("");
                                                        }
                                                    }

                                                    $("#faktur_pajak_status").val("");

                                                }
                                            });

                                            myDropzone.on("maxfilesexceeded", function(file){
                                                alert("Upload hanya boleh 1 file");
                                                this.removeFile(file);
                                            });

                                            myDropzone.on("sending", function handleTimeout(file, xhr, formData) {
                                                xhr.ontimeout = function (e) {
                                                    alert('Upload Dibatalkan karena proses terlalu lama');
                                                    this.removeFile(file);
                                                    $('#progress-modal').modal('hide');
                                                };
                                            });

                                            myDropzone.on("uploadprogress", function(file, progress, bytesSent){
                                                console.log(progress);
                                                if(progress > 99){
                                                    $('#progress-modal').modal('show');
                                                }
                                            });

                                            myDropzone.on("error", function(file, res) {
                                                $('#progress-modal').modal('hide');
                                                if(typeof res.uploaded!= 'undefined'){
                                                    $('#uploaded_fp_filepath_2').val(res.uploaded.filepath);
                                                    $('#uploaded_fp_filename_2').val(res.uploaded.filename);
                                                    $('#uploaded_fp_filesize_2').val(res.uploaded.filesize);
                                                }
                                            });

                                            jQuery("#faktur_pajak_number_2").inputmask({
                                                "mask": "999.999-99.99999999",
                                            });
                                            break;
                                        default:

                                    }
                                }

                            } else {
                                App.unblockUI(jQuery("#qrcode_modal"));
                                $('#qrcode_modal').modal('hide');
                                jQuery.gritter.add({
                                    title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                    text: "Gagal Mengambil Data",
                                    sticky: false,
                                    time: 3000
                                });
                            }
                        }
                    });
                },1000);
            }
            e.preventDefault();
        });

        $('#barcode_scanner').live('click', function(){
            if(typeof myDropzone != 'undefined' ){
                if(myDropzone.files.length > 0 && $('#my-awesome-dropzone2').length){
                    alert("Harap hapus file upload yang ke-2 terlebih dahulu");
                    return;
                }
            }
            $('#qrcode_modal').modal('show');
        });

        $('#qrcode_modal').on('shown', function() {
            App.unblockUI(jQuery("#qrcode_modal"));
            $("#barcode_faktur_url").val("");
            $("#barcode_faktur_url").focus();
        })


        ////##########////
        jQuery('#qrcode-form-2').submit(function(e) {
            if(jQuery('#barcode_faktur_url_2').val() != '' && $("#qrcode_modal_2").data('blockUI.isBlocked') != 1){
                App.blockUI(jQuery("#qrcode_modal_2"), true, '<?php echo $this->lang->line('global_please_wait'); ?>');
                setTimeout(function(){
                    jQuery.ajax({
                        url: '<?php echo app_backend_url('main/berkas/get_scanned_url'); ?>',
                        type: 'POST',
                        data: {
                            barcode_faktur_url: jQuery('#barcode_faktur_url_2').val()
                        },
                        dataType: 'JSON',
                        success: function (obj) {
                            if(obj.success == true) {
                                App.unblockUI(jQuery("#qrcode_modal_2"));
                                jQuery.gritter.add({
                                    title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                    text: obj.message,
                                    sticky: false,
                                    time: 3000
                                });
                                console.log(obj.data);

                                $("#qrcode_modal_2").modal('hide');

                                if(typeof obj.data != 'undefined'){

                                    if($('#faktur_pajak_url').val()){
                                        var first_nsfp = $('#faktur_pajak_number').inputmask("unmaskedvalue");
                                        var firstFp = first_nsfp.substr(3);
                                        var second_nsfp = obj.data.nomorFaktur;

                                        if(firstFp == second_nsfp){
                                            var statusFaktur = obj.data.statusFaktur;
                                            switch (statusFaktur){
                                                case 'Faktur Diganti':
                                                    var fpNumber = obj.data.kdJenisTransaksi + obj.data.fgPengganti + obj.data.nomorFaktur;
                                                    var fpDate =  obj.data.tanggalFaktur.split('/').join("-");
                                                    $('#faktur_pajak_url_2').val($("#barcode_faktur_url_2").val());
                                                    $('#faktur_pajak_number_2').val(fpNumber);
                                                    $('#faktur_pajak_approval_2').val(obj.data.statusApproval);
                                                    $('#faktur_pajak_status_2').val(obj.data.statusFaktur);
                                                    $('#faktur_pajak_date_2').val(fpDate);
                                                    $('#fp_amount_2').val(obj.data.jumlahPpn);
                                                    $('#dpp_amount_2').val(obj.data.jumlahDpp);
                                                    $('#ppnbm_amount_2').val(obj.data.jumlahPpnBm);
                                                    setPajakType2();
                                                    setPajakPeriodValGanti();
                                                    break;
                                                default:
                                                    alert("Status FP yang discan adalah '"+obj.data.statusFaktur+"', harap scan Faktur Diganti");
                                                    $('#faktur_pajak_url_2').val("");
                                                    $('#faktur_pajak_number_2').val("");
                                                    $('#faktur_pajak_approval_2').val("");
                                                    $('#faktur_pajak_status_2').val("");
                                                    $('#faktur_pajak_date_2').val("");
                                                    $('#fp_amount_2').val("");
                                                    $('#dpp_amount_2').val("");
                                                    $('#ppnbm_amount_2').val("");

                                            }
                                        }else{
                                            alert("NSFP berbeda dengan faktur pajak sebelumnya. Harap scan faktur pajak dengan NSFP yang sama.");
                                            $('#faktur_pajak_url_2').val("");
                                            $('#faktur_pajak_number_2').val("");
                                            $('#faktur_pajak_approval_2').val("");
                                            $('#faktur_pajak_status_2').val("");
                                            $('#faktur_pajak_date_2').val("");
                                            $('#fp_amount_2').val("");
                                            $('#dpp_amount_2').val("");
                                            $('#ppnbm_amount_2').val("");
                                        }
                                    }else{

                                        var first_nsfp = $('#faktur_pajak_number_2').inputmask("unmaskedvalue");
                                        var firstFp = first_nsfp.substr(3);
                                        var second_nsfp = obj.data.nomorFaktur;

                                        if(firstFp == second_nsfp){

                                            if($('#faktur_pajak_status_2').val() == 'Faktur Dibatalkan'){
                                                var statusFaktur = obj.data.statusFaktur;
                                                switch (statusFaktur){
                                                    case 'Faktur Pajak Normal-Pengganti' || 'Faktur Pajak Normal':
                                                        var fpNumber = obj.data.kdJenisTransaksi + obj.data.fgPengganti + obj.data.nomorFaktur;
                                                        var fpDate =  obj.data.tanggalFaktur.split('/').join("-");
                                                        $('#faktur_pajak_url').val($("#barcode_faktur_url_2").val());
                                                        $('#faktur_pajak_number').val(fpNumber);
                                                        $('#faktur_pajak_approval').val(obj.data.statusApproval);
                                                        $('#faktur_pajak_status').val(obj.data.statusFaktur);
                                                        $('#faktur_pajak_date').val(fpDate);
                                                        $('#fp_amount').val(obj.data.jumlahPpn);
                                                        $('#dpp_amount').val(obj.data.jumlahDpp);
                                                        $('#ppnbm_amount').val(obj.data.jumlahPpnBm);

                                                        $('#vendor_npwp').val(obj.data.npwpPenjual);
                                                        $('#vendor_name').val(obj.data.namaPenjual);
                                                        $('#vendor_address').val(obj.data.alamatPenjual);

                                                        $('#npwp_pembeli').val(obj.data.npwpLawanTransaksi);
                                                        $('#nama_pembeli').val(obj.data.namaLawanTransaksi);
                                                        $('#alamat_pembeli').val(obj.data.alamatLawanTransaksi);

                                                        $('#nama_penjual').val(obj.data.namaPenjual);
                                                        $('#alamat_penjual').val(obj.data.alamatPenjual);

                                                        setPajakType();
                                                        break;
                                                    default:
                                                        alert("Status FP yang discan adalah '"+obj.data.statusFaktur+"', harap scan Faktur Normal / Normal-Pengganti");
                                                        $('#faktur_pajak_url').val("");
                                                        $('#faktur_pajak_number').val("");
                                                        $('#faktur_pajak_approval').val("");
                                                        $('#faktur_pajak_status').val("");
                                                        $('#faktur_pajak_date').val("");
                                                        $('#fp_amount').val("");
                                                        $('#dpp_amount').val("");
                                                        $('#ppnbm_amount').val("");

                                                        $('#vendor_npwp').val("");

                                                        $('#npwp_pembeli').val("");
                                                        $('#nama_pembeli').val("");
                                                        $('#alamat_pembeli').val("");

                                                        $('#nama_penjual').val("");
                                                        $('#alamat_penjual').val("");

                                                }
                                            }else{
                                                var statusFaktur = obj.data.statusFaktur;
                                                switch (statusFaktur){
                                                    case 'Faktur Pajak Normal-Pengganti':
                                                        var fpNumber = obj.data.kdJenisTransaksi + obj.data.fgPengganti + obj.data.nomorFaktur;
                                                        var fpDate =  obj.data.tanggalFaktur.split('/').join("-");
                                                        $('#faktur_pajak_url').val($("#barcode_faktur_url_2").val());
                                                        $('#faktur_pajak_number').val(fpNumber);
                                                        $('#faktur_pajak_approval').val(obj.data.statusApproval);
                                                        $('#faktur_pajak_status').val(obj.data.statusFaktur);
                                                        $('#faktur_pajak_date').val(fpDate);
                                                        $('#fp_amount').val(obj.data.jumlahPpn);
                                                        $('#dpp_amount').val(obj.data.jumlahDpp);
                                                        $('#ppnbm_amount').val(obj.data.jumlahPpnBm);

                                                        $('#vendor_npwp').val(obj.data.npwpPenjual);
                                                        $('#vendor_name').val(obj.data.namaPenjual);
                                                        $('#vendor_address').val(obj.data.alamatPenjual);

                                                        $('#npwp_pembeli').val(obj.data.npwpLawanTransaksi);
                                                        $('#nama_pembeli').val(obj.data.namaLawanTransaksi);
                                                        $('#alamat_pembeli').val(obj.data.alamatLawanTransaksi);

                                                        $('#nama_penjual').val(obj.data.namaPenjual);
                                                        $('#alamat_penjual').val(obj.data.alamatPenjual);

                                                        setPajakType();
                                                        break;
                                                    default:
                                                        alert("Status FP yang discan adalah '"+obj.data.statusFaktur+"', harap scan Faktur Normal-Pengganti");
                                                        $('#faktur_pajak_url').val("");
                                                        $('#faktur_pajak_number').val("");
                                                        $('#faktur_pajak_approval').val("");
                                                        $('#faktur_pajak_status').val("");
                                                        $('#faktur_pajak_date').val("");
                                                        $('#fp_amount').val("");
                                                        $('#dpp_amount').val("");
                                                        $('#ppnbm_amount').val("");

                                                        $('#vendor_npwp').val("");

                                                        $('#npwp_pembeli').val("");
                                                        $('#nama_pembeli').val("");
                                                        $('#alamat_pembeli').val("");

                                                        $('#nama_penjual').val("");
                                                        $('#alamat_penjual').val("");

                                                }
                                            }
                                        }else{
                                            alert("NSFP berbeda dengan faktur pajak sebelumnya. Harap scan faktur pajak dengan NSFP yang sama.");
                                            $('#faktur_pajak_url').val("");
                                            $('#faktur_pajak_number').val("");
                                            $('#faktur_pajak_approval').val("");
                                            $('#faktur_pajak_status').val("");
                                            $('#faktur_pajak_date').val("");
                                            $('#fp_amount').val("");
                                            $('#dpp_amount').val("");
                                            $('#ppnbm_amount').val("");

                                            $('#vendor_npwp').val("");

                                            $('#npwp_pembeli').val("");
                                            $('#nama_pembeli').val("");
                                            $('#alamat_pembeli').val("");

                                            $('#nama_penjual').val("");
                                            $('#alamat_penjual').val("");
                                        }
                                    }
                                }

                            } else {
                                App.unblockUI(jQuery("#qrcode_modal_2"));
                                $('#qrcode_modal_2').modal('hide');
                                jQuery.gritter.add({
                                    title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                                    text: 'Gagal Mengambil Data',
                                    sticky: false,
                                    time: 3000
                                });
                            }
                        }
                    });
                },1000);
            }
            e.preventDefault();
        });

        $('#barcode_scanner_2').live('click', function(){

            if($('#faktur_pajak_url').val() && $('#faktur_pajak_url_2').val()){
                alert("Semua data sudah terisi, harap hapus file upload ini terlebih dahulu")
                return;
            }
            $('#qrcode_modal_2').modal('show');
        });

        $('#qrcode_modal_2').on('shown', function() {
            App.unblockUI(jQuery("#qrcode_modal_2"));
            $("#barcode_faktur_url_2").val("");
            $("#barcode_faktur_url_2").focus();
        })

	});
</script>