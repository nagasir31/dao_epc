<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
/*body .modal#add_edit-modal {
  width: 50%; 
  left: 25%;
  margin-left:auto;
  margin-right:auto; 
}*/
</style>
<div class="row-fluid">
	<div class="span12 responsive" data-tablet="span12 fix-offset" data-desktop="span12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="icon-reorder"></i><?php echo $this->lang->line('global_list'); ?></div>
			</div>
			<div class="portlet-body">
				<table class="table table-bordered table-hover" id="table1">
					<thead>
						<tr>
							<th style="width:2%;" class="hidden-480"><?php echo $this->lang->line('global_no'); ?></th>
							<th style="width:5%;" ><?php echo $this->lang->line('berkas_number'); ?></th>
							<th style="width:5%;" class="hidden-480 hidden-767 hidden-979"><?php echo $this->lang->line('berkas_project_code'); ?></th>
							<th style="width:5%;" class="hidden-480 hidden-767 hidden-979"><?php echo $this->lang->line('berkas_vendor_type'); ?></th>
							<th style="width:15%;" class="hidden-480 hidden-767 hidden-979"><?php echo $this->lang->line('berkas_vendor_name'); ?></th>
							<th style="width:6%;" class="hidden-480"><?php echo $this->lang->line('berkas_payment_method'); ?></th>
							<th style="width:7%;" class="hidden-480"><?php echo $this->lang->line('berkas_verify_level'); ?></th>
							<th style="width:7%;text-align:center;" class="hidden-480"><?php echo $this->lang->line('berkas_verify_status'); ?></th>
							<th style="width:5%;text-align:center;" class="hidden-480  hidden-767"><?php echo $this->lang->line('berkas_status'); ?></th>
							<th style="width:4%;text-align:center;" class="hidden-480 hidden-767"><?php echo $this->lang->line('berkas_revision'); ?></th>
							<th style="width:5%;text-align:center;" class="hidden-480 hidden-767"><?php echo $this->lang->line('berkas_released'); ?></th>
							<th style="width:5%;text-align:center;" >#</th>
						</tr>
					</thead>
					<tbody>
						<td colspan="12" class="dataTables_empty"><?php echo $this->lang->line('global_loading_data'); ?></td>
					</tbody>
					<tfoot>
						<tr>
							<td ><input type="hidden" /></td>
							<td ><input type="text" id="search_berkas_number" name="search_berkas_number" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_project_code" name="search_project_code" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_vendor_type" name="search_vendor_type" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_vendor_name" name="search_vendor_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_payment_method" name="search_payment_method" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_last_verify_level" name="search_verify_level" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td style="text-align:center;"><input type="text" id="search_last_verify_status" name="search_verify_status" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td style="text-align:center;"><input type="text" id="search_berkas_status" name="search_berkas_status" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td style="text-align:center;"><input type="text" id="search_revision" name="search_revision" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td style="text-align:center;"><input type="text" id="search_is_rilis" name="search_is_rilis" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td style="text-align:center;"><input type="hidden" /></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="paramFilter" name="paramFilter" value="<?php echo isset($param_filter) ? $param_filter : ""; ?>" />
<script>
	jQuery(document).ready(function(){
		var aSelected = [];
		var asInitVals = [];
		
		// init grid
		var oTable = jQuery('#table1');
		App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
		oTable.dataTable({
            "sPaginationType": "bootstrap",
			"aaSorting": [],
			"aoColumnDefs": [
				{"bSortable": false, "aTargets": [0,11], "bSearchable": false, "aTargets": [0,11]}
		    ],
			"bFilter" : MODULE_FUNCTION.search ? true : false,
			"bServerSide": true,
			"sAjaxSource": "<?php echo app_backend_url('main/berkas/get_list'); ?>",
			"sServerMethod": "POST",
			"bStateSave": true,
			"fnStateSave": function(oSettings, oData) { 
				ceki.fnSaveDtView(oSettings, oData); 
			},
			"fnStateLoad": function(oSettings) { 
				dataFN = JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname));
				if (dataFN != null) {
					jQuery('#search_berkas_number').val(dataFN.aoSearchCols[1].sSearch);
					jQuery('#search_project_code').val(dataFN.aoSearchCols[2].sSearch);
					jQuery('#search_vendor_type').val(dataFN.aoSearchCols[3].sSearch);
					jQuery('#search_vendor_name').val(dataFN.aoSearchCols[4].sSearch);
					jQuery('#search_payment_method').val(dataFN.aoSearchCols[5].sSearch);
					jQuery('#search_last_verify_level').val(dataFN.aoSearchCols[6].sSearch);
					jQuery('#search_last_verify_status').val(dataFN.aoSearchCols[7].sSearch);
					jQuery('#search_berkas_status').val(dataFN.aoSearchCols[8].sSearch);
					jQuery('#search_verify_status').val(dataFN.aoSearchCols[9].sSearch);
					jQuery('#search_berkas_status').val(dataFN.aoSearchCols[10].sSearch);
				}
				return ceki.fnLoadDtView(oSettings); 
			},
			"oLanguage": {
				"sLengthMenu": "<?php echo $this->lang->line('global_DT_sLengthMenu'); ?>",
				"sZeroRecords": "<?php echo $this->lang->line('global_DT_sZeroRecords'); ?>",
				"sInfo": "<?php echo $this->lang->line('global_DT_sInfo'); ?>",
				"sInfoEmpty": "<?php echo $this->lang->line('global_DT_sInfoEmpty'); ?>",
				"sInfoFiltered": "(<?php echo $this->lang->line('global_DT_sInfoFiltered'); ?>)",
				"sSearch": "<?php echo $this->lang->line('global_search'); ?> :",
				"oPaginate": {
					"sPrevious": "<?php echo $this->lang->line('global_prev'); ?>",
					"sNext": "<?php echo $this->lang->line('global_next'); ?>"
				}
			},
			"fnServerParams": function (aoData) {
				aoData.push({"name": "is_ajax", "value": true});
			},
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(jQuery('#paramFilter').val() != '') {
					aoData.push({"name": "paramFilter", "value": jQuery('#paramFilter').val()});
				}
				jQuery.post(sSource, aoData, function(obj) {
					if(obj.success == false){
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: obj.message,
							sticky: false,
							time: 3000
						});
						
						if(obj.expired) {
							setTimeout(function(){window.location= '<?php echo app_backend_url('login'); ?>'},1000);
						}
					}
					else{
						fnCallback(obj)
					}
				}, 'json');
			},
            "fnDrawCallback": function(oSettings) {
				if(!jQuery(this).children('.blockUI').length){
					App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				}
				
				App.initUniform();
				App.initPopovers();
				App.initTooltips();
				
				App.unblockUI(jQuery(this));
				
				oTable.floatThead({
					top: ceki.pageTop
				});
				
				jQuery('.blink').pulsate({
					color: "#bf1c56"
				});	
            },
			"aoColumns": [
				{ "mData": "rnum", "sClass": "hidden-480" },
				{ "mData": "berkas_number" },
				{ "mData": "project_code", "sClass": "hidden-480 hidden-767 hidden-979" },
				{ "mData": "vendor_type", "sClass": "hidden-480 hidden-767 hidden-979" },
				{ "mData": "vendor_name", "sClass": "hidden-480 hidden-767 hidden-979" },
				{ "mData": "payment_method", "sClass": "hidden-480" },
				{ "mData": "last_verify_level", "sClass": "hidden-480" },
				{ "mData": "last_verify_status", "sClass": "hidden-480" },
				{ "mData": "berkas_status", "sClass": "hidden-480 hidden-767" },
				{ "mData": "revision", "sClass": "hidden-480 hidden-767" },
				{ "mData": "is_rilis", "sClass": "hidden-480 hidden-767"  },
				{ "mData": "actions" }
			],
			"aLengthMenu": [
				[5, 15, 20, 50, 100, -1],
				[5, 15, 20, 50, 100, "All"]
			],
            "iDisplayLength": 5,
			"sDom": "<'dataTables_toolbar'><'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"fnPreDrawCallback": function () { 
				if (jQuery('.dataTables_length select').val() == -1) {
					if(oTable.fnSettings().fnRecordsTotal() > 500) {
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_limit_record_page'); ?>',
							sticky: false,
							time: 3000
						});
						return false;
					}
				}				
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				if (aData['last_verify_statuss'] == refVerifyStatus[3] && MODULE_FUNCTION.revision){
					jQuery(nRow).addClass("blink");
				}
			}
        });
		
		jQuery('#table1_wrapper .dataTables_filter input').addClass("m-wrap");
		jQuery('#table1_wrapper .dataTables_length select').addClass("m-wrap small"); 
		jQuery('#table1_wrapper .dataTables_length select').select2(); 
		
		jQuery('#table1_filter input').unbind();
	    jQuery('#table1_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				oTable.fnFilter(this.value);   
			}
		}); 
		
		jQuery('tfoot input').unbind();
		jQuery("tfoot input").keyup(function (e) {
			if(e.keyCode == 13) {
				App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				oTable.fnFilter(this.value, jQuery("tfoot input").index(this));  
			}
			e.preventDefault();
		});
		
		jQuery("tfoot input").each( function (i) {
			asInitVals[i] = this.value;
		});
		
		jQuery("tfoot input").blur( function (i) {
			if (this.value == ""){
				this.className = "search_init";
				//this.value = asInitVals[jQuery("tfoot input").index(this)];
			}
		});
		
		jQuery('.checkall').click(function(){
			
			var ch = oTable.find('input[type=checkbox]');
			
			var checked = jQuery(this).is(':checked');
			
			ch.each(function(){
				if(this.value != 'on') {
					fnSelected(this, this.value, checked , true);
				}
			});	
			
			fnTbarDisableEnable();
		});
		
		function fnSelected(cmp, id, checked, all){
			var index = jQuery.inArray(id, aSelected);
			if(id == 'on') return;		 
			if ( index === -1 ) {
				if(all){
					if(checked){
						aSelected.push(id);
					}
				}else{
					aSelected.push(id);
				}
			} else {
				if(all){
					if(!checked){
						aSelected.splice(index, 1);
					}
				}else{
					aSelected.splice(index, 1);
				}
			}
			
			if(checked) {	
				jQuery(cmp).attr('checked',true);
				jQuery(cmp).parent().addClass('checked');
				jQuery(cmp).parents('tr').addClass('selected');
			}else{
				jQuery(cmp).attr('checked',false);
				jQuery(cmp).parent().removeClass('checked');
				jQuery(cmp).parents('tr').removeClass('selected');
			}
			
			console.log(aSelected);
		}
		
		function fnTbarDisableEnable(){
			var rowSelected = aSelected.length;
			
			if(rowSelected > 0){
				if(rowSelected > 1){
					jQuery('#button-edit').attr("disabled", "disabled");
				}else{
					jQuery('#button-edit').removeAttr("disabled");
				}
				jQuery('#button-delete').removeAttr("disabled");
			}else{
				jQuery('#button-edit').attr("disabled", "disabled");
				jQuery('#button-delete').attr("disabled", "disabled");
			}
		}
		
		jQuery("div.dataTables_toolbar").html(' '+
		'<button id="button-refresh" class="btn purple"><i class="icon-refresh"></i> <?php echo $this->lang->line('global_refresh'); ?></button>&nbsp;' +
		'<button id="button-add" class="btn green" style="visibility:hidden;"><i class="icon-plus"></i></i> <?php echo $this->lang->line('global_add'); ?></button>&nbsp;' +
		'<div style="float:right;"><button id="button-clear" class="btn black"><i class="icon-share"></i></a></div> ');
		
	
		jQuery('#button-refresh').click(function(){
			App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
			oTable.fnDraw();
		});
		
		jQuery('#button-clear').click(function(){
			ceki.fnResetDtView();
			ceki.fnRefresh()
		});
		
		if(MODULE_FUNCTION.add && jQuery('#paramFilter').val() == ''){
			jQuery("#button-add").css('visibility','visible');
		}
		
		jQuery('#button-add').click(function(){
			ceki.fnLocation('<?php echo app_backend_url('main/berkas/input'); ?>');
		});
		
		jQuery('#button-delete').live('click',function(){
			var id = jQuery(this).attr('data-id');
			var berkas_number = jQuery(this).attr('data-berkas_number');
			jConfirm('<?php echo $this->lang->line('global_confirm_delete'); ?>', '<?php echo $this->lang->line('global_confirm_delete_header'); ?>', function(r) {
				if(r){
					var cAjax = new ceki.fnAjax({
						method : '/delete',
						data : ({
							id: id,
							berkas_number: berkas_number
						}),
						successCallBack : function() {
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_success_delete'); ?>',
								sticky: false,
								time: 3000
							});
							oTable.fnDraw();
							aSelected = [];
					
							fnTbarDisableEnable();
						}
					});
				}
			});
		});
		
	});
</script>