<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
.form-horizontal .control-label {
	width:260px;
	padding-right: 10px;
}

.thumb-img{
	height:200px;
	margin-left:auto;
	margin-right:auto;
}
</style>
<div id="main-container">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-credit-card"></i><?php echo $this->lang->line('global_detail'); ?> <?php echo $this->lang->line('berkas_project'); ?> </div>
		</div>
		<div class="portlet-body form">
			<div class="row-fluid">
				<div class="span12">
					<h3 class="block center hidden-480">
						<?php echo $res_berkas_proyek[0]['project_name']; ?> ( <?php echo $res_berkas_proyek[0]['project_code']; ?> )
					</h3>
					<br>
					<div class="row-fluid">
						<div class="span12">
							<div class="span4 responsive" >
								<div class="dashboard-stat grey">
									<div class="visual">
										<i class="icon-idr"></i>
									</div>
									<div class="details">
										<div class="number" style="font-size:22px;font-weight:bold;">
											Rp. <?php echo $total_hutang_valid_ncl; ?>
										</div>
										<div class="desc">                           
											<?php echo $this->lang->line('berkas_total_hutang_valid'); ?> NCL
										</div>
									</div>   
									<a class="more" href="javascript;"></a>	  
								</div>
							</div>
							
							<div class="span4 responsive" >
								<div class="dashboard-stat red">
									<div class="visual">
										<i class="icon-idr"></i>
									</div>
									<div class="details">
										<div class="number" style="font-size:22px;font-weight:bold;">
											Rp. <?php echo $total_hutang_valid; ?>
										</div>
										<div class="desc">                           
											<?php echo $this->lang->line('berkas_total_hutang_valid'); ?>
										</div>
									</div>   
									<a class="more" href="javascript;"></a>	  
								</div>
							</div>
							
							<div class="span4 responsive" >
								<div class="dashboard-stat green">
									<div class="visual">
										<i class="icon-check"></i>
									</div>
									<div class="details">
										<div class="number" style="font-size:22px;font-weight:bold;">
											<?php echo $total_berkas_selesai; ?>
										</div>
										<div class="desc">                           
											<?php echo $this->lang->line('berkas_total_status_selesai'); ?>
										</div>
									</div>
									<a class="more" href="javascript;"></a>           
								</div>
							</div>
						</div>
					</div>
					
					<div class="row-fluid">
						<div class="span12">
							<div class="span4 responsive" >
								<div class="dashboard-stat purple">
									<div class="visual">
										<i class="icon-money"></i>
									</div>
									<div class="details">
										<div class="number" style="font-size:22px;font-weight:bold;">
											Rp. <?php echo $total_pembayaran_ncl; ?>
										</div>
										<div class="desc">                           
											<?php echo $this->lang->line('berkas_total_pembayaran'); ?> NCL
										</div>
									</div>  
									<a class="more" href="javascript;"></a>	    				
								</div>
							</div>
							
							<div class="span4 responsive" >
								<div class="dashboard-stat blue">
									<div class="visual">
										<i class="icon-money"></i>
									</div>
									<div class="details">
										<div class="number" style="font-size:22px;font-weight:bold;">
											Rp. <?php echo $total_pembayaran; ?>
										</div>
										<div class="desc">                           
											<?php echo $this->lang->line('berkas_total_pembayaran'); ?>
										</div>
									</div>  
									<a class="more" href="javascript;"></a>	    				
								</div>
							</div>
							
							<div class="span4 responsive" >
								<div class="dashboard-stat yellow">
									<div class="visual">
										<i class="icon-retweet"></i>
									</div>
									<div class="details">
										<div class="number" style="font-size:22px;font-weight:bold;">
											<?php echo $total_berkas_proses; ?>
										</div>
										<div class="desc">                           
											<?php echo $this->lang->line('berkas_total_status_proses'); ?>
										</div>
									</div>     
									<a class="more" href="javascript;"></a>			
								</div>
							</div>
						</div>
					</div>
									
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><?php echo $this->lang->line('global_info'); ?> <?php echo $this->lang->line('berkas_project'); ?> </div>
							<div class="tools">
								<a class="collapse" href="javascript:;"></a>
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-hover table-striped table-bordered">
								<tbody>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_project_code'); ?></b></td>
										<td><?php echo ! empty($res_berkas_proyek[0]['project_code']) ? $res_berkas_proyek[0]['project_code'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_project_name'); ?></b></td>
										<td><?php echo ! empty($res_berkas_proyek[0]['project_code']) ? $res_berkas_proyek[0]['project_name'] : '-'; ?></td>
									</tr>
									<tr>
										<td class="right" style="width:30%;"><b><?php echo $this->lang->line('berkas_project_location'); ?></b></td>
										<td><?php echo ! empty($res_berkas_proyek[0]['project_location']) ? $res_berkas_proyek[0]['project_location'] : '-'; ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><?php echo $this->lang->line('berkas_list'); ?></div>
							<div class="tools">
								<a class="config" href="javascript:;"></a>
								<a class="reload" href="javascript:;"></a>
								<a class="collapse" href="javascript:;"></a>
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-bordered table-hover" id="table1">
								<thead>
									<tr>
										<th style="width:2%;" class="hidden-480"><?php echo $this->lang->line('global_no'); ?></th>
										<th style="width:5%;" ><?php echo $this->lang->line('berkas_number'); ?></th>
										<th style="width:5%;" class="hidden-480"><?php echo $this->lang->line('berkas_vendor_type'); ?></th>
										<th style="width:15%;" class="hidden-480"><?php echo $this->lang->line('berkas_vendor_name'); ?></th>
										<th style="width:5%;" class="hidden-480"><?php echo $this->lang->line('berkas_payment_method'); ?></th>
										<th style="width:5%;" class="hidden-480"><?php echo $this->lang->line('berkas_verify_level'); ?></th>
										<th style="width:5%;text-align:center;" class="hidden-480"><?php echo $this->lang->line('berkas_verify_status'); ?></th>
										<th style="width:5%;text-align:center;" class="hidden-480"><?php echo $this->lang->line('berkas_status'); ?></th>
										<th style="width:4%;text-align:center;" class="hidden-480"><?php echo $this->lang->line('berkas_revision'); ?></th>
										<th style="width:7%;" class="hidden-480"><?php echo $this->lang->line('berkas_total_hutang_valid'); ?></th>
										<th style="width:7%;" class="hidden-480"><?php echo $this->lang->line('berkas_total_pembayaran'); ?></th>
										<th style="width:3%;text-align:center;" >#</th>
									</tr>
								</thead>
								<tbody>
									<td colspan="12" class="dataTables_empty"><?php echo $this->lang->line('global_loading_data'); ?></td>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="row-fluid">
				<div class="span12">
					<div class="span4 offset2">
						<div class="portlet">
							<div class="portlet-title">
								<div class="caption">Statistik Status Berkas - NCL</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table-bordered table-striped table-hover table-condensed cf" style="width:100%">
									<thead class="of">
										<tr>
											<th colspan="2"><?php echo $this->lang->line('global_description'); ?></th>
											<th><?php echo $this->lang->line('global_total'); ?></th>
										</tr>
									</thead>
									
									<tbody>
										<?php if(isset($stat_status_ncl['stat_status']) && ! empty($stat_status_ncl['stat_status'][0])): ?>
										<tr>
											<td colspan="2" style="text-align: right;">
												<?php echo $this->lang->line('berkas_name').' '.$this->lang->line('berkas_release'); ?>
												<div class="label label" title="<?php echo $this->lang->line('berkas_release'); ?>">                        
													<i class="icon-plus"></i>
												</div>
												
											</td>
											<td class="center">
												<?php echo $stat_status_ncl['stat_status'][0]['total_rilis']; ?>
											</td>
										</tr>
										
										<?php foreach($stat_status_ncl['stat_status'][0]['total_verify'] as $key => $val): ?>
										<tr>
											<td rowspan="3" style="text-align:right;"><?php echo $key; ?></td>
											<?php $i=0;foreach($val as $k => $v): ?>
											<?php
												$label_class = $icon_class = '';
												if($k == $this->ref_verify_status[1])
												{
													$label_class = 'label-warning';
													$icon_class = 'icon-signin';
												}
												elseif($k == $this->ref_verify_status[2])
												{
													$label_class = 'label-success';
													$icon_class = 'icon-thumbs-up';
												}
												elseif($k == $this->ref_verify_status[3])
												{
													$label_class = 'label-important';
													$icon_class = 'icon-thumbs-down';
												}
											?>
											<?php echo $i != 0 ? "<tr>" : ""; ?>
											<td style="text-align: right;">
												<?php echo $this->lang->line('berkas_name').' '.$k; ?> 
												<div class="label <?php echo $label_class; ?>" title="<?php echo $k; ?>">                        
													<i class="<?php echo $icon_class; ?>"></i>
												</div>
												
											</td>
											<td class="center"><?php echo $v; ?></td>
											<?php echo $i != 0 ? "</tr>" : ""; ?>
											<?php $i++;endforeach; ?>
										</tr>
										<?php endforeach; ?>
										<tr>
											<td colspan="2" style="text-align: right;">
												<?php echo $this->lang->line('berkas_name').' '.$this->ref_berkas_status[2]; ?>
												<div class="label label-info" title="<?php echo $this->ref_berkas_status[2]; ?>">                        
													<i class="icon-ok"></i>
												</div>
												
											</td>
											<td class="center">
												<?php echo $stat_status_ncl['stat_status'][0]['total_selesai']; ?>
											</td>
										</tr>
										<?php else: ?>
										<tr>
											<td colspan="3" class="center"><?php echo $this->lang->line('global_DT_sZeroRecords'); ?></td>
										</tr>
										<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
					<div class="span4">
						<div class="portlet">
							<div class="portlet-title">
								<div class="caption">Statistik Status Berkas - REGULAR</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table-bordered table-striped table-hover table-condensed cf" style="width:100%">
									<thead class="of">
										<tr>
											<th colspan="2"><?php echo $this->lang->line('global_description'); ?></th>
											<th><?php echo $this->lang->line('global_total'); ?></th>
										</tr>
									</thead>
									
									<tbody>
										<?php if(isset($stat_status_reg['stat_status']) && ! empty($stat_status_reg['stat_status'][0])): ?>
										<tr>
											<td colspan="2" style="text-align: right;">
												<?php echo $this->lang->line('berkas_name').' '.$this->lang->line('berkas_release'); ?>
												<div class="label label" title="<?php echo $this->lang->line('berkas_release'); ?>">                        
													<i class="icon-plus"></i>
												</div>
												
											</td>
											<td class="center">
												<?php echo $stat_status_reg['stat_status'][0]['total_rilis']; ?>
											</td>
										</tr>
										
										<?php foreach($stat_status_reg['stat_status'][0]['total_verify'] as $key => $val): ?>
										<tr>
											<td rowspan="3" style="text-align:right;"><?php echo $key; ?></td>
											<?php $i=0;foreach($val as $k => $v): ?>
											<?php
												$label_class = $icon_class = '';
												if($k == $this->ref_verify_status[1])
												{
													$label_class = 'label-warning';
													$icon_class = 'icon-signin';
												}
												elseif($k == $this->ref_verify_status[2])
												{
													$label_class = 'label-success';
													$icon_class = 'icon-thumbs-up';
												}
												elseif($k == $this->ref_verify_status[3])
												{
													$label_class = 'label-important';
													$icon_class = 'icon-thumbs-down';
												}
											?>
											<?php echo $i != 0 ? "<tr>" : ""; ?>
											<td style="text-align: right;">
												<?php echo $this->lang->line('berkas_name').' '.$k.' '; ?> 
												<div class="label <?php echo $label_class; ?>" title="<?php echo $k; ?>">                        
													<i class="<?php echo $icon_class; ?>"></i>
												</div>
												
											</td>
											<td class="center"><?php echo $v; ?></td>
											<?php echo $i != 0 ? "</tr>" : ""; ?>
											<?php $i++;endforeach; ?>
										</tr>
										<?php endforeach; ?>
										<tr>
											<td colspan="2" style="text-align: right;">
												<?php echo $this->lang->line('berkas_name').' '.$this->ref_berkas_status[2]; ?>
												<div class="label label-info" title="<?php echo $this->ref_berkas_status[2]; ?>">                        
													<i class="icon-ok"></i>
												</div>
												
											</td>
											<td class="center">
												<?php echo $stat_status_reg['stat_status'][0]['total_selesai']; ?>
											</td>
										</tr>
										<?php else: ?>
										<tr>
											<td colspan="3" class="center"><?php echo $this->lang->line('global_DT_sZeroRecords'); ?></td>
										</tr>
										<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div> 
	</div>
	<p class="center">
		<button id="btn_back" type="button" class="btn"><?php echo $this->lang->line('global_back'); ?></button>
	</p>
	<div class="clearfix space20"></div>
</div>
<input type="hidden" id="paramFilter" name="paramFilter" value="<?php echo isset($param_filter) ? $param_filter : ""; ?>" />
<script>
	jQuery(document).ready(function(){
		// init grid
		var oTable = jQuery('#table1');
		App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
		oTable.dataTable({
			"sPaginationType": "bootstrap",
			"aaSorting": [],
			"aoColumnDefs": [
				{"bSortable": false, "aTargets": [0,9,10,11], "bSearchable": false, "aTargets": [0,9,10,11]}
			],
			"bFilter" : true,
			"bServerSide": true,
			"sAjaxSource": "<?php echo app_backend_url('backend/get_berkas_proyek_list'); ?>",
			"sServerMethod": "POST",
			"bStateSave": true,
			"fnStateSave": function(oSettings, oData) { 
				ceki.fnSaveDtView(oSettings, oData); 
			},
			"fnStateLoad": function(oSettings) { 
				dataFN = JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname));
				return ceki.fnLoadDtView(oSettings); 
			},
			"oLanguage": {
				"sLengthMenu": "<?php echo $this->lang->line('global_DT_sLengthMenu'); ?>",
				"sZeroRecords": "<?php echo $this->lang->line('global_DT_sZeroRecords'); ?>",
				"sInfo": "<?php echo $this->lang->line('global_DT_sInfo'); ?>",
				"sInfoEmpty": "<?php echo $this->lang->line('global_DT_sInfoEmpty'); ?>",
				"sInfoFiltered": "(<?php echo $this->lang->line('global_DT_sInfoFiltered'); ?>)",
				"sSearch": "<?php echo $this->lang->line('global_search'); ?> :",
				"oPaginate": {
					"sPrevious": "<?php echo $this->lang->line('global_prev'); ?>",
					"sNext": "<?php echo $this->lang->line('global_next'); ?>"
				}
			},
			"fnServerParams": function (aoData) {
				aoData.push({"name": "is_ajax", "value": true});
			},
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(jQuery('#paramFilter').val() != '') {
					aoData.push({"name": "paramFilter", "value": jQuery('#paramFilter').val()});
				}
				jQuery.post(sSource, aoData, function(obj) {
					if(obj.success == false){
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: obj.message,
							sticky: false,
							time: 3000
						});
						
						if(obj.expired) {
							setTimeout(function(){window.location= '<?php echo app_backend_url('login'); ?>'},1000);
						}
					}
					else{
						fnCallback(obj)
					}
				}, 'json');
			},
			"fnDrawCallback": function(oSettings) {
				if(!jQuery(this).children('.blockUI').length){
					App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				}
				
				App.initUniform();
				App.initPopovers();
				App.initTooltips();
				
				App.unblockUI(jQuery(this));
				
				oTable.floatThead({
					top: ceki.pageTop
				});
				
				jQuery('.blink').pulsate({
					color: "#bf1c56"
				});	
			},
			"aoColumns": [
				{ "mData": "rnum", "sClass": "hidden-480" },
				{ "mData": "berkas_number" },
				{ "mData": "vendor_type", "sClass": "hidden-480" },
				{ "mData": "vendor_name", "sClass": "hidden-480" },
				{ "mData": "payment_method", "sClass": "hidden-480" },
				{ "mData": "last_verify_level", "sClass": "hidden-480" },
				{ "mData": "last_verify_status", "sClass": "hidden-480" },
				{ "mData": "berkas_status", "sClass": "hidden-480" },
				{ "mData": "revision", "sClass": "hidden-480" },
				{ "mData": "total_hutang_valid", "sClass": "hidden-480" },
				{ "mData": "total_pembayaran", "sClass": "hidden-480" },
				{ "mData": "actions" }
			],
			"aLengthMenu": [
				[5, 15, 20, 50, 100, -1],
				[5, 15, 20, 50, 100, "All"]
			],
			"iDisplayLength": 5,
			"sDom": "<'dataTables_toolbar'><'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"fnPreDrawCallback": function () { 
				if (jQuery('.dataTables_length select').val() == -1) {
					if(oTable.fnSettings().fnRecordsTotal() > 500) {
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_limit_record_page'); ?>',
							sticky: false,
							time: 3000
						});
						return false;
					}
				}				
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				if (aData['last_verify_statuss'] == refVerifyStatus[3] && MODULE_FUNCTION.revision){
					jQuery(nRow).addClass("blink");
				}
			}
		});
		
		jQuery('#table1_wrapper .dataTables_filter input').addClass("m-wrap");
		jQuery('#table1_wrapper .dataTables_length select').addClass("m-wrap small"); 
		jQuery('#table1_wrapper .dataTables_length select').select2(); 
		
		jQuery('#table1_filter input').unbind();
		jQuery('#table1_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				oTable.fnFilter(this.value);   
			}
		}); 
		
		jQuery('.reload').click(function(){
			App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
			oTable.fnDraw();
		});
		
		jQuery('.config').click(function(){
			ceki.fnResetDtView();
			ceki.fnRefresh();
		});
		
		jQuery('#btn_back').click(function(e) {
			ceki.fnLocation('<?php echo app_backend_url(); ?>');
		});
	});
</script>