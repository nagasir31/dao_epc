<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="main-container">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-credit-card"></i><?php echo $this->lang->line('berkas_tracking_result'); ?>:  <?php echo $res_berkas['project_code'].$res_berkas['berkas_number']; ?></div>
		</div>
		<div class="portlet-body form">
			<div class="row-fluid">
				<div class="span12">
					<h3 class="block center">
						<?php echo $res_berkas['project_name']; ?> ( <?php echo $res_berkas['project_code']; ?> ) - <?php echo $res_berkas['berkas_number']; ?>
					</h3>
									
					<div class="row-fluid">
						<div class="span6 offset3">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th style="width:50%"><?php echo $this->lang->line('global_description'); ?></th>
										<th style="width:30%;text-align:center;"><?php echo $this->lang->line('global_date_time_long'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php $rnum=1;foreach($res_berkas_verify as $dt): ?>
									<?php if(! empty($dt['verify_status'])): ?>
										<?php
											$verify_datetime = $CTR->convert_date($dt['verify_datetime'], 'd M Y H:i');
										?>
										
										<?php if($dt['verify_level_index'] == 1): ?>
										<?php 
											$revision_datetime = $CTR->convert_date($dt['verify_datetime'], 'd M Y H:i');
											$revisi_ke = '';
											if(! empty($dt['revision']))
											{
												$revisi_ke = ', Pada '.$this->lang->line('berkas_revision').': '.$dt['revision'];
												$revision_datetime = $CTR->convert_date($dt['revision_datetime'], 'd M Y H:i');
											}
										?>
											<tr>
												<td>
													<div class="label label">                        
														<i class="icon-plus"></i>
													</div>
													<?php echo $this->lang->line('berkas_name').' '.$this->lang->line('berkas_release').''.$revisi_ke; ?>
												</td>
												<td style="width:30%;text-align:center;">
													<i><?php echo $revision_datetime; ?></i>
												</td>
											</tr>
										<?php endif; ?>
										<tr>
											<td>
												<div class="label label-warning">                        
													<i class="icon-signin"></i>
												</div>
												<?php echo $this->lang->line('berkas_name').' '.$this->lang->line('berkas_pending_at').' <b>'.$dt['verify_level'].'</b>'; ?>
											</td>
											<td style="width:30%;text-align:center;">
												<i><?php echo $verify_datetime; ?></i>
											</td>
										</tr>
									
										<?php if(! empty($dt['approval_datetime'])): ?>
										<?php 
											$approval_datetime = $CTR->convert_date($dt['approval_datetime'], 'd M Y H:i');
											
											$lang_status = $dt['verify_status'];
											
											$label_class = "label-success";
											$icon_class = "icon-thumbs-up";
											if($dt['verify_status'] == $CTR->ref_verify_status[3])
											{
												$label_class = "label-important";
												$icon_class = "icon-thumbs-down";
											}
										?>
										<tr>
											<td>
												<div class="label <?php echo $label_class; ?>">                        
													<i class="<?php echo $icon_class; ?>"></i>
												</div>
												<?php echo $this->lang->line('berkas_name').' '.$lang_status.' Oleh <b>'.$dt['verify_level'].'</b>'; ?>
											</td>
											<td style="width:30%;text-align:center;">
												<i><?php echo $approval_datetime; ?></i>
											</td>
										</tr>
										<?php endif; ?>
										
										<?php if($dt['verify_status'] == $CTR->ref_verify_status[2]): ?>
											<?php
												$max_level = $CTR->def_model->get_max(array('table' => 'berkas_verify', 'where' => array('berkas_id' => $dt['berkas_id']), 'select' => 'verify_level_index'));
												$max_level_index = $max_level['verify_level_index'];
											?>
											<?php if($dt['verify_level_index'] == $max_level_index): ?>
											<tr>
												<td>
													<span id="done">
													<div class="label label-info">                        
														<i class="icon-ok"></i>
													</div>
													<?php
														$text = ! empty($res_berkas['revision']) ? ', Pada '.$this->lang->line('berkas_revision').': '.$dt['revision'] : '';
													?>
													<?php echo $this->lang->line('berkas_name').' '.$res_berkas['berkas_status'].$text; ?>
													</span>
												</td>
												<td style="width:30%;text-align:center;">
													<i><?php echo $approval_datetime; ?></i>
												</td>
											</tr>
											<?php endif; ?>
										<?php endif; ?>
										
									<?php endif; ?>
									<?php $rnum++;endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				
				</div>
			</div>
		</div> 
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		var $table = jQuery('table.table');
		$table.floatThead({
			top: ceki.pageTop
		});
		
		$blinking = jQuery('#done');
		setInterval(function() {
			visibility = $blinking.css('visibility');

			if (visibility === 'hidden') {
				$blinking.css('visibility', 'visible');
			} else {
				$blinking.css('visibility', 'hidden');
			}
		}, 1000);
	});
</script>