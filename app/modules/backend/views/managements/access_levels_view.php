<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style></style>
<div id="access_levels">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-reorder"></i><?php echo $this->lang->line('global_access_level'); ?></div>
		</div>
		
		<div class="portlet-body" style="overflow:auto;">
			<div class="span12">
				<div class="tabbable tabbable-custom tabs-left">
					<ul class="nav nav-tabs tabs-left">
						<?php $x = 1; foreach($group as $grp): ?>
							<li class="nav <?php echo $x == 1 ? 'active': ''; ?>" id="<?php echo $grp['id']; ?>"><a data-toggle="tab" href="#tabs-group-<?php echo $grp['id']; ?>"><?php echo ucwords($grp['description']); ?></a></li>
						<?php $x++; endforeach;  ?>
					</ul>
					<div class="tab-content">
						<?php  $x = 1; foreach($group as $grp): ?>
						<div id="tabs-group-<?php echo $grp['id'] ?>" class="tab-pane <?php echo $x == 1 ? 'active': ''; ?>">
							<div class="tabbable tabbable-custom">
								<ul class="nav nav-tabs">
									<?php if($this->site_config['is_active_backend']): ?>
										<li class="tb active"><a href="#tabs-sys-<?php echo $grp['id']; ?>" data-toggle="tab" onclick="fnGetMenuGroup('sys','<?php echo $grp['id']; ?>');"><?php echo $this->lang->line('global_backend'); ?></a></li>
									<?php endif; ?>
									<?php if($this->site_config['is_active_frontend'] && $this->site_config['is_active_frontend_acl']): ?>
										<li><a href="#tabs-app-<?php echo $grp['id']; ?>" data-toggle="tab" onclick="fnGetMenuGroup('app','<?php echo $grp['id']; ?>');"><?php echo $this->lang->line('global_frontkend'); ?></a></li>
									<?php endif; ?>
								</ul>
								<div class="tab-content">
									<?php if($this->site_config['is_active_backend']): ?>
										<div  class="tab-pane active" id="tabs-sys-<?php echo $grp['id']; ?>">
											
											<div class="span6">
												<div class="dd" id="nestable_backend">
													<ol id="sys-<?php echo $grp['id']; ?>" class="dd-list"></ol>
												</div>
											</div>	
											<div class="span6" id="sys-function-<?php echo $grp['id']; ?>"></div>
										</div>
									<?php endif; ?>
									<?php if($this->site_config['is_active_frontend'] && $this->site_config['is_active_frontend_acl']): ?>
										<div class="tab-pane" id="tabs-app-<?php echo $grp['id']; ?>">
											<div class="span6">
												<div class="dd" id="nestable_backend">
													<ol id="app-<?php echo $grp['id']; ?>" class="dd-list"></ol>
												</div>
											</div>	
											<div class="span6" id="app-function-<?php echo $grp['id']; ?>"></div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<?php  $x++; endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		
		fnGetMenuGroup('sys',jQuery('li.nav.active').attr('id'));
		
		jQuery('li.nav').on('click', function() {
			fnGetMenuGroup('sys',jQuery(this).attr('id'));
		});
	});
	
	function fnGetMenuGroup(mode, groupID){
		
		ceki.fnLoading(mode+'-'+groupID);
		var cAjax = new ceki.fnAjax({
			method : '/get_menu_group/' + mode,
			data : ({
				groupID: groupID,
				is_ajax: 1
			}),
			successCallBack : function(obj) {
				jQuery('#'+mode+'-'+groupID).html(obj.message).fadeIn("slow");
				jQuery('#'+mode+'-'+groupID).css('opacity', 0);
				jQuery('#'+mode+'-'+groupID).animate({
					opacity : 1
				}, 3000);
				
				jQuery('.menulink').on('click', function() {
					ceki.fnLoading(mode+'-function-'+groupID);
					var cAjax = new ceki.fnAjax({
						method : '/get_function_acl/' + mode,
						data : ({
							menuID: jQuery(this).attr('id'),
							groupID: groupID,
							menuName: jQuery(this).html(),
							is_ajax: 1
						}),
						successCallBack : function(obj) {
							jQuery('#'+mode+'-function-'+groupID).html(obj.message).fadeIn("slow");
							jQuery('#'+mode+'-function-'+groupID).css('opacity', 0);
							jQuery('#'+mode+'-function-'+groupID).animate({
								opacity : 1
							}, 3000);
							
							jQuery('#form-'+mode+'-function-'+groupID).submit(function(e) {
								var cAjax = new ceki.fnAjax({
									method : '/save_function_acl/' + mode,
									data : jQuery(this).serialize(),
									successCallBack : function(obj) {
										jQuery.gritter.add({
											title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
											text: '<?php echo $this->lang->line('global_success_update'); ?>',
											sticky: false,
											time: 1000
										});
									}
								});
								e.preventDefault();
							});
						}
					});
				});
				
				jQuery("input[name='cbox_"+mode+"']").on('click', function() {
					
					isActive = typeof jQuery(this).attr("checked") == 'undefined' ? 0 : 1;
					
					var cAjax = new ceki.fnAjax({
						method : '/disable_enable_acl/' + mode,
						data : ({
							menuID: jQuery(this).attr('menuID'),
							groupID: groupID,
							isActive: isActive,
							is_ajax: 1
						}),
						successCallBack : function(obj) {
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_success_update'); ?>',
								sticky: false,
								time: 1000
							});
						}
					});
				});
			}
		});
	}
</script>