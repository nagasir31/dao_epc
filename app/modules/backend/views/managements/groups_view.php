<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="row-fluid">
	<div class="span12 responsive" data-tablet="span12 fix-offset" data-desktop="span12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="icon-reorder"></i><?php echo $this->lang->line('global_list'); ?></div>
			</div>
			<div class="portlet-body">
				<table class="table table-bordered table-hover" id="table1">
					<thead>
						<tr>
							<th style="width:2%;text-align:center;"><input type="checkbox" class="checkall" data-set="#table1 .checkboxes" /></th>
							<th style="width:2%;" class="hidden-480"><?php echo $this->lang->line('global_no'); ?></th>
							<th style="width:20%;"><?php echo $this->lang->line('global_name'); ?></th>
							<th style="width:20%;" class="hidden-480"><?php echo $this->lang->line('global_desc'); ?></th>
							<th style="width:20%;" class="hidden-480"><?php echo $this->lang->line('groups_parent_name'); ?></th>
						</tr>
					</thead>
					<tbody>
						<td colspan="5" class="dataTables_empty"><?php echo $this->lang->line('global_loading_data'); ?></td>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="add_edit-modal">
	<form id="add_edit-form" class="form-horizontal" method="post" autocomplete="off">
		<div class="modal-header">
			<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
			<h3></h3>
		</div>
		<div class="modal-body">
			<div class="control-group">
				<label class="control-label"><?php echo $this->lang->line('global_name'); ?></label>
				<div class="controls">
					<input type="text" name="name" id="name" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo $this->lang->line('global_desc'); ?></label>
				<div class="controls">
					<input type="text" name="description" id="description" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo $this->lang->line('groups_parent_name'); ?></label>
				<div class="controls">
					<select name="sys_group_parent_id" id="sys_group_parent_id" class="uniformselect">
					<?php foreach($res_group_parent as $parent): ?>
						<option value="<?php echo $parent['id']; ?>"><?php echo $parent['description']; ?> (<?php echo $parent['name']; ?>)</option>
					<?php endforeach; ?>
					</select>
				</div>
			</div>	
		</div>
		
		<div class="modal-footer">
			<input type="hidden" name="is_ajax" value="1" />
			<button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
			<button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
		</div>
	</form>
</div>
<script>
	jQuery(document).ready(function(){
		var aSelected = [];
		
		// init grid
		var oTable = jQuery('#table1');
		oTable.dataTable({
            "sPaginationType": "bootstrap",
			"aaSorting": [[1,"desc"]],
			"aoColumnDefs": [
				{"bSortable": false, "aTargets": [0,1], "bSearchable": false, "aTargets": [0,1]}
		    ],
			"bFilter" : MODULE_FUNCTION.search ? true : false,
			"bServerSide": true,
			"sAjaxSource": THIS_URL + "/get_list",
			"sServerMethod": "POST",
			"oLanguage": {
				"sLengthMenu": "<?php echo $this->lang->line('global_DT_sLengthMenu'); ?>",
				"sZeroRecords": "<?php echo $this->lang->line('global_DT_sZeroRecords'); ?>",
				"sInfo": "<?php echo $this->lang->line('global_DT_sInfo'); ?>",
				"sInfoEmpty": "<?php echo $this->lang->line('global_DT_sInfoEmpty'); ?>",
				"sInfoFiltered": "(<?php echo $this->lang->line('global_DT_sInfoFiltered'); ?>)",
				"sSearch": "<?php echo $this->lang->line('global_search'); ?> :"
			},
			"fnServerParams": function (aoData) {
				aoData.push({"name": "is_ajax", "value": true});
			},
            "fnDrawCallback": function(oSettings) {
				if(!jQuery(this).children('.blockUI').length){
					App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				}
				aSelected = [];
				jQuery(this).find('input[type=checkbox]').click(function(){
					var id = this.value;
					var checked = jQuery(this).is(':checked');
					fnSelected(this, id, checked);
					fnTbarDisableEnable();
				});
				
				App.unblockUI(jQuery(this));
				fnTbarDisableEnable();
				jQuery('.checkall').parent().removeClass('checked');
				App.initUniform();
            },
			"aoColumns": [
				{ "mData": "cbox" },
				{ "mData": "rnum", "sClass": "hidden-480" },
				{ "mData": "name" },
				{ "mData": "description", "sClass": "hidden-480" },
				{ "mData": "parent_name", "sClass": "hidden-480" }
			],
			"aLengthMenu": [
				[5, 15, 20, 50, 100, -1],
				[5, 15, 20, 50, 100, "All"]
			],
            "iDisplayLength": 5,
			"sDom": "<'dataTables_toolbar'><'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"fnPreDrawCallback": function () { 
				if (jQuery('.dataTables_length select').val() == -1) {
					if(oTable.fnSettings().fnRecordsTotal() > 500) {
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_limit_record_page'); ?>',
							sticky: false,
							time: 3000
						});
						return false;
					}
				}				
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {}
        });
		
		jQuery('#table1_wrapper .dataTables_filter input').addClass("m-wrap");
		jQuery('#table1_wrapper .dataTables_length select').addClass("m-wrap small"); 
		jQuery('#table1_wrapper .dataTables_length select').select2(); 
		
	
		jQuery('.dataTables_filter input').unbind();
		jQuery(".dataTables_filter input").keyup(function (e) {
			if(e.keyCode == 13) {
				App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				oTable.fnFilter(this.value);   
			}
			e.preventDefault();
		});
		
		jQuery('.checkall').click(function(){
			
			var ch = oTable.find('input[type=checkbox]');
			
			var checked = jQuery(this).is(':checked');
			
			ch.each(function(){
				if(this.value != 'on') {
					fnSelected(this, this.value, checked , true);
				}
			});	
			
			fnTbarDisableEnable();
		});
		
		function fnSelected(cmp, id, checked, all){
			var index = jQuery.inArray(id, aSelected);
			if(id == 'on') return;		 
			if ( index === -1 ) {
				if(all){
					if(checked){
						aSelected.push(id);
					}
				}else{
					aSelected.push(id);
				}
			} else {
				if(all){
					if(!checked){
						aSelected.splice(index, 1);
					}
				}else{
					aSelected.splice(index, 1);
				}
			}
			
			if(checked) {	
				jQuery(cmp).attr('checked',true);
				jQuery(cmp).parent().addClass('checked');
				jQuery(cmp).parents('tr').addClass('selected');
			}else{
				jQuery(cmp).attr('checked',false);
				jQuery(cmp).parent().removeClass('checked');
				jQuery(cmp).parents('tr').removeClass('selected');
			}
			
			console.log(aSelected);
		}
		
		function fnTbarDisableEnable(){
			var rowSelected = aSelected.length;
			
			if(rowSelected > 0){
				if(rowSelected > 1){
					jQuery('#button-edit').attr("disabled", "disabled");
				}else{
					jQuery('#button-edit').removeAttr("disabled");
				}
				jQuery('#button-delete').removeAttr("disabled");
			}else{
				jQuery('#button-edit').attr("disabled", "disabled");
				jQuery('#button-delete').attr("disabled", "disabled");
			}
		}
		
		jQuery("div.dataTables_toolbar").html(' '+
		'<button id="button-refresh" class="btn purple"><i class="icon-refresh"></i> <?php echo $this->lang->line('global_refresh'); ?></button>&nbsp;' +
		'<button id="button-add" class="btn green" href="#add_edit-modal" data-toggle="modal" style="visibility:hidden;"><i class="icon-plus"></i></i> <?php echo $this->lang->line('global_add'); ?></button>&nbsp;' +
		'<button disabled="disabled" id="button-edit" class="btn yellow" href="#add_edit-modal" data-toggle="modal" style="visibility:hidden;"><i class="icon-edit"></i> <?php echo $this->lang->line('global_edit'); ?></button>&nbsp;' +
		'<button disabled="disabled" id="button-delete" class="btn red" style="visibility:hidden;"><i class="icon-trash"></i> <?php echo $this->lang->line('global_delete'); ?></button>&nbsp;' +
		' ');
		
		jQuery('#button-refresh').click(function(){
			App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
			oTable.fnDraw();
		});
		
		if(MODULE_FUNCTION.add){
			jQuery("#button-add").css('visibility','visible');
		}
		
		if(MODULE_FUNCTION.edit){
			jQuery("#button-edit").css('visibility','visible');
		}
		
		if(MODULE_FUNCTION.del){
			jQuery("#button-delete").css('visibility','visible');
		}
		
		jQuery('#button-add').click(function(){
			if(jQuery("#id").length > 0) {
				jQuery("#id").remove();  
			}
			jQuery('#add_edit-form')[0].reset();
			jQuery('#name').removeAttr("readOnly");
			
			jQuery('.modal-header h3').html('<?php echo $this->lang->line('global_form_add'); ?>');
		});
		
		jQuery('#button-edit').click(function(){
			
			jQuery('.modal-header h3').html('<?php echo $this->lang->line('global_form_edit'); ?>');
			id = aSelected[0];
			
			var rowIndex = oTable.fnGetPosition(jQuery('#'+id).closest('tr')[0]);
			
			var data = oTable.fnGetData(rowIndex);
			
			jQuery('#name').attr("readOnly","readOnly");
			jQuery('#name').val(data.name);
			jQuery('#description').val(data.description);
			jQuery('#sys_group_parent_id').val(data.sys_group_parent_id);
		
			if(jQuery("#id").length > 0) {
				jQuery("#id").remove();  
			}
			jQuery('#add_edit-form').append('<input type="hidden" id="id" name="id" value="'+id+'" />');
		});
			
		jQuery('#button-delete').click(function(){
			jConfirm('<?php echo $this->lang->line('global_confirm_delete'); ?>', '<?php echo $this->lang->line('global_confirm_delete_header'); ?>', function(r) {
				if(r){
					var ids = { };

					for(var i = 0; i < aSelected.length; i++) {
					  ids[i] = aSelected[i];
					}
					
					var cAjax = new ceki.fnAjax({
						method : '/delete',
						data : ({
							ids: ids,
							is_ajax: 1
						}),
						successCallBack : function() {
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_success_delete'); ?>',
								sticky: false,
								time: 3000
							});
							oTable.fnDraw();
							aSelected = [];
					
							fnTbarDisableEnable();
						}
					});
				}
			});
		});
		
		//init form
		ceki.fnStrNoSpace('name', '_');
		
		var rules = {};
		var messages = {};
		
		rules["name"] = "required";
		messages["name"] = "<?php echo $this->lang->line('global_error_required'); ?>";
		rules["description"] = "required";
		messages["description"] = "<?php echo $this->lang->line('global_error_required'); ?>";
		rules["sys_group_parent_id"] = "required";
		messages["sys_group_parent_id"] = "<?php echo $this->lang->line('global_error_required'); ?>";
		
		jQuery("#add_edit-form").validate({
			onsubmit: false,
			ignore: "",
			rules: rules,
			messages: messages,
			highlight: function(label) {},
			success: function(label) {},
			invalidHandler: function(event, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error_form'); ?>',
						sticky: false,
						time: 3000
					});
					return false;
				}
				event.preventDefault();
			}
		});
		
		jQuery('#add_edit-form').submit(function(e) {
			if(jQuery(this).valid()){
				var cAjax = new ceki.fnAjax({
					method : '/save',
					data : jQuery(this).serialize(),
					successCallBack : function(obj) {
						jQuery('#add_edit-modal').modal('hide');
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_success_update'); ?>',
							sticky: false,
							time: 3000
						});
						oTable.fnDraw();
						
						aSelected = [];
						
						fnTbarDisableEnable();
					},
					successErrorCallBack : function(obj) {
						if(jQuery("#alert").length > 0) {
							jQuery("#alert").remove();  
						}
						msg = '<div id="alert" class="alert alert-error"><button data-dismiss="alert" class="close" type="button"></button>'+obj.message+'</div>';
						jQuery(".modal-header").after(msg);
					}
				});
			}
			e.preventDefault();
		});
	});
</script>