<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
div.tagsinput {
	height: 85px !important;
}

.row_selected {
   background-color: #B0BED9;
}
</style>
<div id="menus">
	<?php echo validation_errors(); ?>
	<?php if($this->session->flashdata('success_alert')): ?>
		<div class="alert alert-success">
			<button data-dismiss="alert" class="close"></button>
			<?php echo $this->session->flashdata('success_alert'); ?>
		</div>
	<?php endif; ?>
	<?php if($this->session->flashdata('error_alert')): ?>
		<div class="alert alert-error">
			<button data-dismiss="alert" class="close"></button>
			<?php echo $this->session->flashdata('error_alert'); ?>
		</div>
	<?php endif; ?>
	
	<?php if($this->site_config['is_active_backend']): ?>
	<div class="portlet box blue" id="box_sys">
		<div class="portlet-title">
			<div class="caption"><i class="icon-reorder"></i><?php echo $this->lang->line('global_backend'); ?></div>
			<div class="tools">
			<?php if(isset($this->site_config['module_function']['add'])): ?>
				<a class="btn mini green" href="#" style="margin-top:-5px;padding-top:7px;" onclick="fnAddEditMenuForm('sys','1');return false;"><i class="icon-plus"></i> <?php echo $this->lang->line('global_add'); ?></a>
			<?php endif; ?>
			</div>
		</div>
		<div class="portlet-body" style="overflow:auto;">
			<div class="span6">
				<div class="dd" id="nestable_backend">
					<ol class="dd-list" id="sys">
						<?php echo $menu_backend_config; ?>
					</ol>
				</div>	
			</div>
			<div class="span6" id="form_sys">
				
			</div>
		</div>
		<input type="hidden" id="sys_current_id" />
	</div>
	<?php endif; ?>
										
	<?php if($this->site_config['is_active_frontend']): ?>
	<div class="portlet box blue" id="box_app">
		<div class="portlet-title">
			<div class="caption"><i class="icon-reorder"></i><?php echo $this->lang->line('global_frontend'); ?></div>
			<div class="tools">
			<?php if(isset($this->site_config['module_function']['add'])): ?>
				<a class="btn mini" href="#" style="margin-top:-5px;padding-top:7px;" onclick="fnAddEditMenuForm('app','1');return false;"><i class="icon-plus"></i> <?php echo $this->lang->line('global_add'); ?></a>
			<?php endif; ?>
			</div>
		</div>
		<div class="portlet-body" style="overflow:auto;">
			<div class="span6">
				<div class="dd" id="nestable_frontend">
					<ol class="dd-list" id="app">
						<?php echo $menu_frontend_config; ?>
					</ol>
				</div>	
			</div>
			<div class="span6" id="form_app"></div>
		</div>
		<input type="hidden" id="app_current_id" />
	</div>
	<?php endif; ?>
</div>

<script>
	jQuery(document).ready(function(){
		 var updateOutput = function (e) {
			var list = e.length ? e : jQuery(e.target);
			if (window.JSON) {
				var cAjax = new ceki.fnAjax({
					method : '/set_menu_order/sys',
					data : ({
						data: window.JSON.stringify(list.nestable('serialize')),
						is_ajax: 1
					}),
					successCallBack : function(obj) {
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_success_update'); ?>',
							sticky: false,
							time: 3000
						});
					}
				});
			}
		};

		jQuery('#nestable_backend').nestable().on('change', updateOutput);
		jQuery('#nestable_backend').nestable('collapseAll');
		jQuery('#nestable_frontend').nestable().on('change', updateOutput);
		
		jQuery('.dd3-content').on('click', function() {
			var parentId = jQuery(this).parents('div')[0].id;
			
			var hasClass = false;
			if(jQuery(this).hasClass('row_selected')) {
				hasClass = true;
			}
			
			jQuery('#'+parentId).children().find('div').each(function (){
				jQuery(this).removeClass('row_selected');
			});
			
			var mode = jQuery(this).attr('data-mode');
			if(!hasClass) {
				jQuery(this).addClass('row_selected');
			
				var id = jQuery(this).attr('data-id');
				
				
				jQuery('#'+mode+'_current_id').val(id);
				ceki.fnLoading('form_'+mode);
				//ceki.fnScrollTo('box_'+mode);
				var cAjax = new ceki.fnAjax({
					method : '/get_menu_detail/' + mode,
					data : ({
						id: id,
						is_ajax: 1
					}),
					successCallBack : function(obj) {
						jQuery('#form_'+mode).html(obj.message).fadeIn("slow");
						jQuery('#form_'+mode).css('opacity', 0);
						jQuery('#form_'+mode).animate({
							opacity : 1
						}, 3000);
					}
				});
			} else {
				jQuery('#'+mode+'_current_id').val('');
				jQuery('#form_'+mode).hide();
			}
		});
	});
	
	function fnDeleteMenu(mode, id){
		jConfirm('<?php echo $this->lang->line('global_confirm_delete'); ?>', '<?php echo $this->lang->line('global_confirm_delete_header'); ?>', function(r) {
			if(r){
				var cAjax = new ceki.fnAjax({
					method : '/delete_menu/' + mode,
					data : ({
						id: id,
						is_ajax: 1
					}),
					successCallBack : function(obj) {
						ceki.fnRefresh();
					}
				});
			}
		});
	}
	
	function fnAddEditMenuForm(mode, add){
		ceki.fnLoading('form_'+mode);
		//ceki.fnScrollTo('box_'+mode);
		var id = jQuery('#'+mode+'_current_id').val();
		var add = typeof add != 'undefined' ? 1 : "";
		var cAjax = new ceki.fnAjax({
			method : '/add_edit_menu_form/' + mode + '/' + add,
			data : ({
				id: (typeof id != 'undefined') ? id : '',
				is_ajax: 1
			}),
			successCallBack : function(obj) {
				
				jQuery('#form_'+mode).html(obj.message).fadeIn("slow");
				jQuery('#form_'+mode).css('opacity', 0);
				jQuery('#form_'+mode).animate({
					opacity : 1
				}, 3000);
				
				// tag input widget
				jQuery('#'+mode+'_function').tagsInput({
					defaultText : '<?php echo $this->lang->line('menus_add_function'); ?>'
				});
				
				var rules = {};
				var messages = {};
				
				rules[mode+"_menu_link_location"] = "required";
				messages[mode+"_menu_link_location"] = "<?php echo $this->lang->line('global_error_required'); ?>";
				rules[mode+"_menu_name"] = "required";
				messages[mode+"_menu_name"] = "<?php echo $this->lang->line('global_error_required'); ?>";
				
				for(i=0;i<LANGUAGE['LANG_ARRAY'].length;i++)
				{
					rules[mode+"_menu_lang_desc_"+LANGUAGE['LANG_ARRAY'][i]['id']] = "required";
					messages[mode+"_menu_lang_desc_"+LANGUAGE['LANG_ARRAY'][i]['id']] = "<?php echo $this->lang->line('global_error_required'); ?>";
				}
				
				jQuery("#form_"+mode+"_menu").validate({
					ignore: "",
					rules: rules,
					messages: messages,
					highlight: function(label) {},
					success: function(label) {},
					invalidHandler: function(event, validator) {
						var errors = validator.numberOfInvalids();
						if (errors) {
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_error_form'); ?>',
								sticky: false,
								time: 3000
							});
						}
					}
				});
				
				jQuery('input[name='+mode+'_menu_link_type]').click(function(){
					var val = jQuery(this).val();
					var substr = '';
					if(val == 2){
						substr = 'http://';
					}
					jQuery('#'+mode+'_menu_link_location').val(substr);
				});
				
				ceki.fnStrNoSpace(mode+'_menu_name', '_');
				ceki.fnStrNoSpace(mode+'_menu_link_location', '-');
			}
		});
	}
</script>