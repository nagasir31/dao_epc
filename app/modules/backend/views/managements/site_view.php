<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
body .modal {
  width: 90%; /* desired relative width */
  left: 5%; /* (100%-width)/2 */
  /* place center */
  margin-left:auto;
  margin-right:auto; 
}
</style>
<div id="main-container">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-reorder"></i><?php echo $this->lang->line('global_application_managements'); ?></div>
		</div>
		<div class="portlet-body form">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('success_alert')): ?>
				<div class="alert alert-success">
					<button data-dismiss="alert" class="close" type="button">&times;</button>
					<?php echo $this->session->flashdata('success_alert'); ?>
				</div>
			<?php endif; ?>
			<?php if($this->session->flashdata('error_alert')): ?>
				<div class="alert alert-error">
					<button data-dismiss="alert" class="close" type="button">&times;</button>
					<?php echo $this->session->flashdata('error_alert'); ?>
				</div>
			<?php endif; ?>
			<div id="alertdiv"></div>
			
			<div class="tabbable tabbable-custom">
				<form id="add_edit-form" class="form-horizontal" method="post" autocomplete="off" action="#">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#tab_1_1"><?php echo $this->lang->line('global_general'); ?></a></li>
						<li class=""><a data-toggle="tab" href="#tab_1_2"><?php echo $this->lang->line('global_sync_data_sdm'); ?></a></li>
                        <li class=""><a data-toggle="tab" href="#tab_1_3">Sinkronisasi Data Vendor<?php echo isset($vendor_log) ? " | ".$vendor_log: "" ?></a></li>
					</ul>
					<div class="tab-content">
						<div id="tab_1_1" class="tab-pane active">
							<div class="span6">
								<!--div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('site_app_logo'); ?></label>
									<div class="controls">
										<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
											<img src="<?php echo app_asset_url("global/images/".$this->site_config['app_logo']); ?>" alt="" />
										</div>
										<br>
										<span style="margin-left:0px;"><a class="btn btn-file" href="#filemanagerModal" data-toggle="modal" id="btn_fileManager"> <i class="iconfa-camera"></i> &nbsp; <?php echo strtoupper($this->lang->line('global_change')); ?></a></span>
									</div>
									
									<label class="control-label"><?php echo $this->lang->line('site_app_logo'); ?></label>
									<div class="controls">
										<div class="fileupload fileupload-new" data-provides="fileupload">
											<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
												<img src="<?php echo app_asset_url("global/images/".$this->site_config['app_logo']); ?>" alt="" />
											</div>
											<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
											<div>
												<span class="btn btn-file"><span class="fileupload-new"><?php echo $this->lang->line('global_select_image'); ?></span>
												<span class="fileupload-exists"><?php echo $this->lang->line('global_change'); ?></span>
												<input type="file" class="default" /></span>
												<a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><?php echo $this->lang->line('global_remove'); ?></a>
											</div>
										</div>
									</div>
								</div-->
								
								<div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('site_app_name'); ?></label>
									<div class="controls">
										<input type="text" id="app_name" name="app_name" value="<?php echo $this->site_config['app_name']; ?>" class="m-wrap span12" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('site_app_author'); ?></label>
									<div class="controls">
										<input type="text" id="app_author" name="app_author" value="<?php echo $this->site_config['app_author']; ?>" class="m-wrap span12" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('global_address'); ?></label>
									<div class="controls">
										<textarea id="company_address" name="company_address"  class="tinymce2"><?php echo $this->site_config['company_address']; ?></textarea>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('site_is_backend_online'); ?></label>
									<div class="controls">
										<input id="is_backend_online" name="is_backend_online" class="switch" type="checkbox" data-on-text="<?php echo $this->lang->line('global_yes'); ?>" data-off-text="<?php echo $this->lang->line('global_no'); ?>" <?php echo $this->site_config['is_backend_online'] == 1 ? "checked": ""; ?>  />
									</div>
								</div>
								
								<div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('site_is_email_system_active'); ?></label>
									<div class="controls">
										<input id="is_email_system_active" name="is_email_system_active" class="switch" type="checkbox" data-on-text="<?php echo $this->lang->line('global_yes'); ?>" data-off-text="<?php echo $this->lang->line('global_no'); ?>" <?php echo $this->site_config['is_email_system_active'] == 1 ? "checked": ""; ?>  />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('site_app_email'); ?></label>
									<div class="controls">
										<input type="text" id="app_admin_email" name="app_admin_email" value="<?php echo $this->site_config['app_admin_email']; ?>" class="m-wrap span12" />
									</div>
								</div>
								
								<div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('site_is_email_test_active'); ?></label>
									<div class="controls">
										<input id="is_email_test_active" name="is_email_test_active" class="switch" type="checkbox" data-on-text="<?php echo $this->lang->line('global_yes'); ?>" data-off-text="<?php echo $this->lang->line('global_no'); ?>" <?php echo $this->site_config['is_email_test_active'] == 1 ? "checked": ""; ?>  />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('site_email_test'); ?></label>
									<div class="controls">
										<input type="text" id="email_test" name="email_test" value="<?php echo $this->site_config['email_test']; ?>" class="m-wrap span12" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"><?php echo $this->lang->line('global_email_protocol'); ?></label>
									<div class="controls">
										<select id="email_protocol" name="email_protocol" class="m-wrap span12">
											<option <?php echo $this->site_config['email_protocol'] == 'mail' ? 'selected="selected"': ''; ?> value="mail">mail</option>
											<option <?php echo $this->site_config['email_protocol'] == 'sendmail' ? 'selected="selected"': ''; ?> value="sendmail">sendmail</option>
										</select>
									</div>
								</div>
								<!--div class="control-group">
									<label class="control-label">Real time notifikasi</label>
									<div class="controls">
										<input id="real_time_notif" name="real_time_notif" class="switch" type="checkbox" data-on-text="<?php echo $this->lang->line('global_yes'); ?>" data-off-text="<?php echo $this->lang->line('global_no'); ?>" <?php echo $this->site_config['real_time_notif'] == 1 ? "checked": ""; ?>  />
									</div>
								</div-->
							</div>
							
							<div class="span6">
								<div id="accordion2" class="accordion scrollable in collapse" style="height: auto;">
									<div class="accordion-group">
										<div class="accordion-heading">
											<a href="#collapse_2_1" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle">
											<i class="icon-angle-left"></i>
											<?php echo $this->lang->line('berkas_verify_email'); ?>
											</a>
										</div>
										<div class="accordion-body in collapse" id="collapse_2_1" style="height: auto;">
											<div class="control-group">
												<br>
												<label class="control-label" style="width:100px"><?php echo $this->lang->line('global_subject'); ?></label>
												<div class="controls" style="margin-left:150px">
													<input type="text" id="email_template_verify_berkas_subject" name="email_template_verify_berkas_subject" value="<?php echo $this->site_config['email_template_verify_berkas_subject']; ?>" class="m-wrap span8" />
												</div>
											</div>
											<div class="accordion-inner">
												<textarea cols="100" rows="15" style="width: 100%;height:100%" id="email_template_verify_berkas" name="email_template_verify_berkas" class="tinymce"><?php echo $this->site_config['email_template_verify_berkas']; ?></textarea>
											</div>
										</div>
									</div>
									<div class="accordion-group">
										<div class="accordion-heading">
											<a href="#collapse_2_2" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle collapsed">
											<i class="icon-angle-left"></i>
											<?php echo $this->lang->line('berkas_approval_email'); ?>
											</a>
										</div>
										<div class="accordion-body collapse" id="collapse_2_2">
											<div class="accordion-inner">
												<div class="control-group">
													<br>
													<label class="control-label" style="width:100px"><?php echo $this->lang->line('global_subject'); ?></label>
													<div class="controls" style="margin-left:150px">
														<input type="text" id="email_template_complete_berkas_subject" name="email_template_complete_berkas_subject" value="<?php echo $this->site_config['email_template_complete_berkas_subject']; ?>" class="m-wrap span8" />
													</div>
												</div>
												<div class="accordion-inner">
													<textarea cols="100" rows="15" style="width: 100%;height:100%" id="email_template_complete_berkas" name="email_template_complete_berkas" class="tinymce"><?php echo $this->site_config['email_template_complete_berkas']; ?></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="accordion-group">
										<div class="accordion-heading">
											<a href="#collapse_2_3" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle collapsed">
											<i class="icon-angle-left"></i>
											<?php echo $this->lang->line('berkas_rejected_email'); ?>
											</a>
										</div>
										<div class="accordion-body collapse" id="collapse_2_3">
											<div class="accordion-inner">
												<div class="control-group">
													<br>
													<label class="control-label" style="width:100px"><?php echo $this->lang->line('global_subject'); ?></label>
													<div class="controls" style="margin-left:150px">
														<input type="text" id="email_template_rejected_berkas_subject" name="email_template_rejected_berkas_subject" value="<?php echo $this->site_config['email_template_rejected_berkas_subject']; ?>" class="m-wrap span8" />
													</div>
												</div>
												<div class="accordion-inner">
													<textarea cols="100" rows="15" style="width: 100%;height:100%" id="email_template_rejected_berkas" name="email_template_rejected_berkas" class="tinymce"><?php echo $this->site_config['email_template_rejected_berkas']; ?></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					
						<div id="tab_1_2" class="tab-pane">
							<div class="span12">
								<h4>References</h4>
								<div id="prog1">
									<div class="progress progress-striped active">
										<div class="bar" style="width: 0%;"><span class="msg_percentage" style="font-weight:bold;"></span></div>
									</div>
									<div>
										<span class="msg_bar"></span>
									</div>
								</div>
								<div class="clearfix space10"></div>
								<h4>Employees</h4>
								<div id="prog2">
									<div class="progress progress-striped active">
										<div class="bar" style="width: 0%;"><span class="msg_percentage" style="font-weight:bold;"></span></div>
									</div>
									<div>
										<span class="msg_bar"></span>
									</div>
								</div>
								<div class="clearfix space10"></div>
								<h4>Users</h4>
								<div id="prog3">
									<div class="progress progress-striped active">
										<div class="bar" style="width: 0%;"><span class="msg_percentage" style="font-weight:bold;"></span></div>
									</div>
									<div>
										<span class="msg_bar"></span>
									</div>
								</div>
								<br><br>
							</div>
						</div>
                        <div id="tab_1_3" class="tab-pane">
                            <div class="span12">
                                <div id="prog4">
                                    <div class="progress progress-striped active">
                                        <div class="bar" style="width: 0%;"><span class="msg_percentage" style="font-weight:bold;"></span></div>
                                    </div>
                                    <div>
                                        <span class="msg_bar"></span>
                                    </div>
                                </div>
                                <div class="clearfix space10"></div>
                                <br></br>
                            </div>
                        </div>
					</div>
					<div class="form-actions center">
						<input type="hidden" name="is_ajax" value="1" />
						<input type="hidden" id="image_src" name="app_logo" value="<?php echo $this->site_config['app_logo']; ?>" onchange="ceki.fnSetSrc('image_src','logo');" />
						<button id="btn-save" type="submit" class="btn submit blue"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
						<button id="btn-sync" type="button" class="btn green" style="display:none;"><i class="icon-refresh"></i> <?php echo $this->lang->line('global_sync_data'); ?></button>
                        <button id="btn-sync-vendor" type="button" class="btn green" style="display:none;"><i class="icon-refresh"></i> <?php echo $this->lang->line('global_sync_data'); ?> Vendor</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade in" id="filemanagerModal" style="">
	<div class="modal-header">
			<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
			<h3><?php echo $this->lang->line('global_file_manager'); ?></h3>
	</div>
	<div class="modal-body">
		<iframe id="iframe_fileManager" width="100%" height="500px" frameborder="0" src=""> </iframe>
	</div>
	<div class="modal-footer"></div>
</div>
<script>
var timer1, timer2, timer3; 
var timer = {
	'1': null,
	'2': null,
	'3': null
};
jQuery(document).ready(function(){
	jQuery('#real_time_notif').live('switchChange.bootstrapSwitch', function (e, data) {
		//console.log(data);
		if(!data){
			//ceki.sockClose();
		} 
	});
	jQuery(".switch").bootstrapSwitch();
	
	jQuery("a[href='#tab_1_2']").on('show.bs.tab', function(e) {
		jQuery('#btn-save').hide();
		jQuery('#btn-sync').show();
        jQuery('#btn-sync-vendor').hide();
	});
	
	jQuery("a[href='#tab_1_1']").on('show.bs.tab', function(e) {
		jQuery('#btn-save').show();
		jQuery('#btn-sync').hide();
        jQuery('#btn-sync-vendor').hide();
	});


    jQuery("a[href='#tab_1_3']").on('show.bs.tab', function(e) {
        jQuery('#btn-save').hide();
        jQuery('#btn-sync').hide();
        jQuery('#btn-sync-vendor').show();
    });

	jQuery('textarea.tinymce').tinymce({
		script_url : APP_ASSET_BACKEND_URL + 'plugins/tinymce/tinymce.min.js',
		selector: "textarea",
		convert_urls: false,
		theme: "modern",
		width: '100%',
		height: 250, 
		plugins: [ "advlist autolink link image lists charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking", "table contextmenu directionality emoticons paste textcolor responsivefilemanager", "code"], 
		toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | fontsizeselect", 
		toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor | print preview code ", 
		image_advtab: true, 
		external_filemanager_path: APP_BACKEND_URL + "filemanager", 
		filemanager_title: "<?php echo $this->lang->line('global_filemanager'); ?>" , 
		external_plugins: { "filemanager" : APP_ASSET_BACKEND_URL + "plugins/responsiveFilemanager/plugin.min.js"}
	});
	
	jQuery('textarea.tinymce2').tinymce({
		script_url : APP_ASSET_BACKEND_URL + 'plugins/tinymce/tinymce.min.js',
		selector: "textarea",
		convert_urls: false,
		theme: "modern",
		width: '100%',
		height: 90,
		styles: {
			'float' : 'right', 
			'margin': '100px;'
		},
		style_margin_left_measurement: 200
	});
	
	var rules = {};
	var messages = {};
	
	rules["app_name"] = { required: true, minlength: 5 };
	messages["app_name"] = { required: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('site_app_name')); ?>", minlength: "<?php echo sprintf($this->lang->line('global_error_minlength_field'),$this->lang->line('site_app_name'),5); ?>" };
	rules["app_author"] = { required: true, minlength: 5 };
	messages["app_author"] = { required: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('site_app_author')); ?>", minlength: "<?php echo sprintf($this->lang->line('global_error_minlength_field'),$this->lang->line('site_app_author'),5); ?>" };
	rules["app_admin_email"] =  { required: true, email: true };
	messages["app_admin_email"] =  { required: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('site_app_admin_email')); ?>", email: "<?php echo $this->lang->line('global_email_invalid'); ?>" };
	rules["company_address"] = { required: true, minlength: 5 };
	messages["company_address"] = { required: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('global_address')); ?>", minlength: "<?php echo sprintf($this->lang->line('global_error_minlength_field'),$this->lang->line('global_address'),5); ?>" };
	rules["email_test"] =  { required: true, email: true };
	messages["email_test"] =  { required: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('site_email_test')); ?>", email: "<?php echo $this->lang->line('global_email_invalid'); ?>" };
	
	jQuery("#add_edit-form").validate({
		onsubmit: false,
		ignore: "",
		rules: rules,
		messages: messages,
		highlight: function(el) {},
		success: function(label) {},
		invalidHandler: function(e, validator) {
			var errors = validator.numberOfInvalids();
			if (errors) {
				jQuery.gritter.add({
					title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
					text: "<?php echo $this->lang->line('global_error_form'); ?>",
					sticky: false,
					time: 3000
				});
			}
			e.preventDefault();
		}
	});
	
	jQuery('#add_edit-form').submit(function(e) {
		if(jQuery(this).valid()){
			if(tinyMCE.get('email_template_verify_berkas').getContent() == '') {
				jQuery.gritter.add({
					title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
					text: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('berkas_verify_email')); ?>",
					sticky: false,
					time: 3000
				});
				return false;
			} 
			
			if(tinyMCE.get('email_template_complete_berkas').getContent() == '') {
				jQuery.gritter.add({
					title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
					text: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('berkas_approval_email')); ?>",
					sticky: false,
					time: 3000
				});
				return false;
			} 
			
			if(tinyMCE.get('email_template_rejected_berkas').getContent() == '') {
				jQuery.gritter.add({
					title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
					text: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('berkas_rejected_email')); ?>",
					sticky: false,
					time: 3000
				});
				return false;
			} 
			
			App.blockUI(jQuery("#main-container"), true, '<?php echo $this->lang->line('global_saving_data'); ?>');
			var cAjax = new ceki.fnAjax({
				method : '/save',
				data : jQuery(this).serialize({checkboxesAsBools: true}),
				successCallBack : function() {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_success_save'); ?>',
						sticky: false,
						time: 3000
					});
			
					if(jQuery("#alert").length > 0) {
						jQuery("#alert").remove();  
					}
					msg = '<div id="alert" class="alert alert-success"><button data-dismiss="alert" class="close" type="button"></button><?php echo $this->lang->line('global_success_update'); ?></div>';
					jQuery("#alertdiv").append(msg);
					App.unblockUI(jQuery("#main-container"));
				},
				successErrorCallBack : function(obj) {
					if(jQuery("#alert").length > 0) {
						jQuery("#alert").remove();  
					}
					msg = '<div id="alert" class="alert alert-error"><button data-dismiss="alert" class="close" type="button"></button>'+obj.message+'</div>';
					jQuery("#alertdiv").append(msg);
					App.unblockUI(jQuery("#main-container"));
				}
			});		
		}
		e.preventDefault();
	});

	jQuery('#btn_fileManager').click(function(){
		jQuery('#iframe_fileManager').prop('src', APP_BACKEND_URL + 'filemanager?type=1&field_id=image_src');
	});
	
	jQuery('#btn-sync').click(function() {
		jConfirm('<?php echo $this->lang->line('global_sync_confirm'); ?>', '<?php echo $this->lang->line('global_sync_data_sdm'); ?>', function(r) {
			if(r){
				jQuery('#btn-sync').attr("disabled","disabled");
				jQuery("#btn-sync").text('Sedang diproses, mohon tunggu..');
				
				jQuery.gritter.add({
					title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
					text: '<?php echo $this->lang->line('global_sync_notif_process'); ?>',
					sticky: false,
					time: 7000
				});
				
				sync_data('references_sync',1);
				timer[1] = window.setInterval(function(){
					sync_checker('references',1);
				}, 1000);
			}
		});
	});
	
	function sync_checker(apiModul,id) {
		var cAjax = new ceki.fnAjax({
			url : "<?php echo site_url('api/client/sync_checker'); ?>",
			data : {
				'sess_id' : SESS_ID,
				'api_module': apiModul
			},
			successCallBack : function(data) {
				if(data.percent != null) {
					$('#prog'+id).find('.bar').css({
						width: data.percent + '%'
					});
					
					$('#prog'+id).find(".msg_percentage").html(data.percent + '%');
					$('#prog'+id).find(".msg_bar").html(data.message);
				}
				
				if (data.percent >= 100) {
					window.clearInterval(timer[id]);
					$('#prog'+id).find(".msg_bar").html("<span class='done' style='color:#35aa47'><?php echo $this->lang->line('global_end'); ?> <i class='icon-ok'></i></span>");
					$('#prog'+id).find(".progress-striped").removeClass('active');
					
					sync_check_complete();
				 }
			},
			successErrorCallBack : function(obj) {
				console.log(apiModul+' error');
			}
		});
	}
	
	function sync_check_complete() {
		if($("#tab_1_2").find(".icon-ok").length == 3){
			jQuery.gritter.add({
				title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
				text: '<?php echo $this->lang->line('global_sync_all_done'); ?>',
				sticky: true,
				time: 7000
			});
			
			jQuery('#btn-sync').removeAttr("disabled");
			jQuery("#btn-sync").text('<?php echo $this->lang->line('global_sync_data'); ?>');
		
			jQuery('.bar').css({width: '0%'});
			jQuery('.progress.progress-striped').addClass('active');
			jQuery(".msg_percentage").html('');
			jQuery(".msg_bar").html('');
		}
	}		
	
	function sync_data(module,id) {
		var cAjax = new ceki.fnAjax({
			url : "<?php echo site_url('api/client'); ?>/"+module,
			data : {
				'sess_id' : SESS_ID
			},
			successCallBack : function(obj) {
				var nextId = id+1;
				if(nextId == 2){
					sync_data('employees_sync',2);
					timer[2] = window.setInterval(function() {
						sync_checker('employees',2);
					}, 1000);
				}
				
				if(nextId == 3){
					sync_data('users_sync',3);
					timer[3] = window.setInterval(function() {
						sync_checker('users',3);
					}, 1000);
				}
				
				jQuery.gritter.add({
					title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
					text: obj.message,
					sticky: false,
					time: 7000
				});
			},
			successErrorCallBack : function(obj) {
				window.clearInterval(timer[id]);
				alert(module+': '+obj.message);
				
				jQuery('#btn-sync').removeAttr("disabled");
				jQuery("#btn-sync").text('<?php echo $this->lang->line('global_sync_data'); ?>');
			}
		});
		
	}
	
	function users_sync() {
		var cAjax = new ceki.fnAjax({
			url : "<?php echo site_url('api/client/users_sync'); ?>",
			data : {
				'sess_id' : SESS_ID
			},
			successCallBack : function(obj) {
				jQuery.gritter.add({
					title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
					text: obj.message,
					sticky: true,
					time: 3000
				});
			},
			successErrorCallBack : function(obj) {
				window.clearInterval(timer[1]);
				alert('Users: '+obj.message);
			}
		});
		
	}
	
	function employees_sync() {
		var cAjax = new ceki.fnAjax({
			url : "<?php echo site_url('api/client/employees_sync'); ?>",
			data : {
				'sess_id' : SESS_ID
			},
			successCallBack : function(obj) {
				jQuery.gritter.add({
					title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
					text: obj.message,
					sticky: true,
					time: 3000
				});
			},
			successErrorCallBack : function(obj) {
				window.clearInterval(timer[2]);
				alert('Employees: '+obj.message);
			}
		});
		
	}
	
	function references_sync() {
		var cAjax = new ceki.fnAjax({
			url : "<?php echo site_url('api/client/references_sync'); ?>",
			data : {
				'sess_id' : SESS_ID
			},
			successCallBack : function(obj) {
				jQuery.gritter.add({
					title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
					text: obj.message,
					sticky: true,
					time: 3000
				});
			},
			successErrorCallBack : function(obj) {
				window.clearInterval(timer[3]);
				alert('References: '+obj.message);
			}
		});
	}


    jQuery('#btn-sync-vendor').click(function() {
        jConfirm('<?php echo $this->lang->line('global_sync_confirm'); ?>', 'Sinkronisasi Data Vendor', function(r) {
            if(r){
                jQuery('#btn-sync-vendor').attr("disabled","disabled");
                jQuery("#btn-sync-vendor").text('Sedang diproses, mohon tunggu..');

                $('#prog4').find('.bar').css({
                    width: '100%'
                });

                $('#prog4').find('.msg_bar').html('Dalam Proses...')
                $('#prog4').find('.progress').addClass('active');

                jQuery.gritter.add({
                    title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                    text: '<?php echo $this->lang->line('global_sync_notif_process'); ?>',
                    sticky: false,
                    time: 7000
                });

                vendor_sync()
            }
        });
    });

    function vendor_sync() {
        var cAjax = new ceki.fnAjax({
            url : "<?php echo site_url('api/client/sap_ref_sync?mod=listvendor'); ?>",
            data : {
                'sess_id' : SESS_ID
            },
            successCallBack : function(obj) {
                var dAjax = new ceki.fnAjax({
                    url : "<?php echo site_url('api/client/sap_ref_sync?mod=partnerbanktype'); ?>",
                    data : {
                        'sess_id' : SESS_ID
                    },
                    successCallBack : function(obj) {
                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: obj.message,
                            sticky: true,
                            time: 3000
                        });

                        jQuery('#btn-sync-vendor').removeAttr("disabled");
                        jQuery("#btn-sync-vendor").html('<i class="icon-refresh"></i>&nbsp;Mulai Sinkronisasi');

                        $('#prog4').find('.msg_bar').html(obj.success ? 'Sukses' : 'Gagal - ' + obj.message);
                        $('#prog4').find('.progress').removeClass('active');

                    },
                    successErrorCallBack : function(obj) {
                        window.clearInterval(timer[3]);
                        alert('Data Bank: '+obj.message);

                        jQuery('#btn-sync-vendor').removeAttr("disabled");
                        jQuery("#btn-sync-vendor").text('<i class="icon-refresh"></i>&nbsp;Mulai Sinkronisasi');
                        $('#prog4').find('.msg_bar').html(obj.success ? 'Sukses' : 'Gagal - ' + obj.message);
                        $('#prog4').find('.progress').removeClass('active');
                    }
                });
            },
            successErrorCallBack : function(obj) {
                window.clearInterval(timer[3]);
                alert('Data Vendor: '+obj.message);

                jQuery('#btn-sync-vendor').removeAttr("disabled");
                jQuery("#btn-sync-vendor").text('<i class="icon-refresh"></i>&nbsp;Mulai Sinkronisasi');
                $('#prog4').find('.msg_bar').html(obj.success ? 'Sukses' : 'Gagal - ' + obj.message);
                $('#prog4').find('.progress').removeClass('active');
            }
        });
    };
});
</script>