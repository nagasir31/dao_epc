<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
body .modal#add_edit-modal {
  width: 40%; 
  left: 30%;
  margin-left:auto;
  margin-right:auto; 
}
</style>
<div class="row-fluid">
	<div class="span12 responsive" data-tablet="span12 fix-offset" data-desktop="span12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="icon-reorder"></i><?php echo $this->lang->line('global_list'); ?></div>
			</div>
			<div class="portlet-body">
				<table class="table table-bordered table-hover" id="table1">
					<thead>
						<tr>
							<th style="width:2%;text-align:center;"><input type="checkbox" class="checkall" data-set="#table1 .checkboxes" /></th>
							<th style="width:2%;" class="hidden-480"><?php echo $this->lang->line('global_no'); ?></th>
							<th style="width:10%;" ><?php echo $this->lang->line('users_name'); ?></th>
							<th style="width:5%;" class="hidden-480"><?php echo $this->lang->line('users_group_name'); ?></th>
							<th style="width:15%;" class="hidden-480"><?php echo $this->lang->line('employee_name'); ?></th>
							<th style="width:5%;" class="hidden-480"><?php echo $this->lang->line('employee_nrp'); ?></th>
							<th style="width:5%;" class="hidden-480 hidden-480 hidden-767"><?php echo $this->lang->line('employee_position'); ?></th>
							<th style="width:3%;" class="hidden-480 hidden-480 hidden-767"><?php echo $this->lang->line('employee_division'); ?></th>
							<th style="width:3%;" class="hidden-480 hidden-767 hidden-979"><?php echo $this->lang->line('employee_branch'); ?></th>
							<th style="width:5%;" class="hidden-480 hidden-767 hidden-979"><?php echo $this->lang->line('employee_project_code'); ?></th>
							<th style="width:5%;" class="hidden-480 hidden-767 hidden-979"><?php echo $this->lang->line('global_email'); ?></th>
							<th style="width:3%;text-align:center;"><?php echo $this->lang->line('global_active'); ?></th>
						</tr>
					</thead>
					<tbody>
						<td colspan="11" class="dataTables_empty"><?php echo $this->lang->line('global_loading_data'); ?></td>
					</tbody>
					<tfoot>
						<tr>
							<td ><input type="hidden" /></td>
							<td ><input type="hidden" /></td>
							<td ><input type="text" id="search_name" name="search_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_group_name" name="search_group_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_employee_name" name="search_employee_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_employee_nrp" name="search_employee_nrp" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_employee_jabatan" name="search_employee_jabatan" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_employee_divisi" name="search_employee_divisi" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_employee_cabang" name="search_employee_cabang" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_employee_ref_proyek_id" name="search_employee_ref_proyek_id" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_employee_email" name="search_employee_email" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td ><input type="text" id="search_is_active" name="search_is_active" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="add_edit-modal">
	<form id="add_edit-form" class="form-horizontal" method="post" autocomplete="off">
		<div class="modal-header">
			<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
			<h3></h3>
		</div>
		<div class="modal-body">
			<div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
				<div class="row-fluid">
					<div class="span12">
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('users_name'); ?></label>
							<div class="controls">
								<input type="text" name="name" id="name" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('users_password'); ?></label>
							<div class="controls">
								<input type="password" name="password" id="password" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('users_password_conf'); ?></label>
							<div class="controls">
								<input type="password" name="password_conf" id="password_conf" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><label class="control-label"><?php echo $this->lang->line('users_active'); ?></label></label>
							<div class="controls">                                                
								<label class="radio">
									<div class="radio">
										<span>
											<input type="radio" name="is_active" id="is_active1" value="1" checked="checked">
										</span>
									</div>
									<?php echo $this->lang->line('global_yes'); ?>
								</label>
								<label class="radio">
									<div class="radio">
										<span>
											<input type="radio" name="is_active" id="is_active0" value="0">
										</span>
									</div>
									<?php echo $this->lang->line('global_no'); ?>
								</label>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('users_group_name'); ?></label>
							<div class="controls">
								<select name="sys_group_child_id" id="sys_group_child_id" class="uniformselect">
								<option value=""><?php echo $this->lang->line('global_none'); ?></option>
								<?php foreach($res_group_child as $child): ?>
									<option value="<?php echo $child['id']; ?>"><?php echo $child['description']; ?> (<?php echo $child['name']; ?>)</option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>	
						
						<div class="control-group" style="display:block;" id="control_proyeks">
							<label class="control-label"><?php echo $this->lang->line('berkas_project'); ?></label>
							<div class="controls">
								<select id="proyeks" name="proyeks[]" data-placeholder="<?php echo $this->lang->line('global_select'); ?> <?php echo $this->lang->line('berkas_project'); ?>" class="chosen"  multiple="multiple">
									<?php foreach($ref_proyek as $proyek): ?>
									<option id="<?php echo $proyek['id']; ?>" value="<?php echo $proyek['id']; ?>">(<?php echo $proyek['id']; ?>) <?php echo $proyek['name']; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('global_signature2'); ?></label>
							<div class="controls">
								<div data-provides="fileupload" class="fileupload fileupload-new">
									<div class="input-append">
										<div class="uneditable-input">
											<i class="icon-file fileupload-exists"></i> 
											<span class="fileupload-preview"></span>
										</div>
										<span class="btn btn-file">
										<span class="fileupload-new"><?php echo $this->lang->line('global_select_file'); ?></span>
										<span class="fileupload-exists"><?php echo $this->lang->line('global_change'); ?></span>
										<input type="file" class="default" name="signature">
										</span>
										<a data-dismiss="fileupload" class="btn fileupload-exists" href="#"><?php echo $this->lang->line('global_remove'); ?></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal-footer">
			<input type="hidden" name="is_ajax" value="1" />
			<button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
			<button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
		</div>
	</form>
</div>
<script>
	jQuery(document).ready(function(){
		
		fnEmptyForm();
		var aSelected = [];
		var asInitVals = [];
		
		// init grid
		var oTable = jQuery('#table1');
		App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
		oTable.dataTable({
            "sPaginationType": "bootstrap",
			"aaSorting": [[1,"desc"]],
			"aoColumnDefs": [
				{"bSortable": false, "aTargets": [0,1], "bSearchable": false, "aTargets": [0,1]}
		    ],
			"bFilter" : MODULE_FUNCTION.search ? true : false,
			"bServerSide": true,
			"sAjaxSource": THIS_URL + "/get_list",
			"sServerMethod": "POST",
			"bStateSave": true,
			"fnStateSave": function(oSettings, oData) { 
				ceki.fnSaveDtView(oSettings, oData); 
			},
			"fnStateLoad": function(oSettings) { 
				dataFN = JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname));
				
				if (dataFN != null) {
					jQuery('#search_name').val(dataFN.aoSearchCols[2].sSearch);
					jQuery('#search_group_name').val(dataFN.aoSearchCols[3].sSearch);
					jQuery('#search_employee_name').val(dataFN.aoSearchCols[4].sSearch);
					jQuery('#search_employee_nrp').val(dataFN.aoSearchCols[5].sSearch);
					jQuery('#search_employee_jabatan').val(dataFN.aoSearchCols[6].sSearch);
					jQuery('#search_employee_divisi').val(dataFN.aoSearchCols[7].sSearch);
					jQuery('#search_employee_cabang').val(dataFN.aoSearchCols[8].sSearch);
					jQuery('#search_employee_ref_proyek_id').val(dataFN.aoSearchCols[9].sSearch);
					jQuery('#search_is_active').val(dataFN.aoSearchCols[10].sSearch);
				}
				return ceki.fnLoadDtView(oSettings); 
			},
			"oLanguage": {
				"sLengthMenu": "<?php echo $this->lang->line('global_DT_sLengthMenu'); ?>",
				"sZeroRecords": "<?php echo $this->lang->line('global_DT_sZeroRecords'); ?>",
				"sInfo": "<?php echo $this->lang->line('global_DT_sInfo'); ?>",
				"sInfoEmpty": "<?php echo $this->lang->line('global_DT_sInfoEmpty'); ?>",
				"sInfoFiltered": "(<?php echo $this->lang->line('global_DT_sInfoFiltered'); ?>)",
				"sSearch": "<?php echo $this->lang->line('global_search'); ?> :",
				"oPaginate": {
					"sPrevious": "<?php echo $this->lang->line('global_prev'); ?>",
					"sNext": "<?php echo $this->lang->line('global_next'); ?>"
				}
			},
			"fnServerParams": function (aoData) {
				aoData.push({"name": "is_ajax", "value": true});
			},
            "fnDrawCallback": function(oSettings) {
				if(!jQuery(this).children('.blockUI').length){
					App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				}
				
				App.initUniform();
				App.initPopovers();
				aSelected = [];
				jQuery(this).find('input[type=checkbox]').click(function(){
					var id = this.value;
					var checked = jQuery(this).is(':checked');
					fnSelected(this, id, checked);
					fnTbarDisableEnable();
				});
				
				App.unblockUI(jQuery(this));
				fnTbarDisableEnable();
				jQuery('.checkall').parent().removeClass('checked');
				
				oTable.floatThead({
					top: ceki.pageTop
				});
            },
			"aoColumns": [
				{ "mData": "cbox" },
				{ "mData": "rnum", "sClass": "hidden-480" },
				{ "mData": "name" },
				{ "mData": "group_name", "sClass": "hidden-480" },
				{ "mData": "employee_name", "sClass": "hidden-480" },
				{ "mData": "employee_nrp", "sClass": "hidden-480" },
				{ "mData": "employee_jabatan", "sClass": "hidden-480 hidden-767" },
				{ "mData": "employee_divisi", "sClass": "hidden-480 hidden-767" },
				{ "mData": "employee_cabang", "sClass": "hidden-480 hidden-767 hidden-979" },
				{ "mData": "employee_ref_proyek_id", "sClass": "hidden-480 hidden-767 hidden-979" },
				{ "mData": "employee_email", "sClass": "hidden-480 hidden-767 hidden-979" },
				{ "mData": "is_active" }
			],
			"aLengthMenu": [
				[5, 15, 20, 50, 100, -1],
				[5, 15, 20, 50, 100, "All"]
			],
            "iDisplayLength": 5,
			"sDom": "<'dataTables_toolbar'><'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"fnPreDrawCallback": function () { 
				if (jQuery('.dataTables_length select').val() == -1) {
					if(oTable.fnSettings().fnRecordsTotal() > 500) {
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_limit_record_page'); ?>',
							sticky: false,
							time: 3000
						});
						return false;
					}
				}				
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {}
        });
		
		jQuery('#table1_wrapper .dataTables_filter input').addClass("m-wrap");
		jQuery('#table1_wrapper .dataTables_length select').addClass("m-wrap small"); 
		jQuery('#table1_wrapper .dataTables_length select').select2(); 
		
		jQuery('.dataTables_filter input').unbind();
		jQuery(".dataTables_filter input").keyup(function (e) {
			if(e.keyCode == 13) {
				App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				oTable.fnFilter(this.value);   
			}
			e.preventDefault();
		});
		
		jQuery('tfoot input').unbind();
		jQuery("tfoot input").keyup(function (e) {
			if(e.keyCode == 13) {
				App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				oTable.fnFilter(this.value, jQuery("tfoot input").index(this)); 
			}
			e.preventDefault();
		});
		
		jQuery("tfoot input").each( function (i) {
			asInitVals[i] = this.value;
		});
		
		jQuery("tfoot input").blur( function (i) {
			if (this.value == ""){
				this.className = "search_init";
				//this.value = asInitVals[jQuery("tfoot input").index(this)];
			}
		});
		
		function save_dt_view (oSettings, oData) {
			localStorage.setItem( 'DataTables_'+window.location.pathname, JSON.stringify(oData) );
		}
		function load_dt_view (oSettings) {
			return JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname) );
		}
		function reset_dt_view() {
			aSelected = [];
			localStorage.removeItem('DataTables_'+window.location.pathname);
		}
		
		jQuery('.checkall').click(function(){
			
			var ch = oTable.find('input[type=checkbox]');
			
			var checked = jQuery(this).is(':checked');
			
			ch.each(function(){
				if(this.value != 'on') {
					fnSelected(this, this.value, checked , true);
				}
			});	
			
			fnTbarDisableEnable();
		});
		
		function fnSelected(cmp, id, checked, all){
			var index = jQuery.inArray(id, aSelected);
			if(id == 'on') return;		 
			if ( index === -1 ) {
				if(all){
					if(checked){
						aSelected.push(id);
					}
				}else{
					aSelected.push(id);
				}
			} else {
				if(all){
					if(!checked){
						aSelected.splice(index, 1);
					}
				}else{
					aSelected.splice(index, 1);
				}
			}
			
			if(checked) {	
				jQuery(cmp).attr('checked',true);
				jQuery(cmp).parent().addClass('checked');
				jQuery(cmp).parents('tr').addClass('selected');
			}else{
				jQuery(cmp).attr('checked',false);
				jQuery(cmp).parent().removeClass('checked');
				jQuery(cmp).parents('tr').removeClass('selected');
			}
			
			console.log(aSelected);
		}
		
		function fnTbarDisableEnable(){
			var rowSelected = aSelected.length;
			
			if(rowSelected > 0){
				if(rowSelected > 1){
					jQuery('#button-edit').attr("disabled", "disabled");
				}else{
					jQuery('#button-edit').removeAttr("disabled");
				}
				jQuery('#button-delete').removeAttr("disabled");
			}else{
				jQuery('#button-edit').attr("disabled", "disabled");
				jQuery('#button-delete').attr("disabled", "disabled");
			}
		}
		
		jQuery("div.dataTables_toolbar").html(' '+
		'<button id="button-refresh" class="btn purple"><i class="icon-refresh"></i> <?php echo $this->lang->line('global_refresh'); ?></button>&nbsp;' +
		'<button id="button-add" class="btn green" href="#add_edit-modal" data-toggle="modal" style="visibility:hidden;"><i class="icon-plus"></i></i> <?php echo $this->lang->line('global_add'); ?></button>&nbsp;' +
		'<button disabled="disabled" id="button-edit" class="btn yellow" href="#add_edit-modal" data-toggle="modal" style="visibility:hidden;"><i class="icon-edit"></i> <?php echo $this->lang->line('global_edit'); ?></button>&nbsp;' +
		'<button disabled="disabled" id="button-delete" class="btn red" style="visibility:hidden;"><i class="icon-trash"></i> <?php echo $this->lang->line('global_delete'); ?></button>&nbsp;' +
		'<div style="float:right;"><button id="button-clear" class="btn black"><i class="icon-share"></i></a></div> ');
		
	
		jQuery('#button-refresh').click(function(){
			App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
			oTable.fnDraw();
		});
		
		jQuery('#button-clear').click(function(){
			ceki.fnResetDtView();
			ceki.fnRefresh()
		});
		
		if(MODULE_FUNCTION.add){
			jQuery("#button-add").css('visibility','visible');
		}
		
		if(MODULE_FUNCTION.edit){
			jQuery("#button-edit").css('visibility','visible');
		}
		
		if(MODULE_FUNCTION.del){
			jQuery("#button-delete").css('visibility','visible');
		}
		
		jQuery('#add_edit-modal').on('hide', function () {
			fnEmptyForm();
		});
		
		jQuery('#button-add').click(function(){
			jQuery('.modal-header h3').html('<?php echo $this->lang->line('global_form_add'); ?>');
			jQuery('#name').removeAttr("disabled");
			if(jQuery("#upload_link").length > 0) {
				jQuery("#upload_link").remove();  
			}
		});
		
		jQuery('#button-edit').click(function(){
			jQuery('#proyeks').val('');
			id = aSelected[0];
			
			var rowIndex = oTable.fnGetPosition(jQuery('#'+id).closest('tr')[0]);
			
			var data = oTable.fnGetData(rowIndex);
			console.log(data);
			
			jQuery('.modal-header h3').html('<?php echo $this->lang->line('global_form_edit'); ?> - <span style="color:#FFF" class="hidden-480">'+data.employee_name+'</span>');
			
			jQuery('#name').attr("disabled", "disabled");
			jQuery('#name').val(data.names);
			
			if(data.is_actives == 1){
				jQuery("#is_active0").attr("checked", false);
				jQuery("#is_active1").attr("checked", true);
			}else{
				jQuery("#is_active1").attr("checked", false);
				jQuery("#is_active0").attr("checked", true);
			}
			App.initUniform();
			jQuery('#sys_group_child_id').val(data.sys_group_child_id);
			if(data.proyeks != '' && data.proyeks != null) {
				if(typeof data.proyeks.length != 'undefined') {
					for(var i=0; i < data.proyeks.length; i++){
						var obj = data.proyeks[i];
						for (var key in obj) {
							var attrName = key;
							var attrValue = obj[key];
						}
						$('#'+obj).attr('selected', true);
					}
				} else {
					jQuery('#proyeks').val(data.proyeks);
				}
				
				jQuery("#proyeks").trigger("liszt:updated");
			}
			
			if(jQuery("#upload_link").length > 0) {
				jQuery("#upload_link").remove();  
			}

			if(data.signature_path != '') {
				jQuery(".fileupload.fileupload-new" ).after(' ' +
					'<div style="line-height:20px;" id="upload_link">' +
						'<img style="width:50px;height:50px;" src="'+APP_ASSET_URL+data.signature_path+'" alt="signature" />' +
					'</div>' +
				' ');
			}
		
			if(jQuery("#id").length > 0) {
				jQuery("#id").remove();  
			}
			jQuery('#add_edit-form').append('<input type="hidden" id="id" name="id" value="'+id+'" />');
		});
			
		jQuery('#button-delete').click(function(){
			jConfirm('<?php echo $this->lang->line('global_confirm_delete'); ?>', '<?php echo $this->lang->line('global_confirm_delete_header'); ?>', function(r) {
				if(r){
					var ids = { };

					for(var i = 0; i < aSelected.length; i++) {
					  ids[i] = aSelected[i];
					}
					
					var cAjax = new ceki.fnAjax({
						method : '/delete',
						data : ({
							ids: ids,
							is_ajax: 1
						}),
						successCallBack : function() {
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_success_delete'); ?>',
								sticky: false,
								time: 3000
							});
							oTable.fnDraw();
							aSelected = [];
					
							fnTbarDisableEnable();
						}
					});
				}
			});
		});
		
		//init form
		ceki.fnStrNoSpace('name', '_');
		
		var rules = {};
		var messages = {};
		
		rules["name"] = { required: true, minlength: 5 };
		messages["name"] = { required: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('users_name')); ?>", minlength: "<?php echo sprintf($this->lang->line('global_error_minlength_field'),$this->lang->line('users_name'),5); ?>" };
		rules["sys_group_child_id"] = { required: false};
		messages["sys_group_child_id"] = { required: "<?php echo sprintf($this->lang->line('global_error_required_field'),$this->lang->line('users_group_name')); ?>" };
		rules["signature"] = { extension: "gif|jpg|jpeg|png" };
		messages["signature"] = { 
			required: "<?php echo $this->lang->line('global_error_required_field2'); ?>", 
			extension: "<?php echo $this->lang->line('global_format_file'); ?> <?php echo $this->lang->line('global_must_be'); ?> gif|jpg|jpeg|png", 
			filesize: "<?php echo $this->lang->line('global_error_file_size')." 1MB"; ?>" 
		};
		
		
		jQuery("#add_edit-form").validate({
			onsubmit: false,
			ignore: "",
			rules: rules,
			messages: messages,
			highlight: function(label) {},
			success: function(label) {},
			errorPlacement: function(error, element) {
				if (element.attr("name") == "signature") {
					error.insertAfter(".fileupload.fileupload-new");
				} else {
					error.appendTo(element.parent());
				}
			},
			invalidHandler: function(e, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error_form'); ?>',
						sticky: false,
						time: 3000
					});
					return false;
				}
				e.preventDefault();
			}
		});
		
		jQuery("#password_conf").blur(function(){
			if(jQuery("#id").length > 0){
				if(jQuery(this).val() == jQuery("#password").val()){
					
					if(jQuery("#p_old").length > 0) {
						jQuery("#p_old").remove();  
					}
					
					jQuery("#password_conf").parent().parent().after('<div id="p_old" class="control-group"><label class="control-label"><?php echo $this->lang->line('users_password_old'); ?></label><div class="controls"><input type="password" name="password_old" id="password_old" /></div></div>');
					jQuery("#password_old").focus();
				}
			}
		});
		
		jQuery('#add_edit-form').submit(function(e) {
			if(jQuery(this).valid()){
				form = jQuery(this);
				var formData = new FormData(form[0]);
				var cAjax = new ceki.fnAjax({
					method : '/save',
					data : formData,
					async: false,
					cache: false,
					contentType: false,
					processData: false,
					successCallBack : function() {
						jQuery('#add_edit-modal').modal('hide');
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_success_update'); ?>',
							sticky: false,
							time: 3000
						});
						oTable.fnDraw();
						
						aSelected = [];
						
						fnTbarDisableEnable();
					},
					successErrorCallBack : function(obj) {
						if(jQuery("#alert").length > 0) {
							jQuery("#alert").remove();  
						}
						msg = '<div id="alert" class="alert alert-error"><button data-dismiss="alert" class="close" type="button"></button>'+obj.message+'</div>';
						jQuery(".modal-header").after(msg);
					}
				});
			}
			e.preventDefault();
		});
		
		function fnEmptyForm(){
			if(jQuery("#id").length > 0) {
				jQuery("#id").remove();  
			}
			
			if(jQuery("#alert").length > 0) {
				jQuery("#alert").remove();  
			}
			
			if(jQuery("#p_old").length > 0) {
				jQuery("#p_old").remove();  
			}
		
			jQuery('#name').val('');
			jQuery('#password').val('');
			jQuery('#password_conf').val('');
			jQuery('#email').val('');
			jQuery("#is_active1").attr("checked", false);
			jQuery("#is_active0").attr("checked", false);
			jQuery('#sys_group_child_id').val(1);
			jQuery('#proyeks').val(null);
			jQuery("#proyeks").trigger("liszt:updated");
			
			
			jQuery('#add_edit-form')[0].reset();
		}
	});
</script>