<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
.controls .text {
	width: 250px;
}
.form-horizontal .control-label {
    float: left;
    padding-top: 5px;
    width: 150px;
}
</style>
<div id="profile">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-credit-card"></i><?php echo $this->lang->line('global_form'); ?></div>
		</div>
		<div class="portlet-body form">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('success_alert')): ?>
				<div class="alert alert-success">
					<button data-dismiss="alert" class="close"></button>
					<?php echo $this->session->flashdata('success_alert'); ?>
				</div>
			<?php endif; ?>
			<?php if($this->session->flashdata('error_alert')): ?>
				<div class="alert alert-error">
					<button data-dismiss="alert" class="close"></button>
					<?php echo $this->session->flashdata('error_alert'); ?>
				</div>
			<?php endif; ?>
			
			<div class="row-fluid">
				<div class="span7 form-horizontal">
					<h3 class="form-section"><?php echo $this->lang->line('global_employee_info'); ?></h3>
					<div class="span1">
						<div style="width: 150px; height: auto;" class="thumbnail">
							<img alt="" src="<?php echo $picture; ?>">
						</div>
						<div class="space20"></div>
					</div>
					
					<div class="offset1 span4">
						<div class="control-group">
							<label class="control-label"><b><?php echo $this->lang->line('employee_name'); ?>:</b></label>
							<div class="controls">
								<span id="employee_name" class="text bold"><?php echo isset($res_user_employee['name']) ? $res_user_employee['name'] : '-' ?></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><b><?php echo $this->lang->line('employee_nrp'); ?>:</b></label>
							<div class="controls">
								<span id="employee_nrp" class="text bold"><?php echo isset($res_user_employee['nrp']) ? $res_user_employee['nrp'] : '-' ?></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><b><?php echo $this->lang->line('employee_position'); ?>:</b></label>
							<div class="controls">
								<span id="employee_position" class="text bold"><?php echo isset($res_user_employee['jabatan_name']) ? $res_user_employee['jabatan_name'] : '-' ?></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><b><?php echo $this->lang->line('employee_branch'); ?>:</b></label>
							<div class="controls">
								<span id="employee_branch" class="text bold"><?php echo isset($res_user_employee['cabang']) ? $res_user_employee['cabang'] : '-' ?></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><b><?php echo $this->lang->line('employee_division'); ?>:</b></label>
							<div class="controls">
								<span id="employee_division" class="text bold"><?php echo isset($res_user_employee['divisi']) ? $res_user_employee['divisi'] : '-' ?></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="span5 ">
					<h3 class="form-section"><?php echo $this->lang->line('global_user_info'); ?></h3>
					<form id="add_edit-form" class="form-horizontal" method="post" autocomplete="off" action="<?php echo app_backend_url('managements/users/save'); ?>">
						<div class="control-group">
							<label class="control-label" style="text-align:left;"><b><?php echo $this->lang->line('users_name'); ?>:</b></label>
							<div class="controls">
								<span id="user_name" class="text bold"><?php echo isset($res_user_employee['user_name']) ? $res_user_employee['user_name'] : '-' ?></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" style="text-align:left;"><b><?php echo $this->lang->line('users_group_name'); ?>:</b></label>
							<div class="controls">
								<span id="user_name" class="text bold"><?php echo isset($res_user_employee['sys_group_child_desc']) ? $res_user_employee['sys_group_child_desc'] : '-' ?></span>
							</div>
						</div>
						<?php if(! $this->session->userdata(APPLICATION.'_id')): ?>
						<div class="control-group password-strength">
							<label class="control-label" style="text-align:left;"><b><?php echo $this->lang->line('users_password'); ?>:</b></label>
							<div class="controls">
								<input type="password" class="m-wrap" name="password" id="password" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" style="text-align:left;"><b><?php echo $this->lang->line('users_password_conf'); ?></b></label>
							<div class="controls">
								<input type="password" class="m-wrap" name="password_conf" id="password_conf" />
							</div>
						</div>
						<div class="center">
							<input type="hidden" id="id" name="id" value="<?php echo $this->session->userdata(APPLICATION.'_user_id'); ?>" />
							<input type="hidden" id="is_active" name="is_active" value="1" />
							<input type="hidden" id="sys_group_child_id" name="sys_group_child_id" value="<?php echo $this->session->userdata(APPLICATION.'_sys_group_child_id'); ?>" />
							<button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
						</div>
						<?php endif; ?>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	jQuery(document).ready(function(){
		var rules = {};
		var messages = {};
		
		rules["password"] = { required: true, minlength: 5 };
		messages["password"] = { required: "<?php echo $this->lang->line('global_error_required_field2'); ?>", minlength: "<?php echo sprintf($this->lang->line('global_error_minlength_field'),$this->lang->line('users_password'),5); ?>" };
		
		rules["password_conf"] = { required: true, minlength: 5, equalTo: "#password" };
		messages["password_conf"] = { required: "<?php echo $this->lang->line('global_error_required_field2'); ?>", minlength: "<?php echo sprintf($this->lang->line('global_error_minlength_field'),$this->lang->line('users_password'),5); ?>", equalTo: "<?php echo $this->lang->line('users_password_conf_wrong'); ?>" };
		
		jQuery("#add_edit-form").validate({
			onsubmit: false,
			ignore: "",
			rules: rules,
			messages: messages,
			highlight: function(label) {},
			success: function(label) {},
			invalidHandler: function(e, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error_form'); ?>',
						sticky: false,
						time: 3000
					});
					return false;
				}
				e.preventDefault();
			}
		});
		
		jQuery("#password_conf").blur(function(){
			if(jQuery("#id").length > 0){
				if(jQuery(this).val() == jQuery("#password").val()){
					
					if(jQuery("#p_old").length > 0) {
						jQuery("#p_old").remove();  
					}
					
					jQuery("#password_conf").parent().parent().after('<div id="p_old" class="control-group"><label class="control-label" style="text-align:left;"><b><?php echo $this->lang->line('users_password_old'); ?></b></label><div class="controls"><input class="m-wrap required" type="password" name="password_old" id="password_old" /></div></div>');
					jQuery("#password_old").focus();
				}
			}
		});
		
		jQuery('#add_edit-form').submit(function(e) {
			if(jQuery(this).valid()){
				return true;
			}
			e.preventDefault();
		});
		
		// password strength
		var initialized = false;
        var input = jQuery("#password");

        input.keydown(function(){
            if (initialized === false) {
                input.pwstrength({
                    raisePower: 1.4,
                    minChar: 8,
                    verdicts: ["<?php echo $this->lang->line('global_weak'); ?>", "<?php echo $this->lang->line('global_normal'); ?>", "<?php echo $this->lang->line('global_sedang'); ?>", "<?php echo $this->lang->line('global_strong'); ?>", "<?php echo $this->lang->line('global_very_strong'); ?>"],
                    scores: [17, 26, 40, 50, 60]
                });

                input.pwstrength("addRule", "demoRule", function (options, word, score) {
                    return word.match(/[a-z].[0-9]/) && score;
                }, 10, true);

                jQuery('.progress', input.parents('.password-strength')).css('width', input.outerWidth() - 2); 

                initialized = true;
            }
        });        
		
		var rules2 = {};
		var messages2 = {};
		
		rules2["ref_mandor_type_id"] = { required: true };
		messages2["ref_mandor_type_id"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>" };
		rules2["jobs_type[]"] = { required: true };
		messages2["jobs_type[]"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>" };
		
		/*
		rules["name"] = { required: true };
		messages["name"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>" };
		rules["wakil_mandor"] = { required: true };
		messages["wakil_mandor"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>" };
		
		rules["province"] = { required: true };
		messages["province"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>" };
		rules["city"] = { required: true };
		messages["city"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>" };
		
		rules["address"] = { required: true };
		messages["address"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>" };
		
		rules["email"] = { email: true };
		messages["email"] = { email: "<?php echo $this->lang->line('global_email_invalid'); ?>" };
		rules["npwp_number"] = { required: true, number: true };
		messages["npwp_number"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>", number: "<?php echo $this->lang->line('global_error_number'); ?>" };
		
		rules["rek_name"] = { required: true};
		messages["rek_name"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>" };
		rules["rek_number"] = { required: true, number: true };
		messages["rek_number"] = { required: "<?php echo $this->lang->line('global_error_required_field2') ?>", number: "<?php echo $this->lang->line('global_error_number'); ?>" };
		*/
		
		jQuery("#form_save").validate({
			onsubmit: false,
			ignore: "",
			rules: rules2,
			messages: messages2,
			highlight: function(label) {},
			success: function(label) {},
			errorPlacement: function(error, element) {
				error.appendTo(element.parent());
			},
			invalidHandler: function(e, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error_form'); ?>',
						sticky: false,
						time: 3000
					});

					return false;
				}
				e.preventDefault();
			}
		});
		
		jQuery('#form_save').submit(function(e) {
			if(jQuery(this).valid()){
				return true;
			}
			e.preventDefault();
		});
		
		jQuery('#btn_cancel').click(function(e) {
			ceki.fnLocation('<?php echo app_backend_url(); ?>');
		});
	});
</script>