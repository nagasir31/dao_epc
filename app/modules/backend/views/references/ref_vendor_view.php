<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
body .modal#add_edit-modal {
  width: 40%; 
  left: 30%;
  margin-left:auto;
  margin-right:auto; 
}
</style>
<div class="row-fluid">
	<div class="span12 responsive" data-tablet="span12 fix-offset" data-desktop="span12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="icon-reorder"></i><?php echo $this->lang->line('global_list'); ?></div>
			</div>
			<div class="portlet-body">
				<table class="table table-bordered table-hover" id="table1">
					<thead>
						<tr>
							<th style="width:3%;text-align:center;"><input type="checkbox" class="checkall" data-set="#table1 .checkboxes" /></th>
							<th style="width:3%;" class="hidden-480"><?php echo $this->lang->line('global_no'); ?></th>
							<th style="width:20%;" ><?php echo $this->lang->line('global_name'); ?></th>
							<th style="width:10%;" class="hidden-480"><?php echo $this->lang->line('berkas_npwp'); ?></th>
							<th style="width:20%;" class="hidden-480"><?php echo $this->lang->line('berkas_bank_name'); ?></th>
							<th style="width:10%;" class="hidden-480"><?php echo $this->lang->line('berkas_bank_rek'); ?></th>
							<th style="width:10%;" class="hidden-480"><?php echo $this->lang->line('berkas_bank_account'); ?></th>
							<th style="width:7%;" class="hidden-480"><?php echo $this->lang->line('berkas_vendor_type'); ?></th>
						</tr>
					</thead>
					<tbody>
						<td colspan="7" class="dataTables_empty"><?php echo $this->lang->line('global_loading_data'); ?></td>
					</tbody>
					<tfoot>
						<tr>
							<td width="3%"><input type="hidden" /></td>
							<td width="3%"><input type="hidden" /></td>
							<td width="20%"><input type="text" id="search_name" name="search_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td width="10%"><input type="text" id="search_npwp" name="search_npwp" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td width="20%"><input type="text" id="search_bank_name" name="search_bank_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td width="10%"><input type="text" id="search_bank_rek" name="search_bank_rek" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td width="10%"><input type="text" id="search_bank_account" name="search_bank_account" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
							<td width="7%"><input type="text" id="search_vendor_type" name="search_vendor_type" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="add_edit-modal">
	<form id="add_edit-form" class="form-horizontal" method="post" autocomplete="off">
		<div class="modal-header">
			<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
			<h3></h3>
		</div>
		<div class="modal-body">
			<div class="scroller" style="height:400px" data-always-visible="1" data-rail-visible1="1">
				<div class="row-fluid">
					<div class="span12">
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('global_code'); ?></label>
							<div class="controls">
								<input type="text" name="code" id="code" class="m-wrap span10" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('global_name'); ?></label>
							<div class="controls">
								<input type="text" name="name" id="name" class="m-wrap span10" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('berkas_npwp'); ?></label>
							<div class="controls">
								<input type="text" name="npwp" id="npwp" class="m-wrap span10" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('berkas_bank_name'); ?></label>
							<div class="controls">
								<input type="text" name="bank_name" id="bank_name" class="m-wrap span10" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('berkas_bank_rek'); ?></label>
							<div class="controls">
								<input type="text" name="bank_rek" id="bank_rek" class="m-wrap span10" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('berkas_bank_account'); ?></label>
							<div class="controls">
								<input type="text" name="bank_account" id="bank_account" class="m-wrap span10" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('berkas_vendor_type'); ?></label>
							<div class="controls">
								<select name="ref_vendor_type_id" id="ref_vendor_type_id" class="uniformselect m-wrap span10">
								<?php foreach($ref_vendor_type as $dt): ?>
									<option value="<?php echo $dt['id']; ?>"><?php echo $dt['name']; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('global_address'); ?></label>
							<div class="controls">
								<textarea style="width:60%" rows="3" id="address" name="address"></textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('global_city'); ?></label>
							<div class="controls">
								<input type="text" name="city" id="city" class="m-wrap span10" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo $this->lang->line('global_email'); ?></label>
							<div class="controls">
								<input type="text" name="email" id="email" class="m-wrap span10" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
			<button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
		</div>
	</form>
</div>
<script>
	jQuery(document).ready(function(){
		
		fnEmptyForm();
		
		// init grid
		var aSelected = [];
		var asInitVals = [];
		
		var oTable = jQuery('#table1');
		App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
		oTable.dataTable({
            "sPaginationType": "bootstrap",
			"aaSorting": [[1,"desc"]],
			"aoColumnDefs": [
				{"bSortable": false, "aTargets": [0,1], "bSearchable": false, "aTargets": [0,1]}
		    ],
			"bFilter" : MODULE_FUNCTION.search ? true : false,
			"bServerSide": true,
			"sAjaxSource": THIS_URL + "/get_list",
			"sServerMethod": "POST",
			"bStateSave": true,
			"fnStateSave": function(oSettings, oData) { 
				ceki.fnSaveDtView(oSettings, oData); 
			},
			"fnStateLoad": function(oSettings) { 
				dataFN = JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname));
				if (dataFN != null) {
					jQuery('#search_name').val(dataFN.aoSearchCols[2].sSearch);
					jQuery('#search_npwp').val(dataFN.aoSearchCols[3].sSearch);
					jQuery('#search_bank_name').val(dataFN.aoSearchCols[4].sSearch);
					jQuery('#search_bank_rek').val(dataFN.aoSearchCols[5].sSearch);
					jQuery('#search_bank_account').val(dataFN.aoSearchCols[6].sSearch);
					jQuery('#search_vendor_type').val(dataFN.aoSearchCols[7].sSearch);
				}
				return ceki.fnLoadDtView(oSettings); 
			},
			"oLanguage": {
				"sLengthMenu": "<?php echo $this->lang->line('global_DT_sLengthMenu'); ?>",
				"sZeroRecords": "<?php echo $this->lang->line('global_DT_sZeroRecords'); ?>",
				"sInfo": "<?php echo $this->lang->line('global_DT_sInfo'); ?>",
				"sInfoEmpty": "<?php echo $this->lang->line('global_DT_sInfoEmpty'); ?>",
				"sInfoFiltered": "(<?php echo $this->lang->line('global_DT_sInfoFiltered'); ?>)",
				"sSearch": "<?php echo $this->lang->line('global_search'); ?> :",
				"oPaginate": {
					"sPrevious": "<?php echo $this->lang->line('global_prev'); ?>",
					"sNext": "<?php echo $this->lang->line('global_next'); ?>"
				}
			},
			"fnServerParams": function (aoData) {
				aoData.push({"name": "is_ajax", "value": true});
			},
            "fnDrawCallback": function(oSettings) {
				if(!jQuery(this).children('.blockUI').length){
					App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				}
				
				App.initUniform();
				App.initPopovers();
				aSelected = [];
				jQuery(this).find('input[type=checkbox]').click(function(){
					var id = this.value;
					var checked = jQuery(this).is(':checked');
					fnSelected(this, id, checked);
					fnTbarDisableEnable();
				});
				
				App.unblockUI(jQuery(this));
				fnTbarDisableEnable();
				jQuery('.checkall').parent().removeClass('checked');
				
				oTable.floatThead({
					top: ceki.pageTop
				});
            },
			"aoColumns": [
				{ "mData": "cbox" },
				{ "mData": "rnum", "sClass": "hidden-480" },
				{ "mData": "name" },
				{ "mData": "npwp", "sClass": "hidden-480" },
				{ "mData": "bank_name", "sClass": "hidden-480" },
				{ "mData": "bank_rek", "sClass": "hidden-480" },
				{ "mData": "bank_account", "sClass": "hidden-480" },
				{ "mData": "vendor_type", "sClass": "hidden-480" }
			],
			"aLengthMenu": [
				[5, 15, 20, 50, 100, -1],
				[5, 15, 20, 50, 100, "All"]
			],
            "iDisplayLength": 5,
			"sDom": "<'dataTables_toolbar'><'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"fnPreDrawCallback": function () { 
				if (jQuery('.dataTables_length select').val() == -1) {
					if(oTable.fnSettings().fnRecordsTotal() > 500) {
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_limit_record_page'); ?>',
							sticky: false,
							time: 3000
						});
						return false;
					}
				}				
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {}
        });
		
		jQuery('#table1_wrapper .dataTables_filter input').addClass("m-wrap");
		jQuery('#table1_wrapper .dataTables_length select').addClass("m-wrap small"); 
		jQuery('#table1_wrapper .dataTables_length select').select2(); 
		
		jQuery('.dataTables_filter input').unbind();
		jQuery(".dataTables_filter input").keyup(function (e) {
			if(e.keyCode == 13) {
				App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				oTable.fnFilter(this.value);   
			}
			e.preventDefault();
		});
		
		jQuery('tfoot input').unbind();
		jQuery("tfoot input").keyup(function (e) {
			if(e.keyCode == 13) {
				App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				oTable.fnFilter(this.value, jQuery("tfoot input").index(this));  
			}
			e.preventDefault();
		});
		
		jQuery("tfoot input").each( function (i) {
			asInitVals[i] = this.value;
		});
		
		jQuery("tfoot input").blur( function (i) {
			if (this.value == ""){
				this.className = "search_init";
				this.value = asInitVals[jQuery("tfoot input").index(this)];
			}
		});
		
		jQuery('.checkall').click(function(){
			
			var ch = oTable.find('input[type=checkbox]');
			
			var checked = jQuery(this).is(':checked');
			
			ch.each(function(){
				if(this.value != 'on') {
					fnSelected(this, this.value, checked , true);
				}
			});	
			
			fnTbarDisableEnable();
		});
		
		function fnSelected(cmp, id, checked, all){
			var index = jQuery.inArray(id, aSelected);
			if(id == 'on') return;		 
			if ( index === -1 ) {
				if(all){
					if(checked){
						aSelected.push(id);
					}
				}else{
					aSelected.push(id);
				}
			} else {
				if(all){
					if(!checked){
						aSelected.splice(index, 1);
					}
				}else{
					aSelected.splice(index, 1);
				}
			}
			
			if(checked) {	
				jQuery(cmp).attr('checked',true);
				jQuery(cmp).parent().addClass('checked');
				jQuery(cmp).parents('tr').addClass('selected');
			}else{
				jQuery(cmp).attr('checked',false);
				jQuery(cmp).parent().removeClass('checked');
				jQuery(cmp).parents('tr').removeClass('selected');
			}
			
			console.log(aSelected);
		}
		
		function fnTbarDisableEnable(){
			var rowSelected = aSelected.length;
			
			if(rowSelected > 0){
				if(rowSelected > 1){
					jQuery('#button-edit').attr("disabled", "disabled");
				}else{
					jQuery('#button-edit').removeAttr("disabled");
				}
				jQuery('#button-delete').removeAttr("disabled");
			}else{
				jQuery('#button-edit').attr("disabled", "disabled");
				jQuery('#button-delete').attr("disabled", "disabled");
			}
		}
		
		jQuery("div.dataTables_toolbar").html(' '+
		'<button id="button-refresh" class="btn purple"><i class="icon-refresh"></i> <?php echo $this->lang->line('global_refresh'); ?></button>&nbsp;' +
		'<button id="button-add" class="btn green" href="#add_edit-modal" data-toggle="modal" style="visibility:hidden;"><i class="icon-plus"></i></i> <?php echo $this->lang->line('global_add'); ?></button>&nbsp;' +
		'<button disabled="disabled" id="button-edit" class="btn yellow" href="#add_edit-modal" data-toggle="modal" style="visibility:hidden;"><i class="icon-edit"></i> <?php echo $this->lang->line('global_edit'); ?></button>&nbsp;' +
		'<button disabled="disabled" id="button-delete" class="btn red" style="visibility:hidden;"><i class="icon-trash"></i> <?php echo $this->lang->line('global_delete'); ?></button>&nbsp;' +
		'<div style="float:right;"><button id="button-clear" class="btn black"><i class="icon-share"></i></a></div> ');
		
	
		jQuery('#button-refresh').click(function(){
			App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
			oTable.fnDraw();
		});
		
		jQuery('#button-clear').click(function(){
			ceki.fnResetDtView();
			ceki.fnRefresh()
		});
		
		if(MODULE_FUNCTION.add){
			jQuery("#button-add").css('visibility','visible');
		}
		
		if(MODULE_FUNCTION.edit){
			jQuery("#button-edit").css('visibility','visible');
		}
		
		if(MODULE_FUNCTION.del){
			jQuery("#button-delete").css('visibility','visible');
		}
		
		jQuery('#add_edit-modal').on('hide', function () {
			fnEmptyForm();
		});
		
		jQuery('#button-add').click(function(){
			jQuery('.modal-header h3').html('<?php echo $this->lang->line('global_form_add'); ?>');
			jQuery('#name').removeAttr("disabled");
			jQuery('#name').removeAttr("readOnly");
		});
		
		jQuery('#button-edit').click(function(){
			App.initUniform();
			
			id = aSelected[0];
			
			var rowIndex = oTable.fnGetPosition(jQuery('#'+id).closest('tr')[0]);
			
			var data = oTable.fnGetData(rowIndex);
			
			
			jQuery('.modal-header h3').html('<?php echo $this->lang->line('global_form_edit'); ?> - <span style="color:#3399FF">'+data.employee_name+'</span>');
			
			jQuery('#name').attr("readOnly", "readOnly");
			jQuery('#name').val(data.name);
			
			jQuery('#npwp').val(data.npwp);
			jQuery('#bank_name').val(data.bank_name);
			jQuery('#bank_rek').val(data.bank_rek);
			jQuery('#bank_account').val(data.bank_account);
			jQuery('#ref_vendor_type_id').val(data.ref_vendor_type_id);
			
			jQuery('#code').val(data.code);
			jQuery('#address').val(data.address);
			jQuery('#city').val(data.city);
			jQuery('#email').val(data.email);
		
			if(jQuery("#id").length > 0) {
				jQuery("#id").remove();  
			}
			jQuery('#add_edit-form').append('<input type="hidden" id="id" name="id" value="'+id+'" />');
		});
			
		jQuery('#button-delete').click(function(){
			jConfirm('<?php echo $this->lang->line('global_confirm_delete'); ?>', '<?php echo $this->lang->line('global_confirm_delete_header'); ?>', function(r) {
				if(r){
					var ids = { };

					for(var i = 0; i < aSelected.length; i++) {
					  ids[i] = aSelected[i];
					}
					
					var cAjax = new ceki.fnAjax({
						method : '/delete',
						data : ({
							ids: ids,
							is_ajax: 1
						}),
						successCallBack : function() {
							jQuery.gritter.add({
								title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
								text: '<?php echo $this->lang->line('global_success_delete'); ?>',
								sticky: false,
								time: 3000
							});
							oTable.fnDraw();
							aSelected = [];
					
							fnTbarDisableEnable();
						}
					});
				}
			});
		});
		
		//init form
		jQuery("#npwp").inputmask({"mask": "99.999.999.9.999.999"}); 

		var rules = {};
		var messages = {};
		
		rules["name"] = { required: true, minlength: 5 };
		messages["name"] = { required: "<?php echo $this->lang->line('global_error_required_field2'); ?>", minlength: "<?php echo sprintf($this->lang->line('global_error_minlength_field'),$this->lang->line('global_name'),5); ?>" };
		
		rules["ref_vendor_type_id"] = { required: true};
		messages["ref_vendor_type_id"] = { required: "<?php echo $this->lang->line('global_error_required_field2'); ?>" };
		
		rules["email"] = { required: false, email: true };
		messages["email"] = { required: "<?php echo $this->lang->line('global_error_required_field2'); ?>", email: "<?php echo $this->lang->line('global_email_invalid'); ?>" };
		
		
		jQuery("#add_edit-form").validate({
			onsubmit: false,
			ignore: "",
			rules: rules,
			messages: messages,
			highlight: function(label) {},
			success: function(label) {},
			invalidHandler: function(e, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error_form'); ?>',
						sticky: false,
						time: 3000
					});
					return false;
				}
				e.preventDefault();
			}
		});
		
		jQuery('#add_edit-form').submit(function(e) {
			if(jQuery(this).valid()){
				var cAjax = new ceki.fnAjax({
					method : '/save',
					data : jQuery(this).serialize(),
					successCallBack : function() {
						jQuery('#add_edit-modal').modal('hide');
						jQuery.gritter.add({
							title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
							text: '<?php echo $this->lang->line('global_success_update'); ?>',
							sticky: false,
							time: 3000
						});
						oTable.fnDraw();
						
						aSelected = [];
						
						fnTbarDisableEnable();
					},
					successErrorCallBack : function(obj) {
						if(jQuery("#alert").length > 0) {
							jQuery("#alert").remove();  
						}
						msg = '<div id="alert" class="alert alert-error"><button data-dismiss="alert" class="close" type="button"></button>'+obj.message+'</div>';
						jQuery(".modal-header").after(msg);
					}
				});
			}
			e.preventDefault();
		});
		
		function fnEmptyForm(){
			if(jQuery("#id").length > 0) {
				jQuery("#id").remove();  
			}
			
			if(jQuery("#alert").length > 0) {
				jQuery("#alert").remove();  
			}
			
			jQuery('#add_edit-form')[0].reset();
		}
	});
</script>