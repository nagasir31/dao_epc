<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>

    /*table.dataTable tbody td {*/
        /*word-break: break-word;*/
        /*vertical-align: top;*/
    /*}*/


    /*div.DTFC_LeftBodyWrapper{*/
        /*top: -5px !important;*/
    /*}*/

    /*div.DTFC_LeftFootWrapper table{*/
        /*margin-top: 0px ;*/
        /*border-right: 0px;*/
    /*}*/

    /*div.DTFC_LeftFootWrapper table tfoot tr td:first-child {*/
    /*width: 37px !important;*/
    /*}*/

    /*div.DTFC_LeftFootWrapper table tfoot tr td:first-child + td {*/
    /*width: 35px !important;*/
    /*}*/


</style>
<div class="row-fluid">
    <div class="span12 responsive" data-tablet="span12 fix-offset" data-desktop="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i><?php echo $this->lang->line('global_list'); ?></div>
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-hover" id="table1">
                    <thead>
                    <tr>
                        <th style="min-width:20px;text-align:center;"><?php echo $this->lang->line('global_no'); ?></th>
                        <th style="min-width: 50px;text-align:center;"><?php echo $this->lang->line('berkas_vendor_type'); ?></th>
                        <th style="min-width: 65px;text-align:center;"><?php echo $this->lang->line('berkas_project_code'); ?></th>
                        <th style="min-width: 90px;text-align:center;"><?php echo $this->lang->line('berkas_project_name'); ?></th>
                        <th style="min-width: 90px;text-align:center;">Nomor NPWP</th>
                        <th style="min-width: 90px;text-align:center;">Nama Penjual</th>
                        <th style="min-width: 90px;text-align:center;">Alamat Penjual</th>
                        <th style="min-width: 90px;text-align:center;">Nomor Faktur</th>
                        <th style="min-width: 70px;text-align:center;">Tanggal Faktur</th>
                        <th style="min-width: 50px;text-align:center;">Masa Pajak</th>
                        <th style="min-width: 50px;text-align:center;">Tahun Pajak</th>
                        <th style="min-width: 90px;text-align:center;">DPP</th>
                        <th style="min-width: 90px;text-align:center;">PPN</th>
                        <th style="min-width: 90px;text-align:center;">NOMOR NTPN</th>
                        <th style="min-width: 70px;text-align:center;">TGL BAYAR NTPN</th>
                        <th style="min-width: 90px;text-align:center;"><?php echo $this->lang->line('berkas_number'); ?></th>
                        <th style="min-width: 90px;text-align:center;"><?php echo $this->lang->line('berkas_barang_jasa_name'); ?></th>
<!--                        <th style="min-width: 90px;text-align:center;">--><?php //echo $this->lang->line('berkas_spk_po_number'); ?><!--</th>-->
<!--                        <th style="min-width: 70px;text-align:center;">--><?php //echo $this->lang->line('berkas_spk_po_date'); ?><!--</th>-->
<!--                        <th style="min-width: 90px;text-align:center;">--><?php //echo $this->lang->line('berkas_surat_jalan_lkp_number'); ?><!--</th>-->
<!--                        <th style="min-width: 70px;text-align:center;">--><?php //echo $this->lang->line('berkas_surat_jalan_lkp_date'); ?><!--</th>-->
<!--                        <th style="min-width: 90px;text-align:center;">--><?php //echo $this->lang->line('berkas_invoice_number'); ?><!--</th>-->
<!--                        <th style="min-width: 80px;text-align:center;">--><?php //echo $this->lang->line('berkas_payment_method'); ?><!--</th>-->
<!--                        <th style="min-width: 90px;text-align:center;">--><?php //echo $this->lang->line('berkas_vendor_code'); ?><!--</th>-->
<!--                        <th style="min-width: 50px;text-align:center;">Jenis Pajak</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <td colspan="12" class="dataTables_empty"><?php echo $this->lang->line('global_loading_data'); ?></td>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td><input type="hidden" /></td>
                        <td><input type="text" id="search_vendor_type_name" name="search_vendor_type_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_project_code" name="search_project_code" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_project_name" name="search_project_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_vendor_npwp" name="search_vendor_npwp" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_vendor_name" name="search_vendor_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_vendor_address" name="search_vendor_address" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_faktur_pajak_number" name="search_faktur_pajak_number" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_faktur_pajak_date" name="search_faktur_pajak_date" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_pajak_period" name="search_pajak_period" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_pajak_year" name="search_pajak_year" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_dpp_amount" name="search_dpp_amount" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_fp_amount" name="search_fp_amount" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_ntpn_number" name="search_ntpn_number" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_ntpn_date" name="search_ntpn_date" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_berkas_number" name="search_berkas_number" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
                        <td><input type="text" id="search_barang_jasa_name" name="search_barang_jasa_name" placeholder="<?php echo $this->lang->line('global_search'); ?>" class="search_init" /></td>
<!--                        <td><input type="text" id="search_spk_po_number" name="search_spk_po_number" placeholder="--><?php //echo $this->lang->line('global_search'); ?><!--" class="search_init" /></td>-->
<!--                        <td><input type="text" id="search_spk_po_date" name="search_spk_po_date" placeholder="--><?php //echo $this->lang->line('global_search'); ?><!--" class="search_init" /></td>-->
<!--                        <td><input type="text" id="search_surat_jalan_lkp_number" name="search_surat_jalan_lkp_number" placeholder="--><?php //echo $this->lang->line('global_search'); ?><!--" class="search_init" /></td>-->
<!--                        <td><input type="text" id="search_surat_jalan_lkp_date" name="search_surat_jalan_lkp_date" placeholder="--><?php //echo $this->lang->line('global_search'); ?><!--" class="search_init" /></td>-->
<!--                        <td><input type="text" id="search_invoice_number" name="search_invoice_number" placeholder="--><?php //echo $this->lang->line('global_search'); ?><!--" class="search_init" /></td>-->
<!--                        <td><input type="text" id="search_payment_method" name="search_payment_method" placeholder="--><?php //echo $this->lang->line('global_search'); ?><!--" class="search_init" /></td>-->
<!--                        <td><input type="text" id="search_vendor_code" name="search_vendor_code" placeholder="--><?php //echo $this->lang->line('global_search'); ?><!--" class="search_init" /></td>-->
<!--                        <td><input type="text" id="search_pajak_type" name="search_pajak_type" placeholder="--><?php //echo $this->lang->line('global_search'); ?><!--" class="search_init" /></td>-->
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="import-modal">
    <form id="import-form" class="form-horizontal" method="post" autocomplete="off">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
            <h3>Import NTPN</h3>
        </div>
        <div class="modal-body">
            <div class="scroller" style="height:50px" data-always-visible="1" data-rail-visible1="1">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group">
                            <label class="control-label">File (Excel)</label>
                            <div class="controls">
                                <div data-provides="fileupload" class="fileupload fileupload-new">
                                    <input type="hidden" name="ntpn" value="" />
                                    <div class="input-append ">
                                        <div class="uneditable-input">
                                            <i class="icon-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-file" data-title="IMPORT NTPN">
                                        <span class="fileupload-new">Pilih File</span>
                                        <span class="fileupload-exists">Ubah</span>
                                        <input type="file" class="default" name="ntpn"/>
                                    </span>
                                        <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Hapus</a>
                                    </div>
                                </div>
                                <div id="file_link" style="line-height:10px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <input type="hidden" name="is_ajax" value="1" />
            <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo $this->lang->line('global_save'); ?></button>
            <button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
        </div>
    </form>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="export-sap-modal">
    <form id="export-sap-form" class="form-horizontal" method="post" autocomplete="off" action="<?php echo app_backend_url('reports/arus_barang/export_sap'); ?>"target="_blank">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
            <h3>Filter Export SAP</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="col-md-6 ">
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject bold font-grey-gallery uppercase"> Tanggal Input Berkas </span>
                                <!--<span class="caption-helper">more samples...</span>-->
                            </div>
                            <div class="tools">
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="control-group">
                                <label class="control-label">Mulai Dari</label>
                                <div class="controls">
                                    <input type="text" id="start" name="start" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Sampai Dengan</label>
                                <div class="controls">
                                    <input type="text" id="end" name="end" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END GRID PORTLET-->
                    <div class="col-md-6 ">
                        <!-- BEGIN Portlet PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold font-grey-gallery uppercase"> Tanggal Faktur</span>
                                    <!--<span class="caption-helper">more samples...</span>-->
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"> </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="control-group">
                                    <label class="control-label">Mulai Dari</label>
                                    <div class="controls">
                                        <input type="text" id="faktur_start" name="faktur_start" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Sampai Dengan</label>
                                    <div class="controls">
                                        <input type="text" id="faktur_end" name="faktur_end" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <!-- BEGIN Portlet PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold font-grey-gallery uppercase"> Tanggal Selesai</span>
                                    <!--<span class="caption-helper">more samples...</span>-->
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"> </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="control-group">
                                    <label class="control-label">Mulai Dari</label>
                                    <div class="controls">
                                        <input type="text" id="finish_start" name="finish_start" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Sampai Dengan</label>
                                    <div class="controls">
                                        <input type="text" id="finish_end" name="finish_end" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
<!--            <input type="hidden" name="is_ajax" value="1" />-->
            <button type="submit" class="btn blue"><i class="icon-ok"></i> Export</button>
            <input type="hidden" id="berkas_status" name="berkas_status" value="">
            <button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
        </div>
    </form>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="status-berkas-modal">
    <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
            <h3>Pilih Status Berkas</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-6 center">
                <!-- BEGIN Portlet PORTLET-->
                <button type="button" class="btn blue" id="berkas-blm-selesai">Berkas Belum Selesai</button>&nbsp;&nbsp;
                <button type="button" class="btn green" id="berkas-selesai">Berkas Selesai</button> &nbsp;
                <button type="button" class="btn yellow" id="semua-berkas">Semua Berkas</button>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="export-tarra-fm-modal">
    <form id="export-tarra-fm-form" class="form-horizontal" method="post" autocomplete="off" action="<?php echo app_backend_url('reports/arus_barang/export_tarra_fm'); ?>"target="_blank">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
            <h3>Filter Export TARRA-FM</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="col-md-6 ">
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject bold font-grey-gallery uppercase"> Tanggal Input Berkas </span>
                            </div>
                            <div class="tools">
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="control-group">
                                <label class="control-label">Mulai Dari</label>
                                <div class="controls">
                                    <input type="text" id="start_tarra" name="start_tarra" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Sampai Dengan</label>
                                <div class="controls">
                                    <input type="text" id="end_tarra" name="end_tarra" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END GRID PORTLET-->
                    <div class="col-md-6 ">
                        <!-- BEGIN Portlet PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold font-grey-gallery uppercase"> Tanggal Faktur</span>
                                    <!--<span class="caption-helper">more samples...</span>-->
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"> </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="control-group">
                                    <label class="control-label">Mulai Dari</label>
                                    <div class="controls">
                                        <input type="text" id="faktur_start_tarra" name="faktur_start_tarra" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Sampai Dengan</label>
                                    <div class="controls">
                                        <input type="text" id="faktur_end_tarra" name="faktur_end_tarra" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <!-- BEGIN Portlet PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold font-grey-gallery uppercase"> Tanggal Selesai</span>
                                    <!--<span class="caption-helper">more samples...</span>-->
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"> </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="control-group">
                                    <label class="control-label">Mulai Dari</label>
                                    <div class="controls">
                                        <input type="text" id="finish_start_tarra" name="finish_start_tarra" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Sampai Dengan</label>
                                    <div class="controls">
                                        <input type="text" id="finish_end_tarra" name="finish_end_tarra" class="m-wrap large date-picker" size="16" maxlength="10" placeholder="<?php echo $this->lang->line('global_format_date'); ?>: <?php echo $this->site_config['app_def_date_format']; ?>" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn blue"><i class="icon-ok"></i> Export</button>
            <input type="hidden" id="berkas_status_tarra" name="berkas_status_tarra" value="">
            <button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
        </div>
    </form>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="status-berkas-modal-tarra">
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
        <h3>Pilih Status Berkas</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-6 center">
                <!-- BEGIN Portlet PORTLET-->
                <button type="button" class="btn blue" id="berkas-blm-selesai-tarra">Berkas Belum Selesai</button>&nbsp;&nbsp;
                <button type="button" class="btn green" id="berkas-selesai-tarra">Berkas Selesai</button> &nbsp;
                <button type="button" class="btn yellow" id="semua-berkas-tarra">Semua Berkas</button>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_cancel'); ?></button>
    </div>
</div>
<script>
    jQuery(document).ready(function(){
        var aSelected = [];
        var asInitVals = [];

        // init grid
        var oTable = jQuery('#table1');
        App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
        oTable.dataTable({
            "autoWidth": false,
            "sPaginationType": "bootstrap",
            "aaSorting": [],
            "aoColumnDefs": [
                {"bSortable": false, "aTargets": [0], "bSearchable": false, "aTargets": [0]}
            ],
            "bFilter" : MODULE_FUNCTION.search ? true : false,
            "bServerSide": true,
            "sAjaxSource": "<?php echo app_backend_url('reports/arus_barang/get_list'); ?>",
            "sServerMethod": "POST",
            "bStateSave": true,
            "fnStateSave": function(oSettings, oData) {
                ceki.fnSaveDtView(oSettings, oData);
            },
            "sScrollX": "100%",
//            "sScrollXInner": "100%",
            "bScrollCollapse": true,
            "fnStateLoad": function(oSettings) {
//				dataFN = JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname));
//				if (dataFN != null) {
//                    jQuery('#search_swift_number').val(dataFN.aoSearchCols[2].sSearch);
//                    jQuery('#search_angsuran_ke').val(dataFN.aoSearchCols[3].sSearch);
//                    jQuery('#search_bap_amount').val(dataFN.aoSearchCols[4].sSearch);
//                    jQuery('#search_bap_number').val(dataFN.aoSearchCols[5].sSearch);
//                    jQuery('#search_division_name').val(dataFN.aoSearchCols[6].sSearch);
//                    jQuery('#search_project_code').val(dataFN.aoSearchCols[7].sSearch);
//                    jQuery('#search_project_name').val(dataFN.aoSearchCols[8].sSearch);
//				}
//				return ceki.fnLoadDtView(oSettings);
            },
            "oLanguage": {
                "sLengthMenu": "<?php echo $this->lang->line('global_DT_sLengthMenu'); ?>",
                "sZeroRecords": "<?php echo $this->lang->line('global_DT_sZeroRecords'); ?>",
                "sInfo": "<?php echo $this->lang->line('global_DT_sInfo'); ?>",
                "sInfoEmpty": "<?php echo $this->lang->line('global_DT_sInfoEmpty'); ?>",
                "sInfoFiltered": "(<?php echo $this->lang->line('global_DT_sInfoFiltered'); ?>)",
                "sSearch": "<?php echo $this->lang->line('global_search'); ?> :",
                "oPaginate": {
                    "sPrevious": "<?php echo $this->lang->line('global_prev'); ?>",
                    "sNext": "<?php echo $this->lang->line('global_next'); ?>"
                }
            },
            "fnServerParams": function (aoData) {
                aoData.push({"name": "is_ajax", "value": true});
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if(jQuery('#paramFilter').val() != '') {
                    aoData.push({"name": "paramFilter", "value": jQuery('#paramFilter').val()});
                }
                if(!jQuery(this).children('.blockUI').length){
                    App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
                }
                jQuery.post(sSource, aoData, function(obj) {
                    if(obj.success == false){
                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: obj.message,
                            sticky: false,
                            time: 3000
                        });

                        if(obj.expired == true) {
                            setTimeout(function(){window.location= '<?php echo app_backend_url('login'); ?>'},1000);
                        }
                    }
                    else{
                        fnCallback(obj)
                    }
                }, 'json');
            },
            "fnDrawCallback": function(oSettings) {
                if(!jQuery(this).children('.blockUI').length){
                    App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
                }

                App.initUniform();
                App.initPopovers();
                App.unblockUI(jQuery(this));
                jQuery('.checkall').parent().removeClass('checked');
                // jQuery('tr.sb0').css("cssText", "background-color: #eef8c5 !important;")
            },
            "aoColumns": [
                { "mData": "rnum", "sClass": "center"},
                { "mData": "vendor_type_name", "sClass": "left"},
                { "mData": "project_code", "sClass": "center"},
                { "mData": "project_name"},
                { "mData": "vendor_npwp"},
                { "mData": "vendor_name"},
                { "mData": "vendor_address"},
                { "mData": "faktur_pajak_number"},
                { "mData": "faktur_pajak_date", "sClass": "center"},
                { "mData": "pajak_period", "sClass": "center"},
                { "mData": "pajak_year", "sClass": "center"},
                { "mData": "dpp_amount", "sClass": "right"},
                { "mData": "fp_amount", "sClass": "right"},
                { "mData": "ntpn_number", "sClass": "center"},
                { "mData": "ntpn_date", "sClass": "center"},
                { "mData": "berkas_number"},
                { "mData": "barang_jasa_name","sClass": "left"},
                // { "mData": "spk_po_number","sClass": "left"},
                // { "mData": "spk_po_date","sClass": "left"},
                // { "mData": "surat_jalan_lkp_number","sClass": "left"},
                // { "mData": "surat_jalan_lkp_date","sClass": "left"},
                // { "mData": "invoice_number","sClass": "left"},
                // { "mData": "payment_method","sClass": "center"},
                // { "mData": "vendor_code","sClass": "center"},
                // { "mData": "pajak_type","sClass": "center"},
            ],
            "aLengthMenu": [
                [5, 15, 20, 50, 100, 200, 300, 400, 500, -1],
                [5, 15, 20, 50, 100, 200, 300, 400, 500, "All"]
            ],
            "iDisplayLength": 5,
            "sDom": "<'dataTables_toolbar'><'row-fluid'<'span6'l><'span6'f>r><'table-scrollable't><'row-fluid'<'span6'i><'span6'p>>",
            "fnPreDrawCallback": function (oSettings) {
                if (jQuery('.dataTables_length select').val() == -1) {
                    if(oTable.fnSettings().fnRecordsTotal() > 500) {
                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: '<?php echo $this->lang->line('global_limit_record_page'); ?>',
                            sticky: false,
                            time: 3000
                        });

                        App.unblockUI(jQuery(this));
                        return false;
                    }
                }

            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            },
//            "fnInitComplete": function () {
//                new FixedColumns( oTable, {
//                    "iLeftColumns": 8,
//                    "iLeftWidth":750,
//                    heightMatch: "auto",
//                    "fnDrawCallback" : function () {
//                        App.unblockUI(jQuery('.DTFC_LeftWrapper table'));
//                        fnTbarDisableEnable();
//                        jQuery('.DTFC_LeftWrapper table').find('input[type=checkbox]').click(function(){
//                            var id = this.value;
//                            var checked = jQuery(this).is(':checked');
//                            fnSelected(this, id, checked);
//                            fnTbarDisableEnable();
//                        });
//
//                        for(var i = 0; i < aSelected.length; i++) {
//                            jQuery('.DTFC_LeftWrapper table').find("input[type=checkbox][value="+aSelected[i]+"]").prop("checked",true);
//                        }
//                    }
//                } );
//            }

        });

        jQuery(window).on('resize', function (e) {
            e.preventDefault();
            oTable.fnAdjustColumnSizing();
        });


//        jQuery('.sidebar-toggler').click(function (e) {
//            e.preventDefault();
//            oTable.fnAdjustColumnSizing();
//        });

        jQuery('#table1_wrapper .dataTables_filter input').addClass("m-wrap");
        jQuery('#table1_wrapper .dataTables_length select').addClass("m-wrap small");
        jQuery('#table1_wrapper .dataTables_length select').select2();

        jQuery('#table1_filter input').unbind();
        jQuery('#table1_filter input').bind('keyup', function(e) {
            if(e.keyCode == 13) {
                console.log('search boy')
                App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
                oTable.fnFilter(this.value);
            }
        });

        jQuery('tfoot input').off();
        jQuery("tfoot").on('keyup', 'input', function (e) {
            if(e.keyCode == 13) {
                App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
                oTable.fnFilter(this.value, jQuery("tfoot :input").index(this));
            }
            e.preventDefault();
        });

        jQuery("tfoot select").change(function (e) {
            App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
            var $this = $(this);
            oTable.fnFilter($this.val(), jQuery("tfoot :input").index($this));
            e.preventDefault();
        });

        jQuery("tfoot input, tfoot select").each( function (i) {
            asInitVals[i] = this.value;
        });

        jQuery("tfoot input, tfoot select").blur( function (i) {
            if (this.value == ""){
                this.className = "search_init";
                //this.value = asInitVals[jQuery("tfoot input").index(this)];
            }
        });

        jQuery('.checkall').click(function(){

            var ch = oTable.find('input[type=checkbox]');

            var checked = jQuery(this).is(':checked');

            ch.each(function(){
                if(this.value != 'on') {
                    fnSelected(this, this.value, checked , true);
                }
            });

            fnTbarDisableEnable();
        });

        function fnSelected(cmp, id, checked, all){
            var index = jQuery.inArray(id, aSelected);
            if(id == 'on') return;
            if ( index === -1 ) {
                if(all){
                    if(checked){
                        aSelected.push(id);
                    }
                }else{
                    aSelected.push(id);
                }
            } else {
                if(all){
                    if(!checked){
                        aSelected.splice(index, 1);
                    }
                }else{
                    aSelected.splice(index, 1);
                }
            }

            if(checked) {
                jQuery(cmp).attr('checked',true);
                jQuery(cmp).parent().addClass('checked');
                jQuery(cmp).parents('tr').addClass('selected');
            }else{
                jQuery(cmp).attr('checked',false);
                jQuery(cmp).parent().removeClass('checked');
                jQuery(cmp).parents('tr').removeClass('selected');
            }

            console.log(aSelected);
        }

        function fnTbarDisableEnable(){
            var rowSelected = aSelected.length;

            if(rowSelected > 0){
                if(rowSelected > 1){
                    jQuery('#button-edit').attr("disabled", "disabled");
                }else{
                    jQuery('#button-edit').removeAttr("disabled");
                }
                jQuery('#button-delete').removeAttr("disabled");
                jQuery('#button-kirim-scm').removeAttr("disabled");
                jQuery('#button-received-dvo').removeAttr("disabled");
                jQuery('#button-park-invoice-selesai').removeAttr("disabled");
            }else{
                jQuery('#button-edit').attr("disabled", "disabled");
                jQuery('#button-delete').attr("disabled", "disabled");
                jQuery('#button-kirim-scm').attr("disabled", "disabled");
                jQuery('#button-received-dvo').attr("disabled", "disabled");
                jQuery('#button-park-invoice-selesai').attr("disabled", "disabled");
            }
        }

        jQuery("div.dataTables_toolbar").html(' '+
            '<button id="button-refresh" class="btn purple"><i class="icon-refresh"></i> <?php echo $this->lang->line('global_refresh'); ?></button>&nbsp;' +
            '<div style="float:right;"><button id="button-clear" class="btn black"><i class="icon-share"></i></a></div>' +
            '<div style="float:right;"><button id="button-import" class="btn black" href="#import-modal" data-toggle="modal" style="visibility:hidden;"><i class="icon-cloud-upload"></i></a></div>'+
            '<div style="float:right;"><button id="button-export" style="visibility:hidden;" class="btn green"> Excel</button></div>' +
            '<div style="float:right;"><button id="button-export-sap" style="visibility:hidden;background-color: #609cff;" class="btn blue" href="#export-sap-modal" data-toggle="modal"> SAP</button></div>' +
            '<div style="float:right;"><button id="button-export-tarra-fm" style="visibility:hidden" class="btn yellow" href="#export-tarra-fm-modal" data-toggle="modal"> TARRA-FM</button></div>');

        jQuery('#button-refresh').click(function(){
            App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
            oTable.fnDraw();
        });

        jQuery('#button-clear').click(function(){
            ceki.fnResetDtView();
            ceki.fnRefresh()
        });

        jQuery( "#button-export" ).click(function() {
            var params = oTable._fnAjaxParameters();
            var form = document.createElement('form');

            var url = window.location.href;
            var segment = url.split("/").length - 1 - (url.indexOf("http://")==-1?0:2);
            console.log(segment);

            form.method='POST';
            form.action= '<?php echo app_backend_url("reports/arus_barang/export_xls"); ?>';
            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", params[key].name);
                    hiddenField.setAttribute("value", params[key].value);

                    form.appendChild(hiddenField);
                }
            }

            form.target='_blank';
            document.body.appendChild(form);
            form.submit()
        });

        if(MODULE_FUNCTION.add && jQuery('#paramFilter').val() == ''){
            jQuery("#button-add").css('visibility','visible');
        }
        if(MODULE_FUNCTION.export_xls){
            jQuery("#button-export").css('visibility','visible');
        }
        if(MODULE_FUNCTION.export_sap){
            jQuery("#button-export-sap").css('visibility','visible');
        }
        if(MODULE_FUNCTION.import_xls){
            jQuery("#button-import").css('visibility','visible');
        }

        if(MODULE_FUNCTION.tarra_fm){
            jQuery("#button-export-tarra-fm").css('visibility','visible');
        }


        jQuery.validator.addMethod(
            "indonesianDate",
            function(value, element) {
                if(value != ''){
                    return value.match(/^\d\d?\-\d\d?\-\d\d\d\d$/);
                } else {
                    return true;
                }
            },
            "<?php echo $this->lang->line('global_error_date'); ?>"
        );

        var import_ntpn = {};
        var import_ntpn_msg = {};

        import_ntpn["ntpn"] = { required: true, extension: "xls|xlsx"};
        import_ntpn_msg["ntpn"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            extension: "<?php echo $this->lang->line('global_format_file'); ?> <?php echo $this->lang->line('global_must_be'); ?> xls/xlsx",
        };

        jQuery("#import-form").validate({
            onsubmit: false,
            ignore: "",
            rules: import_ntpn,
            messages: import_ntpn_msg,
            highlight: function(label) {},
            success: function(label) {
                console.log("success bro")
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: '<?php echo $this->lang->line('global_error_form'); ?>',
                        sticky: false,
                        time: 3000
                    });
                    return false;
                }
                e.preventDefault();
            }
        });

        jQuery('#import-form').submit(function(e) {
//            jQuery(this).find(':button').attr("disabled", 'disabled');
            if(jQuery(this).valid()){
                $('#import-modal').modal('hide');
                var form = jQuery(this);
                ajaxSubmit(form);
            }else{
                jQuery(this).find(':button').attr("disabled",false);
            }

            e.preventDefault();
        });

        function ajaxSubmit(form) {

            var formData = new FormData(form[0]);
            jQuery.ajax({
                url: '<?php echo app_backend_url('reports/arus_barang/import'); ?>',
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function (obj) {
                    if(obj.success == true) {
                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: obj.message,
                            sticky: false,
                            time: 3000
                        });


                        jQuery('.form-actions').find(':button').prop("disabled",false);
                        if(typeof sock != 'undefined' && IS_SOCK == 1) {
                            sock.send();
                        }

                        App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
                        oTable.fnDraw();

                    } else {
                        jQuery('.form-actions').each(function(){
                            jQuery(this).find(':button').removeAttr("disabled");
                        });

                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: "Import Data Gagal",
                            sticky: false,
                            time: 3000
                        });

                        App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
                        oTable.fnDraw();
                    }

                    $('#import-form').trigger("reset");
                }
            });
        }

        jQuery.validator.addMethod(
            "higherThanAwal",
            function(value, element) {
                if(value != ''){
                    var end = value.split("-");
                    var endDate = new Date(end[2], end[1] - 1, end[0]);
                    if($("#start").val()){
                        var awal = $("#start").val().split("-");
                        var awalDate = new Date(awal[2], awal[1] - 1, awal[0]);
                        return endDate >= awalDate
                    }
                    return true
                } else {
                    return true;
                }
            },
            "Harus Sama atau Lebih Besar dari Tanggal Awal"
        );

        jQuery.validator.addMethod(
            "lowerThanEnd",
            function(value, element) {
                if(value != ''){
                    var start = value.split("-");
                    var startDate = new Date(start[2], start[1] - 1, start[0]);
                    if($("#end").val()){
                        var end = $("#end").val().split("-");
                        var endDate = new Date(end[2], end[1] - 1, end[0]);
                        return startDate <= endDate
                    }
                    return true
                } else {
                    return true;
                }
            },
            "Harus Sama atau Lebih Kecil dari Tanggal Akhir"
        );

        var export_sap = {};
        var export_sap_msg = {};

        // Tanggal Input
        export_sap["start"] = { required : function (element)
            {
                if($('#end').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_sap_msg["start"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        export_sap["end"] = { required : function (element)
            {
                if($('#start').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_sap_msg["end"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        //Tanggal Faktur
        export_sap["faktur_start"] = { required : function (element)
            {
                if($('#faktur_end').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_sap_msg["faktur_start"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        export_sap["faktur_end"] = { required : function (element)
            {
                if($('#faktur_start').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_sap_msg["faktur_end"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        //Tanggal Selesai
        export_sap["finish_start"] = { required : function (element)
            {
                if($('#finish_end').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_sap_msg["finish_start"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        export_sap["finish_end"] = { required : function (element)
            {
                if($('#finish_start').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_sap_msg["finish_end"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        jQuery("#export-sap-form").validate({
            ignore: "",
            rules: export_sap,
            messages: export_sap_msg,
            highlight: function(label) {},
            success: function(label) {

            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: '<?php echo $this->lang->line('global_error_form'); ?>',
                        sticky: false,
                        time: 3000
                    });
                    return false;
                }
                e.preventDefault();
            },
            submitHandler: function(form) {
                if(!$('#finish_start').val() && !$('#finish_end').val()){
                    $('#status-berkas-modal').modal('show');

                    $('#berkas-blm-selesai').click(function () {
                        $('#status-berkas-modal').modal('hide');
                        $('#berkas_status').val('belum');
                        form.submit();
                    });

                    $('#berkas-selesai').click(function () {
                        $('#status-berkas-modal').modal('hide');
                        $('#berkas_status').val('selesai');
                        form.submit();
                    });

                    $('#semua-berkas').click(function () {
                        $('#status-berkas-modal').modal('hide');
                        $('#berkas_status').val('');
                        form.submit();
                    });

                }else{
                    $('#berkas_status').val('')
                    form.submit();
                }
            }
        });

        jQuery.validator.addMethod(
            "higherThanAwalTarra",
            function(value, element) {
                if(value != ''){
                    var end = value.split("-");
                    var endDate = new Date(end[2], end[1] - 1, end[0]);
                    if($("#start_tarra").val()){
                        var awal = $("#start_tarra").val().split("-");
                        var awalDate = new Date(awal[2], awal[1] - 1, awal[0]);
                        return endDate >= awalDate
                    }
                    return true
                } else {
                    return true;
                }
            },
            "Harus Sama atau Lebih Besar dari Tanggal Awal"
        );

        jQuery.validator.addMethod(
            "lowerThanEndTarra",
            function(value, element) {
                if(value != ''){
                    var start = value.split("-");
                    var startDate = new Date(start[2], start[1] - 1, start[0]);
                    if($("#end_tarra").val()){
                        var end = $("#end_tarra").val().split("-");
                        var endDate = new Date(end[2], end[1] - 1, end[0]);
                        return startDate <= endDate
                    }
                    return true
                } else {
                    return true;
                }
            },
            "Harus Sama atau Lebih Kecil dari Tanggal Akhir"
        );

        var export_tarra = {};
        var export_tarra_msg = {};

        // Tanggal Input
        export_tarra["start_tarra"] = { required : function (element)
            {
                if($('#end_tarra').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_tarra_msg["start_tarra"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        export_tarra["end_tarra"] = { required : function (element)
            {
                if($('#start_tarra').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_tarra_msg["end_tarra"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        //Tanggal Faktur
        export_tarra["faktur_start_tarra"] = { required : function (element)
            {
                if($('#faktur_end_tarra').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_tarra_msg["faktur_start_tarra"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        export_tarra["faktur_end_tarra"] = { required : function (element)
            {
                if($('#faktur_start_tarra').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_tarra_msg["faktur_end_tarra"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        //Tanggal Selesai
        export_tarra["finish_start_tarra"] = { required : function (element)
            {
                if($('#finish_end_tarra').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_tarra_msg["finish_start_tarra"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        export_tarra["finish_end_tarra"] = { required : function (element)
            {
                if($('#finish_start_tarra').val()){
                    return true
                }
                return false;
            }, indonesianDate: true};

        export_tarra_msg["finish_end_tarra"] = {
            required: "<?php echo $this->lang->line('global_error_required_field2'); ?>",
            indonesianDate: "<?php echo $this->lang->line('global_error_date'); ?>"
        };

        jQuery("#export-tarra-fm-form").validate({
            ignore: "",
            rules: export_tarra,
            messages: export_tarra_msg,
            highlight: function(label) {},
            success: function(label) {

            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    jQuery.gritter.add({
                        title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                        text: '<?php echo $this->lang->line('global_error_form'); ?>',
                        sticky: false,
                        time: 3000
                    });
                    return false;
                }
                e.preventDefault();
            },
            submitHandler: function(form) {
                if(!$('#finish_start_tarra').val() && !$('#finish_end_tarra').val()){
                    $('#status-berkas-modal-tarra').modal('show');

                    $('#berkas-blm-selesai-tarra').click(function () {
                        $('#status-berkas-modal-tarra').modal('hide');
                        $('#berkas_status_tarra').val('belum');
                        form.submit();
                    });

                    $('#berkas-selesai-tarra').click(function () {
                        $('#status-berkas-modal-tarra').modal('hide');
                        $('#berkas_status_tarra').val('selesai');
                        form.submit();
                    });

                    $('#semua-berkas-tarra').click(function () {
                        $('#status-berkas-modal-tarra').modal('hide');
                        $('#berkas_status_tarra').val('');
                        form.submit();
                    });

                }else{
                    $('#berkas_status_tarra').val('')
                    form.submit();
                }
            }
        });

//         jQuery('#export-sap-form').submit(function(e) {
// //            jQuery(this).find(':button').attr("disabled", 'disabled');
//             if(jQuery(this).valid()){
//                 $('#export-sap-modal').modal('hide');
//                 var form = jQuery(this);
//                 $(this).submit();
//                 // ajaxSubmitExportSap(form);
//
//             }else{
//                 jQuery(this).find(':button').attr("disabled",false);
//             }
//
//             e.preventDefault();
//         });

        function ajaxSubmitExportSap(form) {

            var formData = new FormData(form[0]);
            jQuery.ajax({
                url: '<?php echo app_backend_url('reports/arus_barang/export_tarra'); ?>',
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function (obj) {
                    if(obj.success == true) {
                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_info'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: obj.message,
                            sticky: false,
                            time: 3000
                        });


                        jQuery('.form-actions').find(':button').prop("disabled",false);
                        if(typeof sock != 'undefined' && IS_SOCK == 1) {
                            sock.send();
                        }

                        App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');


                    } else {
                        jQuery('.form-actions').each(function(){
                            jQuery(this).find(':button').removeAttr("disabled");
                        });

                        jQuery.gritter.add({
                            title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
                            text: "Import Data Gagal",
                            sticky: false,
                            time: 3000
                        });

                        App.blockUI(oTable, true, '<?php echo $this->lang->line('global_loading_data'); ?>');
                    }

                    $('#export-sap-form').trigger("reset");
                }
            });
        }

        jQuery('.date-picker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });
    });
</script>