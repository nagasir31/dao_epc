<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
.form-horizontal .control-label {
	width:260px;
	padding-right: 10px;
}

.thumb-img{
	height:200px;
	margin-left:auto;
	margin-right:auto;
}

body .modal#detail_pembayaran_modal {
  width: 70%; /* desired relative width */
  left: 15%; /* (100%-width)/2 */
  /* place center */
  margin-left:auto;
  margin-right:auto; 
}

</style>
<div id="main-container">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-credit-card"></i><?php echo $this->lang->line('global_report'); ?> <?php echo $this->lang->line('berkas_spm'); ?> </div>
		</div>
		<div class="portlet-body form">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('success_alert')): ?>
				<div class="alert alert-success">
					<button data-dismiss="alert" class="close"></button>
					<?php echo $this->session->flashdata('success_alert'); ?>
				</div>
			<?php endif; ?>
			<?php if($this->session->flashdata('error_alert')): ?>
				<div class="alert alert-error">
					<button data-dismiss="alert" class="close"></button>
					<?php echo $this->session->flashdata('error_alert'); ?>
				</div>
			<?php endif; ?>
			<div class="row-fluid">
				<div class="span6 offset3">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><?php echo $this->lang->line('global_parameter'); ?></div>
							<div class="tools">
								<a class="collapse" href="javascript:;"></a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="report-search">
								<form id="report_search" autocomplete="off" method="post" action="#">
									<div class="row-fluid">
										<div class="span12">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('global_report_type'); ?></label>
												<div class="controls">
													<select id="report_type" name="report_type" class="m-wrap span12">
														<option value="1"><?php echo $this->lang->line('global_report_summary'); ?></option>
														<option value="2"><?php echo $this->lang->line('global_report_detail'); ?></option>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row-fluid">
										<div class="span12">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('berkas_project'); ?></label>
												<div class="controls">
													<select id="project_code" name="project_code" class="m-wrap span12">
														<option value=""><?php echo $this->lang->line('global_all'); ?></option>
														<?php foreach($ref_proyek as $dt): ?>
														<option value="<?php echo $dt['project_code']; ?>"><?php echo $dt['project_name']; ?> (<?php echo $dt['project_code']; ?>)</option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row-fluid" id="field_vendor" style="display:block;">
										<div class="span12">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('berkas_vendor_name'); ?></label>
												<div class="controls">
													<select id="vendor_name" name="vendor_name" class="m-wrap span12">
														<option value=""><?php echo $this->lang->line('global_all'); ?></option>
														<?php foreach($ref_vendor as $dt): ?>
														<option value="<?php echo $dt['vendor_name']; ?>"><?php echo $dt['vendor_name']; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row-fluid">
										<div class="span6">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('berkas_payment_method'); ?></label>
												<div class="controls">
													<select id="payment_method" name="payment_method" class="m-wrap span12">
														<option value=""><?php echo $this->lang->line('global_all'); ?></option>
														<?php foreach($ref_payment_method as $dt): ?>
														<option value="<?php echo $dt['name']; ?>"><?php echo $dt['name']; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
										
										<div class="span6">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('global_period'); ?></label>
												<div class="controls">
													<select id="period" name="period"" class="m-wrap span12">
														<option value="weekly"><?php echo $this->lang->line('global_weekly'); ?></option>
														<option value="monthly"><?php echo $this->lang->line('global_monthly'); ?></option>
														<option value="yearly"><?php echo $this->lang->line('global_yearly'); ?></option>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row-fluid" id="field_weekly">
										<div class="span6">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('global_month'); ?></label>
												<div class="controls">
													<select name="weekly_month" id="weekly_month" class="m-wrap span12">
														<?php foreach($ref_months as $key => $dt): ?>
														<option <?php echo $key == $current_month ? 'selected="selected"': ''; ?> value="<?php echo $key; ?>"><?php echo $dt; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
										
										<div class="span6">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('global_year'); ?></label>
												<div class="controls">
													<select name="weekly_year" id="weekly_year" class="m-wrap span12">
														<?php foreach($ref_years as $dt): ?>
														<option <?php echo $dt == $current_year ? 'selected="selected"': ''; ?> value="<?php echo $dt; ?>"><?php echo $dt; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row-fluid" id="field_monthly" style="display:none;">
										<div class="span12">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('global_year'); ?></label>
												<div class="controls">
													<select name="monthly_year" id="monthly_year" class="m-wrap span12">
														<?php foreach($ref_years as $dt): ?>
														<option <?php echo $dt == $current_year ? 'selected="selected"': ''; ?> value="<?php echo $dt; ?>"><?php echo $dt; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row-fluid" id="field_yearly" style="display:none;">
										<div class="span6">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('global_year'); ?> <?php echo $this->lang->line('global_start'); ?></label>
												<div class="controls">
													<select name="yearly_syear" id="yearly_syear" class="m-wrap span12">
														<?php foreach($ref_years as $dt): ?>
														<option <?php echo $dt == $current_year ? 'selected="selected"': ''; ?> value="<?php echo $dt; ?>"><?php echo $dt; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
										
										<div class="span6">
											<div class="control-group">
												<label class="control-label"><?php echo $this->lang->line('global_year'); ?> <?php echo $this->lang->line('global_end'); ?></label>
												<div class="controls">
													<select name="yearly_eyear" id="yearly_eyear" class="m-wrap span12">
														<?php foreach($ref_years as $dt): ?>
														<option <?php echo $dt == $current_year ? 'selected="selected"': ''; ?> value="<?php echo $dt; ?>"><?php echo $dt; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="clearfix space20"></div>
									<input type="hidden" id="page" name="page" />
									<button id="search_button" type="submit" class="btn green btn-block"><?php echo strtoupper($this->lang->line('global_search')); ?> <i class="m-icon-swapright m-icon-white"></i></button>
									<div class="clearfix space20"></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>    
			<div class="clearfix space20"></div>			
			
			<div class="row-fluid">
				<div class="span12">
					<div id="result" style="min-height:100px;"></div>
				</div>
				<div class="clearfix space20"></div> <br>
			</div>
		</div>
	</div>
	<div class="clearfix space20"></div> <br>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade in" id="detail_pembayaran_modal">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
		<h3><?php echo $this->lang->line('berkas_detail_pembayaran'); ?> - <span id="pembayaran_title"></span></h3>
	</div>
	<div class="modal-body">
		<div class="scroller" style="height:400px" data-always-visible="1" data-rail-visible1="1">
		<table class="table table-hover" id="table_detail_pembayaran" >
			<thead>
				<tr>
					<th style="width:7%;text-align:center;"><?php echo $this->lang->line('berkas_number'); ?></th>
					<th style="width:20%;text-align:left;"><?php echo $this->lang->line('berkas_vendor_name'); ?></th>
					<th style="width:7%;text-align:center;"><?php echo $this->lang->line('berkas_termin'); ?></th>
					<th style="width:15%;text-align:left;"><?php echo $this->lang->line('berkas_nominal'); ?> ( Rp )</th>
					<th style="width:25%;text-align:left;" class="hidden-480"><?php echo $this->lang->line('global_note'); ?></th>
					<th style="width:10%;text-align:center;" class="hidden-480"><?php echo $this->lang->line('berkas_payment_date'); ?></th>
					<th style="width:10%;text-align:center;" class="hidden-480"><?php echo $this->lang->line('berkas_invoice_date'); ?></th>
				</tr>
			</thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<td style="width:7%;text-align:right;" colspan="3"><?php echo $this->lang->line('global_total'); ?>:</td>
					<td><b><span id="total_nominal_pembayaran">-</span></b></td>
					<td colspan="3" class="hidden-480"></td>
				</tr>
			</tfoot>
		</table>
		</div>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" aria-hidden="true" class="btn"><?php echo $this->lang->line('global_close'); ?></button>
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		
		jQuery('#period').change(function(e) {
			var val = this.value;
			if(val == 'yearly') {
				jQuery("#field_weekly").hide();
				jQuery("#field_monthly").hide();
				jQuery("#field_yearly").show();
			} else if(val == 'monthly') {
				jQuery("#field_weekly").hide();
				jQuery("#field_monthly").show();
				jQuery("#field_yearly").hide();
			} else {
				jQuery("#field_weekly").show();
				jQuery("#field_monthly").hide();		
				jQuery("#field_yearly").hide();
			}
		});
		
		jQuery('.spm_report.pagination ul li a').live('click', function(e) {
			e.preventDefault();
			if(!jQuery(this).parent().hasClass('active')) {
				var page = jQuery(this).attr("href").split(/\//g).pop();
				fnGetData(page);
			}
			return false;
		});
		
		jQuery('#report_search').submit(function(e) {
			e.preventDefault();
			fnGetData();
		});
		
		
		fnGetData();
		
		function fnGetData(page) {
			var page = page || 0;
			
			App.scrollTo(jQuery("#result"));
			App.blockUI(jQuery("#result"), true, '<?php echo $this->lang->line('global_loading_data'); ?>');
				
			var cAjax = new ceki.fnAjax({
				dataType : 'html',
				url :  APP_BACKEND_URL + 'reports/spm/get_report/'+page,
				data : jQuery('#report_search').serialize(),
				successCallBack : function(html) {
					App.unblockUI(jQuery("#result"));
				},
				processHtml: function(html) {
					jQuery('#result').html(html);
				},
				successErrorCallBack : function(obj) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error'); ?>',
						sticky: false,
						time: 3000
					});
					App.unblockUI(jQuery("#result"));
				}
			});
		}
		
		jQuery(".get_detail_pembayaran").live('click', function(){
			
			var title = jQuery(this).attr('data-project_code')+'/'+jQuery(this).attr('data-period');
			jQuery("#pembayaran_title").html(title);
			jQuery('#detail_pembayaran_modal').modal('show');
			
			jQuery.ajax({
				url :  APP_BACKEND_URL + 'reports/spm/get_detail_pembayaran',
				type: 'POST',
				data : ({
					project_code: jQuery(this).attr('data-project_code'),
					w_str: jQuery(this).attr('data-w_str')
				}),
				dataType: 'JSON',
				success: function (obj) {
					if(obj.success == true) {
						var data = obj.data;
						var totalNominal = obj.total_nominal;
						htmlData = "";
						for(var i = 0; i < data.length; i++) {
							htmlData += '<tr>' +
								'<td style="width:7%;text-align:center;">'+ data[i].berkas_number +'</td>' +
								'<td style="width:20%;text-align:left;">'+ data[i].vendor_name +'</td>' +
								'<td style="width:7%;text-align:center;">'+ data[i].termin +'</td>' +
								'<td style="width:15%;text-align:left;">'+ data[i].nominal +'</td>' +
								'<td style="width:25%;text-align:left;" class="hidden-480">'+ data[i].note +'</td>' +
								'<td style="width:10%;text-align:center;" class="hidden-480">'+ data[i].payment_date +'</td>' +
								'<td style="width:10%;text-align:center;" class="hidden-480">'+ data[i].invoice_date +'</td>' +
							'</tr>';
						}
						
						jQuery('#table_detail_pembayaran').find('tbody').html('');
						jQuery('#table_detail_pembayaran').find('tbody').append(htmlData);	
						
						jQuery('#total_nominal_pembayaran').text(totalNominal);
					} else {
						alert('<?php echo $this->lang->line('global_error'); ?>');
					}
				} 
			});
		});
		
		jQuery('#project_code').on('change', function(){
			getVendor();
		});
		
		function getVendor() {
			var cAjax = new ceki.fnAjax({
				url : '<?php echo app_backend_url('get_vendor_by_project'); ?>',
				dataType : 'html',
				data: {
					project_code: jQuery('#project_code').val()
				},
				successCallBack : function(html) {
					console.log(html);
				},
				processHtml: function(html) {
					jQuery('#vendor_name').html(html);
				},
				successErrorCallBack : function(obj) {
					jQuery.gritter.add({
						title: '<?php echo $this->lang->line('global_notice'); ?> <span class="label label-info"><i class="icon-bullhorn"></i></span>',
						text: '<?php echo $this->lang->line('global_error'); ?>',
						sticky: false,
						time: 3000
					});
				}
			});
		}
		
		jQuery('.export').live('click', function(e) {
			e.preventDefault();
			var form = document.createElement('form');
			form.method = 'post';
			form.action = THIS_URL + '/export/'+jQuery(this).attr('data-type')+'/'+jQuery(this).attr('data-report_type');
			form.target = '_blank';
			
			var params = jQuery('#report_search').serializeArray();
			
			for (var i = 0; i < params.length; i++) {
				var obj = params[i];
				for (var key in obj) {
					var attrName = key;
					var attrValue = obj[key];
				}
				var data = document.createElement('input');
				data.type = 'hidden';
				data.name = obj.name;
				data.value = attrValue;
				form.appendChild(data);
			}
			
			document.body.appendChild(form);
			form.submit();
		});
	});
</script>