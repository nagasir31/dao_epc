<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offline extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->view('site_view');
	}
	
	public function mobile()
	{
		$this->load->view('mobile_view');
	}
	
	public function mobile5()
	{
		$this->load->view('mobile5_view');
	}
}