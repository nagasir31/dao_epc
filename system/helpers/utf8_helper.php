<?php 
function UTF8($text){
  if(is_array($text))
    {
      foreach($text as $k => $v)
    {
      $text[$k] = UTF8($v);
    }
      return $text;
    }

    $max = strlen($text);
    $buf = "";
    for($i = 0; $i < $max; $i++){
        $c1 = $text{$i};
        if($c1>="\xc0"){
          $c2 = $i+1 >= $max? "\x00" : $text{$i+1};
          $c3 = $i+2 >= $max? "\x00" : $text{$i+2};
          $c4 = $i+3 >= $max? "\x00" : $text{$i+3};
            if($c1 >= "\xc0" & $c1 <= "\xdf"){
                if($c2 >= "\x80" && $c2 <= "\xbf"){
                    $buf .= $c1 . $c2;
                    $i++;
                } else {
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = ($c1 & "\x3f") | "\x80";
                    $buf .= $cc1 . $cc2;
                }
            } elseif($c1 >= "\xe0" & $c1 <= "\xef"){
                if($c2 >= "\x80" && $c2 <= "\xbf" && $c3 >= "\x80" && $c3 <= "\xbf"){
                    $buf .= $c1 . $c2 . $c3;
                    $i = $i + 2;
                } else {
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = ($c1 & "\x3f") | "\x80";
                    $buf .= $cc1 . $cc2;
                }
            } elseif($c1 >= "\xf0" & $c1 <= "\xf7"){
                if($c2 >= "\x80" && $c2 <= "\xbf" && $c3 >= "\x80" && $c3 <= "\xbf" && $c4 >= "\x80" && $c4 <= "\xbf"){
                    $buf .= $c1 . $c2 . $c3;
                    $i = $i + 2;
                } else {
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = ($c1 & "\x3f") | "\x80";
                    $buf .= $cc1 . $cc2;
                }
            } else {
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = (($c1 & "\x3f") | "\x80");
                    $buf .= $cc1 . $cc2;				
            }
        } elseif(($c1 & "\xc0") == "\x80"){
                $cc1 = (chr(ord($c1) / 64) | "\xc0");
                $cc2 = (($c1 & "\x3f") | "\x80");
                $buf .= $cc1 . $cc2;				
        } else {
            $buf .= $c1;
        }
    }
    return $buf;
}

function fixUTF8($text){
  if(is_array($text)) {
    foreach($text as $k => $v) {
      $text[$k] = fixUTF8($v);
    }
    //return preg_replace("/(<\/?)(\w+)([^>]*>)/e","",$text);
    return $text;
  }
  
  $last = "";
  while($last <> $text){
    $last = $text;
    $text = UTF8(utf8_decode(UTF8($text)));
  }
  //return preg_replace("/(<\/?)(\w+)([^>]*>)/e","",$text);
  return $text;
}
